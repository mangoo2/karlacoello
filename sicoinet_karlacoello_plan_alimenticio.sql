-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 09-02-2021 a las 22:58:44
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sicoinet_karlacoello`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan_alimenticion_jueves`
--

DROP TABLE IF EXISTS `plan_alimenticion_jueves`;
CREATE TABLE IF NOT EXISTS `plan_alimenticion_jueves` (
  `planId` bigint(20) NOT NULL AUTO_INCREMENT,
  `consultaId` bigint(20) DEFAULT NULL,
  `contenido` text DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`planId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan_alimenticion_lunes`
--

DROP TABLE IF EXISTS `plan_alimenticion_lunes`;
CREATE TABLE IF NOT EXISTS `plan_alimenticion_lunes` (
  `planId` bigint(20) NOT NULL AUTO_INCREMENT,
  `consultaId` bigint(20) DEFAULT NULL,
  `contenido` text DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`planId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan_alimenticion_martes`
--

DROP TABLE IF EXISTS `plan_alimenticion_martes`;
CREATE TABLE IF NOT EXISTS `plan_alimenticion_martes` (
  `planId` bigint(20) NOT NULL AUTO_INCREMENT,
  `consultaId` bigint(20) DEFAULT NULL,
  `contenido` text DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`planId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan_alimenticion_miercoles`
--

DROP TABLE IF EXISTS `plan_alimenticion_miercoles`;
CREATE TABLE IF NOT EXISTS `plan_alimenticion_miercoles` (
  `planId` bigint(20) NOT NULL AUTO_INCREMENT,
  `consultaId` bigint(20) DEFAULT NULL,
  `contenido` text DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`planId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan_alimenticion_viernes`
--

DROP TABLE IF EXISTS `plan_alimenticion_viernes`;
CREATE TABLE IF NOT EXISTS `plan_alimenticion_viernes` (
  `planId` bigint(20) NOT NULL AUTO_INCREMENT,
  `consultaId` bigint(20) DEFAULT NULL,
  `contenido` text DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`planId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
