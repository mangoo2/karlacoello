-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 19-02-2021 a las 18:46:59
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sicoinet_karlacoello2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `n_aceites_grasas_con_proteina`
--

DROP TABLE IF EXISTS `n_aceites_grasas_con_proteina`;
CREATE TABLE IF NOT EXISTS `n_aceites_grasas_con_proteina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `equivalente` varchar(30) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `n_aceites_grasas_con_proteina`
--

INSERT INTO `n_aceites_grasas_con_proteina` (`id`, `nombre`, `equivalente`, `activo`) VALUES
(1, 'Ajonjolí', '4 cucharaditas', 0),
(2, 'Almendra', '10 piezas', 0),
(3, 'Cacahuate', '14 piezas', 0),
(4, 'Mantequilla de cacahuate, almendra o nuez', '2 cucharaditas', 0),
(5, 'Nuez de la india o en mitades', '7 piezas', 0),
(6, 'Pepitas', 'Un puñito', 0),
(7, 'Pistache', '18 piezas', 0),
(8, 'Aceituna negra', '', 1),
(9, 'Aceituna verde', '', 1),
(10, 'Aguacate', '', 1),
(11, 'Ajonjolí', '', 1),
(12, 'Almendras', '', 1),
(13, 'Avellana', '', 1),
(14, 'Cacahuates', '', 1),
(15, 'Guacamole', '', 1),
(16, 'Nuez', '', 1),
(17, 'Nuez de la india', '', 1),
(18, 'Pistachos', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `n_aceites_grasas_sin_proteína`
--

DROP TABLE IF EXISTS `n_aceites_grasas_sin_proteína`;
CREATE TABLE IF NOT EXISTS `n_aceites_grasas_sin_proteína` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `equivalente` varchar(30) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `n_aceites_grasas_sin_proteína`
--

INSERT INTO `n_aceites_grasas_sin_proteína` (`id`, `nombre`, `equivalente`, `activo`) VALUES
(1, 'Aceite comestible', '1 cucharadita', 0),
(2, 'Aceite en spray', '5 disparos', 0),
(3, 'Aceite de oliva', '1 cucharadita', 0),
(4, 'Aceituna verde o negra', '6 piezas', 0),
(5, 'Aguacate', '1/3 pieza', 0),
(6, 'Crema', '1 cucharada', 0),
(7, 'Guacamole', '2 cucharadas', 0),
(8, 'Mantequilla', '1 cucharadita', 0),
(9, 'Mayonesa', '1 cucharadita', 0),
(10, 'Vinagreta', '1/2 cucharada', 0),
(11, 'Aceite de canola soya oliva', '', 1),
(12, 'Aceite de girasol maíz cártamo', '', 1),
(13, 'Aceite en spray', '', 1),
(14, 'Pepitas sin cáscara', '', 1),
(15, 'Piñón', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `n_alimentos_origen_animal`
--

DROP TABLE IF EXISTS `n_alimentos_origen_animal`;
CREATE TABLE IF NOT EXISTS `n_alimentos_origen_animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `equivalente` varchar(30) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `n_alimentos_origen_animal`
--

INSERT INTO `n_alimentos_origen_animal` (`id`, `nombre`, `equivalente`, `activo`) VALUES
(1, 'Atún en agua', '1/2 lata', 0),
(2, 'Atún fresco', '30 g', 0),
(3, 'Bistec', '30 g', 0),
(4, 'Camarón gigante', '2 piezas', 0),
(5, 'Camarón pacotilla', '6 piezas', 0),
(6, 'Cangrejo', '2 piezas', 0),
(7, 'Cecina', '25 g', 0),
(8, 'Chuleta ahumada', '1/2 pieza', 0),
(9, 'Clara de huevo', '2 piezas', 0),
(10, 'Falda de res', '35 g', 0),
(11, 'Filete de pescado', '40 g', 0),
(12, 'Filete de res', '30 g', 0),
(13, 'Huevo entero', '1 pieza', 0),
(14, 'Jamón de pierna o de pavo', '2 rebanadas', 0),
(15, 'Milanesa de pollo o res', '30 g', 0),
(16, 'Pechuga de pollo a la plancha', '25 g', 0),
(17, 'Pollo deshebrado', '25 g', 0),
(18, 'Puntas de filete o de res', '30 g', 0),
(19, 'Queso cottage', '3 cucharadas', 0),
(20, 'Requesón', '2 cucharadas', 0),
(21, 'Salmón ahumado', '35 g', 0),
(22, 'Surimi', '2/3 barra', 0),
(23, 'Arrachera de res', '30 g', 0),
(24, 'Atún en aceite', '25 g', 0),
(25, 'Carne de cerdo', '40 g', 0),
(26, 'Carne de res molida', '30 g', 0),
(27, 'Lomo de cerdo', '40 g', 0),
(28, 'Milanesa de cerdo', '40 g', 0),
(29, 'Pescado blanco', '75 g', 0),
(30, 'Queso fresco, panela, oaxaca', '40 g', 0),
(31, 'Huevo', '25 g', 0),
(32, 'Filete pescado', '', 1),
(33, 'Huevo', '', 1),
(34, 'Clara', '', 1),
(35, 'Atún', '', 1),
(36, 'Pechuga pollo', '', 1),
(37, 'Bistec de res', '', 1),
(38, 'Carne de cerdo', '', 1),
(39, 'Salmón', '', 1),
(40, 'jamón de pechuga', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `n_azucar`
--

DROP TABLE IF EXISTS `n_azucar`;
CREATE TABLE IF NOT EXISTS `n_azucar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `equivalente` varchar(30) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `n_azucar`
--

INSERT INTO `n_azucar` (`id`, `nombre`, `equivalente`, `activo`) VALUES
(1, 'Cajeta', '2 cucharaditas', 1),
(2, 'Caramelo', '2 piezas pequeñas', 1),
(3, 'Cátsup', '2 cucharaditas', 1),
(4, 'Cocoa en polvo', '2 cucharaditas', 1),
(5, 'Gomitas', '4 piezas', 1),
(6, 'Mermelada', '2 cucharaditas', 1),
(7, 'Miel', '2 cucharaditas', 1),
(8, 'Chocolate Turín sin azúcar', '1 pieza chica', 1),
(9, 'Yakult', '1 pieza', 1),
(10, 'Arándanos', '4 piezas', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `n_cereal`
--

DROP TABLE IF EXISTS `n_cereal`;
CREATE TABLE IF NOT EXISTS `n_cereal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `equivalente` varchar(30) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `n_cereal`
--

INSERT INTO `n_cereal` (`id`, `nombre`, `equivalente`, `activo`) VALUES
(1, 'Arroz cocido', '1/4 taza', 0),
(2, 'Avena, amaranto, quinoa, couscous', '1/2 taza', 0),
(3, 'Baguette', '1/7 pieza', 0),
(4, 'Barrita de avena o granola', '1 pieza', 0),
(5, 'Barrita Special K', '1 pieza', 0),
(6, 'Barrita Stila', '1 pieza', 0),
(7, 'Barrita de amaranto Nutri Energy', '1 pieza', 0),
(8, 'Bolillo', '1/2 pieza', 0),
(9, 'Brownie Fiber One o Special K', '1 pieza', 0),
(10, 'Amaranto', '1/4 de taza', 0),
(11, 'Cheerios multigrano', '1/2 taza', 0),
(12, 'Puré de papa', '1/2 taza', 0),
(13, 'Elote cocido', '1 pieza', 0),
(14, 'Elote entero', '1/2 pieza', 0),
(15, 'Galletas María', '5 piezas', 0),
(16, 'Galletas saladas o integrales', '4 piezas', 0),
(17, 'Palomitas naturales', '2 tazas', 0),
(18, 'Pambazo', '1 pieza', 0),
(19, 'Pan blanco o integral', '1 rebanada', 0),
(20, 'Pan hot dog', '1/2 pieza', 0),
(21, 'Papa', '3/4 pieza', 0),
(22, 'Papa de cambray', '5 piezas', 0),
(23, 'Pasta cocida (fusilli, tallarín, espagueti,coditos)', '1/2 taza', 0),
(24, 'Pasta de municiones, letras o estrellitas', '1/4 taza', 0),
(25, 'Salmas', '1 paquetito', 0),
(26, 'Tortilla de maíz', '1 pieza', 0),
(27, 'Tortilla de nopal o tostada horneada', '2 piezas', 0),
(28, 'Tortilla de harina', '1 pieza', 0),
(29, 'Camote hervido', '', 1),
(30, 'Tortilla de maíz', '', 1),
(31, 'Pan Integral', '', 1),
(32, 'Avena cocida', '', 1),
(33, 'Tortilla de nopal', '', 1),
(34, 'Pasta', '', 1),
(35, 'Papa hervida', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `n_fruta`
--

DROP TABLE IF EXISTS `n_fruta`;
CREATE TABLE IF NOT EXISTS `n_fruta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `equivalente` varchar(30) NOT NULL,
  `activo` int(1) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `n_fruta`
--

INSERT INTO `n_fruta` (`id`, `nombre`, `equivalente`, `activo`) VALUES
(1, 'Agua de coco', '1 taza', 0),
(2, 'Cereza', '20 piezas', 0),
(3, 'Ciruela', 'piezas', 0),
(4, 'Ciruela', 'pasa 4 piezas', 0),
(5, 'Durazno', '2 piezas', 0),
(6, 'Fresa', '1 taza', 0),
(7, 'Granada', '2 piezas', 0),
(8, 'Guanábana', '3/4 pieza', 0),
(9, 'Higo', '2 piezas', 0),
(10, 'Jugo de naranja', '1/2 taza', 0),
(11, 'Kiwi', '1 1/2 pieza', 0),
(12, 'Mamey', ' 1/3 pieza', 0),
(13, 'Mandarina', '2 piezas', 0),
(14, 'Mango manila', ' 1 pieza', 0),
(15, 'Manzana', '1 pieza', 0),
(16, 'Pasitas o arándanos', ' 10 piezas', 0),
(17, 'Pera', '1/2 pieza', 0),
(18, 'Piña picada', '3/4 taza', 0),
(19, 'Plátano', '1/2 pieza', 0),
(20, 'Plátano macho', '1/4 pieza', 0),
(21, 'Toronja', '1 pieza', 0),
(22, 'Tuna', '2 piezas', 0),
(23, 'Uva', '18 piezas', 0),
(24, 'Platano dominíco', '', 0),
(25, 'Fresas', '', 0),
(26, 'Melón', '', 0),
(27, 'Papaya', '', 0),
(28, 'Sandia', '', 0),
(29, 'Naranja', '', 0),
(30, 'Jugo verde', '', 0),
(31, 'Jugo de naranja', '', 0),
(32, 'Fruta', '1 pieza', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `n_leche`
--

DROP TABLE IF EXISTS `n_leche`;
CREATE TABLE IF NOT EXISTS `n_leche` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `equivalente` varchar(30) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `n_leche`
--

INSERT INTO `n_leche` (`id`, `nombre`, `equivalente`, `activo`) VALUES
(1, 'Leche descremada', '1 taza', 0),
(2, 'Vitalinea bebible o sólido', '1 pieza', 0),
(3, 'Yogurt bajo en grasa o light', '3/4 de taza', 0),
(4, 'Yogurt para beber con fruta bajo en grasa o light', '1 pieza', 0),
(5, 'Leche de soya', '1 taza', 0),
(6, 'Gelatina Light Svelty', '150 g', 0),
(7, 'Yogurt para beber Svelty', '1 pieza', 0),
(8, 'Soful', '150 g', 0),
(9, 'Leche light o yogurt light', '1 taza', 0),
(10, 'Helado de yogurt', '1/2 taza', 0),
(11, 'Leche Carnation Light', '1/2 taza ', 0),
(12, 'YOGURT', '', 1),
(13, 'REQUESÓN', '', 1),
(14, 'QUESO COTTAGE', '', 1),
(15, 'QUESO FRESCO', '', 1),
(16, 'LECHE', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `n_leguminosas`
--

DROP TABLE IF EXISTS `n_leguminosas`;
CREATE TABLE IF NOT EXISTS `n_leguminosas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `equivalente` varchar(30) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `n_leguminosas`
--

INSERT INTO `n_leguminosas` (`id`, `nombre`, `equivalente`, `activo`) VALUES
(1, 'Alubia', '1/2 taza', 1),
(2, 'Alverjón', '½ taza', 1),
(3, 'Frijol cocido o refrito', '1/2 taza', 1),
(4, 'Soya texturizada o hidratada', '1/2 taza', 1),
(5, 'Hummus de garbanzo', '2 cucharadas', 1),
(6, 'Garbanzo cocido', '1/2 taza', 1),
(7, 'Haba cocida', '1/2 taza', 1),
(8, 'Lenteja cocida', '1/2 taza', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `n_libre`
--

DROP TABLE IF EXISTS `n_libre`;
CREATE TABLE IF NOT EXISTS `n_libre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `equivalente` varchar(30) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `n_libre`
--

INSERT INTO `n_libre` (`id`, `nombre`, `equivalente`, `activo`) VALUES
(1, 'Agua mineral', '1 taza', 1),
(2, 'Especias', '1 cucharadita', 1),
(3, 'Café americano', '1 taza', 1),
(4, 'Café o té sin azúcar', '1 taza', 1),
(5, 'Chile en polvo', '1 cucharadita', 1),
(6, 'Flor de jamaica', '1 taza', 1),
(7, 'Gelatina light', '1/2 taza', 1),
(8, 'Jengibre en polvo o fresco', '1 cucharada', 1),
(9, 'Limón', '1/2 pieza', 1),
(10, 'Mostaza', '1 cucharadita', 1),
(11, 'Pimienta', '1 cucharadita', 1),
(12, 'Raja de canela', '1 pieza', 1),
(13, 'Salsa de soya', '1 cucharadita', 1),
(14, 'Salsa Tabasco', '1 cucharadita', 1),
(15, 'Salsa inglesa o Maggi', '1 cucharadita', 1),
(16, 'Vainilla', '1 cucharadita', 1),
(17, 'Vinagre, vinagre balsámico, vinagre de manzana', '1 cucharadita', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `n_verduras`
--

DROP TABLE IF EXISTS `n_verduras`;
CREATE TABLE IF NOT EXISTS `n_verduras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `equivalente` varchar(30) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `n_verduras`
--

INSERT INTO `n_verduras` (`id`, `nombre`, `equivalente`, `activo`) VALUES
(1, 'Alcachofa', '1/2 taza', 0),
(2, 'Brócoli cocido', '1/2 taza', 0),
(3, 'Calabacita', '1 pieza', 0),
(4, 'Champiñón cocido rebanado', '1 1/2 taza', 0),
(5, 'Chayote', '1/2 taza', 0),
(6, 'Chícharo cocido sin vaina', '1/5 taza', 0),
(7, 'Chile poblano', '2/3 pieza', 0),
(8, 'Coliflor cocida', '3/4 taza', 0),
(9, 'Ejotes cocidos picados', '1/2 taza', 0),
(10, 'Espárragos', '6 piezas', 0),
(11, 'Espinaca cruda', '2 tazas', 0),
(12, 'Germen de alfalfa', '3 tazas', 0),
(13, 'Jícama picada', '1/2 taza', 0),
(14, 'Jitomate bola', '1 pieza', 0),
(15, 'Lechuga', '3 tazas', 0),
(16, 'Nopal cocido', '1 taza', 0),
(17, 'Pepino rebanado', '1 taza', 0),
(18, 'Zanahoria', '1/2 taza', 0),
(19, 'Pimientos morron', '1/2 taza', 0),
(20, 'Remolacha', '1/2 taza', 0),
(21, 'Verdura', '1 pieza', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
