var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    table();
    total_stock();
});
function reload_registro(){
    tabla.destroy();
    table();
}
function table(){
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Productos/getlistado_almacen",
            type: "post",
            "data":{id_producto:$('#id_producto').val(),lote:$('#lote').val(),fecha_actual:$('#fecha_actual').val()},
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data": null,
                "render": function ( data, type, row, meta ){
                return ChangeDate(row.fecha_registro);
                }
            },
            {"data":"lote"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                return ChangeDate(row.fecha_caducidad);
                }
            },
            {"data":"cantidad"},
            {"data":"stock"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html=''; 
                    var f_ca = new Date(row.fecha_caducidad);
                    var fecha_actual=$('#fecha_actual').val();
                    var f_al = new Date(fecha_actual);  
                    var fecha_mayor=$('#fecha_mayor').val();
                    var f_my = new Date(fecha_mayor);  
                    var etiqueta='';
                    if(f_al<=f_ca && f_my>=f_ca){ 
                        html+='<span class="label label-warning m-r-10">Próximo a caducar</span>';
                    }else if(f_al>f_ca){
                        html+='<span class="label label-danger m-r-10">Lote caducado</span>';
                    }else{
                        html+='<span class="label label-success m-r-10">Vigente</span>';
                    }  
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button" class="btn btn-danger btn-circle" onclick="elimnal_almacen_modal('+row.idalmacen+');"><i class="fas fa-trash-alt"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function total_stock(){
    $.ajax({
        type:'POST',
        url: base_url+"Productos/total_stock_get",
        data:{id:$('#id_producto').val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            if(data!=''){
                $('.p_stock').html(data);
            }else{
                $('.p_stock').html(0);
            }
            
        }
    });  
    
}

function ChangeDate(data){
  var parte=data.split('-');
  return parte[2]+"/"+parte[1]+"/"+parte[0];
}
function elimnal_almacen_modal(id){
    $('#id_producto_almacen').val(id);
    $('#eliminar_modal_almacen').modal();
}

function delete_registro_almacen(){
    var idp=$('#id_producto_almacen').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Productos/deleteregistro_almacen",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#eliminar_modal_almacen').modal('hide');
            setTimeout(function(){ 
                tabla.ajax.reload(); 
                total_stock();
            }, 1000); 
        }
    });  
}