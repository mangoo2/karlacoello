var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    table();
});
function modal_registro(){
    $('#idproveedor').val('0');
    $('#nombre').val('');
    $('#correo').val('');
    $('#celular').val('');
    $('#tel_oficina').val('');
    $('#contacto').val('');
    
    $('#check_fiscales').prop('checked',false);
    $('.text_fiscales').css('display','none');

    $('#razon_social').val('');
    $('#calle').val('');
    $('#no_ext').val('');
    $('#no_int').val('');
    $('#colonia').val('');
    $('#localidad').val('');
    
    $('#municipio').val('');
    $('#estado').val('');
    $('#pais').val('');
    $('#codigo_postal').val('');
    $('#rfc').val('');

	$('#registro_datos').modal();
}

function guarda_registro(){
    var form_register = $('#form_registro');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Proveedores/registro',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                  });
                setTimeout(function(){ 
                    window.location = base_url+'Proveedores';
                }, 1500);
            }
        });
    }   
}

function agregar_fiscales(){
    if($('#check_fiscales').is(':checked')){
        $('.text_fiscales').css('display','block');
    }else{
        $('.text_fiscales').css('display','none');
    }
}

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Proveedores/getlistado",
            type: "post",
            "data":{proveedor:$('#proveedor_busqueda').val()},
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"nombre"},
            {"data":"correo"},
            {"data":"celular"},
            {"data":"tel_oficina"},
            {"data":"contacto"},
            {"data": null,
                "render": function ( data, type, row, meta ){

                var html='<button type="button"\
                            data-idproveedor="'+row.idproveedor+'"\
                            data-nombre="'+row.nombre+'"\
                            data-correo="'+row.correo+'"\
                            data-celular="'+row.celular+'"\
                            data-tel_oficina="'+row.tel_oficina+'"\
                            data-contacto="'+row.contacto+'"\
                            data-check_fiscales="'+row.check_fiscales+'"\
                            data-razon_social="'+row.razon_social+'"\
                            data-calle="'+row.calle+'"\
                            data-no_ext="'+row.no_ext+'"\
                            data-no_int="'+row.no_int+'"\
                            data-colonia="'+row.colonia+'"\
                            data-localidad="'+row.localidad+'"\
                            data-municipio="'+row.municipio+'"\
                            data-estado="'+row.estado+'"\
                            data-pais="'+row.pais+'"\
                            data-codigo_postal="'+row.codigo_postal+'"\
                            data-rfc="'+row.rfc+'"\
                          class="btn btn-success btn-circle registro_datos_'+row.idproveedor+'" onclick="modal_editar('+row.idproveedor+')"><i class="far fa-edit"></i></button> <button type="button" class="btn btn-danger btn-circle" onclick="eliminar_registro('+row.idproveedor+');"><i class="fas fa-trash-alt"></i> </button>';
                
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function eliminar_registro(id){ 
    $('#id_proveedor').val(id);
    $('#elimina_registro_modal').modal();
}
function delete_registro(){
    var idp=$('#id_proveedor').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Proveedores/deleteregistro",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_registro_modal').modal('hide');
            setTimeout(function(){ 
                tabla.ajax.reload(); 
            }, 1000); 
        }
    });  
}

function modal_editar(id){
    $('#idproveedor').val($('.registro_datos_'+id).data('idproveedor'));
    $('#nombre').val($('.registro_datos_'+id).data('nombre'));
    $('#correo').val($('.registro_datos_'+id).data('correo'));
    $('#celular').val($('.registro_datos_'+id).data('celular'));    
    $('#tel_oficina').val($('.registro_datos_'+id).data('tel_oficina'));
    $('#contacto').val($('.registro_datos_'+id).data('contacto'));
    var check_fiscales = $('.registro_datos_'+id).data('check_fiscales');
    if(check_fiscales=='on'){
       $('#check_fiscales').prop('checked',true);
       $('.text_fiscales').css('display','block');
    }else{
       $('#verificar_check').prop('checked',false);
       $('.text_fiscales').css('display','none');
    }
    $('#razon_social').val($('.registro_datos_'+id).data('razon_social'));
    $('#calle').val($('.registro_datos_'+id).data('calle'));
    $('#no_ext').val($('.registro_datos_'+id).data('no_ext'));
    $('#no_int').val($('.registro_datos_'+id).data('no_int'));
    $('#colonia').val($('.registro_datos_'+id).data('colonia'));
    $('#localidad').val($('.registro_datos_'+id).data('localidad'));
    $('#municipio').val($('.registro_datos_'+id).data('municipio'));
    $('#estado').val($('.registro_datos_'+id).data('estado'));
    $('#pais').val($('.registro_datos_'+id).data('pais'));
    $('#codigo_postal').val($('.registro_datos_'+id).data('codigo_postal'));
    $('#rfc').val($('.registro_datos_'+id).data('rfc'));

    $('#registro_datos').modal();
}
