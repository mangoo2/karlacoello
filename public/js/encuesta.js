var base_url = $('#base_url').val();
$(document).ready(function($) {
	$('.btnenvio').click(function(event) {
		var form = $('#formencuenta');
		var datos = form.serialize();
		$.ajax({
	        type:'POST',
	        url: base_url+'Encuestas/capturar',
	        data:datos,
	        statusCode:{
	            404: function(data){
	                $.toast({
	                    heading: '¡Error!',
	                    text: 'No Se encuentra el archivo',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            },
	            500: function(){
	                $.toast({
	                    heading: '¡Error!',
	                    text: '500',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            }
	        },
	        success:function(data){
	            $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                  });
	            var html='<div class="row">\
					          <div class="col-md-12">\
					            <h4 class="card-title">Encuesta enviada</h4>\
					          </div>\
					        </div>';
	            $('.formencuentaenvio').html(html);
	        }
	    });
	});
});