var base_url = $('#base_url').val();
var tabla;
var tabla_tipo;
var tabla_cate;
var tabla_marca;
var edi_tipo=0;
var edi_cate=0;
var edi_marca=0;
$(document).ready(function() {
	$('#foto_avatar').change(function(){
	     document.getElementById('img_avatar').src = window.URL.createObjectURL(this.files[0]);
         $('#foto_validar').val(0); 
	});
	table();
    //$('textarea.js-auto-size').textareaAutoSize();
    $('#idproveedor').select2({
        width: '100%', 
        dropdownParent: $("#modal_almacen_datos"),
        //dropdownParent: $('#modal_almacen_datos'),
        //width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un proveedor',
        allowClear:true,
        ajax: {
            url: base_url + 'Productos/searchproveedor',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.idproveedor,
                        text: element.nombre,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
});

function modal_registro(){
    $('#idproducto').val(0);
    document.getElementById('img_avatar').src = base_url+'images/final.png';
    $('#foto_validar').val(0); 
    $('#descriccion').val('');
    $('#producto').val('');
    $('#codigo').val('');
    $('#tipo').val('');
    $('#categoria').val('');
    $('#marca').val('');
    $('#observaciones').val('');
    $('#compra').val('');
    $('#clinica').val('');
    $('#mayoreo').val('');
	$('#registro_datos').modal();
}
function modal_tipos(){
    edi_tipo=0;
	$('#modal_tipo').modal();
    reload_tipo();
}
function modal_categorias(){
    edi_cate=0;
	$('#modal_categoria').modal();
    reload_categoria();
}

function modal_marcas(){
    edi_marca=0;
    $('#modal_marca').modal();
    reload_marca();
}

function guarda_registro(){
    var form_register = $('#form_registro');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Productos/registro',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var id=data;
                var id_f=$('#foto_validar').val();
                if(id_f==0){
                    add_file(id);
                }
                
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                  });
                setTimeout(function(){ 
                    window.location = base_url+'Productos';
                }, 1500);
            }
        });
    }   
}

function add_file(id){
  //console.log("archivo: "+archivo);
  //console.log("name: "+name);
    var archivo=$('#foto_avatar').val()
	//var name=$('#foto_avatar').val()
	extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
	extensiones_permitidas = new Array(".jpeg",".png",".jpg");
	permitida = false;
    if($('#foto_avatar')[0].files.length > 0) {
	    for (var i = 0; i < extensiones_permitidas.length; i++) {
	       if (extensiones_permitidas[i] == extension) {
	       permitida = true;
	       break;
	       }
	    }  
	    if(permitida==true){
	      //console.log("tamaño permitido");
	        var inputFileImage = document.getElementById('foto_avatar');
	        var file = inputFileImage.files[0];
	        var data = new FormData();
	        data.append('foto',file);
	        data.append('id',id);
	        $.ajax({
	            url:base_url+'Productos/cargafiles',
	            type:'POST',
	            contentType:false,
	            data:data,
	            processData:false,
	            cache:false,
	            success: function(data) {
	                var array = $.parseJSON(data);
	                    //console.log(array.ok);
	                    /*
	                    if (array.ok=true) {
	                        swal("Éxito", "Guardado Correctamente", "success");
	                    }else{
	                        swal("Error!", "No Se encuentra el archivo", "error");
	                    }
	                    */
	            },
	            error: function(jqXHR, textStatus, errorThrown) {
	                var data = JSON.parse(jqXHR.responseText);
	            }
	        });
	    }
    }        
}

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
    tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Productos/getlistado",
            type: "post",
            "data":{producto:$('#producto_busqueda').val()},
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';        
                    if(row.foto!=''){
                        html='<img style="width: 40px; height: 40px; border-radius: 20px;" src="'+base_url+'uploads/productos/'+row.foto+'">';  
                    }else{
                        html='<img style="width: 40px; height: 40px; border-radius: 20px;" src="'+base_url+'images/final.png">';
                    }
                return html;
                }
            },
            {"data":"descriccion"},
            {"data":"producto"},
            {"data":"codigo"},
            {"data":"tipo_producto"},
            {"data":"categoria_producto"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    //var html='';//=row.fecha_caducidad;
                    //html+='<span class="label label-warning m-r-10">Próximo a caducar</span><br>\
                    //<span class="label label-danger m-r-10">Lote caducado</span><br>';
                return validar_stock(row.idproducto);
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idproducto="'+row.idproducto+'"\
                            data-descriccion="'+row.descriccion+'"\
                            data-producto="'+row.producto+'"\
                            data-codigo="'+row.codigo+'"\
                            data-tipo="'+row.tipo+'"\
                            data-categoria="'+row.categoria+'"\
                            data-marca="'+row.marca+'"\
                            data-observaciones="'+row.observaciones+'"\
                            data-compra="'+row.compra+'"\
                            data-clinica="'+row.clinica+'"\
                            data-mayoreo="'+row.mayoreo+'"\
                            data-foto="'+row.foto+'"\
                            class="btn btn-success btn-circle registro_datos_'+row.idproducto+'" onclick="modal_editar('+row.idproducto+')"><i class="far fa-edit"></i></button>\
                            <button type="button" class="btn btn-danger btn-circle" onclick="eliminar_registro('+row.idproducto+');"><i class="fas fa-trash-alt"></i> </button>\
                            <button type="button" class="btn btn-warning btn-circle" onclick="modal_almacen('+row.idproducto+');"><i class="fas fa-capsules"></i> </button>\
                            <button type="button" class="btn btn-secondary btn-circle" onclick="href_inventario('+row.idproducto+');"><i class="fas fa-clipboard-list"></i></button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function href_inventario(id){
    window.location = base_url+'Productos/inventario_producto/'+id;
}
function modal_editar(id){
    $('#idproducto').val($('.registro_datos_'+id).data('idproducto'));
    var foto = $('.registro_datos_'+id).data('foto');
    if(foto!=''){
        document.getElementById('img_avatar').src = base_url+'uploads/productos/'+$('.registro_datos_'+id).data('foto');
        $('#foto_validar').val(1); 
    }else{
        document.getElementById('img_avatar').src = base_url+'images/final.png';
        $('#foto_validar').val(0); 
    }    
    $('#descriccion').val($('.registro_datos_'+id).data('descriccion'));
    $('#producto').val($('.registro_datos_'+id).data('producto'));
    $('#codigo').val($('.registro_datos_'+id).data('codigo'));
    $('#tipo').val($('.registro_datos_'+id).data('tipo'));
    $('#categoria').val($('.registro_datos_'+id).data('categoria'));
    $('#marca').val($('.registro_datos_'+id).data('marca'));
    $('#observaciones').val($('.registro_datos_'+id).data('observaciones'));
    $('#compra').val($('.registro_datos_'+id).data('compra'));
    $('#clinica').val($('.registro_datos_'+id).data('clinica'));
    $('#mayoreo').val($('.registro_datos_'+id).data('mayoreo'));
    /*
    var estado_civil = $('.registro_datos_'+id).data('estado_civil')
    if(estado_civil==1){
        $("#estado_civil option[value='1']").attr("selected", true);
    }else if(estado_civil==2){
        $("#estado_civil option[value='2']").attr("selected", true);
    }else if(estado_civil==3){
        $("#estado_civil option[value='3']").attr("selected", true);
    }else if(estado_civil==4){
        $("#estado_civil option[value='4']").attr("selected", true);
    }else if(estado_civil==5){
        $("#estado_civil option[value='5']").attr("selected", true);
    }
    */
    $('#registro_datos').modal();
}

function eliminar_registro(id){ 
    $('#id_producto').val(id);
    $('#elimina_registro_modal').modal();
}
function delete_registro(){
    var idp=$('#id_producto').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Productos/deleteregistro",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_registro_modal').modal('hide');
            setTimeout(function(){ 
                tabla.ajax.reload(); 
            }, 1000); 
        }
    });  
}
//===========================================================
function guarda_registro_tipo(){
    var form_register = $('#form_registro_tipo');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_tipo").valid();
    if($valid) {
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Productos/registro_tipo',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var id=data;
                $('#nombre_tipo').val();
                if(edi_tipo==1){
                    $("#tipo option[value='"+id+"']").attr("selected", true);
                    $("#tipo option[value='"+id+"']").text($('#nombre_tipo').val());
                }else{
                    $('#tipo').prepend("<option value='"+id+"' selected>"+$('#nombre_tipo').val()+"</option>");    
                }
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                setTimeout(function(){ 
                    reload_tipo();
                    $('#idtipo_tipo').val(0);
                    $('#nombre_tipo').val('');
                }, 1000); 
                
            }
        });
    }   
}
function reload_tipo(){
    tabla_tipo=$("#table_datos_tipo").DataTable();
    tabla_tipo.destroy();
    table_tipo();
}
function table_tipo(){
    tabla_tipo=$("#table_datos_tipo").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Productos/getlistado_tipo",
            type: "post",
            error: function(){
               $("#table_datos_tipo").css("display","none");
            }
        },
        "columns": [
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idtipo="'+row.idtipo+'"\
                            data-nombre="'+row.nombre+'"\
                            class="btn waves-effect waves-light btn-rounded btn-xs btn-success tipo_'+row.idtipo+'" onclick="editar_tipo('+row.idtipo+')"><i class="far fa-edit"></i></button>\
                            <button type="button" class="btn waves-effect waves-light btn-rounded btn-xs  btn-danger" onclick="eliminar_registro_tipo('+row.idtipo+');"><i class="fas fa-trash-alt"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function editar_tipo(id){
    edi_tipo=1;
    $('#idtipo_tipo').val($('.tipo_'+id).data('idtipo'));
    $('#nombre_tipo').val($('.tipo_'+id).data('nombre'));
}
function eliminar_registro_tipo(id){
    $('#id_tipo').val(id);
    $('#elimina_registro_modal_tipo').modal();
}
function delete_registro_tipo(){
    var idp=$('#id_tipo').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Productos/deleteregistro_tipo",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){

            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#tipo option[value="'+idp+'"]').remove();
            $('#elimina_registro_modal_tipo').modal('hide');
            setTimeout(function(){ 
               reload_tipo(); 
            }, 1000); 
        }
    });  
}
//===========================================================
function guarda_registro_categoria(){
    var form_register = $('#form_registro_categoria');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_categoria").valid();
    if($valid) {
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Productos/registro_categoria',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                
                var id=data;
                $('#nombre_categoria').val();
                if(edi_cate==1){
                    $("#categoria option[value='"+id+"']").attr("selected", true);
                    $("#categoria option[value='"+id+"']").text($('#nombre_categoria').val());
                }else{
                    $('#categoria').prepend("<option value='"+id+"' selected>"+$('#nombre_categoria').val()+"</option>");    
                }
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                setTimeout(function(){ 
                    reload_categoria();
                    $('#idcategoria_categoria').val(0);
                    $('#nombre_categoria').val('');
                }, 1000); 
        
            }
        });
    }   
}

function reload_categoria(){
    tabla_cate=$("#table_datos_categoria").DataTable();
    tabla_cate.destroy();
    table_categoria();
}
function table_categoria(){
    tabla_cate=$("#table_datos_categoria").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Productos/getlistado_categoria",
            type: "post",
            error: function(){
               $("#table_datos_categoria").css("display","none");
            }
        },
        "columns": [
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idcategoria="'+row.idcategoria+'"\
                            data-nombre="'+row.nombre+'"\
                            class="btn waves-effect waves-light btn-rounded btn-xs btn-success cate_'+row.idcategoria+'" onclick="editar_categoria('+row.idcategoria+')"><i class="far fa-edit"></i></button>\
                            <button type="button" class="btn waves-effect waves-light btn-rounded btn-xs  btn-danger" onclick="eliminar_registro_cate('+row.idcategoria+');"><i class="fas fa-trash-alt"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function editar_categoria(id){
    edi_cate=1;
    $('#idcategoria_categoria').val($('.cate_'+id).data('idcategoria'));
    $('#nombre_categoria').val($('.cate_'+id).data('nombre'));
}
function eliminar_registro_cate(id){
    $('#id_cate').val(id);
    $('#elimina_registro_modal_cate').modal();
}
function delete_registro_cate(){
    var idp=$('#id_cate').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Productos/deleteregistro_cate",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#categoria option[value="'+idp+'"]').remove();
            $('#elimina_registro_modal_cate').modal('hide');
            setTimeout(function(){ 
               reload_categoria(); 
            }, 1000); 
        }
    });  
}
//===========================================================
function guarda_registro_marca(){
    var form_register = $('#form_registro_marca');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_marca").valid();
    if($valid) {
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Productos/registro_marca',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                
                var id=data;
                $('#nombre_marca').val();
                if(edi_marca==1){
                    $("#marca option[value='"+id+"']").attr("selected", true);
                    $("#marca option[value='"+id+"']").text($('#nombre_marca').val());
                }else{
                    $('#marca').prepend("<option value='"+id+"' selected>"+$('#nombre_marca').val()+"</option>");    
                }
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                setTimeout(function(){ 
                    reload_marca();
                    $('#idmarca_marca').val(0);
                    $('#nombre_marca').val('');
                }, 1000); 
        
            }
        });
    }   
}
function reload_marca(){
    tabla_marca=$("#table_datos_marcas").DataTable();
    tabla_marca.destroy();
    table_marcas();
}
function table_marcas(){
    tabla_marca=$("#table_datos_marcas").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Productos/getlistado_marca",
            type: "post",
            error: function(){
               $("#table_datos_marcas").css("display","none");
            }
        },
        "columns": [
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idmarca="'+row.idmarca+'"\
                            data-nombre="'+row.nombre+'"\
                            class="btn waves-effect waves-light btn-rounded btn-xs btn-success marc_'+row.idmarca+'" onclick="editar_marca('+row.idmarca+')"><i class="far fa-edit"></i></button>\
                            <button type="button" class="btn waves-effect waves-light btn-rounded btn-xs  btn-danger" onclick="eliminar_registro_marca('+row.idmarca+');"><i class="fas fa-trash-alt"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function editar_marca(id){
    edi_marca=1;
    $('#idmarca_marca').val($('.marc_'+id).data('idmarca'));
    $('#nombre_marca').val($('.marc_'+id).data('nombre'));
}

function eliminar_registro_marca(id){
    $('#id_marc').val(id);
    $('#elimina_registro_modal_marca').modal();
}
function delete_registro_marca(){
    var idp=$('#id_marc').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Productos/deleteregistro_marca",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#marca option[value="'+idp+'"]').remove();
            $('#elimina_registro_modal_marca').modal('hide');
            setTimeout(function(){ 
               reload_marca(); 
            }, 1000); 
        }
    });  
}
function modal_almacen(id){
    $('#idproducto_almacen').val(id);
    $('#modal_almacen_datos').modal();
}
function add_almacen(){
    var stock=$('#stock').val();
    var lote=$('#lote').val();
    var fecha_registro=$('#fecha_registro').val();
    var fecha_caducidad=$('#fecha_caducidad').val();
    var idproveedor=$('#idproveedor option:selected').val();
    var proveedor=$('#idproveedor option:selected').text();
    if(stock!='' && lote!='' && fecha_registro!='' && fecha_caducidad!=''){
        agregar_almacen(stock,lote,fecha_registro,fecha_caducidad,idproveedor,proveedor);    
    }else{
        $.toast({
            heading: 'Ateción!',
            text: 'Por favor verique que todos los campos esten llenos',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    } 
    
}
var aux_almacen=0;
function agregar_almacen(stock,lote,fecha_registro,fecha_caducidad,idproveedor,proveedor){
    var html='<tr class="row_'+aux_almacen+'">\
              <td><input type="hidden" id="stock_al" value="'+stock+'">\
                '+stock+'</td>\
              <td><input type="hidden" id="lote_al" value="'+lote+'">\
                '+lote+'</td>\
              <td><input type="hidden" id="fecha_registro_al" value="'+fecha_registro+'">\
                '+fecha_registro+'</td>\
              <td><input type="hidden" id="fecha_caducidad_al" value="'+fecha_caducidad+'">\
                '+fecha_caducidad+'</td>\
              <td><input type="hidden" id="proveedor_al" value="'+idproveedor+'">\
                '+proveedor+'</td>\
              <td><button type="button" class="btn btn-danger btn-circle" onclick="eliminar_registro_almacen('+aux_almacen+');">\
                <i class="fas fa-trash-alt"></i> </button>\
              </td>\
            </tr>';
    $('.table_datos_almacen #table_datos_almacen tbody').append(html);
    suma_total_stock();
    aux_almacen++;
    $('#stock').val('');
    $('#lote').val('');
    $('#fecha_registro').val('');
    $('#fecha_caducidad').val('');
}
function suma_total_stock(){
    var addtp = 0;
    var TABLAP = $(".table_datos_almacen #table_datos_almacen tbody > tr");            
    TABLAP.each(function(){         
        var totalmonto = $(this).find("input[id*='stock_al']").val();
        var vstotal = totalmonto;
        addtp += Number(vstotal);
    });
    $('.total_stock').html(addtp);
}
function registro_almacen(){
    
    var id= $('#idproducto_almacen').val();
    var aux_verificar=0;
    var DATA  = [];
        var TABLA = $(".table_datos_almacen #table_datos_almacen tbody > tr");         
        TABLA.each(function(){ 
            aux_verificar=1;  
            item = {};
            item ["idproducto"] = id;
            item ["stock"] = $(this).find("input[id*='stock_al']").val();
            item ["lote"] = $(this).find("input[id*='lote_al']").val();
            item ["fecha_registro"] = $(this).find("input[id*='fecha_registro_al']").val();
            item ["fecha_caducidad"] = $(this).find("input[id*='fecha_caducidad_al']").val();
            item ["idproveedor"] = $(this).find("input[id*='proveedor_al']").val();
            DATA.push(item);   
        });
      if(aux_verificar==1){
        $('.btn_almacen').attr('disabled',true);
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
          data: INFO, 
          type: 'POST',
          url : base_url + 'Productos/registra_almacen',
          processData: false, 
          contentType: false,
          async: false,
          statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
          },
          success: function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                setTimeout(function(){ 
                    window.location = base_url+'Productos';
                }, 1500); 
          }
        }); 
    }else{
        $.toast({
            heading: 'Ateción!',
            text: 'No se a agregado ningun producto',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }
}
function eliminar_registro_almacen(id){
    $('.row_'+id).remove();
    suma_total_stock();
}
function validar_stock(id){
    var aux_stock=0;
    $.ajax({
          data:{id:id}, 
          type: 'POST',
          url : base_url + 'Productos/validar_stock_almacen',
          //processData: false, 
          //contentType: false,
          async: false,
          statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
          },
          success: function(data){
            aux_stock=data;
          }
      }); 
    return aux_stock;
    
}