var base_url = $('#base_url').val();
var tabla;
$(document).ready(function () {

    text_citas_pendientes();
    setTimeout(function(){ 
        $('.skin-blue').addClass("mini-sidebar");
        $('.img_unne').css('display','none');
    }, 120);
    
    $('.datepickerc').datepicker({
    }).on('changeDate', function(ev){
        var fecha = new Date(ev.date);
        var dd = fecha.getDate();
        var mm = fecha.getMonth()+1;
        var yyyy = fecha.getFullYear();
        if(dd<10) {
            dd='0'+dd;
        } 
        if(mm<10) {
            mm='0'+mm;
        } 
        var f = yyyy+'-'+mm+'-'+dd;
        date2 = moment(f, "YYYY-MM-DD");
        $("#calendar").fullCalendar( 'gotoDate', date2);
    });

     $('.selectpicker').selectpicker();
    calendario();
    var id_p=$('#id_paciente').val();
    if(id_p>=1){
        mosrar_cita(0,2,id_p,0,1);
    } 
    //
    lista_espera_get();
    resumen_hoy();
});    

function mosrar_cita(id,tipo,idp,plg,tip){
    var aux_idp=0;
    if(tip==1){
        aux_idp=idp;
    }else{
        aux_idp=0;
    }
    //$("#tipo_cita_agenda option[value="+tipo+"]").attr("selected",true);
    $('#tipo_cita_agenda option[value="'+tipo+'"]').attr("selected", "selected");
    $('#tipo_cita_agenda').prop('disabled', true);
    $('#tipo_cita_agenda').selectpicker('refresh');
    $.ajax({
        type:'POST',
        url: base_url+'Agenda/tipo_cita_text',
        data:{
            idc:id,
            tipo:tipo,
            idp:aux_idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){

            $('.tipo_cita_texto').html(data);
            $('.t_calendario').css('display','none');
            $("#color_spec").spectrum({
                showPaletteOnly: true,
                togglePaletteOnly: true
            });
        }
    });
    
    if(id!=0){
        $('#idcita_p').val(id);
        $('#idpaciente_p').val(idp);
        if(plg==1){
            $('.forma_pago_texto').css('display','block');
            $('.corte_hoy_txt').css('display','none');
        }else{
            $('.forma_pago_texto').css('display','none');
            $('.corte_hoy_txt').css('display','block');
        }  
    }else{
        $('.forma_pago_texto').css('display','none');
        $('.corte_hoy_txt').css('display','block');
    }

}

function tipo_cita(){
    var tipo_n=$('#tipo_cita_agenda option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+'Agenda/tipo_cita_text',
        data: {
           idc:0, 
           tipo:tipo_n,
           idc:0
        },
        success:function(data){
            $('.tipo_cita_texto').html(data);
            //
            $("#color_spec").spectrum({
                showPaletteOnly: true,
                togglePaletteOnly: true
            });
            //
        }
    });
    var n_c=parseFloat(tipo_n);
    if(n_c>0){
        $('#tipo_cita_agenda').prop('disabled', true);
        $('#tipo_cita_agenda').selectpicker('refresh');
        $('.t_calendario').css('display','none');
    }
}
function quitar_tipo_p(){
    $('.tipo_cita_texto').html('');
}
function mostrar_tel_c_o(){
    $('.tele_c_o').css('display','block');
    $('.btn_tel').html('<button type="button" class="btn btn-danger btn-circle" onclick="ocultar_tel_c_o()">-</button></div> ');
}
function ocultar_tel_c_o(){
    $('.tele_c_o').css('display','none');
    $('.btn_tel').html('<button type="button" class="btn btn-info btn-circle" onclick="mostrar_tel_c_o()">+</button></div>');
}
function mostra_calendario(){
    $('.t_calendario').css('display','block');
    $('.tipo_cita_texto').html('');
    $('#tipo_cita_agenda').prop('disabled', false);
    $('#tipo_cita_agenda').selectpicker('refresh');
    $('#tipo_cita_agenda option').attr("selected",false);
    $('#tipo_cita_agenda').selectpicker('refresh');
    $('.forma_pago_texto').css('display','none');
    $('.corte_hoy_txt').css('display','block');
    resumen_hoy();
}    
function restablecer_calendario(){
    var fecha = new Date();
    var dd = fecha.getDate();
    var mm = fecha.getMonth()+1;
    var yyyy = fecha.getFullYear();
    if(dd<10) {
        dd='0'+dd;
    } 
    if(mm<10) {
        mm='0'+mm;
    } 
    var f = yyyy+'-'+mm+'-'+dd;
    date2 = moment(f, "YYYY-MM-DD");
    $("#calendar").fullCalendar( 'gotoDate', date2);
}
function guarda_cita(){
    var listaespera_aux=0;
    if($('#listaespera').is(':checked')){
        listaespera_aux=1;
    }else{
        listaespera_aux=0;
    }
    
    if(listaespera_aux==0){
        var form_register = $('#form_cita');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                fecha_consulta:{
                  required: true
                },
                hora_de:{
                  required: true
                },
                hora_hasta:{
                  required: true
                }
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
    }

    var form_register2 = $('#form_paciente');
    var error_register2 = $('.alert-danger', form_register2);
    var success_register2 = $('.alert-success', form_register2);

    var $validator2=form_register2.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            },
            apll_paterno:{
              required: true
            },
            apll_materno:{
              required: true
            },
            sexo:{
              required: true
            },
            celular:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register2.fadeOut(500);
                error_register2.fadeIn(500);
                scrollTo(form_register2,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_cita").valid();
    var $valid2 = $("#form_paciente").valid();
    if($valid && $valid2) {
        var datos = form_register2.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Agenda/registraPaciente',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var id=data;
                var datos = $('#form_cita').serialize()+'&idpaciente='+id;
                $.ajax({
                    type:'POST',
                    url: base_url+'Agenda/registraCita',
                    data: datos,
                    statusCode:{
                        404: function(data){
                            $.toast({
                                heading: '¡Error!',
                                text: 'No Se encuentra el archivo',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'error',
                                hideAfter: 3500
                            });
                        },
                        500: function(){
                            $.toast({
                                heading: '¡Error!',
                                text: '500',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'error',
                                hideAfter: 3500
                            });
                        }
                    },
                    success:function(data){
                        enviar_cita(data);
                    }
                });
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                  });
                setTimeout(function(){ 
                    window.location = base_url+'Agenda';
                }, 1500);
            }
        });
                
    } 
     
}
function buscar_paciente(){
    $.ajax({
        type:'POST',
        url: base_url+'Agenda/buscarPaciente',
        data:{nombre:$('#buscar_nombre_txt').val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.lista_empleado').html(data);
        }
    });
}
function buscar_paciente_txt(){
    var pacientes_buscartxt=$('#pacientes_buscartxt').val();
    if(pacientes_buscartxt!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Agenda/buscarPaciente',
            data:{nombre:pacientes_buscartxt},
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){  
                    $('.pacientes_buscar_t').html(data);
            }
        });
    }else{
        setTimeout(function(){ 
            $('.pacientes_buscar_t').html('');
        }, 1000);
    }
}
function guardar_cita_tipo3(){
    var form_register = $('#form_personal');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            fecha_consulta:{
              required: true
            },
            hora_de:{
              required: true
            },
            hora_hasta:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_personal").valid();
    if($valid) {
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Agenda/registraCita',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                  });
                setTimeout(function(){ 
                    window.location = base_url+'Agenda';
                }, 1500);
            }
        });
                
    }  
}
function guardar_cita_tipo4(){
    var form_register = $('#form_congreso');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            fecha_consulta:{
              required: true
            },
            fechafin:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_congreso").valid();
    if($valid) {
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Agenda/registraCita',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                  });
                setTimeout(function(){ 
                    window.location = base_url+'Agenda';
                }, 1500);
            }
        });
                
    }  
}
function guardar_cita_tipo5(){
    var form_register = $('#form_vacacional');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            fecha_consulta:{
              required: true
            },
            fechafin:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_vacacional").valid();
    if($valid) {
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Agenda/registraCita',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                  });
                setTimeout(function(){ 
                    window.location = base_url+'Agenda';
                }, 1500);
            }
        });
                
    }  
}
function verexpediente(id){
    window.location = base_url+'Pacientes/paciente/'+id;
}
function mostrarpacientetipo1(){
    $('.paciente_texto_tipo1').css('display','none');
    $('.paciente_texto_tipo1_e').css('display','block');
}
///////////Editar
function guarda_citae(id){
    var form_register = $('#form_cita');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            fecha_consulta:{
              required: true
            },
            hora_de:{
              required: true
            },
            hora_hasta:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_cita").valid();
    if($valid) {
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Agenda/registraCita',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                  });
                setTimeout(function(){ 
                    window.location = base_url+'Agenda';
                }, 1500);
            }
        });         
    }  
}
function listaespera_chek(){
    if($('#listaespera').is(':checked')){
        $('.ocultar_lista_prioridad').css('display','block');
    }else{
        $('.ocultar_lista_prioridad').css('display','none');
    }
}
function cita_status1(){
    if($('#cita_confirmada').is(':checked')){
        $('#paciente_no_llego').prop('checked',false);
        $('.forma_pago_texto').css('display','none');
        $('.corte_hoy_txt').css('display','block');
    }else{
        $('#cita_confirmada').prop('checked',false);   
        $('#paciente_llego').prop('checked',false);   
    }  
    var datos = $('#form_cita_check').serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Agenda/check_cita',
        data: datos,
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Guardado Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
              });
            setTimeout(function(){ 
                //window.location = base_url+'Pacientes/paciente/'+id;
            }, 1500);
        }
    });
    setTimeout(function(){ 
        $('#calendar').fullCalendar( 'refetchEvents' );
    }, 500); 

    resumen_hoy();
}
function cita_status2(){
    if($('#paciente_llego').is(':checked')){
        $('#cita_confirmada').prop('checked',true); 
        $('#paciente_no_llego').prop('checked',false);   
        $('.forma_pago_texto').css('display','block');
        $('.corte_hoy_txt').css('display','none');
    }else{
        $('.forma_pago_texto').css('display','none');
        $('.corte_hoy_txt').css('display','block');
    }
    var datos = $('#form_cita_check').serialize(); 
    $.ajax({
        type:'POST',
        url: base_url+'Agenda/check_cita',
        data: datos,
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Guardado Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
              });
            setTimeout(function(){ 
                //window.location = base_url+'Pacientes/paciente/'+id;
            }, 1500);
        }
    }); 
    setTimeout(function(){ 
        $('#calendar').fullCalendar( 'refetchEvents' );
    }, 500); 
    resumen_hoy();
}
function cita_status3(){
    if($('#paciente_no_llego').is(':checked')){
        $('#cita_confirmada').prop('checked',false);   
        $('#paciente_llego').prop('checked',false);   
        $('.forma_pago_texto').css('display','none');
        $('.corte_hoy_txt').css('display','block');
    }  
    var datos = $('#form_cita_check').serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Agenda/check_cita',
        data: datos,
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Guardado Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
              });
            setTimeout(function(){ 
                //window.location = base_url+'Pacientes/paciente/'+id;
            }, 1500);
        }
    });  
    setTimeout(function(){ 
        $('#calendar').fullCalendar( 'refetchEvents' );
    }, 500); 
    resumen_hoy();
}
function listaesperar_check3(){
    if($('#listaespera3').is(':checked')){
        $('.horario_consulta_ckeck').css('display','none'); 
        $('#hora_de').val('00:00:00');
        $('#hora_hasta').val('23:53:00');
    }else{
        $('.horario_consulta_ckeck').css('display','block');

    } 
}
function modal_cancelar(id){
    if(id==0){
        $('.t_calendario').css('display','block');
        $('.tipo_cita_texto').html('');
        $('#tipo_cita_agenda').prop('disabled', false);
        $('#tipo_cita_agenda').selectpicker('refresh');
        $('#tipo_cita_agenda option').attr("selected",false);
        $('#tipo_cita_agenda').selectpicker('refresh');
    }else{
        $('#cancelar_cita_modal').modal();
        $('#idcancelar_cita').val(id);
    }
}
function cancelar_cita(){
    var id=$('#idcancelar_cita').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Agenda/cancelar_cita",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Cancelada Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#cancelar_cita_modal').modal('hide');
            $('.t_calendario').css('display','block');
            $('.tipo_cita_texto').html('');
            $('#tipo_cita_agenda').prop('disabled', false);
            $('#tipo_cita_agenda').selectpicker('refresh');
            $('#tipo_cita_agenda option').attr("selected",false);
            $('#tipo_cita_agenda').selectpicker('refresh');
            $('#calendar').fullCalendar( 'refetchEvents' );
        }
    });  
}
function modal_reenviarformulario(id){
    $('#modal_reenviar_f').modal();
    $('#correo_pac').val($('.correo_paciente').data('correop'));
    $('#correo_pac2').val($('.correo_paciente').data('correop2'));
    $('#idcita_rv').val(id);
}

function enviar_formulario(){
    var id = $('#idcita_rv').val();
    var correo = $('#correo_pac').val();
    var correo2 = $('#correo_pac2').val();
    if(correo!=''){
        $('#modal_reenviar_f').modal('hide');
        $.toast({
            heading: 'Éxito',
            text: 'Sé está enviado el correo',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
        });
        $.ajax({
            type:'POST',
            url: base_url+"Agenda/enviar",
            data:{id:id,correo:correo},
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Enviado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
            /////
                if(correo2!=''){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Agenda/enviar",
                        data:{id:id,correo:correo2},
                        success:function(data){
                        }
                    }); 
                }
            ///// 
            }
        }); 
    }else{
        $.toast({
            heading: '¡Atención!',
            text: 'El campo debe estar lleno',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }    
}
function enviar_cita(id){
    var correo = $('#correo_p').val();
    $.ajax({
            type:'POST',
            url: base_url+"Agenda/enviar",
            data:{id:id,correo:correo},
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Enviado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
            }
        }); 
}
function enviar_whatsapp(){
    var lada=$('#lada option:selected').val();
    var cel=$('.info_pas').data('celular_w');
    var per=$('.info_pas').data('pernombre_w');
    var fecha=$('.info_pas').data('fecha_w');
    var res = fecha.split("-");
    var hora=$('.info_pas').data('hora_w');
    var resh = hora.split(":");
    window.open('https://wa.me/'+lada+''+cel+'?text=Buen%20d%C3%ADa,%20nos%20comunicamos%20del%20consultorio%20de%20'+'Dr.%20Karla%20Coello'+'%20para%20recordarle%20que%20tiene%20una%20cita%20programada%20para%20el%20\
                 d%C3%ADa%20'+res[2]+'/'+res[1]+'/'+res[0]+'%20a%20las%20'+resh[0]+':'+resh[1]+' hrs.%20%C2%BFDesea%20confirmar%20su%20cita?', '_blank');
    /*
    window.open('https://wa.me/'+lada+''+cel+'?text=Buen%20d%C3%ADa,%20nos%20comunicamos%20del%20consultorio%20de%20'+per+'%20para%20recordarle%20que%20tiene%20una%20cita%20programada%20para%20el%20\
                 d%C3%ADa%20'+res[2]+'/'+res[1]+'/'+res[0]+'%20a%20las%20'+resh[0]+':'+resh[1]+' hrs.%20%C2%BFDesea%20confirmar%20su%20cita?', '_blank');
    */             
}

function text_citas_pendientes(){
    $.ajax({
        type:'POST',
        url: base_url+"Agenda/total_citas_pendientes",
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.text_consultas_pendientes').html(data);
            //reload_tabla_citas_pendites();
        }
    }); 
}
function modal_total_citas_pendientes(){
    $('#modal_total_citas_pendientes').modal();
    $('.tabla_c_p').html('<table class="table" id="table_pendientes_citas">\
                    <thead class="bg-blue">\
                        <tr>\
                            <th>Nombre</th>\
                            <th>Teléfono</th>\
                            <th>Última consulta </th>\
                            <th></th>\
                        </tr>\
                    </thead>\
                    <tbody>\
                    </tbody>\
                </table>');
    tablecitaspendientes();
}
function tablecitaspendientes(){
    tabla=$("#table_pendientes_citas").DataTable({
        "serverSide": true,
        "searching": false,
        "info":     false,
        "paging": false,
        "ajax": {
           "url": base_url+"Agenda/getlistadocitaspendientes",
           type: "post",
            error: function(){
               $("#table_pacientes").css("display","none");
            }
        },
        "columns": [
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html=row.nombre+' '+row.apll_paterno+' '+row.apll_materno;        
                return html;
                }
            },
            {"data":"celular"},
            {"data":"fecha_consulta"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<a href="'+base_url+'Agenda/?id='+row.idpaciente+'" class="btn waves-effect waves-light btn-sm btn-outline-info">Agendar cita</a>\
                        <button type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-outline-danger" onclick="eliminar_citapendiente('+row.idcita+')"><i class="fas fa-minus"></i></button>';        
                return html;
                }
            },
        ],
    });
}
function reload_tabla_citas_pendites(){
    tabla.destroy();
    tablecitaspendientes();
}
function eliminar_citapendiente(id){
    $.ajax({
        type:'POST',
        url: base_url+"Agenda/eliminar_cita_pendiente",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            text_citas_pendientes();
            reload_tabla_citas_pendites();
        }
    }); 
}
function guardar_pago(){
    var idcita_p = $('#idcita_p').val();
    var idpaciente_p = $('#idpaciente_p').val();
    var fecha_pago_p = $('#fecha_pago_p').val();
    var metodo_pago_p = $('input:radio[name=metodo_pago_p]:checked').val();
    var factura_p = $('input:checkbox[name=factura_p]:checked').val();
    ////////////////
    var monto_p = $('#monto_p').val();
    var notas_p = $('#notas_p').val();
    ////////////////
    $.ajax({
        type:'POST',
        url: base_url+"Agenda/guardar_pago_paciente",
        data:{idpaciente:idpaciente_p,
              fecha_pago:fecha_pago_p,
              metodo_pago:metodo_pago_p,
              factura:factura_p},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            var id_pago=data;
            $.ajax({
                type:'POST',
                url: base_url+"Agenda/guardar_pago_paciente_detalles",
                data:{idpagos_paciente:id_pago,
                      monto_p:monto_p,
                      notas_p:notas_p},
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success:function(data){
                }
            });
            ////////////////////
            $.toast({
                heading: 'Éxito',
                text: 'Guardado Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            setTimeout(function(){ 
                window.location = base_url+'Agenda';
            }, 500);
        }
    });
    //=====
    $.ajax({
        type:'POST',
        url: base_url+"Agenda/editar_cita_pago",
        data:{id:idcita_p},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
        }
    });
    //=====
}
function lista_espera_get(){
    $.ajax({
        type:'POST',
        url: base_url+"Agenda/gat_lista_espera",
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.lista_espera_txt').html(data);
        }
    });   
}
function btn_text_prioridad_a(){
    var int_p=$('#int_prioridad_a').val();
    var aux_p=0;
    if(int_p==0){
        $('#int_prioridad_a').val(1); aux_p=1;
    }else{
        $('#int_prioridad_a').val(0); aux_p=0;
    }
    if(aux_p==1){
        $('.texto_prioridad_a').css('display','block');
    }else{
        $('.texto_prioridad_a').css('display','none');
    }
}
function btn_text_prioridad_m(){
    var int_p=$('#int_prioridad_m').val();
    var aux_p=0;
    if(int_p==0){
        $('#int_prioridad_m').val(1); aux_p=1;
    }else{
        $('#int_prioridad_m').val(0); aux_p=0;
    }
    if(aux_p==1){
        $('.texto_prioridad_m').css('display','block');
    }else{
        $('.texto_prioridad_m').css('display','none');
    }
}
function btn_text_prioridad_b(){
    var int_p=$('#int_prioridad_b').val();
    var aux_p=0;
    if(int_p==0){
        $('#int_prioridad_b').val(1); aux_p=1;
    }else{
        $('#int_prioridad_b').val(0); aux_p=0;
    }
    if(aux_p==1){
        $('.texto_prioridad_b').css('display','block');
    }else{
        $('.texto_prioridad_b').css('display','none');
    }
}
function editar_cita_espera(id,idp,plg){
    mosrar_cita(id,2,idp,plg,0);
    //mosrar_cita(event.id_cita,event.tipo,event.idpaciente,event.paciente_llego,0);
}

function resumen_hoy(){
    $.ajax({
        type:'POST',
        url: base_url+"Agenda/get_resumen_hoy",
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.resumen_hoy').html(data);
            //reload_tabla_citas_pendites();
        }
    }); 
}

    ///
    /*
    $('.select2').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar Paciente',
        ajax: {
            url: base_url + 'Agenda/searchpaciente',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre+' '+element.apll_paterno+' '+element.apll_materno+'<p>fsd</p>' 
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    */

/*
function calendario(){
    $('#calendar').fullCalendar({
        
        allDaySlot: false,
        //minTime: "08:00:00",
        //maxTime: "20:00:00",
        slotLabelFormat:"HH:mm",
        defaultView: 'agendaWeek',
        blockDays:[3],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'agendaWeek,agendaDay'
            //right: 'month,agendaWeek,agendaDay'
        
        },
        events: function(start, end, timezone, callback) {
            $.ajax({
                url: base_url+'Agenda/listcitas',
                dataType: 'json',
                data: {
                    start: start.unix(),
                    end: end.unix()
                },
                success: function(doc) {
                    var datos=doc;
                    var events = [];
                    datos.forEach(function(r) {
                        if(r.tipo_cita==1 || r.tipo_cita==2){
                            events.push({
                                id_cita: r.idcita,
                                title: r.nombre+' '+r.apll_paterno+' '+r.apll_materno,
                                start: r.fecha_consulta+'T'+r.hora_de,
                                end: r.fecha_consulta+'T'+r.hora_hasta,
                                color: r.color,
                                tipo: r.tipo_cita,
                                cita_confirmada: r.cita_confirmada,
                                paciente_llego: r.paciente_llego,
                                paciente_no_llego: r.paciente_no_llego,
                                idpaciente: r.idpaciente, 
                            });
                        }else if(r.tipo_cita==3){
                            events.push({
                                id_cita: r.idcita,
                                title: r.motivo_consulta,
                                start: r.fecha_consulta+'T'+r.hora_de,
                                end: r.fecha_consulta+'T'+r.hora_hasta,
                                color: r.color,
                                tipo: r.tipo_cita,
                                cita_confirmada: r.cita_confirmada,
                                paciente_llego: r.paciente_llego,
                                paciente_no_llego: r.paciente_no_llego,
                                idpaciente:0
                            });
                        }else if(r.tipo_cita==4){
                            events.push({
                                id_cita: r.idcita,
                                title: r.motivo_consulta,
                                start: r.fecha_consulta+'T'+'00:00:00',
                                end: r.fechafin+'T'+'23:59:59',
                                color: r.color,
                                tipo: r.tipo_cita,
                                cita_confirmada: r.cita_confirmada,
                                paciente_llego: r.paciente_llego,
                                paciente_no_llego: r.paciente_no_llego,
                                idpaciente:0
                            });
                        }else if(r.tipo_cita==5){
                            events.push({
                                id_cita: r.idcita,
                                title: r.motivo_consulta,
                                start: r.fecha_consulta+'T'+'00:00:00',
                                end: r.fechafin+'T'+'23:59:59',
                                color: r.color,
                                tipo: r.tipo_cita,
                                cita_confirmada: r.cita_confirmada,
                                paciente_llego: r.paciente_llego,
                                paciente_no_llego: r.paciente_no_llego,
                                idpaciente:0
                            });
                        }

                    });
                    callback(events);
                }
            });
        },
        eventRender: function(event, element){ 
            //console.log(event);
            element.click(function() {
                mosrar_cita(event.id_cita,event.tipo,event.idpaciente,event.paciente_llego,0);
            });  
            if(event.paciente_no_llego==1){
                element.find(".fc-title").prepend('<i class="fas fa-stop" style="color:#8C77FE"></i> ');
            }else{
                if(event.paciente_llego==1){
                    element.find(".fc-title").prepend('<i class="fas fa-stop" style="color:#28e652"></i> ');
                }else{
                    if(event.cita_confirmada==1){
                        element.find(".fc-title").prepend('<i class="fas fa-stop" style="color:yellow"></i> ');
                    }
                }
            }    
        }

    });
}
*/