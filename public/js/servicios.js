var base_url = $('#base_url').val();
var tabla_cate;
var edi_cate=0;
var tabla;
$(document).ready(function() {
    table_servicios();
});
function modal_registro(){
    $('#idservicio').val('0');
    $('#nombre').val('');
    $('#descripcion').val('');
    $('#cantidad').val('');
    $('#costo').val('');
    $('#duracion').val('');
    $('#categoria').val('');

	$('#registro_datos').modal();
}

function guarda_registro(){
    var form_register = $('#form_registro');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Servicios/registro',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                  });
                setTimeout(function(){ 
                    window.location = base_url+'Servicios';
                }, 1500);
            }
        });
    }   
}

function modal_categorias(){
    edi_cate=0;
	$('#modal_categoria').modal();
    reload_categoria();
}

//===========================================================
function guarda_registro_categoria(){
    var form_register = $('#form_registro_categoria');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_categoria").valid();
    if($valid) {
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Servicios/registro_categoria',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                
                var id=data;
                $('#nombre_categoria').val();
                if(edi_cate==1){
                    $("#categoria option[value='"+id+"']").attr("selected", true);
                    $("#categoria option[value='"+id+"']").text($('#nombre_categoria').val());
                }else{
                    $('#categoria').prepend("<option value='"+id+"' selected>"+$('#nombre_categoria').val()+"</option>");    
                }
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                setTimeout(function(){ 
                    reload_categoria();
                    $('#idcategoria_categoria').val(0);
                    $('#nombre_categoria').val('');
                }, 1000); 
        
            }
        });
    }   
}

function reload_categoria(){
    tabla_cate=$("#table_datos_categoria").DataTable();
    tabla_cate.destroy();
    table_categoria();
}

function table_categoria(){
    tabla_cate=$("#table_datos_categoria").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Servicios/getlistado_categoria",
            type: "post",
            error: function(){
               $("#table_datos_categoria").css("display","none");
            }
        },
        "columns": [
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idcategoria="'+row.idcategoria+'"\
                            data-nombre="'+row.nombre+'"\
                            class="btn waves-effect waves-light btn-rounded btn-xs btn-success cate_'+row.idcategoria+'" onclick="editar_categoria('+row.idcategoria+')"><i class="far fa-edit"></i></button>\
                            <button type="button" class="btn waves-effect waves-light btn-rounded btn-xs  btn-danger" onclick="eliminar_registro_cate('+row.idcategoria+');"><i class="fas fa-trash-alt"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function eliminar_registro_cate(id){
    $('#id_cate').val(id);
    $('#elimina_registro_modal_cate').modal();
}
function delete_registro_cate(){
    var idp=$('#id_cate').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Servicios/deleteregistro_cate",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#categoria option[value="'+idp+'"]').remove();
            $('#elimina_registro_modal_cate').modal('hide');
            setTimeout(function(){ 
               reload_categoria(); 
            }, 1000); 
        }
    });  
}

function editar_categoria(id){
    edi_cate=1;
    $('#idcategoria_categoria').val($('.cate_'+id).data('idcategoria'));
    $('#nombre_categoria').val($('.cate_'+id).data('nombre'));
}

function reload_registro_servicios(){
    tabla=$("#table_datos").DataTable();
    tabla.destroy();
    table_servicios();
}

function table_servicios(){
    tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Servicios/getlistado_servicios",
            type: "post",
            "data":{servicio:$('#servicio_busqueda').val()},
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"nombre"},
            {"data":"cantidad"},
            {"data":"costo"},
            {"data":"duracion"},
            {"data":"servicio"},
            {"data":"encargado"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idservicio="'+row.idservicio+'"\
                            data-nombre="'+row.nombre+'"\
                            data-descripcion="'+row.descripcion+'"\
                            data-cantidad="'+row.cantidad+'"\
                            data-costo="'+row.costo+'"\
                            data-duracion="'+row.duracion+'"\
                            data-categoria="'+row.categoria+'"\
                            data-encargado="'+row.encargado+'"\
                            class="btn btn-success btn-circle registro_datos_'+row.idservicio+'" onclick="modal_editar('+row.idservicio+')"><i class="far fa-edit"></i></button>\
                            <button type="button" class="btn btn-danger btn-circle" onclick="eliminar_registro('+row.idservicio+');"><i class="fas fa-trash-alt"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function modal_editar(id){
    $('#idservicio').val($('.registro_datos_'+id).data('idservicio'));    
    $('#nombre').val($('.registro_datos_'+id).data('nombre'));
    $('#descripcion').val($('.registro_datos_'+id).data('descripcion'));
    $('#cantidad').val($('.registro_datos_'+id).data('cantidad'));
    $('#costo').val($('.registro_datos_'+id).data('costo'));
    $('#duracion').val($('.registro_datos_'+id).data('duracion'));
    $('#categoria').val($('.registro_datos_'+id).data('categoria'));
    $('#encargado').val($('.registro_datos_'+id).data('encargado'));
    $('#registro_datos').modal();
}
function eliminar_registro(id){ 
    $('#id_servicio').val(id);
    $('#elimina_registro_modal').modal();
}

function delete_registro(){
    var idp=$('#id_servicio').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Servicios/deleteregistro",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_registro_modal').modal('hide');
            setTimeout(function(){ 
                tabla.ajax.reload(); 
            }, 1000); 
        }
    });  
}