var base_url = $('#base_url').val();
var tabla;
var tabla_entrenamiento;
var aux_idempleado=0;
$(document).ready(function() {
	$('#foto_avatar').change(function(){
	     document.getElementById('img_avatar').src = window.URL.createObjectURL(this.files[0]);
         $('#foto_validar').val(0); 
	});
	table();
    $('textarea.js-auto-size').textareaAutoSize();
});

function modal_empleado(){
    document.getElementById('img_avatar').src = base_url+'images/annon.png';
    $('#foto_validar').val('0');
    $('#personalId').val('0');
    $('#nombre').val('');
    $('#fechanacimiento').val('');
    $('#correo').val('');
    $('#celular').val('');
    $('#telefono').val('');
    $('#tel_oficina').val('');
    $('#domicilio').val('');
    $('#observaciones').val('');
    $('#ocupacion').val('');
    $('#sueldo').val('');
    $('#fechaingreso').val('');
    $('#verificar_check').prop('checked',false);
    $('.baja_texto').css('display','none');
    $('#fechabaja').val('');
    $('#motivo').val('');

	$('#registro_empleado').modal();
}
function guarda_empleado(){
    var form_register = $('#form_empleado');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_empleado").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Empleados/registra_empleado',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var id=data;
                var id_f=$('#foto_validar').val();
                if(id_f==0){
                    add_file(id);
                }
                
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                  });
                setTimeout(function(){ 
                    window.location = base_url+'Empleados';
                }, 1500);
            }
        });
    }   
}

function add_file(id){
  //console.log("archivo: "+archivo);
  //console.log("name: "+name);
    var archivo=$('#foto_avatar').val()
	//var name=$('#foto_avatar').val()
	extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
	extensiones_permitidas = new Array(".jpeg",".png",".jpg");
	permitida = false;
    if($('#foto_avatar')[0].files.length > 0) {
	    for (var i = 0; i < extensiones_permitidas.length; i++) {
	       if (extensiones_permitidas[i] == extension) {
	       permitida = true;
	       break;
	       }
	    }  
	    if(permitida==true){
	      //console.log("tamaño permitido");
	        var inputFileImage = document.getElementById('foto_avatar');
	        var file = inputFileImage.files[0];
	        var data = new FormData();
	        data.append('foto',file);
	        data.append('id',id);
	        $.ajax({
	            url:base_url+'Empleados/cargafiles',
	            type:'POST',
	            contentType:false,
	            data:data,
	            processData:false,
	            cache:false,
	            success: function(data) {
	                var array = $.parseJSON(data);
	                    //console.log(array.ok);
	                    /*
	                    if (array.ok=true) {
	                        swal("Éxito", "Guardado Correctamente", "success");
	                    }else{
	                        swal("Error!", "No Se encuentra el archivo", "error");
	                    }
	                    */
	            },
	            error: function(jqXHR, textStatus, errorThrown) {
	                var data = JSON.parse(jqXHR.responseText);
	            }
	        });
	    }
    }        
}

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Empleados/getlistado",
            type: "post",
            "data":{empleado:$('#empleado_busqueda').val()},
            error: function(){
               $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data": null,
                "render": function ( data, type, row, meta ){
                	var html='';        
                	if(row.foto!=''){
                        html='<img style="width: 40px; height: 40px; border-radius: 20px;" src="'+base_url+'uploads/empleados/'+row.foto+'">';  
                	}else{
                        html='<img style="width: 40px; height: 40px; border-radius: 20px;" src="'+base_url+'images/annon.png">';
                	}
                return html;
                }
            },
            {"data":"nombre"},
            {"data":"correo"},
            {"data":"telefono"},
            {"data":"celular"},
            {"data":"tel_oficina"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html=''; 
                    if(row.check_baja=='on'){
                       html+='<span class="label label-danger m-r-10">Baja</span>';    
                    }else{
                       html+='';
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button" data-foto="'+row.foto+'" data-perid="'+row.personalId+'" data-nombre="'+row.nombre+'" data-fechanacimiento="'+row.fechanacimiento+'" data-sexo="'+row.sexo+'" data-correo="'+row.correo+'" data-telefono="'+row.telefono+'" data-celular="'+row.celular+'" data-tel_oficina="'+row.tel_oficina+'" data-domicilio="'+row.domicilio+'" data-estado_civil="'+row.estado_civil+'" data-observaciones="'+row.observaciones+'" data-ocupacion="'+row.ocupacion+'" data-sueldo="'+row.sueldo+'" data-fechaingreso="'+row.fechaingreso+'" data-fechabaja="'+row.fechabaja+'" data-motivo="'+row.motivo+'" data-check_baja="'+row.check_baja+'" class="btn btn-success btn-circle paciente_datos_'+row.personalId+'" onclick="modal_editar('+row.personalId+')"><i class="far fa-edit"></i></button>\
                          <button type="button" class="btn btn-danger btn-circle" onclick="eliminar_empleado('+row.personalId+');"><i class="fas fa-trash-alt"></i> </button>\
                          <button type="button" class="btn btn-secondary btn-circle" onclick="modal_entrenamientos('+row.personalId+');"><i class="fas fa-universal-access"></i></button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function modal_editar(id){
	$('#personalId').val($('.paciente_datos_'+id).data('perid'));
	var foto = $('.paciente_datos_'+id).data('foto');
	if(foto!=''){
	    document.getElementById('img_avatar').src = base_url+'uploads/empleados/'+$('.paciente_datos_'+id).data('foto');
        $('#foto_validar').val(1); 
	}else{
		document.getElementById('img_avatar').src = base_url+'images/annon.png';
        $('#foto_validar').val(0); 
	}    
	//alert($('.paciente_datos_'+id).data('check_baja'));
	$('#foto').val($('.paciente_datos_'+id).data('foto'));
	$('#nombre').val($('.paciente_datos_'+id).data('nombre'));
	$('#fechanacimiento').val($('.paciente_datos_'+id).data('fechanacimiento'));
    var sexo = $('.paciente_datos_'+id).data('sexo')
	if(sexo==1){
        $("#sexo option[value='1']").attr("selected", true);
    }else if(sexo==2){
        $("#sexo option[value='2']").attr("selected", true);
    }
	
    $('#correo').val($('.paciente_datos_'+id).data('correo'));
	$('#telefono').val($('.paciente_datos_'+id).data('telefono'));
	$('#celular').val($('.paciente_datos_'+id).data('celular'));
	$('#tel_oficina').val($('.paciente_datos_'+id).data('tel_oficina'));
    $('#domicilio').val($('.paciente_datos_'+id).data('domicilio'));
	var estado_civil = $('.paciente_datos_'+id).data('estado_civil')
    if(estado_civil==1){
        $("#estado_civil option[value='1']").attr("selected", true);
    }else if(estado_civil==2){
        $("#estado_civil option[value='2']").attr("selected", true);
    }else if(estado_civil==3){
        $("#estado_civil option[value='3']").attr("selected", true);
    }else if(estado_civil==4){
        $("#estado_civil option[value='4']").attr("selected", true);
    }else if(estado_civil==5){
        $("#estado_civil option[value='5']").attr("selected", true);
    }
    $('#observaciones').val($('.paciente_datos_'+id).data('observaciones'));

    $('#sueldo').val($('.paciente_datos_'+id).data('sueldo'));
    $('#ocupacion').val($('.paciente_datos_'+id).data('ocupacion'));
    $('#fechaingreso').val($('.paciente_datos_'+id).data('fechaingreso'));
    var check_baja = $('.paciente_datos_'+id).data('check_baja');
    if(check_baja=='on'){
       $('#verificar_check').prop('checked',true);
       $('.baja_texto').css('display','block');
    }else{
       $('#verificar_check').prop('checked',false);
       $('.baja_texto').css('display','none');
    }
    $('#fechabaja').val($('.paciente_datos_'+id).data('fechabaja'));
    $('#motivo').val($('.paciente_datos_'+id).data('motivo'));

	$('#registro_empleado').modal();
}

function check_baja_btn(){
    if($('#verificar_check').is(':checked')){
        $('.baja_texto').css('display','block');
    }else{
        $('.baja_texto').css('display','none');
    }
}

function eliminar_empleado(id){ 
    $('#id_empleado').val(id);
    $('#elimina_empleado_modal').modal();
}
function delete_empleado(){
    var idp=$('#id_empleado').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Empleados/delete_empleado",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_empleado_modal').modal('hide');
            setTimeout(function(){ 
                tabla.ajax.reload(); 
            }, 1000); 
        }
    });  
}
//===========================================================
function guarda_registro_entrenamiento(){
    var form_register = $('#form_registro_entrenamiento');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_entrenamiento").valid();
    if($valid) {
        var datos = form_register.serialize()+'&personalId='+aux_idempleado;
        $.ajax({
            type:'POST',
            url: base_url+'Empleados/registro_entrenamiento',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                setTimeout(function(){ 
                    reload_entrenamiento(aux_idempleado);
                    $('#identrenamiento').val(0);
                    $('#nombre_entrenamiento').val('');
                }, 1000); 
                
            }
        });
    }   
}
function modal_entrenamientos(id){
    aux_idempleado=id;
    $('#entrenamientos_empleado_modal').modal();
    reload_entrenamiento(id);
}
function reload_entrenamiento(id){
    tabla_entrenamiento=$("#table_entrenamientos").DataTable();
    tabla_entrenamiento.destroy();
    table_entrenamiento(id);
}
function table_entrenamiento(id){
    tabla_entrenamiento=$("#table_entrenamientos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Empleados/getlistado_entrenamiento",
            type: "post",
            "data":{personalId:id},
            error: function(){
               $("#table_entrenamientos").css("display","none");
            }
        },
        "columns": [
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-identrenamiento="'+row.identrenamiento+'"\
                            data-nombre="'+row.nombre+'"\
                            class="btn waves-effect waves-light btn-rounded btn-xs btn-success entrenamiento_'+row.identrenamiento+'" onclick="editar_entrenamiento('+row.identrenamiento+')"><i class="far fa-edit"></i></button>\
                            <button type="button" class="btn waves-effect waves-light btn-rounded btn-xs  btn-danger" onclick="eliminar_registro_entrenamiento('+row.identrenamiento+');"><i class="fas fa-trash-alt"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[4, 10, 25], [4, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function editar_entrenamiento(id){
    $('#identrenamiento').val($('.entrenamiento_'+id).data('identrenamiento'));
    $('#nombre_entrenamiento').val($('.entrenamiento_'+id).data('nombre'));
}

function eliminar_registro_entrenamiento(id){
    $('#elimina_registro_modal_entrenamiento').modal();
    $('#id_entrenamiento').val(id);
}
function delete_registro_entrenamiento(){
    var idp=$('#id_entrenamiento').val();
    $.ajax({
        type:'POST',
        url: base_url+"Empleados/deleteregistro_entrenamiento",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_registro_modal_entrenamiento').modal('hide');
            setTimeout(function(){ 
               reload_entrenamiento(aux_idempleado); 
            }, 1000); 
        }
    });  
}