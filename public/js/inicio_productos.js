var table;
$(document).ready(function() {
	table=$("#table_datos").DataTable({
		//"lengthMenu": [[3, 50, 100], [3, 50, 100]],
		"language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
	});

    table.on( 'search.dt', function () {
        //$('#filterInfo').html( 'Currently applied global search: '+table.search() );
        var num = 0;
        setTimeout(function(){ 
        $('#table_datos tbody tr').each(function () {
            var cant=$(this).find('td').eq(4).text();
            var suma=parseFloat(cant);
            num+=suma;
        });
            $('.total_pro').html(num);
        }, 1000);
    } );
});