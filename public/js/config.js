var base_url=$('#base_url').val();
var tabla_espacio;
var tabla;
var tabla_perfil;
$(document).ready(function($) {
	//GetNtable();
	$('.addhorarionodisponible').click(function(event) {
		$.ajax({
            type:'POST',
            url: base_url+"index.php/Configuracion/hora_nd",
            success: function (data){
            	$('.table-h-nodisponibletbody').html('');
                loadconfiguraciones();
            }
        });
	});
	$('.clockpicker').clockpicker({
        donetext: 'Agregar',
    });
	loadconfiguraciones();
	reload_espacio();

	/// Estilo de color 
	$("#color_trabajo").spectrum({
        showPaletteOnly: true,
        togglePaletteOnly: true
    });
    reload_registro();
    usuario_clean();
    reload_registro_perfil();
    get_text_perfil();
});
function addhorarionodisponible(id){
	var html ='<tr class="rowhnd_'+id+'">\
                    <td>\
                        <input type="hidden"  id="horario_nd_row" value="'+id+'">\
                        <div class="dias_horario_nd_g">\
                            <div class="dias_horario_nd">\
                                <label  >L</label>\
                                <input type="checkbox"  class="horario_nd_1_'+id+'" id="horario_nd_1" onchange="hora_nd_d()">\
                            </div>\
                            <div class="dias_horario_nd">\
                                <label  >M</label>\
                                <input type="checkbox"  class="horario_nd_2_'+id+'" id="horario_nd_2" onchange="hora_nd_d()">\
                            </div>\
                            <div class="dias_horario_nd">\
                                <label  >M</label>\
                                <input type="checkbox"  class="horario_nd_3_'+id+'" id="horario_nd_3" onchange="hora_nd_d()">\
                            </div>\
                            <div class="dias_horario_nd">\
                                <label  >J</label>\
                                <input type="checkbox"  class="horario_nd_4_'+id+'" id="horario_nd_4" onchange="hora_nd_d()">\
                            </div>\
                            <div class="dias_horario_nd">\
                                <label  >V</label>\
                                <input type="checkbox"  class="horario_nd_5_'+id+'" id="horario_nd_5" onchange="hora_nd_d()">\
                            </div>\
                            <div class="dias_horario_nd">\
                                <label  >S</label>\
                                <input type="checkbox"  class="horario_nd_6_'+id+'" id="horario_nd_6" onchange="hora_nd_d()">\
                            </div>\
                            <div class="dias_horario_nd">\
                                <label  >D</label>\
                                <input type="checkbox"  class="horario_nd_7_'+id+'" id="horario_nd_7" onchange="hora_nd_d()">\
                            </div>\
                        </div>\
                    </td>\
                    <td>\
                        <select class="form-control hora_nd_i_'+id+'" id="hora_nd_i" data-horai="'+id+'"  onchange="hora_nd($(this))">\
                            <option >00:00</option>\
                            <option >01:00</option>\
                            <option >02:00</option>\
                            <option >03:00</option>\
                            <option >04:00</option>\
                            <option >05:00</option>\
                            <option >06:00</option>\
                            <option >07:00</option>\
                            <option >08:00</option>\
                            <option >09:00</option>\
                            <option >10:00</option>\
                            <option >11:00</option>\
                            <option >12:00</option>\
                            <option >13:00</option>\
                            <option >14:00</option>\
                            <option >15:00</option>\
                            <option >16:00</option>\
                            <option >17:00</option>\
                            <option >18:00</option>\
                            <option >19:00</option>\
                            <option >20:00</option>\
                            <option >21:00</option>\
                            <option >22:00</option>\
                            <option >23:00</option>\
                        </select>\
                    </td>\
                    <td>\
                        <select class="form-control hora_nd_f_'+id+'" id="hora_nd_f" onchange="hora_nd_d()">\
                            <option >00:00</option>\
                            <option >01:00</option>\
                            <option >02:00</option>\
                            <option >03:00</option>\
                            <option >04:00</option>\
                            <option >05:00</option>\
                            <option >06:00</option>\
                            <option >07:00</option>\
                            <option >08:00</option>\
                            <option >09:00</option>\
                            <option >10:00</option>\
                            <option >11:00</option>\
                            <option >12:00</option>\
                            <option >13:00</option>\
                            <option >14:00</option>\
                            <option >15:00</option>\
                            <option >16:00</option>\
                            <option >17:00</option>\
                            <option >18:00</option>\
                            <option >19:00</option>\
                            <option >20:00</option>\
                            <option >21:00</option>\
                            <option >22:00</option>\
                            <option >23:00</option>\
                        </select>\
                    </td>\
                    <td>\
                        <button type="button" class="btn waves-effect waves-light btn-rounded btn-danger btn-xs" onclick="rowhndremove('+id+')">\
                        <i class="fas fa-minus"></i></button>\
                    </td>\
                </tr>';
	$('.table-h-nodisponibletbody').append(html);
}
function rowhndremove(id){
	if (id==0) {
		$('.rowhnd_'+id).remove();
	}else{
		$.ajax({
	        type:'POST',
	        url: base_url+'Configuracion/eliminar_horario_disponible',
	        data:{
	        	id:id
	        },
	        statusCode:{
	            404: function(data){
	                $.toast({
	                    heading: '¡Error!',
	                    text: 'No Se encuentra el archivo',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            },
	            500: function(){
	                $.toast({
	                    heading: '¡Error!',
	                    text: '500',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            }
	        },
	        success:function(data){
	        	$('.rowhnd_'+id).remove();
	            $.toast({
                    heading: '¡Éxito!',
                    text: 'Borrado correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500
                });
	        }
	    });
	}
}
function loadconfiguraciones(){
	$.ajax({
	        type:'POST',
	        url: base_url+'Configuracion/loadconfiguraciones',
	        statusCode:{
	            404: function(data){
	                $.toast({
	                    heading: '¡Error!',
	                    text: 'No Se encuentra el archivo',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            },
	            500: function(){
	                $.toast({
	                    heading: '¡Error!',
	                    text: '500',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            }
	        },
	        success:function(data){
	            var array = $.parseJSON(data);
	            array.horariol.forEach(function(element) {
	            	if (element.activo==1) {
	            		$('#horario_'+element.id).prop('checked', true);
	            	}else{
	            		$('#horario_'+element.id).prop('checked', false);
	            	}
	            	$('#horario_i_'+element.id).val(element.horainicio);
					$('#horario_f_'+element.id).val(element.horafin);
	            });
	            array.horariond.forEach(function(element) {
	            	addhorarionodisponible(element.id);
	            	if (element.l==1) {
	            		$('.horario_nd_1_'+element.id).prop('checked', true);
	            	}else{
	            		$('.horario_nd_1_'+element.id).prop('checked', false);
	            	}
	            	if (element.m==1) {
						$('.horario_nd_2_'+element.id).prop('checked', true);
	            	}else{
	            		$('.horario_nd_2_'+element.id).prop('checked', false);
	            	}
	            	if (element.mi==1) {
						$('.horario_nd_3_'+element.id).prop('checked', true);
	            	}else{
	            		$('.horario_nd_3_'+element.id).prop('checked', false);
	            	}
	            	if (element.j==1) {
						$('.horario_nd_4_'+element.id).prop('checked', true);
	            	}else{
	            		$('.horario_nd_4_'+element.id).prop('checked', false);
	            	}
	            	if (element.v==1) {
						$('.horario_nd_5_'+element.id).prop('checked', true);
	            	}else{
	            		$('.horario_nd_5_'+element.id).prop('checked', false);
	            	}
	            	if (element.s==1) {
						$('.horario_nd_6_'+element.id).prop('checked', true);
	            	}else{
	            		$('.horario_nd_6_'+element.id).prop('checked', false);
	            	}
	            	if (element.s==1) {
						$('.horario_nd_7_'+element.id).prop('checked', true);
	            	}else{
	            		$('.horario_nd_7_'+element.id).prop('checked', false);
	            	}
	            	
	            	$('.hora_nd_i_'+element.id).val(element.h_inicio);
					$('.hora_nd_f_'+element.id).val(element.h_fin);
	            });
	            if (array.sms==1) {
	            	$('#sms').prop('checked', true);
	            }else{
	            	$('#sms').prop('checked', true);
	            }
	            $('#pag_inicio').val(array.pagina);
	        }
	    });
}
function horario(id){
	setTimeout(function(){ 
		var dia = $('#horario_'+id).is(':checked')==true?1:0;
		var hincial = $('#horario_i_'+id).val();
		var hfinal = $('#horario_f_'+id).val();
		$.ajax({
	        type:'POST',
	        url: base_url+'Configuracion/horariolaboral',
	        data:{
	        	id:id,
	        	activo:dia,
	        	horainicio:hincial,
	        	horafin:hfinal
	        },
	        statusCode:{
	            404: function(data){
	                $.toast({
	                    heading: '¡Error!',
	                    text: 'No Se encuentra el archivo',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            },
	            500: function(){
	                $.toast({
	                    heading: '¡Error!',
	                    text: '500',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            }
	        },
	        success:function(data){
	            $.toast({
	                    heading: '¡Actualizado!',
	                    text: 'Horario actualizado',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'success',
	                    hideAfter: 3500
	                });
	        }
	    });
     }, 1000);
}
function hora_nd(datas){
	//console.log(datas.val());
	var horaini =datas.val();
	var newhora =horaini.split(':');
		newhora=parseInt(newhora[0])+1;
		console.log(newhora);
		if (newhora<10) {
			newhora='0'+newhora
		} 
		newhora=newhora+':00';
	var row =datas.data('horai');
	$('.hora_nd_f_'+row).val(newhora);
	setTimeout(function(){ 
		save_hora_nd();
	}, 1000);
}
function hora_nd_d(){
	setTimeout(function(){ 
		save_hora_nd();
	}, 1000);
}
function save_hora_nd(){
	var DATA  = [];
            var TABLA   = $("#table-h-nodisponible tbody > tr");
            TABLA.each(function(){         
                item = {};
                item ["id"] = 	$(this).find("input[id*='horario_nd_row']").val();
                item ["l"] = 	$(this).find("input[id*='horario_nd_1']").is(':checked')==true?1:0;
                item ["m"]  =	$(this).find("input[id*='horario_nd_2']").is(':checked')==true?1:0;
                item ["mi"] = 	$(this).find("input[id*='horario_nd_3']").is(':checked')==true?1:0;
                item ["j"] = 	$(this).find("input[id*='horario_nd_4']").is(':checked')==true?1:0;
                item ["v"] = 	$(this).find("input[id*='horario_nd_5']").is(':checked')==true?1:0;
                item ["s"]  = 	$(this).find("input[id*='horario_nd_6']").is(':checked')==true?1:0;
                item ["d"] = 	$(this).find("input[id*='horario_nd_7']").is(':checked')==true?1:0;
                item ["h_inicio"] = $(this).find("select[id*='hora_nd_i']").val();
                item ["h_fin"] = $(this).find("select[id*='hora_nd_f']").val();
                DATA.push(item);
            });
            datahorarionds   = JSON.stringify(DATA);
    var data = 'datahorarionds='+datahorarionds;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Configuracion/save_hora_nd",
            data: data,
            success: function (data){
                //console.log(data);
            }
        });
}
function update_dvm(id){
	setTimeout(function(){ 
		var vm = $('#otra_config_'+id).is(':checked')==true?1:0;
		var tiempov = $('#tiempo_'+id).val();
		$.ajax({
	        type:'POST',
	        url: base_url+'Configuracion/visitamedica',
	        data:{
	        	id:id,
	        	status:vm,
	        	tiempo:tiempov
	        },
	        statusCode:{
	            404: function(data){
	                $.toast({
	                    heading: '¡Error!',
	                    text: 'No Se encuentra el archivo',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            },
	            500: function(){
	                $.toast({
	                    heading: '¡Error!',
	                    text: '500',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            }
	        },
	        success:function(data){
	            $.toast({
	                    heading: '¡Actualizado!',
	                    text: 'Horario actualizado',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'success',
	                    hideAfter: 3500
	                });
	        }
	    });
     }, 1000);
}
function configsms(){
	setTimeout(function(){ 
		var vm = $('#sms').is(':checked')==true?1:0;
		$.ajax({
	        type:'POST',
	        url: base_url+'Configuracion/configsms',
	        data:{
	        	status:vm,
	        },
	        statusCode:{
	            404: function(data){
	                $.toast({
	                    heading: '¡Error!',
	                    text: 'No Se encuentra el archivo',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            },
	            500: function(){
	                $.toast({
	                    heading: '¡Error!',
	                    text: '500',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            }
	        },
	        success:function(data){
	            $.toast({
	                    heading: '¡Actualizado!',
	                    text: 'actualizado',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'success',
	                    hideAfter: 3500
	                });
	        }
	    });
     }, 1000);
}
function configinicio(){
	setTimeout(function(){ 
		var pagina = $('#pag_inicio').val();
		$.ajax({
	        type:'POST',
	        url: base_url+'Configuracion/configinicio',
	        data:{
	        	inicio:pagina,
	        },
	        statusCode:{
	            404: function(data){
	                $.toast({
	                    heading: '¡Error!',
	                    text: 'No Se encuentra el archivo',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            },
	            500: function(){
	                $.toast({
	                    heading: '¡Error!',
	                    text: '500',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            }
	        },
	        success:function(data){
	            $.toast({
	                    heading: '¡Actualizado!',
	                    text: 'actualizado',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'success',
	                    hideAfter: 3500
	                });
	        }
	    });
     }, 1000);
}

//===========================================================
function guarda_registro_espacio(){
    var form_register = $('#form_registro_espacio');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_espacio").valid();
    if($valid) {
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Configuracion/registro_espacio',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                $('#idespacio_trabajo').val(0);
                $('#nombre_trabajo').val('');
                setTimeout(function(){ 
                    reload_espacio();
                }, 1000); 
               
        
            }
        });
    }   
}

function reload_espacio(){
    tabla_espacio=$("#table_datos_espacio").DataTable();
    tabla_espacio.destroy();
    table_espacio();
}
function table_espacio(){
	tabla_espacio=$("#table_datos_espacio").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Configuracion/getlistado_espacio",
            type: "post",
            error: function(){
               $("#table_datos_espacio").css("display","none");
            }
        },
        "columns": [
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<span class="label m-r-10" style="background-color:'+row.color+' ;color:'+row.color+'">___</span>';
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idespacio="'+row.idespacio+'"\
                            data-nombre="'+row.nombre+'"\
                            data-color="'+row.color+'"\
                            class="btn waves-effect waves-light btn-rounded btn-xs btn-success espacio_'+row.idespacio+'" onclick="editar_espacio('+row.idespacio+')"><i class="far fa-edit"></i></button>\
                            <button type="button" class="btn waves-effect waves-light btn-rounded btn-xs  btn-danger" onclick="eliminar_registro_espacio('+row.idespacio+');"><i class="fas fa-trash-alt"></i> </button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

$("#showdttable").click(function(event) {
	GetNtable();
});

function GetNtable(){
	tabla_grupos=$("#GruposAlimenticios").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        destroy: true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Configuracion/getlistado_gupos",
            type: "post",
            error: function(){
               $("#GruposAlimenticios").css("display","none");
            }
        },
        "columns": [
            {"data":"grupo_alimento"},
            {"data":"subgrupo"},
            {"data":"energia",
            	"render": function ( data, type, row, meta ){
                	var html='<input value="'+row.energia+'" id="energ-'+row.ids+'_energia" onblur="SetData(this)">';
                	return html;
                }
        	},
            {"data":"proteina",
            	"render": function ( data, type, row, meta ){
                	var html='<input value="'+row.proteina+'" id="protei-'+row.ids+'_proteina" onblur="SetData(this)">';
                	return html;
                }
            },
            {"data":"lipidos",
            	"render": function ( data, type, row, meta ){
                	var html='<input value="'+row.lipidos+'" id="lipid-'+row.ids+'_lipidos" onblur="SetData(this)">';
                	return html;
                }
            },
            {"data":"carbohidratos",
            	"render": function ( data, type, row, meta ){
                	var html='<input value="'+row.carbohidratos+'" id="carbohidrat-'+row.ids+'_carbohidratos" onblur="SetData(this)">';
                	return html;
                }
            },
            {"data":"sodio",
            	"render": function ( data, type, row, meta ){
                	var html='<input value="'+row.sodio+'" id="sod-'+row.ids+'_sodio" onblur="SetData(this)">';
                	return html;
                }
            },
            {"data":"fibra",
            	"render": function ( data, type, row, meta ){
                	var html='<input value="'+row.fibra+'" id="fib-'+row.ids+'_fibra" onblur="SetData(this)">';
                	return html;
                }
            },
            {"data":"conteoHc",
            	"render": function ( data, type, row, meta ){
                	var html='<input value="'+row.conteoHc+'" id="conteo-'+row.ids+'_conteoHc" onblur="SetData('+this+')">';
                	return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function SetData(T){
	valor=T.value;
	iden=T.id;
	seprarate=iden.split("-");
	data=seprarate[1].split("_");
	idp=data[0];
	campo=data[1];
	$.ajax({
        type:'POST',
        url: base_url+"Configuracion/SetDataGrupo",
        data:{
        	'id':idp,
        	'campo':campo,
        	'valor': valor,
        },
        success:function(data){
            /*$.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });*/
        }
    });
}

function editar_espacio(id){
    $('#idespacio_trabajo').val($('.espacio_'+id).data('idespacio'));
    $('#nombre_trabajo').val($('.espacio_'+id).data('nombre'));
    //$('#color_trabajo').val($('.espacio_'+id).data('color'));
    $('.div_color').html('<div class="col-md-4">\
                            <div class="form-group">\
                                <input type="color" name="color" id="color_trabajo" value="'+$('.espacio_'+id).data('color')+'" class="form-control">\
                            </div>\
                        </div>');
    /// Estilo de color 
	$("#color_trabajo").spectrum({
        showPaletteOnly: true,
        togglePaletteOnly: true
    });
}

function eliminar_registro_espacio(id){
    $('#id_espacio').val(id);
    $('#elimina_registro_modal_espacio').modal();
}
function delete_registro_espacio(){
    var idp=$('#id_espacio').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Configuracion/deleteregistro_espacio",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_registro_modal_espacio').modal('hide');
            setTimeout(function(){ 
               reload_espacio(); 
            }, 1000); 
        }
    });  
}

function reload_registro(){
	tabla=$("#table_datos_usurios").DataTable();
    tabla.destroy();
    table_load();
}
function table_load(){
	tabla=$("#table_datos_usurios").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Configuracion/getlistado",
            type: "post",
            error: function(){
               $("#table_datos_usurios").css("display","none");
            }
        },
        "columns": [
            {"data": null,
                "render": function ( data, type, row, meta ){
                	var html='';        
                	if(row.foto!=''){
                        html='<img style="width: 40px; height: 40px; border-radius: 20px;" src="'+base_url+'uploads/empleados/'+row.foto+'">';  
                	}else{
                        html='<img style="width: 40px; height: 40px; border-radius: 20px;" src="'+base_url+'images/annon.png">';
                	}
                return html;
                }
            },
            {"data":"personal"},
            {"data":"Usuario"},
            {"data":"perfil"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button" class="btn btn-danger btn-circle" onclick="eliminar_usuario('+row.UsuarioID+');"><i class="fas fa-trash-alt"></i> </button>\
                          <button type="button" class="btn btn-success btn-circle edit_usu_'+row.UsuarioID+'"\
                            data-Usuario="'+row.Usuario+'"\
                            data-personalid="'+row.personalId+'"\
                            data-perfilid="'+row.perfilId+'"\
                             onclick="editar_usuario('+row.UsuarioID+');"><i class="far fa-edit"></i></button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function editar_usuario(id){
	$('#UsuarioID').val(id);
	$('#usuario').val($('.edit_usu_'+id).data('usuario'));
	$('#contrasena').val('xxxxxxxxxx.');
	$('#contrasena2').val('xxxxxxxxxx.');
	$("#personalId option[value='"+$('.edit_usu_'+id).data('personalid')+"']").attr("selected", true);
    $("#perfilId option[value='"+$('.edit_usu_'+id).data('perfilid')+"']").attr("selected", true);
    $('.txt_edit').html('Edición de ');
    $('.btn_cancelar_usuario').html('<button type="button" class="btn waves-effect waves-light btn-rounded" style="color: red;border-color: red;" onclick="cancelar_usuario()">Cancelar</button>');
}
function usuario_clean(){
	$('#UsuarioID').val(0);
	$('#usuario').val('');
	$('#contrasena').val('');
	$('#contrasena2').val('');
	$('.txt_usuario').html('');
	setTimeout(function(){ 
	$('.txt_usu').html('<div class="row">\
                          <div class="col-md-12">\
                              <div class="form-group">\
                                  <label>Nombre de usuario <div style="color: red" class="txt_usuario"></div></label>\
                                  <input type="text" name="usuario" id="usuario" oninput="verificar_usuario()" class="form-control">\
                              </div>\
                          </div>\
                      </div>');
    }, 1000); 
}
function verificar_usuario(){
	$.ajax({
        type: 'POST',
        url: base_url + 'Configuracion/validar',
        data: {
            Usuario:$('#usuario').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function(data) {
            if (data == 1) {
            	$('.txt_usuario').html('El usuario ya existe');
                $.toast({
                    heading: '¡Atención!',
                    text: 'El usuario ya existe',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });

            }else{
            	$('.txt_usuario').html('');
            }
        }
    });
}
function guardar_usuario(){
	var form_register = $('#form_usuario');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            usuario:{
              required: true
            },
            contrasena:{
              required: true
            },
            contrasena2: {
                equalTo: contrasena,
                required: true
            },
            personalId: {
                required: true
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_usuario").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Configuracion/add_usuarios',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                $('#UsuarioID').val('');
                $('#usuario').val('');
                $('#contrasena').val('');
                $('#contrasena2').val('');
                $('.txt_usuario').html('');
                $('.btn_cancelar_usuario').html('');
                reload_registro();
                $('.vd_red').remove();
            }
        });
    }   
}
function cancelar_usuario(){
	$('#UsuarioID').val(0);
	$('#usuario').val('');
	$('#contrasena').val('');
	$('#contrasena2').val('');
	$("#personalId option[value='0']").attr("selected", true);
    $("#perfilId option[value='0']").attr("selected", true);
    $('.txt_edit').html('');
    $('.btn_cancelar_usuario').html('');
}
function eliminar_usuario(id){ 
    $('#id_usuario').val(id);
    $('#elimina_usuario_modal').modal();
}
function delete_usuario(){
    var idp=$('#id_usuario').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Configuracion/deleteusuario",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_usuario_modal').modal('hide');
            setTimeout(function(){ 
                tabla.ajax.reload(); 
            }, 1000); 
        }
    });  
}
function verificar_perfil(){
	$.ajax({
        type: 'POST',
        url: base_url + 'Configuracion/validar_perfil',
        data: {
            perfil:$('#perfil_aux').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function(data) {
            if (data == 1) {
            	$('.txt_perfil_aux').html('El perfil ya existe');
                $.toast({
                    heading: '¡Atención!',
                    text: 'El perfil ya existe',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });

            }else{
            	$('.txt_perfil_aux').html('');
            }
        }
    });
}
///
function reload_registro_perfil(){
	tabla_perfil=$("#table_datos_perfiles").DataTable();
    tabla_perfil.destroy();
    table_load_perfil();
}
function table_load_perfil(){
	tabla_perfil=$("#table_datos_perfiles").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        "ajax": {
            "url": base_url+"Configuracion/getlistado_perfil",
            type: "post",
            error: function(){
               $("#table_datos_perfiles").css("display","none");
            }
        },
        "columns": [
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button" class="btn btn-danger btn-circle" onclick="eliminar_perfil('+row.perfilId+');"><i class="fas fa-trash-alt"></i> </button>\
                          <button type="button" class="btn btn-success btn-circle edit_per_'+row.perfilId+'"\
                            data-perfil="'+row.nombre+'"\
                             onclick="editar_perfil('+row.perfilId+');"><i class="far fa-edit"></i></button>';
                    html+=' <button type="button" class="btn btn-info btn-circle"\
                             onclick="detalles_permisos('+row.perfilId+');"><i class="fas fa-lock"></i></button>';       
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function eliminar_perfil(id){ 
    $('#idperfile').val(id);
    $('#elimina_perfil_modal').modal();
}
function delete_perfil(){
    var idp=$('#idperfile').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Configuracion/deleteperfil",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_perfil_modal').modal('hide');
            setTimeout(function(){ 
                tabla_perfil.ajax.reload(); 
            }, 1000); 
        }
    });  
}
function guardar_perfil(){
	var form_register = $('#form_registro_perfil');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_perfil").valid();
    if($valid) {
        $('.btn_perfil_alta').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Configuracion/add_perfil',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                $('#perfilId_aux').val(0);
                $('#perfil_aux').val('');
                tabla_perfil.ajax.reload(); 
                $('.btn_perfil_alta').attr('disabled',false);
                get_text_perfil();
            }
        });
    }   
}
function editar_perfil(id){
	$('#perfilId_aux').val(id);
	$('#perfil_aux').val($('.edit_per_'+id).data('perfil'));
	$('.txt_edit_perfil').html('Edición de ');
    $('.btn_cancelar_perfil').html('<button type="button" class="btn waves-effect waves-light btn-rounded" style="color: red;border-color: red;" onclick="cancelar_perfil()">Cancelar</button>');
}
function cancelar_perfil(){
	$('#perfilId_aux').val(0);
	$('#perfil_aux').val('');
	$('.txt_edit_perfil').html('');
    $('.btn_cancelar_perfil').html('');
}
function detalles_permisos(id){
	$('.txt_perfil_tabla').html('');
    $('.permisos_acceso').html('');  
	$.ajax({
        type:'POST',
        url: base_url+"Configuracion/get_permisos_perfil",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
        	$('.txt_perfil_tabla').html('<h4>'+$('.edit_per_'+id).data('perfil')+'</h4>');
            $('.permisos_acceso').html(data);  
        }
    });  
}
function guardar_permiso_menu(id){
	var id_menu=$('#menuid option:selected').val();
	if(id_menu!=0){
		$('.btn_perfil_detalle').attr('disabled',true);
		$.ajax({
	        type:'POST',
	        url: base_url+'Configuracion/add_perfil_detalle',
	        data:{
	        	perfilId:id,
	            MenusubId:id_menu},
	        statusCode:{
	            404: function(data){
	                $.toast({
	                    heading: '¡Error!',
	                    text: 'No Se encuentra el archivo',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            },
	            500: function(){
	                $.toast({
	                    heading: '¡Error!',
	                    text: '500',
	                    position: 'top-right',
	                    loaderBg:'#ff6849',
	                    icon: 'error',
	                    hideAfter: 3500
	                });
	            }
	        },
	        success:function(data){
	            $.toast({
	                heading: 'Éxito',
	                text: 'Guardado Correctamente',
	                position: 'top-right',
	                loaderBg:'#ff6849',
	                icon: 'success',
	                hideAfter: 3500, 
	                stack: 6
	            });
	            $('.btn_perfil_detalle').attr('disabled',false);
                detalles_permisos(id);
	        }
	    });
    }else{
		$.toast({
            heading: '¡Atención!',
            text: 'Debes de selecionar un menú',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
	}
}
function elimnar_permiso_menu(id,id_aux){ 
    $('#idperfile_detalle').val(id);
    $('#idperfile_e').val(id_aux);
    $('#elimina_perfil_detalle_modal').modal();
}
function delete_detalle_perfil(){
    var idp=$('#idperfile_detalle').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Configuracion/deleteperfil_detalles",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_perfil_detalle_modal').modal('hide');
            setTimeout(function(){ 
            	var idperfile_e=$('#idperfile_e').val();
                detalles_permisos(idperfile_e); 
            }, 1000); 
        }
    });  
}

function guardar_permiso_consulta(id){
	var form_register = $('#form_permiso_consultas');
	var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Configuracion/add_permisos_consultas',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                detalles_permisos(id); 
            }
        });
}
function get_text_perfil(){
	$.ajax({
        type:'POST',
        url: base_url+'Configuracion/modal_texto_perfil',
        //data: datos,
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
           $('.text_perfil').html(data);  
        }
    });
}
function guardar_config_general(id){
	var form_register = $('#form_config_general');
	var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Configuracion/add_config_general',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                detalles_permisos(id); 
            }
        });
}