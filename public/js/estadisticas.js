var base_url = $('#base_url').val();
$(document).ready(function(){
    //loadProds();
    $("#tipo_reporte").on("change", function(){
        if(this.value==0){
            $("#cont_p").hide();
            $("#cont_me").hide();
            $("#cont_s").hide();
            $("#cont_n").hide();
        }

        else if(this.value==1){
            loadProds();
            totalConsult("");
            $("#cont_p").show();
            $("#cont_me").hide();
            $("#cont_s").hide();
            $("#cont_n").hide();
            /*tableN.destroy();
            tableS.destroy();
            tableME.destroy();*/
        }
        else if(this.value==2){
            loadNutri();
            totalConsultNut("");
            $("#cont_n").show();
            $("#cont_p").hide();
            $("#cont_s").hide();
            $("#cont_me").hide();
            /*tableP.destroy();
            tableS.destroy();
            tableME.destroy();*/
        }
        else if(this.value==3){
            loadSpa();
            totalConsultSpa("");
            $("#cont_s").show();
            $("#cont_p").hide();
            $("#cont_n").hide();
            $("#cont_me").hide();
            /*tableP.destroy();
            tableN.destroy();
            tableME.destroy();*/
        }
        else if(this.value==4){
            loadME();
            totalConsultME("");
            $("#cont_me").show();
            $("#cont_p").hide();
            $("#cont_s").hide();
            $("#cont_n").hide();
            /*tableP.destroy();
            tableN.destroy();
            tableS.destroy();*/
        }
    });
    $("#fechai").on("change", function(){
        if($("#tipo_reporte").val()==1){
            loadProds();
            totalConsult("");
        }
        else if($("#tipo_reporte").val()==2){
            loadNutri();
            totalConsultNut("");
        }
        else if($("#tipo_reporte").val()==3){
            loadSpa();
            totalConsultSpa("");
        }
        else if($("#tipo_reporte").val()==4){
            loadME();
            totalConsultME("");
        }
    });
    $("#fechaf").on("change", function(){
        if($("#tipo_reporte").val()==1){
            loadProds();
            totalConsult("");
        }
        else if($("#tipo_reporte").val()==2){
            loadNutri();
            totalConsultNut("");
        }
        else if($("#tipo_reporte").val()==3){
            loadSpa();
            totalConsultSpa("");
        }
        else if($("#tipo_reporte").val()==4){
            loadME();
            totalConsultME("");
        }
    });


});

function loadProds(){
    tableP = $('#table_prods').DataTable({
      destroy:true,
      orderCellsTop: true,
      fixedHeader: true,
      "ajax": {
         "url": base_url+"Estadisticas/datatable_prods",
         type: "post",
         data: { fechai: $("#fechai").val(), fechaf: $("#fechaf").val() },
      },
      "columns": [
          {"data": "producto"},
          {"data": "tipo"},
          {"data": "marca"},
          {"data": "lote"},
          {"data": "consultafecha"},
          {"data": "personal"},
          /*{"data": null, 
            render: function(data,type,row){ //usuario
              var html="";         
              return html;
            }, 
          },*/
          {"data": "total", type: 'formatted-num',
            "render": function ( url, type, full) {
                var rw=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(full['total']);
                return rw;
            } 
          },
        ],
       "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 6, {filter:'applied'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 6, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 6 ).footer() ).html(
                 '$'+ total
            );
        },
        dom: 'Blfrtip',
        buttons: [
            {extend: 'excel'},
            {extend: 'pdfHtml5'},
            'print',
        ],
    });      

    const $dt = tableP;
    $dt.on('search.dt', function(e) {
        //console.log('Texto buscado: ' + $dt.search());
        totalConsult($dt.search());
    });
}

function totalConsult(text){
    $.ajax({
        url:base_url+'Estadisticas/datatable_tot_consult',
        type:'POST',
        data: { fechai: $("#fechai").val(), fechaf: $("#fechaf").val(), texto: text },
        success: function(data) {
          var data = JSON.parse(data);
          var totalT=0;
          $.each(data, function (key, dato){
            //console.log("tota: "+dato.totConsul);
            totalT = totalT + parseFloat(dato.totConsul);
          });
          $("#tot_cons").html(totalT);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          var data = JSON.parse(jqXHR.responseText);
        }
    }); 
}

function loadSpa(){
    tableS = $('#table_spa').DataTable({
      destroy:true,
      "ajax": {
         "url": base_url+"Estadisticas/datatable_spa",
         type: "post",
         data: { fechai: $("#fechai").val(), fechaf: $("#fechaf").val() },
      },
      "columns": [
          {"data": "consultafecha"},
          {"data": "paciente"},
          {"data": "servicio"},
          {"data": "personal"},
          {"data": "costo", 
            "render": function ( url, type, full) {
                var rw=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(full['costo']);
                return rw;

            } 
          },
        ],
        dom: 'Blfrtip',
        buttons: [
            {extend: 'excel'},
            {extend: 'pdfHtml5'},
            'print',
        ],
       "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 4, {filter:'applied'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 4 ).footer() ).html(
                 '$'+ total
            );
        },
    });   

    const $dts = tableS;
    $dts.on('search.dt', function(e) {
        //console.log('Texto buscado: ' + $dt.search());
        totalConsultSpa($dts.search());
    });    
}

function loadNutri(){
    tableN = $('#table_nutri').DataTable({
      destroy:true,
      "ajax": {
         "url": base_url+"Estadisticas/datatable_nutri",
         type: "post",
         data: { fechai: $("#fechai").val(), fechaf: $("#fechaf").val() },
      },
      "columns": [
          {"data": "consultafecha"},
          {"data": "paciente"},
          {"data": "servicio"},
          {"data": "personal"},
          {"data": "costo", 
            "render": function ( url, type, full) {
                var rw=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(full['costo']);
                return rw;
            } 
          },
       ],
       dom: 'Blfrtip',
        buttons: [
            {extend: 'excel'},
            {extend: 'pdfHtml5'},
            'print',
        ],
       "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 4, {filter:'applied'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 4 ).footer() ).html(
                 '$'+ total
            );
        },
    });   
    const $dtn = tableN;
    $dtn.on('search.dt', function(e) {
        //console.log('Texto buscado: ' + $dt.search());
        totalConsultNut($dtn.search());
    });    
}

function loadME(){
    tableME = $('#table_med').DataTable({
      destroy:true,
      "ajax": {
         "url": base_url+"Estadisticas/datatable_med",
         type: "post",
         data: { fechai: $("#fechai").val(), fechaf: $("#fechaf").val() },
      },
      "columns": [
          {"data": "consultafecha"},
          {"data": "paciente"},
          {"data": "servicio"},
          {"data": "personal"},
          {"data": "costo", 
            "render": function ( url, type, full) {
                var rw=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(full['costo']);
                return rw;
            } 
          },
       ],
       dom: 'Blfrtip',
        buttons: [
            {extend: 'excel'},
            {extend: 'pdfHtml5'},
            'print',
        ],
       "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 4, {filter:'applied'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 4 ).footer() ).html(
                 '$'+ total
            );
        },
    });       
    const $dtme = tableME;
    $dtme.on('search.dt', function(e) {
        //console.log('Texto buscado: ' + $dt.search());
        totalConsultME($dtme.search());
    }); 
}

function totalConsultSpa(text){
    $.ajax({
        url:base_url+'Estadisticas/datatable_tot_consultSpa',
        type:'POST',
        data: { fechai: $("#fechai").val(), fechaf: $("#fechaf").val(), texto: text },
        success: function(data) {
          //console.log("data: "+data);
          var array = $.parseJSON(data)
          var totals=0;
          totals = array[0].totConsul;
          //console.log("total cons spa: "+array[0].totConsul);
          $("#tot_conspa").html(totals);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          var data = JSON.parse(jqXHR.responseText);
        }
    }); 
}

function totalConsultNut(text){
    $.ajax({
        url:base_url+'Estadisticas/datatable_tot_consultNut',
        type:'POST',
        data: { fechai: $("#fechai").val(), fechaf: $("#fechaf").val(), texto: text },
        success: function(data) {
          //console.log("data: "+data);
          var array = $.parseJSON(data)
          var totals=0;
          totals = array[0].totConsul;
          //console.log("total cons spa: "+array[0].totConsul);
          $("#tot_consn").html(totals);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          var data = JSON.parse(jqXHR.responseText);
        }
    }); 
}

function totalConsultME(text){
    $.ajax({
        url:base_url+'Estadisticas/datatable_tot_consultME',
        type:'POST',
        data: { fechai: $("#fechai").val(), fechaf: $("#fechaf").val(), texto: text },
        success: function(data) {
          //console.log("data: "+data);
          var array = $.parseJSON(data)
          var totals=0;
          totals = array[0].totConsul;
          //console.log("total cons spa: "+array[0].totConsul);
          $("#tot_consnme").html(totals);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          var data = JSON.parse(jqXHR.responseText);
        }
    }); 
}



var consulta = echarts.init(document.getElementById('bar-chart'));
option = {
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['Semana Actual','Semana Anterior']
    },
    toolbox: {
        show : true,
        feature : {
            
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    color: ["#55ce63", "#009efb"],
    calculable : true,
    xAxis : [
        {
            type : 'category',
            data : ['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo']
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        {
            name:'Semana Actual',
            type:'bar',
            data:[2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3],
            markPoint : {
                data : [
                    {type : 'max', name: 'Max'},
                    {type : 'min', name: 'Min'}
                ]
            },
            markLine : {
                data : [
                    {type : 'average', name: 'Average'}
                ]
            }
        },
        {
            name:'Semana Anterior',
            type:'bar',
            data:[2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3],
            markPoint : {
                data : [
                    {name : 'The highest year', value : 182.2, xAxis: 7, yAxis: 183, symbolSize:18},
                    {name : 'Year minimum', value : 2.3, xAxis: 11, yAxis: 3}
                ]
            },
            markLine : {
                data : [
                    {type : 'average', name : 'Average'}
                ]
            }
        }
    ]
};

consulta.setOption(option, true), $(function() {
    function resize() {
        setTimeout(function() {
            consulta.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});


var consultam = echarts.init(document.getElementById('bar-chartm'));
optionm = {
    tooltip : {
        trigger: 'axis'
    },
    toolbox: {
        show : true,
        feature : {
            
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    color: ["#55ce63", "#009efb"],
    calculable : true,
    xAxis : [
        {
            type : 'category',
            data : ['18/07/2020','18/07/2020','18/07/2020','18/07/2020','18/07/2020','18/07/2020','18/07/2020']
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        {
            name:'Semana Actual',
            type:'line',
            data:[2.0, 4.9, 14.0, 233.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3],
            markPoint : {
                data : [
                    {type : 'max', name: 'Max'},
                    {type : 'min', name: 'Min'}
                ]
            },
            markLine : {
                data : [
                    {type : 'average', name: 'Average'}
                ]
            }
        },
    ]
};

consultam.setOption(optionm, true), $(function() {
    function resize() {
        setTimeout(function() {
            consultam.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

var consultamn = echarts.init(document.getElementById('bar-chartmn'));
optionmn = {
    tooltip : {
        trigger: 'axis'
    },
    toolbox: {
        show : true,
        feature : {
            
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    color: ["#55ce63", "#009efb"],
    calculable : true,
    xAxis : [
        {
            type : 'category',
            data : ['07/2020','07/2020','07/2020','07/2020','07/2020','07/2020','07/2020']
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        {
            name:'Semana Actual',
            type:'line',
            data:[2.0, 4.9, 14.0, 45.2, 25.6, 76.7, 233.6, 162.2, 32.6, 20.0, 6.4, 3.3],
            markPoint : {
                data : [
                    {type : 'max', name: 'Max'},
                    {type : 'min', name: 'Min'}
                ]
            },
            markLine : {
                data : [
                    {type : 'average', name: 'Average'}
                ]
            }
        },
    ]
};

consultamn.setOption(optionmn, true), $(function() {
    function resize() {
        setTimeout(function() {
            consultamn.resize()
        }, 100)
    }
    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});