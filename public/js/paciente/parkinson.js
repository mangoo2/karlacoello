var receta_aux=0;
var estudio_aux=0;
var intografia_aux=0;
var medico_aux=0;
var base_url = $('#base_url').val();
$(document).ready(function() {
	$('#arch_medico').change(function(){
        $('#archivo_medico_modal').modal();
	    document.getElementById('img_avatar2').src = window.URL.createObjectURL(this.files[0]);
	});
	$('#arch_medico2').change(function(){
	    document.getElementById('img_avatar2').src = window.URL.createObjectURL(this.files[0]);
	});
});
function receta(){
	if(receta_aux==0){
        receta_aux=1;
        $('.text_receta').css('display','none');
	}else{
		$('.text_receta').css('display','block');

		receta_aux=0;
	}
}
function estudio(){
	if(estudio_aux==0){
        estudio_aux=1;
        $('.text_estudio').css('display','block');
	}else{
		$('.text_estudio').css('display','none');
		estudio_aux=0;
	}
}
function intografia(){
	if(intografia_aux==0){
        intografia_aux=1;
        $('.text_intografia').css('display','block');
	}else{
		$('.text_intografia').css('display','none');
		intografia_aux=0;
	}
}
function medico(){
	if(medico_aux==0){
        medico_aux=1;
        $('.text_medico').css('display','block');
	}else{
		$('.text_medico').css('display','none');
		medico_aux=0;
	}
}
function ver_pef(id){
	window.open(base_url+'Parkinson/pdfestudiolaboratorio/'+id, '_blank');
}

function modal_confirmar(id){
    $('#confimar_cita_modal').modal();
    $('#idconfirmar_cita').val(id);
}
function confirmar_cita(){
    var id=$('#idconfirmar_cita').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Parkinson/cita_confirmada",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Cita confirmada Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            
            setTimeout(function(){ 
                location.reload();
            }, 1500);
        }
    });  
}
function modal_cancelar(id){
    $('#cancelar_cita_modal').modal();
    $('#idcancelar_cita').val(id);
}

function cancelar_cita(){
    var id=$('#idcancelar_cita').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Parkinson/cancelar_cita",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Cancelada Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            
            setTimeout(function(){ 
                location.reload();
            }, 1500);
        }
    });  
}

function imprimir_pdf_hospitalizacion(id){ 
    window.open(base_url+'Parkinson/pdfhospitalizacion/'+id,'_blank');  
}
function imprimir_pdf_interconsulta(id){
    window.open(base_url+'Pacientes/pdfinterconsulta/'+id,'_blank');  
}