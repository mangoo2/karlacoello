var base_url=$('#base_url').val();
$(document).ready(function() {
     nutri_imc(0);
     calcularimc();
     calcular_dieta();
});    


function nutri_imc(imc){
    /*
    SI(C8<18.5,"BAJO PESO",
    SI(C8<24.9,"PESO NORMAL",
    SI(C8<29.9,"SOBREPESO",
    SI(C8<34.5,"OBESIDAD GRADO 1",
    SI(C8<39.9,"OBESIDAD GRADO 2",
    SI(C8>=40,"OBESIDAD GRADO 3"))))))
    */
    var sexo_text=$('#sexo_text').val();
    var html='';
    /// hombre
    var img1='1.jpg';
    var img1g='gris2.png';
    var img1h='';
    var h1='';
    if(imc>0 && imc<18.5){ // SI(C8<18.5,"BAJO PESO",
        img1h=img1;
        h1='#93b4d7';
    }else{
        img1h=img1g;
        h1='#aaaaaa';
    }
    var img2='2.jpg';
    var img2g='gris8.png';
    var img2h='';
    var h2='';
    if(imc>18.5 && imc<24.9){ // SI(C8<24.9,"PESO NORMAL",
        img2h=img2;
        h2='#8fc59f';
    }else{
        img2h=img2g;
        h2='#aaaaaa';
    }
    var img3='3.jpg';
    var img3g='gris3.png';
    var img3h='';
    var h3='';
    if(imc>24.9 && imc<29.9){ // SI(C8<29.9,"SOBREPESO",
        img3h=img3;
        h3='#f9d648';
    }else{
        img3h=img3g;
        h3='#aaaaaa';
    }
    var img4='4.jpg';
    var img4g='gris9.png';
    var img4h='';
    var h4='';
    if(imc>29.9 && imc<34.5){ // SI(C8<34.5,"OBESIDAD GRADO 1",
        img4h=img4;
        h4='#e4985e';
    }else{
        img4h=img4g;
        h4='#aaaaaa';
    }
    var img5='5.jpg';
    var img5g='gris7.png';
    var img5h='';
    var h5='';
    if(imc>34.5 && imc<39.9){ // SI(C8<39.9,"OBESIDAD GRADO 2",
        img5h=img5;
        h5='#d55c5b';
    }else{
        img5h=img5g;
        h5='#aaaaaa';
    }
    var img6='5.jpg';
    var img6g='gris7.png';
    var img6h='';
    var h6='';
    if(imc>=40){ // SI(C8>=40,"OBESIDAD GRADO 3"
        img6h=img6;
        h6='#d55c5b';
    }else{
        img6h=img6g;
        h6='#aaaaaa';
    }
    //// Mujer
    var img1m='1m.jpg';
    var img1mg='gris10.png';
    var img1f='';
    var m1='';
    if(imc>0 && imc<18.5){ // SI(C8<18.5,"BAJO PESO",
        img1f=img1m;
        m1='#93b4d7';
    }else{
        img1f=img1mg;
        m1='#aaaaaa';
    }
    var img2m='2m.jpg';
    var img2mg='gris5.png';
    var img2f='';
    var m2='';
    if(imc>18.5 && imc<24.9){ // SI(C8<24.9,"PESO NORMAL",
        img2f=img2m;
        m2='#8fc59f';
    }else{
        img2f=img2mg;
        m2='#aaaaaa';
    }
    var img3m='3m.jpg';
    var img3mg='gris6.png';
    var img3f='';
    var m3='';
    if(imc>24.9 && imc<29.9){ // SI(C8<29.9,"SOBREPESO",
        img3f=img3m;
        m3='#f9d648';
    }else{
        img3f=img3mg;
        m3='#aaaaaa';
    }
    var img4m='4m.jpg';
    var img4mg='gris4.png';
    var img4f='';
    var m4='';
    if(imc>29.9 && imc<34.5){ // SI(C8<34.5,"OBESIDAD GRADO 1",
        img4f=img4m;
        m4='#e4985e';
    }else{
        img4f=img4mg;
        m4='#aaaaaa';
    }
    var img5m='5m.jpg';
    var img5mg='gris1.png';
    var img5f='';
    var m5='';
    if(imc>34.5 && imc<39.9){ // SI(C8<39.9,"OBESIDAD GRADO 2",
        img5f=img5m;
        m5='#d55c5b';
    }else{
        img5f=img5mg;
        m5='#aaaaaa';
    }
    var img6m='5m.jpg';
    var img6mg='gris1.png';
    var img6f='';
    var m6='';
    if(imc>=40){ // C8>=40,"OBESIDAD GRADO 3"
        img6f=img6m;
        m6='#d55c5b';
    }else{
        img6f=img6mg;
        m6='#aaaaaa';
    }
    if(sexo_text==1){
        html='<div class="row">\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img1h+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+h1+';color: #fff;">Bajo peso</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img2h+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+h2+';color: #fff;">Peso normal</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img3h+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+h3+';color: #fff;">Sobrepeso</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img4h+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+h4+';color: #fff;">Obesidad grado 1</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img5h+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+h5+';color: #fff;">Obesidad grado 2</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img6h+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+h6+';color: #fff;">Obesidad grado 3</span> \
                    </div>\
                </div>\
            </div>     \
            <div class="row">\
                <div class="col-md-4"></div>\
                <div class="col-md-4"><br><br><br>\
                    <div class="card">\
                        <div class="box bg-megna text-center">\
                            <h1 class="font-light text-white"><span class="peso_idealh_txt">0</span></h1>\
                            <h6 class="text-white">Peso ideal</h6>\
                        </div>\
                    </div>\
                </div>\
        </div>';
    }else{
        html='<div class="row">\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img1f+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+m1+';color: #fff;">Bajo peso</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img2f+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+m2+';color: #fff;">Peso normal</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img3f+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+m3+';color: #fff;">Sobrepeso</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img4f+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+m4+';color: #fff;">Obesidad grado 1</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img5f+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+m5+';color: #fff;">Obesidad grado 2</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img6f+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+m6+';color: #fff;">Obesidad grado 3</span> \
                    </div>\
                </div>\
            </div>     \
            <div class="row">\
                <div class="col-md-4"></div>\
                <div class="col-md-4"><br><br><br>\
                    <div class="card">\
                        <div class="box bg-megna text-center">\
                            <h1 class="font-light text-white"><span class="peso_idealm_txt">0</span></h1>\
                            <h6 class="text-white">Peso ideal</h6>\
                        </div>\
                    </div>\
                </div>\
        </div>';
    }    
    $('.nutricion_imc').html(html);
}

function calcularimc(){
    var edad = $('#edadn').val();
    var ed = 0;
    if(edad==''){
        ed = 0;
    }else{
        ed = edad;
    }
    var e = parseFloat(ed);//edad
    var peso = $('#peson').val();
    
    var altura = $('#alturan').val();
    
    var cuello = $('#cuellon').val();

    
    var pe = 0;
    if(peso==''){
        pe = 0;
    }else{
        pe = peso;
    }
    var p = parseFloat(pe);//peso

    var al = 0;
    if(altura==''){
        al = 0;
    }else{
        al = altura;
    }
    var a = parseFloat(al);//Altura
    // Cuello
    var ce = 0;
    if(cuello==''){
        ce = 0;
    }else{
        ce = cuello;
    }
    var c = parseFloat(ce);// Cuello
    //
    var multi_altura = al * al;
    var suma = p / multi_altura;
   
    var conDecimal = Number(suma.toFixed(1));
    //alert(conDecimal);
    if(conDecimal==Infinity){
        $('.imc_n').html(0);
    }else{
        if(conDecimal==NaN){
            $('.imc_n').html(0);
        }else{
            $('.imc_n').html(conDecimal);
        }
        nutri_imc(conDecimal);
    }
    
    var sexo_text=$('#sexo_text').val();
    var peso_h='';/// Peso ideal 
    if(sexo_text==1){//hombre
        /// Peso ideal
        peso_i=23*multi_altura;
        var conDecimalh = Number(peso_i.toFixed(1));
        $('.peso_idealh_txt').html(conDecimalh);
        /*
        var gr1=1.0324-0.19077;
        var gr2=gr1;
        var gr_log1=89.0-Math.log();
        var gr=495/gr1;
        */
    }else{//Mujer
        /// Peso ideal 
        peso_i=21.5*multi_altura;
        var conDecimalm = Number(peso_i.toFixed(1));
        $('.peso_idealm_txt').html(conDecimalm);
        // Grasa 
    }
    /// Agua
    var cal_agua=0.033*pe;
    var cal_agua_decimal=cal_agua.toFixed(1);
    $('.agua_txt').html(cal_agua_decimal);
    /// Grasa 
    //console.log(c+' | '+a);
    /// cintura y alrtur
    var cintura = $('#cinturan').val();
    var cadera = $('#caderan').val();

    var ci = 0;
    if(cintura==''){
        ci = 0;
    }else{
        ci = cintura;
    }
    var cin = parseFloat(ci);//peso

    var ca = 0;
    if(cadera==''){
        ca = 0;
    }else{
        ca = cadera;
    }
    var can = parseFloat(ca);//peso
    /// calculo de indice cintura altura
    var suma_in_cin_can = cin/al;
    var suma_in_cin_can_dec=parseFloat(suma_in_cin_can);
    var suma_in_cin_can_deci=Number(suma_in_cin_can_dec.toFixed(1));
    $('.indice_cin_can_txt').html(suma_in_cin_can_deci);
    /// indice cintura cadena
    var sum_cadera=cin / can;
    var sum_caderan=parseFloat(sum_cadera);
    var sum_cadera_total = sum_caderan.toFixed(2);
    $('.indice_cintura_cadera_txt').html(sum_cadera_total);
    $.ajax({
        type:'POST',
        url: base_url+'Consultas/get_grasa',
        data: {c13:cin,c14:can,c5:c,c4:a,tipo:sexo_text},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            var gr=parseFloat(data);
            var gr_d=gr.toFixed(2);
            if (isNaN(gr_d)) gr_d = 0;
            $('.grasa_txt').html(gr_d);
        }
    });
    setTimeout(function(){
        // masa magra   
            var mg_grasa=$('.grasa_txt').text();
            var mg_grasan=parseFloat(mg_grasa);
        if(sexo_text==1){  
            var resta_mg=100-mg_grasan;
            var mg=pe*resta_mg/100; 
            var mg_suma=mg.toFixed(2);
            $('.masa_magra_txt').html(mg_suma);
        }else{
            var resta_mg=100-mg_grasan;
            var mg=pe*resta_mg/100; 
            var mg_suma=mg.toFixed(2);
            $('.masa_magra_txt').html(mg_suma);
        }
        /////  Mifflin St-Jeor
        mifflin_st_jeor_y_harris_benedict(p,a,e,sexo_text);
        //// FAO/OMS / Fórmula
        fao_oms_formula(p);
        setTimeout(function(){
            var mifflin=$('.mifflin_st_jeor_txt').text();
            var harris=$('.harris_benedict_txt').text();
            fisica_mifflin_st_jeor(mifflin);
            fisica_harris_benedict(harris);
        }, 500);
    }, 1000);
}

function fao_oms_formula(peso){
        var for1=(15.3*peso)+679;
        var form1=Number(parseFloat(for1.toFixed(0)));
        $('.for1_txt').html(form1);
        // formula 2
        var for2=(11.6*peso)+879;
        var form2=Number(parseFloat(for2.toFixed(0)));
        $('.for2_txt').html(form2);
        // formula 3
        var for3=(13.5*peso)+489;
        var form3=Number(parseFloat(for3.toFixed(0)));
        $('.for3_txt').html(form3);
        /// kcal
        //kcal 1
        var kcal1=peso*20;
        var kcalk1=Number(parseFloat(kcal1.toFixed(0)));
        $('.kcal1_txt').html(kcalk1);
        // kcal 2
        var kcal2=25*peso;
        var kcalk2=Number(parseFloat(kcal2.toFixed(0)));
        $('.kcal2_txt').html(kcalk2);
        // kcal 3
        var kcal3=30*peso;
        var kcalk3=Number(parseFloat(kcal3.toFixed(0)));
        $('.kcal3_txt').html(kcalk3);
        /// formula de fao/oms
        setTimeout(function(){
            var for1_txt=$('.for1_txt').text();
            var for2_txt=$('.for2_txt').text();
            var for3_txt=$('.for3_txt').text();
            calculo_fao_oms_18(for1_txt);
            calculo_fao_oms_30(for2_txt);
            calculo_fao_oms_60(for3_txt);
        }, 500);
}

function mifflin_st_jeor_y_harris_benedict(peso,altura,edad,sexo_text){
    if(sexo_text==1){  
            var s1= ((10*peso)+(6.25*altura))-(5*edad)+5;
            var miffs1=Number(parseFloat(s1.toFixed(0)));

            $('.mifflin_st_jeor_txt').html(miffs1);
        }else{
            var s1= ((10*peso)+(6.25*altura))-(5*edad)-161;
            var miffs1=Number(parseFloat(s1.toFixed(0)));

            $('.mifflin_st_jeor_txt').html(miffs1);
        }
        /// Harris-Benedict
        if(sexo_text==1){  
            var h1=(13.75*peso)+(5*altura)-(6.79*edad)+66
            var harr=Number(parseFloat(h1.toFixed(0)));
            $('.harris_benedict_txt').html(harr);
        }else{
            var har=(9.56*peso)+(1.85*altura)-(4.68*edad)+655;
            var harris=Number(parseFloat(har.toFixed(0)));
            $('.harris_benedict_txt').html(harris);
        }
}

function calculo_fao_oms_18(for1_txt){
    //Sedentario
    var se=1.3*for1_txt;
    var sed=Number(parseFloat(se.toFixed(0)));
    $('.sed_18').html(sed);
    //Ligera
    var li=1.5*for1_txt;
    var lig=Number(parseFloat(li.toFixed(0)));
    $('.lig_18').html(lig);
    //Moderada
    var mo=1.6*for1_txt;
    var mod=Number(parseFloat(mo.toFixed(0)));
    $('.mod_18').html(mod);
    //Alta
    var al=1.9*for1_txt;
    var alt=Number(parseFloat(al.toFixed(0)));
    $('.alt_18').html(alt);
    //Muy Alta
    var mu=2.2*for1_txt;
    var muy=Number(parseFloat(mu.toFixed(0)));
    $('.muy_18').html(muy);
}
function calculo_fao_oms_30(for2_txt){
    //Sedentario
    var se=1.3*for2_txt;
    var sed=Number(parseFloat(se.toFixed(0)));
    $('.sed_30').html(sed);
    //Ligera
    var li=1.5*for2_txt;
    var lig=Number(parseFloat(li.toFixed(0)));
    $('.lig_30').html(lig);
    //Moderada
    var mo=1.6*for2_txt;
    var mod=Number(parseFloat(mo.toFixed(0)));
    $('.mod_30').html(mod);
    //Alta
    var al=1.9*for2_txt;
    var alt=Number(parseFloat(al.toFixed(0)));
    $('.alt_30').html(alt);
    //Muy Alta
    var mu=2.2*for2_txt;
    var muy=Number(parseFloat(mu.toFixed(0)));
    $('.muy_30').html(muy);
}
function calculo_fao_oms_60(for3_txt){
    //Sedentario
    var se=1.3*for3_txt;
    var sed=Number(parseFloat(se.toFixed(0)));
    $('.sed_60').html(sed);
    //Ligera
    var li=1.5*for3_txt;
    var lig=Number(parseFloat(li.toFixed(0)));
    $('.lig_60').html(lig);
    //Moderada
    var mo=1.6*for3_txt;
    var mod=Number(parseFloat(mo.toFixed(0)));
    $('.mod_60').html(mod);
    //Alta
    var al=1.9*for3_txt;
    var alt=Number(parseFloat(al.toFixed(0)));
    $('.alt_60').html(alt);
    //Muy Alta
    var mu=2.2*for3_txt;
    var muy=Number(parseFloat(mu.toFixed(0)));
    $('.muy_60').html(muy);
}

/// Actividad Física
/// Mifflin St-Jeor
function fisica_mifflin_st_jeor(mifflin){
    //Encamado
    var en=1.1*mifflin;
    var enca=Number(parseFloat(en.toFixed(0)));
    $('.enc_mifflin').html(enca);
    //Leve/Sedentario
    var le=1.2*mifflin;
    var len=Number(parseFloat(le.toFixed(0)));
    $('.lev_mifflin').html(len);
    //Moderada
    var mo=1.3*mifflin;
    var mod=Number(parseFloat(mo.toFixed(0)));
    $('.mod_mifflin').html(mod);
    //Fuerte
    var fu=1.5*mifflin;
    var fue=Number(parseFloat(fu.toFixed(0)));
    $('.fue_mifflin').html(fue);
}
/// Harris-Benedict
function fisica_harris_benedict(harris){
    //Encamado
    var en=1.1*harris;
    var enca=Number(parseFloat(en.toFixed(0)));
    $('.enc_harris').html(enca);
    //Leve/Sedentario
    var le=1.2*harris;
    var len=Number(parseFloat(le.toFixed(0)));
    $('.lev_harris').html(len);
    //Moderada
    var mo=1.3*harris;
    var mod=Number(parseFloat(mo.toFixed(0)));
    $('.mod_harris').html(mod);
    //Fuerte
    var fu=1.5*harris;
    var fue=Number(parseFloat(fu.toFixed(0)));
    $('.fue_harris').html(fue);
}

function calcular_dieta(){
    var aporte1=$('#aporte1').val();
    var aporte2=$('#aporte2').val();
    var aporte3=$('#aporte3').val();
    var aporte4=$('#aporte4').val();
    var aporte5=$('#aporte5').val();
    var aporte6=$('#aporte6').val();
    var aporte7=$('#aporte7').val();
    var aporte8=$('#aporte8').val();
    var aporte9=$('#aporte9').val();
    var aporte10=$('#aporte10').val();
    var aporte11=$('#aporte11').val();
    var aporte12=$('#aporte12').val();
    var aporte13=$('#aporte13').val();
    var aporte14=$('#aporte14').val();
    var aporte15=$('#aporte15').val();
    var aporte16=$('#aporte16').val();
    var aporte17=$('#aporte17').val();
    var aporte18=$('#aporte18').val();
    var aporte19=$('#aporte19').val();
    //////////////////////////////////
    //// Aporte nutrimental información 2
    $('.aportext1').html(aporte1);
    $('.aportext2').html(aporte2);
    $('.aportext3').html(aporte3);
    $('.aportext4').html(aporte4);
    $('.aportext5').html(aporte5);
    $('.aportext6').html(aporte6);
    $('.aportext7').html(aporte7);
    $('.aportext8').html(aporte8);
    $('.aportext9').html(aporte9);
    $('.aportext10').html(aporte10);
    $('.aportext11').html(aporte11);
    $('.aportext12').html(aporte12);
    $('.aportext13').html(aporte13);
    $('.aportext14').html(aporte14);
    $('.aportext15').html(aporte15);
    $('.aportext16').html(aporte16);
    $('.aportext17').html(aporte17);
    $('.aportext18').html(aporte18);
    $('.aportext19').html(aporte19);
    /// operadores de nutrimental de comida
}