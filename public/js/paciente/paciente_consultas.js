var tipo_consulta_general=0;
//==========================((( General Consultas )))========================
function eliminar_consulta(id,tipo_consulta){
    tipo_consulta_aux=tipo_consulta;
    $('#elimina_consulta_modal').modal();
    $('#idconsulta_e').val(id);
}
function eliminarconsulta(){
    var id = $('#idconsulta_e').val();
    $("#consulta_tipo option[value='4']").attr("selected", true);
    var tipo_tabla='';
    if(tipo_consulta_aux==1){
        tipo_tabla='consulta_medicina_estetica';
    }else if(tipo_consulta_aux==2){
        tipo_tabla='consulta_spa';
    }else if(tipo_consulta_aux==3){
        tipo_tabla='consulta_nutricion';
    }
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/delete_consulta",
        data:{id:id,tabla:tipo_tabla},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Eliminado Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_consulta_modal').modal('hide');
            if(tipo_consulta_aux==1){
                visualizar_consulta_medicina_esttetica(id);
            }else if(tipo_consulta_aux==2){
                visualizar_spa(id);
            }else if(tipo_consulta_aux==3){
                visualizar_consulta_nutricion(id);
            }
            select_tipo_consulta();
        }
    }); 
}
function restaurar_consulta(id,tipo_consulta){
    $("#consulta_tipo option[value='1']").attr("selected", true);
    var tipo_tabla='';
    if(tipo_consulta==1){
        tipo_tabla='consulta_medicina_estetica';
    }else if(tipo_consulta==2){
        tipo_tabla='consulta_spa';
    }else if(tipo_consulta==3){
        tipo_tabla='consulta_nutricion';
    }
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/restaura_consulta",
        data:{id:id,tabla:tipo_tabla},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Se ha restaurado la consulta',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            if(tipo_consulta==1){
                visualizar_consulta_medicina_esttetica(id);
            }else if(tipo_consulta==2){
                visualizar_spa(id);
            }else if(tipo_consulta==3){
                visualizar_consulta_nutricion(id);
            }
            select_tipo_consulta();
        }
    }); 
}
function marcar_importante(id,status,tipo_consulta){
    if(tipo_consulta==1){
        tipo_tabla='consulta_medicina_estetica';
    }else if(tipo_consulta==2){
        tipo_tabla='consulta_spa';
    }else if(tipo_consulta==3){
        tipo_tabla='consulta_nutricion';
    }
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/consulta_importante",
        data:{id:id,estatus:status,tabla:tipo_tabla},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Guardado Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            if(tipo_consulta==1){
                visualizar_consulta_medicina_esttetica(id);
            }else if(tipo_consulta==2){
                visualizar_spa(id);
            }else if(tipo_consulta==3){
                visualizar_consulta_nutricion(id);
            }
            
            select_tipo_consulta();
        }
    }); 
}
//==========(((((( Medicina Estetica))))))===============
var aux_signov_c1=0;
function ocultar_signosvitales_c1(){
    if(aux_signov_c1==0){
        aux_signov_c1=1;
        $('.txt_signosvitales_c1').css('display','none');
    }else{
        aux_signov_c1=0;
        $('.txt_signosvitales_c1').css('display','block');
    }    
}
function agregar_diagnostico_estetica(){
    add_diagnostico_estetica(0,'');
}
var aux_diagnostico_c1=0;
function add_diagnostico_estetica(id,diagnostico){
    var html='';
    var btn_html='';
        if(aux_diagnostico_c1==0){
        btn_html='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-info" onclick="agregar_diagnostico_estetica()"><i class="fas fa-plus"></i></button>';
        }else{
        btn_html='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-secondary" onclick="diagnostico_remove_estetica('+aux_diagnostico_c1+','+id+')"><i class="fas fa-minus"></i></button>';
        }
        html+='<li class="row_diag_c1_'+aux_diagnostico_c1+'">\
                    <div class="row">\
                        <div class="col-md-11">\
                            <div class="form-line">\
                                <input type="hidden" id="iddiagnostico_c1" value="'+id+'">\
                                <input type="text" id="diagnos_c1" class="form-control colorlabel_white js-auto-size diagnos_c1_'+aux_diagnostico_c1+'" value="'+diagnostico+'" oninput="buscar_diagnostico_estetica_txt('+aux_diagnostico_c1+')">\
                            </div>\
                            <div style="position: relative;">\
                                <div class="diagnostico_estetico_buscar_t_'+aux_diagnostico_c1+'" style="position: absolute; z-index: 3;">\
                                </div>\
                            </div>\
                        </div>\
                        <div class="col-md-1">\
                            '+btn_html+'\
                        </div>\
                    </div>\
                </li>';
    $('.ol_diagnost_text_c1').append(html);
    $('textarea.js-auto-size').textareaAutoSize();
    aux_diagnostico_c1++;
}
function buscar_diagnostico_estetica_txt(id){
    id_diag_estetica_aux=id;
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/buscardiagnostico_estetico',
        data:{nombre:$('.diagnos_c1_'+id).val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.diagnostico_estetico_buscar_t_'+id).html(data);
        }
    });
}
function diagnostico_estetica_select(id){
    var dtext = $('.diag_estetica_text'+id).text(); 
    $('.diagnos_c1_'+id_diag_estetica_aux).val(dtext);
    $('.diagnostico_estetico_buscar_t_'+id_diag_estetica_aux).html('');
}
function tabla_diagnostico_estetica(id){
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/get_diagnostico_estetica",
        data: {id:id},
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  add_diagnostico_estetica(element.iddiagnostico,element.diagnostico);
                });
            }else{
                agregar_diagnostico_estetica();
            }   
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}
function diagnostico_remove_estetica(id,idd){
    if(idd==0){
        $('.row_diag_c1_'+id).remove();
    }else{
        $('#elimina_diagnostico_estetica_modal').modal();
        $('#iddiagnostico_c1').val(idd);
        $('#iddiagnostico_remove_c1').val(id);
    }
}
function delete_diagnostico_estetica(){
    var id=$('#iddiagnostico_c1').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/delete_diagnostico_estetica",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Se ha eliminado correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_diagnostico_estetica_modal').modal('hide');
            $('.row_diag_c1_'+$('#iddiagnostico_remove_c1').val()).remove();
        }
    });  
}
//((((((Tratamiento))))))
function agregar_tratamiento_estetica(){
    add_tratamiento_estetica(0,'');
}
var aux_tratamiento_c1=0;
function add_tratamiento_estetica(id,tratamiento){
    var html='';
    var btn_html='';
        if(aux_tratamiento_c1==0){
        btn_html='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-info" onclick="agregar_tratamiento_estetica()"><i class="fas fa-plus"></i></button>';
        }else{
        btn_html='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-secondary" onclick="tratamiento_remove_estetica('+aux_tratamiento_c1+','+id+')"><i class="fas fa-minus"></i></button>';
        }
        html+='<li class="row_trat_c1_'+aux_tratamiento_c1+'">\
                    <div class="row">\
                        <div class="col-md-11">\
                            <div class="form-line">\
                                <input type="hidden" id="idtratamiento_c1" value="'+id+'">\
                                <input type="text" id="trata_c1" class="form-control colorlabel_white js-auto-size trata_c1_'+aux_tratamiento_c1+'" value="'+tratamiento+'" oninput="buscar_tratamiento_estetica_txt('+aux_tratamiento_c1+')">\
                            </div>\
                            <div style="position: relative;">\
                                <div class="tratamiento_buscar_t_'+aux_tratamiento_c1+'" style="position: absolute; z-index: 3;">\
                                </div>\
                            </div>\
                        </div>\
                        <div class="col-md-1">\
                            '+btn_html+'\
                        </div>\
                    </div>\
                </li>';
    $('.ol_tratamiento_text_c1').append(html);
    $('textarea.js-auto-size').textareaAutoSize();
    aux_tratamiento_c1++;
}
function buscar_tratamiento_estetica_txt(id){
    id_trata_estetica_aux=id;
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/buscartratamiento_estetico',
        data:{nombre:$('.trata_c1_'+id).val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.tratamiento_buscar_t_'+id).html(data);
        }
    });
}
function tratamiento_estetica_select(id){
    var dtext = $('.trata_estetica_text'+id).text(); 
    $('.trata_c1_'+id_trata_estetica_aux).val(dtext);
    $('.tratamiento_buscar_t_'+id_trata_estetica_aux).html('');
}
function tratamiento_remove_estetica(id,idd){
    if(idd==0){
        $('.row_trat_c1_'+id).remove();
    }else{
        $('#elimina_tratamiento_estetica_modal').modal();
        $('#idtratami_c1').val(idd);
        $('#idtratami_remove_c1').val(id);
    }
}
function delete_tratamiento_estetica(){
    var id=$('#idtratami_c1').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/delete_tratamiento_estetica",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Se ha eliminado correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_tratamiento_estetica_modal').modal('hide');
            $('.row_trat_c1_'+$('#idtratami_remove_c1').val()).remove();
        }
    });  
}
function tabla_tratamiento_estetica(id){
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/get_tratamiento_estetica",
        data: {id:id},
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  add_tratamiento_estetica(element.idtratamiento,element.tratamiento);
                });
            }else{
                agregar_tratamiento_estetica();
            }   
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}
//((()))
function guardar_consulta_medicina_estetica(){
    //============================
    /*
        var DATAmes  = [];
        var TABLA   = $("#table_me_session tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["fecha"] = $(this).find("input[id*='t_fecha_me']").val();
            item ["row"]   = $(this).find("input[id*='t_row_me']").val();
            item ["obser"]  = $(this).find("input[id*='t_obser_me']").val();
            DATAmes.push(item);
        });
        INFO  = new FormData();
        arraymessession   = JSON.stringify(DATAmes);
    */
    //============================
    var datos = $('#form_consulta_medicina_estetica').serialize();//+'&arraymessession='+arraymessession;
    $('.btn_consulta_medicina_estetica').prop('disabled', true);
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/registra_consulta_medicina_estetica',
        data: datos,
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            var idcli = data
            /// Diagnostico
            var DATA  = [];
            var TABLA   = $(".texto_diagnostico_c1 .ol_diagnost_text_c1 > li");
            TABLA.each(function(){  
                if($(this).find("input[id*='diagnos_c1']").val()!=''){
                    item = {};
                    item ["idconsulta"] = idcli;
                    item ["iddiagnostico"] = $(this).find("input[id*='iddiagnostico_c1']").val();
                    item ["diagnostico"] = $(this).find("input[id*='diagnos_c1']").val();
                    DATA.push(item);
                }
            });
            INFO  = new FormData();
            aInfo   = JSON.stringify(DATA);
            INFO.append('data', aInfo);
            $.ajax({
                data: INFO, 
                type: 'POST',
                url : base_url + 'PacientesConsultas/registro_diagnostico_estetica',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            }); 
            /// Tratamiento
            var DATAT  = [];
            var TABLAT   = $(".texto_tratamiento_c1 .ol_tratamiento_text_c1 > li");
            TABLAT.each(function(){  
                if($(this).find("input[id*='trata_c1']").val()!=''){
                    item = {};
                    item ["idconsulta"] = idcli;
                    item ["idtratamiento"] = $(this).find("input[id*='idtratamiento_c1']").val();
                    item ["tratamiento"] = $(this).find("input[id*='trata_c1']").val();
                    DATAT.push(item);
                }
            });
            INFOT  = new FormData();
            aInfot   = JSON.stringify(DATAT);
            INFOT.append('data', aInfot);
            $.ajax({
                data: INFOT, 
                type: 'POST',
                url : base_url + 'PacientesConsultas/registro_tratamiento_estetica',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            }); 

            /// Venta de Servicios
            var DATAT  = [];
            var TABLAT   = $("#table_servicio_estetica tbody > tr");
            TABLAT.each(function(){  
                    item = {};
                    item ["idconsulta"] = idcli;
                    item ["iddetalles"] = $(this).find("input[id*='iddetalles_c1']").val();
                    item ["idservicio"] = $(this).find("input[id*='idservicio_c1']").val();
                    item ["costo"] = $(this).find("input[id*='costo_c1']").val();
                    
                    DATAT.push(item);
            });
            INFOT  = new FormData();
            aInfot   = JSON.stringify(DATAT);
            INFOT.append('data', aInfot);
            $.ajax({
                data: INFOT, 
                type: 'POST',
                url : base_url + 'PacientesConsultas/registro_venta_servicios',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            }); 
            //=====Ventas=====
            var DATAP  = [];
            var TABLAP   = $("#tabla_producto_venta_consultas tbody > tr");
            TABLAP.each(function(){  
                item = {};
                item ["idconsulta"] = idcli;
                item ["iddetalles"] = $(this).find("input[id*='iddetalles_c1']").val();
                item ["idalmacen"] = $(this).find("input[id*='idalmacen_c1']").val();
                item ["cantidad"] = $(this).find("input[id*='cantidad_c1']").val();
                item ["preciou"] = $(this).find("input[id*='preciou_c1']").val();
                item ["totalp"] = $(this).find("input[id*='total_c1']").val();

                DATAP.push(item);
            });
            INFOP  = new FormData();
            aInfop   = JSON.stringify(DATAP);
            INFOP.append('data', aInfop);
            $.ajax({
                data: INFOP, 
                type: 'POST',
                url : base_url + 'PacientesConsultas/registro_venta_proyecto',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            }); 
            //==================
            var analisis_aux=$('#editar_analisis_facial_c1').val();
            if(analisis_aux==0){
                var canvas = $("#patientSignature")[0];
                if (!isCanvasBlank(canvas)) {
                    var canvas = document.getElementById('patientSignature');
                    var dataURL = canvas.toDataURL();
                    $.ajax({
                        type:'POST',
                        url: base_url+'PacientesConsultas/add_facial_firma',
                        data: {id:idcli,facial:dataURL},
                        statusCode:{
                            404: function(data){
                                $.toast({
                                    heading: 'Error!',
                                    text: '404',
                                    position: 'top-right',
                                    loaderBg:'#ff6849',
                                    icon: 'error',
                                    hideAfter: 3500
                                });
                            },
                            500: function(){
                                $.toast({
                                    heading: 'Error!',
                                    text: '500',
                                    position: 'top-right',
                                    loaderBg:'#ff6849',
                                    icon: 'error',
                                    hideAfter: 3500
                                });
                            }
                        },
                        success:function(data){
                            setTimeout(function(){  
                                window.close();
                            }, 1500);
                        }
                    });
                }
            }    
            //==Firma del paciente
            //==================
            var canvaspc1 = $("#patientSignaturepfc1")[0];
            if (!isCanvasBlank(canvaspc1)) {
                var canvaspc1 = document.getElementById('patientSignaturepfc1');
                var dataURLfpc1 = canvaspc1.toDataURL();
                $.ajax({
                    type:'POST',
                    url: base_url+'PacientesConsultas/add_firma_pacientec1',
                    data: {id:idcli,firma:dataURLfpc1},
                    statusCode:{
                        404: function(data){
                            $.toast({
                                heading: 'Error!',
                                text: '404',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'error',
                                hideAfter: 3500
                            });
                        },
                        500: function(){
                            $.toast({
                                heading: 'Error!',
                                text: '500',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'error',
                                hideAfter: 3500
                            });
                        }
                    },
                    success:function(data){
                        setTimeout(function(){  
                            window.close();
                        }, 1500);
                    }
                });
            }
            //==Firma del cosmetologa
            //==================
            var canvascc1 = $("#patientsignatureccfc1")[0];
            if (!isCanvasBlank(canvascc1)) {
                var canvascc1 = document.getElementById('patientsignatureccfc1');
                var dataURLfcc1 = canvascc1.toDataURL();
                $.ajax({
                    type:'POST',
                    url: base_url+'PacientesConsultas/add_firma_comestologac1',
                    data: {id:idcli,firma:dataURLfcc1},
                    statusCode:{
                        404: function(data){
                            $.toast({
                                heading: 'Error!',
                                text: '404',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'error',
                                hideAfter: 3500
                            });
                        },
                        500: function(){
                            $.toast({
                                heading: 'Error!',
                                text: '500',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'error',
                                hideAfter: 3500
                            });
                        }
                    },
                    success:function(data){
                        setTimeout(function(){  
                            window.close();
                        }, 1500);
                    }
                });
            }
            //==================
            $.toast({
                heading: 'Éxito',
                text: 'Se guardaron los cambios de forma exitosa',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            setTimeout(function(){ 
                window.location.reload();
            }, 1500);
        }
    });
}
function visualizar_consulta_medicina_esttetica(idc){
    aux_diagnostico_c1=0;
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/visualizar_consulta_medicina_estetica_text',
        data:{id:idc},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.consulta').html(data);
        }
    });
}
// Medicina
function txt_consulta_medicina(idc){
    tipo_consulta_general=1;
    aux_diagnostico_c1=0;
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/text_consulta_medicina',
        data:{idc:idc,idp:$('#idpte').val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.consulta').html(data);
            $('#recomendaciones').ckeditor();
            if(idc==0){
                agregar_diagnostico_estetica();
                agregar_tratamiento_estetica();
            }else{
                setTimeout(function(){ 
                    tabla_diagnostico_estetica(idc);
                    tabla_tratamiento_estetica(idc);
                    tabla_venta_servicio_c1(idc);
                    tabla_venta_producto_c1(idc);
                }, 1500);
            }
            setTimeout(function(){ 
                $('textarea.js-auto-size').textareaAutoSize();
                $('[data-toggle="tooltip"]').tooltip();
                createSignature('patientSignature');
                //======================================
                setTimeout(function(){ 
                    
                    volveracargarimgfondo();
                }, 1000);
                //=======================================
                createSignaturepfc1('patientSignaturepfc1');
                createSignaturecfc1('patientsignatureccfc1');
            }, 1500);
            servicio_select();
            producto_select();

        }
    }); 
}
function isCanvasBlank(canvas) {
    var blank = document.createElement('canvas');
    blank.width = canvas.width;
    blank.height = canvas.height;
    return canvas.toDataURL() == blank.toDataURL();
}
function servicio_select(){
    $('#idservicio').select2({
        width: '100%', 
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un servicio',
        allowClear:true,
        ajax: {
            url: base_url + 'PacientesConsultas/searchservicio',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.idservicio,
                        text: element.nombre,
                        desc: element.descripcion,
                        costo: element.costo,
                        cantidad: element.cantidad
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        add_servicio_estetica(0,data.id,data.text,data.desc,data.costo,data.cantidad);
    });
}
var aux_servicio_c1=0;
function add_servicio_estetica(iddetalles,idservicio,nombre,descrpcion,costo,cantidad){
    var html='';
    var conDecimal=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(costo);
    var btn_html='';
        btn_html='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs  btn-danger" onclick="remove_servicio_c1('+aux_servicio_c1+','+iddetalles+')"><i class="fas fa-trash-alt"></i> </button>';
        html+='<tr class="row_serv_c1_'+aux_servicio_c1+'">\
                    <th>\
                        <input type="hidden" id="iddetalles_c1" value="'+iddetalles+'">\
                        <input type="hidden" id="idservicio_c1" value="'+idservicio+'">\
                        <input type="hidden" id="costo_c1" value="'+costo+'">\
                        <input type="hidden" class="ser_cantidad" value="'+cantidad+'">\
                        '+nombre+'\
                    </th>\
                    <th>'+descrpcion+'</th>\
                    <th>'+conDecimal+'</th>\
                    <th>'+btn_html+'</th>\
                </tr>';
    $('#table_servicio_estetica tbody').append(html);

    suma_costo_total_servicio();
    aux_servicio_c1++;
}

function remove_servicio_c1(id,idd){
    if(idd==0){
        $('.row_serv_c1_'+id).remove();
        suma_costo_total_servicio();
    }else{
        $('#elimina_modal_venta_servicio_c1').modal();
        $('#idvet_c1').val(idd);
        $('#idvet_c1_remove').val(id);
    }

}
function delete_venta_servicio_c1(){
    var id=$('#idvet_c1').val();
    var tipo_consulta = tipo_consulta_general;
    $.ajax({
        type:'POST',
        url: base_url+"PacientesConsultas/delete_venta_servicio_c1",
        data:{id:id,tipo_consulta:tipo_consulta},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Se ha eliminado correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_modal_venta_servicio_c1').modal('hide');
            $('.row_serv_c1_'+$('#idvet_c1_remove').val()).remove();
            setTimeout(function(){ 
                suma_costo_total_servicio();
            }, 1000);
        }
    });  
}

function suma_costo_total_servicio(){
    var addtp = 0;
    var TABLAP = $("#table_servicio_estetica tbody > tr");            
    TABLAP.each(function(){         
        var totalmonto = $(this).find("input[id*='costo_c1']").val();
        var vstotal = totalmonto;
        addtp += Number(vstotal);
    });
    var conDecimal = addtp.toFixed(2);
    //var monto_mtl=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(conDecimal);
    $('.total_costo').html(conDecimal);
}
//(((Tabla de venta de servicios)))
function tabla_venta_servicio_c1(id){
    $.ajax({
        type:'POST',
        url: base_url+"PacientesConsultas/get_venta_servicios_c1",
        data: {id:id},//Consulta
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                    add_servicio_estetica(element.iddetalles,element.idservicio,element.servicio,element.descripcion,element.costo,0);
                });
            }  
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}
// Producto venta
function producto_select(){
    $('#idproducto').select2({
        width: '100%', 
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar producto',
        allowClear:true,
        ajax: {
            url: base_url + 'PacientesConsultas/searchproducto',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.idproducto,
                        text: element.producto,
                        clinica: element.clinica,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e){
        var data = e.params.data;
        $('#precio_clinica').val(data.clinica);
        lotes_productos(data.id);
        /*
        var data = e.params.data;
        var idv_c1=0;
        if(idventa_c1_aux==0){
            idv_c1=0;
        }else{
            idv_c1=idventa_c1_aux;
        }
        add_servicio_estetica(idv_c1,0,data.id,data.text,data.desc,data.costo);
        */
    });
}
function lotes_productos(id){
    $.ajax({
        type:'POST',
        url: base_url+"PacientesConsultas/get_detalles_productos",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.lotes_productos').html(data);

        }
    });  
}
var idalmacen_aux=0;
var lote_aux='';
var cadu_aux='';
var cantidad_aux=0;
function agregar_productos_venta(){

    var producto=$('#idproducto option:selected').text();
    var idalmacen=idalmacen_aux;
    var lote=lote_aux;
    var caducidad=cadu_aux;
    var preciou=$('#precio_clinica').val();

    var canti=$('#cantidad_lote').val();
    var multi=preciou*canti;
    var total=multi; 
    if(parseFloat(cantidad_aux)>=parseFloat(canti)){
        var cantidad=canti;
        add_venta_productos(0,producto,idalmacen,lote,caducidad,cantidad,preciou,total);
        $('#cantidad_lote').val('');
    }else{
        $.toast({
            heading: 'Atención!',
            text: 'Ingrese una cantidad no mayor al stock',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }    
} 
var aux_producto=0;
function add_venta_productos(iddetalles,producto,idalmacen,lote,caducidad,cantidad,preciou,total){
    var html='';
    var preciou_aux=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(preciou);
    var total_aux=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total);
    var btn_html=0;
        if(iddetalles==0){
            btn_html='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs  btn-danger" onclick="remove_producto_c1('+aux_producto+','+iddetalles+')"><i class="fas fa-trash-alt"></i> </button>';
        }else{   
            btn_html='';
        }    
        html+='<tr class="row_pro_c1_'+aux_producto+'">\
                    <th>\
                        <input type="hidden" id="iddetalles_c1" value="'+iddetalles+'">\
                        <input type="hidden" id="idalmacen_c1" value="'+idalmacen+'">\
                        <input type="hidden" id="cantidad_c1" value="'+cantidad+'">\
                        <input type="hidden" id="preciou_c1" value="'+preciou+'">\
                        <input type="hidden" id="total_c1" value="'+total+'">\
                        '+producto+'</th>\
                    <th>'+lote+'</th>\
                    <th>'+caducidad+'</th>\
                    <th>'+cantidad+'</th>\
                    <th>'+preciou_aux+'</th>\
                    <th>'+total_aux+'</th>\
                    <th>'+btn_html+'</th>\
                </tr>';
    $('#tabla_producto_venta_consultas tbody').append(html);
    suma_costo_total_productos();
    aux_producto++;
}

function remove_producto_c1(id,idd){
    if(idd==0){
        $('.row_pro_c1_'+id).remove();
        suma_costo_total_productos();
    }else{
        //$('#elimina_modal_venta_servicio_c1').modal();
        //$('#idvet_c1').val(idd);
        //$('#idvet_c1_remove').val(id);
    }

}
function suma_costo_total_productos(){
    var addtp = 0;
    var TABLAP = $("#tabla_producto_venta_consultas tbody > tr");            
    TABLAP.each(function(){         
        var totalmonto = $(this).find("input[id*='total_c1']").val();
        var vstotal = totalmonto;
        addtp += Number(vstotal);
    });
    var conDecimal = addtp.toFixed(2);
    //var monto_mtl=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(conDecimal);
    $('.total_costop').html(conDecimal);
}
function select_producto_detalle(){
    var id = $('#idalmacen option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"PacientesConsultas/get_producto_almacen",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            var y = $.parseJSON(data);
            idalmacen_aux=y.idalmacen;
            lote_aux=y.lote;
            cadu_aux=y.cadu;
            cantidad_aux=y.cantidad;
        }
    });  
}

//(((Tabla de venta de servicios)))
function tabla_venta_producto_c1(id){
    $.ajax({
        type:'POST',
        url: base_url+"PacientesConsultas/get_venta_producto_c1",
        data: {id:id},//Consulta
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                    add_venta_productos(element.iddetalles,element.producto,element.idalmacen,element.lote,element.fecha_caducidad,element.cantidad,element.preciou,element.total);
                });
            }  
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}
function editar_fasial_btn(id){
    $('.canvas_registroc1').css('display','none');
    $('.canvas_nuevoc1').css('display','block');
    $('#editar_analisis_facial_c1').val(0);
    tipo_servicio_select();
}
function firma_paciente_c1_btn(id){
    $('.canvas_firmafpc1').css('display','none');
    $('.canvas_firmafepc1').css('display','block');
}
function firma_cosmetologa_c1_btn(id){
    $('.canvas_firmafcc1').css('display','none');
    $('.canvas_firmafecc1').css('display','block');
}
function imprimir_medicina_estetica_c1(id){
    if(id==0){
        $.toast({
            heading: '¡Atención!',
            text: 'Por favor primero guarde la consulta ',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }else{
        setTimeout(function(){ 
            window.open(base_url+'PacientesConsultas/imprimimedicinaesteticac1/'+id,'_blank');  
        }, 1000);
    }
}
function imprimir_medicina_estetica_ticketc1(id){
    if(id==0){
        $.toast({
            heading: '¡Atención!',
            text: 'Por favor primero guarde la consulta ',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }else{
        setTimeout(function(){ 
            $('#modal_ticket_medicina_estetica').modal(); 
            $('.iframereporte').html('<iframe src="' + base_url + 'Reportes/ticket_medicina_estetica/'+id+'" class="iframeprintc1" id="iframeprintc1"></iframe>');
        }, 1000);
    }
}
function imprimiriframac1(documentId){
    document.getElementById(documentId).contentWindow.print();
}
///////////////////////////////////////////////////////////////////////////////////////////////////
// EPA
function txt_consulta_spa(idc){
    tipo_consulta_general=2;
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/text_consulta_spa',
        data:{idc:idc,idp:$('#idpte').val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.consulta').html(data);
            $('#recomendaciones').ckeditor();
            if(idc!=0){
                setTimeout(function(){ 
                    tabla_venta_servicio_c2(idc);
                    tabla_venta_producto_c2(idc);
                }, 1500);
            }
            setTimeout(function(){ 
                $('textarea.js-auto-size').textareaAutoSize();
                $('[data-toggle="tooltip"]').tooltip();
                createSignature('patientSignature');
                //======================================
                setTimeout(function(){ 
                    volveracargarimgfondo();
                }, 1000);
                //=======================================
                createSignaturepfc1('patientSignaturepfc1');
                createSignaturecfc1('patientsignatureccfc1');
            }, 1500);

            servicio_select();
            producto_select();
        }
    }); 
}
///===============================================================
//((()))
function guardar_consulta_spa(){
    var datos = $('#form_spa').serialize();
    $('.btn_consulta_spa').prop('disabled', true);
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/registra_consulta_spa',
        data: datos,
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            var idcli = data
            //==================
            /// Venta de Servicios
            var DATAT  = [];
            var TABLAT   = $("#table_servicio_estetica tbody > tr");
            TABLAT.each(function(){  
                    item = {};
                    item ["idconsulta"] = idcli;
                    item ["iddetalles"] = $(this).find("input[id*='iddetalles_c1']").val();
                    item ["idservicio"] = $(this).find("input[id*='idservicio_c1']").val();
                    item ["costo"] = $(this).find("input[id*='costo_c1']").val();
                    
                    DATAT.push(item);
            });
            INFOT  = new FormData();
            aInfot   = JSON.stringify(DATAT);
            INFOT.append('data', aInfot);
            $.ajax({
                data: INFOT, 
                type: 'POST',
                url : base_url + 'PacientesConsultas/registro_venta_servicios_spa',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            }); 
            //=====Ventas=====
            var DATAP  = [];
            var TABLAP   = $("#tabla_producto_venta_consultas tbody > tr");
            TABLAP.each(function(){  
                item = {};
                item ["idconsulta"] = idcli;
                item ["iddetalles"] = $(this).find("input[id*='iddetalles_c1']").val();
                item ["idalmacen"] = $(this).find("input[id*='idalmacen_c1']").val();
                item ["cantidad"] = $(this).find("input[id*='cantidad_c1']").val();
                item ["preciou"] = $(this).find("input[id*='preciou_c1']").val();
                item ["totalp"] = $(this).find("input[id*='total_c1']").val();

                DATAP.push(item);
            });
            INFOP  = new FormData();
            aInfop   = JSON.stringify(DATAP);
            INFOP.append('data', aInfop);
            $.ajax({
                data: INFOP, 
                type: 'POST',
                url : base_url + 'PacientesConsultas/registro_venta_proyecto_spa',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            }); 
            /// analisis facial
            var analisis_aux=$('#editar_analisis_facial_c2').val();
            if(analisis_aux==0){
                var canvas = $("#patientSignature")[0];
                if (!isCanvasBlank(canvas)) {
                    var canvas = document.getElementById('patientSignature');
                    var dataURL = canvas.toDataURL();
                    $.ajax({
                        type:'POST',
                        url: base_url+'PacientesConsultas/add_facial_spa',
                        data: {id:idcli,facial:dataURL},
                        statusCode:{
                            404: function(data){
                                $.toast({
                                    heading: 'Error!',
                                    text: '404',
                                    position: 'top-right',
                                    loaderBg:'#ff6849',
                                    icon: 'error',
                                    hideAfter: 3500
                                });
                            },
                            500: function(){
                                $.toast({
                                    heading: 'Error!',
                                    text: '500',
                                    position: 'top-right',
                                    loaderBg:'#ff6849',
                                    icon: 'error',
                                    hideAfter: 3500
                                });
                            }
                        },
                        success:function(data){
                            setTimeout(function(){  
                                window.close();
                            }, 1500);
                        }
                    });
                }
            }
            //==Firma del paciente
            //==================
            var canvaspc1 = $("#patientSignaturepfc1")[0];
            if (!isCanvasBlank(canvaspc1)) {
                var canvaspc1 = document.getElementById('patientSignaturepfc1');
                var dataURLfpc1 = canvaspc1.toDataURL();
                $.ajax({
                    type:'POST',
                    url: base_url+'PacientesConsultas/add_firma_paciente_spa',
                    data: {id:idcli,firma:dataURLfpc1},
                    statusCode:{
                        404: function(data){
                            $.toast({
                                heading: 'Error!',
                                text: '404',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'error',
                                hideAfter: 3500
                            });
                        },
                        500: function(){
                            $.toast({
                                heading: 'Error!',
                                text: '500',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'error',
                                hideAfter: 3500
                            });
                        }
                    },
                    success:function(data){
                        setTimeout(function(){  
                            window.close();
                        }, 1500);
                    }
                });
            }
            //==Firma del cosmetologa
            //==================
            var canvascc1 = $("#patientsignatureccfc1")[0];
            if (!isCanvasBlank(canvascc1)) {
                var canvascc1 = document.getElementById('patientsignatureccfc1');
                var dataURLfcc1 = canvascc1.toDataURL();
                $.ajax({
                    type:'POST',
                    url: base_url+'PacientesConsultas/add_firma_comestologaspa',
                    data: {id:idcli,firma:dataURLfcc1},
                    statusCode:{
                        404: function(data){
                            $.toast({
                                heading: 'Error!',
                                text: '404',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'error',
                                hideAfter: 3500
                            });
                        },
                        500: function(){
                            $.toast({
                                heading: 'Error!',
                                text: '500',
                                position: 'top-right',
                                loaderBg:'#ff6849',
                                icon: 'error',
                                hideAfter: 3500
                            });
                        }
                    },
                    success:function(data){
                        setTimeout(function(){  
                            window.close();
                        }, 1500);
                    }
                });
            }
            //==================
            $.toast({
                heading: 'Éxito',
                text: 'Se guardaron los cambios de forma exitosa',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            setTimeout(function(){ 
                window.location.reload();
            }, 1500);
        }
    });
}
// Visualiza SPA
function visualizar_spa(idc){
    aux_diagnostico_c1=0;
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/visualizar_consulta_spa_text',
        data:{id:idc},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.consulta').html(data);
            //======================================
                setTimeout(function(){ 
                    
                    volveracargarimgfondo();
            }, 1000);
            //=======================================
        }
    });
}
function volveracargarimgfondo(){
    var sexo =$('#sexo_text').val();
    //================================
        var canvas = document.getElementById('patientSignature');
        var ctx  = canvas.getContext("2d");

        var img = new Image();
        if(sexo==2){
            img.src = base_url+"images/medicina/cara.png";
        }else{
            img.src = base_url+"images/medicina/cara_h.png";
        }
        img.onload = function(){
            ctx.drawImage(img, 0, 0);
        }
    $('.cargarimgcara').click(function(event) {
        var canvas = document.getElementById('patientSignature');
        var ctx  = canvas.getContext("2d");

        var img = new Image();
        if(sexo==2){
            img.src = base_url+"images/medicina/cara.png";
        }else{
            img.src = base_url+"images/medicina/cara_h.png";
        }
        img.onload = function(){
            ctx.drawImage(img, 0, 0);
        }
    });
}

// Nutricon
function txt_consulta_nutricion(idc){
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/text_consulta_nutricion',
        data:{idc:idc,idp:$('#idpte').val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){


            $('.consulta').html(data);
            $('#recomendaciones').ckeditor();
            nutri_imc(0);
            ///===========================
            if(idc!=0){
                setTimeout(function(){ 
                    tabla_venta_servicio_c3(idc);
                    tabla_venta_producto_c3(idc);
                }, 1500);
            }

            setTimeout(function(){ 
                $('textarea.js-auto-size').textareaAutoSize();
                $('[data-toggle="tooltip"]').tooltip();
            }, 1500);
            setTimeout(function(){ 
                calcularimc();
                btn_macronutrientes_kcal();
                calcular_calorias();
            }, 1000);
            setTimeout(function(){ 
                calcular_dieta();
            }, 1500);
            servicio_select();
            producto_select();
        }
    }); 
}
function nutri_imc(imc){
    /*
    SI(C8<18.5,"BAJO PESO",
    SI(C8<24.9,"PESO NORMAL",
    SI(C8<29.9,"SOBREPESO",
    SI(C8<34.5,"OBESIDAD GRADO 1",
    SI(C8<39.9,"OBESIDAD GRADO 2",
    SI(C8>=40,"OBESIDAD GRADO 3"))))))
    */
    var sexo_text=$('#sexo_text').val();
    var html='';
    /// hombre
    var img1='1.jpg';
    var img1g='gris2.png';
    var img1h='';
    var h1='';
    if(imc>0 && imc<18.5){ // SI(C8<18.5,"BAJO PESO",
        img1h=img1;
        h1='#93b4d7';
    }else{
        img1h=img1g;
        h1='#aaaaaa';
    }
    var img2='2.jpg';
    var img2g='gris8.png';
    var img2h='';
    var h2='';
    if(imc>18.5 && imc<24.9){ // SI(C8<24.9,"PESO NORMAL",
        img2h=img2;
        h2='#8fc59f';
    }else{
        img2h=img2g;
        h2='#aaaaaa';
    }
    var img3='3.jpg';
    var img3g='gris3.png';
    var img3h='';
    var h3='';
    if(imc>24.9 && imc<29.9){ // SI(C8<29.9,"SOBREPESO",
        img3h=img3;
        h3='#f9d648';
    }else{
        img3h=img3g;
        h3='#aaaaaa';
    }
    var img4='4.jpg';
    var img4g='gris9.png';
    var img4h='';
    var h4='';
    if(imc>29.9 && imc<34.5){ // SI(C8<34.5,"OBESIDAD GRADO 1",
        img4h=img4;
        h4='#e4985e';
    }else{
        img4h=img4g;
        h4='#aaaaaa';
    }
    var img5='5.jpg';
    var img5g='gris7.png';
    var img5h='';
    var h5='';
    if(imc>34.5 && imc<39.9){ // SI(C8<39.9,"OBESIDAD GRADO 2",
        img5h=img5;
        h5='#d55c5b';
    }else{
        img5h=img5g;
        h5='#aaaaaa';
    }
    var img6='5.jpg';
    var img6g='gris7.png';
    var img6h='';
    var h6='';
    if(imc>=40){ // SI(C8>=40,"OBESIDAD GRADO 3"
        img6h=img6;
        h6='#d55c5b';
    }else{
        img6h=img6g;
        h6='#aaaaaa';
    }
    //// Mujer
    var img1m='1m.jpg';
    var img1mg='gris10.png';
    var img1f='';
    var m1='';
    if(imc>0 && imc<18.5){ // SI(C8<18.5,"BAJO PESO",
        img1f=img1m;
        m1='#93b4d7';
    }else{
        img1f=img1mg;
        m1='#aaaaaa';
    }
    var img2m='2m.jpg';
    var img2mg='gris5.png';
    var img2f='';
    var m2='';
    if(imc>18.5 && imc<24.9){ // SI(C8<24.9,"PESO NORMAL",
        img2f=img2m;
        m2='#8fc59f';
    }else{
        img2f=img2mg;
        m2='#aaaaaa';
    }
    var img3m='3m.jpg';
    var img3mg='gris6.png';
    var img3f='';
    var m3='';
    if(imc>24.9 && imc<29.9){ // SI(C8<29.9,"SOBREPESO",
        img3f=img3m;
        m3='#f9d648';
    }else{
        img3f=img3mg;
        m3='#aaaaaa';
    }
    var img4m='4m.jpg';
    var img4mg='gris4.png';
    var img4f='';
    var m4='';
    if(imc>29.9 && imc<34.5){ // SI(C8<34.5,"OBESIDAD GRADO 1",
        img4f=img4m;
        m4='#e4985e';
    }else{
        img4f=img4mg;
        m4='#aaaaaa';
    }
    var img5m='5m.jpg';
    var img5mg='gris1.png';
    var img5f='';
    var m5='';
    if(imc>34.5 && imc<39.9){ // SI(C8<39.9,"OBESIDAD GRADO 2",
        img5f=img5m;
        m5='#d55c5b';
    }else{
        img5f=img5mg;
        m5='#aaaaaa';
    }
    var img6m='5m.jpg';
    var img6mg='gris1.png';
    var img6f='';
    var m6='';
    if(imc>=40){ // C8>=40,"OBESIDAD GRADO 3"
        img6f=img6m;
        m6='#d55c5b';
    }else{
        img6f=img6mg;
        m6='#aaaaaa';
    }
    if(sexo_text==1){
        html='<div class="row">\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img1h+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+h1+';color: #fff;">Bajo peso</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img2h+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+h2+';color: #fff;">Peso normal</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img3h+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+h3+';color: #fff;">Sobrepeso</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img4h+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+h4+';color: #fff;">Obesidad grado 1</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img5h+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+h5+';color: #fff;">Obesidad grado 2</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img6h+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+h6+';color: #fff;">Obesidad grado 3</span> \
                    </div>\
                </div>\
            </div>     \
            <div class="row">\
                <div class="col-md-4"></div>\
                <div class="col-md-4"><br><br><br>\
                    <div class="card">\
                        <div class="box bg-megna text-center">\
                            <h1 class="font-light text-white"><span class="peso_idealh_txt">0</span></h1>\
                            <h6 class="text-white">Peso ideal</h6>\
                        </div>\
                    </div>\
                </div>\
        </div>';
    }else{
        html='<div class="row">\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img1f+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+m1+';color: #fff;">Bajo peso</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img2f+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+m2+';color: #fff;">Peso normal</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img3f+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+m3+';color: #fff;">Sobrepeso</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img4f+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+m4+';color: #fff;">Obesidad grado 1</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img5f+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+m5+';color: #fff;">Obesidad grado 2</span> \
                    </div>\
                </div>\
                <div class="col-lg-2">\
                    <div align="center">\
                       <img style="height: 142px;" src="'+base_url+'images/imc/'+img6f+'"><br>\
                       <span class="badge badge-pill" style="background-color: '+m6+';color: #fff;">Obesidad grado 3</span> \
                    </div>\
                </div>\
            </div>     \
            <div class="row">\
                <div class="col-md-4"></div>\
                <div class="col-md-4"><br><br><br>\
                    <div class="card">\
                        <div class="box bg-megna text-center">\
                            <h1 class="font-light text-white"><span class="peso_idealm_txt">0</span></h1>\
                            <h6 class="text-white">Peso ideal</h6>\
                        </div>\
                    </div>\
                </div>\
        </div>';
    }    
    $('.nutricion_imc').html(html);
}

// Consulta de nutricion
function guardar_consulta_nutricion(){
    var datos = $('#form_consulta_nutricion').serialize()+'&imc='+$('.imc_n').text()+'&grasa_magra='+$('.masa_magra_txt').text()+'&agua='+
                $('.agua_txt').text()+'&grasa='+$('.grasa_txt').text()+'&peso_ideal='+$('.peso_idealh_txt').text()+'&indice_cintura_cadera='+
                $('.indice_cin_can_txt').text()+'&indice_cintura_cadera_porcentaje='+$('.indice_cintura_cadera_txt').text()+'&mifflin_st_jeor='+
                $('.mifflin_st_jeor_txt').text()+'&harris_benedict='+$('.harris_benedict_txt').text()+'&fao_oms_18_30='+
                $('.for1_txt').text()+'&fao_oms_30_60='+$('.for2_txt').text()+'&fao_oms_60='+$('.for3_txt').text()+'&fao_oms_20_kcal='+
                $('.kcal1_txt').text()+'&fao_oms_25_kcal='+$('.kcal2_txt').text()+'&fao_oms_30__kcal='+$('.kcal3_txt').text()+'&macro_carboidratos_gramo='+
                $('.car_gra_txt ').text()+'&macro_lipidos_gramos='+$('.lip_gra_txt').text()+'&macro_proteinas_gramos='+
                $('.pro_gra_txt').text();

    $('.btn_consulta_nutri').prop('disabled', true);
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/registra_consulta_nutricion',
        data: datos,
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            var idcli = data
            /// Venta de Servicios
            saveplan(idcli);
            var DATAT  = [];
            var TABLAT   = $("#table_servicio_estetica tbody > tr");
            TABLAT.each(function(){  
                    item = {};
                    item ["idconsulta"] = idcli;
                    item ["iddetalles"] = $(this).find("input[id*='iddetalles_c1']").val();
                    item ["idservicio"] = $(this).find("input[id*='idservicio_c1']").val();
                    item ["costo"] = $(this).find("input[id*='costo_c1']").val();
                    
                    DATAT.push(item);
            });
            INFOT  = new FormData();
            aInfot   = JSON.stringify(DATAT);
            INFOT.append('data', aInfot);
            $.ajax({
                data: INFOT, 
                type: 'POST',
                url : base_url + 'PacientesConsultas/registro_venta_servicios_nutricion',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            }); 
            //=====Ventas=====
            var DATAP  = [];
            var TABLAP   = $("#tabla_producto_venta_consultas tbody > tr");
            TABLAP.each(function(){  
                item = {};
                item ["idconsulta"] = idcli;
                item ["iddetalles"] = $(this).find("input[id*='iddetalles_c1']").val();
                item ["idalmacen"] = $(this).find("input[id*='idalmacen_c1']").val();
                item ["cantidad"] = $(this).find("input[id*='cantidad_c1']").val();
                item ["preciou"] = $(this).find("input[id*='preciou_c1']").val();
                item ["totalp"] = $(this).find("input[id*='total_c1']").val();

                DATAP.push(item);
            });
            INFOP  = new FormData();
            aInfop   = JSON.stringify(DATAP);
            INFOP.append('data', aInfop);
            $.ajax({
                data: INFOP, 
                type: 'POST',
                url : base_url + 'PacientesConsultas/registro_venta_producto_nutricion',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            }); 

            //==================
            $.toast({
                heading: 'Éxito',
                text: 'Se guardaron los cambios de forma exitosa',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            setTimeout(function(){ 
                window.location.reload();
            }, 1500);
        }
    });
}

function calcularimc(){
    var edad = $('#edadn').val();
    var ed = 0;
    if(edad==''){
        ed = 0;
    }else{
        ed = edad;
    }
    var e = parseFloat(ed);//edad
    var peso = $('#peson').val();
    
    var altura = $('#alturan').val();
    
    var cuello = $('#cuellon').val();

    
    var pe = 0;
    if(peso==''){
        pe = 0;
    }else{
        pe = peso;
    }
    var p = parseFloat(pe);//peso

    var al = 0;
    if(altura==''){
        al = 0;
    }else{
        al = altura;
    }
    var a = parseFloat(al);//Altura
    // Cuello
    var ce = 0;
    if(cuello==''){
        ce = 0;
    }else{
        ce = cuello;
    }
    var c = parseFloat(ce);// Cuello
    //
    var multi_altura = al * al;
    var suma = p / multi_altura;
   
    var conDecimal = Number(suma.toFixed(1));
    //alert(conDecimal);
    if(conDecimal==Infinity){
        $('.imc_n').html(0);
    }else{
        if(conDecimal==NaN){
            $('.imc_n').html(0);
        }else{
            $('.imc_n').html(conDecimal);
        }
        nutri_imc(conDecimal);
    }
    
    var sexo_text=$('#sexo_text').val();
    var peso_h='';/// Peso ideal 
    if(sexo_text==1){//hombre
        /// Peso ideal
        peso_i=23*multi_altura;
        var conDecimalh = Number(peso_i.toFixed(1));
        $('.peso_idealh_txt').html(conDecimalh);
        /*
        var gr1=1.0324-0.19077;
        var gr2=gr1;
        var gr_log1=89.0-Math.log();
        var gr=495/gr1;
        */
    }else{//Mujer
        /// Peso ideal 
        peso_i=21.5*multi_altura;
        var conDecimalm = Number(peso_i.toFixed(1));
        $('.peso_idealm_txt').html(conDecimalm);
        // Grasa 
    }
    /// Agua
    var cal_agua=0.033*pe;
    var cal_agua_decimal=cal_agua.toFixed(1);
    $('.agua_txt').html(cal_agua_decimal);
    /// Grasa 
    //console.log(c+' | '+a);
    /// cintura y alrtur
    var cintura = $('#cinturan').val();
    var cadera = $('#caderan').val();

    var ci = 0;
    if(cintura==''){
        ci = 0;
    }else{
        ci = cintura;
    }
    var cin = parseFloat(ci);//peso

    var ca = 0;
    if(cadera==''){
        ca = 0;
    }else{
        ca = cadera;
    }
    var can = parseFloat(ca);//peso
    /// calculo de indice cintura altura
    var suma_in_cin_can = cin/al;
    var suma_in_cin_can_dec=parseFloat(suma_in_cin_can);
    var suma_in_cin_can_deci=Number(suma_in_cin_can_dec.toFixed(1));
    $('.indice_cin_can_txt').html(suma_in_cin_can_deci);
    /// indice cintura cadena
    var sum_cadera=cin / can;
    var sum_caderan=parseFloat(sum_cadera);
    var sum_cadera_total = sum_caderan.toFixed(2);
    $('.indice_cintura_cadera_txt').html(sum_cadera_total);
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/get_grasa',
        data: {c13:cin,c14:can,c5:c,c4:a,tipo:sexo_text},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            var gr=parseFloat(data);
            var gr_d=gr.toFixed(2);
            if (isNaN(gr_d)) gr_d = 0;
            $('.grasa_txt').html(gr_d);
        }
    });
    setTimeout(function(){
        // masa magra   
            var mg_grasa=$('.grasa_txt').text();
            var mg_grasan=parseFloat(mg_grasa);
        if(sexo_text==1){  
            var resta_mg=100-mg_grasan;
            var mg=pe*resta_mg/100; 
            var mg_suma=mg.toFixed(2);
            $('.masa_magra_txt').html(mg_suma);
        }else{
            var resta_mg=100-mg_grasan;
            var mg=pe*resta_mg/100; 
            var mg_suma=mg.toFixed(2);
            $('.masa_magra_txt').html(mg_suma);
        }
        /////  Mifflin St-Jeor
        mifflin_st_jeor_y_harris_benedict(p,a,e,sexo_text);
        //// FAO/OMS / Fórmula
        fao_oms_formula(p);
        setTimeout(function(){
            var mifflin=$('.mifflin_st_jeor_txt').text();
            var harris=$('.harris_benedict_txt').text();
            fisica_mifflin_st_jeor(mifflin);
            fisica_harris_benedict(harris);
        }, 500);
    }, 1000);
}
function mifflin_st_jeor_y_harris_benedict(peso,altura,edad,sexo_text){
    if(sexo_text==1){  
            var s1= ((10*peso)+(6.25*altura))-(5*edad)+5;
            var miffs1=Number(parseFloat(s1.toFixed(0)));

            $('.mifflin_st_jeor_txt').html(miffs1);
        }else{
            var s1= ((10*peso)+(6.25*altura))-(5*edad)-161;
            var miffs1=Number(parseFloat(s1.toFixed(0)));

            $('.mifflin_st_jeor_txt').html(miffs1);
        }
        /// Harris-Benedict
        if(sexo_text==1){  
            var h1=(13.75*peso)+(5*altura)-(6.79*edad)+66
            var harr=Number(parseFloat(h1.toFixed(0)));
            $('.harris_benedict_txt').html(harr);
        }else{
            var har=(9.56*peso)+(1.85*altura)-(4.68*edad)+655;
            var harris=Number(parseFloat(har.toFixed(0)));
            $('.harris_benedict_txt').html(harris);
        }
}
//// FAO/OMS / Fórmula
function fao_oms_formula(peso){
        var for1=(15.3*peso)+679;
        var form1=Number(parseFloat(for1.toFixed(0)));
        $('.for1_txt').html(form1);
        // formula 2
        var for2=(11.6*peso)+879;
        var form2=Number(parseFloat(for2.toFixed(0)));
        $('.for2_txt').html(form2);
        // formula 3
        var for3=(13.5*peso)+489;
        var form3=Number(parseFloat(for3.toFixed(0)));
        $('.for3_txt').html(form3);
        /// kcal
        //kcal 1
        var kcal1=peso*20;
        var kcalk1=Number(parseFloat(kcal1.toFixed(0)));
        $('.kcal1_txt').html(kcalk1);
        // kcal 2
        var kcal2=25*peso;
        var kcalk2=Number(parseFloat(kcal2.toFixed(0)));
        $('.kcal2_txt').html(kcalk2);
        // kcal 3
        var kcal3=30*peso;
        var kcalk3=Number(parseFloat(kcal3.toFixed(0)));
        $('.kcal3_txt').html(kcalk3);
        /// formula de fao/oms
        setTimeout(function(){
            var for1_txt=$('.for1_txt').text();
            var for2_txt=$('.for2_txt').text();
            var for3_txt=$('.for3_txt').text();
            calculo_fao_oms_18(for1_txt);
            calculo_fao_oms_30(for2_txt);
            calculo_fao_oms_60(for3_txt);
        }, 500);
}
function btn_macronutrientes(){
    
    var carbohidratos=$('#carbohidratos').val();
    var lipidos=$('#lipidos').val();
    var proteina=$('#proteina').val();
    var die_kcal=parseFloat(dieta_kcal);
    var car=parseFloat(carbohidratos);
    var lip=parseFloat(lipidos);
    var pro=parseFloat(proteina);
    var sumar_total=parseFloat(car+lip+pro);
    if(sumar_total>100){
        $.toast({
            heading: 'Ateción!',
            text: 'sobrepasa el 100%',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }
}
function btn_macronutrientes_kcal(){
    var dieta_kcal=$('#dieta_kcal').val();
    var carbohidratos=$('#carbohidratos').val();
    var lipidos=$('#lipidos').val();
    var proteina=$('#proteina').val();
    var die_kcal=parseFloat(dieta_kcal);
    var car=parseFloat(carbohidratos);
    var lip=parseFloat(lipidos);
    var pro=parseFloat(proteina);
    /// Carbohidratos kcal
    var car_kcal=die_kcal*(car/100);
    $('.car_kcal_txt').html(car_kcal.toFixed(0));
    /// Lipidos kcal
    var lip_kcal=die_kcal*(lip/100);
    $('.lip_kcal_txt').html(lip_kcal.toFixed(0));
    /// Proteinas kcal
    var pro_kcal=die_kcal*(pro/100);
    $('.pro_kcal_txt').html(pro_kcal.toFixed(0));
    /// calculos de gramamos
    /// carboidratos
    var car_kcaln=$('.car_kcal_txt').text();
    var car_kcalnu=parseFloat(car_kcaln);
    var car_kcalt=car_kcalnu/4;
    $('.car_gra_txt').html(car_kcalt.toFixed(0));
    /// Lipidos
    var lip_kcaln=$('.lip_kcal_txt').text();
    var lip_kcalnu=parseFloat(lip_kcaln);
    var lip_kcalt=lip_kcalnu/9;
    $('.lip_gra_txt').html(lip_kcalt.toFixed(0));
    /// Proteinas
    var pro_kcaln=$('.pro_kcal_txt').text();
    var pro_kcalnu=parseFloat(pro_kcaln);
    var pro_kcalt=pro_kcalnu/4;
    $('.pro_gra_txt').html(pro_kcalt.toFixed(0)); 
}
/// Actividad Física
/// Mifflin St-Jeor
function fisica_mifflin_st_jeor(mifflin){
    //Encamado
    var en=1.1*mifflin;
    var enca=Number(parseFloat(en.toFixed(0)));
    $('.enc_mifflin').html(enca);
    //Leve/Sedentario
    var le=1.2*mifflin;
    var len=Number(parseFloat(le.toFixed(0)));
    $('.lev_mifflin').html(len);
    //Moderada
    var mo=1.3*mifflin;
    var mod=Number(parseFloat(mo.toFixed(0)));
    $('.mod_mifflin').html(mod);
    //Fuerte
    var fu=1.5*mifflin;
    var fue=Number(parseFloat(fu.toFixed(0)));
    $('.fue_mifflin').html(fue);
}
/// Harris-Benedict
function fisica_harris_benedict(harris){
    //Encamado
    var en=1.1*harris;
    var enca=Number(parseFloat(en.toFixed(0)));
    $('.enc_harris').html(enca);
    //Leve/Sedentario
    var le=1.2*harris;
    var len=Number(parseFloat(le.toFixed(0)));
    $('.lev_harris').html(len);
    //Moderada
    var mo=1.3*harris;
    var mod=Number(parseFloat(mo.toFixed(0)));
    $('.mod_harris').html(mod);
    //Fuerte
    var fu=1.5*harris;
    var fue=Number(parseFloat(fu.toFixed(0)));
    $('.fue_harris').html(fue);
}
function calculo_fao_oms_18(for1_txt){
    //Sedentario
    var se=1.3*for1_txt;
    var sed=Number(parseFloat(se.toFixed(0)));
    $('.sed_18').html(sed);
    //Ligera
    var li=1.5*for1_txt;
    var lig=Number(parseFloat(li.toFixed(0)));
    $('.lig_18').html(lig);
    //Moderada
    var mo=1.6*for1_txt;
    var mod=Number(parseFloat(mo.toFixed(0)));
    $('.mod_18').html(mod);
    //Alta
    var al=1.9*for1_txt;
    var alt=Number(parseFloat(al.toFixed(0)));
    $('.alt_18').html(alt);
    //Muy Alta
    var mu=2.2*for1_txt;
    var muy=Number(parseFloat(mu.toFixed(0)));
    $('.muy_18').html(muy);
}
function calculo_fao_oms_30(for2_txt){
    //Sedentario
    var se=1.3*for2_txt;
    var sed=Number(parseFloat(se.toFixed(0)));
    $('.sed_30').html(sed);
    //Ligera
    var li=1.5*for2_txt;
    var lig=Number(parseFloat(li.toFixed(0)));
    $('.lig_30').html(lig);
    //Moderada
    var mo=1.6*for2_txt;
    var mod=Number(parseFloat(mo.toFixed(0)));
    $('.mod_30').html(mod);
    //Alta
    var al=1.9*for2_txt;
    var alt=Number(parseFloat(al.toFixed(0)));
    $('.alt_30').html(alt);
    //Muy Alta
    var mu=2.2*for2_txt;
    var muy=Number(parseFloat(mu.toFixed(0)));
    $('.muy_30').html(muy);
}
function calculo_fao_oms_60(for3_txt){
    //Sedentario
    var se=1.3*for3_txt;
    var sed=Number(parseFloat(se.toFixed(0)));
    $('.sed_60').html(sed);
    //Ligera
    var li=1.5*for3_txt;
    var lig=Number(parseFloat(li.toFixed(0)));
    $('.lig_60').html(lig);
    //Moderada
    var mo=1.6*for3_txt;
    var mod=Number(parseFloat(mo.toFixed(0)));
    $('.mod_60').html(mod);
    //Alta
    var al=1.9*for3_txt;
    var alt=Number(parseFloat(al.toFixed(0)));
    $('.alt_60').html(alt);
    //Muy Alta
    var mu=2.2*for3_txt;
    var muy=Number(parseFloat(mu.toFixed(0)));
    $('.muy_60').html(muy);
}
function ocultar_nutricion1(){
    $('.nutricion_text_1').css('display','none');
    $('.nutricion_text_2').css('display','block');
}
function ocultar_nutricion2(){
    $('.nutricion_text_1').css('display','block');
    $('.nutricion_text_2').css('display','none');
}
function calcular_dieta(){
    var aporte1=$('#aporte1').val();
    var aporte2=$('#aporte2').val();
    var aporte3=$('#aporte3').val();
    var aporte4=$('#aporte4').val();
    var aporte5=$('#aporte5').val();
    var aporte6=$('#aporte6').val();
    var aporte7=$('#aporte7').val();
    var aporte8=$('#aporte8').val();
    var aporte9=$('#aporte9').val();
    var aporte10=$('#aporte10').val();
    var aporte11=$('#aporte11').val();
    var aporte12=$('#aporte12').val();
    var aporte13=$('#aporte13').val();
    var aporte14=$('#aporte14').val();
    var aporte15=$('#aporte15').val();
    var aporte16=$('#aporte16').val();
    var aporte17=$('#aporte17').val();
    var aporte18=$('#aporte18').val();
    var aporte19=$('#aporte19').val();
    //////////////////////////////////
    var car_gra_txt=$('.car_gra_txt').text();
    var lip_gra_txt=$('.lip_gra_txt').text();
    var pro_gra_txt=$('.pro_gra_txt').text();
    $('.total_proteina_gramos').html(pro_gra_txt);
    $('.total_lipidos_gramos').html(lip_gra_txt);
    $('.total_carboidratos_gramos').html(car_gra_txt);
    
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/calculo_aporte_nutrimental_dieta',
        data: {
            dieta1:aporte1,
            dieta2:aporte2,
            dieta3:aporte3,
            dieta4:aporte4,
            dieta5:aporte5,
            dieta6:aporte6,
            dieta7:aporte7,
            dieta8:aporte8,
            dieta9:aporte9,
            dieta10:aporte10,
            dieta11:aporte11,
            dieta12:aporte12,
            dieta13:aporte13,
            dieta14:aporte14,
            dieta15:aporte15,
            dieta16:aporte16,
            dieta17:aporte17,
            dieta18:aporte18,
            dieta19:aporte19
            },
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            var array=$.parseJSON(data);
            $('.ene1_tx').html(array.cal_energia1);
            $('.pro1_tx').html(array.cal_proteina1);
            $('.lip1_tx').html(array.cal_lipidos1);
            $('.car1_tx').html(array.cal_carbohidratos1);
            $('.sod1_tx').html(array.cal_sodio1);
            $('.fib1_tx').html(array.cal_fibra1);

            $('.ene2_tx').html(array.cal_energia2);
            $('.pro2_tx').html(array.cal_proteina2);
            $('.lip2_tx').html(array.cal_lipidos2);
            $('.car2_tx').html(array.cal_carbohidratos2);
            $('.sod2_tx').html(array.cal_sodio2);
            $('.fib2_tx').html(array.cal_fibra2);

            $('.ene3_tx').html(array.cal_energia3);
            $('.pro3_tx').html(array.cal_proteina3);
            $('.lip3_tx').html(array.cal_lipidos3);
            $('.car3_tx').html(array.cal_carbohidratos3);
            $('.sod3_tx').html(array.cal_sodio3);
            $('.fib3_tx').html(array.cal_fibra3);

            $('.ene4_tx').html(array.cal_energia4);
            $('.pro4_tx').html(array.cal_proteina4);
            $('.lip4_tx').html(array.cal_lipidos4);
            $('.car4_tx').html(array.cal_carbohidratos4);
            $('.sod4_tx').html(array.cal_sodio4);
            $('.fib4_tx').html(array.cal_fibra4);

            $('.ene5_tx').html(array.cal_energia5);
            $('.pro5_tx').html(array.cal_proteina5);
            $('.lip5_tx').html(array.cal_lipidos5);
            $('.car5_tx').html(array.cal_carbohidratos5);
            $('.sod5_tx').html(array.cal_sodio5);
            $('.fib5_tx').html(array.cal_fibra5);

            $('.ene6_tx').html(array.cal_energia6);
            $('.pro6_tx').html(array.cal_proteina6);
            $('.lip6_tx').html(array.cal_lipidos6);
            $('.car6_tx').html(array.cal_carbohidratos6);
            $('.sod6_tx').html(array.cal_sodio6);
            $('.fib6_tx').html(array.cal_fibra6);

            $('.ene7_tx').html(array.cal_energia7);
            $('.pro7_tx').html(array.cal_proteina7);
            $('.lip7_tx').html(array.cal_lipidos7);
            $('.car7_tx').html(array.cal_carbohidratos7);
            $('.sod7_tx').html(array.cal_sodio7);
            $('.fib7_tx').html(array.cal_fibra7);

            $('.ene8_tx').html(array.cal_energia8);
            $('.pro8_tx').html(array.cal_proteina8);
            $('.lip8_tx').html(array.cal_lipidos8);
            $('.car8_tx').html(array.cal_carbohidratos8);
            $('.sod8_tx').html(array.cal_sodio8);
            $('.fib8_tx').html(array.cal_fibra8);

            $('.ene9_tx').html(array.cal_energia9);
            $('.pro9_tx').html(array.cal_proteina9);
            $('.lip9_tx').html(array.cal_lipidos9);
            $('.car9_tx').html(array.cal_carbohidratos9);
            $('.sod9_tx').html(array.cal_sodio9);
            $('.fib9_tx').html(array.cal_fibra9);

            $('.ene10_tx').html(array.cal_energia10);
            $('.pro10_tx').html(array.cal_proteina10);
            $('.lip10_tx').html(array.cal_lipidos10);
            $('.car10_tx').html(array.cal_carbohidratos10);
            $('.sod10_tx').html(array.cal_sodio10);
            $('.fib10_tx').html(array.cal_fibra10);

            $('.ene11_tx').html(array.cal_energia11);
            $('.pro11_tx').html(array.cal_proteina11);
            $('.lip11_tx').html(array.cal_lipidos11);
            $('.car11_tx').html(array.cal_carbohidratos11);
            $('.sod11_tx').html(array.cal_sodio11);
            $('.fib11_tx').html(array.cal_fibra11);

            $('.ene12_tx').html(array.cal_energia12);
            $('.pro12_tx').html(array.cal_proteina12);
            $('.lip12_tx').html(array.cal_lipidos12);
            $('.car12_tx').html(array.cal_carbohidratos12);
            $('.sod12_tx').html(array.cal_sodio12);
            $('.fib12_tx').html(array.cal_fibra12);

            $('.ene13_tx').html(array.cal_energia13);
            $('.pro13_tx').html(array.cal_proteina13);
            $('.lip13_tx').html(array.cal_lipidos13);
            $('.car13_tx').html(array.cal_carbohidratos13);
            $('.sod13_tx').html(array.cal_sodio13);
            $('.fib13_tx').html(array.cal_fibra13);

            $('.ene14_tx').html(array.cal_energia14);
            $('.pro14_tx').html(array.cal_proteina14);
            $('.lip14_tx').html(array.cal_lipidos14);
            $('.car14_tx').html(array.cal_carbohidratos14);
            $('.sod14_tx').html(array.cal_sodio14);
            $('.fib14_tx').html(array.cal_fibra14);

            $('.ene15_tx').html(array.cal_energia15);
            $('.pro15_tx').html(array.cal_proteina15);
            $('.lip15_tx').html(array.cal_lipidos15);
            $('.car15_tx').html(array.cal_carbohidratos15);
            $('.sod15_tx').html(array.cal_sodio15);
            $('.fib15_tx').html(array.cal_fibra15);

            $('.ene16_tx').html(array.cal_energia16);
            $('.pro16_tx').html(array.cal_proteina16);
            $('.lip16_tx').html(array.cal_lipidos16);
            $('.car16_tx').html(array.cal_carbohidratos16);
            $('.sod16_tx').html(array.cal_sodio16);
            $('.fib16_tx').html(array.cal_fibra16);

            $('.ene17_tx').html(array.cal_energia17);
            $('.pro17_tx').html(array.cal_proteina17);
            $('.lip17_tx').html(array.cal_lipidos17);
            $('.car17_tx').html(array.cal_carbohidratos17);
            $('.sod17_tx').html(array.cal_sodio17);
            $('.fib17_tx').html(array.cal_fibra17);

            $('.ene18_tx').html(array.cal_energia18);
            $('.pro18_tx').html(array.cal_proteina18);
            $('.lip18_tx').html(array.cal_lipidos18);
            $('.car18_tx').html(array.cal_carbohidratos18);
            $('.sod18_tx').html(array.cal_sodio18);
            $('.fib18_tx').html(array.cal_fibra18);

            $('.ene19_tx').html(array.cal_energia19);
            $('.pro19_tx').html(array.cal_proteina19);
            $('.lip19_tx').html(array.cal_lipidos19);
            $('.car19_tx').html(array.cal_carbohidratos19);
            $('.sod19_tx').html(array.cal_sodio19);
            $('.fib19_tx').html(array.cal_fibra19);
            setTimeout(function(){
                var suma_sodio = parseFloat($('.sod1_tx').text())+
                                 parseFloat($('.sod2_tx').text())+
                                 parseFloat($('.sod3_tx').text())+
                                 parseFloat($('.sod4_tx').text())+
                                 parseFloat($('.sod5_tx').text())+
                                 parseFloat($('.sod6_tx').text())+
                                 parseFloat($('.sod7_tx').text())+
                                 parseFloat($('.sod8_tx').text())+
                                 parseFloat($('.sod9_tx').text())+
                                 parseFloat($('.sod10_tx').text())+
                                 parseFloat($('.sod11_tx').text())+
                                 parseFloat($('.sod12_tx').text())+
                                 parseFloat($('.sod13_tx').text())+
                                 parseFloat($('.sod14_tx').text())+
                                 parseFloat($('.sod15_tx').text())+
                                 parseFloat($('.sod16_tx').text())+
                                 parseFloat($('.sod17_tx').text())+
                                 parseFloat($('.sod18_tx').text())+
                                 parseFloat($('.sod19_tx').text());
                $('.total_sodio').html(suma_sodio);  
                var suma_fibra = parseFloat($('.fib1_tx').text())+
                                 parseFloat($('.fib2_tx').text())+
                                 parseFloat($('.fib3_tx').text())+
                                 parseFloat($('.fib4_tx').text())+
                                 parseFloat($('.fib5_tx').text())+
                                 parseFloat($('.fib6_tx').text())+
                                 parseFloat($('.fib7_tx').text())+
                                 parseFloat($('.fib8_tx').text())+
                                 parseFloat($('.fib9_tx').text())+
                                 parseFloat($('.fib10_tx').text())+
                                 parseFloat($('.fib11_tx').text())+
                                 parseFloat($('.fib12_tx').text())+
                                 parseFloat($('.fib13_tx').text())+
                                 parseFloat($('.fib14_tx').text())+
                                 parseFloat($('.fib15_tx').text())+
                                 parseFloat($('.fib16_tx').text())+
                                 parseFloat($('.fib17_tx').text())+
                                 parseFloat($('.fib18_tx').text())+
                                 parseFloat($('.fib19_tx').text());
                $('.total_fibra').html(suma_fibra);  
                var suma_carboidratos = parseFloat($('.car1_tx').text())+
                                 parseFloat($('.car2_tx').text())+
                                 parseFloat($('.car3_tx').text())+
                                 parseFloat($('.car4_tx').text())+
                                 parseFloat($('.car5_tx').text())+
                                 parseFloat($('.car6_tx').text())+
                                 parseFloat($('.car7_tx').text())+
                                 parseFloat($('.car8_tx').text())+
                                 parseFloat($('.car9_tx').text())+
                                 parseFloat($('.car10_tx').text())+
                                 parseFloat($('.car11_tx').text())+
                                 parseFloat($('.car12_tx').text())+
                                 parseFloat($('.car13_tx').text())+
                                 parseFloat($('.car14_tx').text())+
                                 parseFloat($('.car15_tx').text())+
                                 parseFloat($('.car16_tx').text())+
                                 parseFloat($('.car17_tx').text())+
                                 parseFloat($('.car18_tx').text())+
                                 parseFloat($('.car19_tx').text());
                $('.total_carboidratos').html(suma_carboidratos);  
                var suma_lipidos = parseFloat($('.lip1_tx').text())+
                                 parseFloat($('.lip2_tx').text())+
                                 parseFloat($('.lip3_tx').text())+
                                 parseFloat($('.lip4_tx').text())+
                                 parseFloat($('.lip5_tx').text())+
                                 parseFloat($('.lip6_tx').text())+
                                 parseFloat($('.lip7_tx').text())+
                                 parseFloat($('.lip8_tx').text())+
                                 parseFloat($('.lip9_tx').text())+
                                 parseFloat($('.lip10_tx').text())+
                                 parseFloat($('.lip11_tx').text())+
                                 parseFloat($('.lip12_tx').text())+
                                 parseFloat($('.lip13_tx').text())+
                                 parseFloat($('.lip14_tx').text())+
                                 parseFloat($('.lip15_tx').text())+
                                 parseFloat($('.lip16_tx').text())+
                                 parseFloat($('.lip17_tx').text())+
                                 parseFloat($('.lip18_tx').text())+
                                 parseFloat($('.lip19_tx').text());
                $('.total_lipidos').html(suma_lipidos);  
                var suma_proteina = parseFloat($('.pro1_tx').text())+
                                 parseFloat($('.pro2_tx').text())+
                                 parseFloat($('.pro3_tx').text())+
                                 parseFloat($('.pro4_tx').text())+
                                 parseFloat($('.pro5_tx').text())+
                                 parseFloat($('.pro6_tx').text())+
                                 parseFloat($('.pro7_tx').text())+
                                 parseFloat($('.pro8_tx').text())+
                                 parseFloat($('.pro9_tx').text())+
                                 parseFloat($('.pro10_tx').text())+
                                 parseFloat($('.pro11_tx').text())+
                                 parseFloat($('.pro12_tx').text())+
                                 parseFloat($('.pro13_tx').text())+
                                 parseFloat($('.pro14_tx').text())+
                                 parseFloat($('.pro15_tx').text())+
                                 parseFloat($('.pro16_tx').text())+
                                 parseFloat($('.pro17_tx').text())+
                                 parseFloat($('.pro18_tx').text())+
                                 parseFloat($('.pro19_tx').text());
                $('.total_proteina').html(suma_proteina);
                var total2=parseFloat(suma_proteina);
                var meta_total2=parseFloat(pro_gra_txt);
                calcular_porcentaje_educacion2(total2,meta_total2);
                var total3=parseFloat(suma_lipidos);
                var meta_total3=parseFloat(lip_gra_txt);
                calcular_porcentaje_educacion3(total3,meta_total3);
                var total4=parseFloat(suma_carboidratos);
                var meta_total4=parseFloat(car_gra_txt);
                calcular_porcentaje_educacion4(total4,meta_total4);
                //  Cuantos g FALTAN
                calcular_cuantos_faltan(pro_gra_txt,suma_proteina);
                calcular_cuantos2_faltan(lip_gra_txt,suma_lipidos);
                calcular_cuantos3_faltan(car_gra_txt,suma_carboidratos);


            }, 500);
        }
    });
    //// Aporte nutrimental información 2
    $('.aportext1').html(aporte1);
    $('.aportext2').html(aporte2);
    $('.aportext3').html(aporte3);
    $('.aportext4').html(aporte4);
    $('.aportext5').html(aporte5);
    $('.aportext6').html(aporte6);
    $('.aportext7').html(aporte7);
    $('.aportext8').html(aporte8);
    $('.aportext9').html(aporte9);
    $('.aportext10').html(aporte10);
    $('.aportext11').html(aporte11);
    $('.aportext12').html(aporte12);
    $('.aportext13').html(aporte13);
    $('.aportext14').html(aporte14);
    $('.aportext15').html(aporte15);
    $('.aportext16').html(aporte16);
    $('.aportext17').html(aporte17);
    $('.aportext18').html(aporte18);
    $('.aportext19').html(aporte19);
    /// operadores de nutrimental de comida
    calcular_verdura();
    calcular_fruta();
    calcular_cereals();
    calcular_cerealc();
    calcular_leguminosa();
    calcular_alimentos_muy_bajo();
    calcular_alimentos_bajo();
    calcular_alimentos_moderado();
    calcular_alimentos_alto_aporte();
    calcular_leche_descremada();
    calcular_leche_semi();
    calcular_leche_entera();
    calcular_leche_con_azucar();
    calcular_aceite_sin_proteina();
    calcular_aceite_con_proteina();
    calcular_azucares_sin_grasa();
    calcular_azucares_con_grasa();
    calcular_alimentos_libres();
    calcular_bebidas_alcholicas();
}

function calcular_calorias(){
    var calorias = $('#total_calorias').val();
    var total_meta = $('#total_meta').val();
    $('.total_calori_txt').html(calorias);
    $('.total_meta_txt').html(total_meta);   
    var total=parseFloat(calorias);
    var meta_total=parseFloat(total_meta);
    calcular_porcentaje_educacion(total,meta_total);
}
function calcular_porcentaje_educacion(total,meta_total){
    var opera=(total*100)/meta_total;
    var decimal=opera.toFixed(0);
    $('.total_porcentaje_txt').html(decimal);
}
function calcular_porcentaje_educacion2(total,meta_total){
    var opera=(total*100)/meta_total;
    var decimal=opera.toFixed(0);
    $('.total_porcentaje2_txt').html(decimal);
}
function calcular_porcentaje_educacion3(total,meta_total){
    var opera=(total*100)/meta_total;
    var decimal=opera.toFixed(0);
    $('.total_porcentaje3_txt').html(decimal);
}
function calcular_porcentaje_educacion4(total,meta_total){
    var opera=(total*100)/meta_total;
    var decimal=opera.toFixed(0);
    $('.total_porcentaje4_txt').html(decimal);
}
/// Cuantos g FALTAN
function calcular_cuantos_faltan(meta_total,total){
    var opera=meta_total-total;
    var decimal=opera.toFixed(0);
    $('.total_cuantos_faltan1_txt').html(decimal);
    calcular_no_raciones(decimal);
}
function calcular_cuantos2_faltan(meta_total,total){
    var opera=meta_total-total;
    var decimal=opera.toFixed(0);
    $('.total_cuantos_faltan2_txt').html(decimal);
    calcular_no_raciones2(decimal);
}
function calcular_cuantos3_faltan(meta_total,total){
    var opera=meta_total-total;
    var decimal=opera.toFixed(0);
    $('.total_cuantos_faltan3_txt').html(decimal);
    calcular_no_raciones3(decimal);
}
function calcular_no_raciones(porcentaje){
    var opera=porcentaje/7;
    var decimal=opera.toFixed(1);
    $('.total_no_raciones_txt').html(decimal);
}
function calcular_no_raciones2(porcentaje){
    var opera=porcentaje/5;
    var decimal=opera.toFixed(1);
    $('.total_no_raciones2_txt').html(decimal);
}
function calcular_no_raciones3(porcentaje){
    var opera=porcentaje/15;
    var decimal=opera.toFixed(1);
    $('.total_no_raciones3_txt').html(decimal);
}
function calcular_verdura(){
    var op1 = $('#ver1').val();
    var op2 = $('#ver2').val();
    var op3 = $('#ver3').val();
    var op4 = $('#ver4').val();
    var op5 = $('#ver5').val();
    var op6 = $('#ver6').val();
    var op7 = $('#ver7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext1').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.verdura_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.verdura_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_verdura').html(icono);
}
function calcular_fruta(){
    var op1 = $('#fru1').val();
    var op2 = $('#fru2').val();
    var op3 = $('#fru3').val();
    var op4 = $('#fru4').val();
    var op5 = $('#fru5').val();
    var op6 = $('#fru6').val();
    var op7 = $('#fru7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext2').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.fruta_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.fruta_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_fruta').html(icono);
}
function calcular_cereals(){
    var op1 = $('#ces1').val();
    var op2 = $('#ces2').val();
    var op3 = $('#ces3').val();
    var op4 = $('#ces4').val();
    var op5 = $('#ces5').val();
    var op6 = $('#ces6').val();
    var op7 = $('#ces7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext3').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.cerials_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.cerials_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_cerials').html(icono);
}
function calcular_cerealc(){
    var op1 = $('#cec1').val();
    var op2 = $('#cec2').val();
    var op3 = $('#cec3').val();
    var op4 = $('#cec4').val();
    var op5 = $('#cec5').val();
    var op6 = $('#cec6').val();
    var op7 = $('#cec7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext4').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.cerialc_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.cerialc_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_cerialc').html(icono);
}
function calcular_leguminosa(){
    var op1 = $('#leg1').val();
    var op2 = $('#leg2').val();
    var op3 = $('#leg3').val();
    var op4 = $('#leg4').val();
    var op5 = $('#leg5').val();
    var op6 = $('#leg6').val();
    var op7 = $('#leg7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext5').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.leguminosa_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.leguminosa_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_leguminosa').html(icono);
}
function calcular_alimentos_muy_bajo(){
    var op1 = $('#amu1').val();
    var op2 = $('#amu2').val();
    var op3 = $('#amu3').val();
    var op4 = $('#amu4').val();
    var op5 = $('#amu5').val();
    var op6 = $('#amu6').val();
    var op7 = $('#amu7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext6').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.alimentos_muy_bajo_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.alimentos_muy_bajo_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_alimentos_muy_bajo').html(icono);
}
function calcular_alimentos_bajo(){
    var op1 = $('#aba1').val();
    var op2 = $('#aba2').val();
    var op3 = $('#aba3').val();
    var op4 = $('#aba4').val();
    var op5 = $('#aba5').val();
    var op6 = $('#aba6').val();
    var op7 = $('#aba7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext7').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.alimentos_bajo_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.alimentos_bajo_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_alimentos_bajo').html(icono);
}
function calcular_alimentos_moderado(){
    var op1 = $('#amo1').val();
    var op2 = $('#amo2').val();
    var op3 = $('#amo3').val();
    var op4 = $('#amo4').val();
    var op5 = $('#amo5').val();
    var op6 = $('#amo6').val();
    var op7 = $('#amo7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext8').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.alimentos_moderado_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.alimentos_moderado_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_moderado_bajo').html(icono);
}
function calcular_alimentos_alto_aporte(){
    var op1 = $('#aal1').val();
    var op2 = $('#aal2').val();
    var op3 = $('#aal3').val();
    var op4 = $('#aal4').val();
    var op5 = $('#aal5').val();
    var op6 = $('#aal6').val();
    var op7 = $('#aal7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext9').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.ali_alto_aporte_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.ali_alto_aporte_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_ali_alto_aporte').html(icono);
}
function calcular_leche_descremada(){
    var op1 = $('#led1').val();
    var op2 = $('#led2').val();
    var op3 = $('#led3').val();
    var op4 = $('#led4').val();
    var op5 = $('#led5').val();
    var op6 = $('#led6').val();
    var op7 = $('#led7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext10').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.leche_descremada_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.leche_descremada_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_leche_descremada').html(icono);
}
function calcular_leche_semi(){
    var op1 = $('#les1').val();
    var op2 = $('#les2').val();
    var op3 = $('#les3').val();
    var op4 = $('#les4').val();
    var op5 = $('#les5').val();
    var op6 = $('#les6').val();
    var op7 = $('#les7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext11').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.leche_semi_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.leche_semi_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_leche_semi').html(icono);
}
function calcular_leche_entera(){
    var op1 = $('#lee1').val();
    var op2 = $('#lee2').val();
    var op3 = $('#lee3').val();
    var op4 = $('#lee4').val();
    var op5 = $('#lee5').val();
    var op6 = $('#lee6').val();
    var op7 = $('#lee7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext12').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.leche_entera_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.leche_entera_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_leche_entera').html(icono);
}
function calcular_leche_con_azucar(){
    var op1 = $('#lec1').val();
    var op2 = $('#lec2').val();
    var op3 = $('#lec3').val();
    var op4 = $('#lec4').val();
    var op5 = $('#lec5').val();
    var op6 = $('#lec6').val();
    var op7 = $('#lec7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext13').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.leche_con_azucar_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.leche_con_azucar_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_leche_con_azucar').html(icono);
}
function calcular_aceite_sin_proteina(){
    var op1 = $('#acs1').val();
    var op2 = $('#acs2').val();
    var op3 = $('#acs3').val();
    var op4 = $('#acs4').val();
    var op5 = $('#acs5').val();
    var op6 = $('#acs6').val();
    var op7 = $('#acs7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext14').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.aceite_sin_proteina_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.aceite_sin_proteina_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_aceite_sin_proteina').html(icono);
}
function calcular_aceite_con_proteina(){
    var op1 = $('#acc1').val();
    var op2 = $('#acc2').val();
    var op3 = $('#acc3').val();
    var op4 = $('#acc4').val();
    var op5 = $('#acc5').val();
    var op6 = $('#acc6').val();
    var op7 = $('#acc7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext15').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.aceite_con_proteina_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.aceite_con_proteina_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_aceite_con_proteina').html(icono);
}
function calcular_azucares_sin_grasa(){
    var op1 = $('#azs1').val();
    var op2 = $('#azs2').val();
    var op3 = $('#azs3').val();
    var op4 = $('#azs4').val();
    var op5 = $('#azs5').val();
    var op6 = $('#azs6').val();
    var op7 = $('#azs7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext16').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.azucares_sin_grasa_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.azucares_sin_grasa_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_azucares_sin_grasa').html(icono);
}
function calcular_azucares_con_grasa(){
    var op1 = $('#azc1').val();
    var op2 = $('#azc2').val();
    var op3 = $('#azc3').val();
    var op4 = $('#azc4').val();
    var op5 = $('#azc5').val();
    var op6 = $('#azc6').val();
    var op7 = $('#azc7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext17').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.azucares_con_grasa_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.azucares_con_grasa_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_azucares_con_grasa').html(icono);
}
function calcular_alimentos_libres(){
    var op1 = $('#ali1').val();
    var op2 = $('#ali2').val();
    var op3 = $('#ali3').val();
    var op4 = $('#ali4').val();
    var op5 = $('#ali5').val();
    var op6 = $('#ali6').val();
    var op7 = $('#ali7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext18').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.alimentos_libres_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.alimentos_libres_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_alimentos_libres').html(icono);
}
function calcular_bebidas_alcholicas(){
    var op1 = $('#bed1').val();
    var op2 = $('#bed2').val();
    var op3 = $('#bed3').val();
    var op4 = $('#bed4').val();
    var op5 = $('#bed5').val();
    var op6 = $('#bed6').val();
    var op7 = $('#bed7').val();
    var opd1=parseFloat(op1);
    if (isNaN(opd1)) opd1 = 0;
    var opd2=parseFloat(op2);
    if (isNaN(opd2)) opd2 = 0;
    var opd3=parseFloat(op3);
    if (isNaN(opd3)) opd3 = 0;
    var opd4=parseFloat(op4);
    if (isNaN(opd4)) opd4 = 0;
    var opd5=parseFloat(op5);
    if (isNaN(opd5)) opd5 = 0;
    var opd6=parseFloat(op6);
    if (isNaN(opd6)) opd6 = 0;
    var opd7=parseFloat(op7);
    if (isNaN(opd7)) opd7 = 0;
    var aporte= $('.aportext19').text();
    var aporten=parseFloat(aporte);
    var cal = (aporten)-(opd1+opd2+opd3+opd4+opd5+opd6+opd7);
    var decimal=cal.toFixed(1);
    var icono='';
    if(cal==0){
        $('.bebidas_acl_color').css('background-color','#8bc34a');
        icono='<span><i class="fas fa-check"></i></span>';
    }else{ 
        $('.bebidas_acl_color').css('background-color','red');
        icono='<span><i class="fas fa-times"></i></span>';
    }
    $('.icono_bebidas_acl').html(icono);
}
/*
function modal_btn_nutricion_servicios_especificos(){
    $('#modal_nutricion_servicios_especificos').modal();
}

function modal_btn_nutricion_productos_especificos(){
    $('#modal_nutricion_productos_especificos').modal();
}
*/
function modal_btn_nutricion_plan_alimenticios(){
    $('#modal_nutricion_plan_alimenticio').modal();
    procesarplanalimenticio();
}
function modal_cotrol_sesiones_me(id){
    $('.imprimir_sesiones_estetica').html('<div class="row">\
            <div class="col-md-12">\
                <button type="button" class="btn waves-effect waves-light btn-info btn_consulta"\
                 onclick="imprimir_sesion_estetica('+id+')"><i class="ti-printer"></i> Imprimir</button>\
            </div>\
        </div>  ');
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/get_servicios_consulta_medicina_estetica',
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.texto_control_sesiones_c1').html(data);
        }
    });

    $('#modal_cotrol_sesion_me').modal();

}
var row_num_sess_me=1;
function addsess(id_aux,idservicio,idc,cantidad){
    var fecha_me = $('#fecha_me'+id_aux).val();
    var obser_me = $('#obser_me'+id_aux).val();
    if(fecha_me!='' &&  obser_me!=''){
        $.ajax({
            type:'POST',
            url: base_url+'PacientesConsultas/add_sesiones_estetica',
            data: {idconsulta:idc,
                    idservicio:idservicio,
                    fecha:fecha_me,
                    observacion:obser_me},
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $('#fecha_me'+id_aux).val('');
                $('#obser_me'+id_aux).val('');
            }
        });        
    }else{
        $.toast({
            heading: '¡Atención!',
            text: 'Hace falta llenar el campo de fecha o el de obervaciones',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }  
    setTimeout(function(){
        tabla_estetica_get(id_aux,idservicio,idc,cantidad);  
    }, 500);    
}

function tabla_estetica_get(id_aux,idservicio,idc,cantidad){
    setTimeout(function(){
        text_total_estetica(id_aux,idservicio,idc,cantidad);
    }, 1000);
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/get_tabla_control_sesiones_estetica',
        data:{idc:idc,ids:idservicio},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.tabla_sesiones_estetica_'+id_aux).html(data);
        }
    });
}

function procesarplanalimenticio(){
    var desayunotitle=$(".grupo_plan_1").val()==''?'Desayuno':$(".grupo_plan_1").val();
    var html='<tr class="title_plan"><th>'+desayunotitle+'<input type="hidden" value="<hr><strong>'+desayunotitle+'</strong><hr>" class="inputplan" readonly></th></tr>';
    $('.tbodytablegeneraplan_l').html(html);
    $('.tbodytablegeneraplan_m').html(html);
    $('.tbodytablegeneraplan_mi').html(html);
    $('.tbodytablegeneraplan_j').html(html);
    $('.tbodytablegeneraplan_v').html(html);
  
    //==========1================================
        if ($('#ver1').val()>0) {
            var cant =$('#ver1').val();
            verdura(cant);
        }
        if ($('#fru1').val()>0) {
            var cant =$('#fru1').val();
            fruta(cant);
        }
        //==========================================
        var cerales1 = 0;
        $(".cerales1").each(function() {
            var vstotal = $(this).val();
            cerales1 += Number(vstotal);
        });
        if (cerales1>0) {
            cerales(cerales1);
        }
        //==========================================
        if ($('#leg1').val()>0) {
            var cant =$('#leg1').val();
            leguminosas(cant);
        }
        //==========================================
        var aoa1 = 0;
        $(".aoa1").each(function() {
            var vstotal = $(this).val();
            aoa1 += Number(vstotal);
        });
        if (aoa1>0) {
            aoal(aoa1);
        }
        //==========================================
        var leyog1 = 0;
        $(".leyog1").each(function() {
            var vstotal = $(this).val();
            leyog1 += Number(vstotal);
        });
        if (leyog1>0) {
            leyog(leyog1);
        }
        //==========================================
        if ($('#acs1').val()>0) {
            var cant =$('#acs1').val();
            acs(cant);
        }
        //==========================================
        if ($('#acc1').val()>0) {
            var cant =$('#acc1').val();
            acc(cant);
        }
        //==========================================
        var azucara1 = 0;
        $(".azucara1").each(function() {
            var vstotal = $(this).val();
            azucara1 += Number(vstotal);
        });
        if (azucara1>0) {
            azucara(azucara1);
        }
    //==========================================
    setTimeout(function(){ 
        var desayunotitle=$(".grupo_plan_2").val()==''?'Colacion 1':$(".grupo_plan_2").val();
        var html='<tr class="title_plan"><th>'+desayunotitle+'<input type="hidden" value="<hr><strong></strong><hr>" class="inputplan" readonly></th></tr>'; 
        $('.tbodytablegeneraplan_l').append(html);
        $('.tbodytablegeneraplan_m').append(html);
        $('.tbodytablegeneraplan_mi').append(html);
        $('.tbodytablegeneraplan_j').append(html);
        $('.tbodytablegeneraplan_v').append(html);
        //==========2================================
            if ($('#ver2').val()>0) {
                var cant =$('#ver2').val();
                verdura(cant);
            }
            if ($('#fru2').val()>0) {
                var cant =$('#fru2').val();
                fruta(cant);
            }
            //==========================================
            var cerales2 = 0;
            $(".cerales2").each(function() {
                var vstotal = $(this).val();
                cerales2 += Number(vstotal);
            });
            if (cerales2>0) {
                cerales(cerales2);
            }
            //==========================================
            if ($('#leg2').val()>0) {
                var cant =$('#leg2').val();
                leguminosas(cant);
            }
            //==========================================
            var aoa2 = 0;
            $(".aoa2").each(function() {
                var vstotal = $(this).val();
                aoa2 += Number(vstotal);
            });
            if (aoa2>0) {
                aoal(aoa2);
            }
            //==========================================
            var leyog2 = 0;
            $(".leyog2").each(function() {
                var vstotal = $(this).val();
                leyog2 += Number(vstotal);
            });
            if (leyog2>0) {
                leyog(leyog2);
            }
            //==========================================
            if ($('#acs2').val()>0) {
                var cant =$('#acs1').val();
                acs(cant);
            }
            //==========================================
            if ($('#acc2').val()>0) {
                var cant =$('#acc1').val();
                acc(cant);
            }
            //==========================================
            var azucara2 = 0;
            $(".azucara2").each(function() {
                var vstotal = $(this).val();
                azucara2 += Number(vstotal);
            });
            if (azucara2>0) {
                azucara(azucara2);
            }
        //==========================================
    }, 1000);
    setTimeout(function(){ 
        var desayunotitle=$(".grupo_plan_3").val()==''?'Comida':$(".grupo_plan_3").val();
        var html='<tr class="title_plan"><th>'+desayunotitle+'<input type="hidden" value="<hr><strong>'+desayunotitle+'</strong><hr>" class="inputplan" readonly></th></tr>';
        $('.tbodytablegeneraplan_l').append(html);
        $('.tbodytablegeneraplan_m').append(html);
        $('.tbodytablegeneraplan_mi').append(html);
        $('.tbodytablegeneraplan_j').append(html);
        $('.tbodytablegeneraplan_v').append(html);
        //==========3================================
            if ($('#ver3').val()>0) {
                var cant =$('#ver3').val();
                verdura(cant);
            }
            if ($('#fru3').val()>0) {
                var cant =$('#fru3').val();
                fruta(cant);
            }
            //==========================================
            var cerales3 = 0;
            $(".cerales3").each(function() {
                var vstotal = $(this).val();
                cerales3 += Number(vstotal);
            });
            if (cerales3>0) {
                cerales(cerales3);
            }
            //==========================================
            if ($('#leg3').val()>0) {
                var cant =$('#leg3').val();
                leguminosas(cant);
            }
            //==========================================
            var aoa3 = 0;
            $(".aoa3").each(function() {
                var vstotal = $(this).val();
                aoa3 += Number(vstotal);
            });
            if (aoa3>0) {
                aoal(aoa3);
            }
            //==========================================
            var leyog3 = 0;
            $(".leyog3").each(function() {
                var vstotal = $(this).val();
                leyog3 += Number(vstotal);
            });
            if (leyog3>0) {
                leyog(leyog3);
            }
            //==========================================
            if ($('#acs3').val()>0) {
                var cant =$('#acs3').val();
                acs(cant);
            }
            //==========================================
            if ($('#acc3').val()>0) {
                var cant =$('#acc3').val();
                acc(cant);
            }
            //==========================================
            var azucara3 = 0;
            $(".azucara3").each(function() {
                var vstotal = $(this).val();
                azucara3 += Number(vstotal);
            });
            if (azucara3>0) {
                azucara(azucara3);
            }
        //==========================================
    }, 2000);
    setTimeout(function(){ 
        var desayunotitle=$(".grupo_plan_4").val()==''?'Colacion 2':$(".grupo_plan_4").val();
        var html='<tr class="title_plan"><th>'+desayunotitle+'<input type="hidden" value="<hr><strong>'+desayunotitle+'</strong><hr>" class="inputplan" readonly></th></tr>';
        $('.tbodytablegeneraplan_l').append(html);
        $('.tbodytablegeneraplan_m').append(html);
        $('.tbodytablegeneraplan_mi').append(html);
        $('.tbodytablegeneraplan_j').append(html);
        $('.tbodytablegeneraplan_v').append(html);
        //=============4=============================
            if ($('#ver4').val()>0) {
                var cant =$('#ver4').val();
                verdura(cant);
            }
            if ($('#fru4').val()>0) {
                var cant =$('#fru4').val();
                fruta(cant);
            }
            //==========================================
            var cerales4 = 0;
            $(".cerales4").each(function() {
                var vstotal = $(this).val();
                cerales4 += Number(vstotal);
            });
            if (cerales4>0) {
                cerales(cerales4);
            }
            //==========================================
            if ($('#leg4').val()>0) {
                var cant =$('#leg4').val();
                leguminosas(cant);
            }
            //==========================================
            var aoa4 = 0;
            $(".aoa4").each(function() {
                var vstotal = $(this).val();
                aoa4 += Number(vstotal);
            });
            if (aoa4>0) {
                aoal(aoa4);
            }
            //==========================================
            var leyog4 = 0;
            $(".leyog4").each(function() {
                var vstotal = $(this).val();
                leyog4 += Number(vstotal);
            });
            if (leyog4>0) {
                leyog(leyog4);
            }
            //==========================================
            if ($('#acs4').val()>0) {
                var cant =$('#acs4').val();
                acs(cant);
            }
            //==========================================
            if ($('#acc4').val()>0) {
                var cant =$('#acc4').val();
                acc(cant);
            }
            //==========================================
            var azucara4 = 0;
            $(".azucara4").each(function() {
                var vstotal = $(this).val();
                azucara4 += Number(vstotal);
            });
            if (azucara4>0) {
                azucara(azucara4);
            }
        //==========================================
    }, 3000);
    setTimeout(function(){ 
        var desayunotitle=$(".grupo_plan_5").val()==''?'Cena ':$(".grupo_plan_5").val();
        var html='<tr class="title_plan"><th>'+desayunotitle+'<input type="hidden" value="<hr><strong>'+desayunotitle+'</strong><hr>" class="inputplan" readonly></th></tr>';
        $('.tbodytablegeneraplan_l').append(html);
        $('.tbodytablegeneraplan_m').append(html);
        $('.tbodytablegeneraplan_mi').append(html);
        $('.tbodytablegeneraplan_j').append(html);
        $('.tbodytablegeneraplan_v').append(html);
        //=============5=============================
            if ($('#ver5').val()>0) {
                var cant =$('#ver5').val();
                verdura(cant);
            }
            if ($('#fru5').val()>0) {
                var cant =$('#fru5').val();
                fruta(cant);
            }
            //==========================================
            var cerales5 = 0;
            $(".cerales5").each(function() {
                var vstotal = $(this).val();
                cerales5 += Number(vstotal);
            });
            if (cerales5>0) {
                cerales(cerales5);
            }
            //==========================================
            if ($('#leg5').val()>0) {
                var cant =$('#leg5').val();
                leguminosas(cant);
            }
            //==========================================
            var aoa5 = 0;
            $(".aoa5").each(function() {
                var vstotal = $(this).val();
                aoa5 += Number(vstotal);
            });
            if (aoa5>0) {
                aoal(aoa5);
            }
            //==========================================
            var leyog5 = 0;
            $(".leyog5").each(function() {
                var vstotal = $(this).val();
                leyog5 += Number(vstotal);
            });
            if (leyog5>0) {
                leyog(leyog5);
            }
            //==========================================
            if ($('#acs5').val()>0) {
                var cant =$('#acs5').val();
                acs(cant);
            }
            //==========================================
            if ($('#acc5').val()>0) {
                var cant =$('#acc5').val();
                acc(cant);
            }
            //==========================================
            var azucara5 = 0;
            $(".azucara5").each(function() {
                var vstotal = $(this).val();
                azucara5 += Number(vstotal);
            });
            if (azucara5>0) {
                azucara(azucara5);
            }
        //==========================================
    }, 4000);
    setTimeout(function(){ 
        var desayunotitle=$(".grupo_plan_6").val()==''?'Colacion 3':$(".grupo_plan_6").val();
        var html='<tr class="title_plan"><th>'+desayunotitle+'<input type="hidden" value="<hr><strong>'+desayunotitle+'</strong><hr>" class="inputplan" readonly></th></tr>';
        $('.tbodytablegeneraplan_l').append(html);
        $('.tbodytablegeneraplan_m').append(html);
        $('.tbodytablegeneraplan_mi').append(html);
        $('.tbodytablegeneraplan_j').append(html);
        $('.tbodytablegeneraplan_v').append(html);
        //=============6=============================
            if ($('#ver6').val()>0) {
                var cant =$('#ver6').val();
                verdura(cant);
            }
            if ($('#fru6').val()>0) {
                var cant =$('#fru6').val();
                fruta(cant);
            }
            //==========================================
            var cerales6 = 0;
            $(".cerales6").each(function() {
                var vstotal = $(this).val();
                cerales6 += Number(vstotal);
            });
            if (cerales6>0) {
                cerales(cerales6);
            }
            //==========================================
            if ($('#leg6').val()>0) {
                var cant =$('#leg6').val();
                leguminosas(cant);
            }
            //==========================================
            var aoa6 = 0;
            $(".aoa6").each(function() {
                var vstotal = $(this).val();
                aoa6 += Number(vstotal);
            });
            if (aoa6>0) {
                aoal(aoa6);
            }
            //==========================================
            var leyog6 = 0;
            $(".leyog6").each(function() {
                var vstotal = $(this).val();
                leyog6 += Number(vstotal);
            });
            if (leyog6>0) {
                leyog(leyog6);
            }
            //==========================================
            if ($('#acs6').val()>0) {
                var cant =$('#acs6').val();
                acs(cant);
            }
            //==========================================
            if ($('#acc6').val()>0) {
                var cant =$('#acc6').val();
                acc(cant);
            }
            //==========================================
            var azucara6 = 0;
            $(".azucara6").each(function() {
                var vstotal = $(this).val();
                azucara6 += Number(vstotal);
            });
            if (azucara6>0) {
                azucara(azucara6);
            }
        //==========================================
    }, 5000);
    setTimeout(function(){ 
        var desayunotitle=$(".grupo_plan_7").val()==''?'':$(".grupo_plan_7").val();
        var html='<tr class="title_plan"><th>'+desayunotitle+'<input type="hidden" value="<hr><strong>'+desayunotitle+'</strong><hr>" class="inputplan" readonly></th></tr>';
        $('.tbodytablegeneraplan_l').append(html);
        $('.tbodytablegeneraplan_m').append(html);
        $('.tbodytablegeneraplan_mi').append(html);
        $('.tbodytablegeneraplan_j').append(html);
        $('.tbodytablegeneraplan_v').append(html);
        //=============7=============================
            if ($('#ver7').val()>0) {
                var cant =$('#ver7').val();
                verdura(cant);
            }
            if ($('#fru7').val()>0) {
                var cant =$('#fru7').val();
                fruta(cant);
            }
            //==========================================
            var cerales7 = 0;
            $(".cerales7").each(function() {
                var vstotal = $(this).val();
                cerales7 += Number(vstotal);
            });
            if (cerales7>0) {
                cerales(cerales7);
            }
            //==========================================
            if ($('#leg7').val()>0) {
                var cant =$('#leg7').val();
                leguminosas(cant);
            }
            //==========================================
            var aoa7 = 0;
            $(".aoa7").each(function() {
                var vstotal = $(this).val();
                aoa7 += Number(vstotal);
            });
            if (aoa7>0) {
                aoal(aoa7);
            }
            //==========================================
            var leyog7 = 0;
            $(".leyog7").each(function() {
                var vstotal = $(this).val();
                leyog7 += Number(vstotal);
            });
            if (leyog7>0) {
                leyog(leyog7);
            }
            //==========================================
            if ($('#acs7').val()>0) {
                var cant =$('#acs7').val();
                acs(cant);
            }
            //==========================================
            if ($('#acc7').val()>0) {
                var cant =$('#acc7').val();
                acc(cant);
            }
            //==========================================
            var azucara7 = 0;
            $(".azucara7").each(function() {
                var vstotal = $(this).val();
                azucara7 += Number(vstotal);
            });
            if (azucara7>0) {
                azucara(azucara7);
            }
        //==========================================
    }, 6000);
    
    removetr();
}
//=========================
function verdura(cant){
    $.ajax({
            type:'POST',
            url: base_url+'General/verduras',
            data: {
                cantidad:cant
            },
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',text: 'No Se encuentra el archivo',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',text: '500',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var array = $.parseJSON(data);
                $.each(array.lu, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_l').append(html);
                });
                $.each(array.ma, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_m').append(html);
                });
                $.each(array.mi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_mi').append(html);
                });
                $.each(array.ju, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_j').append(html);
                });
                $.each(array.vi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_v').append(html);
                });

            }
        });
}
function fruta(cant){
    $.ajax({
            type:'POST',
            url: base_url+'General/fruta',
            data: {
                cantidad:cant
            },
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',text: 'No Se encuentra el archivo',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',text: '500',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var array = $.parseJSON(data);
                $.each(array.lu, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_l').append(html);
                });
                $.each(array.ma, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_m').append(html);
                });
                $.each(array.mi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_mi').append(html);
                });
                $.each(array.ju, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_j').append(html);
                });
                $.each(array.vi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_v').append(html);
                });

            }
        });
}
function cerales(cant){
    $.ajax({
            type:'POST',
            url: base_url+'General/cereales',
            data: {
                cantidad:cant
            },
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',text: 'No Se encuentra el archivo',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',text: '500',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var array = $.parseJSON(data);
                $.each(array.lu, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_l').append(html);
                });
                $.each(array.ma, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_m').append(html);
                });
                $.each(array.mi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_mi').append(html);
                });
                $.each(array.ju, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_j').append(html);
                });
                $.each(array.vi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_v').append(html);
                });

            }
        });
}
function leguminosas(cant){
    $.ajax({
            type:'POST',
            url: base_url+'General/leguminosas',
            data: {
                cantidad:cant
            },
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',text: 'No Se encuentra el archivo',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',text: '500',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var array = $.parseJSON(data);
                $.each(array.lu, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_l').append(html);
                });
                $.each(array.ma, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_m').append(html);
                });
                $.each(array.mi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_mi').append(html);
                });
                $.each(array.ju, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_j').append(html);
                });
                $.each(array.vi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_v').append(html);
                });

            }
        });
}
function aoal(cant){
    $.ajax({
            type:'POST',
            url: base_url+'General/aoal',
            data: {
                cantidad:cant
            },
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',text: 'No Se encuentra el archivo',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',text: '500',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var array = $.parseJSON(data);
                $.each(array.lu, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_l').append(html);
                });
                $.each(array.ma, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_m').append(html);
                });
                $.each(array.mi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_mi').append(html);
                });
                $.each(array.ju, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_j').append(html);
                });
                $.each(array.vi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_v').append(html);
                });

            }
        });
}
function leyog(cant){
    $.ajax({
            type:'POST',
            url: base_url+'General/leyog',
            data: {
                cantidad:cant
            },
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',text: 'No Se encuentra el archivo',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',text: '500',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var array = $.parseJSON(data);
                $.each(array.lu, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_l').append(html);
                });
                $.each(array.ma, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_m').append(html);
                });
                $.each(array.mi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_mi').append(html);
                });
                $.each(array.ju, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_j').append(html);
                });
                $.each(array.vi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_v').append(html);
                });

            }
        });
}
function acs(cant){
    $.ajax({
            type:'POST',
            url: base_url+'General/acs',
            data: {
                cantidad:cant
            },
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',text: 'No Se encuentra el archivo',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',text: '500',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var array = $.parseJSON(data);
                $.each(array.lu, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_l').append(html);
                });
                $.each(array.ma, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_m').append(html);
                });
                $.each(array.mi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_mi').append(html);
                });
                $.each(array.ju, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_j').append(html);
                });
                $.each(array.vi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_v').append(html);
                });

            }
        });
}
function acc(cant){
    $.ajax({
            type:'POST',
            url: base_url+'General/acc',
            data: {
                cantidad:cant
            },
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',text: 'No Se encuentra el archivo',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',text: '500',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var array = $.parseJSON(data);
                $.each(array.lu, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_l').append(html);
                });
                $.each(array.ma, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_m').append(html);
                });
                $.each(array.mi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_mi').append(html);
                });
                $.each(array.ju, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_j').append(html);
                });
                $.each(array.vi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_v').append(html);
                });

            }
        });
}
function azucara(cant){
    $.ajax({
            type:'POST',
            url: base_url+'General/azucara',
            data: {
                cantidad:cant
            },
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',text: 'No Se encuentra el archivo',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',text: '500',position: 'top-right',loaderBg:'#ff6849',icon: 'error',hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var array = $.parseJSON(data);
                $.each(array.lu, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_l').append(html);
                });
                $.each(array.ma, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_m').append(html);
                });
                $.each(array.mi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_mi').append(html);
                });
                $.each(array.ju, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_j').append(html);
                });
                $.each(array.vi, function(index, item) {
                    var html='<tr>\
                                    <td><input type="text" value="'+item.nombre+'" class="inputplan"><span class="badge badge-danger removetr">x</span></td>\
                                </tr>';
                    $('.tbodytablegeneraplan_v').append(html);
                });

            }
        });
}
//============================
function removetr(){
    setTimeout(function(){ 
        $('.removetr').attr('onClick', "$(this).closest('tr').remove();");
    }, 7000);   
}
function saveplan(id){
    //================================
        var DATAplu  = [];
        var TABLAlu   = $("#tablegeneraplan_l tbody > tr");
            TABLAlu.each(function(){         
            itemlu = {};
            itemlu ["consultaId"] = id;
            itemlu ["contenido"]   = $(this).find("input[class*='inputplan']").val();
            if(TABLAlu.length>7){
                DATAplu.push(itemlu);
            }
        });
        INFO  = new FormData();
        arrayPlu   = JSON.stringify(DATAplu);
    //================================
    //================================
        var DATApma  = [];
        var TABLAma   = $("#tablegeneraplan_m tbody > tr");
            TABLAma.each(function(){         
            itemma = {};
            itemma ["consultaId"] = id;
            itemma ["contenido"]   = $(this).find("input[class*='inputplan']").val();
            if(TABLAma.length>7){
                DATApma.push(itemma);
            }
        });
        INFO  = new FormData();
        arrayPma   = JSON.stringify(DATApma);
    //================================
    //================================
        var DATApmi  = [];
        var TABLAmi   = $("#tablegeneraplan_mi tbody > tr");
            TABLAmi.each(function(){         
            itemmi = {};
            itemmi ["consultaId"] = id;
            itemmi ["contenido"]   = $(this).find("input[class*='inputplan']").val();
            if(TABLAmi.length>7){
                DATApmi.push(itemmi);
            }
        });
        INFO  = new FormData();
        arrayPmi   = JSON.stringify(DATApmi);
    //================================
    //================================
        var DATApju  = [];
        var TABLAju   = $("#tablegeneraplan_ju tbody > tr");
            TABLAju.each(function(){         
            itemju = {};
            itemju ["consultaId"] = id;
            itemju ["contenido"]   = $(this).find("input[class*='inputplan']").val();
            if(TABLAju.length>7){
                DATApju.push(itemju);
            }
        });
        INFO  = new FormData();
        arrayPju   = JSON.stringify(DATApju);
    //================================
    //================================
        var DATApvi  = [];
        var TABLAvi   = $("#tablegeneraplan_vi tbody > tr");
            TABLAvi.each(function(){         
            itemvi = {};
            itemvi ["consultaId"] = id;
            itemvi ["contenido"]   = $(this).find("input[class*='inputplan']").val();
            if(TABLAvi.length>7){
                DATApvi.push(itemvi);
            }
        });
        INFO  = new FormData();
        arrayPvi   = JSON.stringify(DATApvi);
    //================================

    var datos = 'arrayPlu='+arrayPlu+'&arrayPma='+arrayPma+'&arrayPmi='+arrayPmi+'&arrayPju='+arrayPju+'&arrayPvi='+arrayPvi;
    console.log(datos);
    $.ajax({
        type:'POST',
        url: base_url+"index.php/General/save_pla_alimenticio",
        data: datos,
        success: function (response){
                $('#modal_nutricion_plan_alimenticio').modal('hide');  
        },
        error: function(response){
        }
    });
}
function text_total_estetica(id_aux,idservicio,idc,cantidad){
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/get_texto_control_sesiones_estetica',
        data:{id_aux:id_aux,idc:idc,ids:idservicio,cantidad:cantidad},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.text_sesiones_totales_'+id_aux).html(data);
        }
    });
}
function imprimir_sesion_estetica(id){
     window.open(base_url+'PacientesConsultas/imprimir_sesion_estetica/'+id,'_blank');  
}

//(((Tabla de venta de servicios)))
function tabla_venta_servicio_c2(id){
    $.ajax({
        type:'POST',
        url: base_url+"PacientesConsultas/get_venta_servicios_c2",
        data: {id:id},//Consulta
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                    add_servicio_estetica(element.iddetalles,element.idservicio,element.servicio,element.descripcion,element.costo,0);
                });
            }  
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}
//(((Tabla de venta de servicios)))
function tabla_venta_producto_c2(id){
    $.ajax({
        type:'POST',
        url: base_url+"PacientesConsultas/get_venta_producto_c2",
        data: {id:id},//Consulta
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                    add_venta_productos(element.iddetalles,element.producto,element.idalmacen,element.lote,element.fecha_caducidad,element.cantidad,element.preciou,element.total);
                });
            }  
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}

function tipo_servicio_select(){
    tipo=$('#tipo_servicio option:selected').val();
    if(tipo==1){
        $('.tipo_imagen').html('<div class="row">\
            <div class="col-md-12" id="aceptance">\
                <div id="signature" class="signature">\
                <label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md "></label>\
                    <canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="975" height="627" style="width: 975px; height: 627px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>\
                    <img src="'+base_url+'public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignature cargarimgcara" data-signature="patientSignature">\
                </div>\
            </div>\
        </div>');
        createSignature('patientSignature');
        volveracargarimgfondo();
    }else if(tipo==2){
        $('.tipo_imagen').html('<div class="row">\
            <div class="col-md-12" id="aceptance">\
                <div id="signature" class="signature">\
                <label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md "></label>\
                    <canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="975" height="627" style="width: 975px; height: 627px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>\
                    <img src="'+base_url+'public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignature cargarimgcara" data-signature="patientSignature">\
                </div>\
            </div>\
        </div>');
        createSignature('patientSignature');
        volveracargarimgfondo_cuerpo_completo();
    }
}

function volveracargarimgfondo_cuerpo_completo(){
    var sexo =$('#sexo_text').val();

        var canvas = document.getElementById('patientSignature');
        var ctx  = canvas.getContext("2d");

        var img = new Image();
        if(sexo==2){
            img.src = base_url+"images/medicina/cuerpo_completo6.png";
        }else{
            img.src = base_url+"images/medicina/cuerpo_hombre.png";
        }
        img.onload = function(){
            ctx.drawImage(img, 0, 0);
        }
    $('.cargarimgcara').click(function(event) {
        var canvas = document.getElementById('patientSignature');
        var ctx  = canvas.getContext("2d");

        var img = new Image();
        if(sexo==2){
            img.src = base_url+"images/medicina/cuerpo_completo6.png";
        }else{
            img.src = base_url+"images/medicina/cuerpo_hombre.png";
        }
        img.onload = function(){
            ctx.drawImage(img, 0, 0);
        }
    });
}
function imprimir_span_c2(id){
    if(id==0){
        $.toast({
            heading: '¡Atención!',
            text: 'Por favor primero guarde la consulta ',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }else{
        setTimeout(function(){ 
            window.open(base_url+'PacientesConsultas/imprimirspac2/'+id,'_blank');  
        }, 1000);
    }
}

function imprimir_span_ticket(id){
    if(id==0){
        $.toast({
            heading: '¡Atención!',
            text: 'Por favor primero guarde la consulta ',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }else{
        setTimeout(function(){ 
            $('#modal_ticket_medicina_estetica').modal(); 
            $('.iframereporte').html('<iframe src="' + base_url + 'Reportes/ticket_pan/'+id+'" class="iframeprintc1" id="iframeprintc1"></iframe>');
        }, 1000);
    }
}
function imprimiriframac1(documentId){
    document.getElementById(documentId).contentWindow.print();
}

function visualizar_consulta_nutricion(idc){
    aux_diagnostico_c1=0;
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/visualizar_consulta_nutricion_text',
        data:{id:idc},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.consulta').html(data);
        }
    });
}

//(((Tabla de venta de servicios)))
function tabla_venta_servicio_c3(id){
    $.ajax({
        type:'POST',
        url: base_url+"PacientesConsultas/get_venta_servicios_c3",
        data: {id:id},//Consulta
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                    add_servicio_estetica(element.iddetalles,element.idservicio,element.servicio,element.descripcion,element.costo,0);
                });
            }  
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}
//(((Tabla de venta de servicios)))
function tabla_venta_producto_c3(id){
    $.ajax({
        type:'POST',
        url: base_url+"PacientesConsultas/get_venta_producto_c3",
        data: {id:id},//Consulta
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                    add_venta_productos(element.iddetalles,element.producto,element.idalmacen,element.lote,element.fecha_caducidad,element.cantidad,element.preciou,element.total);
                });
            }  
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}
function imprimir_nutricion(id){
    if(id==0){
        $.toast({
            heading: '¡Atención!',
            text: 'Por favor primero guarde la consulta ',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }else{
        setTimeout(function(){ 
            window.open(base_url+'Reportes/reportenutricion/'+id,'_blank');  
        }, 1000);
    }
}
function imprimir_nutricion_ticket(id){
    if(id==0){
        $.toast({
            heading: '¡Atención!',
            text: 'Por favor primero guarde la consulta ',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }else{
        setTimeout(function(){ 
            $('#modal_ticket_medicina_estetica').modal(); 
            $('.iframereporte').html('<iframe src="' + base_url + 'Reportes/ticket_nutricion/'+id+'" class="iframeprintc1" id="iframeprintc1"></iframe>');
        }, 1000);
    }
}
///////////////// Sesiones spa//////////////////////////////////////////////// 
function modal_cotrol_sesiones_spa(id){
    $('.imprimir_sesiones_estetica').html('<div class="row">\
            <div class="col-md-12">\
                <button type="button" class="btn waves-effect waves-light btn-info btn_consulta"\
                 onclick="imprimir_sesion_spa_im('+id+')"><i class="ti-printer"></i> Imprimir</button>\
            </div>\
        </div>  ');
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/get_servicios_consulta_spa',
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.texto_control_sesiones_c1').html(data);
        }
    });
    $('#modal_cotrol_sesion_me').modal();

}
function addsess_spa(id_aux,idservicio,idc,cantidad){
    var fecha_me = $('#fecha_me'+id_aux).val();
    var obser_me = $('#obser_me'+id_aux).val();
    if(fecha_me!='' &&  obser_me!=''){
        $.ajax({
            type:'POST',
            url: base_url+'PacientesConsultas/add_sesiones_spa',
            data: {idconsulta:idc,
                    idservicio:idservicio,
                    fecha:fecha_me,
                    observacion:obser_me},
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $('#fecha_me'+id_aux).val('');
                $('#obser_me'+id_aux).val('');
            }
        });        
    }else{
        $.toast({
            heading: '¡Atención!',
            text: 'Hace falta llenar el campo de fecha o el de obervaciones',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }  
    setTimeout(function(){
        tabla_ses_spa_get(id_aux,idservicio,idc,cantidad);  
    }, 500);
    
    
}

function tabla_ses_spa_get(id_aux,idservicio,idc,cantidad){
    setTimeout(function(){
        text_total_spa(id_aux,idservicio,idc,cantidad);
    }, 1000);
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/get_tabla_control_sesiones_spa',
        data:{idc:idc,ids:idservicio},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.tabla_sesiones_estetica_'+id_aux).html(data);
        }
    });
}
function text_total_spa(id_aux,idservicio,idc,cantidad){
    $.ajax({
        type:'POST',
        url: base_url+'PacientesConsultas/get_texto_control_sesiones_spa',
        data:{id_aux:id_aux,idc:idc,ids:idservicio,cantidad:cantidad},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.text_sesiones_totales_'+id_aux).html(data);
        }
    });
}
function imprimir_sesion_spa_im(id){
     window.open(base_url+'PacientesConsultas/imprimir_sesion_spa/'+id,'_blank');  
}