var base_url = $('#base_url').val();
var auxdetalle=0;
var id_diag_aux=0;
var id_laboratorio_aux=0;
var id_medi_aux=0;
var id_medi_id_aux=0;
var id_tomar_aux=0;
var id_tomar_id_aux=0;
var id_analisis_aux=0;
var id_analisis_id_aux=0;
var id_medico_aux=0;
var id_medico_id_aux=0;
//===((( Consultas ))) =====
var tipo_consulta_aux=0;
var id_diag_estetica_aux=0;
var id_trata_estetica_aux=0;
$(document).ready(function() {
    //txt_consulta();
    select_tipo_consulta();
    existeconsulta();
    /*
    if($('#tipo_consulta').val()==1){
        txt_consulta(0);

    }
    */
    if($('#tipo_consulta').val()==1){
        $('.consultas_li').addClass('active');
        $('.consultas_li').attr('aria-selected', true);
        $('.hitoria_cli').removeClass('active');
        $('.hitoria_cli').attr('aria-selected', false);
        $('.consultas_li_ar').removeClass('active');
        $('.consultas_li_ar').attr('aria-selected', false);
        $('#relevante1').removeClass('active')
        $('#consultas2').addClass('active');
    }
});

function detalle_paciente(idp){
    if(auxdetalle==0){
    $('.detalles_paciente_primera').css('display','none');
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/detallepaciente',
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.detalles_paciente').html(data);
            $('#foto_avatar').change(function(){
                document.getElementById('img_avatar').src = window.URL.createObjectURL(this.files[0]);
                setTimeout(function(){ 
                    add_file($('#idpte').val());
                }, 1500);
            });
        }
    });
    }
    if(auxdetalle==1){
        auxdetalle=0;
        $('.detalles_paciente').html('');
        $('.detalles_paciente_primera').css('display','block');
    }else{
        auxdetalle=1; 

    }
}
function add_file(id){
    var archivo=$('#foto_avatar').val()
    //var name=$('#foto_avatar').val()
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg");
    permitida = false;
    if($('#foto_avatar')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('foto_avatar');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('id',id);
            $.ajax({
                url:base_url+'Pacientes/cargafiles',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                    $.toast({
                        heading: 'Éxito',
                        text: 'Guardado Correctamente',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 3500, 
                        stack: 6
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }        
}
function infopaciente(id){
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/detallepaciente_info',
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.detalle_paciente').css('display','none');
            $('.btn_datos1').css('display','none');
            $('.detalle_paciente_datos').html(data);
        }
    });
}
function guarda_paciente(){
    var form_register = $('#form_paciente');
    var datos = form_register.serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/registraPaciente',
        data: datos,
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            var id=data;
            $.toast({
                heading: 'Éxito',
                text: 'Se guardaron los cambios de forma exitosa',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
              });
            setTimeout(function(){ 
                window.location = base_url+'Pacientes/paciente/'+id;
            }, 1500);
        }
    }); 
}
var aux_tipo_servicio=0;
function modal_nuevo_pago(id){
    $('#nuevopago').modal();
    $('#idpacientep').val(id);
    $('#fecha_pago').val('');
    $('#factura').prop('checked',false);
    $('.text_tipo_servicio').html('');
    aux_tipo_servicio=0;
    btn_pago_t_s();
}
function btn_pago_t_s(){
    tipo_servicio(0,0,0,'');
}
function tipo_servicio(id,t_servicio,monto,nota){
    $html='<div class="row row_'+aux_tipo_servicio+'">\
                        <div class="col-md-5">\
                            <div class="form-group">\
                                <label><i class="fas fa-hand-holding-heart"></i> Tipo de servicio</label>\
                                <select id="tipo_servicio" class="form-control">\
                                    <option selected="" disabled="" value="0">Seleccione</option>\
                                    <option value="1">Consulta de primera vez</option>\
                                    <option value="2">Consulta subsecuente</option>\
                                    <option value="3">Vídeo Consulta</option>\
                                    <option value="4">Estudios</option>\
                                    <option value="5">Endoscopía</option>\
                                    <option value="6">Medicamentos</option>\
                                    <option value="7">Vacunas</option>\
                                    <option value="8">Venta de Productos</option>\
                                    <option value="9">Otros</option>\
                                </select>\
                            </div>\
                        </div>\
                        <div class="col-md-3">\
                            <div class="form-group">\
                                <div class="form-group">\
                                    <label><i class="fas fa-dollar-sign"></i> Monto</label>\
                                    <input type="number" id="monto" class="form-control">\
                                </div>\
                            </div>\
                        </div>\
                        <div class="col-md-3">\
                            <div class="form-group">\
                                <label><i class="fas fa-sticky-note"></i> Notas</label>\
                                <input type="text" id="notas" class="form-control">\
                            </div>\
                        </div>\
                        <div class="col-md-1"><br><br>';
                        if(aux_tipo_servicio<1){
                    $html+='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-info btn_add_ts_'+aux_tipo_servicio+'" onclick="btn_pago_t_s('+aux_tipo_servicio+')"><i class="fas fa-plus"></i></button><br>';
                        }else{
                    $html+='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-secondary" onclick="remover_tipo_servicio('+aux_tipo_servicio+')"><i class="fas fa-minus"></i></button>';
                        }
                    $html+='</div>\
                    </div> ';
    $('.text_tipo_servicio').append($html); 
    aux_tipo_servicio++;
}
function remover_tipo_servicio(aux){
    $('.row_'+aux).remove();
}
function guarda_pagos(){
    var TABLA   = $(".text_tipo_servicio > div");
    var aux_verifica=0;
    TABLA.each(function(){
        if ($(this).find("input[id*='monto']").val()!='') {
           aux_verifica=1;
        }
    });    
    if(aux_verifica==1){
        var form_register = $('#form_pagos');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                fecha_pago:{
                  required: true
                },
                metodo_pago:{
                  required: true
                },
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        var $valid = $("#form_pagos").valid();
        if($valid){
            $('.btn_pago').prop('disabled',true);
            var datos = form_register.serialize();
            $.ajax({
                type:'POST',
                url: base_url+'Pacientes/registraPago',
                data: datos,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success:function(data){
                    var id=data;
                    var DATA  = [];
                        var TABLA   = $(".text_tipo_servicio > div");
                        TABLA.each(function(){   
                          if ($(this).find("input[id*='monto']").val()!='') {
                              item = {};
                              item ["idpagos_paciente"] = id;
                              item ["tipo_servicio"] = $(this).find("select[id*='tipo_servicio']").val();
                              item ["monto"] = $(this).find("input[id*='monto']").val();
                              item ["notas"] = $(this).find("input[id*='notas']").val();
                              DATA.push(item);
                          }   
                      });
                      INFO  = new FormData();
                      aInfo   = JSON.stringify(DATA);
                      INFO.append('data', aInfo);
                      $.ajax({
                          data: INFO, 
                          type: 'POST',
                          url : base_url + 'Pacientes/registraPagodetalle',
                          processData: false, 
                          contentType: false,
                          async: false,
                          statusCode:{
                            404: function(data){
                                $.toast({
                                    heading: '¡Error!',
                                    text: 'No Se encuentra el archivo',
                                    position: 'top-right',
                                    loaderBg:'#ff6849',
                                    icon: 'error',
                                    hideAfter: 3500
                                });
                            },
                            500: function(){
                                $.toast({
                                    heading: '¡Error!',
                                    text: '500',
                                    position: 'top-right',
                                    loaderBg:'#ff6849',
                                    icon: 'error',
                                    hideAfter: 3500
                                });
                            }
                          },
                          success: function(data){
                          }
                      }); 



                    $.toast({
                        heading: 'Éxito',
                        text: 'Guardado Correctamente',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 3500, 
                        stack: 6
                    });
                    $('#nuevopago').modal('hide');
                    setTimeout(function(){ 
                        $('.btn_pago').prop('disabled',false);
                    }, 5000);
                }
            }); 
        }
    }else{
        $.toast({
            heading: '¡Error!',
            text: 'Servicio [0] inválido',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }    
}
function modal_reenviarformulario(id){
    $('#modal_reenviar_f').modal();
    $('#correo_pac').val($('.correo_paciente').data('correop'));
    $('#idpacientec').val(id);
}
function href_agenda(id){
    window.location.href = base_url+"Agenda/?id="+id;
}
function enviar_formulario(){
    var id = $('#idpacientec').val();
    var correo = $('#correo_pac').val();
    if(correo!=''){
        $('#modal_reenviar_f').modal('hide');
        $.toast({
            heading: 'Éxito',
            text: 'Se esta enviado el correo',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
        });
        $.ajax({
            type:'POST',
            url: base_url+"Pacientes/enviar",
            data:{id:id,correo:correo},
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Enviado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
            }
        }); 
    }else{
        $.toast({
            heading: '¡Atención!',
            text: 'El campo debe estar lleno',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }    
}
var auxdfp=0;
function direccion_fiscal_paciente(){
    if(auxdfp==0){
        auxdfp=1;
        $('.dfp').html('<i class="fas fa-caret-down"></i>');
        $('.dfp_texto').css('display','block'); 
    }else{
        auxdfp=0;
        $('.dfp').html('<i class="fas fa-caret-right"></i>');
        $('.dfp_texto').css('display','none'); 
    }
}
function btn_edit_dfc(id){
    $('.datos_factura').html('');
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/datos_factura_text',
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.datos_factura').html(data);
        }
    });
}
function guarda_factura_paciente(){
    var form_register = $('#form_paciente_factura');
    var datos = form_register.serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/registra_factura_paciente',
        data: datos,
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            var id=data;
            $.toast({
                heading: 'Éxito',
                text: 'Se guardaron los cambios de forma exitosa',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
              });
            setTimeout(function(){ 
                window.location.reload();
            }, 1500);
        }
    }); 
}
function agregar_acompaniante(id){
    $('.btn_agregar_familiar').css('display','none');
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/agregar_familiares',
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.add_acompaniante').html(data);
            $('#foto_avatar_familiar').change(function(){
                document.getElementById('img_agre_familiar').src = window.URL.createObjectURL(this.files[0]); 
            });
        }
    });
}
function guarda_acompanante(id) {
    var form_register = $('#form_paciente_acompanante');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            },
            apll_paterno:{
              required: true
            },
            apll_materno:{
              required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var $valid = $("#form_paciente_acompanante").valid();
    if($valid) {
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Pacientes/registra_acompanante',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var id=data;
                add_file_acompanante(id);
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                  });
                setTimeout(function(){ 
                    $('.add_acompaniante').html('');
                    $('.btn_agregar_familiar').css('display','block');
                    window.location.reload();
                }, 1500);

            }
        });
    }  
}
function add_file_acompanante(id){
    var archivo=$('#foto_avatar_familiar').val()
    //var name=$('#foto_avatar').val()
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg");
    permitida = false;
    if($('#foto_avatar_familiar')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('foto_avatar_familiar');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('id',id);
            $.ajax({
                url:base_url+'Pacientes/cargafiles_acompanante',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }        
}
function list_acompanante(id){
    $('.acompanante_list').html('');
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/listado_acompanante',
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
            $('.acompanante_list').html(array.info);
            var listacp=array.list_acompa;
            listacp.forEach(function(e){
                $('#foto_avat_'+e.idacompanante).change(function(){
                    document.getElementById('img_avat_'+e.idacompanante).src = window.URL.createObjectURL(this.files[0]); 
                    add_file_acompanante_edit(e.idacompanante);
                });
            });
        }
    });
}
function add_file_acompanante_edit(id){
    var archivo=$('#foto_avat_'+id).val()
    //var name=$('#foto_avatar').val()
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg");
    permitida = false;
    if($('#foto_avat_'+id)[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('foto_avat_'+id);
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('id',id);
            $.ajax({
                url:base_url+'Pacientes/cargafiles_acompanante',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data){
                    $.toast({
                        heading: 'Éxito',
                        text: 'Guardado Correctamente',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 3500, 
                        stack: 6
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }        
}
function eliminar_acompanante(ida,idp){
    $('#eliminar_acompanante_modal').modal();
    $('#idacompaniante_e').val(ida);
    $('#id_paciente_e').val(idp);
}
function aliminar_acompanante(){
    var ida=$('#idacompaniante_e').val();
    var idp=$('#id_paciente_e').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/alimanaracompanante",
        data:{id:ida},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            list_acompanante(idp);
            $('#eliminar_acompanante_modal').modal('hide');
        }
    });  
}
function edit_acompanante(ida,idp){
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/texto_acompanate_e',
        data:{ida:ida,idp:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.texto_acompanate_'+ida).html(data);
        }
    });
}
function guarda_acompanante_edit(){
    var datos = $('#form_paciente_acompanante_edit').serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Pacientes/registra_acompanante',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                list_acompanante($('#idpaciente_edit').val());
            }
        });
}
function imprimir_aviso_privacidad(id,tipo,idd){
    var html='';
    if(tipo==1){
        html='<iframe src="'+base_url+'Pacientes/aviso_inyeccion/'+id+'/'+idd+'"></iframe>'; 
    }else if(tipo==2){
        html='<iframe src="'+base_url+'Pacientes/aviso_enzimas/'+id+'/'+idd+'"></iframe>';
    }else if(tipo==3){
        html='<iframe src="'+base_url+'Pacientes/aviso_toxina/'+id+'/'+idd+'"></iframe>';
    }else if(tipo==4){
        html='<iframe src="'+base_url+'Pacientes/aviso_bichectomia/'+id+'/'+idd+'"></iframe>';
    }else if(tipo==5){
        html='<iframe src="'+base_url+'Pacientes/aviso_luz_pulsada/'+id+'/'+idd+'"></iframe>';
    }else if(tipo==6){
        html='<iframe src="'+base_url+'Pacientes/aviso_radio_frecuencia/'+id+'/'+idd+'"></iframe>';
    }else if(tipo==7){
        html='<iframe src="'+base_url+'Pacientes/aviso_hidrolipoclasia/'+id+'/'+idd+'"></iframe>';
    }
        $('.iframereceta').html('');
        $('.iframereceta').html(html);
}
function firmar_aviso_privacidad(id,tipo){
    if(tipo==1){
        window.open(base_url+'Pacientes/firma_inyeccion/'+id, '_blank');
    }else if(tipo==2){
        window.open(base_url+'Pacientes/firma_enzimas/'+id, '_blank');
    }else if(tipo==3){
        window.open(base_url+'Pacientes/firma_toxina/'+id, '_blank');
    }else if(tipo==4){
        window.open(base_url+'Pacientes/firma_bichectomia/'+id, '_blank');
    }else if(tipo==5){
        window.open(base_url+'Pacientes/firma_luz_pulsada/'+id, '_blank');
    }else if(tipo==6){
        window.open(base_url+'Pacientes/firma_radio_frecuencia/'+id, '_blank');
    }else if(tipo==7){    
        window.open(base_url+'Pacientes/firma_hidrolipoclasia/'+id, '_blank');
    }
    
}
////////////////////////////////////////////////////////
/////////////////////Inf Relevante / Historia Clinica /////////////////////
function click_negadas(){
    if($('#negadas').is(':checked')){
        $('#alergias').val('Negadas');
    }else{
        $('#alergias').val('');
    }
}
function inf_relevante(){
    var datos = $('#form_relevante').serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/registra_inf_relevante',
        data: datos,
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('#idrelevante').val(data);
        }
    });
}
var auxbtnh1=0;
var auxbtnh2=0;
var auxbtnh3=0;
var auxbtnh4=0;
var auxbtnh5=0;
var auxbtnh6=0;
function btn_his1(){
    if(auxbtnh1==0){
        auxbtnh1=1;
        $('.btn_his1').css('display','block');
    }else{
        auxbtnh1=0;
        $('.btn_his1').css('display','none');
    }
}
function btn_his2(){
    if(auxbtnh2==0){
        auxbtnh2=1;
        $('.btn_his2').css('display','block');
    }else{
        auxbtnh2=0;
        $('.btn_his2').css('display','none');
    }
}
function btn_his3(){
    if(auxbtnh3==0){
        auxbtnh3=1;
        $('.btn_his3').css('display','block');
    }else{
        auxbtnh3=0;
        $('.btn_his3').css('display','none');
    }
}
function btn_his4(){
    if(auxbtnh4==0){
        auxbtnh4=1;
        $('.btn_his4').css('display','block');
    }else{
        auxbtnh4=0;
        $('.btn_his4').css('display','none');
    }
}
function btn_his5(){
    if(auxbtnh5==0){
        auxbtnh5=1;
        $('.btn_his5').css('display','block');
    }else{
        auxbtnh5=0;
        $('.btn_his5').css('display','none');
    }
}
function btn_his6(){
    if(auxbtnh6==0){
        auxbtnh6=1;
        $('.btn_his6').css('display','block');
    }else{
        auxbtnh6=0;
        $('.btn_his6').css('display','none');
    }
}

function editarhistoriacli(){
        $('.historia_clinica_visual').css('display','none');
        $('.historia_clinica_editar').css('display','block');
        $.ajax({
            type:'POST',
            url: base_url+'Pacientes/texto_historia_clinica',
            data:{idp:$('#idpte').val()},
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                array=$.parseJSON(data);
                $('.text_his1').html(array.his1);
                $('.text_his2').html(array.his2);
                $('.text_his3').html(array.his3);
                $('.text_his4').html(array.his4);
                $('.text_his5').html(array.his6_5);
                $('.text_his6').html(array.his6_6);
                $('.text_historia_clinica_abajo').html(array.his5);
                //Antecedentes Familiares
                $('#chediabetes').click(function(event){
                    if($('#chediabetes').is(':checked')){
                        $('.t_in_1').css('display','block');
                    }else{
                        $('.t_in_1').css('display','none');
                    }
                });
                $('#checancer').click(function(event){
                    if($('#checancer').is(':checked')){
                        $('.t_in_2').css('display','block');
                    }else{
                        $('.t_in_2').css('display','none');
                    }
                });
                $('#chehipertencion').click(function(event){
                    if($('#chehipertencion').is(':checked')){
                        $('.t_in_3').css('display','block');
                    }else{
                        $('.t_in_3').css('display','none');
                    }
                });
                $('#cheotro_antecedente').click(function(event){
                    if($('#cheotro_antecedente').is(':checked')){
                        $('.t_in_4').css('display','block');
                    }else{
                        $('.t_in_4').css('display','none');
                    }
                });
                //Personales Patologicos
                $('#cheingesta').click(function(event){
                    if($('#cheingesta').is(':checked')){
                        $('.t_pe_1').css('display','block');
                    }else{
                        $('.t_pe_1').css('display','none');
                    }
                });
                $('#checirugia').click(function(event){
                    if($('#checirugia').is(':checked')){
                        $('.t_pe_2').css('display','block');
                    }else{
                        $('.t_pe_2').css('display','none');
                    }
                });
                $('#chetransfusiones').click(function(event){
                    if($('#chetransfusiones').is(':checked')){
                        $('.t_pe_3').css('display','block');
                    }else{
                        $('.t_pe_3').css('display','none');
                    }
                });
                $('#chehepatitis').click(function(event){
                    if($('#chehepatitis').is(':checked')){
                        $('.t_pe_4').css('display','block');
                    }else{
                        $('.t_pe_4').css('display','none');
                    }
                });
                $('#chediabetesmillitus').click(function(event){
                    if($('#chediabetesmillitus').is(':checked')){
                        $('.t_pe_5').css('display','block');
                    }else{
                        $('.t_pe_5').css('display','none');
                    }
                });
                $('#chehipertensionarterial').click(function(event){
                    if($('#chehipertensionarterial').is(':checked')){
                        $('.t_pe_6').css('display','block');
                    }else{
                        $('.t_pe_6').css('display','none');
                    }
                });
                //Personales No Patologicos
                $('#chetabaquismo').click(function(event){
                    if($('#chetabaquismo').is(':checked')){
                        $('.t_ap_1').css('display','block');
                    }else{
                        $('.t_ap_1').css('display','none');
                    }
                });
                $('#chetomabebidas').click(function(event){
                    if($('#chetomabebidas').is(':checked')){
                        $('.t_ap_2').css('display','block');
                    }else{
                        $('.t_ap_2').css('display','none');
                    }
                });
                $('#checonsumodrogas').click(function(event){
                    if($('#checonsumodrogas').is(':checked')){
                        $('.t_ap_3').css('display','block');
                    }else{
                        $('.t_ap_3').css('display','none');
                    }
                });
                $('#cherealizaejercicio').click(function(event){
                    if($('#cherealizaejercicio').is(':checked')){
                        $('.t_ap_4').css('display','block');
                    }else{
                        $('.t_ap_4').css('display','none');
                    }
                });
                //======================================================
                //Personales No Patologicos
                $('#chetpersonal').click(function(event){
                    if($('#chetpersonal').is(':checked')){
                        $('.t_ap_5s').css('display','block');
                    }else{
                        $('.t_ap_5s').css('display','none');
                    }
                });
                $('#chetalimentacion').click(function(event){
                    if($('#chetalimentacion').is(':checked')){
                        $('.t_ap_6s').css('display','block');
                    }else{
                        $('.t_ap_6s').css('display','none');
                    }
                });
                $('#chetfruta').click(function(event){
                    if($('#chetfruta').is(':checked')){
                        $('.t_ap_7s').css('display','block');
                    }else{
                        $('.t_ap_7s').css('display','none');
                    }
                });
                $('#chetcarne').click(function(event){
                    if($('#chetcarne').is(':checked')){
                        $('.t_ap_8s').css('display','block');
                    }else{
                        $('.t_ap_8s').css('display','none');
                    }
                });
                $('#chetcerdo').click(function(event){
                    if($('#chetcerdo').is(':checked')){
                        $('.t_ap_9s').css('display','block');
                    }else{
                        $('.t_ap_9s').css('display','none');
                    }
                });
                $('#chetpollo').click(function(event){
                    if($('#chetpollo').is(':checked')){
                        $('.t_ap_10s').css('display','block');
                    }else{
                        $('.t_ap_10s').css('display','none');
                    }
                });
                $('#chetlacteo').click(function(event){
                    if($('#chetlacteo').is(':checked')){
                        $('.t_ap_11s').css('display','block');
                    }else{
                        $('.t_ap_11s').css('display','none');
                    }
                });

                $('#chetharina').click(function(event){
                    if($('#chetharina').is(':checked')){
                        $('.t_ap_12s').css('display','block');
                    }else{
                        $('.t_ap_12s').css('display','none');
                    }
                });

                $('#chetagua').click(function(event){
                    if($('#chetagua').is(':checked')){
                        $('.t_ap_13s').css('display','block');
                    }else{
                        $('.t_ap_13s').css('display','none');
                    }
                });

                $('#chetchatarra').click(function(event){
                    if($('#chetchatarra').is(':checked')){
                        $('.t_ap_14s').css('display','block');
                    }else{
                        $('.t_ap_14s').css('display','none');
                    }
                });

                $('#chetalcohol').click(function(event){
                    if($('#chetalcohol').is(':checked')){
                        $('.t_ap_15s').css('display','block');
                    }else{
                        $('.t_ap_15s').css('display','none');
                    }
                });

                $('#chettabaco').click(function(event){
                    if($('#chettabaco').is(':checked')){
                        $('.t_ap_16s').css('display','block');
                    }else{
                        $('.t_ap_16s').css('display','none');
                    }
                });

                $('#chetdroga').click(function(event){
                    if($('#chetdroga').is(':checked')){
                        $('.t_ap_17s').css('display','block');
                    }else{
                        $('.t_ap_17s').css('display','none');
                    }
                });

                $('#chetmedicamento').click(function(event){
                    if($('#chetmedicamento').is(':checked')){
                        $('.t_ap_18s').css('display','block');
                    }else{
                        $('.t_ap_18s').css('display','none');
                    }
                });
                $('#chethospitalizacion').click(function(event){
                    if($('#chethospitalizacion').is(':checked')){
                        $('.t_pe_7').css('display','block');
                    }else{
                        $('.t_pe_7').css('display','none');
                    }
                });

                $('#chetalegia').click(function(event){
                    if($('#chetalegia').is(':checked')){
                        $('.t_pe_19s').css('display','block');
                    }else{
                        $('.t_pe_19s').css('display','none');
                    }
                });

                $('#chetfractura').click(function(event){
                    if($('#chetfractura').is(':checked')){
                        $('.t_pe_20s').css('display','block');
                    }else{
                        $('.t_pe_20s').css('display','none');
                    }
                });

                $('#chetlactancia').click(function(event){
                    if($('#chetlactancia').is(':checked')){
                        $('.t_pe_21s').css('display','block');
                    }else{
                        $('.t_pe_21s').css('display','none');
                    }
                });


            }
        });
        $('.editarhistoriacli_btn').html('<button  type="button" class="btn waves-effect waves-light btn-sm btn-secondary" onclick="registra_historialclinica()">Guardar</button>');
}
var auxtext_his1=1;
var auxtext_his2=1;
var auxtext_his3=1;
var auxtext_his4=1;
var auxtext_his5=1;
var auxtext_his6=1;
function btn_text_his1(){
    if(auxtext_his1==0){
        auxtext_his1=1;
        $('.text_his1').css('display','block');
    }else{
        auxtext_his1=0;
        $('.text_his1').css('display','none');
    }
}
function btn_text_his2(){
    if(auxtext_his2==0){
        auxtext_his2=1;
        $('.text_his2').css('display','block');
    }else{
        auxtext_his2=0;
        $('.text_his2').css('display','none');
    }
}
function btn_text_his3(){
    if(auxtext_his3==0){
        auxtext_his3=1;
        $('.text_his3').css('display','block');
    }else{
        auxtext_his3=0;
        $('.text_his3').css('display','none');
    }
}
function btn_text_his4(){
    if(auxtext_his4==0){
        auxtext_his4=1;
        $('.text_his4').css('display','block');
    }else{
        auxtext_his4=0;
        $('.text_his4').css('display','none');
    }
}
function btn_text_his5(){
    if(auxtext_his5==0){
        auxtext_his5=1;
        $('.text_his5').css('display','block');
    }else{
        auxtext_his5=0;
        $('.text_his5').css('display','none');
    }
}
function btn_text_his6(){
    if(auxtext_his6==0){
        auxtext_his6=1;
        $('.text_his6').css('display','block');
    }else{
        auxtext_his6=0;
        $('.text_his6').css('display','none');
    }
}
function registra_historialclinica(){
    var datos = $('#form_histotiaclinica').serialize();
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/registra_historia_clinica',
        data: datos,
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('#idclinica_e').val(data);
            $.toast({
                heading: 'Éxito',
                text: 'Se guardaron los cambios de forma exitosa',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
              });
            setTimeout(function(){ 
                window.location.reload();
            }, 1500);
        }
    });
}
///
//
function txt_consulta(idc){
    $('.consultas_li').addClass('active');
    $('.consultas_li').attr('aria-selected', true);
    $('.hitoria_cli').removeClass('active');
    $('.hitoria_cli').attr('aria-selected', false);
    $('.consultas_li_ar').removeClass('active');
    $('.consultas_li_ar').attr('aria-selected', false);
    $('#relevante1').removeClass('active');
    $('#consultas2').addClass('active');

    
    /*
    aux_diagnostico=0;
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/text_consulta',
        data:{idc:idc,idp:$('#idpte').val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.consulta').html(data);
            $('#proximaconsulta').change(function(event){
                var proximaconsulta = $('#proximaconsulta option:selected').val();
                 var consultafecha = $('#consultafecha').val();
                    var fecha = new Date(consultafecha);
                    if(proximaconsulta==1){
                        fecha.setDate(fecha.getDate() + 8);
                        y = fecha.getFullYear();
                        //Mes
                        m = fecha.getMonth() + 1;
                        if(m<=9){
                            mm='0'+m; 
                        }else{
                            mm=m;
                        }
                        //Día
                        d = fecha.getDate();
                        if(d<=9){
                            dd='0'+d; 
                        }else{
                            dd=d;
                        }
                        $('#proximafecha').val(y+"-"+mm+"-"+dd);
                    }
                    if(proximaconsulta==2){
                        fecha.setDate(fecha.getDate() + 15);
                        y = fecha.getFullYear();
                        //Mes
                        m = fecha.getMonth() + 1;
                        if(m<=9){
                            mm='0'+m; 
                        }else{
                            mm=m;
                        }
                        //Día
                        d = fecha.getDate();
                        if(d<=9){
                            dd='0'+d; 
                        }else{
                            dd=d;
                        }
                        $('#proximafecha').val(y+"-"+mm+"-"+dd);
                    }
                    if(proximaconsulta==3){
                        fecha.setDate(fecha.getDate() + 22);
                        y = fecha.getFullYear();
                        //Mes
                        m = fecha.getMonth() + 1;
                        if(m<=9){
                            mm='0'+m; 
                        }else{
                            mm=m;
                        }
                        //Día
                        d = fecha.getDate();
                        if(d<=9){
                            dd='0'+d; 
                        }else{
                            dd=d;
                        }
                        $('#proximafecha').val(y+"-"+mm+"-"+dd);
                    }
                    if(proximaconsulta==4){
                        fecha.setMonth(fecha.getMonth() + 1);
                        y = fecha.getFullYear();
                        //Mes
                        m = fecha.getMonth() + 1;
                        if(m<=9){
                            mm='0'+m; 
                        }else{
                            mm=m;
                        }
                        //Día
                        d = fecha.getDate();
                        if(d<=9){
                            dd='0'+d; 
                        }else{
                            dd=d;
                        }
                        $('#proximafecha').val(y+"-"+mm+"-"+dd);
                    }
                    if(proximaconsulta==5){
                        fecha.setMonth(fecha.getMonth() + 2);
                        y = fecha.getFullYear();
                        //Mes
                        m = fecha.getMonth() + 1;
                        if(m<=9){
                            mm='0'+m; 
                        }else{
                            mm=m;
                        }
                        //Día
                        d = fecha.getDate();
                        if(d<=9){
                            dd='0'+d; 
                        }else{
                            dd=d;
                        }
                        $('#proximafecha').val(y+"-"+mm+"-"+dd);
                    }
                    if(proximaconsulta==6){
                        fecha.setMonth(fecha.getMonth() + 3);
                        y = fecha.getFullYear();
                        //Mes
                        m = fecha.getMonth() + 1;
                        if(m<=9){
                            mm='0'+m; 
                        }else{
                            mm=m;
                        }
                        //Día
                        d = fecha.getDate();
                        if(d<=9){
                            dd='0'+d; 
                        }else{
                            dd=d;
                        }
                        $('#proximafecha').val(y+"-"+mm+"-"+dd);
                    }
                    if(proximaconsulta==7){
                        fecha.setMonth(fecha.getMonth() + 6);
                        y = fecha.getFullYear();
                        //Mes
                        m = fecha.getMonth() + 1;
                        if(m<=9){
                            mm='0'+m; 
                        }else{
                            mm=m;
                        }
                        //Día
                        d = fecha.getDate();
                        if(d<=9){
                            dd='0'+d; 
                        }else{
                            dd=d;
                        }
                        $('#proximafecha').val(y+"-"+mm+"-"+dd);
                    }
                    if(proximaconsulta==8){
                        fecha.setMonth(fecha.getMonth() + 12);
                        y = fecha.getFullYear();
                        //Mes
                        m = fecha.getMonth() + 1;
                        if(m<=9){
                            mm='0'+m; 
                        }else{
                            mm=m;
                        }
                        //Día
                        d = fecha.getDate();
                        if(d<=9){
                            dd='0'+d; 
                        }else{
                            dd=d;
                        }
                        $('#proximafecha').val(y+"-"+mm+"-"+dd);
                    }
            });
            if(idc==0){
                agregar_diag();
                agregar_recet();
            }else{
                setTimeout(function(){ 
                    tabla_diagnostico(idc);
                    tabla_receta(idc);
                    tabla_ordenes(idc);
                    tabla_interconsulta(idc);
                    tabla_orden_hospital(idc);
                    tabla_certificado_medico(idc);
                    tabla_justificante_medico(idc);
                    tabla_orden_libre(idc);
                }, 1500);
            }
            $('textarea.js-auto-size').textareaAutoSize();
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
    */ 
}
function select_tipo_consulta(){
    var tipo=$('#consulta_tipo option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/listado_consulta',
        data:{tipo:tipo,idp:$('#idpte').val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            setTimeout(function(){ 
                $('.tipo_text_consulta').html(data);
            }, 500);
        }
    });
}
var aux_signov=0;
function ocultar_signosvitales(){
    if(aux_signov==0){
        aux_signov=1;
        $('.txt_signosvitales').css('display','none');
    }else{
        aux_signov=0;
        $('.txt_signosvitales').css('display','block');
    }    
}
function guardar_consulta(tipo){
    var datos = $('#form_consulta').serialize();
    $('.btn_consulta').prop('disabled', true);
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/registra_consulta',
        data: datos,
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            var idcli=data;
            $('#idclinica_e').val(idcli);
            if(tipo==1){
                $.toast({
                    heading: 'Éxito',
                    text: 'Se guardaron los cambios de forma exitosa',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
            }else{
                $('.btn_consulta').prop('disabled', false); 
            }
            ////Diagnósticos
            var DATA  = [];
            var TABLA   = $(".texto_diagnostico .ol_diagnost_text > li");
            TABLA.each(function(){  
                if($(this).find("input[id*='diagnos_d']").val()!=''){
                    item = {};
                    item ["idconsulta"] = idcli;
                    item ["iddiagnostico"] = $(this).find("input[id*='iddiagnostico_d']").val();
                    item ["diagnostico"] = $(this).find("input[id*='diagnos_d']").val();
                    DATA.push(item);
                }
            });
            INFO  = new FormData();
            aInfo   = JSON.stringify(DATA);
            INFO.append('data', aInfo);
            $.ajax({
                data: INFO, 
                type: 'POST',
                url : base_url + 'Pacientes/registro_diagnostico',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            }); 

            //Receta
            var DATAR  = [];
            var TABLAR   = $(".texto_recet .texto_receta > div");
            TABLAR.each(function(){  
                if($(this).find("input[id*='medicamento_tipo_r']").val()!=''){
                    item = {};
                    item ["idconsulta"] = idcli;
                    item ["idreceta"] = $(this).find("input[id*='idreceta_r']").val();
                    item ["indicaciones"] = $(this).find("input[id*='indicaciones_r']").val();
                    item ["idreceta_aux"] = $(this).find("input[id*='idreceta_aux']").val();
                    DATAR.push(item);
                }
            });
            var DATARM  = [];
            var TABLARM   = $(".txt_recet .txt_receta > li");
            TABLARM.each(function(){  
                if($(this).find("input[id*='medicamento_tipo_r']").val()!=''){
                    item = {};
                    item ["idmedicamento"] = $(this).find("input[id*='idmedicamento_r']").val();
                    item ["medi_receta_aux"] = $(this).find("input[id*='medi_receta_aux']").val();
                    item ["medicamento"] = $(this).find("input[id*='medicamento_tipo_r']").val();
                    item ["tomar"] = $(this).find("input[id*='tomar_medi_r']").val();
                    DATARM.push(item);
                }    
            });

            INFOR  = new FormData();
            aInfor   = JSON.stringify(DATAR);
            aInform   = JSON.stringify(DATARM);
            INFOR.append('data', aInfor);
            INFOR.append('datam', aInform);
            $.ajax({
                data: INFOR, 
                type: 'POST',
                url : base_url + 'Pacientes/registro_receta',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            });

            //Orden de estudio
            var DATAO  = [];
            var TABLAO   = $(".orden_estudio_txt .orden_estudio_texto > .orden_laboratorio");
            TABLAO.each(function(){  
                    item = {};
                    item ["idconsulta"] = idcli;
                    item ["idorden"] = $(this).find("input[id*='idorden_e']").val();
                    item ["idorden_aux"] = $(this).find("input[id*='idorden_aux_e']").val();
                    item ["recomendar"] = $(this).find("input[class*='recomendar_e']").is(':checked');
                    item ["nombre_laboratorio"] = $(this).find("input[id*='nombre_laboratorio_e']").val();
                    item ["direccion"] = $(this).find("input[id*='direccion_e']").val();
                    item ["telefono"] = $(this).find("input[id*='telefono_e']").val();
                    item ["observaciones"] = $(this).find("input[id*='observaciones_e']").val();
                    item ["incluir"] = $(this).find("input[class*='incluir_e']").is(':checked');
                    item ["diagnostico"] = $(this).find("input[id*='diagnostico_e']").val();
                    DATAO.push(item);
            });
            var DATAOL  = [];
            var TABLAOL  = $(".txt_orden_estud .txt_orden_estudio > li");
            TABLAOL.each(function(){  
                    item = {};
                    item ["idlaboratorio"] = $(this).find("input[id*='idlaboratorio_e']").val();
                    item ["laboratorio_aux"] = $(this).find("input[id*='laboratorio_aux_e']").val();
                    item ["detalles"] = $(this).find("input[id*='detalles_e']").val();
                DATAOL.push(item);
            });
            INFOO  = new FormData();
            aInfoo   = JSON.stringify(DATAO);
            aInfool   = JSON.stringify(DATAOL);
            INFOO.append('data', aInfoo);
            INFOO.append('datal', aInfool);
            $.ajax({
                data: INFOO, 
                type: 'POST',
                url : base_url + 'Pacientes/registro_ordenestudio',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            });
            // Interconuslta
            var DATAIN  = [];
            var TABLAIN   = $(".orden_estudio_txt .orden_estudio_texto > .interconsulta_txt");
            TABLAIN.each(function(){  
                item = {};
                item ["idconsulta"] = idcli;
                item ["idinterconsulta"] = $(this).find("input[id*='idinterconsulta_int']").val();
                item ["idinterconsulta_aux"] = $(this).find("input[id*='idinterconsulta_aux']").val();
                item ["incluir_diagnostico"] = $(this).find("input[class*='incluir_diagnostico_in']").is(':checked');
                item ["nombre_diagnostico"] = $(this).find("input[id*='nombre_diagnostico_in']").val();
                item ["motivo_interconsulta"] = $(this).find("input[id*='motivo_interconsulta_in']").val();
                DATAIN.push(item);
            });
            var DATAINM  = [];
            var TABLAINM   = $(".txt_interconsult .txt_interconsulta > .interconsulta_medicos_n");
            TABLAINM.each(function(){  
                item = {};
                item ["idmedico"] = $(this).find("input[id*='idmedico_in']").val();
                item ["meid_medico_aux"] = $(this).find("input[id*='meid_medico_aux']").val();
                item ["nombre"] = $(this).find("input[id*='nombre_in']").val();
                item ["especialidad"] = $(this).find("input[id*='especialidad_in']").val();
                item ["hospital"] = $(this).find("input[id*='hospital_in']").val();
                item ["no_consultorio"] = $(this).find("input[id*='no_consulta_in']").val();
                item ["domicilio"] = $(this).find("input[id*='domicilio_in']").val();
                item ["telefono"] = $(this).find("input[id*='telefono_in']").val();
                item ["ciudad"] = $(this).find("input[id*='ciudad_in']").val();
                DATAINM.push(item); 
            });

            INFOIN  = new FormData();
            aInfoin   = JSON.stringify(DATAIN);
            aInfoinm   = JSON.stringify(DATAINM);
            INFOIN.append('data', aInfoin);
            INFOIN.append('datam', aInfoinm);
            $.ajax({
                data: INFOIN, 
                type: 'POST',
                url : base_url + 'Pacientes/registro_interconsulta',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            });
            ///// Orden hospital
            var DATAH  = [];
            var TABLAH   = $(".orden_estudio_txt .orden_estudio_texto > .orden_hopital");
            TABLAH.each(function(){  
                if($(this).find("input[id*='detalles_h']").val()!=''){
                    item = {};
                    item ["idconsulta"] = idcli;
                    item ["idorden_hospital"] = $(this).find("input[id*='idorden_hospital_h']").val();
                    item ["detalles"] = $(this).find("input[id*='detalles_h']").val();
                    DATAH.push(item);
                }
            });
            INFOH  = new FormData();
            aInfoh   = JSON.stringify(DATAH);
            INFOH.append('data', aInfoh);
            $.ajax({
                data: INFOH, 
                type: 'POST',
                url : base_url + 'Pacientes/registro_orden_hospital',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            });
            ///// Certificado medico
            var DATAH  = [];
            var TABLAH   = $(".orden_estudio_txt .orden_estudio_texto > .certificado_medico");
            TABLAH.each(function(){  
                if($(this).find("textarea[id*='detalles_c']").val()!=''){
                    item = {};
                    item ["idconsulta"] = idcli;
                    item ["idcertificado_medico"] = $(this).find("input[id*='idcertificado_medico_c']").val();
                    item ["detalles"] = $(this).find("textarea[id*='detalles_c']").val();
                    DATAH.push(item);
                }
            });
            INFOH  = new FormData();
            aInfoh   = JSON.stringify(DATAH);
            INFOH.append('data', aInfoh);
            $.ajax({
                data: INFOH, 
                type: 'POST',
                url : base_url + 'Pacientes/registro_certificado_medico',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            });
            ///// Justificante médico
            var DATAJ  = [];
            var TABLAJ   = $(".orden_estudio_txt .orden_estudio_texto > .justificante_medico");
            TABLAJ.each(function(){  
                if($(this).find("textarea[id*='detalles_j']").val()!=''){
                    item = {};
                    item ["idconsulta"] = idcli;
                    item ["idjustificante"] = $(this).find("input[id*='idjustificante_j']").val();
                    item ["detalles"] = $(this).find("textarea[id*='detalles_j']").val();
                    DATAJ.push(item);
                }
            });
            INFOJ  = new FormData();
            aInfoj   = JSON.stringify(DATAJ);
            INFOJ.append('data', aInfoj);
            $.ajax({
                data: INFOJ, 
                type: 'POST',
                url : base_url + 'Pacientes/registro_justificante_medico',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            });
            ///// Orden libre
            var DATAOR  = [];
            var TABLAOR   = $(".orden_estudio_txt .orden_estudio_texto > .orden_libre");
            TABLAOR.each(function(){  
                if($(this).find("input[id*='detalles_o']").val()!=''){
                    item = {};
                    item ["idconsulta"] = idcli;
                    item ["idorden"] = $(this).find("input[id*='idorden_o']").val();
                    item ["detalles"] = $(this).find("textarea[id*='detalles_o']").val();
                    DATAOR.push(item);
                }
            });
            INFOOR  = new FormData();
            aInfoor   = JSON.stringify(DATAOR);
            INFOOR.append('data', aInfoor);
            $.ajax({
                data: INFOOR, 
                type: 'POST',
                url : base_url + 'Pacientes/registro_orden_libre',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                }
            });


            if(tipo==1){
                setTimeout(function(){ 
                    window.location.reload();
                }, 1500);
            }
        }
    });
}
function seleccionar_consultas(){
    $('.consultas_li').addClass('active');
    $('.consultas_li').attr('aria-selected', true);
    $('.hitoria_cli').removeClass('active');
    $('.hitoria_cli').attr('aria-selected', false);
    $('.consultas_li_ar').removeClass('active');
    $('.consultas_li_ar').attr('aria-selected', false);
    $('#relevante1').removeClass('active');
    $('#consultas2').addClass('active');
    existeconsulta()
}
function existeconsulta(){
    //var idconsulta_existe=$('#idconsulta_existe').val();
    //if(idconsulta_existe==0){
        /*
        $('.consulta').html('<div align="center"><br><br><br>\
                <h4>Aún no has hecho una consulta</h4>\
                <h6>Crea una nueva consulta haciendo click en el botón</h6>\
                <button type="button" class="btn waves-effect waves-light btn-info" onclick="txt_consulta(0)">Nueva Consulta</button>\
            </div>');
        */  
        var permiso_perfil1=$('#permiso_perfil1').val();
        var permiso_perfil2=$('#permiso_perfil2').val();
        var permiso_perfil3=$('#permiso_perfil3').val();
        var consulta1='';
        var consulta2='';
        var consulta3='';
        if(permiso_perfil1==1){
            consulta1='<div class="col-md-4">\
                            <div class="card">\
                                <button type="button" onclick="txt_consulta_medicina(0)" style="border-radius: 50px;">\
                                    <img class="card-img" style="border-radius: 50px;" src="'+base_url+'images/estetica.jpg" height="300" alt="Card image">\
                                    <div class="card-img-overlay card-inverse text-white social-profile d-flex justify-content-center" style="border-radius: 50px;">\
                                        <div class="align-self-center"> <img  src="'+base_url+'images/FAV.png" class="img-circle" width="100">\
                                            <h4 class="card-title">Medicina estética</h4>\
                                            <h6 class="card-subtitle"></h6>\
                                        </div>\
                                    </div>\
                                </button>\
                            </div>\
                        </div>';
        }else{
            consulta1='';
        }
        if(permiso_perfil2==1){
            consulta2='<div class="col-md-4">\
                            <div class="card">\
                                <button type="button" onclick="txt_consulta_spa(0)" style="border-radius: 50px;">\
                                    <img class="card-img" style="border-radius: 50px;" src="'+base_url+'images/spa.jpg" height="300" alt="Card image">\
                                    <div style="border-radius: 50px;" class="card-img-overlay card-inverse text-white social-profile d-flex justify-content-center">\
                                        <div class="align-self-center"> <img src="'+base_url+'images/FAV.png" class="img-circle" width="100">\
                                            <h4 class="card-title">SPA</h4>\
                                            <h6 class="card-subtitle"></h6>\
                                        </div>\
                                    </div>\
                                </button>\
                            </div>\
                        </div>';
        }else{
            consulta2='';
        }
        if(permiso_perfil3==1){
            consulta3='<div class="col-md-4">\
                            <div class="card">\
                                <button style="border-radius: 50px;" type="button" onclick="txt_consulta_nutricion(0)">\
                                <img style="border-radius: 50px;" class="card-img" src="'+base_url+'assets/images/background/socialbg.jpg" height="300" alt="Card image">\
                                <div style="border-radius: 50px;" class="card-img-overlay card-inverse text-white social-profile d-flex justify-content-center">\
                                    <div class="align-self-center"> <img src="'+base_url+'images/FAV.png" class="img-circle" width="100">\
                                        <h4 class="card-title">Nutrición</h4>\
                                        <h6 class="card-subtitle"></h6>\
                                    </div>\
                                </div>\
                                </button>\
                            </div>\
                        </div>';
        }else{
            consulta3='';
        }
        $('.consulta').html('<div class="row">'+consulta1+consulta2+consulta3+'</div>');  
    //}else{
      //  visualizar_consulta(idconsulta_existe);
    //}
}
/*
function btn_tipo_consulta(){
    $('.text_tipo_botones').css('display','block');
    $('.nueva_consulta').css('display','none');
}
*/
function visualizar_consulta(idc){
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/visualizar_consulta_text',
        data:{id:idc},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.consulta').html(data);
        }
    });
}
function agregar_diag(){
    agregar_diagnostico(0,'');
}
var aux_diagnostico=0;
function agregar_diagnostico(id,diagnostico){
    var html='';
    var btn_html='';
        if(aux_diagnostico==0){
        btn_html='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-info" onclick="agregar_diag()"><i class="fas fa-plus"></i></button>';
        }else{
        btn_html='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-secondary" onclick="diagnostico_remove('+aux_diagnostico+','+id+')"><i class="fas fa-minus"></i></button>';
        }
        /*
        btn_htmlf='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-secondary" onclick="fav_diag('+aux_diagnostico+')"><i class="fas fa-star fav_diag_icon_'+aux_diagnostico+'" style="color:darkgray;"></i></button>\
                    <input type="hidden" class="favorito_d_'+aux_diagnostico+'" id="favorito_d" value="0">';
        */
        html+='<li class="row_diag_'+aux_diagnostico+'">\
                    <div class="row">\
                        <div class="col-md-11">\
                            <div class="form-line">\
                                <input type="hidden" id="iddiagnostico_d" value="'+id+'">\
                                <input type="text" id="diagnos_d" class="form-control colorlabel_white js-auto-size diagnos_d_'+aux_diagnostico+'" value="'+diagnostico+'" oninput="buscar_diagnostico_txt('+aux_diagnostico+')">\
                            </div>\
                            <div style="position: relative;">\
                                <div class="diagnostico_buscar_t_'+aux_diagnostico+'" style="position: absolute; z-index: 3;">\
                                </div>\
                            </div>\
                        </div>\
                        <div class="col-md-1">\
                            '+btn_html+'\
                        </div>\
                    </div>\
                </li>';
    $('.ol_diagnost_text').append(html);
    $('textarea.js-auto-size').textareaAutoSize();
    aux_diagnostico++;
}
function diagnostico_remove(id,idd){
    if(idd==0){
        $('.row_diag_'+id).remove();
    }else{
        $('#elimina_diagnostico_modal').modal();
        $('#iddiagnostico_e').val(idd);
        $('#iddiagnostico_remove').val(id);
    }
}
function tabla_diagnostico(id){
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/get_diagnostico",
        data: {id:id},
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  agregar_diagnostico(element.iddiagnostico,element.diagnostico);
                });
            }else{
                agregar_diag();
            }   
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}
/*
function fav_diag(id){
    fav_aux=$('.favorito_d_'+id).val();
    if(fav_aux==0){
        $('.favorito_d_'+id).val(1);
        $('.fav_diag_icon_'+id).css('color','#ffe000 ');
    }else{
        $('.favorito_d_'+id).val(0);
        $('.fav_diag_icon_'+id).css('color','darkgray');
    }
}
*/
function archivardiagnostico(){
    var id=$('#iddiagnostico_e').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/archivar_diagnostico",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Se guardaron los cambios de forma exitosa',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_diagnostico_modal').modal('hide');
            $('.row_diag_'+$('#iddiagnostico_remove').val()).remove();
        }
    });  
}
function agregar_recet(){
    agregar_receta(0,'');
}
var aux_receta=0; 
function agregar_receta(idr,indicaciones){
    var html='';
    html+='<div class="row_receta_texto_'+aux_receta+'">\
                <div class="margen_div"></div>\
                <div class="row">\
                    <div class="col-md-12">\
                        <div class="margen_todo">\
                            <div class="card-body margen_left" style="padding: 0.25rem !important;">\
                                <div class="row">\
                                    <div class="col-md-12">\
                                        <div class="row">\
                                            <div class="col-md-8">\
                                                <h6 class="m-b-0"><i class="fas fa-file-medical"></i> Receta</h6>\
                                                <input type="hidden" id="idreceta_r" value="'+idr+'">\
                                                <input type="hidden" id="idreceta_aux" value="'+aux_receta+'">\
                                            </div>\
                                            <div class="col-md-4" align="right">\
                                                <div class="editarhistoriacli_btn">\
                                                    <button  type="button" class="btn waves-effect waves-light btn-sm btn-outline-danger" onclick="receta_remove('+aux_receta+','+idr+')"><i class="fas fa-trash-alt"></i></button>\
                                                </div>\
                                            </div>\
                                        </div>\
                                        <hr>\
                                        <!------->\
                                        <div class="row">\
                                            <div class="col-md-12">\
                                                <div class="txt_recet">\
                                                    <ol class="txt_receta ol_receta_'+aux_receta+'" id="ol_receta_'+aux_receta+'">\
                                                    </ol>\
                                                </div>\
                                            </div>\
                                        </div>\
                                        <!------->\
                                        <div class="row">\
                                            <div class="col-md-11">\
                                                    <label>Indicaciones Adicionales de la Receta:</label>\
                                                    <div class="form-group input-group mb-3 recordableHolder">\
                                                    <input type="text" id="indicaciones_r" value="'+indicaciones+'" class="form-control colorlabel_white recordable rinited">\
                                                    <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>\
                                                </div>\
                                            </div>\
                                        </div> \
                                        <div class="row">\
                                            <div class="col-md-6"></div>\
                                            <div class="col-md-6" align="right">\
                                                <br>\
                                                <button type="button" class="btn waves-effect waves-light btn-rounded btn-outline-info" onclick="imprimir_receta('+idr+')">Imprimir Receta (Carta)</button>\
                                                <!--<button type="button" class="btn waves-effect waves-light btn-rounded btn-outline-info">Imprimir Receta</button>-->\
                                            </div>\
                                            <!------->\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>';
    $('.texto_receta').append(html);
    if(idr==0){
       agregar_li_recet(aux_receta); 
    }else{
       tabla_li_receta(aux_receta,idr);
    }
    
    aux_receta++;     
}
function receta_remove(id,idr){
    if(idr==0){
        $('.row_receta_texto_'+id).remove();
    }else{
        $('#elimina_receta_modal').modal();
        $('#idreceta_e').val(idr);
        $('#idreceta_remove').val(id);
    }   
}
function  agregar_li_recet(id){
    agregar_li_receta(id,0,'','');
}
var aux_li_receta=0;
function agregar_li_receta(id,idli,receta,tomar){
    var html='';
    var btn_html='';
        btn_html+='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-info" onclick="agregar_li_recet('+id+')"><i class="fas fa-plus"></i></button>\
                  <button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-secondary" onclick="receta_li_remove('+id+','+aux_li_receta+','+idli+')"><i class="fas fa-minus"></i></button>';
        html+='<li class="row_li_receta_'+id+'_'+aux_li_receta+'">\
                <div class="row">\
                    <div class="col-md-5">\
                        <div class="form-line">\
                            <input type="hidden" id="idmedicamento_r" value="'+idli+'">\
                            <input type="hidden" id="medi_receta_aux" value="'+id+'">\
                            <input type="text" id="medicamento_tipo_r" placeholder="Medicamento" value="'+receta+'" class="form-control colorlabel_white medicamento_b_'+aux_li_receta+'_'+id+'" oninput="buscar_medicamento_txt('+aux_li_receta+','+id+')">\
                            <div style="position: relative;">\
                                <div class="medicamento_buscar_t_'+aux_li_receta+'_'+id+'" style="position: absolute; z-index: 3;">\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="col-md-1">\
                    </div>\
                    <div class="col-md-5">\
                        <div class="form-line">\
                            <input type="text" id="tomar_medi_r" placeholder="Prescripción" value="'+tomar+'"  class="form-control colorlabel_white tomar_b_'+aux_li_receta+'_'+id+'" oninput="buscar_tomar_txt('+aux_li_receta+','+id+')">\
                            <div style="position: relative;">\
                                <div class="tomar_buscar_t_'+aux_li_receta+'_'+id+'" style="position: absolute; z-index: 3;">\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="col-md-1">\
                        '+btn_html+'\
                    </div>    \
                </div>  \
            </li>';
    $('.ol_receta_'+id).append(html);
    aux_li_receta++;
}
function receta_li_remove(id,idli_aux,idli){
    if(idli==0){
        $('.row_li_receta_'+id+'_'+idli_aux).remove();
    }else{
        $('#elimina_receta_li_modal').modal();
        $('#idmedicamento_e').val(idli);
        $('#idmedicamento_remove').val(id);
        $('#idmedicamento_remove_aux').val(idli_aux);
    }
}
function tabla_receta(id){
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/get_receta",
        data: {id:id},
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  agregar_receta(element.idreceta,element.indicaciones);
                });
            }else{
                agregar_recet();
            }   
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}
function tabla_li_receta(id,idr){
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/get_li_receta",
        data: {id:idr},
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  agregar_li_receta(id,element.idmedicamento,element.medicamento,element.tomar);
                });
            }else{
                agregar_li_recet(id);
            }   
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}
function tabla_ordenes(id){
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/get_ordenes_estudio",
        data: {id:id},
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  agregar_orden_estudio(element.idorden,element.recomendar,element.nombre_laboratorio,element.direccion,element.telefono,element.observaciones,element.incluir,element.diagnostico);
                });
            }   
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}
function tabla_li_ordenes(id,idr){
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/get_li_ordenes_estudio",
        data: {id:idr},
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  agregar_li_ordenes(id,element.idlaboratorio,element.detalles);
                });
            }else{
                agregar_li_orden(id);
            }   
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}
function eliminarreceta(){
    var id=$('#idreceta_e').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/eliminar_receta",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Se guardaron los cambios de forma exitosa',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_receta_modal').modal('hide');
            $('.row_receta_texto_'+$('#idreceta_remove').val()).remove();
        }
    });  
}
function eliminarlireceta(){
    var id=$('#idmedicamento_e').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/eliminar_li_receta",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Se guardaron los cambios de forma exitosa',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_receta_li_modal').modal('hide');
            $('.row_li_receta_'+$('#idmedicamento_remove').val()+'_'+$('#idmedicamento_remove_aux').val()).remove();
        }
    });  
}
function imprimir_receta(id){
    if(id==0){
        $.toast({
            heading: '¡Atención!',
            text: 'Por favor primero guarde la consulta ',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }else{
        guardar_consulta(0);
        setTimeout(function(){ 
        window.open(base_url+'Pacientes/imprimireceta2/'+id,'_blank');  
    }, 1000);
        /*
        setTimeout(function(){ 
            var html='<iframe src="'+base_url+'Pacientes/imprimireceta/'+id+'"></iframe>';
            $('.iframereceta').html('');
            $('.iframereceta').html(html);
        }, 1500);
        */
    }        
}

function imprimir_ultima_receta(id){
    //guardar_consulta(0);
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/consultar_ultima_receta",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            if(data==0){
                $.toast({
                    heading: '¡Atención!',
                    text: 'No existe receta',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }else{
                /*
                setTimeout(function(){ 
                    var html='<iframe src="'+base_url+'Pacientes/imprimireceta/'+data+'"></iframe>';
                    $('.iframereceta').html('');
                    $('.iframereceta').html(html);
                }, 1500);
                */
                window.open(base_url+'Pacientes/imprimireceta/'+data,'_blank');  
            }
        }
    });  
      
}

function imprimir_orden(id){
    if(id==0){
        $.toast({
            heading: '¡Atención!',
            text: 'Por favor primero guarde la consulta ',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }else{
        guardar_consulta(0);
        setTimeout(function(){ 
            window.open(base_url+'Pacientes/imprimirordenv2/'+id,'_blank');
        }, 1000);
        /*
        setTimeout(function(){ 
            var html='<iframe src="'+base_url+'Pacientes/imprimirordenv2/'+id+'"></iframe>';
            $('.iframereceta').html('');
            $('.iframereceta').html(html);
        }, 1500);
        */    
    }
    
}
function imprimir_pdf_interconsulta(id){
    if(id==0){
        $.toast({
            heading: '¡Atención!',
            text: 'Por favor primero guarde la consulta ',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }else{
        guardar_consulta(0);
        setTimeout(function(){ 
            window.open(base_url+'Pacientes/pdfinterconsulta/'+id,'_blank');  
        }, 1000);
    }
    
}
function agregar_orden_estud(){
    agregar_orden_estudio(0,0,'','','','',0,'');
}
var aux_ordenestudio=0;
function agregar_orden_estudio(idorden,recomendar,nombre_laboratorio,direccion,telefono,observaciones,incluir,diagnostico){
    var html='';
        var checke_r='';
        var style_r='';
        if(recomendar==1){
            checke_r='checked';
            style_r='display:block';
        }else{
            checke_r='';
            style_r='display:none';
        }
        var checke_in='';
        var style_in='';
        if(incluir==1){
            checke_in='checked';
            style_in='display:block';
        }else{
            checke_in='';
            style_in='display:none';
        }
        html+='<div class="orden_laboratorio  row_orden_'+aux_ordenestudio+'">\
                    <div class="row">\
                        <div class="col-md-12">\
                             <div class="card_borde bg-secon">\
                                <div class="row">\
                                    <div class="col-md-8">\
                                        <a class="m-b-0 btn-h btn btn-block textleft" onclick="ocultar_orden_estudio_text('+aux_ordenestudio+')">&nbsp;&nbsp;&nbsp;<span class="icon_orden_'+aux_ordenestudio+'"> <i class="fas fa-caret-down"></i> </span> Orden de estudio de laboratorio</a>\
                                    </div>\
                                    <div class="col-md-4" align="right">\
                                        <div class="editarhistoriacli_btn">\
                                            <button  type="button" class="btn waves-effect waves-light btn-sm btn-outline-danger" onclick="remover_ordenestudio('+aux_ordenestudio+','+idorden+')"><i class="fas fa-trash-alt"></i></button>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div> \
                    <div class="row texto_orden_'+aux_ordenestudio+'">\
                        <input type="hidden" id="click_style_orden_'+aux_ordenestudio+'" value="0">\
                        <div class="col-md-12">\
                            <div class="margen_todo_gris">\
                                <div class="card-body" style="background-color: #f8f9fa;">\
                                    <div class="row">\
                                        <div class="col-md-12">\
                                            <div class="margen_todo" style="background-color: white;">\
                                                <div class="card-body" style="padding: 0.25rem !important;">\
                                                    <input type="hidden" id="idorden_e" value='+idorden+'>\
                                                    <input type="hidden" id="idorden_aux_e" value='+aux_ordenestudio+'>\
                                                    <div class="row">\
                                                        <div class="col-md-12">\
                                                            <div class="custom-control custom-switch">\
                                                              <input type="checkbox" class="custom-control-input recomendar_e" id="recomendar_e_'+aux_ordenestudio+'" '+checke_r+' onclick="click_recomedada('+aux_ordenestudio+')">\
                                                              <label class="custom-control-label" for="recomendar_e_'+aux_ordenestudio+'">Recomendar laboratorio: (opcional)</label>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="text_recomendar_'+aux_ordenestudio+'" style="'+style_r+'">\
                                                        <div class="row">\
                                                            <div class="col-md-12">\
                                                                <div class="form-group row">\
                                                                    <label for="example-text-input" class="col-3 col-form-label">Nombre del laboratorio:</label>\
                                                                    <div class="col-9">\
                                                                        <input type="text" id="nombre_laboratorio_e" value="'+nombre_laboratorio+'" class="form-control colorlabel_white  laboratorio_d_'+aux_ordenestudio+'" oninput="buscar_laboratorio_txt('+aux_ordenestudio+')">\
                                                                        <div style="position: relative;">\
                                                                            <div class="laboratiro_buscar_t_'+aux_ordenestudio+'" style="position: absolute; z-index: 3;">\
                                                                            </div>\
                                                                        </div>\
                                                                    </div>\
                                                                </div>\
                                                            </div>\
                                                            <div class="col-md-12">\
                                                                <div class="form-group row">\
                                                                    <label for="example-text-input" class="col-3 col-form-label">Dirección:</label>\
                                                                    <div class="col-9">\
                                                                        <input type="text" id="direccion_e" value="'+direccion+'" class="form-control colorlabel_white direccion_e_'+aux_ordenestudio+'">\
                                                                    </div>\
                                                                </div>\
                                                            </div>\
                                                            <div class="col-md-12">\
                                                                <div class="form-group row">\
                                                                    <label for="example-text-input" class="col-3 col-form-label">Teléfono:</label>\
                                                                    <div class="col-9">\
                                                                        <input type="text" id="telefono_e" value="'+telefono+'" class="form-control colorlabel_white telefono_e_'+aux_ordenestudio+'"">\
                                                                    </div>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="row">\
                                                        <div class="col-md-12">\
                                                            <h4 class="div_abajo_solid_grosor" style="2px">Agregar estudios de laboratorio:</h4>\
                                                        </div>\
                                                    </div>\
                                                    <div class="row">\
                                                        <div class="col-md-12">\
                                                            <h5>Favor de realizar:</h5>\
                                                            <div class="txt_orden_estud">\
                                                                <ol class="txt_orden_estudio txt_orden_estudio_'+aux_ordenestudio+'">\
                                                                <ol>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-md-12">\
                                                            <label>Observaciones:</label>\
                                                            <div class="form-group input-group mb-3 recordableHolder">\
                                                                <input type="text" id="observaciones_e" value="'+observaciones+'" class="form-control colorlabel_white recordable rinited">\
                                                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-md-12">\
                                                            <div class="custom-control custom-switch">\
                                                              <input type="checkbox" class="custom-control-input incluir_e" id="incluir_e_'+aux_ordenestudio+'" '+checke_in+' onclick="click_incluir('+aux_ordenestudio+')">\
                                                              <label class="custom-control-label" for="incluir_e_'+aux_ordenestudio+'">Incluir diagnóstico:(opcional)</label>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-md-12 incluir_text_'+aux_ordenestudio+'" style="'+style_in+'">\
                                                            <div class="form-group">\
                                                                <input type="text" id="diagnostico_e" value="'+diagnostico+'" class="form-control colorlabel_white">\
                                                            </div>  \
                                                        </div>\
                                                        <div class="col-md-12" align="right">\
                                                            <button type="button" class="btn waves-effect waves-light btn-rounded btn-outline-info" onclick="imprimir_orden('+idorden+')">Imprimir orden</button>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="margen_div"></div>\
                </div>';
    $('.orden_estudio_texto').append(html);  
    
    if(idorden==0){
       agregar_li_orden(aux_ordenestudio);
    }else{
       tabla_li_ordenes(aux_ordenestudio,idorden);
    }
    aux_ordenestudio++;
}
function remover_ordenestudio(id,ido){
    if(ido==0){
        $('.row_orden_'+id).remove();
    }else{
        $('#elimina_orden_modal').modal();
        $('#idorden_e').val(ido);
        $('#idorden_remove').val(id);
    }
}
function eliminarorden(){
    var id=$('#idorden_e').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/eliminar_orden",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Se guardaron los cambios de forma exitosa',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_orden_modal').modal('hide');
            $('.row_orden_'+$('#idorden_remove').val()).remove();
        }
    });  
}
function eliminarliorden(){
    var id=$('#idlaborat_e').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/eliminar_li_orden",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Se guardaron los cambios de forma exitosa',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_orden_li_modal').modal('hide');
            $('.row_li_orden_'+$('#idlaboratorio_remove').val()+'_'+$('#idlaboratorio_remove_aux').val()).remove();
        }
    });  
}
function click_recomedada(idaux){
    if($('#recomendar_e_'+idaux).is(':checked')){
        $('.text_recomendar_'+idaux).css('display','block');
    }else{
        $('.text_recomendar_'+idaux).css('display','none');
    }  
}
function click_incluir(idaux){
    if($('#incluir_e_'+idaux).is(':checked')){
        $('.incluir_text_'+idaux).css('display','block');
    }else{
        $('.incluir_text_'+idaux).css('display','none');
    }  
}
function agregar_li_orden(id){
    agregar_li_ordenes(id,0,'');
}
aux_li_orden=0;
function agregar_li_ordenes(id,idli,detalles){
    var html='';
        btn_html='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-info" onclick="agregar_li_orden('+id+')"><i class="fas fa-plus"></i></button>\
                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-secondary" onclick="receta_li_orden_remove('+id+','+aux_li_orden+','+idli+')"><i class="fas fa-minus"></i></button>';
         html+='<li class="row_li_orden_'+id+'_'+aux_li_orden+'">\
                <div class="row">\
                    <div class="col-md-10">\
                        <div class="form-line">\
                            <input type="hidden" id="idlaboratorio_e" value="'+idli+'" class="form-control colorlabel_white">\
                            <input type="hidden" id="laboratorio_aux_e" value="'+id+'" class="form-control colorlabel_white">\
                            <input type="text" id="detalles_e" value="'+detalles+'" class="form-control colorlabel_white analisis_b_'+aux_li_orden+'_'+id+'"  oninput="buscar_analisis_txt('+aux_li_orden+','+id+')">\
                            <div style="position: relative;">\
                                <div class="analisis_buscar_t_'+aux_li_orden+'_'+id+'" style="position: absolute; z-index: 3;">\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="col-md-2">\
                         '+btn_html+'\
                    </div>\
                <div>\
            </li>';   
    $('.txt_orden_estudio_'+id).append(html);
    aux_li_orden++; 
}
function receta_li_orden_remove(id,idli_aux,idli){
    if(idli==0){
        $('.row_li_orden_'+id+'_'+idli_aux).remove();
    }else{
        $('#elimina_orden_li_modal').modal();
        $('#idlaborat_e').val(idli);
        $('#idlaboratorio_remove').val(id);
        $('#idlaboratorio_remove_aux').val(idli_aux);
    }
}

function selector_orden(){
    var selectorder=$('#selectorder option:selected').val();
    if(selectorder==1){
        agregar_orden_estud();
    }else if(selectorder==2){
        agregar_interconsulta_medic();
    }else if(selectorder==3){
        agregar_orden_hospitalizacio();
    }else if(selectorder==4){
        agregar_certificado_medic();
    }else if(selectorder==5){
        agregar_justificante_medic();
    }else if(selectorder==6){
        agregar_orden_libr();
    }
    setTimeout(function(){ 
        $('.select_ordenestudio').html('\
            <select id="selectorder" class="form-control" style="background-color: #03a9f3;color: #FFF;" onchange="selector_orden()">\
                <option selected="selected" disabled="" value="0">Agregar nueva orden</option>\
                <option value="1">Orden de estudios de laboratorio </option>\
                <option value="2">Orden de interconsulta</option>\
                <option value="3">Orden de hospitalización</option>\
                <option value="4">Certificado médico</option>\
                <option value="5">Justificante médico</option>\
                <option value="6" data-search="false">Orden libre</option>\
            </select>');
    }, 1000); 
}
function ocultar_orden_estudio_text(id){
    var int_orde=$('#click_style_orden_'+id).val();
    if(int_orde==0){
       $('.texto_orden_'+id).css('display','none');
       $('#click_style_orden_'+id).val(1); 
       $('.icon_orden_'+id).html('<i class="fas fa-caret-right"></i>');   
    }else{
       $('.texto_orden_'+id).css('display','block'); 
       $('#click_style_orden_'+id).val(0);  
       $('.icon_orden_'+id).html('<i class="fas fa-caret-down"></i>');     
    }
    
}
function modal_video_consulta(){
    $('#video_consulta_modal').modal();
    var d = new Date();
    var dia=d.getDate();//1 31
    var dias=d.getDay();//0 6
    var mes = d.getMonth();//0 11
    var yy = d.getFullYear();//9999
    var hr = d.getHours();//0 24
    var min = d.getMinutes();//0 59
    var seg = d.getSeconds();//0 59
    var yyy = 18;
    var ram = Math.floor((Math.random() * 10) + 1);
    var codigo=seg+''+min+''+hr+''+yyy+''+ram+''+mes+''+dia+''+dias;
    $('#link_video_consulta').html(base_url+'Videollamada/video_llamada#'+codigo);
    $('#codigo_video').val(codigo);
}
function copiar_portapapel(idelemento){
  var aux = document.createElement("div");
  aux.setAttribute("contentEditable", true);
  aux.innerHTML = document.getElementById(idelemento).innerHTML;
  aux.setAttribute("onfocus", "document.execCommand('selectAll',false,null)"); 
  document.body.appendChild(aux);
  aux.focus();
  document.execCommand("copy");
  document.body.removeChild(aux);
    
    $.toast({
        heading: 'Éxito',
        text: 'El texto se copió al portapapeles',
        position: 'top-right',
        loaderBg:'#ff6849',
        icon: 'success',
        hideAfter: 3500, 
        stack: 6
    });
}
function ejecutar(idelemento){

}
function iniciar_video_consulta(){
    $('#video_consulta_modal').modal('hide');
    var url_text=$('#link_video_consulta').text();
     window.open(url_text, "Prefactura", "width=400, height=612");
}
function qr_firmar_aviso(id,tipo){
    var num_f=$('#firma_id_text').val();
    if(num_f==0){
        $('.firma_aviso_text').css('display','block');
        $('#firma_id_text').val(1); 
    }else{
        $('.firma_aviso_text').css('display','none');
        $('#firma_id_text').val(0);
    }
}
///
function modal_eliminar_doc_legal(id){
    $('#elimnar_doc_modal_legal').modal();
    $('#iddocumento_e').val(id);
}
function eliminardoc_legal(){
    var id = $('#iddocumento_e').val();
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/eliminar_doc_legal",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Eliminado Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimnar_doc_modal_legal').modal('hide');
            window.location.reload();
        }
    }); 
}
///
function modal_eliminar_doc(id){
    $('#elimnar_doc_modal').modal();
    $('#idpaciente_doc').val(id);
}
function eliminardoc(){
    var id = $('#idpaciente_doc').val();
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/eliminar_doc",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Eliminado Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimnar_doc_modal').modal('hide');
            window.location.reload();
        }
    }); 
}
function enviar_videollamda(){
    var id = $('#idpte').val();
    var correo = $('#correo_videollamda').val();
    var direc= $('#link_video_consulta').text();
    if(correo!=''){
        $.toast({
            heading: 'Éxito',
            text: 'Se esta enviado el correo',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
        });
        $.ajax({
            type:'POST',
            url: base_url+"Pacientes/enviar_video_llamada",
            data:{id:id,correo:correo,direcc:direc},
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Enviado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
            }
        }); 
    }else{
        $.toast({
            heading: '¡Atención!',
            text: 'El campo debe estar lleno',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }    
}

function acepta_utilicen_datos(id){
    var check_aceptar=0;
    if($('#check_acepta').is(':checked')){
        check_aceptar=1;
    }else{
        check_aceptar=0;
    }
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/datos_utilizar",
        data:{id:id,che_acet:check_aceptar},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Se guardaron los cambios de forma exitosa',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
              });
        }
    }); 
}
///// Funcion para guardar el tipo de archivo
function add_file_doc_legal(id,tipo){
    var archivo=$('#doc_legal').val()
    //var name=$('#foto_avatar').val()
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg",".pdf");
    permitida = false;
    if($('#doc_legal')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('doc_legal');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('id',id);
            data.append('tipo',tipo);
            $.ajax({
                url:base_url+'Pacientes/cargafiles_doc_legal',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                    $.toast({
                        heading: 'Éxito',
                        text: 'Guardado Correctamente',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 3500, 
                        stack: 6
                    });
                    window.location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }        
}
/////
function add_file_doc(id){
    var archivo=$('#doc').val()
    //var name=$('#foto_avatar').val()
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg",".pdf");
    permitida = false;
    if($('#doc')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('doc');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('id',id);
            $.ajax({
                url:base_url+'Pacientes/cargafiles_doc',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                    $.toast({
                        heading: 'Éxito',
                        text: 'Guardado Correctamente',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 3500, 
                        stack: 6
                    });
                    window.location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }        
}
function agregar_orden_hospitalizacio(){
    agregar_orden_hospitalizacion(0,'');
}
var aux_orden_hosp=0;
function agregar_orden_hospitalizacion(id,detalle){
    var nombre_paciente=$('#nombre_paciente_text').val();
    var html='<div class="orden_hopital row_orden_hospi_'+aux_orden_hosp+'">\
                <div class="row">\
                    <div class="col-md-12">\
                         <div class="card_borde bg-secon">\
                            <div class="row">\
                                <div class="col-md-8">\
                                    <a class="m-b-0 btn-h btn btn-block textleft" onclick="ocultar_orden_host_text('+aux_orden_hosp+')">&nbsp;&nbsp;&nbsp;<span class="icon_orden_host_'+aux_orden_hosp+'"> <i class="fas fa-caret-down"></i> </span>  Orden de hospitalización</a>\
                                </div>\
                                <div class="col-md-4" align="right">\
                                    <div class="editarhistoriacli_btn">\
                                        <button  type="button" class="btn waves-effect waves-light btn-sm btn-outline-danger" onclick="remover_orden_hospi('+aux_orden_hosp+','+id+')"><i class="fas fa-trash-alt"></i></button>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
                <div class="row texto_orden_hosp_'+aux_orden_hosp+'">\
                    <input type="hidden" id="click_style_orden_host_'+aux_orden_hosp+'" value="0">\
                    <div class="col-md-12">\
                        <div class="margen_todo_gris">\
                            <div class="card-body" style="background-color: #f8f9fa;">\
                                <div class="row">\
                                    <div class="col-md-12">\
                                        <div class="margen_todo" style="background-color: white;">\
                                            <div class="card-body" style="padding: 0.25rem !important;">\
                                                <div class="text_recomendar" style="">\
                                                    <div class="row">\
                                                        <div class="col-md-12">\
                                                            <h6>Favor de hospitalizar a: '+nombre_paciente+'</h6>\
                                                        </div>\
                                                        <div class="col-md-12">\
                                                            <input type="hidden" id="idorden_hospital_h" value="'+id+'">\
                                                            <div class="form-group input-group mb-3 recordableHolder">\
                                                                <input type="text" id="detalles_h" value="'+detalle+'" class="form-control colorlabel_white recordable rinited">\
                                                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                </div>  \
                                                <br>\
                                                <div class="row" align="right">\
                                                    <div class="col-md-12" align="right">\
                                                        <button type="button" class="btn waves-effect waves-light btn-rounded btn-outline-info" onclick="imprimir_pdf_hospitalizacion('+id+')">Imprimir orden</button>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
                <div class="margen_div"></div>\
            </div>';
    $('.orden_estudio_texto').append(html);        
    aux_orden_hosp++;        
}
function remover_orden_hospi(aux,id){
    if(id==0){
        $('.row_orden_hospi_'+aux).remove();
    }else{
        $('#elimina_orden_hosp_modal').modal();
        $('#idorden_hospital_e').val(id);
        $('#idorden_hospital_remove').val(aux);
    }
}
function ocultar_orden_host_text(id){
    var int_orde=$('#click_style_orden_host_'+id).val();
    if(int_orde==0){
       $('.texto_orden_hosp_'+id).css('display','none');
       $('#click_style_orden_host_'+id).val(1); 
       $('.icon_orden_host_'+id).html('<i class="fas fa-caret-right"></i>');   
    }else{
       $('.texto_orden_hosp_'+id).css('display','block'); 
       $('#click_style_orden_host_'+id).val(0);  
       $('.icon_orden_host_'+id).html('<i class="fas fa-caret-down"></i>');     
    }  
}
function tabla_orden_hospital(id){
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/get_orden_hospital",
        data: {id:id},
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  agregar_orden_hospitalizacion(element.idorden_hospital,element.detalles);
                });
            }   
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}
function eliminarordenhospi(){
    var id = $('#idorden_hospital_e').val();
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/eliminar_orden_hospi",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Eliminado Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_orden_hosp_modal').modal('hide');
            $('.row_orden_hospi_'+$('#idorden_hospital_remove').val()).remove();

        }
    }); 
}
function agregar_certificado_medic(){
    agregar_certificado_medico(0,'');
}
var aux_certificado_m=0;
function agregar_certificado_medico(id,detalle){
    var administrador=$('#administrador').val();
    var nombre_paciente=$('#nombre_paciente_text').val();
    var edad_text=$('#edad_text').val();
    var sexo_text=$('#sexo_text').val();
    var sexo_t='';
    if(sexo_text==1){
        sexo_t='Masculino'; 
    }else{
        sexo_t='Femenino';
    }
    var aquien_txt='';
    if(id==0){
        aquien_txt='Después de haber practicado un examen médico minucioso, a '+nombre_paciente+' de sexo '+sexo_t+' con edad '+edad_text+' años de edad, certifico que tiene el siguiente diagnóstico:';
    }else{
        aquien_txt='';
    }
    var html='<div class="certificado_medico row_certificado_'+aux_certificado_m+'">\
                <div class="row">\
                    <div class="col-md-12">\
                         <div class="card_borde bg-secon">\
                            <div class="row">\
                                <div class="col-md-8">\
                                    <a class="m-b-0 btn-h btn btn-block textleft" onclick="ocultar_certificado_text('+aux_certificado_m+')">&nbsp;&nbsp;&nbsp;<span class="icon_certificado_'+aux_certificado_m+'"> <i class="fas fa-caret-down"></i> </span> Certificado Médico</a>\
                                </div>\
                                <div class="col-md-4" align="right">\
                                    <div class="editarhistoriacli_btn">\
                                        <button  type="button" class="btn waves-effect waves-light btn-sm btn-outline-danger" onclick="remover_certicado_medico('+aux_certificado_m+','+id+')"><i class="fas fa-trash-alt"></i></button>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
                <div class="row texto_certifi_'+aux_certificado_m+'">\
                    <input type="hidden" id="click_style_certifi_'+aux_certificado_m+'" value="0">\
                    <div class="col-md-12">\
                        <div class="margen_todo_gris">\
                            <div class="card-body" style="background-color: #f8f9fa;">\
                                <div class="row">\
                                    <div class="col-md-12">\
                                        <div class="margen_todo" style="background-color: white;">\
                                            <div class="card-body" style="padding: 0.25rem !important;">\
                                                <div class="text_recomendar" style="">\
                                                    <div class="row">\
                                                        <div class="col-md-12">\
                                                            <h6 align="center"><strong><u>Certificado Médico</u></strong></h6>\
                                                            <h6>A quien corresponda:</h6>\
                                                        </div>\
                                                        <div class="col-md-12">\
                                                            <input type="hidden" id="idcertificado_medico_c" value="'+id+'">\
                                                            <div class="form-group input-group mb-3 recordableHolder">\
                                                                <textarea type="text" id="detalles_c" class="form-control colorlabel_white recordable rinited js-auto-size">'+aquien_txt+detalle+'</textarea>\
                                                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-md-12">\
                                                          <h6>De requerir mayor información quedo a sus órdenes.</h6>\
                                                          <br>\
                                                          <h6>Médico Tratante: Dr. DANTE OROPEZA CANTO <br>Especialidad: Neurología  Cédula: 2018958 / 3413958</h6>\
                                                          <!--<h6>Especialidad:Fisioterapia  Ced. Esp.:</h6>-->\
                                                          <!--<h6>Teléfono de contacto: 2224269705</h6>-->\
                                                        </div>\
                                                    </div>\
                                                </div>  \
                                                <br>\
                                                <div class="row" align="right">\
                                                    <div class="col-md-12" align="right">\
                                                        <button type="button" class="btn waves-effect waves-light btn-rounded btn-outline-info" onclick="imprimir_pdf_certificado_medico('+id+')">Imprimir orden</button>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
                <div class="margen_div"></div>\
            </div>'; 
    $('.orden_estudio_texto').append(html);        
    aux_certificado_m++;        
}
function ocultar_certificado_text(id){
    var int_orde=$('#click_style_certifi_'+id).val();
    if(int_orde==0){
       $('.texto_certifi_'+id).css('display','none');
       $('#click_style_certifi_'+id).val(1); 
       $('.icon_certificado_'+id).html('<i class="fas fa-caret-right"></i>');   
    }else{
       $('.texto_certifi_'+id).css('display','block'); 
       $('#click_style_certifi_'+id).val(0);  
       $('.icon_certificado_'+id).html('<i class="fas fa-caret-down"></i>');     
    }  
}
function remover_certicado_medico(aux,id){
    if(id==0){
        $('.row_certificado_'+aux).remove();
    }else{
        $('#elimina_certificado_modal').modal();
        $('#idcertificado_medico_e').val(id);
        $('#idcertificado_medico_remove').val(aux);
    }
}
function eliminarcertificado(){
    var id = $('#idcertificado_medico_e').val();
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/eliminar_certificado",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Eliminado Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_certificado_modal').modal('hide');
            $('.row_certificado_'+$('#idcertificado_medico_remove').val()).remove();

        }
    }); 
}
function tabla_certificado_medico(id){
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/get_certificado",
        data: {id:id},
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  agregar_certificado_medico(element.idcertificado_medico,element.detalles);
                });
            }   
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}
function buscar_diagnostico_txt(id){
    id_diag_aux=id;
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/buscardignostico',
        data:{nombre:$('.diagnos_d_'+id).val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.diagnostico_buscar_t_'+id).html(data);
        }
    });
}
function diagnostico_select(id){
    var dtext = $('.diag_text'+id).text(); 
    $('.diagnos_d_'+id_diag_aux).val(dtext);
    $('.diagnostico_buscar_t_'+id_diag_aux).html('');
}
/*
function enviar_whatsapp(){
    var idpte=$('#idpte').val();
    var cel=$('.info_pas').data('celular_w');
    var lada=$('#lada option:selected').val();
    window.open('https://wa.me/'+lada+''+cel+'?text=Buen%20d%C3%ADa,%20su%20médico%20le%20ha%20enviado%20una%20Receta%20Electrónica,%20para%20consultarla%20ingrese%20al%20siguiente%20enlace:%20'+base_url+'Parkinson/consulta/'+idpte, '_blank');
}
*/
function imprimir_resumen(id){
    window.open(base_url+'Pacientes/imprimiresumen/'+id,'_blank');  
}
function buscar_laboratorio_txt(id){
    id_laboratorio_aux=id;
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/buscarlaboratorio',
        data:{nombre:$('.laboratorio_d_'+id).val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.laboratiro_buscar_t_'+id).html(data);
        }
    });
}
function laboratorio_select(id){
    $('.laboratorio_d_'+id_laboratorio_aux).val($('.orden_laboratorio_'+id).data('nombre_laboratorio'));
    $('.direccion_e_'+id_laboratorio_aux).val($('.orden_laboratorio_'+id).data('direccion'));
    $('.telefono_e_'+id_laboratorio_aux).val($('.orden_laboratorio_'+id).data('telefono'));
    $('.laboratiro_buscar_t_'+id_laboratorio_aux).html('');
}
/*
function imprimir_resumen(id){
    setTimeout(function(){ 
        var html='<iframe src="'+base_url+'Pacientes/imprimiresumen/'+id+'"></iframe>';
        $('.iframereceta').html('');
        $('.iframereceta').html(html);
    }, 1500);
}
*/
function buscar_medicamento_txt(aux,id){
    id_medi_aux=aux;
    id_medi_id_aux=id;
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/buscarmedicamento',
        data:{nombre:$('.medicamento_b_'+aux+'_'+id).val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.medicamento_buscar_t_'+aux+'_'+id).html(data);
        }
    });
}
function medicamento_select(id){
    var dtext = $('.medica_text'+id).text(); 
    $('.medicamento_b_'+id_medi_aux+'_'+id_medi_id_aux).val(dtext);
    $('.medicamento_buscar_t_'+id_medi_aux+'_'+id_medi_id_aux).html('');
}
function buscar_tomar_txt(aux,id){
    id_tomar_aux=aux;
    id_tomar_id_aux=id;
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/buscartomar',
        data:{nombre:$('.tomar_b_'+aux+'_'+id).val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.tomar_buscar_t_'+aux+'_'+id).html(data);
        }
    });
}

function tomar_select(id){
    var dtext = $('.tomar_text'+id).text(); 
    $('.tomar_b_'+id_tomar_aux+'_'+id_tomar_id_aux).val(dtext);
    $('.tomar_buscar_t_'+id_tomar_aux+'_'+id_tomar_id_aux).html('');
}

function buscar_analisis_txt(aux,id){
    id_analisis_aux=aux;
    id_analisis_id_aux=id;
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/buscaranalisis',
        data:{nombre:$('.analisis_b_'+aux+'_'+id).val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.analisis_buscar_t_'+aux+'_'+id).html(data);
        }
    });
}

function analisis_select(id){
    var dtext = $('.analisis_text'+id).text(); 
    $('.analisis_b_'+id_analisis_aux+'_'+id_analisis_id_aux).val(dtext);
    $('.analisis_buscar_t_'+id_analisis_aux+'_'+id_analisis_id_aux).html('');
}

///////////////////////////
function agregar_interconsulta_medic(){
    agregar_interconsulta_medico(0,0,'','');
}
var aux_interconsulta_m=0;
function agregar_interconsulta_medico(id,incluir_diagnostico,nombre_diagnostico,motivo_interconsulta){
    var nombre_paciente=$('#nombre_paciente_text').val();
    var checke_r='';
    var style_r='';
    if(incluir_diagnostico==1){
        checke_r='checked';
        style_r='display:block';
    }else{
        checke_r='';
        style_r='display:none';
    }
    var html='<div class="interconsulta_txt row_interconsulta_'+aux_interconsulta_m+'">\
                <div class="row">\
                    <div class="col-md-12">\
                         <div class="card_borde bg-secon">\
                            <div class="row">\
                                <div class="col-md-8">\
                                    <a class="m-b-0 btn-h btn btn-block textleft" onclick="ocultar_interconsulta_text('+aux_interconsulta_m+')">&nbsp;&nbsp;&nbsp;<span class="icon_interconsulta_'+aux_interconsulta_m+'"> <i class="fas fa-caret-down"></i> </span> Orden de interconsulta</a>\
                                </div>\
                                <div class="col-md-4" align="right">\
                                    <div class="editarhistoriacli_btn">\
                                        <button  type="button" class="btn waves-effect waves-light btn-sm btn-outline-danger" onclick="remover_interconsulta_medico('+aux_interconsulta_m+','+id+')"><i class="fas fa-trash-alt"></i></button>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
                <div class="row texto_intercon_'+aux_interconsulta_m+'">\
                    <input type="hidden" id="click_style_intercon_'+aux_interconsulta_m+'" value="0">\
                    <input type="hidden" id="idinterconsulta_int" value="'+id+'">\
                    <input type="hidden" id="idinterconsulta_aux" value="'+aux_interconsulta_m+'">\
                    <div class="col-md-12">\
                        <div class="margen_todo_gris">\
                            <div class="card-body" style="background-color: #f8f9fa;">\
                                <div class="row">\
                                    <div class="col-md-12">\
                                        <div class="margen_todo" style="background-color: white;">\
                                            <div class="txt_interconsult">\
                                                <div class="txt_interconsulta ul_interconsulta_'+aux_interconsulta_m+'"></div>\
                                            </div>\
                                            <div class="row" align="right">\
                                                <div class="col-md-12" align="right">\
                                                    <button type="button" class="btn btn-sm waves-light btn-rounded btn-outline-info" onclick="agregar_li_interconsulta_medic('+aux_interconsulta_m+')"><i class="fas fa-plus-circle icono-rey"></i> Nuevo médico</button>\
                                                </div>\
                                            </div>\
                                            <div class="margen_div"></div>\
                                            <div class="margen_todo" style="background-color: white;">\
                                                <div class="card-body" style="padding: 0.25rem !important;">\
                                                    <div class="text_recomendar" style="">\
                                                        <div class="row">\
                                                            <div class="col-md-12">\
                                                                <h6>Orden de interconsulta</h6>\
                                                                <h6>Favor de valorar a: '+nombre_paciente+'</h6>\
                                                            </div>\
                                                        </div>\
                                                        <div class="row">\
                                                            <div class="col-md-12">\
                                                                <div class="custom-control custom-switch">\
                                                                  <input type="checkbox" class="custom-control-input incluir_diagnostico_in" id="incluir_int_'+aux_interconsulta_m+'" '+checke_r+' onclick="click_incluir_intercon('+aux_interconsulta_m+')">\
                                                                  <label class="custom-control-label" for="incluir_int_'+aux_interconsulta_m+'">Incluir diagnóstico (opcional) </label>\
                                                                </div>\
                                                                <br>\
                                                            </div>\
                                                        </div>\
                                                        <div class="text_incluir_interconsulta_'+aux_interconsulta_m+'" style="'+style_r+'">\
                                                            <div class="row">\
                                                                <div class="col-md-12">\
                                                                    <div class="form-group row">\
                                                                        <div class="col-9">\
                                                                            <input type="text" id="nombre_diagnostico_in" value="'+nombre_diagnostico+'" class="form-control colorlabel_white" placeholder="Nombre del diagnóstico">\
                                                                        </div>\
                                                                    </div>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="row">\
                                                            <div class="col-md-12">\
                                                                <div class="form-group">\
                                                                    <label >Motivo de interconsulta:</label>\
                                                                    <input type="text" id="motivo_interconsulta_in" value="'+motivo_interconsulta+'" class="form-control colorlabel_white" placeholder="Escriba el motivo de interconsulta">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                            <div class="row" align="right">\
                                                <div class="col-md-12" align="right">\
                                                    <br>\
                                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-outline-info" onclick="imprimir_pdf_interconsulta('+id+')">Imprimir orden</button>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
                <div class="margen_div"></div>\
            </div>'; 
    $('.orden_estudio_texto').append(html);     
    if(id==0){
       agregar_li_interconsulta_medic(aux_interconsulta_m);
    }else{
       tabla_li_interconsulta_medico(aux_interconsulta_m,id);
    }   
    aux_interconsulta_m++;        
}

function ocultar_interconsulta_text(id){
    var int_orde=$('#click_style_intercon_'+id).val();
    if(int_orde==0){
       $('.texto_intercon_'+id).css('display','none');
       $('#click_style_intercon_'+id).val(1); 
       $('.icon_interconsulta_'+id).html('<i class="fas fa-caret-right"></i>');   
    }else{
       $('.texto_intercon_'+id).css('display','block'); 
       $('#click_style_intercon_'+id).val(0);  
       $('.icon_interconsulta_'+id).html('<i class="fas fa-caret-down"></i>');     
    }  
}

function remover_interconsulta_medico(aux,id){
    if(id==0){
        $('.row_interconsulta_'+aux).remove();
    }else{
        $('#elimina_interconsulta_modal').modal();
        $('#idinterconsulta_medico_e').val(id);
        $('#idinterconsulta_medico_remove').val(aux);
    }
}

function eliminarinterconsulta(){
    var id = $('#idinterconsulta_medico_e').val();
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/eliminar_interconsulta",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Eliminado Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_interconsulta_modal').modal('hide');
            $('.row_interconsulta_'+$('#idinterconsulta_medico_remove').val()).remove();
        }
    }); 
}

function agregar_li_interconsulta_medic(id){
    agregar_li_interconsulta_medico(id,0,'','','','','','','');
}
aux_li_interconsulta_medico=0;
function agregar_li_interconsulta_medico(id,idli,nombre,especialidad,hospital,no_consultorio,domicilio,telefono,ciudad){
    var html='';
         html+='<div class="interconsulta_medicos_n row_li_interconsulta_'+id+'_'+aux_li_interconsulta_medico+'">\
                    <div class="margen_todo" style="background-color: white;">\
                        <div class="card-body" style="padding: 0.25rem !important;">\
                            <div class="text_medico" style="">\
                                <input type="hidden" id="idmedico_in" value="'+idli+'">\
                                <input type="hidden" id="meid_medico_aux" value="'+id+'">\
                                <div class="row">\
                                    <div class="col-md-12" align="right">\
                                        <button  type="button" class="btn waves-effect waves-light btn-sm btn-outline-danger" onclick="remover_il_interconsulta_medico('+id+','+aux_li_interconsulta_medico+','+idli+')"><i class="fas fa-trash-alt"></i></button>\
                                    </div>\
                                </div>\
                                <div class="row">\
                                    <div class="col-md-12">\
                                        <div class="form-group row">\
                                            <label for="example-text-input" class="col-3 col-form-label">Nombre:</label>\
                                            <div class="col-9">\
                                                <input type="text" id="nombre_in" value="'+nombre+'" class="form-control colorlabel_white medico_b_'+aux_li_interconsulta_medico+'_'+id+'"  oninput="buscar_medico_txt('+aux_li_interconsulta_medico+','+id+')" placeholder="Nombre del médico">\
                                                <div style="position: relative;">\
                                                    <div class="medico_buscar_t_'+aux_li_interconsulta_medico+'_'+id+'" style="position: absolute; z-index: 3;">\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <div class="col-md-12">\
                                        <div class="form-group row">\
                                            <label for="example-text-input" class="col-3 col-form-label">Especialidad:</label>\
                                            <div class="col-9">\
                                                <input type="text" id="especialidad_in" value="'+especialidad+'" class="form-control colorlabel_white especialidad_b_'+aux_li_interconsulta_medico+'_'+id+'" placeholder="Especialidad">\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <div class="col-md-12">\
                                        <div class="form-group row">\
                                            <label for="example-text-input" class="col-3 col-form-label">Hospital/Clínica:</label>\
                                            <div class="col-9">\
                                                <input type="text" id="hospital_in" value="'+hospital+'" class="form-control colorlabel_white hospital_b_'+aux_li_interconsulta_medico+'_'+id+'" placeholder="Hospital/Clínica">\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <div class="col-md-12">\
                                        <div class="form-group row">\
                                            <label for="example-text-input" class="col-3 col-form-label">No. de consultorio:</label>\
                                            <div class="col-9">\
                                                <input type="text" id="no_consulta_in" value="'+no_consultorio+'" class="form-control colorlabel_white no_consultorio_b_'+aux_li_interconsulta_medico+'_'+id+'" placeholder="No. de consultorio">\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <div class="col-md-12">\
                                        <div class="form-group row">\
                                            <label for="example-text-input" class="col-3 col-form-label">Domicilio:</label>\
                                            <div class="col-9">\
                                                <input type="text" id="domicilio_in" value="'+domicilio+'" class="form-control colorlabel_white domicilio_b_'+aux_li_interconsulta_medico+'_'+id+'" placeholder="Domicilio">\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <div class="col-md-12">\
                                        <div class="form-group row">\
                                            <label for="example-text-input" class="col-3 col-form-label">Tel. Consultorio:</label>\
                                            <div class="col-9">\
                                                <input type="text" id="telefono_in" value="'+telefono+'" class="form-control colorlabel_white telefono_b_'+aux_li_interconsulta_medico+'_'+id+'" placeholder="Teléfono del consultorio">\
                                            </div>\
                                        </div>\
                                    </div>\
                                    <div class="col-md-12">\
                                        <div class="form-group row">\
                                            <label for="example-text-input" class="col-3 col-form-label">Ciudad</label>\
                                            <div class="col-9">\
                                                <input type="text" id="ciudad_in" value="'+ciudad+'" class="form-control colorlabel_white ciudad_b_'+aux_li_interconsulta_medico+'_'+id+'" placeholder="Ciudad">\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                           </div>\
                        </div>\
                    </div>\
                    <br>';   
    $('.ul_interconsulta_'+id).append(html);
    aux_li_interconsulta_medico++; 
}

function remover_il_interconsulta_medico(id,idli_aux,idli){
    if(idli==0){
        $('.row_li_interconsulta_'+id+'_'+idli_aux).remove();
    }else{
        $('#elimina_interconsulta_li_modal').modal();
        $('#idinterconsulta_e').val(idli);
        $('#idinterconsulta_remove').val(id);
        $('#idinterconsulta_remove_aux').val(idli_aux);
    }
}

function eliminarilinterconsulta(){
    var id=$('#idinterconsulta_e').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/eliminar_li_interconsulta",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Se guardaron los cambios de forma exitosa',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_interconsulta_li_modal').modal('hide');
            $('.row_li_interconsulta_'+$('#idinterconsulta_remove').val()+'_'+$('#idinterconsulta_remove_aux').val()).remove();
        }
    });  
}
function click_incluir_intercon(idaux){
    if($('#incluir_int_'+idaux).is(':checked')){
        $('.text_incluir_interconsulta_'+idaux).css('display','block');
    }else{
        $('.text_incluir_interconsulta_'+idaux).css('display','none');
    }  
}

function tabla_interconsulta(id){
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/get_ordenes_interconsulta",
        data: {id:id},
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  agregar_interconsulta_medico(element.idinterconsulta,element.incluir_diagnostico,element.nombre_diagnostico,element.motivo_interconsulta);
                });
            }   
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}

function tabla_li_interconsulta_medico(id,idr){
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/get_li_ordenes_interconsulta_medico",
        data: {id:idr},
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  agregar_li_interconsulta_medico(id,element.id,element.nombre,element.especialidad,element.hospital,element.no_consultorio,element.domicilio,element.telefono,element.ciudad);
                });
            }else{
                agregar_li_interconsulta_medic(id);
            }   
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}

function buscar_medico_txt(aux,id){
    id_medico_aux=aux;
    id_medico_id_aux=id;
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/buscarmedico',
        data:{nombre:$('.medico_b_'+aux+'_'+id).val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.medico_buscar_t_'+aux+'_'+id).html(data);
        }
    });
}

function medico_select(id){
    $('.medico_b_'+id_medico_aux+'_'+id_medico_id_aux).val($('.medico_int_'+id).data('medico'));
    $('.especialidad_b_'+id_medico_aux+'_'+id_medico_id_aux).val($('.medico_int_'+id).data('especialidad'));
    $('.hospital_b_'+id_medico_aux+'_'+id_medico_id_aux).val($('.medico_int_'+id).data('hospital'));
    $('.no_consultorio_b_'+id_medico_aux+'_'+id_medico_id_aux).val($('.medico_int_'+id).data('no_consultorio'));
    $('.domicilio_b_'+id_medico_aux+'_'+id_medico_id_aux).val($('.medico_int_'+id).data('domicilio'));
    $('.telefono_b_'+id_medico_aux+'_'+id_medico_id_aux).val($('.medico_int_'+id).data('telefono'));
    $('.ciudad_b_'+id_medico_aux+'_'+id_medico_id_aux).val($('.medico_int_'+id).data('ciudad'));

    $('.medico_buscar_t_'+id_medico_aux+'_'+id_medico_id_aux).html('');
}

function imprimir_pdf_hospitalizacion(id){
    if(id==0){
        $.toast({
            heading: '¡Atención!',
            text: 'Por favor primero guarde la consulta ',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }else{
        guardar_consulta(0);
        setTimeout(function(){ 
            window.open(base_url+'Pacientes/pdfhospitalizacion/'+id,'_blank');  
        }, 1000);
    }  
}

function imprimir_pdf_certificado_medico(id){
    if(id==0){
        $.toast({
            heading: '¡Atención!',
            text: 'Por favor primero guarde la consulta ',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }else{
        guardar_consulta(0);
        setTimeout(function(){ 
            window.open(base_url+'Pacientes/pdfcerticadomedico/'+id,'_blank');  
        }, 1000);
    }  
}

function agregar_justificante_medic(){
    agregar_justificante_medico(0,'');
}
var aux_justificante_m=0;
function agregar_justificante_medico(id,detalle){
    var administrador=$('#administrador').val();
    var nombre_paciente=$('#nombre_paciente_text').val();
    var edad_text=$('#edad_text').val();
    var sexo_text=$('#sexo_text').val();
    var sexo_t='';
    if(sexo_text==1){
        sexo_t='Masculino'; 
    }else{
        sexo_t='Femenino';
    }
    var aquien_txt='';
    if(id==0){
        aquien_txt='Por medio de la presente se hace constar que el paciente '+nombre_paciente+' de sexo '+sexo_t+' con edad '+edad_text+' años, presenta el siguiente diagnóstico:';
    }else{
        aquien_txt='';
    }
    var html='<div class="justificante_medico row_justificante_'+aux_justificante_m+'">\
                <div class="row">\
                    <div class="col-md-12">\
                         <div class="card_borde bg-secon">\
                            <div class="row">\
                                <div class="col-md-8">\
                                    <a class="m-b-0 btn-h btn btn-block textleft" onclick="ocultar_justificante_text('+aux_justificante_m+')">&nbsp;&nbsp;&nbsp;<span class="icon_justificante_'+aux_justificante_m+'"> <i class="fas fa-caret-down"></i> </span> Justificante Médico</a>\
                                </div>\
                                <div class="col-md-4" align="right">\
                                    <div class="editarhistoriacli_btn">\
                                        <button  type="button" class="btn waves-effect waves-light btn-sm btn-outline-danger" onclick="remover_justificante_medico('+aux_justificante_m+','+id+')"><i class="fas fa-trash-alt"></i></button>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
                <div class="row texto_justi_'+aux_justificante_m+'">\
                    <input type="hidden" id="click_style_justi_'+aux_justificante_m+'" value="0">\
                    <div class="col-md-12">\
                        <div class="margen_todo_gris">\
                            <div class="card-body" style="background-color: #f8f9fa;">\
                                <div class="row">\
                                    <div class="col-md-12">\
                                        <div class="margen_todo" style="background-color: white;">\
                                            <div class="card-body" style="padding: 0.25rem !important;">\
                                                <div class="text_recomendar" style="">\
                                                    <div class="row">\
                                                        <div class="col-md-12">\
                                                            <h6 align="center"><strong><u>Justificante Médico</u></strong></h6>\
                                                            <h6>A quien corresponda:</h6>\
                                                        </div>\
                                                        <div class="col-md-12">\
                                                            <input type="hidden" id="idjustificante_j" value="'+id+'">\
                                                            <div class="form-group input-group mb-3 recordableHolder">\
                                                                <textarea type="text" id="detalles_j" class="form-control colorlabel_white recordable rinited js-auto-size">'+aquien_txt+detalle+'</textarea>\
                                                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-md-12">\
                                                          <h6>Médico Tratante: Dr. DANTE OROPEZA CANTO <br>Especialidad: Neurología  Cédula: 2018958 / 3413958</h6>\
                                                          <!--<h6>Especialidad:Fisioterapia  Ced. Esp.:</h6>-->\
                                                          <!--<h6>Teléfono de contacto: 2224269705</h6>-->\
                                                        </div>\
                                                    </div>\
                                                </div>  \
                                                <br>\
                                                <div class="row" align="right">\
                                                    <div class="col-md-12" align="right">\
                                                        <button type="button" class="btn waves-effect waves-light btn-rounded btn-outline-info" onclick="imprimir_pdf_justificante_medico('+id+')">Imprimir orden</button>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
                <div class="margen_div"></div>\
            </div>'; 
    $('.orden_estudio_texto').append(html);        
    aux_justificante_m++;        
    $('textarea.js-auto-size').textareaAutoSize();
}

function ocultar_justificante_text(id){
    var int_orde=$('#click_style_justi_'+id).val();
    if(int_orde==0){
       $('.texto_justi_'+id).css('display','none');
       $('#click_style_justi_'+id).val(1); 
       $('.icon_justificante_'+id).html('<i class="fas fa-caret-right"></i>');   
    }else{
       $('.texto_justi_'+id).css('display','block'); 
       $('#click_style_justi_'+id).val(0);  
       $('.icon_justificante_'+id).html('<i class="fas fa-caret-down"></i>');     
    }  
}

function remover_justificante_medico(aux,id){
    if(id==0){
        $('.row_justificante_'+aux).remove();
    }else{
        $('#elimina_justificante_modal').modal();
        $('#idjustificante_e').val(id);
        $('#idjustificante_remove').val(aux);
    }
}

function eliminarjustificante(){
    var id = $('#idjustificante_e').val();
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/eliminar_jutificante_medico",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Eliminado Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_justificante_modal').modal('hide');
            $('.row_justificante_'+$('#idjustificante_remove').val()).remove();

        }
    }); 
}

function tabla_justificante_medico(id){
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/get_justicante_medico",
        data: {id:id},
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  agregar_justificante_medico(element.idjustificante,element.detalles);
                });
            }   
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}

function imprimir_pdf_justificante_medico(id){
    if(id==0){
        $.toast({
            heading: '¡Atención!',
            text: 'Por favor primero guarde la consulta ',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }else{
        guardar_consulta(0);
        setTimeout(function(){ 
            window.open(base_url+'Pacientes/pdfjustificantemedico/'+id,'_blank');  
        }, 1000);
    }  
}

function agregar_orden_libr(){
    agregar_orden_libre(0,'');
}
var aux_orden_libre=0;
function agregar_orden_libre(id,detalle){
    var administrador=$('#administrador').val();
    var nombre_paciente=$('#nombre_paciente_text').val();
    var edad_text=$('#edad_text').val();
    var aquien_txt='';

    var html='<div class="orden_libre row_orden_libre_'+aux_orden_libre+'">\
                <div class="row">\
                    <div class="col-md-12">\
                         <div class="card_borde bg-secon">\
                            <div class="row">\
                                <div class="col-md-8">\
                                    <a class="m-b-0 btn-h btn btn-block textleft" onclick="ocultar_orden_libre_text('+aux_orden_libre+')">&nbsp;&nbsp;&nbsp;<span class="icon_orden_libre_'+aux_orden_libre+'"> <i class="fas fa-caret-down"></i> </span> Orden libre</a>\
                                </div>\
                                <div class="col-md-4" align="right">\
                                    <div class="editarhistoriacli_btn">\
                                        <button  type="button" class="btn waves-effect waves-light btn-sm btn-outline-danger" onclick="remover_orden_libre('+aux_orden_libre+','+id+')"><i class="fas fa-trash-alt"></i></button>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
                <div class="row texto_orden_libre_'+aux_orden_libre+'">\
                    <input type="hidden" id="click_style_orden_libre_'+aux_orden_libre+'" value="0">\
                    <div class="col-md-12">\
                        <div class="margen_todo_gris">\
                            <div class="card-body" style="background-color: #f8f9fa;">\
                                <div class="row">\
                                    <div class="col-md-12">\
                                        <div class="margen_todo" style="background-color: white;">\
                                            <div class="card-body" style="padding: 0.25rem !important;">\
                                                <div class="text_recomendar" style="">\
                                                    <div class="row">\
                                                        <div class="col-md-12">\
                                                            <h6 align="center"><strong><u>Orden libre</u></strong></h6>\
                                                            <h6>'+nombre_paciente+'</h6>\
                                                            <h6>Edad '+edad_text+' años</h6>\
                                                        </div>\
                                                        <div class="col-md-12">\
                                                            <input type="hidden" id="idorden_o" value="'+id+'">\
                                                            <div class="form-group input-group mb-3 recordableHolder">\
                                                                <textarea type="text" id="detalles_o" class="form-control colorlabel_white recordable rinited js-auto-size">'+aquien_txt+detalle+'</textarea>\
                                                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                </div>  \
                                                <br>\
                                                <div class="row" align="right">\
                                                    <div class="col-md-12" align="right">\
                                                        <button type="button" class="btn waves-effect waves-light btn-rounded btn-outline-info" onclick="imprimir_pdf_orden_libre('+id+')">Imprimir orden</button>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
                <div class="margen_div"></div>\
            </div>'; 
    $('.orden_estudio_texto').append(html);        
    aux_orden_libre++;        
    $('textarea.js-auto-size').textareaAutoSize();
}

function ocultar_orden_libre_text(id){
    var int_orde=$('#click_style_orden_libre_'+id).val();
    if(int_orde==0){
       $('.texto_orden_libre_'+id).css('display','none');
       $('#click_style_orden_libre_'+id).val(1); 
       $('.icon_orden_libre_'+id).html('<i class="fas fa-caret-right"></i>');   
    }else{
       $('.texto_orden_libre_'+id).css('display','block'); 
       $('#click_style_orden_libre_'+id).val(0);  
       $('.icon_orden_libre_'+id).html('<i class="fas fa-caret-down"></i>');     
    }  
}
function remover_orden_libre(aux,id){
    if(id==0){
        $('.row_orden_libre_'+aux).remove();
    }else{
        $('#elimina_orden_libre_modal').modal();
        $('#idorden_e').val(id);
        $('#idorden_remove').val(aux);
    }
}
function eliminarorden_libre(){
    var id = $('#idorden_e').val();
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/eliminar_orden_libre",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Eliminado Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_orden_libre_modal').modal('hide');
            $('.row_orden_libre_'+$('#idorden_remove').val()).remove();

        }
    }); 
}

function tabla_orden_libre(id){
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/getorden_libre",
        data: {id:id},
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            console.log(array);
            if(array.length>0) {
                array.forEach(function(element) {
                  agregar_orden_libre(element.idorden,element.detalles);
                });
            }   
        },
        error: function(response){
            $.toast({
                heading: '¡Error!',
                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }
    });
}

function imprimir_pdf_orden_libre(id){
    if(id==0){
        $.toast({
            heading: '¡Atención!',
            text: 'Por favor primero guarde la consulta ',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }else{
        guardar_consulta(0);
        setTimeout(function(){ 
            window.open(base_url+'Pacientes/pdforden_libre/'+id,'_blank');  
        }, 1000); 
    }  
}
function imprimir_expediente(id){ 
    window.open(base_url+'Pacientes/imprimirexpediente/'+id,'_blank');  
}
function imprimir_ultima_receta_paciente(id){
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/consultar_ultima_receta_paciente",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            if(data==0){
                $.toast({
                    heading: '¡Atención!',
                    text: 'No existe receta',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }else{
                window.open(base_url+'Pacientes/imprimireceta/'+data,'_blank');  
            }
        }
    });  
      
}
function eliminar_paciente(id){ 
    $('#id_paciente_registro').val(id);
    $('#elimina_paciente_modal').modal();
}
function aliminar_paciente(){
    var idp=$('#id_paciente_registro').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Pacientes/alimanarpaciente",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            setTimeout(function(){ 
                window.open(base_url+'Inicio');  
            }, 1000); 
        }
    });  
}
function registro_sistemas(id){
    var form_register = $('#form_registro_sistemas'+id);
    var datos = form_register.serialize()+'&idpaciente='+$('#idpte').val();
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/registrosistemas'+id,
        data: datos,
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            if(id==1){
                $('#idsintomas').val(data);
            }else if(id==2){
                $('#idaparato').val(data);
            }else if(id==3){
                $('#iddigestivo3').val(data);
            }else if(id==4){
                $('#idcardio4').val(data);
            }else if(id==5){
                $('#idrenal5').val(data);
            }else if(id==6){
                $('#idgenital6').val(data);
            }else if(id==7){
                $('#idendocrino7').val(data);
            }else if(id==8){
                $('#idhematopoyetico8').val(data);
            }else if(id==9){
                $('#idmusculo9').val(data);
            }else if(id==10){
                $('#idnervioso10').val(data);
            }else if(id==11){
                $('#idorganos11').val(data);
            }else if(id==12){
                $('#idesfera12').val(data);
            }else if(id==13){
                $('#idtegumentario13').val(data);
            }
        }
    }); 
}

function consulta_whats(){
    $('#modal_conuslta_whats').modal();
}
function ver_consultas(id){
    $('.btn_consultas').css('display','none');
    var idp=$('#idpte').val();
    ///
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/tipo_consulta',
        data: {id:id,idp:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.txt_consultas_tipo').html(data); 
        } 
    }); 
}
function ver_consultas_tipo(){
    $('.btn_consultas').css('display','block');
    $('.txt_consultas_tipo').html(''); 
    $('.txt_servicios_consultas').html(''); 
}
function servicios_consulta(id,tipo){
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/tipo_servicios',
        data: {id:id,tipo:tipo},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.txt_servicios_consultas').html(data);
            $('.cuidados_especiales').ckeditor();
        } 
    }); 
}
function guardar_servicios(id,tipo){
    var DATA  = [];
    var TABLA   = $(".cuidado_especial div > .row_col");
    TABLA.each(function(){  
            item = {};
            item ["idservicio"] = $(this).find("input[id*='idservicio_c']").val();
            item ["idconsulta"] = $(this).find("input[id*='idconsulta_c']").val();
            item ["tipo"] = $(this).find("input[id*='tipo_c']").val();
            item ["iddetalles"] = $(this).find("input[id*='iddetalles_c']").val();
            item ["cuidados_especiales"] = $(this).find("textarea[id*='cuidados_especiales']").val();
            DATA.push(item);
    });
    console.log(DATA);
    INFO  = new FormData();
    aInfo   = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO, 
        type: 'POST',
        url : base_url + 'Pacientes/registro_cuidados_especiales',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function(data){
            enviar_consulta_whats_c1(id,tipo);
        }
    });
}
function enviar_consulta_whats_c1(id_c,tipo){
    var cel=$('.info_pas').data('celular_w');
    var lada=$('#lada option:selected').val();
    if(tipo==1){
        window.open('https://wa.me/'+lada+''+cel+'?text=Buen%20d%C3%ADa,%20su%20médico%20le%20ha%20enviado%20su%20consulta,%20para%20consultarla%20ingrese%20al%20siguiente%20enlace:%20'+base_url+'Consultas/medicina/'+id_c, '_blank');
    }else if(tipo==2){
        window.open('https://wa.me/'+lada+''+cel+'?text=Buen%20d%C3%ADa,%20su%20médico%20le%20ha%20enviado%20su%20consulta,%20para%20consultarla%20ingrese%20al%20siguiente%20enlace:%20'+base_url+'Consultas/spa/'+id_c, '_blank');
    }else if(tipo==3){
    }   window.open('https://wa.me/'+lada+''+cel+'?text=Buen%20d%C3%ADa,%20su%20médico%20le%20ha%20enviado%20su%20consulta,%20para%20consultarla%20ingrese%20al%20siguiente%20enlace:%20'+base_url+'Consultas/nutricion/'+id_c, '_blank');
}


function enviar_consulta_m_whats(id_c){
    var cel=$('.info_pas').data('celular_w');
    var lada=$('#lada option:selected').val();
    window.open('https://wa.me/'+lada+''+cel+'?text=Buen%20d%C3%ADa,%20su%20médico%20le%20ha%20enviado%20su%20consulta,%20para%20consultarla%20ingrese%20al%20siguiente%20enlace:%20'+base_url+'Consultas/consulta/'+id_c, '_blank');
}
function agregar_grafica_reporte(id){
    //var canvas = document.getElementById("bar-chartm");
    /*
    setTimeout(function(){ 
        var canvasx= document.getElementById("prueba_img");
        var dataURL = canvasx.toDataURL();
        console.log(dataURL);
    }, 1000); 
    */
    //console.log(canvas);
    //console.log(id);
    //var dataURL = canvas.toDataURL();
    //console.log(dataURL);
    
    canvas = document.getElementsByTagName("canvas");
    var dataURL =canvas[0].toDataURL();

    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/add_img_grafica',
        data: {id:id,grafica:dataURL},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: 'Error!',
                    text: '404',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: 'Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Guardado Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
        }
    });
    
}

function select_doc_legal(id){
    var btn_doc_legal=$('#tipo_doc_legal option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+'Pacientes/get_tipo_documento',
        data: {tipo:btn_doc_legal,id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.text_doc_legal').html(data); 
            $('#doc_legal').change(function(){
                setTimeout(function(){ 
                    var idpte=$('#idpte').val();
                    var idtipo=$('#tipo_doc_legal_imp').val();
                    add_file_doc_legal(idpte,idtipo);
                    //add_file_doc($('#idpte').val());
                }, 1500);
            });
        } 
    }); 
    if(btn_doc_legal==1){
        $('#text_qr').val(base_url+'Pacientes/firma_inyeccion/'+$('#idpte').val());
    }else if(btn_doc_legal==2){
        $('#text_qr').val(base_url+'Pacientes/firma_enzimas/'+$('#idpte').val());
    }else if(btn_doc_legal==3){
        $('#text_qr').val(base_url+'Pacientes/firma_toxina/'+$('#idpte').val());
    }else if(btn_doc_legal==4){
        $('#text_qr').val(base_url+'Pacientes/firma_bichectomia/'+$('#idpte').val());
    }else if(btn_doc_legal==5){
        $('#text_qr').val(base_url+'Pacientes/firma_luz_pulsada/'+$('#idpte').val());
    }else if(btn_doc_legal==6){
        $('#text_qr').val(base_url+'Pacientes/firma_radio_frecuencia/'+$('#idpte').val());
    }else if(btn_doc_legal==7){
        $('#text_qr').val(base_url+'Pacientes/firma_hidrolipoclasia/'+$('#idpte').val());
    }
}