var base_url=$('#base_url').val();
var idperfil=$('#perfilid').val();
var tabla;
var tipop;
var idpaciente_aux=0;
$(document).ready(function() {
	$('#foto_avatar').change(function(){
	     document.getElementById('img_avatar').src = window.URL.createObjectURL(this.files[0]);
	});
    tablePaciente();
});
function reload_paciente(){
    tabla.destroy();
    tablePaciente();
}
function modalPaciente(){
	$('#nuevoPaciente').modal();
    $('.contenido_paciente').css('display','none');
    $('.btn_paciente_registro').css('display','none');
}
function paciente_tipo(id){
    if(id==1){
        $('.titulo_paciente_registro').html('Primera vez');
    }else{
        $('.titulo_paciente_registro').html('Subsecuente'); 
    }
    tipop=id;
    $('#tipo').val(id);
    $('.contenido_paciente').css('display','block');
    $('.btn_paciente_registro').css('display','block');
}
function guarda_paciente(){
    var form_register = $('#form_paciente');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            },
            apll_paterno:{
              required: true
            },
            apll_materno:{
              required: true
            },
            sexo:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    if($('#verificar_check').is(':checked')){
	    var $valid = $("#form_paciente").valid();
	    if($valid) {
	        var datos = form_register.serialize();
	        $.ajax({
	            type:'POST',
	            url: base_url+'Inicio/registraPaciente',
	            data: datos,
	            statusCode:{
	                404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
	                },
	                500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
	                }
	            },
	            success:function(data){
	                var id=data;
	                add_file(id);
                    $.toast({
                        heading: 'Éxito',
                        text: 'Guardado Correctamente',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 3500, 
                        stack: 6
                      });
	                setTimeout(function(){ 
	                    //location.href= base_url+'Inicio';
	                    window.location = base_url+'Pacientes/paciente/'+id;
	                }, 1500);
	            }
	        });
	    }
	}else{
        $.toast({
            heading: '¡Atención!',
            text: 'El paciente debe aceptar los términos y condiciones del servicio',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
            
        });
	}    

}
function add_file(id){
  //console.log("archivo: "+archivo);
  //console.log("name: "+name);
    var archivo=$('#foto_avatar').val()
	//var name=$('#foto_avatar').val()
	extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
	extensiones_permitidas = new Array(".jpeg",".png",".jpg");
	permitida = false;
    if($('#foto_avatar')[0].files.length > 0) {
	    for (var i = 0; i < extensiones_permitidas.length; i++) {
	       if (extensiones_permitidas[i] == extension) {
	       permitida = true;
	       break;
	       }
	    }  
	    if(permitida==true){
	      //console.log("tamaño permitido");
	        var inputFileImage = document.getElementById('foto_avatar');
	        var file = inputFileImage.files[0];
	        var data = new FormData();
	        data.append('foto',file);
	        data.append('id',id);
	        $.ajax({
	            url:base_url+'Inicio/cargafiles',
	            type:'POST',
	            contentType:false,
	            data:data,
	            processData:false,
	            cache:false,
	            success: function(data) {
	                var array = $.parseJSON(data);
	                    //console.log(array.ok);
	                    /*
	                    if (array.ok=true) {
	                        swal("Éxito", "Guardado Correctamente", "success");
	                    }else{
	                        swal("Error!", "No Se encuentra el archivo", "error");
	                    }
	                    */
	            },
	            error: function(jqXHR, textStatus, errorThrown) {
	                var data = JSON.parse(jqXHR.responseText);
	            }
	        });
	    }
    }        
}
function tablePaciente(){
	tabla=$("#table_pacientes").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Inicio/getlistadoPaciente",
            type: "post",
            "data":{paciente:$('#paciente_busqueda').val()},
            error: function(){
               $("#table_pacientes").css("display","none");
            }
        },
        "columns": [
            {"data": null,
                "render": function ( data, type, row, meta ){
                	var html='';        
                	if(row.foto!=''){
                        html='<img style="width: 40px; height: 40px; border-radius: 20px;" src="'+base_url+'uploads/pacientes/'+row.foto+'">';  
                	}else{
                        html='<img style="width: 40px; height: 40px; border-radius: 20px;" src="'+base_url+'images/annon.png">';
                	}
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html=row.nombre+' '+row.apll_paterno+' '+row.apll_materno;        
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.ultima_consulta!=null){
                        html=ChangeDate(row.ultima_consulta);
                    }
                    return html;     
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button" class="btn waves-effect waves-light btn-secondary" onclick="href_agenda('+row.idpaciente+')">Agendar cita</button>\
                          <a href="'+base_url+'Pacientes/paciente/'+row.idpaciente+'" class="btn waves-effect waves-light btn-secondary">Expediente</a> ';
                        //if(idperfil!=1){
                        //    html+='<a href="'+base_url+'Pacientes/paciente/'+row.idpaciente+'/?consulta=1" type="button" class="btn waves-effect waves-light btn-secondary" >Entrar a pre consulta</a> ';        
                        //}else{
                            html+=' <a href="'+base_url+'Pacientes/paciente/'+row.idpaciente+'/?consulta=1" type="button" class="btn waves-effect waves-light btn-secondary" >Consultas</a>';        
                        //}
                        html+=' <a type="button" class="btn waves-effect waves-light btn-secondary pasienterow_'+row.idpaciente+'"\
                                    data-correo="'+row.correo+'"\
                                    data-telefono="'+row.celular+'"\
                                    onclick="modal_encuesta('+row.idpaciente+')"\
                                    > Encuesta </a>';
                    html+=' <a type="button" class="btn waves-effect waves-light btn_sistema" onclick="modal_productos('+row.idpaciente+')"> <i class="fas fa-capsules"></i> </a> \
                            <a type="button" class="btn waves-effect waves-light btn_sistema" onclick="herf_producto('+row.idpaciente+')"> <i class="fas fa-shopping-cart"></i> </a>';        
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function herf_producto(id){
    window.location = base_url+'Inicio/productos/'+id;
}

function href_agenda(id){
    window.location.href = base_url+"Agenda/?id="+id;
}
function ChangeDate(data){
  var parte=data.split('-');
  return parte[2]+"/"+parte[1]+"/"+parte[0];
}
function validar_paciente(){
    var nombre=$('#nombre').val();
    var app_p=$('#apll_paterno').val();
    var app_m=$('#apll_materno').val();
    $.ajax({
        type:'POST',
        url: base_url+'Inicio/get_validar_paciente',
        data:{nom:nombre,appp:app_p,appm:app_m},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.detalle_paciente').css('display','none');
            $('.btn_datos1').css('display','none');
            $('.detalle_paciente_datos').html(data);
        }
    });
}

function modal_encuesta(id){
    $('#idclienteencuesta').val(id);
    var correo = $('.pasienterow_'+id).data('correo');
    var telefono = $('.pasienterow_'+id).data('telefono');
    $('#encuentatel').val(telefono);
    $('#encuentaemail').val(correo);
    $('#modalencuesta').modal();
    $.ajax({
            type:'POST',
            url: base_url+'Encuestas/ultimaescuesta',
            data:{
                paciente:id
            },
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $('.viewencuenta').html(data);
            }
        });
}
function envioencuesta(tipo){
    var tel     = $('#encuentatel').val();
    var email   = $('#encuentaemail').val();
    var cliente = $('#idclienteencuesta').val();
    if (tipo==1) {
        if (tel=='') {
            $.toast({
                heading: '¡Atención!',
                text: 'Ingrese un numero telefonico',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }else{
            window.open(base_url+'Encuestas/whatsapp/'+tel+'/'+cliente, '_blank');
        }
        
    }else{
        if (email=='') {
            $.toast({
                heading: '¡Atención!',
                text: 'Ingrese un numero telefonico',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'error',
                hideAfter: 3500
            });
        }else{
            $.ajax({
            type:'POST',
            url: base_url+'Encuestas/mail',
            data:{
                paciente:cliente,
                mail:email
            },
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                if (data=='enviado') {
                    
                    $.toast({
                    heading: 'Éxito',
                    text: 'Enviado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                  });  
                }else{
                  $.toast({
                        heading: '¡Error!',
                        text: 'No fue posible el envio',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
                
               
            }
        });
        }
    }
}
function modal_productos(id){
    idpaciente_aux=id;
    $('#modal_almacen_datos').modal();
    $('#idproducto').select2({
        width: '100%', 
        dropdownParent: $("#modal_almacen_datos"),
        //dropdownParent: $('#modal_almacen_datos'),
        //width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un producto',
        allowClear:true,
        ajax: {
            url: base_url + 'Inicio/searchproducto',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.idproducto,
                        text: element.producto,
                        clinica: element.clinica,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e){
        var data = e.params.data;
        $('#precio_clinica').val(data.clinica);
        lotes_productos(data.id);
        /*
        var data = e.params.data;
        var idv_c1=0;
        if(idventa_c1_aux==0){
            idv_c1=0;
        }else{
            idv_c1=idventa_c1_aux;
        }
        add_servicio_estetica(idv_c1,0,data.id,data.text,data.desc,data.costo);
        */
    });
}
function lotes_productos(id){
    $.ajax({
        type:'POST',
        url: base_url+"Inicio/get_detalles_productos",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.lotes_productos').html(data);

        }
    });  
}
function agregar_productos_venta(){
    
    var producto=$('#idproducto option:selected').text();
    var idalmacen=idalmacen_aux;
    var lote=lote_aux;
    var caducidad=cadu_aux;
    var preciou=$('#precio_clinica').val();

    var canti=$('#cantidad_lote').val();
    var multi=preciou*canti;
    var total=multi; 
    if(parseFloat(cantidad_aux)>=parseFloat(canti)){
        var cantidad=canti;
        add_venta_productos(0,producto,idalmacen,lote,caducidad,cantidad,preciou,total);
        $('#cantidad_lote').val('');
    }else{
        $.toast({
            heading: 'Atención!',
            text: 'Ingrese una cantidad no mayor al stock',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }    
} 
var aux_producto=0;
function add_venta_productos(iddetalles,producto,idalmacen,lote,caducidad,cantidad,preciou,total){
    var html='';
    var preciou_aux=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(preciou);
    var total_aux=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total);
    var btn_html=0;
        if(iddetalles==0){
            btn_html='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs  btn-danger" onclick="remove_producto_c1('+aux_producto+','+iddetalles+')"><i class="fas fa-trash-alt"></i> </button>';
        }else{   
            btn_html='';
        }    
        html+='<tr class="row_pro_c1_'+aux_producto+'">\
                    <th>\
                        <input type="hidden" id="iddetalles_c1" value="'+iddetalles+'">\
                        <input type="hidden" id="idalmacen_c1" value="'+idalmacen+'">\
                        <input type="hidden" id="cantidad_c1" value="'+cantidad+'">\
                        <input type="hidden" id="preciou_c1" value="'+preciou+'">\
                        <input type="hidden" id="total_c1" value="'+total+'">\
                        '+producto+'</th>\
                    <th>'+lote+'</th>\
                    <th>'+caducidad+'</th>\
                    <th>'+cantidad+'</th>\
                    <th>'+preciou_aux+'</th>\
                    <th>'+total_aux+'</th>\
                    <th>'+btn_html+'</th>\
                </tr>';
    $('#tabla_producto_venta_consultas tbody').append(html);
    suma_costo_total_productos();
    aux_producto++;
}
function remove_producto_c1(id,idd){
    if(idd==0){
        $('.row_pro_c1_'+id).remove();
        suma_costo_total_productos();
    }else{
        //$('#elimina_modal_venta_servicio_c1').modal();
        //$('#idvet_c1').val(idd);
        //$('#idvet_c1_remove').val(id);
    }

}
function suma_costo_total_productos(){
    var addtp = 0;
    var TABLAP = $("#tabla_producto_venta_consultas tbody > tr");            
    TABLAP.each(function(){         
        var totalmonto = $(this).find("input[id*='total_c1']").val();
        var vstotal = totalmonto;
        addtp += Number(vstotal);
    });
    var conDecimal = addtp.toFixed(2);
    //var monto_mtl=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(conDecimal);
    $('.total_costop').html(conDecimal);
}

function select_producto_detalle(){
    var id = $('#idalmacen option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"Inicio/get_producto_almacen",
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            var y = $.parseJSON(data);
            idalmacen_aux=y.idalmacen;
            lote_aux=y.lote;
            cadu_aux=y.cadu;
            cantidad_aux=y.cantidad;
        }
    });  
}
function add_productos_venta(){
    var idcli=idpaciente_aux;
    //=====Ventas=====
    var num_existe=0;
    var TABLAP   = $("#tabla_producto_venta_consultas tbody > tr");
    TABLAP.each(function(){  
        num_existe=1;
    });
    if(num_existe==1){
        $('.btn_almacen').attr('disabled',true);
        $.ajax({
        type:'POST',
        url: base_url+"Inicio/venta_productos",
        data:{idpaciente:idcli,total:$('.total_costop').text()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            var idventa=data;
            //
            var DATAP  = [];
            var TABLAP   = $("#tabla_producto_venta_consultas tbody > tr");
            TABLAP.each(function(){  
                item = {};
                item ["idventa"] = idventa;
                item ["idpaciente"] = idcli;
                item ["iddetalles"] = $(this).find("input[id*='iddetalles_c1']").val();
                item ["idalmacen"] = $(this).find("input[id*='idalmacen_c1']").val();
                item ["cantidad"] = $(this).find("input[id*='cantidad_c1']").val();
                item ["preciou"] = $(this).find("input[id*='preciou_c1']").val();
                item ["totalp"] = $(this).find("input[id*='total_c1']").val();
                DATAP.push(item);
            });
            INFOP  = new FormData();
            aInfop   = JSON.stringify(DATAP);
            INFOP.append('data', aInfop);
            $.ajax({
                data: INFOP, 
                type: 'POST',
                url : base_url + 'Inicio/registro_venta_producto',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    },
                    500: function(){
                        $.toast({
                            heading: '¡Error!',
                            text: '500',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                },
                success: function(data){
                    $.toast({
                        heading: 'Éxito',
                        text: 'Guardado Correctamente',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'success',
                        hideAfter: 3500, 
                        stack: 6
                    });
                    
                    setTimeout(function(){ 
                        $('#tabla_producto_venta_consultas tbody').html('');
                        $('.total_costop').html(0);
                        $('.btn_almacen').attr('disabled',false);
                    }, 1000);
                }
            });
            //
            setTimeout(function(){ 
                imprimir_productos_paciente(idventa);
            }, 1000);
        }
    });  
        
    }else{
        $.toast({
            heading: '¡Ateción!',
            text: ' No has agregado productos',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }   
}
function imprimir_productos_paciente(id){
    $('#modal_ticket_producto').modal(); 
    $('.iframereporte').html('<iframe src="' + base_url + 'Reportes/ticket_paciente_producto/'+id+'" class="iframeprintc1" id="iframeprintc1"></iframe>');
}