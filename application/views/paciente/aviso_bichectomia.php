<html><head>
	<title>Karla Coello</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0">
	<meta name="author" content="Carlos Corona">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>images/FAV.png">
    <link href="<?php echo base_url();?>aviso/bootstrap.min.css?v=34-2-5-16" rel="stylesheet" type="text/css" lazyload="">
    <link href="<?php echo base_url();?>assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
 </head>
 <style type="text/css">
 	@media print {
		html {
			margin: 40px;
		}
	}
 </style>
	<body>
		<div id="" style="text-align:center">
			<h4>CONSENTIMIENTO INFORMADO - BICHECTOMIA</h4>
		</div>
		<div id="date" style="text-align:right;position:fixed;right:1cm;top:0.5cm;">
		    <?php echo date('d/m/Y');?>	
	    </div>
		<div style="text-align: justify; list-style-type: upper-latin; font-size: 13px">
			El paciente y/o su representante legal hace constar y manifiesta que: Como un hecho sobresaliente debe señalarse que la explicación del médico fue lo suficientemente clara para evidenciar los beneficios que el acto médico propuesto le ofrece al paciente respecto a otras opciones de manejo, sobresaliendo particularmente las siguientes ventajas positivas del procedimiento de atención antes mencionado. 
			<br><br>
			<b>RIESGOS DE LA CIRUGIA DE BICHECTOMIA </b><br>
            Cualquier procedimiento quirúrgico entraña un cierto grado de riesgo y es importante que Ud. Comprenda los riesgos asociados. Aunque la mayoría de las mujeres y/o hombres no experimentan las siguientes complicaciones, usted debe de conocer y considerar cada una de ellas para asegurarse de que comprende los riesgos, complicaciones potenciales y consecuencias de la cirugía de BICHECTOMIA. 
			<br><br>
			-Hemorragia:. No debe tomar aspirina o antiinflamatorios desde 10 días antes de la cirugía puesto que puede aumentar el riesgo de sangrado.,
			<br>
			Infección: Después de la cirugía es muy rara. Es necesario seguir instrucciones posteriores como toma de antibiótico recetado. 
			<br>
			-Lesión de la glándula parótida y su conducto excretor: Es posible, aunque muy raro la lesión del conducto de Stenon en la zona de su desembocadura a la cavidad oral. Pueden requerirse cirugía o procedimientos adicionales para resolver la lesión intraquirurgica del conducto de Stenon. 
			<br>
			Cambios en la sensibilidad la piel: Pueden ocurrir cambios temporales en la sensibilidad cutánea después, que habitualmente se resuelven. La disminución o pérdida completa de la sensibilidad cutánea ocurre infrecuentemente y pueden no resolverse totalmente. 
			<br>
			-Cicatrización: Las incisiones se encuentran en la mucosa bucal, dentro de la boca, y quedan totalmente escondidas. 
			<br>
			-Irregularidad del contorno de la piel: Pueden ocurrir irregularidades de contorno y depresiones de la piel después de una Extirpación de la Bolsas Adiposas de Bichat. 
			<br>
			-Asimetría: Pueden no conseguirse un aspecto totalmente simétrico de la cara tras la Extirpación de las Bolsas Adiposas de Bichat. Factores como el tono de la piel, prominencias Oseas, y tono muscular, pueden contribuir a una muy ligera asimetría normal en los rasgos faciales. 
			<br>
			-Efectos a largo plazo: Pueden ocurrir alteraciones posteriores en el contorno facial como resultado del envejecimiento: la frecuente atrofia grasa del envejecimiento puede exagerar el resultado de la Bichectomia. La pérdida o ganancia de peso, embarazo, toma de medicamentos de manera continua u otras circunstancias no relacionadas con la Bichectomia pueden alterar la forma de la cara. -Reacciones alérgicas: En casos raros se han descrito alergias locales al esparadrapo, material de sutura o preparados tópicos. Las reacciones sistémicas, que son más serias, pueden ocurrir por medicaciones utilizadas durante la cirugía o establecidas posteriormente. Las reacciones alérgicas pueden requerir tratamiento adicional. 
			<br>
			-Anestesia: Tanto la anestesia local como general implican un riesgo.  Así mismo, la existencia de riesgos inmediatos, riesgos secundarios y riesgos personalizados (estos últimos en relación a las condiciones propias del paciente y observadas en su valoración médica y de diagnóstico). 
			<br>
            <div style="text-align: center">
			<input type="hidden" id="base_url" value="<?php echo base_url(); ?>" readonly>
            <input type="hidden" id="idpaciente" value="<?php echo $paciente->idpaciente ?>">
            <input type="hidden" id="tipo_aviso" value="<?php echo $tipo_aviso ?>">
            <?php if($iddocumento==0){ ?>
	            <?php if($tipo==0){ ?>
					<div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div>
	            <?php } else{?>
	            	<div class="firma_text" id="aceptance">
						<div id="signature" class="signature col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
							<label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma en el recuadro, su mouse o su dedo</label><br>
							<canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
							<img src="<?php echo base_url() ?>public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignature" data-signature="patientSignature">
							<br>
							<div class="row" >
								<button class="btn_estilo" id="btn_firma_save" onclick="saveex_paciente()">Aceptar Firma</button>
							</div>				
						</div>			
					</div>
	            <?php }?>
            <?php }else{?>
				    <div style="text-align:center;margin-bottom:10px;border-radius:4px;">
				        <?php 
				        $result_get=$this->General_model->get_record('iddocumento',$iddocumento,'documentos_legales');
				        $fh = fopen(base_url()."uploads/paciente_firma/".$result_get->firma, 'r') or die("Se produjo un error al abrir el archivo");
				        $linea = fgets($fh);
				        fclose($fh);  
				        ?>
				        <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
				            <img src="<?php echo $linea ?>" width="170" height="75" style="border:dotted 1px black;">
				        </div> 
				    </div>                 
             <?php }?>
				<div style="font-size: 15px">FIRMA DEL PACIENTE Y FECHA 
				</div>	
			    <div style="font-size: 16px">
			    <?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?>		
		        </div> 
            </div>

			<br>
            Costo $
            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:5cm;border-bottom:1px solid black;"></div>
            <br>
            <div align="center">
	            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div><br>
	            DRA KARLA COELLO VÁZQUEZ  8435100 	
	        </div>    
        </div>	
    </body>
</html>