<html><head>
  <title>Karla Coello</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0">
  <meta name="author" content="Carlos Corona">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>images/FAV.png">
    <link href="<?php echo base_url();?>aviso/bootstrap.min.css?v=34-2-5-16" rel="stylesheet" type="text/css" lazyload="">
    <link href="<?php echo base_url();?>assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
 </head>
 <style type="text/css">
 	@media print {
		html {
			margin: 40px;
		}
	}
 </style>
	<body>
		<div id="" style="text-align:center">
			<h4>CONSENTIMIENTO INFORMADO PARA APLICACIÓN DE RADIOFRECUENCIA BIPOLAR O MONOPOLAR FACIAL O CORPORAL </h4>
		</div>
		<div id="date" style="text-align:right;position:fixed;right:1cm;top:0.5cm;">
		    <?php echo date('d/m/Y');?>	
	    </div>
		<div style="text-align: justify; list-style-type: upper-latin; font-size: 17px">
            Las áreas a tratar son: <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:12cm;border-bottom:1px solid black;"></div>
			<br>
			He sido informado sobre la naturaleza y propósito del tratamiento y se me ha respondido a
			todas mis preguntas relativas al mismo. Entiendo y asumo que éste tratamiento conlleva
			algunos riesgos y complicaciones. Se me han explicado los riesgos y complicaciones a mi
			entera satisfacción. __(iniciales) Manifiesto que no estoy embarazada, no soy
			<b>portador/a de marcapasos/desfibriladores implantables u otros dispositivos eléctricos</b>
			que puedan verse afectados en forma adversa por la corriente o los campos de radiofrecuencia y no
            llevo materiales de relleno cutáneo
            <b>sobre el área a tratar</b>
            y que a criterio médico contraindique en general el uso de radiofrecuencia o laser; que no padezco ningún tipo de
			alergia conocida, que no he tomado el sol o Uva, ni he usado medicamentos
			fotosesibilizantes recientemente y que no padezco ninguna enfermedad que según el
			criterio del Dr./a pudiera contraindicar éste tipo de intervención. 
			<br>
			Confirmo que he leído éste formulario y que comprendo y acepto la información del
			documento. Se me ha explicado de forma clara que durante la sesión de Radiofrecuencia, la
			dermis se calienta dando como resultado la contracción del colágeno dérmico, que voy a
			experimentar una sensación de calor y rojeces en el área tratada. También se me ha
			informado debidamente de otros procedimientos alternativos. Accedo y autorizo a seguir un
			control fotográfico pre y post tratamientos u otros materiales audiovisuales y gráficos y con
			la sola finalidad del control evolutivo de mi tratamiento y valoración científica.
			Considerando que he sido suficientemente informado/a y aclaradas mis posibles dudas
			sobre el procedimiento y posibles resultados.
            <br>
            <b>ES IMPORTANTE QUE LEA CUIDADOSAMENTE LA INFORMACIÓN Y HAYAN SIDO RESPONDIDAS TODAS
			SUS PREGUNTAS ANTES DE QUE FIRME EL CONSENTIMIENTO DE LA PAGINA SIGUIETE</b>
		    <br>	   
            <!-- --->
            <div style="">
            <input type="hidden" id="base_url" value="<?php echo base_url(); ?>" readonly>
            <input type="hidden" id="idpaciente" value="<?php echo $paciente->idpaciente ?>">
            <input type="hidden" id="tipo_aviso" value="<?php echo $tipo_aviso ?>">
             <?php if($iddocumento==0){ ?>
                  <?php if($tipo==0){ ?>
                        <table style="width: 100%">
                              <tbody>
                                    <tr style="text-align: center">
                                          <td style="width: 50%">
                                                <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div>
                                          </td>
                                          <td style="width: 50%">
                                                <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div>
                                          </td>
                                    </tr>
                                    <tr style="text-align: center">
                                          <td style="width: 50%">
                                                Nombre y firma del paciente:
                                                <div style="font-size: 16px">
                                                    <?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?> 
                                                </div> 
                                          </td>
                                          <td style="width: 50%;">
                                                Nombre y firma del médico:
                                          </td>
                                    </tr>
                              </tbody>
                        </table>
                  <?php } else{?>
                        <table style="width: 100%">
                              <tbody>
                                    <tr style="text-align: center">
                                          <td style="width: 50%">
                                                <div class="firma_text" id="aceptance">
                                                      <div id="signature" class="signature col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                                                            <label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma en el recuadro, su mouse o su dedo</label><br>
                                                            <canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
                                                            <img src="<?php echo base_url() ?>public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignature" data-signature="patientSignature">
                                                            <br>                       
                                                      </div>                  
                                                </div>
                                          </td>
                                          <td style="width: 50%">
                                                <div class="firma_text" id="aceptance">
                                                      <div id="signaturepfc1" class="signaturepfc1 col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                                                            <label for="patientSignaturem" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma en el recuadro, su mouse o su dedo</label><br>
                                                            <canvas id="patientSignaturem" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
                                                            <img src="<?php echo base_url() ?>public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignaturepfc1" data-signature="patientSignaturem">
                                                            <br>                       
                                                      </div>                  
                                                </div>
                                          </td>
                                    </tr>
                                    <tr style="text-align: center">
                                          <td style="width: 50%">
                                                Nombre y firma del paciente:
                                                <div style="font-size: 16px">
                                                    <?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?> 
                                                </div> 
                                          </td>
                                          <td style="width: 50%;">
                                                Nombre y firma del médico:
                                          </td>
                                    </tr>
                              </tbody>
                        </table>
                        <div class="row" style="text-align: center">
                              <button class="btn_estilo" id="btn_firma_save" onclick="saveex_paciente_medico()">Aceptar Firma</button>
                        </div>   
                  <?php }?>
             <?php }else {
                  $result_get=$this->General_model->get_record('iddocumento',$iddocumento,'documentos_legales');
                  ?>
                  <table style="width: 100%">
                        <tbody>
                              <tr style="text-align: center">
                                    <td style="width: 50%">
                                          <div style="text-align:center;margin-bottom:10px;border-radius:4px;">
                                            <?php
                                            $fh = fopen(base_url()."uploads/paciente_firma/".$result_get->firma, 'r') or die("Se produjo un error al abrir el archivo");
                                            $linea = fgets($fh);
                                            fclose($fh);  
                                            ?>
                                            <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                                                <img src="<?php echo $linea ?>" width="170" height="75" style="border:dotted 1px black;">
                                            </div> 
                                          </div>          
                                    </td>
                                    <td style="width: 50%">
                                          <?php
                                            $fh2 = fopen(base_url()."uploads/paciente_firma_doc/".$result_get->firma_medico, 'r') or die("Se produjo un error al abrir el archivo");
                                            $linea2 = fgets($fh2);
                                            fclose($fh2);  
                                            ?>
                                            <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                                                <img src="<?php echo $linea2 ?>" width="170" height="75" style="border:dotted 1px black;">
                                            </div> 
                                    </td>
                              </tr>
                              <tr style="text-align: center">
                                    <td style="width: 50%">
                                          Nombre y firma del paciente:
                                          <div style="font-size: 16px">
                                              <?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?> 
                                          </div> 
                                    </td>
                                    <td style="width: 50%;">
                                          Nombre y firma del médico:
                                    </td>
                              </tr>
                        </tbody>
                  </table>
            <?php }?>
            </div>
            <!-- --->
            Fecha: <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:4cm;border-bottom:1px solid black;"></div>
        </div>	
    </body>
</html>