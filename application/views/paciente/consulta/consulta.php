<input type="hidden" id="base_url" value="<?php echo base_url() ?>">
<style type="text/css">
	body{
		background-color: #E6E9F0 !important;
	}
	.square-green {
	    border-left: 8px solid #779155;
	}
	.r-square {
	    background-color: #fff;
	    border-radius: 4px;
	    padding: 15px 15px 15px 20px;
	    box-shadow: 1px 2px 0px 0px rgba(45, 84, 185, 0.07);
	}

	.margen_todo_gris {
	    border-style: solid;
	    border-width: 1px;
	    border-color: #e5e6e5;
	    background-color: f8f9fa;
	}
	.r-square2 {
	    background-color: #779155;
	    border-radius: 4px;
	    padding: 15px 15px 15px 20px;
	    box-shadow: 1px 2px 0px 0px rgba(45, 84, 185, 0.07);
	}
    .btn-info{
	    background: #779155 !important;
	}
    .div_abajo_solid{border-bottom: solid;}	
    pre {
        overflow: auto;
        border: none;
        background: transparent;
        font-family: "Helvetica Neue", Helvetica, "Segoe UI", Tahoma, Arial, sans-serif;
        font-size: 14px;
        padding: 0px 5px;
        word-break: break-word;
        margin-bottom: 20px;
    }
    input{pointer-events: none;}
    /* Tabla de nutricion */
    .tabla_n{ 
        border: solid 1px #000000;
    }
    .tr_n{ 
        border: solid 1px #000000;
        color: black;
    }
    .th_n{ 
        border: solid 1px #000000;
        background-color: #b6cdea;
        color: black;
    }
    .td_n{ 
        border: solid 1px #000000;
        color: black;
    }
    .color_input_tabla{
        background-color: #ffeb3b;
        min-height: 35px !important;
        color: red;
    }
    /* Tamañno de tabla 2 */
    .tm_td{
        width: 45px;
    }
    .td_n1{
        border: solid 1px #000000;
        background-color: #dbdbff;
        color: black;
    }
    .th_n2{ 
        border: solid 1px #000000;
        background-color: #f5d5ef;
        color: black;
    }
    .th_n3{ 
        border: solid 1px #000000;
        background-color: #ffdaa3;
        color: black;
    }
    .th_n4{ 
        border: solid 1px #000000;
        background-color: #d3efc5;
        color: black;
    }
    .th_n5{
        border: solid 1px #000000;
        background-color: #aab8e0;
        color: black;
    }
    .th_n6{
        border: solid 1px #000000;
        background-color: #d3eadb;
        color: black;
    }
    .th_n5_6{
        border: solid 1px #000000;
        background-color: #fff2c9;
        color: black;
    }
    .color_input_tabla2 {
        background-color: #ffffff;
        min-height: 35px !important;
        color: black;
    }
    .oculto_text3{
        color: #ffdaa3;
    }
    .oculto_text4{
        color: #d3efc5;
    }
    .oculto_text5{
        color: #aab8e0;
    }
    .oculto_text6{
        color: #fff2c9;
    }
    .inputplan{
        border:0px;
    }
    .removetr{
        position: absolute;
        z-index: 10;
    }
    .title_plan{
        background: #8bc34a9c;
        font-weight: bold;
    }
    .title_plan input{
        background: transparent;
        font-size: 13.5px;
        font-weight: bold;
        max-width: 100px;
    }
</style>
<input type="hidden" id="base_url" value="<?php echo base_url() ?>">
<div class="row" style="background-color: #779155;">
	<div class="col-md-12" style="margin: 10px !important;">
	</div>
</div>
<div class="row">
	<div class="col-md-12" style="padding: 2.2rem;">
		<div class="r-square square-green">
            <div class="card-body" style="padding: 0.25rem !important;">
            	<div class="row">
					<div class="col-md-12" align="center">
						<img style="width: 200px" src="<?php echo base_url() ?>images/Logo.png">
					</div>
				</div>
				<hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4>Buen día <?php echo $get_p->nombre.' '.$get_p->apll_paterno.' '.$get_p->apll_materno ?></h4>
                        <p style="font-size: 16px !important;"><span style="font-weight: bold;">Su médico de Karla Coello</span> le envía la siguiente información</p>
                    </div>
                </div>
                <hr> 
               <?php echo $info_consulta ?> 
            </div>
        </div>
	</div>
</div> 