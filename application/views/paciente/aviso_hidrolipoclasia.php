<html><head>
  <title>Karla Coello</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0">
  <meta name="author" content="Carlos Corona">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>images/FAV.png">
    <link href="<?php echo base_url();?>aviso/bootstrap.min.css?v=34-2-5-16" rel="stylesheet" type="text/css" lazyload="">
    <link href="<?php echo base_url();?>assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
 </head>
 <style type="text/css">
 	@media print {
		html {
			margin: 40px;
		}
	}
 </style>
	<body>
		<div id="" style="text-align:center">
			<h4>CONSENTIMIENTO INFORMADO PARA LA REALIZACIÓN DE HIDROLIPOCLASIA</h4>
		</div>
		<div id="date" style="text-align:right;position:fixed;right:1cm;top:0.5cm;">
		    <?php echo date('d/m/Y');?>	
	    </div>
		<div style="text-align: justify; list-style-type: upper-latin; font-size: 11.4px">
            <b>Consecuencias que frecuentemente se producen</b>
            Enrojecimiento alrededor de los pinchazos que normalmente pueden tardar unas
			horas o pocos días en desaparecer. Sensación de incomodidad dependiendo de la zona
			a tratar y de la sensibilidad personal.
			<br>
			<b>Riesgos generales</b><br>
            La hidrolipoclasia consiste en la administración de solución fisiológica al 0.9% esteril,
			lidocaína simple 2% y dosis de medicamentos mediante la realización de un número
			variable de pequeños pinchazos en la piel (vía intradérmica). Utilizando:
            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:12cm;border-bottom:1px solid black;"></div>
			<br>
            La técnica esta contraindicada durante el embarazo y lactancia.<br>
            Existe una posibilidad, aunque mínima, de que aparezca pigmentación en la zona de
            tratamiento sobre todo si hay exposición solar o a lámparas UV después del mismo.
            <br>
            La posibilidad de hematomas es un riesgo frecuente que depende también de la toma de
			ciertos medicamentos por parte del paciente (p.ej. aspirina y otros anticoagulantes).
			Informe al médico de la toma de cualquier medicación antes de ser sometido a un
			tratamiento con la técnica mesoterápica.
			<br>
			Hasta pasados 3-4 semanas del procedimiento no debe acudir a saunas o piscinas para
            evitar la contaminación de los puntos de puntura.
            <br>
            <b>Riesgos personales</b>
            Informaciones de interés sobre el procedimiento.
            <br>
            El tratamiento de hidrolipoclasia consiste en la inyección intradérmica de medicamentos
			a pequeñas dosis. La técnica consigue aumentar el efecto de los medicamentos
			administrados por lo que las posibilidades de producir efectos secundarios son muy
			reducidas aunque no se pueden descartar completamente.
			<br>
			El número de sesiones y la frecuencia de aplicación de las mismas es variable en
			función de la patología de tratar y de la idiosincrasia de cada paciente por lo que no
			puede ser determinado de antemano.
            <br>
            Los resultados se obtienen con mayor efectividad si el tratamiento realizado se
			complementa con otros tratamientos que potenciarán sus efectos. (RADIOFRECUENCIA
			O ULTRASONIDO )
            <br>
            <b>Autorización</b>
            <br>
            He sido informado y he entendido que existen riesgos aunque éstos sean mínimos. Si
			surge alguna complicación doy mi consentimiento para que se haga lo que sea más
			conveniente.
			<br>
            He sido informado de que en algunas circunstancias es incompatible tomar el sol de
			forma simultánea a la realización del tratamiento.
			He sido informado de que la posibilidad de que surjan reacciones alérgicas o de
			hipersensibilidad es la misma que si se emplease cualquier otra vía de administración de
			fármacos.	
			<br>
			He sido informado de que la aparición de hematomas es más frecuente por esta vía de administración de fármacos que por otras vías.
            <br>
            Me han informado y he entendido plenamente los posibles riesgos.
            <br>
            Me han informado del derecho que tengo de aceptar o rechazar el procedimiento así
			como también del derecho de anular la aceptación previa, de las posibilidades de éxito
			del tratamiento y he podido preguntar lo que me ha parecido conveniente al respecto.
			Sé que puedo exponer condiciones particulares o reservas referentes al citado
			procedimiento y la forma de realizarlo que he solicitado.
			<br>
            Autorizo al servicio citado y al equipo asistencial a realizar la administración de
			los fármacos necesarios para mi tratamiento asumiendo las consecuencias y los riesgos
			más frecuentes citados.  			
            <br>
            Todo lo anterior se me ha explicado claramente y acepto el
			tratamiento propuesto siendo consciente de las posibilidades de éxito y de las posibles
			complicaciones por lo que firmo en señal de acuerdo, de aceptación y entendimiento de
			este consentimiento.
            <br>
            <!-- --->
            <div style="">
            <input type="hidden" id="base_url" value="<?php echo base_url(); ?>" readonly>
            <input type="hidden" id="idpaciente" value="<?php echo $paciente->idpaciente ?>">
            <input type="hidden" id="tipo_aviso" value="<?php echo $tipo_aviso ?>">
             <?php if($iddocumento==0){ ?>
                  <?php if($tipo==0){ ?>
                        <table style="width: 100%">
                              <tbody>
                                    <tr style="text-align: center">
                                          <td style="width: 50%">
                                                <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div>
                                          </td>
                                          <td style="width: 50%">
                                                <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div>
                                          </td>
                                    </tr>
                                    <tr style="text-align: center">
                                          <td style="width: 50%">
                                                DRA KARLA COELLO VÁZQUEZ:
                                          </td>
                                          <td style="width: 50%;">
                                                Nombre y firma del paciente:
                                                <div style="font-size: 16px">
                                                    <?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?> 
                                                </div> 
                                          </td>
                                    </tr>
                              </tbody>
                        </table>
                  <?php } else{?>
                        <table style="width: 100%">
                              <tbody>
                                    <tr style="text-align: center">
                                    	<td style="width: 50%">
                                            <div class="firma_text" id="aceptance">
                                                  <div id="signaturepfc1" class="signaturepfc1 col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                                                        <label for="patientSignaturem" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma en el recuadro, su mouse o su dedo</label><br>
                                                        <canvas id="patientSignaturem" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
                                                        <img src="<?php echo base_url() ?>public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignaturepfc1" data-signature="patientSignaturem">
                                                        <br>                       
                                                  </div>                  
                                            </div>
                                        </td>
                                         <td style="width: 50%">
                                            <div class="firma_text" id="aceptance">
                                                  <div id="signature" class="signature col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                                                        <label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma en el recuadro, su mouse o su dedo</label><br>
                                                        <canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
                                                        <img src="<?php echo base_url() ?>public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignature" data-signature="patientSignature">
                                                        <br>                       
                                                  </div>                  
                                            </div>
                                        </td>
                                          
                                    </tr>
                                    <tr style="text-align: center">
                                        <td style="width: 50%">
                                            DRA KARLA COELLO VÁZQUEZ:
                                        </td>
                                        <td style="width: 50%;">
                                            Nombre y firma del paciente:
                                            <div style="font-size: 16px">
                                                <?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?> 
                                            </div> 
                                        </td>
                                    </tr>
                              </tbody>
                        </table>
                        <div class="row" style="text-align: center">
                              <button class="btn_estilo" id="btn_firma_save" onclick="saveex_paciente_medico()">Aceptar Firma</button>
                        </div>   
                  <?php }?>
             <?php }else {
                  $result_get=$this->General_model->get_record('iddocumento',$iddocumento,'documentos_legales');
                  ?>
                  <table style="width: 100%">
                        <tbody>
                              <tr style="text-align: center">
                              	<td style="width: 50%">
                                      <?php
                                        $fh2 = fopen(base_url()."uploads/paciente_firma_doc/".$result_get->firma_medico, 'r') or die("Se produjo un error al abrir el archivo");
                                        $linea2 = fgets($fh2);
                                        fclose($fh2);  
                                        ?>
                                        <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                                            <img src="<?php echo $linea2 ?>" width="170" height="75" style="border:dotted 1px black;">
                                        </div> 
                                </td>
                                <td style="width: 50%">
                                      <div style="text-align:center;margin-bottom:10px;border-radius:4px;">
                                        <?php
                                        $fh = fopen(base_url()."uploads/paciente_firma/".$result_get->firma, 'r') or die("Se produjo un error al abrir el archivo");
                                        $linea = fgets($fh);
                                        fclose($fh);  
                                        ?>
                                        <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                                            <img src="<?php echo $linea ?>" width="170" height="75" style="border:dotted 1px black;">
                                        </div> 
                                      </div>          
                                </td>
                              </tr>
                              <tr style="text-align: center">
                                    <tr style="text-align: center">
                                      <td style="width: 50%">
                                            DRA KARLA COELLO VÁZQUEZ:
                                      </td>
                                      <td style="width: 50%;">
                                            Nombre y firma del paciente:
                                            <div style="font-size: 16px">
                                                <?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?> 
                                            </div> 
                                      </td>
                                    </tr>
                              </tr>
                        </tbody>
                  </table>
            <?php }?>
            </div>
            <!-- --->	   
            TRATAMIENTO A REALIZAR
            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:5cm;border-bottom:1px solid black;"></div>ÁREAS
            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:2cm;border-bottom:1px solid black;"></div>
            <br>
            COSTO TOTAL $
            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:5cm;border-bottom:1px solid black;"></div> 
        </div>	
    </body>
</html>