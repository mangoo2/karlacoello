<input type="hidden" id="base_url" value="<?php echo base_url() ?>">
<style type="text/css">
	body{
		background-color: #E6E9F0 !important;
	}
	.square-green {
	    border-left: 8px solid #346c94;
	}
	.r-square {
	    background-color: #fff;
	    border-radius: 4px;
	    padding: 15px 15px 15px 20px;
	    box-shadow: 1px 2px 0px 0px rgba(45, 84, 185, 0.07);
	}

	.margen_todo_gris {
	    border-style: solid;
	    border-width: 1px;
	    border-color: #e5e6e5;
	    background-color: f8f9fa;
	}
	.r-square2 {
	    background-color: #f3f4f5;
	    border-radius: 4px;
	    padding: 15px 15px 15px 20px;
	    box-shadow: 1px 2px 0px 0px rgba(45, 84, 185, 0.07);
	}
    .btn-info{
	    background: #346c94 !important;
	}
</style>
<div class="row" style="background-color: #346c94;">
	<div class="col-md-12" style="margin: 10px !important;">
	</div>
</div>
<div class="row">
	<div class="col-md-12" align="center">
		<img width="11%" src="<?php echo base_url() ?>public/img/centroneuro/logoparkinson2.png">
	</div>
</div>
<div class="row">
	<div class="col-md-12" style="padding: 2.2rem;">
		<div class="r-square square-green">
            <div class="card-body" style="padding: 0.25rem !important;">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Buen día <?php echo $paciente ?></h4>
                        <p style="font-size: 16px !important;"><span style="font-weight: bold;">Su médico del Proyecto Parkinson</span> le envía la siguiente información</p>
                    </div>
                </div>
                <hr> 
                <div class="row">
                	<div class="col-md-12">
                		<p><i class="fas fa-calendar-check"></i> Última consulta: <?php echo date('d/m/Y',strtotime($consultafecha)); ?></p>
          
                	</div>
                </div>
                <?php if($idcita!=0){ ?> 
                <div class="row">
                	<div class="col-md-12">
                		<span class="badge badge-warning badge-pill" style="background-color: #f9efa6 !important; font-weight: bold; color: #346c94; font-size: 14px !important;"><i class="fas fa-exclamation-circle" style="color: #f0ad4e;"></i> Fecha y Hora de la cita: <?php echo date('d/m/Y',strtotime($fecha_consulta)).'  '.$hora_de?></span><br><br>
                		<button type="button" class="btn waves-effect waves-light btn-secondary" onclick="modal_confirmar(<?php echo $idcita ?>)">Confirme su cita</button>
                		<button type="button" class="btn waves-effect waves-light btn-secondary" onclick="modal_cancelar(<?php echo $idcita ?>)">Cancele su cita</button>
                	</div>
                </div>
                <?php } ?> 
            </div>
        </div>
	</div>
</div>
<h4 align="center">
	<i class="fas fa-file-alt"></i> Información de la consulta
</h4> 
<div class="row">
	<div class="col-md-12" style="padding: 2.2rem;">
    
		<button type="button" class="btn waves-effect waves-light btn-light square-green" onclick="receta()"><i class="fas fa-file-medical text-grey"></i>  Última receta</button>
        <div class="text_receta"> 
				<div class="r-square2" style="border-style: solid; border-width: 1px; border-color: #e5e6e5;">
		            <div class="card-body" style="padding: 0.25rem !important;">
		                <div class="row">
		                    <div class="col-md-12">
		                    	<div class="row">
		                    		<div class="col-md-6">
		                                <h4>Receta médica :</h4>
	                                </div>
	                                <div class="col-md-6" align="right">
		                                <span style="font-weight: bold; color: #346c94; font-size: 16px !important;">Fecha de emisión: </span><span><?php echo date('d/m/Y',strtotime($consultafecha)); ?></span>
	                                </div>
	                            </div>    
                                
		                        <div class="row">
				                	<div class="col-md-12">
				                		<div class="r-square">
								            <div class="card-body" style="padding: 0.25rem !important;">
								                <div class="row">
								                    <div class="col-md-12">
								                    	<!--- --->
								                		<h5 class="card-header" style="background-color: #346c94!important; color: white; border-radius: 5px">MEDICAMENTOS</h5>
								                	     	<?php
								                		    $aux_r=0;  
								                		    foreach ($get_receta as $item){ 
                                                                $aux_r=1;      

                                                                $arrayinfom = array('idreceta'=>$item->idreceta,'activo'=>1);
														        $get_medi=$this->General_model->getselectwhereall('receta_medicamento',$arrayinfom);
								                		    ?>
									                            <ul>
									                               <?php 
	                                                              
		                                                            $cont_aux=1;
										                                foreach ($get_medi as $itemm){ ?>	
					                                                    <span><?php echo $cont_aux ?> )<strong> <?php echo $itemm->medicamento ?> </strong></span><br> 
																		<span><span style="color: #346c9400">__</span> <?php echo $itemm->tomar ?> </span><hr>  

		                                                            <?php $cont_aux++;
		                                                            }
		                                                            ?>   
									                            </ul>
									                            <?php if($item->indicaciones!=''){ ?>
		                                                            <h5 class="card-header" style="background-color: #346c94!important; color: white; border-radius: 5px">Indicaciones Adicionales de la Receta:</h5>
					                                                <ul>
					                                                	<strong><?php echo $item->indicaciones ?></strong>
					                                                </ul> 
				                                                <?php } ?>
			                                                <?php } 
                                                            if($aux_r==0){ ?>
                                                                <p>No hay medicamentos asignados!!!</p>  
                                                        <?php }
			                                            ?>    
								                        <h5 class="card-header" style="background-color: #346c94!important; color: white; border-radius: 5px">Diagnósticos:</h5>
		                                                <ul>
		                                                	<?php 
                                                            $cont_d_aux=1;
		                                                	foreach ($get_diagnosticos as $item){ ?>
		                                                	      <span><?php echo $cont_d_aux ?>) <?php echo $item->diagnostico ?></span><br> 
		                                                	<?php $cont_d_aux++;
		                                                	} 
		                                                	?>
							                            </ul>
										                
										                <!--- --->
								                    </div>
								                </div>
								            </div>
								        </div>
				          
				                	</div>
				                </div>
		                    </div>
		                </div>
		            </div>
		        </div>
	    </div>
	    <!-- ===================== -->
	    <br><br>
	    <button type="button" class="btn waves-effect waves-light btn-light square-green" onclick="estudio()"><i class="fas fa-notes-medical"></i> Estudios solicitados
		</button>
        <div class="text_estudio" style="display: none">
			<div class="r-square2" style="border-style: solid; border-width: 1px; border-color: #e5e6e5;">
	            <div class="card-body" style="padding: 0.25rem !important;">
	                <div class="row">
	                    <div class="col-md-12">
	                        <div class="row">
	                    		<div class="col-md-6">
	                                <h4>Estudios solicitados:</h4>
                                </div>
                                <div class="col-md-6" align="right">
	                                <span style="font-weight: bold; color: #346c94; font-size: 16px !important;">Fecha de emisión: </span><span><?php echo date('d/m/Y',strtotime($consultafecha)); ?></span>
                                </div>
                            </div>   
	                        <div class="row">
			                	<div class="col-md-12">
			                		<div class="r-square">
							            <div class="card-body" style="padding: 0.25rem !important;">
							                <div class="row">
							                	<!--- orden_estudio_laboratorio --->
							                	
							                	<?php 
                                                $or_aux=0;
							                	foreach ($get_orden_estudio as $item){ 
							                		$or_aux=1;
							                	}
                                                if($or_aux==1){
							                	?>	
							                    <div class="col-md-12">
							                    	<!--- ======= --->
							                		<h5 class="card-header" style="background-color: #f3f4f5!important; color: #346c94; border-radius: 5px">Orden de laboratorio</h5>
						                            <?php foreach ($get_orden_estudio as $item){ 
                                                            $arrayinfode = array('idorden'=>$item->idorden,'activo'=>1);
														    $get_orden=$this->General_model->getselectwhereall('orden_estudio_laboratorio',$arrayinfode);   
						                                ?>
								                            <ol> 
		                                                        <?php foreach ($get_orden as $iteom){ ?>
		                                                        	<li><u><?php echo $iteom->detalles ?></u></li>
		                                                        <?php } ?>
								                            </ol>
							                            <ul>
							                            	<strong>Observaciones:</strong><br>
							                            	<p><?php echo $item->observaciones ?></p>

							                            </ul>
							                            <?php if($item->recomendar=='1'){ ?>
                                                        <h5 class="card-header" style="background-color: #f3f4f5!important; color: black; border-radius: 5px">FAVOR DE OTORGAR DISCO AL PACIENTE.</h5>
                                                        <span><strong style="font-weight: bold;"><?php echo $item->nombre_laboratorio ?> (tel:<?php echo $item->telefono ?>) </strong>  <?php echo $item->direccion ?></span>
                                                        <?php } ?>    
							                        <?php } ?>    
	                                                <ul>
						                                <button type="button" class="btn waves-effect waves-light btn-info" onclick="ver_pef(<?php echo $idconsulta ?>)">Ver PDF</button>
						                            </ul>
									                
									                <!--- ======= --->
							                    </div>
							                    <?php } 

                                                $int_aux=0;
							                	foreach ($get_interconsulta as $item){ 
							                		$int_aux=1;
							                	}
                                                if($int_aux==1){
							                    ?>    
							                    <!--- Orden de interconsulta --->
							                    <div class="col-md-12">
							                    	<hr>
							                    	<!--- ======= --->
							                		<h5 class="card-header" style="background-color: #f3f4f5!important; color: #346c94; border-radius: 5px">Orden de interconsulta</h5>
						                            <?php foreach ($get_interconsulta as $item){ 
                                                            $arrayinfoin = array('idinterconsulta'=>$item->idinterconsulta,'activo'=>1);
														    $get_ordenin=$this->General_model->getselectwhereall('interconsulta_medico',$arrayinfoin);   
						                                ?>
								                            <ul> 
		                                                        <?php foreach ($get_ordenin as $iteom){ ?>
		                                                        	<li><strong><?php echo $iteom->nombre ?></strong> <?php echo $iteom->especialidad ?></li>
		                                                        <?php } ?>
								                            </ul>
							                            <ul>
                                                            <div class="card-header"><?php echo $item->motivo_interconsulta ?></div>
							                            </ul>
                                                        <ul>
							                                <button type="button" class="btn waves-effect waves-light btn-info" onclick="imprimir_pdf_interconsulta(<?php echo $iteom->idinterconsulta ?>)">Ver PDF</button>
							                            </ul>
							                        <?php } ?>    
	                                                
									                
									                <!--- ======= --->
							                    </div>
							                    <?php }

							                    $oh_aux=0;
							                	foreach ($get_orden_hospital as $item){ 
							                		$oh_aux=1;
							                	}
                                                if($oh_aux==1){
							                    ?>   
							                    <!--- Orden de hospitalización --->
							                    <div class="col-md-12">
							                    	<hr>
							                    	<!--- ======= --->
						                            <?php foreach ($get_orden_hospital as $item){ ?>
						                            <h5 class="card-header" style="background-color: #f3f4f5!important; color: #346c94; border-radius: 5px">Orden de hospitalización</h5><br>
							                		    <ul> 
		                                                    <div class="card-header"><?php echo $item->detalles ?></div><br>
		                                                </ul>   
		                                                <ul>
							                                <button type="button" class="btn waves-effect waves-light btn-info" onclick="ver_pef(<?php echo $item->idorden_hospital ?>)">Ver PDF</button>
							                            </ul>
							                        <?php } ?> 
							                            
									                
									                <!--- ======= --->
							                    </div>
							                    <?php }


							                    $oh_aux=0;
							                	foreach ($get_certificado_medico as $item){ 
							                		$oh_aux=1;
							                	}
                                                if($oh_aux==1){
							                    ?>   
							                    <!--- Certificado Médico --->
							                    <div class="col-md-12">
							                    	<hr>
							                    	<!--- ======= --->
							                		<h5 class="card-header" style="background-color: #f3f4f5!important; color: #346c94; border-radius: 5px">Certificado Médico</h5><br>
							                		    <ul> 
						                            <?php foreach ($get_certificado_medico as $item){ ?>
		                                                    <div class="card-header"><?php echo $item->detalles ?></div><br>
							                        <?php } ?> 
							                            </ul>   
	                                                <ul>
	                                                	<?php /*
						                                <button type="button" class="btn waves-effect waves-light btn-info" onclick="ver_pef(<?php echo $idconsulta ?>)">Ver PDF</button>
						                                */
						                                ?>
						                            </ul>
									                
									                <!--- ======= --->
							                    </div>
							                    <?php } 

							                    $ju_aux=0;
							                	foreach ($get_orden_justificante_medico as $item){ 
							                		$ju_aux=1;
							                	}
                                                if($ju_aux==1){
							                    ?>   
							                    <!--- Certificado Médico --->
							                    <div class="col-md-12">
							                    	<hr>
							                    	<!--- ======= --->
							                		<h5 class="card-header" style="background-color: #f3f4f5!important; color: #346c94; border-radius: 5px">Justificante Médico</h5><br>
							                		    <ul> 
						                            <?php foreach ($get_orden_justificante_medico as $item){ ?>
		                                                    <div class="card-header"><?php echo $item->detalles ?></div><br>
							                        <?php } ?> 
							                            </ul>   
	                                                <ul>
	                                                	<?php /*
						                                <button type="button" class="btn waves-effect waves-light btn-info" onclick="ver_pef(<?php echo $idconsulta ?>)">Ver PDF</button>
						                                */
						                                ?>
						                            </ul>
									                
									                <!--- ======= --->
							                    </div>
							                    <?php } 

							                    $orl_aux=0;
							                	foreach ($get_orden_orden_libre as $item){ 
							                		$orl_aux=1;
							                	}
                                                if($orl_aux==1){
							                    ?>   
							                    <!--- Orden libre --->
							                    <div class="col-md-12">
							                    	<hr>
							                    	<!--- ======= --->
							                		<h5 class="card-header" style="background-color: #f3f4f5!important; color: #346c94; border-radius: 5px">Orden libre</h5><br>
							                		    <ul> 
						                            <?php foreach ($get_orden_orden_libre as $item){ ?>
		                                                    <div class="card-header"><?php echo $item->detalles ?></div><br>
							                        <?php } ?> 
							                            </ul>   
	                                                <ul>
	                                                	<?php /*
						                                <button type="button" class="btn waves-effect waves-light btn-info" onclick="ver_pef(<?php echo $idconsulta ?>)">Ver PDF</button>
						                                */
						                                ?>
						                            </ul>
									                
									                <!--- ======= --->
							                    </div>
							                    <?php } 

							                    ?> 


							                    <!--- --->
							                </div>
							            </div>
							        </div>
			          
			                	</div>
			                </div>
	                    </div>
	                </div>
	            </div>
	        </div>
        </div>
        <!--
        <button type="button" class="btn waves-effect waves-light btn-light square-green" onclick="intografia()"><i class="fas fa-image"></i> Infografías</button>
        <div class="text_intografia" style="display: none">
        
        </div>
        -->
        <button type="button" class="btn waves-effect waves-light btn-light square-green" onclick="medico()"><i class="fas fa-upload"></i> Enviar archivos al médico</button>
        <div class="text_medico" style="display: none">
        	<div class="row">
				<div class="col-md-12" style="padding: 2.2rem;">
					<div class="r-square">
			            <div class="card-body" style="padding: 0.25rem !important;">
			                <div class="row">
			                	<div class="col-md-3" align="center">
	                              <span class="arch_medico" >
	                                <input type="file" name="arch_medico" id="arch_medico" style="display: none">
	                                </span>
	                                <label for="arch_medico">
	                                    <span class="btn waves-effect waves-light btn-secondary"><img style="width: 80px;" src="<?php echo base_url() ?>images/guardar_archivo.png"></span>
	                                </label>
	                            </div>
			                </div>
			            </div>
			        </div>
				</div>
			</div>
        </div>
    <!-- -->    
	</div>
</div>

<div id="archivo_medico_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Subir Archivos</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="form" method="post" role="form" id="form_paciente">
                    <input type="hidden" name="tipo" id="tipo" value="0"> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="">
                                <label>Título</label>
                                <input type="text" name="nombre" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="">
                                <label><i class="fas fa-calendar-alt"></i> Fecha</label>
                                <input type="text" name="nombre" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="">
                                <label>Título</label>
                                <select name="estado_civil" class="form-control">
                                    <option value="1">Enviados por el Paciente</option>
                                </select>
                            </div>
                        </div>
                         <div class="col-md-12">
                         	<br>
                            <div align="center">
	                             <img id="img_avatar2" style="width: 120px;">
                                <span class="arch_medico2" >
                                    <input type="file" name="arch_medico2" id="arch_medico2" style="display: none">
                                </span>
                                <label for="arch_medico2">
                                    <span class="btn waves-effect waves-light btn-secondary"><i class="far fa-images"></i> Cambiar Archivo</span>
                                </label>
                            </div>
                        </div>
                         <div class="col-md-12">
                            <div class="">
                                <label>Comentario</label>
                                <input type="text" name="nombre" class="form-control">
                            </div>
                        </div>
                    </div>
                </form>        
            </div>
            <input type="hidden" id="idorden_e">
            <input type="hidden" id="idorden_remove">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-info waves-effect" onclick="eliminarorden_libre()">Adjuntar</button>
            </div>
        </div>   
    </div>
</div> 
<div id="confimar_cita_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Desea confirmar su cita?</h5>
            </div>
            <input type="hidden" id="idconfirmar_cita">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="confirmar_cita()">Confirmar cita</button>
            </div>
        </div>   
    </div>
</div> 
<div id="cancelar_cita_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Desea cancelar la cita? Esta acción no se puede deshacer?</h5>
            </div>
            <input type="hidden" id="idcancelar_cita">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="cancelar_cita()">Cancelar cita</button>
            </div>
        </div>   
    </div>
</div>   