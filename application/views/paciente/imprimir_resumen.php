<?php 
        $idc=$idconsultap;
        $get_infoc=$this->General_model->get_record('idconsulta',$idc,'consultas');
        if($get_infoc->status==0){
            $style_color='';
            $importante="Importante";
        }else{
            $importante="No Importante";
            $style_color='background-color: #fff7b4;';
        }
        $get_infop=$this->General_model->get_record('idpaciente',$get_infoc->idpaciente,'pacientes');
        $tiempo = strtotime($get_infop->fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
            $edad = floor($edad); 
        $nombre=$get_infop->nombre.' '.$get_infop->apll_paterno.' '.$get_infop->apll_materno;
        $aux_existe=0;
        $fecha_consulta_ultima=$this->fechainicio;
        $arrayinfop = array('idpaciente'=>$get_infoc->idpaciente);
        $get_consultasp_desc=$this->General_model->getselectwhere_orden_desc('consultas',$arrayinfop,'consultafecha');
        foreach($get_consultasp_desc as $item){
            $aux_existe=1;
            $fecha_consulta_ultima=$item->consultafecha;
        }
        $html='';
        ?>
        <style type="text/css">
            body{
                padding-left: 50px;
                padding-right: 50px;
                padding-top: 300px;
            }
        </style>
        <img width="100%" src="<?php echo base_url(); ?>public/img/centroneuro/portada_header.jpg" style="position: absolute;top: 0px;left: 0;">
        <img width="100%" src="<?php echo base_url(); ?>public/img/centroneuro/portada_footer2.jpg" style="position: absolute;bottom: 0px;left: 0;">
        <div class="row">
            <div class="col-md-12" align="right">
                <h3 style="color: black"><u>Fecha de consulta: <?php echo date('d/m/Y',strtotime($get_infoc->consultafecha)); ?></u><h3>    
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <u><h3 style="color: black"><?php echo $nombre; ?><h3></u>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h4 style="color: black">Resumen de consulta<h4> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h4 style="color: black"> Edad del paciente: <?php echo $edad ?> años</span></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="color: black">
            <?php if($fecha_consulta_ultima==$get_infoc->consultafecha || $aux_existe==0){  ?> 
                <h4 class="div_abajo_solid">Padecimiento Actual</h4>
                <p><?php echo $get_infoc->nota_evaluacion ?></p>
                <h4 class="div_abajo_solid">Fecha de inicio del padecimiento</h4>
                <p><?php echo date('d/m/Y',strtotime($get_infoc->fechainicio)) ?></p>
            <?php }else{ ?> 
                <h4 class="div_abajo_solid">Nota de Evolución</h4>
                <p><?php echo $get_infoc->nota_evaluacion ?></p>
            <?php } ?> 
            </div>
        </div>
            <?php if($get_infoc->altrura!=0  || $get_infoc->peso!=0 || $get_infoc->ta!='' || $get_infoc->tempc!=0 || $get_infoc->fc!='' || $get_infoc->fr!=0 || $get_infoc->o2!=0){  ?>
                <div class="row" style="color: black">
                    <div class="col-md-12">
                        <h4 class="div_abajo_solid">Signos Vitales/Básicos</h4>
                    </div>
                </div>
                <div class="row" style="color: black">
                
            <?php if($get_infoc->altrura!=0){ ?>
                   <div class="col-md-3">
                        <p>Altura <span class="div_etiqueta"><?php echo $get_infoc->altrura ?></span></p>
                    </div>
            <?php  } 
                if($get_infoc->peso!=0){ ?>
                    <div class="col-md-3">
                        <p>Peso <span class="div_etiqueta"><?php echo $get_infoc->peso ?></span></p>
                    </div>
            <?php  } 
                if($get_infoc->ta!=''){ ?>
                    <div class="col-md-3">
                        <p>T.A. <span class="div_etiqueta">&nbsp<?php echo $get_infoc->ta ?>&nbsp</span></p>
                    </div>
            <?php  } 
                if($get_infoc->tempc!=0){ ?>
                    <div class="col-md-3">
                        <p>Temp <span class="div_etiqueta">&nbsp<?php echo $get_infoc->tempc ?>&nbsp</span></p>
                    </div>
            <?php  } 
                if($get_infoc->fc!=''){ ?>
                    <div class="col-md-3">
                        <p>F.C. <span class="div_etiqueta">&nbsp<?php echo $get_infoc->fc ?>&nbsp</span></p>
                    </div>
            <?php  } 
                if($get_infoc->fr!=0){ ?>
                    <div class="col-md-3">
                        <p>F.R. <span class="div_etiqueta">&nbsp<?php echo $get_infoc->fr ?>&nbsp</span></p>
                    </div>
            <?php  } 
                if($get_infoc->o2!=0){ ?>
                    <div class="col-md-3">
                        <p>O2 <span class="div_etiqueta">&nbsp<?php echo $get_infoc->o2 ?>&nbsp</span></p>
                    </div>
            <?php  } ?>
                </div>
            <?php } ?> 
            <div class="row" style="color: black">
                <div class="col-md-12">
                    <?php if($get_infoc->exploracionfisica!=''){ ?>
                    <h4 class="div_abajo_solid">Exploración Física</h4>
                        <p><?php echo $get_infoc->exploracionfisica ?></p>
                    
                    <?php } ?> 
                </div>
                <div class="col-md-12">
                    <?php if($get_infoc->resultadoestudiolaboratorio!=''){  ?>
                    <h4 class="div_abajo_solid">Estudios de Laboratorio</h4>
                        <p><?php echo $get_infoc->resultadoestudiolaboratorio ?></p>
                    
                    <?php } ?> 
                </div>
                 <div class="col-md-12">
                    <?php
                    $aux_dg=0;
                    $arraydiag = array('idconsulta' => $idc,'activo'=>1);
                    $get_diagn=$this->General_model->getselectwhereall('diagnosticos',$arraydiag);
                    foreach($get_diagn as $key){
                        $aux_dg=1;
                    } 
                    if($aux_dg==1){ ?> 
                        <h4 class="div_abajo_solid">Diagnósticos</h4>
                        <ol>
                        <?php
                        foreach ($get_diagn as $item){ ?> 
                        <li><?php echo $item->diagnostico ?></li>   
                        <?php } ?> 
                    </ol>
                    <?php } ?> 
                </div>
                <div class="col-md-12">
                    <?php
                    $arrayrece = array('idconsulta' => $idc,'activo'=>1);
                    $get_receta_t=$this->General_model->getselectwhereall('receta',$arrayrece);
                    $aux_v_r=0;
                    foreach ($get_receta_t as $item){
                        $aux_v_r=1;
                    } 
                    if($aux_v_r==1){ ?> 
                        <h4 class="div_abajo_solid">Medicamentos</h4>
                        <ol>
                        <?php   
                        foreach ($get_receta_t as $item){ 
                            $arrayrecem = array('idreceta'=>$item->idreceta,'activo'=>1);
                            $get_receta_m=$this->General_model->getselectwhereall('receta_medicamento',$arrayrecem);
                            foreach ($get_receta_m as $item){ ?> 
                                <li><?php echo $item->medicamento ?> - <?php echo $item->tomar ?></li>
                            <?php } ?> 
                        <?php } ?> 
                        </ol>
                    <?php } ?> 
                              
                </div>
                <div class="col-md-12">
                <?php   
                $arrayorde = array('idconsulta' => $idc,'activo'=>1);
                $get_orden_t=$this->General_model->getselectwhereall('orden_estudio',$arrayorde);
                $aux_o_r=0;
                foreach ($get_orden_t as $item){
                    $aux_o_r=1;
                } 
                if($aux_o_r==1){  ?> 
                    <h4 class="div_abajo_solid">Estudios Requeridos</h4>
                    <?php 
                    foreach ($get_orden_t as $a){ ?>
                        <h4 >Orden de laboratorio</h4>&nbsp;
                    <?php }      
                    foreach ($get_orden_t as $item){
                        $obsev=$item->observaciones; ?>
                        <ol>
                        <?php    
                        $arrayordenm = array('idorden'=>$item->idorden,'activo'=>1);
                        $get_orden_m=$this->General_model->getselectwhereall('orden_estudio_laboratorio',$arrayordenm);
                        foreach ($get_orden_m as $item){ ?>
                            <li><?php echo $item->detalles ?></li>
                        <?php } ?> 
                    </ol>
                    <?php if($obsev!=''){ ?>
                    <h4>Observaciones</h4>
                    <h4><?php echo $obsev ?></h4>
                    <?php } } ?> 
                <?php } ?>                 
                </div>
            </div>    
            <div class="row" style="color: black">
                <div class="col-md-12">
                    <h4><strong>Nombre del médico: <?php echo $this->administrador ?> - Última consulta: <?php echo date('d/m/Y',strtotime($get_infoc->consultafecha)).' '.date('G:i:s',strtotime($get_infoc->horainicio)) ?></strong></h4> 
                </div>
            </div>   