<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>" readonly>
<input type="hidden" name="personalId" id="personalId" value="<?php echo $personal->personalId ?>">
<?php if($paciente->doc_firma==''){?>
<div class="firma_text" id="aceptance">
	<div id="signature" class="signature col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
		<label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma en el recuadro empleando su mouse o su dedo</label><br>
		<canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
		<img src="<?php echo base_url() ?>public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignature" data-signature="patientSignature">
		<div class="arial_text" style="text-align:center;margin-top:0.1cm;">
			<?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?></div><br>
		<div class="row" >
			<button class="btn_estilo" id="btn_firma_save" onclick="saveex()">Aceptar Firma</button>
		</div>				
	</div>			
</div>
<?php }else{?>
<div id="aceptance">
    <div id="signature" class="signature col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
      <!--  <canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
      -->
        <?php 
        $fh = fopen(base_url()."uploads/paciente_firma/".$paciente->doc_firma, 'r') or die("Se produjo un error al abrir el archivo");
        $linea = fgets($fh);
        fclose($fh);  
        ?>
        <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
            <img src="<?php echo $linea ?>" width="355" height="160" style="border:dotted 1px black;">
        </div>
        <div style="display: none">
            <canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
            <div class="arial_text" style="text-align:center;margin-top:0.1cm;">
        </div>
            <?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?></div><br>             
    </div>          
</div>
<?php } ?>
<?php if($paciente->doc_firma!=''){?>
<script type="text/javascript">
    window.print();
</script>
<?php } ?>