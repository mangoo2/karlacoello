<style type="text/css">
	.arial_text{
		font-family: Arial;
	}
	.btn_estilo{
		text-decoration: none;
	    background-color: #45C0A5;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    -webkit-tap-highlight-color: transparent;
	    -webkit-transition: all linear;
	    -moz-transition: all linear;
	    -o-transition: all linear;
	    -ms-transition: all ease-out transition: all linear;
	    color: #fff;
	    box-shadow: 0 1px 3px 0 rgba(0,0,0,0.15);
	    min-width: 130px;
	    padding: 5px 30px;
	    outline: 0px;
	    color: #fff;
	    background-color: #34d4af;
	    height: 35px;
	    min-height: 35px;
	    border-radius: 3px;
	    font-size: 16px;
	    padding: 5px 20px;
	    cursor: pointer;
	    border: none;
	    text-align: center;
	    -webkit-appearance: none;
	    outline: none;
	    box-shadow: 
	}
    .btn-info{
        color: #fff;
        background-color: #03a9f3;
        border-color: #03a9f3;
    }
</style>
<article style="min-height: 640px;">
<img width="20%" src="<?php echo base_url() ?>/public/img/logo_unne.png">	
<div class="arial_text" id="" style="text-align:center"><h3>Aviso de Privacidad</h3></div>
<div id="date" style="text-align:right;position:fixed;right:1cm;top:0.5cm;">
	<?php echo date('d/m/Y',strtotime($fecha)) ?></div>
<div id="privaciy" class="arial_text">
  <ol class="hiddeable">
     <li>
     <u>Identidad y Domicilio del Responsable</u><br>
    <b><?php echo $personal->nombre ?></b>, (en adelante, el(la) «Responsable» y/o su «Médico») dedicado a la prestación de servicios profesionales médicos,  recaba sus datos personales como parte de una relación profesional. La captura y almacenamiento de sus datos personales se realiza por conducto de  karla coello, la cual se desempeña en esta relación como encargado, en términos de la legislación aplicable. Así, el Responsable usará y custodiará dichos datos personales solamente de acuerdo a lo señalado en el presente aviso de privacidad, por lo que puede estar seguro de que los mismos no serán utilizados para ningún otro fin que no sea lo que a continuación se manifiesta.
     </li>
         <br>
     <li>
           <u>Datos Personales que se Recaban</u><br>
            Este aviso de privacidad se pone a su disposición con el fin de ofrecerle el mejor servicio y atención, así como de dar cumplimiento a las disposiciones legales aplicables en la <u>Ley Federal de Protección de Datos Personales en Posesión de los Particulares</u> (en adelante, «LFPDPPP»).
            Con el fin de prestarle nuestros servicios para la finalidad arriba descrita, podemos requerir de usted la siguiente información y documentación:
                    <ol class="letras">
                             <li>Nombre del paciente y/o de quien lo representa;</li>
                            <li>Firma del paciente y/o de quien lo representa;</li>
                           <li> Información de facturación;</li>
                            <li>Fotografía del paciente y/o de quien lo representa;</li>
                            <li>Huella digital del paciente y/o de quien lo representa;</li>
                            <li>Teléfono;</li>
                            <li>Dirección;</li>
                            <li>Dirección de correo electrónico;</li>
                            <li> Nombre de contacto de emergencia;</li>
                            <li>Información de seguros</li>
                            <li>Información médica referida por otros médicos o instituciones médicas</li>
                            <li>Antecedentes de vacunación</li>
                            <li>Antecedentes médico-familiares, personales patológicos, personales no patológicos</li>
                            <li>Resultados de exámenes de laboratorio, análisis;</li>
                    </ol>
           
            Para efectos de la LFPDPPP -y para facilitar el entendimiento de este aviso- definimos a continuación los siguientes términos:
          <br>  <b>Responsable</b> y/o <b>Médico:</b> Su médico(a) y cualquier empleado de éste(a);
         <br>   <b>Titular:</b> Es la persona física a quien corresponden los datos personales quien proporcionará los datos por sí, o a través de la persona que la representa; 
     </li>
     
     <br>
     <li>
          <u> Finalidades Primarias y Secundarias</u> <br>
          <u>Finalidades Primarias</u>
          <br>
        Su médico(a) recaba sus datos personales para (A) conocer su historial, antecedentes y/o cualesquiera condiciones médicas a fin de poderle prestar la atención médica y servicios más aptos para sus necesidades particulares; y (B) compartir con otros especialistas/instituciones médicos aquella información que sea necesaria para atender cualesquiera emergencias médicas; y/o (C) compartir con otros especialistas/instituciones médicos aquella información que sea necesaria para que usted reciba la atención médica y servicios más aptos para sus necesidades particulares.<br>
        <b>¿Para qué usamos sus Datos Personales?</b><br>
        Su médico(a) recaba y usa sus datos para complementar la finalidad primaria de:
       
       
           <ul>   
                 <li>Confirmar su identidad; </li>
                 <li>Elaborar un expediente de paciente que facilite a su médico(a) conocer detalladamente su historial y antecedentes médicos, así como cualesquiera condiciones particulares que se deban considerar al momento de realizar consultas, de emitir diagnósticos y/o pronósticos, de recomendar procedimientos y/o tratamientos y, en caso de ser necesario, de referir al paciente a otro especialista/institución médica;
                 </li>
                 <li>Notificarle sobre citas programadas y/o próximas;</li>
                  <li>Notificarle sobre el momento, frecuencia y cantidad en que debe tomar cualesquiera medicamentos que le sean recetados;</li>
                  <li>Contar con información médica precisa para atender emergencias, en caso de ser necesario, y de notificarlas a la persona que usted indique;</li>
                 <li> En su caso, cumplir lo requerido por la Ley; y</li>
                 <li> Cumplir con los requerimientos de naturaleza legal que cualquier autoridad competente o cualquier legislación aplicable imponga a su médico(a).</li>
                 <ul><li>
                 Elaborar informes/reportes para entregar a compañías aseguradoras a efecto de realizar trámites ante ella; y/o
                 
                 </li></ul>
            </ul>
                 <br>
                    <u>Transferencia de sus Datos para dar Cumplimiento a las Finalidades Primarias:</u><br>
                        Sus datos solamente serán usados dentro del territorio nacional. Sus datos personales se transferirán:
                    <ul>
                        <li>En caso de que la atención y servicios médicos que usted necesita requieran que su médico(a) realice consultas con otro médico(a) especialista o que comparta su información con otro(a) médico(a) y/o institución médica para poder atender emergencias y/o para que usted reciba la atención médica y servicios más aptos para sus necesidades particulares (para lo cual, éstos tendrán la obligación de proporcionarle su propio aviso de privacidad; </li>
                        <li>Instituciones relacionadas con el sector salud que permitiran brindarle un mejor servicio y una mejor atención, y
</li>
                        <li>Para dar cumplimiento a las disposiciones oficiales de acuerdo a la legislación aplicable.</li>
                    </ul>
                    <br><u>Finalidades Secundarias; ¿para qué usamos sus Datos Personales?</u>
                    Su médico(a) recaba y usa sus datos para complementar las finalidades secundarias de:
                    
                    <ul>
                        
                        <li>Hacerle llegar información y/o promociones que pueden ser importantes para el cuidado de su salud.</li>
                    </ul>
                    <p>En caso de que no desee que sus datos personales se utilicen para estos fines secundarios el titular deberá marcar la casilla respectiva que aparece al lado de su nombre en este aviso de privacidad, con lo cual su médico(a) se abstendrá de utilizar sus datos personales para las finalidades secundarias arriba descritas.
                    </p>
                    <br>
                   <!-- <u>Transferencia de sus Datos para dar Cumplimiento a las Finalidades Secundarias:</u><br/>
                    Sus datos solamente serán usados dentro del territorio nacional. Sus datos personales se transferirán:
                    <ul>
                        <li>A su compañía aseguradora a efecto de realizar trámites ante ella </li>
                    </ul>
                    <br/>
                    En caso de que no desee que sus datos personales se utilicen para estos fines secundarios el titular deberá marcar la casilla respectiva que aparece al lado de su nombre en este aviso de privacidad, con lo cual su médico(a) se abstendrá de utilizar sus datos personales para las finalidades secundarias arriba descritas.
                    <br/>
    </li>
        <br/>
    <li>-->
        <u><b>Medios y procedimiento para el ejercicio de los derechos ARCO</b></u><br>
        Usted, como titular de los datos personales, tiene derecho a conocer qué datos personales tiene su médico(a) y para qué los utiliza <u>(Acceso)</u>. Asimismo tiene derecho a corregir su información en caso de que esta sea inexacta, incompleta o se encuentre desactualizada <u>(Rectificación)</u>. También, como titular de los datos personales, usted puede solicitar que estos se eliminen de nuestros registros <u>(Cancelación)</u>, en caso de que sus datos no sean utilizados adecuadamente. Por último, el titular tiene derecho a oponerse al uso de sus datos personales para fines específicos <u>(Oposición)</u>. Asi como la limitación a la divulgación y la revocación al consentimiento. Estos derechos se conocen como derechos ARCO y usted, como titular de los datos personales, los puede ejercer en cualquier momento a través del proceso que establece la Ley y que a continuación resumimos:

        
           <p><u><b>Procedimiento:</b></u><br></p>
            <p>Para ejercitar sus derechos ARCO el titular deberá formular una solicitud a través de los siguientes medios:</p>
                <ul>
                  <li> Solicitud Impresa
                         <ul>
                             <li>Presentada personalmente en el domicilio de su médico(a) dirigida a María Natalia Castelán Olmos</li>
                         </ul>
                   </li>
                      <li>Solicitud Electrónica</li>
                           <ul>
                             <li>Enviada vía correo electrónico a la dirección: ncastelan@inmunizaciones.com.mx</li>
                           </ul>
            
                 </ul>
        Con el fin de que su médico(a) esté en posibilidad de darle respuesta en el tiempo que señala la ley, todas las solicitudes de ejercicio de derechos ARCO -sean impresas o electrónicas- deberán:
        <br>
        <ol class="letras">
            <li>Especificar el(los) derecho(s) que desea hacer valer;</li>
            <li>Especificar las razones para ejercitar dicho derecho;</li>
            <li>Incluir una identificación oficial vigente del titular del derecho;</li>
        </ol>
    <br>
    En caso de que no se cumpla alguno de los requisitos arriba descritos, su médico(a) tendrá un plazo de cinco días naturales (contados a partir de la fecha en que haya recibido la solitud) para requerirle<u> por una sola vez</u> que aporte los documentos y/o información necesarios para dar trámite a la solicitud. A partir de la fecha en que se realice este requerimiento, el titular contará con diez días naturales para darle cumplimiento. En caso de no hacerlo dentro de dicho plazo, la solicitud de ejercicio de derechos ARCO se tendrá por no presentada.
    En caso de que la solicitud de ejercicio de derechos ARCO <u>sí</u> cumpla con los requisitos establecidos, su médico(a) tendrá un plazo máximo de veinte días naturales para comunicarle la determinación adoptada respecto de la solicitud.  
     
             En caso de que la solicitud de ejercicio de derechos ARCO sea procedente, su médico(a) <b>(A)</b> hará efectiva la determinación dentro de un plazo máximo de quince días naturales contados a partir del día en que se notifique al titular la determinación descrita en el párrafo anterior, y <b>(B)</b> dará aviso al titular de que se ha dado cumplimiento su solicitud.
        <br>
        
        <u>¿Cómo conocer los cambios del presente aviso de privacidad?</u>
        <br>
        El presente aviso de privacidad puede sufrir modificaciones, cambios o actualizaciones, por lo que nos comprometemos a mantenerlo informado a través de algunos de los siguientes medios:
        <ul>
                <li>Nuestra página de Internet. [dirección de la página de Internet];</li>
                <li>Notificación a su dirección de correo electrónico; y/o</li>
                <li>Notificación personal, en caso de que tengamos comunicación con usted después del cambio.</li>
                
        </ul>
        <br>
        <u>Conservación de Datos Personales</u><br>
        Su médico(a) conservará la información del titular por un término indefinido, tanto para los fines primarios y secundarios, como para dar cumplimiento a las disposiciones legales correspondientes.
        En el supuesto de que el titular de los derechos desee cambiar de médico deberá ejercer sus derechos ARCO para efecto de que su Médico(a) le proporcione su expediente electrónico.
        En el supuesto de que el Médico(a), por cualquier causa, dejare de prestar sus servicios profesionales, le reintegrará al titular su expediente electrónico enviándolo a la última dirección electrónica que el titular haya proporcionado.<br>
    
        <b>¿Cómo Contactarnos?</b><br>
        Si usted tiene alguna duda sobre el presente aviso de privacidad puede hacerla llegar a nuestra dirección de correo electrónico, dirigida a María Natalia Castelán Olmos, quien es responsable de la privacidad de sus datos personales. Usted puede consultar el presente aviso de privacidad, sus modificaciones y actualizaciones en nuestro domicilio mencionado en el presente documento.

        
               
    </li><!-- FIN DE LI 5 -->
</ol>
</div>

<div class="hiddeable arial_text" style="text-align:right;margin-right:40px;"></div>
<div class="text-center hiddeable arial_text" align="center">
	<div style="margin-left:40px;" class="text-center">
         <a href="<?php echo base_url(); ?>Pacientes/firma_doc/<?php echo $paciente->idpaciente ?>">Siguente</a>
	</div>	
</div><br>
	<div class="hidden-xs hidden-sm text-center hiddeable">
	</div>
</article>
<script type="text/javascript">
    function firma_doc(id){
        //window.location = <?php echo base_url(); ?>"Pacientes/firma_doc/"+id;
    }
</script>
