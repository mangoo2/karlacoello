<html><head>
	<title>Karla Coello</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0">
	<meta name="author" content="Carlos Corona">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>images/FAV.png">
    <link href="<?php echo base_url();?>aviso/bootstrap.min.css?v=34-2-5-16" rel="stylesheet" type="text/css" lazyload="">
    <link href="<?php echo base_url();?>assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
 </head>
 <style type="text/css">
 	@media print {
		html {
			margin: 40px;
		}
	}
 </style>
	<body>
		<div id="" style="text-align:center">
			<h4>Consentimiento informado para tratamiento con Luz Pulsada Intensa </h4>
		</div>
		<div id="date" style="text-align:right;position:fixed;right:1cm;top:0.5cm;">
		    <?php echo date('d/m/Y');?>	
	    </div>
		<div style="text-align: justify; list-style-type: upper-latin; font-size: 17px">
            Solicito y autorizo a Clínica Dra. Karla Coello y personal calificado y debidamente entrenado, a que
			realice el procedimiento de Luz pulsada IPL (Luz Pulsada Intensa), eLight (Luz Pulsada Intensa
			combinada con Radiofrecuencia), o Láser en las siguientes situaciones: 
			<br><br>
			MANCHAS ,CICATRICES DE ACNÉ , FOTOREJUVENECIMIENTO
			<br><br>
			Descripción del Tratamiento, Riesgos y Contraindicaciones
			<br>
			Confirmo que se me ha explicado detalladamente, el efecto y la naturaleza del procedimiento a
			realizar La periodicidad de las sesiones será en promedio de una al mes, siendo al inicio cada 3
			semanas y durante el tratamiento se irán espaciando. En algún caso excepcional (valorado por el
			técnico en fotodepilación), se podrán efectuar más sesiones al mes.
            <b> El cumplimiento de esta periodicidad es indispensable para la obtención de los resultados deseados. </b>
            El número total de sesiones es variable, con una media de 5 A 10, existiendo casos en que con menos se obtendrán
            los resultados deseados, y en otros, hará falta alguna más. 
            <br><br>
            La acción de esta energía lumínica es selectiva sobre la melanina, teniendo mayor efecto sobre el
			pelo negro por ser el que más cantidad posee, siendo menos efectivo sobre los de color más claro,
			con lo cual el número de sesiones de este tipo de pelo podría ser mayor. Ejerce poca o nula acción
			sobre los pelos amelánicos (canas y/o vello rubio). De forma excepcional puede presentarse
			pérdida de la pigmentación del pelo. 
            <br><br>
            El resultado del tratamiento va a depender de cada persona, remitiendo en condiciones normales
			sobre un 70 al 80% en manchas. En algunas personas puede ser necesario la repetición de
			sesiones aisladas para conseguir el mantenimiento del resultado óptimo. Hay que evitar la
			exposición al sol y rayos UVA antes y después de realizar la sesión, con el fin de evitar que la
			mayor concentración de pigmento (melanina) sea posible causa de quemaduras y/o manchas
			cutáneas. Al aplicar la energía lumínica se puede percibir calor en la piel, sensación de quemazón
			en las áreas de mayor densidad pilosa, eritemas (enrojecimiento) y/o petequias (puntos rojos) que
			remiten en 24 a 48 horas, considerándose respuestas esperables del tratamiento. 
            <br>
            Riesgos y Contraindicaciones 
            <br>
            Los pacientes de fototipos IV y V (razas africanas, indias e hispanoamericanos muy oscuros) pueden ser más propensos a sufrir quemaduras, se recomienda discreción.
            <b> Algunos suplementos herbales con Hierba de San Juan (Hypericum perforatum) .</b>
            <br><br>
            Hay fármacos que pueden inducir al crecimiento del pelo y cuyo uso está contraindicado, como son
            <b>acetazolamide, PUVA, cycloporine, danazol, interferon, penicillimide, minoxidil, phenytoin, oxadiazolopyrimide, diazoxide, streptomycen, fenoterol, hexachlorobenzene o los esteroides que se administran por vía tópica. </b>
            <br><br>
            <b>Algunos fármacos como la Isotretinoína (Roacutan, Isotrex, Oratane, Neotrex, etc.) pueden ocasionar cicatrices en la piel del usuario. </b>
            <br><br>
            Los siguientes medicamentos pueden incrementar la fotosensibilidad del paciente y provocar problemas en el tratamiento, como: 
            <br><br>
            TETRACICLINAS: Oxitetraciclina (Terramicina), Clortetraciclina (Aureomicina), Demeclociclina y Doxiciclina (Vibracina), Minociclina (Minocin) 
            <br>
            SULFONAMIDAS: Tiazidas, Sulfonilureas, Ciclamatos o FENOTIAZINA y derivados: Carbamacepina
			(Tegretol), Clorpromacina, Prometacina (Fenergan), Otros o QUINOLONAS: Ciprofloxacino (Baycip,
			Velmonit, Rigoran, Otros), Enoxacino (Almtil), Fleroxacino , Pefloxacino (Azuben), Ácido Nalidixico,
			Otras o ANTI-INFLAMATORIOS NO ESTEROIDEOS: Piroxicam (Feldene), Benoxaprofen, Oxaprozin,
			Tiaprofen, Carprofen, Otros o ANTIFÚNGICOS: Griseofulvina (Fulcin, Greosin) o
			HIPOGLICEMIANTES ORALES: (Tolbutamida)
			<br><br>
			Existen fármacos y agentes químicos que pueden causar reacciones fototóxicas, fotoalérgicas y
			fotodinamizantes, como medicamentos contra el acné, sulfamidas antibacterianas, fármacos
			anticancerosos, antidepresivos, antihistamínicos, antinflamatorios, antituberculosos,
			antimicrobiales, antimaláricos, calmantes, tratamiento de la insuficiencia cardiaca, dilatadores
			vasculares cerebrales, diuréticos, antidiabéticos, terapias hormonales, laxantes o psoralenos. 
			<br><br>
			Efectos Secundarios 
			<br>
			En la mayoría de los casos no se observan efectos secundarios indeseables. Puede presentarse un
			leve enrojecimiento y ocasionalmente en algún paciente producirse una quemadura superficial
			(grado I) parecida a una pequeña quemadura solar y que tratada adecuadamente con los
			productos aconsejados por el técnico en fotodepilación, remitirá. Usando dichos productos hasta
			la remisión de los síntomas. 
			<br>
			En caso de que, accidentalmente, se apliquen directamente en los ojos u en zonas de la piel que
			no son las correctas pueden causar: Cicatrices o Quemaduras o Dificultad para percibir los colores
			azul y verde
			<br>
			Consentimiento Informado para Fotodepilación 
			<br>
			<b>INDICACIONES. </b>
			<br>
			A continuación se describen las indicaciones otorgadas para antes, durante y después del tratamiento, que de manera enunciativa más no limitativa son:
			<br>
			Antes de cada sesión 
			<ul>
				Venir sin maquillaje, cremas ni desodorante. 
                <br>
                Evitar consumir alimentos muy condimentados o que puedan generar algún tipo de alergia en la piel. 
                <br>
                No haber tomado sol la semana previa y después de cada sesión 
                <br>
                Evitar fuentes de calor (Sauna, Vapor, Jacuzzis, Calderas, etc.) 
                <br>
                Evitar Albercas, Spas y entrar en contacto con agentes químicos como el cloro
                <br>
                 Evitar cualquier maniobra agresiva en las áreas tratadas: exfoliantes,astringentes, etc. 
			</ul>
			<br>
		    DECLARACIONES DEL CLIENTE.
		    <br>
		    En razón de ser un tratamiento exclusivamente estético, he sido informado de los riesgos
			trascendentes así como los efectos secundarios y se han contestado todas las preguntas que he
			hecho con respecto al tratamiento que voy a recibir. Así mismo, he sido enterado de toda la
			información médica pertinente que pudiera influir en el tratamiento, así como han sido
			contestadas satisfactoriamente todas las preguntas y dudas que libremente he formulado acerca
			de todo el procedimiento y asumo la responsabilidad total sobre posibles consecuencias resultado
			del tratamiento. 
			<br><br>
			No he omitido ni alterado ningún dato de mi estado actual de salud que contraindique el procedimiento, especialmente los referidos a alergias y enfermedades o riesgos personales. 
			<br><br>
			Entiendo que debo de informar al técnico de cualquier cambio en mi salud, a prescripción de
			medicamentos, y de mi exposición al sol reciente (incluida las lámparas UV) antes de cada nueva
			sesión, para que éste tome las medidas oportunas, en la inteligencia de que para el caso de
			consecuencias resultado de dicha omisión en mis declaraciones, será única y exclusivamente mi
			responsabilidad. 
			<br><br>
			Declaro que toda la información que he dado es cierta y que he de seguir al pie de la letra las
			indicaciones pre y post tratamiento y asumo la responsabilidad total sobre las consecuencias de
			omitir dichas indicaciones. 
            <br><br>
            Autorizo a que se me practiquen fotografías de la zona intervenida y a recabar la evidencia necesaria para documentar el avance en el tratamiento 
            <br><br>
            Aplique cremas tales como Polysporin®, Bactroban® o Fucidin® para mantener el área húmeda y ayudar a que sane. Aplique hasta que los efectos secundarios terminen. 
            <br><br>
             El maquillaje puede ser usado después de las primeras 24 horas en ausencia de costras o ampollas. Si el maquillaje es usado debe ser aplicado y retirado con delicadeza. La irritación puede incrementar las posibilidades de despellejado. 
            <br><br>
            En caso de alguna molestia, utilizar Aloe Vera (gel calmante para después de tomar sol) junto con Acetaminofen (Advyl o Tylenol).Durante todo el tratamiento: 
            <br><br>
            Usar protector solar de alta protección. Mantener la piel bien hidratada. Evitar usar
			productos con AHA’s (alfahidroxiácidos). En caso de encontrarse bajo algún tratamiento médico,
			deberá informarlo al técnico de fotodepilación oportunamente. En algunas mujeres suele
			suceder que días previos a su menstruación la sensibilidad aumente, en ese caso es recomendable
			reprogramar la cita y a que puedan ser utilizadas con fines científicos, docentes o médicos,
			quedando entendido que su uso no constituya ninguna violación a la intimidad o confidencialidad,
			a las que tengo derecho. 
            <br><br>
            Estoy de acuerdo que el presente tratamiento es personal e intransferible.
            <br><br>
            Me comprometo a liquidar en tiempo y forma el costo del tratamiento que he
			adquirido y estoy consciente de que no habrá devolución por ningún motivo. Los honorarios de
			cada tratamiento será abonados en cada sesión o bien abonando el tratamiento ofertado en un
			solo pago al iniciar dicho tratamiento. Estos pueden ser modificados por ESTETIK en función de la
			zona tratada y el tiempo invertido.
            <br><br>
            Estoy de acuerdo en que los paquetes (2 o más sesiones) tienen una fecha límite para ser
			tomados y en caso de que por razones imputables a mi persona no termine en dicha fecha, deberé
			cubrir el costo de las sesiones individuales que se requieran para terminar mi tratamiento. 
            <br><br>
            Me comprometo a avisar con 24 hrs de anticipación cualquier cambio en el horario de las citas
            programadas, de lo contrario estoy de acuerdo en acatar las disposiciones que ESTETIK designe.
            <br><br>
            Estoy consciente que dada la naturaleza y duración de los tratamientos cuento con 15 minutos de tolerancia. 
            <br><br>
             Se me ha informado, igualmente, de mi derecho a rechazar el tratamiento o revocar este consentimiento. 
            <br><br>
            He podido aclarar todas mis dudas acerca de todo lo anteriormente expuesto y he entendido
			totalmente este DOCUMENTO DE CONSENTIMIENTO reafirmándome en todas y cada uno de sus
			puntos y con la firma del documento ratifico y consiento que el tratamiento se realice. 
            <br>
            <div style="text-align: center">
			<input type="hidden" id="base_url" value="<?php echo base_url(); ?>" readonly>
            <input type="hidden" id="idpaciente" value="<?php echo $paciente->idpaciente ?>">
            <input type="hidden" id="tipo_aviso" value="<?php echo $tipo_aviso ?>">
            <?php if($iddocumento==0){ ?>
	            <?php if($tipo==0){ ?>
					<div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div>
	            <?php } else{?>
	            	<div class="firma_text" id="aceptance">
						<div id="signature" class="signature col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
							<label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma en el recuadro, su mouse o su dedo</label><br>
							<canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
							<img src="<?php echo base_url() ?>public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignature" data-signature="patientSignature">
							<br>
							<div class="row" >
								<button class="btn_estilo" id="btn_firma_save" onclick="saveex_paciente()">Aceptar Firma</button>
							</div>				
						</div>			
					</div>
	            <?php }?>
            <?php }else{?>
				    <div style="text-align:center;margin-bottom:10px;border-radius:4px;">
				        <?php 
				        $result_get=$this->General_model->get_record('iddocumento',$iddocumento,'documentos_legales');
				        $fh = fopen(base_url()."uploads/paciente_firma/".$result_get->firma, 'r') or die("Se produjo un error al abrir el archivo");
				        $linea = fgets($fh);
				        fclose($fh);  
				        ?>
				        <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
				            <img src="<?php echo $linea ?>" width="170" height="75" style="border:dotted 1px black;">
				        </div> 
				    </div>                 
             <?php }?>
				<div style="font-size: 15px">FIRMA DEL PACIENTE Y FECHA 
				</div>	
			    <div style="font-size: 16px">
			    <?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?>		
		        </div> 
            </div>

            <br>

            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:5cm;border-bottom:1px solid black;"></div><br>Nombre y firma del Dr	
        </div>	
    </body>
</html>