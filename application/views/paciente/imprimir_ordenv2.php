<div class="row">
    <div class="col-md-3">
        <img width="150" src="<?php  echo base_url(); ?>public/img/neurociencias.jpg">
    </div>
    <div class="col-md-3">
    	<img width="150" src="<?php  echo base_url(); ?>public/img/centroneuro/logoparkinson2.png">
    </div>
    <div class="col-md-6">
    	<h2 style="text-align: right; font-weight: bold;">DR. DATENTE OROPEZA CANTO</h2>
    	<h3 style="text-align: right; font-weight: bold;">Neurólogo Clínico</h3>
    	<h3 style="text-align: right">CED. PROF. 2018947 UPAEP CED. ESP. 3413958</h3>
    </div>
</div>
<div class="row">
	<div class="col-md-12">
		<h3 style="text-align: right; color: red;">N°. 0400</h3>
	</div>
</div>	
<br>
<div class="row">
	<div class="col-md-12">
		<h2>Nombre: <u><?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?></u></h2>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<h2>Fecha:  <u><?php echo date('d/m/Y',strtotime($paciente->fecha_nacimiento)) ?></u></h2>
	</div>
	<div class="col-md-8">
		<h6 style="text-align: right;">CMN 20 de Nov-ISSSTE-UNAM</h6>
		<h6 style="text-align: right;">Certificado en Medicina del Sueño (Folio 8)</h6>
		<h6 style="text-align: right;">Máster en Sueño , Universidad de Pablo de Olavide, Sevilla España</h6>
		<h6 style="text-align: right;">Máster en Transtornos del Movimiento, Universidad de Murcia, España</h6>
		<h6 style="text-align: right;">Miembro de la Academia Mexicana de Neurología</h6>
		<h6 style="text-align: right;">Miembro de la Academia Americana de Neurofisiología</h6>
		<h6 style="text-align: right;">Sociedad Mexicana de Neurofisiologia Clínica </h6>
		<h6 style="text-align: right;">Movement Disorder Society, American Academy of Neurology</h6>
		<h6 style="text-align: right;">Máster en Neuroinmunología Universidad Autónoma de Barcelona (UBA)</h6>
	</div>
</div>
<br>
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-11">
		<h2 style="font-weight: bold;">HOSPITAL ANGELES DE PUEBLA</h2>
		<h2 style="font-weight: bold;">DEPARTAMENTO DE IMAGENOLOGÍA</h2>
		<h2 style="font-weight: bold;">Tel: 222 303 66 00 Ext: 2023</h2>
		<br>
		<h3>Favor de realizar:</h3>
		<ul>
			<?php foreach ($list_estudio as $item) { ?>
			    <li><h3><?php echo $item->detalles ?></h3></li>
			<?php } ?>    
		</ul>
	</div>
</div>
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-11">
		<h3>DX:Distonía Cervical</h3>
	</div>
</div>
<br>
<br>
<br>
<br>
<br>
<div class="row">
	<div class="col-md-12">
		<h4 style="text-align: right; font-weight: bold;">DR. DANTE OROPEZA CANTO</h4>
		<h4 style="text-align: right; font-weight: bold;">NEUROLOGO CLINICO.</h4>
		<h4 style="text-align: right; font-weight: bold;">CED. PROFESIONAL. 3413958</h4>
	</div>
</div>
<br>
<br>
<br>
<br>
<br>
<div class="row">
	<div class="col-md-4"></div>
	<div class="col-md-4">
		<h4>Hospital Ángeles Av. Kepler 2143,</h4>
		<h4>Reserva Territorial Atlixcáyotl</h4>
		<h4>Torre de Especialidades Consultorio</h4>
		<h4>3720</h4>
	</div>
	<div class="col-md-4">
		<h4 style="text-align: right; font-weight: bold;">Cons. 2222 90 76 22</h4>
		<h4 style="text-align: right; font-weight: bold;">Cel. 2222 65 01 33</h4>
	</div>
</div>