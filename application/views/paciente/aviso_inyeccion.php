<html><head>
	<title>Karla Coello</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0">
	<meta name="author" content="Carlos Corona">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>images/FAV.png">
    <link href="<?php echo base_url();?>aviso/bootstrap.min.css?v=34-2-5-16" rel="stylesheet" type="text/css" lazyload="">
    <link href="<?php echo base_url();?>assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
 </head>
 <style type="text/css">
 	@media print {
		html {
			margin: 40px;
		}
	}
 </style>
	<body>
		<div id="" style="text-align:center">
			<h4>CONSENTIMIENTO A LA INYECCIÓN DE ÁCIDO HIALURÓNICO</h4>
		</div>
		<div id="date" style="text-align:right;position:fixed;right:1cm;top:0.5cm;">
		    <?php echo date('d/m/Y');?>	
	    </div>
		<div style="text-align: justify; list-style-type: upper-latin; font-size: 14px">
			<b>INDICACIONES</b>
            Es un gel estéril de ácido hialurónico estabilizado.  Las inyecciones de ácido hialurónico se administran para corregir las arrugas faciales y/o para incrementar el volumen de los labios.  El ácido hialurónico ha sido aprobado por la Administración de Alimentos y Drogas de los Estados Unidos (FDA por sus siglas en inglés) para corrección de arrugas fáciles en el área nasolabial (el área de la nariz y los labios) y en el pliegue entre las mejillas y la nariz/el labio superior (uso “incluido en la etiqueta”).  Entiendo que la seguridad y la efectividad de tratar las áreas faciales distintas de los pliegues nasolabiales no se ha estudiado aún; sin embargo, el ácido hialurónico  ha sido utilizado para mejorar la apariencia de los labios en más de 60 países, además de los Estados Unidos.  Este aspecto del tratamiento “no incluido en la etiqueta” se me ha explicado bien. <b>RESULTADOS</b>  Entiendo que no puede predecirse ni garantizarse el grado real de mejoría.  Además, entiendo que el efecto de las inyecciones irá desapareciendo gradualmente y pueden ser necesarios tratamientos adicionales para mantener el efecto deseado.  
			<br>
			<b>EFECTOS SECUNDARIOS Y COMPLICACIONES: </b>
            Éstos incluyen, sin limitación:  Reacciones alérgicas potenciales.  Al igual que con cualquier producto, pueden desarrollarse alergias durante o después de la inyección.   Reacciones en el sitio de la inyección: sensación de protuberancia o “engrosamiento” bajo la piel, hematoma, enrojecimiento, prurito, dolor, sensibilidad o inflamación leve.  Existe riesgo en cualquier inyección en una arteria provocando una reacción como la oclusion vascular hasta llegar a necrosis. Las inyecciones en el área de los labios pueden desencadenar una recurrencia de infecciones herpéticas de ya existir la enfermedad en el paciente.
			<br><br>
			<b>PRECAUCIONES Y CONTRAINDICACIONES</b>
			Debido al potencial de reacción alérgica, no se recomienda el uso de ácido hialurónico en pacientes con historia de alergias severas o anafilaxia. El riesgo de hematoma o sangrado puede aumentar por el uso de medicamentos como efectos de los anticoagulantes, como la aspirina, los antiinflamatorios no esteroideos, (por ejemplo, Ibuprofeno, naproxen sódico, celecoxib), altas dosis de vitamina E y ciertas preparaciones herbales (Ginkgo Biloba, Verruga de San Juan). La seguridad de ácido hialurónico en mujeres embarazadas o en período de lactancia no se ha establecido y por lo tanto no se recomienda su uso en estas mujeres en gestación o lactando.
			<br><br>
            <b>CONSENTIMIENTO</b>
			Entiendo que requiero anestesia local para reducir las molestias del procedimiento y acepto la aplicación tópica de gel y/o inyecciones de anestesia para bloqueo de nervios o infiltración de anestesia local.  Entiendo la información ya arriba presentada y se me han explicado los riesgos, beneficios y alternativas y he tenido la oportunidad de hacer preguntas.  No se me han dado garantías en cuanto a los resultados.  Hasta donde sé, no estoy embarazada y no estoy en período de lactancia.  Acepto recibir inyecciones de ácido hialurónico ahora y en el futuro, según sea necesario. 
			<div style="text-align: center">
			<input type="hidden" id="base_url" value="<?php echo base_url(); ?>" readonly>
            <input type="hidden" id="idpaciente" value="<?php echo $paciente->idpaciente ?>">
            <input type="hidden" id="tipo_aviso" value="<?php echo $tipo_aviso ?>">
            <?php if($iddocumento==0){ ?>
	            <?php if($tipo==0){ ?>
					<div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div>
	            <?php } else{?>
	            	<div class="firma_text" id="aceptance">
						<div id="signature" class="signature col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
							<label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma en el recuadro, su mouse o su dedo</label><br>
							<canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
							<img src="<?php echo base_url() ?>public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignature" data-signature="patientSignature">
							<br>
							<div class="row" >
								<button class="btn_estilo" id="btn_firma_save" onclick="saveex_paciente()">Aceptar Firma</button>
							</div>				
						</div>			
					</div>
	            <?php }?>
            <?php }else{?>
				    <div style="text-align:center;margin-bottom:10px;border-radius:4px;">
				        <?php 
				        $result_get=$this->General_model->get_record('iddocumento',$iddocumento,'documentos_legales');
				        $fh = fopen(base_url()."uploads/paciente_firma/".$result_get->firma, 'r') or die("Se produjo un error al abrir el archivo");
				        $linea = fgets($fh);
				        fclose($fh);  
				        ?>
				        <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
				            <img src="<?php echo $linea ?>" width="170" height="75" style="border:dotted 1px black;">
				        </div> 
				    </div>                 
             <?php }?>
				<div style="font-size: 15px">FIRMA DEL PACIENTE Y FECHA 
				</div>	
			    <div style="font-size: 16px">
			    <?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?>		
		        </div> 
            </div>
            CANTIDAD A USAR EN <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:5cm;border-bottom:1px solid black;"></div> APLICACIÓN <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:2cm;border-bottom:1px solid black;"></div> ml  <br>
                Precio $<div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:5cm;border-bottom:1px solid black;"></div>
            MARCA <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:5cm;border-bottom:1px solid black;"></div><br>DRA KARLA COELLO VÁZQUEZ  8435100 	
        </div>	
    </body>
</html>