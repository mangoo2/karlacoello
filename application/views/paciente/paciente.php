<?php if(isset($_GET['consulta'])){ ?>
<input type="hidden" id="tipo_consulta" value="<?php echo $_GET['consulta'] ?>"> 
<?php }else{ ?>
<input type="hidden" id="tipo_consulta" value="0"> 
<?php } ?> 
<style type="text/css">
    .customtab2 li a.nav-link.active {
        background: #03a9f3;
        color: #fff;
    }
    .customtab2 li a.nav-link:hover {
        color: #fff;
        background: #03a9f3;
    }
    .centrado{text-align:center;border-style: solid; border-width: 1px; padding:8px; border-radius: 10px;}
    .recuadro{border-style: solid; border-width: 1px; padding:8px; border-radius: 10px;}
    .colorlabel{ background-color: #EFF2F4; border-bottom: 1px solid #5c77b3!important;}
    .colorlabel_white{ background-color: white; border-bottom: 1px solid #5c77b3!important; border: 1px solid #ffffff; min-height: 35px !important;}
    .bordel_arriba{ border-top-left-radius: 10px !important; border-top-right-radius: 10px !important; }
    .textleft{text-align: left !important;}
    .color_input{ background-color: white ; border: 1px solid #ffffff !important; border-bottom: 1px solid #5c77b3!important; background-color: #ffffff !important;}
    .margentexto{margin-bottom: -15px;}
    .btn_style_c{line-height: 0.5 !important;display: flex;align-items: center;}
    .icon_to{font-size: 25px !important;}
    .margen_div{margin-bottom: 0.5em;}
    .btn_letra_b{font-size: 11px; color: #1784F6;}
    .margen_left{border-left: 5px groove #d0d0d0;}
    .margen_todo{border-style: solid; border-width: 1px; border-radius: 12px; border-color: #e5e6e5;}
    .letranegrita{font-weight: bold;}
    .div_abajo_solid{border-bottom: solid;}
    .div_etiqueta{background-color: #e0e0e0; border-radius: 10px; }
    .letra_arial{ font-family: sans-serif; }
    .margen_todo_gris{border-style: solid; border-width: 1px;border-color: #e5e6e5; background-color: f8f9fa;}
    .card_borde{padding: 0.3rem 1.25rem; background-color: rgba(0,0,0,.03);}
    .bg-secon{background-color: #e4e4e4!important;}
    .btn-h:hover {background: #8c8c8c; color:white; border-radius: 10px}
    .div_abajo_solid_grosor{border-bottom: 2px solid #7c7c7c;}
    .margen_link{background-color: #efefef; border-radius: 10px; line-height: 35px;}

    textarea{
        overflow: hidden;
        min-height: 30px;
    }
    #selectorder{
        background-color: #346c94 !important;
    }
    .badge-info{
        background-color: #346c94 !important;
    }
    .form-control2{
        font-size: .875rem;
        line-height: 1.5;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #e9ecef;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .contenedor{
      display: inline-block;
      min-width: 100px;
    }
    /* Tabla de nutricion */
    .tabla_n{ 
        border: solid 1px #000000;
    }
    .tr_n{ 
        border: solid 1px #000000;
        color: black;
    }
    .th_n{ 
        border: solid 1px #000000;
        background-color: #b6cdea;
        color: black;
    }
    .td_n{ 
        border: solid 1px #000000;
        color: black;
    }
    .color_input_tabla{
        background-color: #ffeb3b;
        min-height: 35px !important;
        color: red;
    }
    /* Tamañno de tabla 2 */
    .tm_td{
        width: 45px;
    }
    .td_n1{
        border: solid 1px #000000;
        background-color: #dbdbff;
        color: black;
    }
    .th_n2{ 
        border: solid 1px #000000;
        background-color: #f5d5ef;
        color: black;
    }
    .th_n3{ 
        border: solid 1px #000000;
        background-color: #ffdaa3;
        color: black;
    }
    .th_n4{ 
        border: solid 1px #000000;
        background-color: #d3efc5;
        color: black;
    }
    .th_n5{
        border: solid 1px #000000;
        background-color: #aab8e0;
        color: black;
    }
    .th_n6{
        border: solid 1px #000000;
        background-color: #d3eadb;
        color: black;
    }
    .th_n5_6{
        border: solid 1px #000000;
        background-color: #fff2c9;
        color: black;
    }
    .color_input_tabla2 {
        background-color: #ffffff;
        min-height: 35px !important;
        color: black;
    }
    .oculto_text3{
        color: #ffdaa3;
    }
    .oculto_text4{
        color: #d3efc5;
    }
    .oculto_text5{
        color: #aab8e0;
    }
    .oculto_text6{
        color: #fff2c9;
    }
    .inputplan{
        border:0px;
    }
    .removetr{
        position: absolute;
        z-index: 10;
    }
    .title_plan{
        background: #8bc34a9c;
        font-weight: bold;
    }
    .title_plan input{
        background: transparent;
        font-size: 13.5px;
        font-weight: bold;
        max-width: 100px;
    }
</style>
<style type="text/css">
    .arial_text{
        font-family: Arial;
    }
    pre {
        overflow: auto;
        border: none;
        background: transparent;
        font-family: "Helvetica Neue", Helvetica, "Segoe UI", Tahoma, Arial, sans-serif;
        font-size: 14px;
        padding: 0px 5px;
        word-break: break-word;
        margin-bottom: 20px;
    }
    pre {
        white-space: pre-wrap;
    }
</style>
<style type="text/css">
  .iframeprintc1{
    width: 100%;
    height: 60vh;
    border: 0;
  }
  .iframeticketc1{
    padding: 0px;
  }
</style>



<input type="hidden" id="permiso_perfil1" value="<?php echo $permiso_perfil1 ?>">
<input type="hidden" id="permiso_perfil2" value="<?php echo $permiso_perfil2 ?>">
<input type="hidden" id="permiso_perfil3" value="<?php echo $permiso_perfil3 ?>">

<input type="hidden" id="administrador" value="<?php echo $administrador ?>">
<input type="hidden" id="idpte" value="<?php echo $idpaciente ?>">

<input type="hidden" id="idconsulta_existe" value="<?php echo $idconsulta_existe ?>">
<input type="hidden" id="nombre_paciente_text" value="<?php echo $nombre.' '.$apll_paterno.' '.$apll_materno ?>">
<input type="hidden" id="edad_text" value="<?php echo $edad ?>">
<input type="hidden" id="sexo_text" value="<?php echo $sexo_p ?>">
<input type="hidden" id="celular" value="<?php echo $celular ?>">
<!--<input type="hidden" id="idconsulta_ultima" value=""> -->
<div class="page-wrapper">
    <div class="container-fluid">
        <!--- -->
        <div style="display: block;">
            <input id="text_qr" type="hidden" value="<?php echo base_url() ?>Pacientes/firma_aviso/<?php echo $idpaciente ?>" style="width:80%">
        </div> 
        
        <!-- -->    
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12" align="right">
                                        <button type="button" class="btn waves-effect waves-light btn-secondary" onclick="detalle_paciente(<?php echo $idpaciente ?>)">Detalles del Paciente <i class="fas fa-sort-down"></i></button>
                                        <!--
                                        <button type="button" class="btn waves-effect waves-light btn-secondary"><i class="fas fa-ellipsis-v"></i></button>-->
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-outline-secondary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="fas fa-ellipsis-v"></i>
                                            </button>
                                            <div class="dropdown-menu" x-placement="bottom-start" style="will-change: transform; position: absolute; transform: translate3d(0px, 35px, 0px); top: 0px; left: 0px;">
                                              <a class="dropdown-item" onclick="imprimir_expediente(<?php echo $idpaciente ?>)">Imprimir expediente</a>
                                              <a class="dropdown-item" onclick="eliminar_paciente(<?php echo $idpaciente ?>)">Eliminar Paciente</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <div class="detalles_paciente">

                                </div>  
                                <div class="detalles_paciente_primera">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <?php if($foto!=''){ ?>
                                                <img style="width: 145px; height: 145px; border-radius: 70px;" src="<?php echo base_url(); ?>uploads/pacientes/<?php echo $foto ?>" />
                                            <?php }else{ ?>    
                                                <img style="width: 145px;" src="<?php echo base_url(); ?>images/annon.png">
                                            <?php } ?>
                                        </div>    
                                        <div class="col-md-3">
                                            <h4 class="nombre_paciente"><?php echo $nombre.' '.$apll_paterno.' '.$apll_materno ?></h4>
                                            <?php if($fechan!='0000-00-00'){ ?>
                                                <h5 class="edad_paciente">Edad: <?php echo $edad ?> años</h5>
                                            <?php } ?>
                                            <?php if($correo!=''){ ?>
                                                <h5>Correo: <?php echo $correo ?></h5>
                                            <?php } ?>
                                            <?php if($ocupacion!=''){ ?>
                                                <h5>Ucupación: <?php echo $ocupacion ?></h5>
                                            <?php } ?>
                                            <?php if($celular!=''){ ?>
                                                <h5><i class="fas fa-mobile-alt"></i> <?php echo $celular ?></h5>
                                            <?php } ?>
                                            <?php if($nota!=''){ ?>
                                                <h5><i class="fas fa-thumbtack"></i> <?php echo $nota ?></h5>
                                            <?php } ?>
                                        </div>
                                        <div class="col-md-7" align="right"><br> 
                                            <!--
                                            <button type="button" class="btn waves-effect waves-light btn-facebook">Cobro online</button>
                                            -->
                                            <button type="button" class="btn waves-effect waves-light btn-info" style="background-color: #4760DC!important;" onclick="modal_video_consulta()"><i class="fas fa-video"></i> Video Consulta</button>
                                            <button type="button" class="btn waves-effect waves-light btn-info" onclick="seleccionar_consultas()"><i class="fas fa-stethoscope"></i> Nueva Consulta</button>
                                            <!--
                                            <button type="button" class="btn waves-effect waves-light btn-secondary"><i class="fas fa-briefcase-medical"></i> <i class="fas fa-sort-down"></i></button>&nbsp;&nbsp;&nbsp;&nbsp;-->
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <!--- --->
                                        <ul class="nav nav-tabs customtab2" role="tablist">
                                            <li class="nav-item"> <a class="nav-link active hitoria_cli" data-toggle="tab" href="#relevante1" role="tab"><span class="hidden-xs-down"><i class="fas fa-clipboard"></i>  Inf. Relevante / Historia Clínica</span></a> </li>
                                            <li class="nav-item"> <a class="nav-link consultas_li" data-toggle="tab" href="#consultas2" role="tab"> <span class="hidden-xs-down"><i class="fas fa-file-medical"></i>  Consultas</span></a> </li>
                                            <!--
                                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#archivos1" role="tab" onclick="archivos();"> <span class="hidden-xs-down"><i class="fas fa-file-upload"></i>  Archivos</span></a> </li>
                                            -->
                                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#grafica" role="tab" onclick="grafica()"> <span class="hidden-xs-down"><i class="fas fa-file-medical"></i>  Grafica de evolución</span></a> </li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="relevante1" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <form class="form" method="post" role="form" id="form_relevante">
                                                            <input type="hidden" name="idrelevante" id="idrelevante" class="form-control colorlabel" value="<?php echo $idrelevante  ?>">
                                                            <input type="hidden" name="idpaciente" class="form-control colorlabel" value="<?php echo $idpaciente  ?>">
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-md-12"><label><i class="fas fa-exclamation-circle"></i> Antecedentes Importantes</label>
                                                                    <div class="form-group input-group mb-3 recordableHolder">
                                                                        <textarea type="text" name="antecedentes" class="form-control colorlabel recordable rinited js-auto-size" id="text" onchange="inf_relevante()"><?php echo $antecedentes  ?></textarea><a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label><i class="fab fa-empire"></i> Alergias </label>
                                                                        </div>
                                                                        <div class="col-md-3"><label>| Negadas</label></div>
                                                                        <div class="col-md-5">                                                   
                                                                            <div class="custom-checkbox">
                                                                                <input type="checkbox" class="custom-control-input" name="negadas" id="negadas" onclick="click_negadas()" <?php echo $negadas ?>>
                                                                                <label class="custom-control-label" for="negadas"></label>
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="form-group input-group mb-3 recordableHolder">
                                                                        <textarea type="text" name="alergias" id="alergias" class="form-control colorlabel recordable rinited js-auto-size" onchange="inf_relevante()" ><?php echo $alergias  ?></textarea>
                                                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12"><label><i class="fas fa-comment"></i>
                                                                    Quién refiere al paciente</label>
                                                                    <div class="form-group input-group mb-3 recordableHolder">
                                                                        <input type="text" name="quien_refiere" class="form-control colorlabel recordable rinited" onchange="inf_relevante()" value="<?php echo $quien_refiere  ?>">
                                                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label><i class="fas fa-comment-alt"></i> Motivo de la Consulta (Según el paciente)</label>
                                                                    <div class="form-group input-group mb-3 recordableHolder">
                                                                        <textarea type="text" name="motivo_consulta" class="form-control colorlabel recordable rinited js-auto-size" onchange="inf_relevante()" ><?php echo $motivo_consulta  ?></textarea>
                                                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label><i class="mdi mdi-message-reply"></i> Observaciones sobre el paciente</label>
                                                                    <div class="form-group input-group mb-3 recordableHolder">
                                                                        <textarea type="text" name="observaciones" class="form-control colorlabel recordable rinited js-auto-size" onchange="inf_relevante()"><?php echo $observaciones  ?></textarea>
                                                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="col-md-6"><br>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="card border-info">
                                                                    <div class="card-header bg-info bordel_arriba">
                                                                        <div class="row">
                                                                            <div class="col-md-8">
                                                                                <h4 class="m-b-0 text-white">Historia Clínica </h4>
                                                                            </div>
                                                                            <div class="col-md-4" align="right">
                                                                                <div class="editarhistoriacli_btn">
                                                                                    <button  type="button" class="btn waves-effect waves-light btn-sm btn-secondary" onclick="editarhistoriacli()">Editar</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="button-box">
                                                                        <div class="historia_clinica_visual">
                                                                            <?php     
                                                                            $arrayinfo = array('idpaciente'=>$idpaciente);
                                                                            $get_historia_clinica=$this->General_model->getselectwhereall('historia_clinica',$arrayinfo);
                                                                            foreach ($get_historia_clinica as $item){ ?>
                                                                                <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_his1()"><img src="<?php echo base_url() ?>images/relevacion/ico-paciente.png"> Heredofamiliares (Abuelos, padres, hijos)</button>
                                                                                <div class="btn_his1" style="display: none;">
                                                                                    <ul>
                                                                                        <h6><i class="fas fa-angle-right"></i> Diabetes mellitus
                                                                                            <?php if($item->diabetes!=''){ ?>
                                                                                                <ul>
                                                                                                    <input type="text" class="form-control color_input" value="<?php echo $item->diabetes ?>" readonly>
                                                                                                </ul>
                                                                                            <?php } ?>
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> Cáncer
                                                                                            <?php if($item->cancer!=''){ ?>
                                                                                                <ul>
                                                                                                    <input type="text" class="form-control color_input" value="<?php echo $item->cancer ?>" readonly>
                                                                                                </ul> 
                                                                                            <?php } ?>
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> Hipertensión arterial
                                                                                            <?php if($item->hipertencion!=''){ ?>
                                                                                                <ul>
                                                                                                    <input type="text" class="form-control color_input" value="<?php echo $item->hipertencion ?>" readonly>
                                                                                                </ul> 
                                                                                            <?php } ?>
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> Otro antecedente importante
                                                                                            <?php if($item->hipertencion!=''){ ?>
                                                                                                <ul>
                                                                                                    <input type="text" class="form-control color_input" value="<?php echo $item->otro_antecedente ?>" readonly>
                                                                                                </ul> 
                                                                                            <?php } ?>
                                                                                        </h6> 
                                                                                    </ul>
                                                                                </div>
                                                                                <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_his3()"><img src="<?php echo base_url() ?>images/relevacion/ico-nopatologico.png"> A. Personales NO Patológicos</button>
                                                                                <div class="btn_his3" style="display: none;">
                                                                                    <ul>
                                                                                        <h6><i class="fas fa-angle-right"></i> Personal
                                                                                            <ul>
                                                                                                <span><i class="fas fa-angle-right"></i> ¿Cuenta con todos los servicios?</span>
                                                                                                <?php 
                                                                                                $etiqueta_ser=''; 
                                                                                                if($item->chetpersonal_servicios==1){
                                                                                                    $etiqueta_ser='Si'; 
                                                                                                }else{
                                                                                                    $etiqueta_ser='No'; 
                                                                                                } 
                                                                                                echo $etiqueta_ser;
                                                                                                ?>
                                                                                            </ul> 
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> Alimentación
                                                                                        <ul>
                                                                                            <span><i class="fas fa-angle-right"></i> Frutas y vegetales</span> 
                                                                                            <?php if($item->fruta!=''){ ?>
                                                                                            <input type="text" class="form-control color_input" value="<?php echo $item->fruta ?>" readonly>
                                                                                            <?php } ?> 

                                                                                            <span><i class="fas fa-angle-right"></i> Carnes rojas</span> 
                                                                                            <?php if($item->carnes_rojas!=''){ ?>
                                                                                            <input type="text" class="form-control color_input" value="<?php echo $item->carnes_rojas ?>" readonly>
                                                                                            <?php } ?> 

                                                                                            <span><i class="fas fa-angle-right"></i> Cerdo</span> 
                                                                                            <?php if($item->cerdos!=''){ ?>
                                                                                            <input type="text" class="form-control color_input" value="<?php echo $item->cerdos ?>" readonly>
                                                                                            <?php } ?> 

                                                                                            <span><i class="fas fa-angle-right"></i> Pollo / Pescado</span> 
                                                                                            <?php if($item->pollo!=''){ ?>
                                                                                            <input type="text" class="form-control color_input" value="<?php echo $item->pollo ?>" readonly>
                                                                                            <?php } ?> 

                                                                                            <span><i class="fas fa-angle-right"></i> Lacteos</span> 
                                                                                            <?php if($item->lacteos!=''){ ?>
                                                                                            <input type="text" class="form-control color_input" value="<?php echo $item->lacteos ?>" readonly>
                                                                                            <?php } ?> 

                                                                                            <span><i class="fas fa-angle-right"></i> Harinas refinadas</span> 
                                                                                            <?php if($item->harina!=''){ ?>
                                                                                            <input type="text" class="form-control color_input" value="<?php echo $item->harina ?>" readonly>
                                                                                            <?php } ?> 

                                                                                            <span><i class="fas fa-angle-right"></i> Agua</span> 
                                                                                            <?php if($item->aguas!=''){ ?>
                                                                                            <input type="text" class="form-control color_input" value="<?php echo $item->aguas ?>" readonly>
                                                                                            <?php } ?> 

                                                                                            <span><i class="fas fa-angle-right"></i> Chatarra / Gaseosa</span> 
                                                                                            <?php if($item->chatarras!=''){ ?>
                                                                                            <input type="text" class="form-control color_input" value="<?php echo $item->chatarras ?>" readonly>
                                                                                            <?php } ?> 
                                                                                        </ul> 
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> Alcohol
                                                                                            <ul>
                                                                                                <?php if($item->cuando_inicio_alcohol!=''){ ?>
                                                                                                <span>¿Cuándo inició? <?php echo $item->cuando_inicio_alcohol ?> </span><br>
                                                                                                <?php } 
                                                                                                if($item->cada_cuando_alcohol!=''){ ?>
                                                                                                <span>¿Cada cuánto? <?php echo $item->cada_cuando_alcohol ?> </span><br>
                                                                                                <?php }
                                                                                                if($item->cuantos_dia_alcohol!=''){ ?>
                                                                                                <span>¿Cuántos al día? <?php echo $item->cuantos_dia_alcohol ?> </span><br>
                                                                                                <?php } ?>

                                                                                            </ul> 
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> Drogas
                                                                                            <ul>
                                                                                                <?php if($item->cuando_inicio_drogas!=''){ ?>
                                                                                                <span>¿Cuándo inició? <?php echo $item->cuando_inicio_drogas ?> </span><br>
                                                                                                <?php } 
                                                                                                if($item->cada_cuando_drogas!=''){ ?>
                                                                                                <span>¿Cada cuánto? <?php echo $item->cada_cuando_drogas ?> </span><br>
                                                                                                <?php }
                                                                                                if($item->cuantos_dia_drogas!=''){ ?>
                                                                                                <span>¿Cuántos al día? <?php echo $item->cuantos_dia_drogas ?> </span><br>
                                                                                                <?php } ?>

                                                                                            </ul> 
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> Tabaco
                                                                                            <ul>
                                                                                                <?php if($item->cuando_inicio_tabaco!=''){ ?>
                                                                                                <span>¿Cuándo inició? <?php echo $item->cuando_inicio_tabaco ?> </span><br>
                                                                                                <?php } 
                                                                                                if($item->cada_cuando_tabaco!=''){ ?>
                                                                                                <span>¿Cada cuánto? <?php echo $item->cada_cuando_tabaco ?> </span><br>
                                                                                                <?php }
                                                                                                if($item->cuantos_dia_tabaco!=''){ ?>
                                                                                                <span>¿Cuántos al día? <?php echo $item->cuantos_dia_tabaco ?> </span><br>
                                                                                                <?php } ?>

                                                                                            </ul> 
                                                                                        </h6>
                                                                                        <h6>
                                                                                            <span>Inmunizaciones actual y previa</span>
                                                                                            <?php if($item->inmunizaciones!=''){ ?>
                                                                                            <input type="text" class="form-control color_input" value="<?php echo $item->diabetes ?>" readonly>
                                                                                            <?php } ?>
                                                                                            <span>Última desparasitación</span>
                                                                                            <?php if($item->desparasitacion!=''){ ?>
                                                                                            <input type="text" class="form-control color_input" value="<?php echo $item->desparasitacion ?>" readonly>
                                                                                            <?php } ?>
                                                                                        </h6>
                                                                                    </ul>
                                                                                </div>
                                                                                <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_his5()"><img width="28px" src="<?php echo base_url() ?>images/gineicon.png"> Ginecobstetricos</button>
                                                                                <div class="btn_his5" style="display: none;">
                                                                                    <ul>
                                                                                        <h6>
                                                                                            Menarca <u><?php echo $item->marcar ?></u> Ciclo menstrual <u><?php echo $item->mestrual ?></u> 
                                                                                            Dismenorrea <u><?php echo $item->dismenorrea ?></u> 
                                                                                            Última menstruación <u><?php echo $item->menstruacion ?></u>
                                                                                            Inicio de vida sexual activa <u><?php echo $item->sexual ?></u> 
                                                                                            No. parejas <u><?php echo $item->parejas ?></u> 
                                                                                            Embarazos:<u><?php echo $item->embarazos ?></u> 
                                                                                            Abortos <u><?php echo $item->abortos ?></u> Cesáreas <u><?php echo $item->cesareas ?></u> Lactancia ( ) <u><?php echo $item->lactancia ?></u> 
                                                                                            Métodos anticonceptivos <u><?php echo $item->anticonceptivos ?></u> 
                                                                                            ETS <u><?php echo $item->ets ?></u> Menopausia <u><?php echo $item->menopausia ?></u> 
                                                                                            Climaterio <u><?php echo $item->climaterio ?></u> 
                                                                                            Papanicolau <u><?php echo $item->papanicolau ?></u>
                                                                                            Mastografía <u><?php echo $item->mastografia ?></u>
                                                                                        </h6>
                                                                                    </ul>    
                                                                                </div>
                                                                                <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_his2()"><img src="<?php echo base_url() ?>images/relevacion/ico-patologico.png"> A. Personales Patológicos</button>
                                                                                <div class="btn_his2" style="display: none;">
                                                                                    <ul>
                                                                                        <h6><i class="fas fa-angle-right"></i> ¿Tomas medicamentos contra ansiedad depresión o dormir? / ¿Cúal? Dosis
                                                                                            <?php if($item->medicamentos_deprecion!=''){ ?>
                                                                                                <ul>
                                                                                                    <input type="text" class="form-control color_input" value="<?php echo $item->medicamentos_deprecion ?>" readonly>
                                                                                                </ul>
                                                                                            <?php } ?> 
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> Cirugías:
                                                                                            <?php if($item->cirugia!=''){ ?>
                                                                                                <ul>
                                                                                                    <input type="text" class="form-control color_input" value="<?php echo $item->cirugia ?>" readonly>
                                                                                                </ul> 
                                                                                            <?php } ?> 
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> Otras enfermedades importantes:
                                                                                            <?php if($item->otras_enfemedades!=''){ ?>
                                                                                                <ul>
                                                                                                    <input type="text" class="form-control color_input" value="<?php echo $item->otras_enfemedades ?>" readonly>
                                                                                                </ul> 
                                                                                            <?php } ?> 
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> Transfusiones
                                                                                            <?php if($item->transfusiones!=''){ ?>
                                                                                                <ul>
                                                                                                    <input type="text" class="form-control color_input" value="<?php echo $item->transfusiones ?>" readonly>
                                                                                                </ul> 
                                                                                            <?php } ?> 
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> Hospitalizaciones
                                                                                            <?php if($item->hospitalizacion!=''){ ?>
                                                                                                <ul>
                                                                                                    <input type="text" class="form-control color_input" value="<?php echo $item->hospitalizacion ?>" readonly>
                                                                                                </ul> 
                                                                                            <?php } ?> 
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> Alergias(gravedad)
                                                                                            <?php if($item->alergias!=''){ ?>
                                                                                                <ul>
                                                                                                    <input type="text" class="form-control color_input" value="<?php echo $item->alergias ?>" readonly>
                                                                                                </ul> 
                                                                                            <?php } ?> 
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> Fracturas o lesiones
                                                                                            <?php if($item->fracturas!=''){ ?>
                                                                                                <ul>
                                                                                                    <input type="text" class="form-control color_input" value="<?php echo $item->fracturas ?>" readonly>
                                                                                                </ul> 
                                                                                            <?php } ?> 
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> ¿Podrías estar embarazada o en lactancia? / Fecha de última regla
                                                                                            <?php if($item->embarazo_lactancia!=''){ ?>
                                                                                                <ul>
                                                                                                    <input type="text" class="form-control color_input" value="<?php echo $item->embarazo_lactancia ?>" readonly>
                                                                                                </ul> 
                                                                                            <?php } ?> 
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> Hepatitis</h6>
                                                                                        <ul>
                                                                                            <?php if($item->chehepatitisa==1){ ?>
                                                                                                <h6><i class="fas fa-angle-right"></i> Hepatitis A</h6> 
                                                                                            <?php } if($item->chehepatitisb==1){ ?>
                                                                                                <h6><i class="fas fa-angle-right"></i> Hepatitis B</h6>
                                                                                            <?php } if($item->chehepatitisc==1){ ?>
                                                                                                <h6><i class="fas fa-angle-right"></i> Hepatitis C</h6>
                                                                                            <?php } if($item->hepatitis!=''){ ?>
                                                                                                <ul>
                                                                                                    <input type="text" class="form-control color_input" value="<?php echo $item->hepatitis ?>" readonly>
                                                                                                </ul> 
                                                                                            <?php } ?> 
                                                                                        </ul>
                                                                                        <h6><i class="fas fa-angle-right"></i> Diabetes mellitus
                                                                                            <?php if($item->diabetesmillitus!=''){ ?>
                                                                                                <ul>
                                                                                                    <input type="text" class="form-control color_input" value="<?php echo $item->diabetesmillitus ?>" readonly>
                                                                                                </ul> 
                                                                                            <?php } ?> 
                                                                                        </h6>
                                                                                        <h6><i class="fas fa-angle-right"></i> Hipertensión arterial
                                                                                            <?php if($item->hipertensionarterial!=''){ ?>
                                                                                                <ul>
                                                                                                    <input type="text" class="form-control color_input" value="<?php echo $item->hipertensionarterial ?>" readonly>
                                                                                                </ul> 
                                                                                            <?php } ?> 
                                                                                        </h6>
                                                                                        <?php 
                                                                                        if($item->hipercolesterolemia==1){ ?>
                                                                                            <h6><i class="fas fa-angle-right"></i> Hipercolesterolemia</h6>
                                                                                        <?php }
                                                                                        if($item->epilepsia==1){ ?>
                                                                                            <h6><i class="fas fa-angle-right"></i> Epilepsia</h6>
                                                                                        <?php }
                                                                                        if($item->enfermedades_musculares==1){ ?>   
                                                                                            <h6><i class="fas fa-angle-right"></i> Enfermedades musculares</h6>
                                                                                        <?php } ?>
                                                                                    </ul>
                                                                                </div>
                                                                                <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_his4()"><img src="<?php echo base_url() ?>images/relevacion/ico-aparatos.png"> Interrogatorio por Aparatos y Sistemas</button>
                                                                                <div class="btn_his4" style="display: none;">
                                                                                    <ul>
                                                                                        <div class="form-group">
                                                                                            <input type="text" name="quien_refiere" class="form-control color_input" value="<?php echo $item->interrogatorio_aparatos ?>" readonly>
                                                                                        </div>
                                                                                    </ul>
                                                                                </div>
                                                                                <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_his6()"><img style="border-radius: 25px" src="<?php echo base_url() ?>images/relevacion/tabletas.png"> Medicamentos</button>
                                                                                <div class="btn_his6" style="display: none;">
                                                                                    <ul>
                                                                                        <div class="form-group">
                                                                                            <input type="text" name="quien_refiere" class="form-control color_input" value="<?php echo $item->medicamento ?>" readonly>
                                                                                        </div>
                                                                                    </ul>
                                                                                </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="historia_clinica_editar" style="display: none">
                                                                        <form class="form" method="post" role="form" id="form_histotiaclinica">
                                                                            <input type="hidden" name="idpaciente" class="form-control colorlabel">
                                                                            <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_text_his1()">
                                                                                <div class="row">
                                                                                    <div class="col-md-10">
                                                                                        <img src="<?php echo base_url() ?>images/relevacion/ico-paciente.png"> Heredofamiliares (Abuelos, padres, hijos) 
                                                                                    </div>
                                                                                    <div class="col-md-2" align="right">
                                                                                        <strong><i class="fas fa-caret-down"></i></strong>
                                                                                    </div>
                                                                                </div>
                                                                            </button>
                                                                            <div class="text_his1">
                                                                            </div>
                                                                            <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_text_his3()">
                                                                                <div class="row">
                                                                                    <div class="col-md-10">
                                                                                        <img src="<?php echo base_url() ?>images/relevacion/ico-nopatologico.png"> A. Personales NO Patológicos
                                                                                    </div>
                                                                                    <div class="col-md-2" align="right">
                                                                                        <strong><i class="fas fa-caret-down"></i></strong>
                                                                                    </div>
                                                                                </div>
                                                                            </button>
                                                                            <div class="text_his3">
                                                                            </div>
                                                                            <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_text_his5()">
                                                                                <div class="row">
                                                                                    <div class="col-md-10">
                                                                                        <img width="28px" src="<?php echo base_url() ?>images/gineicon.png"> A. Ginecobstetricos
                                                                                    </div>
                                                                                    <div class="col-md-2" align="right">
                                                                                        <strong><i class="fas fa-caret-down"></i></strong>
                                                                                    </div>
                                                                                </div>
                                                                            </button>
                                                                            <div class="text_his5">
                                                                            </div>
                                                                            <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_text_his2()">
                                                                                <div class="row">
                                                                                    <div class="col-md-10">
                                                                                        <img src="<?php echo base_url() ?>images/relevacion/ico-patologico.png"> A. Personales Patológicos
                                                                                    </div>
                                                                                    <div class="col-md-2" align="right">
                                                                                        <strong><i class="fas fa-caret-down"></i></strong>
                                                                                    </div>
                                                                                </div>
                                                                            </button>    
                                                                            <div class="text_his2">
                                                                            </div>
                                                                            <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_text_his4()">
                                                                                <div class="row">
                                                                                    <div class="col-md-10">
                                                                                        <img src="<?php echo base_url() ?>images/relevacion/ico-aparatos.png"> Interrogatorio por Aparatos y Sistemas
                                                                                    </div>
                                                                                    <div class="col-md-2" align="right">
                                                                                        <strong><i class="fas fa-caret-down"></i></strong>
                                                                                    </div>
                                                                                </div>
                                                                            </button>    
                                                                            <div class="text_his4">
                                                                            </div>
                                                                            <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_text_his6()">
                                                                                <div class="row">
                                                                                    <div class="col-md-10">
                                                                                        <img src="<?php echo base_url() ?>images/relevacion/tabletas.png"> Medicamentos
                                                                                    </div>
                                                                                    <div class="col-md-2" align="right">
                                                                                        <strong><i class="fas fa-caret-down"></i></strong>
                                                                                    </div>
                                                                                </div>
                                                                            </button>  
                                                                            <div class="text_his6">
                                                                            </div>
                                                                        </form>   
                                                                    </div> 
                                                                </div>
                                                                <div class="text_historia_clinica_abajo">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--_-------------------------------------------------------------------_-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card">
                                                            <div class="card-body p-b-0">
                                                                <h4 class="card-title">Interrogatorio por sistemas</h4>
                                                                <!-- Nav tabs -->
                                                                <ul class="nav nav-tabs customtab2" role="tablist">
                                                                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#sist1" role="tab"><span class="hidden-sm-up">
                                                                        <img width="28px" src="<?php echo base_url() ?>images/interrogatorio/sintomasgenerales.png">
                                                                    </span> <span class="hidden-xs-down">Síntomas generales</span></a> </li>
                                                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#sist2" role="tab"><span class="hidden-sm-up">
                                                                        <img width="28px" src="<?php echo base_url() ?>images/interrogatorio/aparatoRespiratorio.png">
                                                                    </span> <span class="hidden-xs-down">Aparato Respiratorio</span></a> </li>
                                                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#sist3" role="tab"><span class="hidden-sm-up">
                                                                        <img width="28px" src="<?php echo base_url() ?>images/interrogatorio/aparatoDigestivo.png">
                                                                    </span> <span class="hidden-xs-down">Digestivo</span></a> </li>
                                                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#sist4" role="tab"><span class="hidden-sm-up">
                                                                        <img width="20px" src="<?php echo base_url() ?>images/interrogatorio/cardiovascular.png">
                                                                    </span> <span class="hidden-xs-down">Cardiovascular</span></a> </li>
                                                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#sist5" role="tab"><span class="hidden-sm-up">
                                                                        <img width="28px" src="<?php echo base_url() ?>images/interrogatorio/renalyurinario.png">
                                                                    </span> <span class="hidden-xs-down">Renal y Urinario</span></a> </li>
                                                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#sist6" role="tab"><span class="hidden-sm-up">
                                                                        <img width="28px" src="<?php echo base_url() ?>images/interrogatorio/Genitalfemenino.png">
                                                                    </span> <span class="hidden-xs-down">Genital femenino</span></a> </li>
                                                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#sist7" role="tab"><span class="hidden-sm-up">
                                                                        <img width="28px" src="<?php echo base_url() ?>images/interrogatorio/endocrino.png">
                                                                    </span> <span class="hidden-xs-down">Endocrino</span></a> </li>
                                                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#sist8" role="tab"><span class="hidden-sm-up">
                                                                        <img width="28px" src="<?php echo base_url() ?>images/interrogatorio/linfatico.png">
                                                                    </span> <span class="hidden-xs-down">Hematopoyético y linfático</span></a> </li>
                                                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#sist9" role="tab"><span class="hidden-sm-up">
                                                                        <img width="36px" src="<?php echo base_url() ?>images/interrogatorio/musculoesqueletico.png">
                                                                    </span> <span class="hidden-xs-down">Músculo esquelético</span></a> </li>
                                                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#sist10" role="tab"><span class="hidden-sm-up">
                                                                        <img width="34px" src="<?php echo base_url() ?>images/interrogatorio/nervioso.png">
                                                                    </span> <span class="hidden-xs-down">Nervioso</span></a> </li>
                                                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#sist11" role="tab"><span class="hidden-sm-up">
                                                                        <img width="30px" src="<?php echo base_url() ?>images/interrogatorio/sentidos.png">
                                                                    </span> <span class="hidden-xs-down">Órganos de los sentidos</span></a> </li>
                                                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#sist12" role="tab"><span class="hidden-sm-up">
                                                                        <img width="30px" src="<?php echo base_url() ?>images/interrogatorio/esfera.png">
                                                                    </span> <span class="hidden-xs-down">Esfera psíquica</span></a> </li>
                                                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#sist13" role="tab"><span class="hidden-sm-up">
                                                                        <img width="48px" src="<?php echo base_url() ?>images/interrogatorio/tegumentario.png">
                                                                    </span> <span class="hidden-xs-down">Tegumentario</span></a> </li>
                                                                </ul>
                                                                <!-- Tab panes -->
                                                                <div class="tab-content">
                                                                    <div class="tab-pane p-20 active" id="sist1" role="tabpanel">
                                                                        <!--1-->
                                                                        <h4>Síntomas generales</h4> 
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <form class="form" method="post" role="form" id="form_registro_sistemas1">
                                                                                    <input type="hidden" name="idsintomas" id="idsintomas" value="<?php echo $idsintomas ?>">
                                                                                    Fiebre 
                                                                                    <input type="text" class="form-control2" name="fiebre" oninput="registro_sistemas(1)" value="<?php echo $fiebre ?>"> Astenia(debilidad o fatiga general) 
                                                                                    <input type="text" class="form-control2" name="astenia" oninput="registro_sistemas(1)" value="<?php echo $astenia ?>"> Aumento o pérdida de peso 
                                                                                    <input type="text" class="form-control2" name="aumento" oninput="registro_sistemas(1)" value="<?php echo $aumento ?>"> 
                                                                                    Modificaciones del hambre 
                                                                                    <input type="text" class="form-control2" name="modificacion" oninput="registro_sistemas(1)" value="<?php echo $modificacion ?>"> 
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane p-20" id="sist2" role="tabpanel">
                                                                        <!--2-->
                                                                        <h4>Aparato Respiratorio</h4>  
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <form class="form" method="post" role="form" id="form_registro_sistemas2">
                                                                                    <input type="hidden" name="idaparato" id="idaparato" value="<?php echo $idaparato ?>">
                                                                                    Rinorrea (nariz congestionada) 
                                                                                    <input type="text" class="form-control2" name="rinorrea" oninput="registro_sistemas(2)" value="<?php echo $rinorrea ?>"> Epistaxis (sangrado) 
                                                                                    <input type="text" class="form-control2" name="epistaxis" oninput="registro_sistemas(2)" value="<?php echo $epistaxis ?>"> 
                                                                                    Tos 
                                                                                    <input type="text" class="form-control2" name="tos" oninput="registro_sistemas(2)" value="<?php echo $tos ?>">
                                                                                    Expectoración (expulsar fluidos por la boca) 
                                                                                    <input type="text" class="form-control2" name="expectoracion" oninput="registro_sistemas(2)" value="<?php echo $expectoracion ?>">
                                                                                     Disfonía (pérdida del timbre normal de voz) 
                                                                                    <input type="text" class="form-control2" name="disfonia" oninput="registro_sistemas(2)" value="<?php echo $disfonia ?>"> 
                                                                                    Hemoptisis (expulsión de pequeñas cantidades de sangre) 
                                                                                    <input type="text" class="form-control2" name="hemoptitis" oninput="registro_sistemas(2)" value="<?php echo $hemoptitis ?>"> 
                                                                                    Cianosis (coloración azul de la piel y las mucosas) 
                                                                                    <input type="text" class="form-control2" name="cianosis" oninput="registro_sistemas(2)" value="<?php echo $cianosis ?>"> 
                                                                                    Dolor torácico 
                                                                                    <input type="text" class="form-control2" name="dolor" oninput="registro_sistemas(2)" value="<?php echo $dolor ?>"> 
                                                                                    Disnea (Dificultad para respirar) 
                                                                                    <input type="text" class="form-control2" name="disnea" oninput="registro_sistemas(2)" value="<?php echo $disnea ?>">
                                                                                    Sibilancias audibles a distancia 
                                                                                    <input type="text" class="form-control2" name="sibilancias" oninput="registro_sistemas(2)" value="<?php echo $sibilancias ?>"> 
                                                                                </form>    
                                                                            </div>
                                                                        </div>        
                                                                    </div>
                                                                    <div class="tab-pane p-20" id="sist3" role="tabpanel">
                                                                        <!--3-->
                                                                        <h4>Digestivo</h4>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <form class="form" method="post" role="form" id="form_registro_sistemas3">
                                                                                    <input type="hidden" name="iddigestivo" id="iddigestivo3" value="<?php echo $iddigestivo3 ?>">
                                                                                    Hambre 
                                                                                    <input type="text" class="form-control2" name="hambre" oninput="registro_sistemas(3)" value="<?php echo $hambre3 ?>">  
                                                                                    Apetito 
                                                                                    <input type="text" class="form-control2" name="apetito" oninput="registro_sistemas(3)" value="<?php echo $apetito3 ?>">  
                                                                                    Alteraciones de la masticación y salivación 
                                                                                    <input type="text" class="form-control2" name="masticacion" oninput="registro_sistemas(3)" value="<?php echo $masticacion3 ?>"> 
                                                                                    Disfagia (Dificultad para tragar) 
                                                                                    <input type="text" class="form-control2" name="disfagia" oninput="registro_sistemas(3)" value="<?php echo $disfagia3 ?>">  
                                                                                    Halitosis (Mal aliento) 
                                                                                    <input type="text" class="form-control2" name="halitosis" oninput="registro_sistemas(3)" value="<?php echo $halitosis3 ?>">  
                                                                                    Náusea 
                                                                                    <input type="text" class="form-control2" name="nausea" oninput="registro_sistemas(3)" value="<?php echo $nausea3 ?>"> 
                                                                                    Rumiación (Devolver los alimentos) 
                                                                                    <input type="text" class="form-control2" name="rumiacion" oninput="registro_sistemas(3)" value="<?php echo $rumiacion3 ?>">  
                                                                                    Pirosis (Sensación de quemadura desde la faringe hasta el stómago 
                                                                                    <input type="text" class="form-control2" name="pirosis" oninput="registro_sistemas(3)" value="<?php echo $pirosis3 ?>"> 
                                                                                    Aerofagia (Ingestión de aire que provoca gases o dolor estomacal) 
                                                                                    <input type="text" class="form-control2" name="aerofagia" oninput="registro_sistemas(3)" value="<?php echo $aerofagia3 ?>">  
                                                                                    Eructos 
                                                                                    <input type="text" class="form-control2" name="eructos" oninput="registro_sistemas(3)" value="<?php echo $eructos3 ?>">  
                                                                                    Meteorismo (Acumulación de gases) 
                                                                                    <input type="text" class="form-control2" name="meteorismo" oninput="registro_sistemas(3)" value="<?php echo $meteorismo3 ?>">  
                                                                                    Distensión abdominal (Sentirse lleno o apretado provocando molestia) 
                                                                                    <input type="text" class="form-control2" name="distension" oninput="registro_sistemas(3)" value="<?php echo $distension3 ?>">  
                                                                                    Gases 
                                                                                    <input type="text" class="form-control2" name="gases" oninput="registro_sistemas(3)" value="<?php echo $gases3 ?>">  
                                                                                    Hematemesis (Vómito de sangre origen estómago) 
                                                                                    <input type="text" class="form-control2" name="hematemesis" oninput="registro_sistemas(3)" value="<?php echo $hematemesis3 ?>">  Ictericia (Coloración amarillenta de la piel y
                                                                                    mucosas 
                                                                                    <input type="text" class="form-control2" name="ictericia" oninput="registro_sistemas(3)" value="<?php echo $ictericia3 ?>">  Características de heces fecales (diarrea 
                                                                                    <input type="text" class="form-control2" name="fecales" oninput="registro_sistemas(3)" value="<?php echo $fecales3 ?>">  
                                                                                    / constipación (estreñimiento) 
                                                                                    <input type="text" class="form-control2" name="constipacion" oninput="registro_sistemas(3)" value="<?php echo $constipacion3 ?>">  
                                                                                    / heces blancas 
                                                                                    <input type="text" class="form-control2" name="haces_blancas" oninput="registro_sistemas(3)" value="<?php echo $haces_blancas3 ?>">  
                                                                                    / verdes 
                                                                                    <input type="text" class="form-control2" name="verdes" oninput="registro_sistemas(3)" value="<?php echo $verdes3 ?>">  
                                                                                    / heces negras
                                                                                    <input type="text" class="form-control2" name="haces_negras" oninput="registro_sistemas(3)" value="<?php echo $haces_negras3 ?>">  
                                                                                    Amarillas 
                                                                                    <input type="text" class="form-control2" name="amarillas" oninput="registro_sistemas(3)" value="<?php echo $amarillas3 ?>">  
                                                                                    Rojas 
                                                                                    <input type="text" class="form-control2" name="rojas" oninput="registro_sistemas(3)" value="<?php echo $rojas3 ?>">  
                                                                                    Esteatorrea (heces mal olientes o aceitosas) Rectorragia (Pérdida de sangre a través del ano) 
                                                                                    <input type="text" class="form-control2" name="esteatorrea_aceitosas" oninput="registro_sistemas(3)" value="<?php echo $esteatorrea_aceitosas3 ?>">  
                                                                                    Parásitos
                                                                                    <input type="text" class="form-control2" name="parsitos" oninput="registro_sistemas(3)" value="<?php echo $parsitos3 ?>">  
                                                                                    Pujo (dolor en abdomen que provoca ganas de evacuar u orinar) 
                                                                                    <input type="text" class="form-control2" name="pujo" oninput="registro_sistemas(3)" value="<?php echo $pujo3 ?>"> 
                                                                                    Lienteria (debilitamiento total o parcial de los intestinos) 
                                                                                    <input type="text" class="form-control2" name="lienteria" oninput="registro_sistemas(3)" value="<?php echo $lienteria3 ?>"> 
                                                                                    Tenesmo(Sensación de defecar continuamente) 
                                                                                    <input type="text" class="form-control2" name="tenesmo" oninput="registro_sistemas(3)" value="<?php echo $tenesmo3 ?>">  
                                                                                    Prurito anal (Picazón anal) 
                                                                                    <input type="text" class="form-control2" name="prurito" oninput="registro_sistemas(3)" value="<?php echo $prurito3 ?>">
                                                                                </form>    
                                                                            </div>
                                                                        </div>        
                                                                    </div>
                                                                    <div class="tab-pane p-20" id="sist4" role="tabpanel">
                                                                        <!--4-->
                                                                        <h4>Cardiovascular</h4>    
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <form class="form" method="post" role="form" id="form_registro_sistemas4">
                                                                                    <input type="hidden" name="idcardio" id="idcardio4" value="<?php echo $idcardio4 ?>">
                                                                                    Palpitaciones 
                                                                                    <input type="text" class="form-control2" name="palpitaciones" oninput="registro_sistemas(4)" value="<?php echo $palpitaciones4 ?>"> 
                                                                                    Dolor precordial(dolor en el pecho)
                                                                                    <input type="text" class="form-control2" name="dolor_precordial" oninput="registro_sistemas(4)" value="<?php echo $dolor_precordial4 ?>"> 
                                                                                    Disnea de esfuerzo(dificultad para respirar al realizar alguna acción) 
                                                                                    <input type="text" class="form-control2" name="disnea_esfuerzo" oninput="registro_sistemas(4)" value="<?php echo $disnea_esfuerzo4 ?>"> 
                                                                                    Disnea paroxística(dificultad para respirar al estar acostado) 
                                                                                    <input type="text" class="form-control2" name="disnea_paroxistica" oninput="registro_sistemas(4)" value="<?php echo $disnea_paroxistica4 ?>"> 
                                                                                    Apnea(dificultad para respirar al dormir) 
                                                                                    <input type="text" class="form-control2" name="apnea" oninput="registro_sistemas(4)" value="<?php echo $apnea4 ?>"> 
                                                                                    Cianosis(coloración azulada o violácea general) 
                                                                                    <input type="text" class="form-control2" name="cianosis" oninput="registro_sistemas(4)" value="<?php echo $cianosis4 ?>">
                                                                                    Acúfenos(zumbidos de oído o ruidos en la cabeza) 
                                                                                    <input type="text" class="form-control2" name="acufenos" oninput="registro_sistemas(4)" value="<?php echo $acufenos4 ?>">
                                                                                    Fosfenos(percepción de un destellos luminosos) 
                                                                                    <input type="text" class="form-control2" name="fosfenos" oninput="registro_sistemas(4)" value="<?php echo $fosfenos4 ?>"> 
                                                                                    Síncope(desmayo)
                                                                                    <input type="text" class="form-control2" name="sincope" oninput="registro_sistemas(4)" value="<?php echo $sincope4 ?>"> 
                                                                                    Lipotimias(desvanecimiento) 
                                                                                    <input type="text" class="form-control2" name="lipotimias" oninput="registro_sistemas(4)" value="<?php echo $lipotimias4 ?>"> 
                                                                                    Edema 
                                                                                    <input type="text" class="form-control2" name="edema" oninput="registro_sistemas(4)" value="<?php echo $edema4 ?>">
                                                                                </form>    
                                                                            </div>  
                                                                        </div>      
                                                                    </div>
                                                                    <div class="tab-pane p-20" id="sist5" role="tabpanel">
                                                                        <!--5-->
                                                                        <h4>Renal y Urinario</h4>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <form class="form" method="post" role="form" id="form_registro_sistemas5">
                                                                                    <input type="hidden" name="idrenal" id="idrenal5" value="<?php echo $idrenal5 ?>">
                                                                                    Dolor renoureteral(dolor o cólico en genitales por obstrucción) 
                                                                                    <input type="text" class="form-control2" name="renoureteral" oninput="registro_sistemas(5)" value="<?php echo $renoureteral5 ?>">
                                                                                    Disuria(dolor al momento de evacuar) 
                                                                                    <input type="text" class="form-control2" name="disuria" oninput="registro_sistemas(5)" value="<?php echo $disuria5 ?>"> 
                                                                                    Anuria(ausencia total de orina)
                                                                                    <input type="text" class="form-control2" name="anuria" oninput="registro_sistemas(5)" value="<?php echo $anuria5 ?>"> 
                                                                                    Oliguria(disminución anormal de orina)
                                                                                    <input type="text" class="form-control2" name="oliguria" oninput="registro_sistemas(5)" value="<?php echo $oliguria5 ?>">
                                                                                    Poliuria(excreción abundante de orina) 
                                                                                    <input type="text" class="form-control2" name="poliuria" oninput="registro_sistemas(5)" value="<?php echo $poliuria5 ?>">
                                                                                    Polaquiuria (necesidad de orinar muchas veces)
                                                                                    <input type="text" class="form-control2" name="polaquiuria" oninput="registro_sistemas(5)" value="<?php echo $polaquiuria5 ?>"> 
                                                                                    Hematuria(presencia de sangre en la orina) 
                                                                                    <input type="text" class="form-control2" name="hematuria" oninput="registro_sistemas(5)" value="<?php echo $hematuria5 ?>">
                                                                                    Piuria(presencia de pus en la orina) 
                                                                                    <input type="text" class="form-control2" name="piuria" oninput="registro_sistemas(5)" value="<?php echo $piuria5 ?>"> 
                                                                                    Coluria(presencia de bilis en la orina) 
                                                                                    <input type="text" class="form-control2" name="coluria" oninput="registro_sistemas(5)" value="<?php echo $coluria5 ?>"> 
                                                                                    Incontinencia 
                                                                                    <input type="text" class="form-control2" name="incontinencia" oninput="registro_sistemas(5)" value="<?php echo $incontinencia5 ?>"> 
                                                                                    Edema(exceso de líquido en alguna zona)
                                                                                    <input type="text" class="form-control2" name="edema" oninput="registro_sistemas(5)" value="<?php echo $edema5 ?>">
                                                                                </form>    
                                                                            </div>
                                                                        </div>        
                                                                    </div>
                                                                    <div class="tab-pane p-20" id="sist6" role="tabpanel">
                                                                        <!--6-->
                                                                        <h4>Genital femenino</h4>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <form class="form" method="post" role="form" id="form_registro_sistemas6">
                                                                                    <input type="hidden" name="idgenital" id="idgenital6" value="<?php echo $idgenital6 ?>">
                                                                                    Leucorrea(flujo abundante) 
                                                                                    <input type="text" class="form-control2" name="leucorrea" oninput="registro_sistemas(6)" value="<?php echo $leucorrea6 ?>"> 
                                                                                    Hemorragias transvaginales 
                                                                                    <input type="text" class="form-control2" name="hemorragias" oninput="registro_sistemas(6)" value="<?php echo $hemorragias6 ?>">
                                                                                    Alteraciones menstruales 
                                                                                    <input type="text" class="form-control2" name="alteraciones_menstruales" oninput="registro_sistemas(6)" value="<?php echo $alteraciones_menstruales6 ?>">
                                                                                    Alteraciones de la libido 
                                                                                    <input type="text" class="form-control2" name="alteracioneslibido" oninput="registro_sistemas(6)" value="<?php echo $alteracioneslibido6 ?>"> 
                                                                                     Práctica sexual
                                                                                    <input type="text" class="form-control2" name="practica_sexual" oninput="registro_sistemas(6)" value="<?php echo $practica_sexual6 ?>">
                                                                                    Alteraciones del sangrado menstrual 
                                                                                    <input type="text" class="form-control2" name="alteraciones_sangrado" oninput="registro_sistemas(6)" value="<?php echo $alteraciones_sangrado6 ?>"> 
                                                                                    Dispareunia(dolor o molestia, después o durante la actividad sexual) 
                                                                                    <input type="text" class="form-control2" name="dispareunia" oninput="registro_sistemas(6)" value="<?php echo $dispareunia6 ?>"> 
                                                                                    Perturbaciones y alteraciones sexuales 
                                                                                    <input type="text" class="form-control2" name="perturbaciones" oninput="registro_sistemas(6)" value="<?php echo $perturbaciones6 ?>">
                                                                                </form>    
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="tab-pane p-20" id="sist7" role="tabpanel">
                                                                        <!--7-->
                                                                        <h4>Endocrino</h4>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <form class="form" method="post" role="form" id="form_registro_sistemas7">
                                                                                    <input type="hidden" name="idendocrino" id="idendocrino7" value="<?php echo $idendocrino7 ?>">
                                                                                    Intolerancia al frío y al calor 
                                                                                    <input type="text" class="form-control2" name="intolerancia_frio" oninput="registro_sistemas(7)" value="<?php echo $intolerancia_frio7 ?>"> 
                                                                                    Hiperactividad 
                                                                                    <input type="text" class="form-control2" name="hiperactividad" oninput="registro_sistemas(7)" value="<?php echo $hiperactividad7 ?>"> 
                                                                                    Aumento de volumen del cuello 
                                                                                    <input type="text" class="form-control2" name="aumento_volumen" oninput="registro_sistemas(7)" value="<?php echo $aumento_volumen7 ?>"> 
                                                                                    Polidipsia(aumento anormal de sed) 
                                                                                    <input type="text" class="form-control2" name="polidipsia" oninput="registro_sistemas(7)" value="<?php echo $polidipsia7 ?>"> 
                                                                                    Polifagia(aumento anormal de comer) 
                                                                                    <input type="text" class="form-control2" name="polifagia" oninput="registro_sistemas(7)" value="<?php echo $polifagia7 ?>"> 
                                                                                    Poliuria(excresión abundante de orina) 
                                                                                    <input type="text" class="form-control2" name="poliuria" oninput="registro_sistemas(7)" value="<?php echo $poliuria7 ?>">
                                                                                    Cambios en los caracteres sexuales secundarios
                                                                                    <input type="text" class="form-control2" name="cambios_caracteres" oninput="registro_sistemas(7)" value="<?php echo $cambios_caracteres7 ?>"> 
                                                                                    Aumento o pérdida de peso 
                                                                                    <input type="text" class="form-control2" name="aumento_perdida" oninput="registro_sistemas(7)" value="<?php echo $aumento_perdida7 ?>">
                                                                                </form>    
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="tab-pane p-20" id="sist8" role="tabpanel">
                                                                        <!--8-->
                                                                        <h4>Hematopoyético y linfático</h4>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <form class="form" method="post" role="form" id="form_registro_sistemas8">
                                                                                    <input type="hidden" name="idhematopoyetico" id="idhematopoyetico8" value="<?php echo $idhematopoyetico8 ?>">
                                                                                    Palidez 
                                                                                    <input type="text" class="form-control2" name="palidez" oninput="registro_sistemas(8)" value="<?php echo $palidez8 ?>"> 
                                                                                    Disnea(dificultad para respirar) 
                                                                                    <input type="text" class="form-control2" name="disnea" oninput="registro_sistemas(8)" value="<?php echo $disnea8 ?>"> 
                                                                                    Fatigabilidad 
                                                                                    <input type="text" class="form-control2" name="fatigabilidad" oninput="registro_sistemas(8)" value="<?php echo $fatigabilidad8 ?>">
                                                                                    Palpitaciones 
                                                                                    <input type="text" class="form-control2" name="palpitaciones" oninput="registro_sistemas(8)" value="<?php echo $palpitaciones8 ?>"> 
                                                                                    Sangrado 
                                                                                    <input type="text" class="form-control2" name="sangrado" oninput="registro_sistemas(8)" value="<?php echo $sangrado8 ?>"> 
                                                                                    Equimosis(moretones)
                                                                                    <input type="text" class="form-control2" name="equimosis" oninput="registro_sistemas(8)" value="<?php echo $equimosis8 ?>">
                                                                                    Petequias(lesiones pequeñas rojas)
                                                                                    <input type="text" class="form-control2" name="petequia" oninput="registro_sistemas(8)" value="<?php echo $petequia8 ?>"> 
                                                                                    Astenia(debilidad general) 
                                                                                    <input type="text" class="form-control2" name="astenia" oninput="registro_sistemas(8)" value="<?php echo $astenia8 ?>">
                                                                                </form>    
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="tab-pane p-20" id="sist9" role="tabpanel">
                                                                        <!--9-->
                                                                        <h4>Músculo esquelético</h4>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <form class="form" method="post" role="form" id="form_registro_sistemas9">
                                                                                    <input type="hidden" name="idmusculo" id="idmusculo9" value="<?php echo $idmusculo9 ?>">
                                                                                    Mialgias (dolor muscular) 
                                                                                    <input type="text" class="form-control2" name="mialgias" oninput="registro_sistemas(9)" value="<?php echo $mialgias9 ?>"> 
                                                                                    Dolor óseo
                                                                                    <input type="text" class="form-control2" name="dolor_oseo" oninput="registro_sistemas(9)" value="<?php echo $dolor_oseo9 ?>">
                                                                                    Artralgias (Dolor articulaciones) 
                                                                                    <input type="text" class="form-control2" name="artralgias" oninput="registro_sistemas(9)" value="<?php echo $artralgias9 ?>">
                                                                                    Alteraciones en la marcha 
                                                                                    <input type="text" class="form-control2" name="alteraciones" oninput="registro_sistemas(9)" value="<?php echo $alteraciones9 ?>">
                                                                                    Disminución del volumen muscular 
                                                                                    <input type="text" class="form-control2" name="disminucion_volumen" oninput="registro_sistemas(9)" value="<?php echo $disminucion_volumen9 ?>"> 
                                                                                    Limitación del movimiento 
                                                                                    <input type="text" class="form-control2" name="limitacion_movimiento" oninput="registro_sistemas(9)" value="<?php echo $limitacion_movimiento9 ?>"> 
                                                                                    Deformaciones 
                                                                                    <input type="text" class="form-control2" name="deformacion" oninput="registro_sistemas(9)" value="<?php echo $deformacion9 ?>">
                                                                                </form>    
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="tab-pane p-20" id="sist10" role="tabpanel">
                                                                        <!--10-->
                                                                        <h4>Nervioso</h4>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <form class="form" method="post" role="form" id="form_registro_sistemas10">
                                                                                    <input type="hidden" name="idnervioso" id="idnervioso10" value="<?php echo $idnervioso10 ?>">
                                                                                    Cefaleas (Dolor de cabeza) 
                                                                                    <input type="text" class="form-control2" name="cefaleas" oninput="registro_sistemas(10)" value="<?php echo $cefaleas10 ?>"> 
                                                                                    Paresias (Parálisis) 
                                                                                    <input type="text" class="form-control2" name="paresias" oninput="registro_sistemas(10)" value="<?php echo $paresias10 ?>"> 
                                                                                    Parestesias (Adormecimiento manos, miembros, etc) 
                                                                                    <input type="text" class="form-control2" name="parestesias" oninput="registro_sistemas(10)" value="<?php echo $parestesias10 ?>"> 
                                                                                    Movimientos anormales 
                                                                                    <input type="text" class="form-control2" name="movimientos_anormales" oninput="registro_sistemas(10)" value="<?php echo $movimientos_anormales10 ?>">
                                                                                    Alteraciones de la marcha 
                                                                                    <input type="text" class="form-control2" name="alteraciones_marcha" oninput="registro_sistemas(10)" value="<?php echo $alteraciones_marcha10 ?>"> 
                                                                                    Vértigo 
                                                                                    <input type="text" class="form-control2" name="vertigo" oninput="registro_sistemas(10)" value="<?php echo $vertigo10 ?>"> 
                                                                                    Mareos 
                                                                                    <input type="text" class="form-control2" name="mareos" oninput="registro_sistemas(10)" value="<?php echo $mareos10 ?>">
                                                                                </form>    
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="tab-pane p-20" id="sist11" role="tabpanel">
                                                                        <!--11-->
                                                                        <h4>Órganos de los sentidos</h4>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <form class="form" method="post" role="form" id="form_registro_sistemas11">
                                                                                    <input type="hidden" name="idorganos" id="idorganos11" value="<?php echo $idorganos11 ?>">
                                                                                    Alteraciones de las visión 
                                                                                    <input type="text" class="form-control2" name="alteraciones_vision" oninput="registro_sistemas(11)" value="<?php echo $alteraciones_vision11 ?>"> 
                                                                                    Audición 
                                                                                    <input type="text" class="form-control2" name="audicion" oninput="registro_sistemas(11)" value="<?php echo $audicion11 ?>"> 
                                                                                    Olfato 
                                                                                    <input type="text" class="form-control2" name="olfato" oninput="registro_sistemas(11)" value="<?php echo $olfato11 ?>">
                                                                                    Gusto 
                                                                                    <input type="text" class="form-control2" name="gusto" oninput="registro_sistemas(11)" value="<?php echo $gusto11 ?>"> 
                                                                                    Tacto 
                                                                                    <input type="text" class="form-control2" name="tacto" oninput="registro_sistemas(11)" value="<?php echo $tacto11 ?>"> 
                                                                                    Mareos 
                                                                                    <input type="text" class="form-control2" name="mareos" oninput="registro_sistemas(11)" value="<?php echo $mareos11 ?>"> 
                                                                                    Sensaciones de líquido en el oído 
                                                                                    <input type="text" class="form-control2" name="sensaciones" oninput="registro_sistemas(11)" value="<?php echo $sensaciones11 ?>">
                                                                                </form>
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="tab-pane p-20" id="sist12" role="tabpanel">
                                                                        <!--12-->
                                                                        <h4>Esfera psíquica</h4>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <form class="form" method="post" role="form" id="form_registro_sistemas12">
                                                                                    <input type="hidden" name="idesfera" id="idesfera12" value="<?php echo $idesfera12 ?>">
                                                                                    Tristeza 
                                                                                    <input type="text" class="form-control2" name="tristeza" oninput="registro_sistemas(12)" value="<?php echo $tristeza12 ?>"> 
                                                                                    Euforia 
                                                                                    <input type="text" class="form-control2" name="euforia" oninput="registro_sistemas(12)" value="<?php echo $euforia12 ?>"> 
                                                                                    Alteraciones del sueño 
                                                                                    <input type="text" class="form-control2" name="alteraciones_sueno" oninput="registro_sistemas(12)" value="<?php echo $alteraciones_sueno12 ?>"> 
                                                                                    Terrores nocturnos
                                                                                    <input type="text" class="form-control2" name="terrores_nocturnos" oninput="registro_sistemas(12)" value="<?php echo $terrores_nocturnos12 ?>"> 
                                                                                    Ideaciones 
                                                                                    <input type="text" class="form-control2" name="ideaciones" oninput="registro_sistemas(12)" value="<?php echo $ideaciones12 ?>"> 
                                                                                    Miedo exagerado 
                                                                                    <input type="text" class="form-control2" name="miedo_exagerado" oninput="registro_sistemas(12)" value="<?php echo $miedo_exagerado12 ?>"> 
                                                                                    Irritabilidad 
                                                                                    <input type="text" class="form-control2" name="irritabilidad" oninput="registro_sistemas(12)" value="<?php echo $irritabilidad12 ?>"> 
                                                                                    Apatía 
                                                                                    <input type="text" class="form-control2" name="relaciones_personales" oninput="registro_sistemas(12)" value="<?php echo $relaciones_personales12 ?>">
                                                                                    Relaciones personales 
                                                                                    <input type="text" class="form-control2" name="relaciones_personales" oninput="registro_sistemas(12)" value="<?php echo $relaciones_personales12 ?>">
                                                                                </form>    
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="tab-pane p-20" id="sist13" role="tabpanel">
                                                                        <!--13-->
                                                                        <h4>Tegumentario</h4>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <form class="form" method="post" role="form" id="form_registro_sistemas13">
                                                                                    <input type="hidden" name="idtegumentario" id="idtegumentario13" value="<?php echo $idtegumentario13 ?>">
                                                                                    Coloración anormal 
                                                                                    <input type="text" class="form-control2" name="coloracion_anormal" oninput="registro_sistemas(13)" value="<?php echo $coloracion_anormal13 ?>"> 
                                                                                    Pigmentación 
                                                                                    <input type="text" class="form-control2" name="pigmentacion" oninput="registro_sistemas(13)" value="<?php echo $pigmentacion13 ?>"> 
                                                                                    Prurito(Hormigueo o irritación)
                                                                                    <input type="text" class="form-control2" name="prurito" oninput="registro_sistemas(13)" value="<?php echo $prurito13 ?>"> 
                                                                                    Características del pelo (seco, graso, normal. lacio, ondulado, chino)
                                                                                    <input type="text" class="form-control2" name="caracteristicas" oninput="registro_sistemas(13)" value="<?php echo $caracteristicas13 ?>"> 
                                                                                    Uñas (firmes, quebradizas) 
                                                                                    <input type="text" class="form-control2" name="unas" oninput="registro_sistemas(13)" value="<?php echo $unas13 ?>">
                                                                                    Hiperhidrosis (exceso de sudoración) 
                                                                                    <input type="text" class="form-control2" name="hiperhidrosis" oninput="registro_sistemas(13)" value="<?php echo $hiperhidrosis13 ?>"> 
                                                                                    Xerodermia (resequedad)
                                                                                    <input type="text" class="form-control2" name="xerodermia" oninput="registro_sistemas(13)" value="<?php echo $xerodermia13 ?>">
                                                                                </form>    
                                                                            </div>
                                                                        </div>    
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--_-------------------------------------------------------------------_-->
                                            </div>
                                            
                                        </div>
                                        <div class="tab-pane" id="archivos1" role="tabpanel">
                                            <div class="row" id="ckfinder-widget">
                                                
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="consultas2" role="tabpanel">
                                            <div class="row">
                                                <div class="col-md-2"><br>
                                                    <div class="row">
                                                        <div class="col-lg-12" align="center">
                                                            <button type="button" class="btn btn-rounded btn-block btn-info">HOY</button>
                                                        </div>
                                                    </div>
                                                    <div class="margen_div"></div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <select id="consulta_tipo" class="form-control" onchange="select_tipo_consulta()">
                                                                <option selected="" value="0">Mostrar todo</option>
                                                                <option value="1">Medicina estética</option>
                                                                <option value="2">SPA</option>
                                                                <option value="3">Nutrición</option>
                                                                <option value="4">Ver eliminados</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="margen_div"></div>
                                                    <div class="tipo_text_consulta"></div>
                                                    <div class="margen_div"></div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                    
                                                            <select class="form-control" id="lada">';
                                                            <?php foreach ($get_lada as $item_l){ ?>       
                                                                <option value="<?php echo $item_l->id ?>" <?php if($item_l->id==521) echo 'selected' ?>><?php echo $item_l->nombre ?></option>
                                                            <?php } ?>
                                                            </select>

                                                            <button target="_blank" class="btn waves-effect waves-light btn-success info_pas" data-category="WhatsApp" data-event="click" data-label="Enviar recordatorio" data-celular_w="<?php echo $celular ?>"  onclick="consulta_whats()"><i class="fab fa-whatsapp"></i>  Envío de cuidados especiales vía whatsapp</button>
                                                            <!--
                                                            <button type="button" class="btn waves-effect waves-light btn-block btn-secondary">Guís CENETEC</button>
                                                            <button type="button" class="btn waves-effect waves-light btn-block btn-secondary margen_left"><span class="btn_letra_b">Carga de Históricos</span></button>
                                                            <button type="button" class="btn waves-effect waves-light btn-block btn-secondary margen_left"><span class="btn_letra_b">Comparativa Estudios de Laboratorio</span></button>
                                                            <button type="button" class="btn waves-effect waves-light btn-block btn-secondary margen_left"><span class="btn_letra_b">Comparativa de Diagnósticos</span></button>
                                                            -->
                                                            <?php if($idconsulta_ultima!=0){?>
                                                            <select class="form-control" id="lada">';
                                                            <?php foreach ($get_lada as $item_l){ ?>       
                                                                <option value="<?php echo $item_l->id ?>" <?php if($item_l->id==521) echo 'selected' ?>><?php echo $item_l->nombre ?></option>
                                                            <?php } ?>
                                                            </select>
                                                            
                                                            <button target="_blank" class="btn waves-effect waves-light btn-success info_pas" data-category="WhatsApp" data-event="click" data-label="Enviar recordatorio" data-celular_w="<?php echo $celular ?>"  onclick="enviar_whatsapp()"><i class="fab fa-whatsapp"></i> Envíar último consulta vía WhatsApp</button>
                                                            <?php } ?>

                                                        </div>
                                                    </div>       
                                                </div>
                                                <div class="col-md-10"><br>
                                                    <div class="consulta">
                                                    </div>
                                                </div>    
                                            </div>
                                        </div>    
                                        <div class="tab-pane" id="grafica" role="tabpanel">
                                            <div class="row">
                                                <div class="col-lg-12" align="center"><br>
                                                    <h3>Gráfica de evolución de nutrición</h3>
                                                </div>
                                            </div>
                                            <hr>
                                            <h3 align="center">Tabla de evolución</h3>
                                            <div class="row">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-6">
                                                    <table class="table" id="table_datos">
                                                        <thead class="bg-blue">
                                                            <tr>
                                                                <th>Fecha de consulta</th> 
                                                                <th>Peso</th> 
                                                                <th>IMC</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $result_nutri = $this->ModelCatalogos->get_grafica_nutricion($idpaciente);
                                                            foreach ($result_nutri as $item){ ?>
                                                                <tr>
                                                                    <td><?php echo date('d/m/Y',strtotime($item->consultafecha)) ?></td> 
                                                                    <td><?php echo $item->peso ?></td> 
                                                                    <td><?php echo $item->imc ?></td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>  
                                            <h3 align="center">Grafica de evolución</h3>      
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="bar-chartm" style="width:100%; height:400px;"></div>
                                                </div> 
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12" align="center">
                                                    <button type="button" class="btn waves-effect waves-light btn-info btn_consulta" onclick="agregar_grafica_reporte(<?php echo $idpaciente ?>)"> <i class="fas fa-save"></i> Guardar grafica</button>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <!--- --->     
                                </div>
                                
                            </div>    
                        </div>
                    </div>
                </div>           
            </div>
        </div>
    </div>
</div>  
</div>   
</div>          
<!-- Modal -->
<div id="nuevopago" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #03a9f3; color: white">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                    <h4 class="modal-title">Registrar pagos</h4>
                </div>
                <div class="col-lg-1">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">    
                        <p class="centrado">
                            En este apartado registra el pago ya recibido del paciente.
                        </p>
                    </div>                        
                </div>
                <form class="form" method="post" role="form" id="form_pagos">
                    <input type="hidden" name="idpaciente" id="idpacientep">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><i class="fas fa-calendar"></i> Fecha del pago</label>
                                <input type="date" name="fecha_pago" id="fecha_pago" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><i class="fas fa-money-bill-alt"></i> Método de pago</label>
                                <select name="metodo_pago" id="metodo_pago" class="form-control">
                                    <option selected="" disabled="" value="0">Seleccione</option>
                                    <option value="1">Efectivo</option>
                                    <option value="2">Tarjeta</option>
                                    <option value="3">Transferencia</option>
                                    <option value="4">Cortesía</option>
                                    <option value="5">Por definir</option>
                                    <option value="6">Cheque</option>
                                    <option value="7">Otros</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3"><br><br>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="factura" class="custom-control-input" id="factura">
                                <label class="custom-control-label" for="factura"> Facturado</label>
                            </div>
                        </div>
                    </div> 
                </form>
                <div class="text_tipo_servicio">
                </div> 
            </div>
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">cerrar</button>
                <button type="button" class="btn btn-info waves-effect btn_pago" onclick="guarda_pagos()">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div id="modal_reenviar_f" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #03a9f3; color: white">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                    <h4 class="modal-title">Reenviar formulario</h4>
                </div>
                <div class="col-lg-1">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div align="center">
                    <h6>Ingrese el correo del paciente y verifique que sea el correcto</h6>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="far fa-envelope"></i> Correo electrónico:</label>
                            <input type="email" id="correo_pac" class="form-control" id="exampleInputEmail1">
                        </div>
                    </div>    
                </div>
            </div>
            <input type="hidden" id="idpacientec">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-info waves-effect" onclick="enviar_formulario()">Enviar</button>
            </div>
        </div>   
    </div>
</div>        
<div id="eliminar_acompanante_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar la relación? Este proceso no se puede deshacer</h5>
            </div>
            <input type="hidden" id="idacompaniante_e">
            <input type="hidden" id="id_paciente_e">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="aliminar_acompanante()">Elimanar</button>
            </div>
        </div>   
    </div>
</div>  
<div id="elimina_diagnostico_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Desea archivar el diagnóstico  como resuelto?</h5>
            </div>
            <input type="hidden" id="iddiagnostico_e">
            <input type="hidden" id="iddiagnostico_remove">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="archivardiagnostico()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>  
<div id="elimina_receta_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar ésta receta?</h5>
            </div>
            <input type="hidden" id="idreceta_e">
            <input type="hidden" id="idreceta_remove">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="eliminarreceta()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>  
<div id="elimina_receta_li_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste registro?</h5>
            </div>
            <input type="hidden" id="idmedicamento_e">
            <input type="hidden" id="idmedicamento_remove">
            <input type="hidden" id="idmedicamento_remove_aux">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="eliminarlireceta()">Aceptar</button>
            </div>
        </div>   
    </div>
</div> 
<div id="elimina_orden_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Desea remover ésta órden? El cambio no se puede deshacer</h5>
            </div>
            <input type="hidden" id="idorden_e">
            <input type="hidden" id="idorden_remove">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="eliminarorden()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>  
<div id="elimina_orden_li_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste registro?</h5>
            </div>
            <input type="hidden" id="idlaborat_e">
            <input type="hidden" id="idlaboratorio_remove">
            <input type="hidden" id="idlaboratorio_remove_aux">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="eliminarliorden()">Aceptar</button>
            </div>
        </div>   
    </div>
</div> 
<div id="video_consulta_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                    <h4 class="modal-title text-white">Iniciar Vídeo Consulta</h4>
                </div>
                <div class="col-lg-1">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8"><br><br><br>
                        <h4> <span class="badge badge-info badge-pill">1</span> Envía el link a tu paciente</h4>
                    </div>
                    <div class="col-md-4">
                        <img src="<?php echo base_url() ?>public/img/img-videocall.svg" style="width:180px;">
                    </div>
                </div>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab" aria-selected="true"><span class="hidden-sm-up"><i class="fas fa-share-alt"></i></span> <span class="hidden-xs-down">Copiar Link</span></a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Enviar por correo</span></a> </li>
                    <?php /*    
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="fab fa-whatsapp"></i></span> <span class="hidden-xs-down">Enviar por WhatsApp</span></a> </li>
                    */ ?>
                </ul>
                <div class="tab-content tabcontent-border">
                    <div class="tab-pane active" id="home" role="tabpanel">
                        <div class="p-20">
                            <p>Puedes copiar éste enlace y compartirlo con tu paciente. Te recordamos que éste es el apartado de (Compartir con Paciente), donde además podrá consultar su última receta y los links de cobro online pendientes.</p>
                            <div class="row">
                                <div class="col-md-8 margen_link">
                                    <u><a href="" target="_blank">
                                        <span id="link_video_consulta"></span></a></u>
                                    <input type="hidden" id="codigo_video">    
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-secondary btn-outline" data-toggle="tooltip" data-placement="top" title="" data-original-title="Copiar" onclick="copiar_portapapel('link_video_consulta')"><i class="fas fa-copy"></i></button>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="tab-pane p-20" id="profile" role="tabpanel">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label>Email</label>
                                    <div class="input-group mb-3">
                                        <input type="text" id="correo_videollamda" class="form-control" placeholder="Correo Electrónico" value="<?php echo $correo ?>">
                                        <div class="input-group-append">
                                            <button type="button" class="btn waves-effect waves-light btn-info" onclick="enviar_videollamda()"><i class="far fa-paper-plane"></i> Enviar</button>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <h4><span class="badge badge-info badge-pill">2</span> Pide a tu paciente abrir el link y otorgar los permisos para la vídeo consulta.</h4>
            </div>
            <input type="hidden" id="idlaborat_e">
            <input type="hidden" id="idlaboratorio_remove">
            <input type="hidden" id="idlaboratorio_remove_aux">
            <div class="modal-footer">
                <div class="col-lg-12" align="center">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-info waves-effect" onclick="iniciar_video_consulta()"><i class="fas fa-video"></i> Iniciar Vídeo Consulta</button>
                 </div>
            </div>
        </div>   
    </div>
</div> 
<div id="elimnar_doc_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea elimiar el archivo de cumplimiento?</h5>
            </div>
            <input type="hidden" id="idpaciente_doc">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="eliminardoc()">Aceptar</button>
            </div>
        </div>   
    </div>
</div> 


<div id="elimnar_doc_modal_legal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea elimiar el archivo de documentos legales?</h5>
            </div>
            <input type="hidden" id="iddocumento_e">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="eliminardoc_legal()">Aceptar</button>
            </div>
        </div>   
    </div>
</div> 


<div id="elimina_orden_hosp_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste registro?</h5>
            </div>
            <input type="hidden" id="idorden_hospital_e">
            <input type="hidden" id="idorden_hospital_remove">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="eliminarordenhospi()">Aceptar</button>
            </div>
        </div>   
    </div>
</div> 
<div id="elimina_certificado_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste registro?</h5>
            </div>
            <input type="hidden" id="idcertificado_medico_e">
            <input type="hidden" id="idcertificado_medico_remove">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="eliminarcertificado()">Aceptar</button>
            </div>
        </div>   
    </div>
</div> 
<div id="elimina_consulta_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar ésta consulta?</h5>
            </div>
            <input type="hidden" id="idconsulta_e">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="eliminarconsulta()">Aceptar</button>
            </div>
        </div>   
    </div>
</div> 
<div id="elimina_interconsulta_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste registro?</h5>
            </div>
            <input type="hidden" id="idinterconsulta_medico_e">
            <input type="hidden" id="idinterconsulta_medico_remove">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="eliminarinterconsulta()">Aceptar</button>
            </div>
        </div>   
    </div>
</div> 

<div id="elimina_interconsulta_li_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste registro?</h5>
            </div>
            <input type="hidden" id="idinterconsulta_e">
            <input type="hidden" id="idinterconsulta_remove">
            <input type="hidden" id="idinterconsulta_remove_aux">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="eliminarilinterconsulta()">Aceptar</button>
            </div>
        </div>   
    </div>
</div> 

<div id="elimina_justificante_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste registro?</h5>
            </div>
            <input type="hidden" id="idjustificante_e">
            <input type="hidden" id="idjustificante_remove">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="eliminarjustificante()">Aceptar</button>
            </div>
        </div>   
    </div>
</div> 

<div id="elimina_orden_libre_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste registro?</h5>
            </div>
            <input type="hidden" id="idorden_e">
            <input type="hidden" id="idorden_remove">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="eliminarorden_libre()">Aceptar</button>
            </div>
        </div>   
    </div>
</div> 

<div id="elimina_paciente_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar éste paciente? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_paciente_registro">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="aliminar_paciente()">Aceptar</button>
            </div>
        </div>   
    </div>
</div> 

<div class="iframereceta" style="display: none;">     
</div>
<!-- -->
<div id="elimina_diagnostico_estetica_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Desea eliminar este diagnóstico?</h5>
            </div>
            <input type="hidden" id="iddiagnostico_c1">
            <input type="hidden" id="iddiagnostico_remove_c1">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_diagnostico_estetica()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>  
<div id="elimina_tratamiento_estetica_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Desea eliminar este tramiento?</h5>
            </div>
            <input type="hidden" id="idtratami_c1">
            <input type="hidden" id="idtratami_remove_c1">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_tratamiento_estetica()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>  

<div id="elimina_modal_venta_servicio_c1" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Desea eliminar esta venta de servicio?</h5>
            </div>
            <input type="hidden" id="idvet_c1">
            <input type="hidden" id="idvet_c1_remove">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_venta_servicio_c1()">Aceptar</button>
            </div>
        </div>   
    </div>
</div> 

<div id="modal_ticket_medicina_estetica" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Ticket</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body iframereporte">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="imprimiriframac1('iframeprintc1')">Imprimir</button>
            </div>
        </div>   
    </div>
</div> 

<div id="modal_conuslta_whats" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #779155 ; color: white">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                    <h4 class="modal-title">Envíar último consulta vía WhatsApp</h4>
                </div>
                <div class="col-lg-1">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="btn_consultas">
                    <div class="row">
                    <?php if($permiso_perfil1==1){ ?>
                        <div class="col-md-4">
                            <div class="card">
                                <button type="button" onclick="ver_consultas(1)" style="border-radius: 50px;">
                                    <img class="card-img" style="border-radius: 50px;" src="<?php echo base_url() ?>images/estetica.jpg" height="300" alt="Card image">
                                    <div class="card-img-overlay card-inverse text-white social-profile d-flex justify-content-center" style="border-radius: 50px;">
                                        <div class="align-self-center"> <img  src="<?php echo base_url() ?>images/FAV.png" class="img-circle" width="100">
                                            <h4 class="card-title">Medicina estética</h4>
                                            <h6 class="card-subtitle"></h6>
                                        </div>
                                    </div>
                                </button>
                            </div>
                        </div>
                    <?php } 
                    if($permiso_perfil2==1){ ?>
                        <div class="col-md-4">
                            <div class="card">
                                <button type="button" onclick="ver_consultas(2)" style="border-radius: 50px;">
                                    <img class="card-img" style="border-radius: 50px;" src="<?php echo base_url() ?>images/spa.jpg" height="300" alt="Card image">
                                    <div style="border-radius: 50px;" class="card-img-overlay card-inverse text-white social-profile d-flex justify-content-center">
                                        <div class="align-self-center"> <img src="<?php echo base_url() ?>images/FAV.png" class="img-circle" width="100">
                                            <h4 class="card-title">SPA</h4>
                                            <h6 class="card-subtitle"></h6>
                                        </div>
                                    </div>
                                </button>
                            </div>
                        </div>
                    <?php } 
                    if($permiso_perfil3==1){ ?>
                        <div class="col-md-4">
                            <div class="card">
                                <button type="button" onclick="ver_consultas(3)" style="border-radius: 50px;">
                                <img style="border-radius: 50px;" class="card-img" src="<?php echo base_url() ?>assets/images/background/socialbg.jpg" height="300" alt="Card image">
                                <div style="border-radius: 50px;" class="card-img-overlay card-inverse text-white social-profile d-flex justify-content-center">
                                    <div class="align-self-center"> <img src="<?php echo base_url() ?>images/FAV.png" class="img-circle" width="100">
                                        <h4 class="card-title">Nutrición</h4>
                                        <h6 class="card-subtitle"></h6>
                                    </div>
                                </div>
                                </button>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                </div>
                <div class="txt_consultas_tipo"></div>
                <div class="txt_servicios_consultas"></div>
            </div>
        </div>   
    </div>
</div>   
<!--
<div id="modal_nutricion_servicios_especificos" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Servicios específicos</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body iframereporte">
                <div class="row">
                    <div class="col-lg-4">
                        <label>Servicio</label>
                        <select class="form-control" id="idservicio">
                        </select>
                    </div>
                    <div class="col-lg-8">
                        <br>
                        <table class="table" id="table_servicio">
                            <thead class="bg-blue">
                                <tr>
                                    <th>Servicio</th> 
                                    <th>Descripción</th>
                                    <th>Costo</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <br>
                        <div align="right">
                            <h4>Total: $<span class="total_costo">0</span></h4>
                        </div>    
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
            </div>
        </div>   
    </div>
</div> 

<div id="modal_nutricion_productos_especificos" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Productos específicos</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body iframereporte">
          
                <div class="row">
                    <div class="col-lg-4">
                        <label>Producto</label>
                        <select class="form-control" id="idproducto">
                        </select>
                        <input type="hidden" id="precio_clinica" value="0">
                    </div>
                    <div class="col-lg-8">
                        <div class="lotes_productos">
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table" id="tabla_producto_venta">
                            <thead class="bg-blue">
                                <tr>
                                    <th>Producto</th> 
                                    <th>Lote</th>
                                    <th>Caducidad</th>
                                    <th>Cantidad</th>
                                    <th>Precio U</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <br>
                        <div align="right">
                            <h4>Total: $<span class="total_costop">0</span></h4>
                        </div>  
                    </div>
                </div>    
       
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
            </div>
        </div>   
    </div>
</div> 
-->
<div id="modal_nutricion_plan_alimenticio" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Plan alimenticio</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <!-------------------------------------->
                <div class="row">
                                <div class="col-md-2">
                                    <table class="table" id="tablegeneraplan_l">
                                        <thead>
                                            <tr>
                                                <th>Lunes</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbodytablegeneraplan_l">
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-2">
                                    <table class="table" id="tablegeneraplan_m">
                                        <thead>
                                            <tr>
                                                <th>Martes</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody class="tbodytablegeneraplan_m">
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-2">
                                    <table class="table" id="tablegeneraplan_mi">
                                        <thead>
                                            <tr>
                                                <th>Miercoles</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbodytablegeneraplan_mi">
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-2">
                                    <table class="table" id="tablegeneraplan_ju">
                                        <thead>
                                            <tr>
                                                <th>Jueves</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbodytablegeneraplan_j">
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-2">
                                    <table class="table" id="tablegeneraplan_vi">
                                        <thead>
                                            <tr>
                                                <th>Viernes</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbodytablegeneraplan_v">
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    
                <!-- -->
            </div>
            <div class="modal-footer">
                <!--
                <button type="button" class="btn btn-primary waves-effect text-left"  onclick="saveplan()">Guardar</button>
                -->
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
            </div>
        </div>   
    </div>
</div> 

<div id="modal_cotrol_sesion_me" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Control de sesiones</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <!-- -->  
                <div class="imprimir_sesiones_estetica">
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="texto_control_sesiones_c1"></div>
                    </div>    
                </div>        
                
                <!-- -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
            </div>
        </div>   
    </div>
</div>