<html><head>
  <title>Karla Coello</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0">
  <meta name="author" content="Carlos Corona">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>images/FAV.png">
    <link href="<?php echo base_url();?>aviso/bootstrap.min.css?v=34-2-5-16" rel="stylesheet" type="text/css" lazyload="">
    <link href="<?php echo base_url();?>assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
 </head>
 <style type="text/css">
 	@media print {
		html {
			margin: 40px;
		}
	}


 </style>
	<body>
		<div id="" style="text-align:center">
			<h4>CONSENTIMIENTO INFORMADO PARA LA APLICACIÓN DE UNA INYECCIÓN DE TOXINA BOTULÍNICA TIPO A, COMO BOTOX® (ALLERGAN),XEOMIN® (MERZ) O DYSPORT® (MEDICIS) LINURASE, NABOTA</h4>
		</div>
		<div id="date" style="text-align:right;position:fixed;right:1cm;top:0.5cm;">
		    <?php echo date('d/m/Y');?>	
	    </div>
		<div style="text-align: justify; list-style-type: upper-latin; font-size: 11.5px">
		<b>Escriba sus iniciales después de cada declaración y firme al pie de la página. </b>
            NOTA: - Este consentimiento será válido para los tratamientos a realizarse el día de hoy
            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:4cm;border-bottom:1px solid black;"></div>
            TOXINA: Botox®, Xeomin®, Dysport® 500u, Linurase 100U Nabota  es una toxina botulínica que actúa como relajante muscular.* Se utiliza para el tratamiento <b>temporal</b> de arrugas en el rostro y el cuello como resultado de las expresiones faciales, además de su uso en hiperhidrosis en manos, axilas y pies. 
		<br>
		Yo, (nombre completo)- 
            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:5cm;border-bottom:1px solid black;"></div>
		, presto mi consentimiento y autorizo A Dra Karla Coello Vázquez. a realizar un tratamiento destinado a la posible reducción de arrugas faciales con Botox®-Xeomin®-Dysport®.Linurase, Nabota
		<br>
            Se me ha explicado la naturaleza y el objeto del tratamiento y se han respondido todas las preguntas en lo referido al tratamiento, a mi entera satisfacción. Tengo pleno conocimiento de los riesgos de complicaciones, reacciones adversas o lesiones  que pueden ocurrir como consecuencia del tratamiento, tanto por causas conocidas como desconocidas. Las complicaciones conocidas pueden incluir: Enrojecimiento, hinchazón/edema, prurito, moretón, dolor o presión que pueden extenderse por más de una semana. Nódulos o endurecimiento en la zona de aplicación de la inyección. Cambio de color de la piel en la zona de aplicación de la inyección. Efecto o resultado pobre.Reacciones alérgicas.Asimetría o irregularidad facial.  Edema palpebral (bolsas en los párpados) y/o cejas caídas, y diplopía (visión doble). Debilidad y/o síntomas pseudogripales. Desarrollo de anticuerpos contra el Botox®-Dysport® Linurase, Nabota. Pérdida semipermanente del tono muscular en la zonas multitratadas, Certifico, además, que no padezco ninguna de las enfermedades conocidas que pueden contraindicar el tratamiento.Estas enfermedades como: cicatrices hipertróficas, antecedentes de alguna enfermedad autoinmune (como lupus, esclerodermia, artritis reumatoide), o tratamiento con inmunodepresores, o alguna enfermedad muscular como miastenia grave.
            <br>
            No estoy <b>embarazada ni en el período de lactancia materna</b>.
            <br>
            No sufro ninguna alergia conocida a la albúmina (clara de huevo) ni al Botox®-Xeomin® o Dysport®. Certifico que soy un adulto competente de al menos 18 años de edad.No se ha otorgado ninguna garantía, aseveración ni seguridad en lo referido a los resultados del tratamiento. 
            <br>
            <b>Entiendo que los resultados normalmente resultan evidentes después de 2 a 5 días, alcanzando su máximo efecto a los 21 días y que la naturaleza de los resultados es temporal (los resultados típicamente duran entre 2 y 4 meses según fuentes oficiales. </b>
			<br> 
            Entiendo las EXPECTATIVAS PROPIAS DE MI TRATAMIENTO QUE SON: Arrugas estáticas en frente que tardarán más tiempo en mejorar recomiendo revitalización facial. Entiendo que la Dra hace un cálculo de unidades aproximadas para mi tratamiento el cual se aplicará en este instante, se tiene un máximo de 15 días para notificar falta de respuesta total o parcial (que aún haya contracción) para acudir a ¨Retocar¨el cual será valorado y podrá tener o no tener costo dependerá del criterio:No tiene costo si se trata de una corrección técnica de  una ceja o área que por técnica haya quedado activa Máximo de 3 U 
            <br>
            Tiene costo si aún hay zonas musculares con movimiento que no tiene que ver con la técnica simplemente hace falta más 1 U TIENE COSTO DE $90. 
            <br>
            10. Me comprometo a seguir todas las precauciones de seguridad e instrucciones de cuidado  posterior, entre las que se incluyen: No recostarse ni reclinarse durante 5 a 6  horas una vez aplicada la inyección. No rascarse ni frotarse la zona de aplicación de la inyección. No inclinarse hacia adelante durante cuatro horas. No maquillarse durante una o dos horas después de aplicada la inyección. Hacer ejercicios postratamiento a cada minuto durante 2 horas después de realizado el tratamiento.No inyectar al mismo tiempo vacunas o enzimas pb serum plus.
            <br>
            11. Hago constar que he leído y entendido el presente consentimiento informado en su totalidad y ratifico la información aquí consignada. 
            <br>
            16. El presente consentimiento informado se realiza de manera libre y voluntaria y será obligatorio para mi cónyuge, parientes, representantes legales, herederos, administradores, sucesores y cesionarios.
            <br>
            <b>Fecha </b>
            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:2cm;border-bottom:1px solid black;"></div>
            <b>Producto: </b>
            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:5cm;border-bottom:1px solid black;"></div> <b>TOTAL $</b>
            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:1cm;border-bottom:1px solid black;"></div><b>MXN </b>
            <br>
            <b>Áreas de aplicación:</b>
            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div>
		        <br>
            <!-- --->
            <div style="">
            <input type="hidden" id="base_url" value="<?php echo base_url(); ?>" readonly>
            <input type="hidden" id="idpaciente" value="<?php echo $paciente->idpaciente ?>">
            <input type="hidden" id="tipo_aviso" value="<?php echo $tipo_aviso ?>">
             <?php if($iddocumento==0){ ?>
                  <?php if($tipo==0){ ?>
                        <table style="width: 100%">
                              <tbody>
                                    <tr style="text-align: center">
                                          <td style="width: 50%">
                                                <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div>
                                          </td>
                                          <td style="width: 50%">
                                                <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div>
                                          </td>
                                    </tr>
                                    <tr style="text-align: center">
                                          <td style="width: 50%">
                                                Nombre y firma del paciente:
                                                <div style="font-size: 16px">
                                                    <?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?> 
                                                </div> 
                                          </td>
                                          <td style="width: 50%;">
                                                Nombre y firma del médico:
                                          </td>
                                    </tr>
                              </tbody>
                        </table>
                  <?php } else{?>
                        <table style="width: 100%">
                              <tbody>
                                    <tr style="text-align: center">
                                          <td style="width: 50%">
                                                <div class="firma_text" id="aceptance">
                                                      <div id="signature" class="signature col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                                                            <label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma en el recuadro, su mouse o su dedo</label><br>
                                                            <canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
                                                            <img src="<?php echo base_url() ?>public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignature" data-signature="patientSignature">
                                                            <br>                       
                                                      </div>                  
                                                </div>
                                          </td>
                                          <td style="width: 50%">
                                                <div class="firma_text" id="aceptance">
                                                      <div id="signaturepfc1" class="signaturepfc1 col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                                                            <label for="patientSignaturem" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma en el recuadro, su mouse o su dedo</label><br>
                                                            <canvas id="patientSignaturem" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
                                                            <img src="<?php echo base_url() ?>public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignaturepfc1" data-signature="patientSignaturem">
                                                            <br>                       
                                                      </div>                  
                                                </div>
                                          </td>
                                    </tr>
                                    <tr style="text-align: center">
                                          <td style="width: 50%">
                                                Nombre y firma del paciente:
                                                <div style="font-size: 16px">
                                                    <?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?> 
                                                </div> 
                                          </td>
                                          <td style="width: 50%;">
                                                Nombre y firma del médico:
                                          </td>
                                    </tr>
                              </tbody>
                        </table>
                        <div class="row" style="text-align: center">
                              <button class="btn_estilo" id="btn_firma_save" onclick="saveex_paciente_medico()">Aceptar Firma</button>
                        </div>   
                  <?php }?>
             <?php }else {
                  $result_get=$this->General_model->get_record('iddocumento',$iddocumento,'documentos_legales');
                  ?>
                  <table style="width: 100%">
                        <tbody>
                              <tr style="text-align: center">
                                    <td style="width: 50%">
                                          <div style="text-align:center;margin-bottom:10px;border-radius:4px;">
                                            <?php
                                            $fh = fopen(base_url()."uploads/paciente_firma/".$result_get->firma, 'r') or die("Se produjo un error al abrir el archivo");
                                            $linea = fgets($fh);
                                            fclose($fh);  
                                            ?>
                                            <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                                                <img src="<?php echo $linea ?>" width="170" height="75" style="border:dotted 1px black;">
                                            </div> 
                                          </div>          
                                    </td>
                                    <td style="width: 50%">
                                          <?php
                                            $fh2 = fopen(base_url()."uploads/paciente_firma_doc/".$result_get->firma_medico, 'r') or die("Se produjo un error al abrir el archivo");
                                            $linea2 = fgets($fh2);
                                            fclose($fh2);  
                                            ?>
                                            <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                                                <img src="<?php echo $linea2 ?>" width="170" height="75" style="border:dotted 1px black;">
                                            </div> 
                                    </td>
                              </tr>
                              <tr style="text-align: center">
                                    <td style="width: 50%">
                                          Nombre y firma del paciente:
                                          <div style="font-size: 16px">
                                              <?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?> 
                                          </div> 
                                    </td>
                                    <td style="width: 50%;">
                                          Nombre y firma del médico:
                                    </td>
                              </tr>
                        </tbody>
                  </table>
            <?php }?>
            </div>
            <!-- --->
            <br>
            DRA KARLA COELLO VÁZQUEZ  8435100 	
        </div>	
    </body>
</html>