<style type="text/css">
	.letra_arial{
		font-family: sans-serif;
	}
</style>      
      	<table >
		  	 <thead>
		  	 	<tr>
		  	 		<th rowspan="9" style="background-color: #003166; color: #003166; -webkit-print-color-adjust: exact;">..</th>
		  	 		<th rowspan="9" style="background-color: #00a5e1; color: #00a5e1; -webkit-print-color-adjust: exact;">..</th>
		  	 	</tr>
		  	 	<tr>
		  	 	    <th rowspan="3" style="width: 200px" align="left">
		  	 	    	<img src="<?php echo base_url(); ?>/public/img/logo_unne.png" style="width: 250px; left: 5px">
		  	 	    </th>
		  	 	    <th style="width: 150px; color: white; -webkit-print-color-adjust: exact;">.......</th>
		  	 	    <th style="width: 150px; color: white; -webkit-print-color-adjust: exact;">.......</th>

		  	 	</tr>
	
		  	 	<tr>
		  	 		<th style="color: white; -webkit-print-color-adjust: exact;">.....</th>
		  	 		<th colspan="3" style="background-color: #00a5e1; color: white; border-radius: 9px; -webkit-print-color-adjust: exact;" class="letra_arial">
		  	 	    	  Órden de Estudio 
		  	 	    </th>	

		  	 	</tr>
		  	 	<tr>
		  	 		<th style="color: white; -webkit-print-color-adjust: exact;">..........</th>
		  	 		<th style="color: white; -webkit-print-color-adjust: exact;">..........</th>
		  	 		<th colspan="3" align="center">

		  	 		</th>
		  	 	</tr>	
		  	 	<tr>
		  	 		<th colspan="5" style="background-color: #00a5e1; color: #00a5e1; -webkit-print-color-adjust: exact;">
		  	 	</tr><!--<?php  ?>-->
		  	 	<tr>
		  	 		<th colspan="5" align="left">
		  	 		    <div style="font-size:13px;" class="letra_arial">Fecha:
		  	 		      <b style="color: white;  -webkit-print-color-adjust: exact;" class="letra_arial">..........</b>
		  	 		      <b style="font-size:11px " class="letra_arial"><?php echo date('d/m/Y',strtotime($consulta->consultafecha)) ?></b>
		  	 		    </div>
		  	 		    <div style="font-size:13px;" class="letra_arial">Nombre:
		  	 		      <b style="color: white;  -webkit-print-color-adjust: exact;" class="letra_arial">.......</b>
		  	 		      <b style="font-size:11px;" class="letra_arial"><?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?></b>
		  	 		    </div>
		  	 		    <div style="font-size:13px;" class="letra_arial">Edad:
		  	 		      <b style="color: white; -webkit-print-color-adjust: exact;" class="letra_arial">...........</b>
		  	 		      <b style="font-size:11px;" class="letra_arial"><?php echo $edad ?> años</b>
		  	 		    </div>
                        <?php if($orden->recomendar==1){ ?>
                        	<?php if($orden->nombre_laboratorio!=''){ ?>
                            <div style="font-size:13px;" class="letra_arial">Laboratorio:
			  	 		      <b style="color: white; -webkit-print-color-adjust: exact;" class="letra_arial">.......</b>
			  	 		      <b style="font-size:11px;" class="letra_arial"><?php echo $orden->nombre_laboratorio ?></b>
			  	 		    </div>
                            <?php } 
                            if($orden->direccion!=''){ ?>
			  	 		    <div style="font-size:13px;" class="letra_arial">Dirección:
			  	 		      <b style="color: white; -webkit-print-color-adjust: exact;" class="letra_arial">..........</b>
			  	 		      <b style="font-size:11px;" class="letra_arial"><?php echo $orden->direccion ?></b>
			  	 		    </div>
                            <?php } 
                            if($orden->telefono!=''){ ?>
			  	 		    <div style="font-size:13px;" class="letra_arial">Teléfono:
			  	 		      <b style="color: white; -webkit-print-color-adjust: exact;" class="letra_arial">...........</b>
			  	 		      <b style="font-size:11px;" class="letra_arial"><?php echo $orden->telefono ?></b>
			  	 		    </div>
                        <?php } } ?>
		  	 		    <div style="font-size:13px;" class="letra_arial">Favor de realizar:
		  	 		        <ol>
                            <?php foreach ($list_estudio as $item) { ?>
		  	 		      	    <li><?php echo $item->detalles ?></li>
                            <?php } ?>
		  	 		        </ol>
		  	 		    </div>
		  	 		    <?php if($orden->incluir==1){ ?>
		  	 		    	<?php if($orden->diagnostico!=''){ ?>
		  	 		    	<div style="font-size:13px;" class="letra_arial">Diagnóstico:
			  	 		    </div>
		  	 		    	<div style="font-size:13px;" class="letra_arial">
			  	 		      <b style="font-size:11px;" class="letra_arial"><?php echo $orden->diagnostico ?></b>
			  	 		    </div>
		  	 		    <?php } } ?>
		  	 		</th>
		  	 	</tr>
		  	 	<tr>

		  	 	</tr>
		  	 	<tr>
		  	 		<th colspan="4" align="left">
		  	 		    <div style="color: #003166; -webkit-print-color-adjust: exact;">
		  	 		       <b style="color: white; -webkit-print-color-adjust: exact;">......</b>
		  	 			</div>
		  	 		</th>
		  	 		<th align="left">
		  	 			<div align="center" style="font-size:11px; color: white; -webkit-print-color-adjust: exact;" class="letra_arial">...........................................
		  	 		    </div>
		  	 		</th>
		  	 	</tr>
		  	 </thead>
		</table>
