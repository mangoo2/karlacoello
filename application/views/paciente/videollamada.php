<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/estilo_css.css">
  <script type='text/javascript' src='https://cdn.scaledrone.com/scaledrone.min.js'></script>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <style>
    /*
    body {
      background: #0098ff;
      display: flex;
      height: 100vh;
      margin: 0;
      align-items: center;
      justify-content: center;
      padding: 0 50px;
      font-family: -apple-system, BlinkMacSystemFont, sans-serif;
    }
    */
    body{
      /*background:url(<?php echo base_url() ?>css/fondounne.png);*/
      text-align: center;
    }
    .localvideo{
      background: white;
      /*
      background-image: url(https://www.kirupa.com/images/orange_logo_svg.svg);
      */
      background-image: url(<?php echo base_url() ?>images/medicox.jpeg);
      background-repeat: no-repeat;
      background-position: center;
      background-size: contain;
      max-width: calc(80% - 5%);
      margin: 0 5%;
      box-sizing: border-box;
      border-radius: 10px;
      padding: 0;
    }
    .remotevideo{
      background: white;
      /*
      background-image: url(https://www.kirupa.com/images/orange_logo_svg.svg);
      */
      background-image: url(<?php echo base_url() ?>images/pacientex.jpeg);
      background-repeat: no-repeat;
      background-position: center;
      background-size: contain;
      max-width: calc(80% - 5%);
      margin: 0 5%;
      box-sizing: border-box;
      border-radius: 10px;
      padding: 0;
    }
    .copy {
      position: fixed;
      top: 25px;
      left: 50%;
      transform: translateX(-50%);
      font-size: 18px;
      color: white;
    }
    .btn_estilo{
    text-decoration: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      -webkit-tap-highlight-color: transparent;
      -webkit-transition: all linear;
      -moz-transition: all linear;
      -o-transition: all linear;
      -ms-transition: all ease-out transition: all linear;
      color: #fff;
      box-shadow: 0 1px 3px 0 rgba(0,0,0,0.15);
      min-width: 130px;
      padding: 5px 30px;
      outline: 0px;
      color: #fff;
      background-color: #2196F3;
      height: 35px;
      min-height: 35px;
      border-radius: 3px;
      font-size: 16px;
      padding: 5px 20px;
      cursor: pointer;
      border: none;
      text-align: center;
      -webkit-appearance: none;
      outline: none;
      box-shadow: 
  }
  </style>
</head>
<body>
  <div class="">
    <div class="container-fluid">
  
      <div class="row">
          <div class="col-md-12"><br>
              <div class="card">
                  <div class="row">
                      <div class="col-lg-12">
                          <div class="card-body">
                            <div class="row">
                              <div class="col-md-12">
                                <video class="remotevideo" id="remoteVideo" autoplay></video>
                              </div> 
                            </div>  
                            <div class="row">
                              <div class="col-md-12">
                                <br><br>
                              </div>
                            </div>    
                            <div class="row">
                              <div class="col-md-12">
                                <video class="localvideo" id="localVideo" autoplay muted></video>
                              </div> 
                            </div>  
                            <div class="row">
                              <div class="col-md-12"><br>
                                <button type="button" class="btn_estilo" onclick="cerrar_video_llamada()">Terminar video consulta</button>
                              </div> 
                            </div>  
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</body>                          
  
  
</html>
