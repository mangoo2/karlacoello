<html><head>
	<title>Karla Coello</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0">
	<meta name="author" content="Carlos Corona">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>images/FAV.png">
    <link href="<?php echo base_url();?>aviso/bootstrap.min.css?v=34-2-5-16" rel="stylesheet" type="text/css" lazyload="">
    <link href="<?php echo base_url();?>assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
 </head>
 <style type="text/css">
 	@media print {
		html {
			margin: 40px;
		}
	}
 </style>
	<body>
		<div id="" style="text-align:center">
			<h4>CONSENTIMIENTO INFORMADO PARA LA APLICACIÓN DE ENZIMAS PLUS</h4>
		</div>
		<div id="date" style="text-align:right;position:fixed;right:1cm;top:0.5cm;">
		    <?php echo date('d/m/Y');?>	
	    </div>
		<div style="text-align: justify; list-style-type: upper-latin; font-size: 12.5px">
			<b>Consecuencias que frecuentemente se producen:</b><br>
            Enrojecimiento alrededor de los pinchazos que normalmente pueden tardar unas horas o pocos días en desaparecer. Sensación de incomodidad  o dolor dependiendo de la zona a tratar, puede haber sensación de protuberancia, ardor durante la aplicación, moretones o hematomas.
			<br><br>
			<b>Riesgos generales </b><br>
            La aplicación intradermoterapia con enzimas consiste en la administración de un cocktail personalizado por tu médico constituido por Enzimas (colagenasa, lipasa o hialuronidasa) ,solución fisiológica 0.9% estétil y lidocaína simple baja dosis .
			La técnica esta contraindicada durante el embarazo y lactancia.
			Existe una posibilidad, aunque mínima, de que aparezca pigmentación en la zona de tratamiento sobre todo si hay exposición solar o a lámparas UV después del mismo.
			La posibilidad de hematomas es un riesgo frecuente que depende también de la toma de ciertos medicamentos por parte del paciente (p.ej. aspirina y otros anticoagulantes). Informe al médico de la toma de cualquier medicación antes de ser sometido a un tratamiento con la técnica mesoterápica.
			Hasta pasados 3-4 días de realizado el tratamiento no debe acudir a saunas o piscinas para evitar la contaminación de los puntos de puntura.
			<br><br>
			<b>Riesgos personales</b><br>
			La intradermoterapia:Consiste en la inyección intradérmica de medicamentos a pequeñas dosis. La técnica consigue aumentar el efecto de los medicamentos administrados por lo que las posibilidades de producir efectos secundarios son muy reducidas aunque no se pueden descartar completamente.
			El número de sesiones y la frecuencia de aplicación de las mismas es variable en función de la patología a tratar y de la idiosincrasia de cada paciente por lo que no puede ser determinado de antemano.
			Los resultados se obtienen con mayor efectividad si el tratamiento realizado se complementa con otros tratamientos que potenciarán sus efectos. (RADIOFRECUENCIA O ULTRASONIDO )
			<br><br>
            <b>Autorización</b>
			He sido informado y he entendido que existen riesgos aunque éstos sean mínimos. Si surge alguna complicación doy mi consentimiento para que se haga lo que sea más conveniente.
			He sido informado de que la posibilidad de que surjan reacciones alérgicas o de hipersensibilidad es la misma que si se emplease cualquier otra vía de administración de fármacos.
            He sido informado de que la aparición de hematomas es más frecuente por esta vía de administración de fármacos que por otras vías.
            <br>
            Me han informado y he entendido plenamente los posibles riesgos.
            <br>
            Me han informado del derecho que tengo de aceptar o rechazar el procedimiento así como también del derecho de anular la aceptación previa, de las posibilidades de éxito del tratamiento y he podido preguntar lo que me ha parecido conveniente al respecto.
            <br>
            Sé que puedo exponer condiciones particulares o reservas referentes al citado procedimiento.
            Me han realizado las exploraciones necesarias y se me ha dado la información sobre el tratamiento y la forma de realizarlo que he solicitado.
            <br>
            Autorizo al servicio citado y al equipo asistencial a realizar la administración de los fármacos necesarios para mi tratamiento mediante la técnica intradermoterápica asumiendo las consecuencias y los riesgos más frecuentes citados. 
            <br>
            Todo lo anterior se me ha explicado claramente y acepto el tratamiento propuesto siendo consciente de las posibilidades de éxito y de las posibles complicaciones por lo que firmo en señal de acuerdo, de aceptación y entendimiento de este consentimiento.
			<div style="text-align: center">
			<input type="hidden" id="base_url" value="<?php echo base_url(); ?>" readonly>
            <input type="hidden" id="idpaciente" value="<?php echo $paciente->idpaciente ?>">
            <input type="hidden" id="tipo_aviso" value="<?php echo $tipo_aviso ?>">
            <?php if($iddocumento==0){ ?>
	            <?php if($tipo==0){ ?>
					<div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div>
	            <?php } else{?>
	            	<div class="firma_text" id="aceptance">
						<div id="signature" class="signature col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
							<label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma en el recuadro, su mouse o su dedo</label><br>
							<canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
							<img src="<?php echo base_url() ?>public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignature" data-signature="patientSignature">
							<br>
							<div class="row" >
								<button class="btn_estilo" id="btn_firma_save" onclick="saveex_paciente()">Aceptar Firma</button>
							</div>				
						</div>			
					</div>
	            <?php }?>
            <?php }else{?>
				    <div style="text-align:center;margin-bottom:10px;border-radius:4px;">
				        <?php 
				        $result_get=$this->General_model->get_record('iddocumento',$iddocumento,'documentos_legales');
				        $fh = fopen(base_url()."uploads/paciente_firma/".$result_get->firma, 'r') or die("Se produjo un error al abrir el archivo");
				        $linea = fgets($fh);
				        fclose($fh);  
				        ?>
				        <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
				            <img src="<?php echo $linea ?>" width="170" height="75" style="border:dotted 1px black;">
				        </div> 
				    </div>                 
             <?php }?>
				<div style="font-size: 15px">FIRMA DEL PACIENTE Y FECHA 
				</div>	
			    <div style="font-size: 16px">
			    <?php echo $paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno ?>		
		        </div> 
            </div>
            
            <br>
            # 
            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:5cm;border-bottom:1px solid black;"></div> FRASCOS Enzima(s) 
            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:2cm;border-bottom:1px solid black;"></div> 
            <br>
            Precio $
            <div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:5cm;border-bottom:1px solid black;"></div>
            <br>DRA KARLA COELLO VÁZQUEZ  8435100 	
            <br>
        </div>	
    </body>
</html>