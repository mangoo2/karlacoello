<link href="<?php echo base_url(); ?>public/css/select2.min.css" rel="stylesheet" />
<!--///-->
<link href="<?php echo base_url() ?>assets/node_modules/morrisjs/morris.css" rel="stylesheet">
<script src="<?php echo base_url() ?>assets/node_modules/echarts/echarts-all.js"></script>
<script src="<?php echo base_url() ?>assets/node_modules/morrisjs/morris.js"></script>
<!--///-->
<script src="<?php echo base_url();?>assets/node_modules/summernote/dist/summernote-bs4.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/paciente/canvas.js"></script> 
<script src="<?php echo base_url();?>public/js/speechRecon.js"></script>
<script src="<?php echo base_url();?>public/js/paciente/paciente.js?v=20201109"></script>
<script src="<?php echo base_url();?>ckfinder/ckfinder.js"></script>
<script src="<?php echo base_url();?>public/js/jquery.textarea_autosize.min.js"></script>
<script type="text/javascript" src='<?php echo base_url(); ?>public/ckeditor/ckeditor.js'></script>
<script type="text/javascript" src='<?php echo base_url(); ?>public/ckeditor/adapters/jquery.js'></script>
<script src="<?php echo base_url(); ?>public/js/select2.full.min.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url();?>public/js/paciente/paciente_files.js?v=20201022"></script>-->

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/paciente/paciente_consultas.js"></script> 
<script type="text/javascript">
	function archivos(){
		CKFinder.widget( 'ckfinder-widget', {
			width: '100%',
			height: 700
		} );
	} 
	function grafica(){
        <?php
            $result_nutri = $this->ModelCatalogos->get_grafica_nutricion($idpaciente);
        ?>
        setTimeout(function() {
            var consultam = echarts.init(document.getElementById('bar-chartm'));
            optionm = {
                tooltip : {
                    trigger: 'axis'
                },
                toolbox: {
                    show : true,
                    feature : {
                        
                        magicType : {show: true, type: ['line', 'bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                color: ["#55ce63", "#009efb"],
                calculable : true,
                xAxis : [
                    {
                        type : 'category',
                        data : [
                        <?php   
                            foreach ($result_nutri as $item) { ?>
                              "<?php echo date('d/m/Y',strtotime($item->consultafecha)) ?>",
                            <?php }?>
                        ]    
                        ///data : ['18/07/2020','18/07/2020','18/07/2020','18/07/2020','18/07/2020','18/07/2020','18/07/2020']

                    }
                ],
                yAxis : [
                    {
                        type : 'value'
                    }
                ],
                series : [
                    {
                        name:'Peso',
                        type:'line',
                        data : [
                        <?php   
                            foreach ($result_nutri as $item) { ?>
                              <?php echo $item->peso ?>,
                            <?php }?>
                        ], 
                        //data:[2.0, 4.9, 14.0, 233.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3],
                        markPoint : {
                            data : [
                                {type : 'max', name: 'Máximo'},
                                {type : 'min', name: 'Mínimo'}
                            ]
                        }

                    },
                    {
                        name:'IMC',
                        type:'line',
                        data : [
                        <?php   
                            foreach ($result_nutri as $item) { ?>
                              <?php echo $item->imc ?>,
                            <?php }?>
                        ], 
                        //data:[2.0, 4.9, 14.0, 233.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3],
                        markPoint : {
                            data : [
                                {type : 'max', name: 'Máximo'},
                                {type : 'min', name: 'Mínimo'}
                            ]
                        }
                        
                    },
                ]
            };

            consultam.setOption(optionm, true), $(function() {
                function resize() {
                    setTimeout(function() {
                        consultam.resize();
                    }, 100);
                }
                $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
            });
        }, 1000);
    }
</script>