<style type="text/css">
    .color_link{
        color: #779155 !important;
    }
    .list-group-item.active {
        z-index: 2;
        color: #fff !important;
        background-color: #779155;
        border-color: #779155;
    }
</style>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Manual Operativo</h3>
                                        <hr style="border-bottom: solid; color: #779155;">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-xlg-2 col-md-4">
                                        <div class="stickyside">
                                            <div class="list-group" id="top-menu">
                                                <a href="#1" class="list-group-item color_link">Agenda</a>
                                                <a href="#2" class="list-group-item color_link">Lista de Pacientes</a>
                                                <a href="#3" class="list-group-item color_link">Empleados</a>
                                                <a href="#4" class="list-group-item color_link">Estadisticas</a>
                                                <a href="#5" class="list-group-item color_link">Productos</a>
                                                <a href="#6" class="list-group-item color_link">Proveedores</a>
                                                <a href="#7" class="list-group-item color_link">Servicios</a>
                                                <a href="#8" class="list-group-item color_link">Configuracion</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-xlg-10 col-md-8">
                                        <div class="card">
                                            <div class="card-body">
                                            <!-- Agenda -->    
                                                <h4 class="card-title" id="1">Agenda</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.</p>
                                                <p> enean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,</p>
                                            <!-- Lista de Pacientes -->    
                                                <h4 class="card-title m-t-40" id="2">Lista de Pacientes</h4>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item"> 
                                                        <a class="nav-link active color_link" data-toggle="tab" href="#list_agenda" role="tab"><span class="hidden-xs-down">Listado de pacientes</span></a> 
                                                    </li>
                                                    <li class="nav-item"> 
                                                        <a class="nav-link color_link" data-toggle="tab" href="#consultas" role="tab"><span class="hidden-xs-down">Espacios de trabajo</span></a> 
                                                    </li>
                                                </ul>
                                                <div class="tab-content tabcontent-border">
                                                    <div class="tab-pane active  p-20" id="list_agenda" role="tabpanel">
                                                        <h4>Agenda</h4>
                                                    </div>
                                                    <div class="tab-pane p-20" id="consultas" role="tabpanel">
                                                        <h4>Espacios de trabajo</h4>
                                                    </div>
                                                </div> 
                                            <!-- Empleados -->    
                                                <h4 class="card-title m-t-40" id="3">Empleados</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.</p>
                                                <p> enean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,</p>
                                                <p> enean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,</p>
                                            <!-- Estadisticas -->   
                                                <h4 class="card-title m-t-40" id="4">Estadisticas</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.</p>
                                                <p> enean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,</p>
                                                <p> enean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,</p>
                                            <!-- Productos -->
                                                <h4 class="card-title m-t-40" id="5">Productos</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.</p>
                                                <p> enean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,</p>
                                                <p> enean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,</p>
                                            <!-- Proveedores -->
                                                <h4 class="card-title m-t-40" id="6">Proveedores</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.</p>
                                                <p> enean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,</p>
                                                <p> enean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,</p>
                                            <!-- Servicios -->
                                                <h4 class="card-title m-t-40" id="7">Servicios</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.</p>
                                                <p> enean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,</p>
                                                <p> enean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,</p>
                                            <!-- Configuracion -->
                                                <h4 class="card-title m-t-40" id="8">Configuraciones</h4>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item"> 
                                                        <a class="nav-link active color_link" data-toggle="tab" href="#agenda" role="tab"><span class="hidden-xs-down">Agenda</span></a> 
                                                    </li>
                                                    <li class="nav-item"> 
                                                        <a class="nav-link color_link" data-toggle="tab" href="#espacios" role="tab"><span class="hidden-xs-down">Espacios de trabajo</span></a> 
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link color_link" data-toggle="tab" href="#gruposalimen" id="showdttable" role="tab"><span class="hidden-xs-down">Grupos Alimenticios</span></a> 
                                                    </li>
                                                    <li class="nav-item "> 
                                                        <a class="nav-link color_link" data-toggle="tab" href="#encuesta_calidad" role="tab"><span class="hidden-xs-down">Encuesta de calidad</span></a> 
                                                    </li>
                                                    <li class="nav-item "> 
                                                        <a class="nav-link color_link" data-toggle="tab" href="#panel_usuarios" role="tab"><span class="hidden-xs-down">Usuarios</span></a> 
                                                    </li> 
                                                    <li class="nav-item"> 
                                                        <a class="nav-link color_link" data-toggle="tab" href="#panel_perfiles" role="tab"><span class="hidden-xs-down">Perfiles</span></a> 
                                                    </li> 
                                                </ul>
                                                <div class="tab-content tabcontent-border">
                                                    <div class="tab-pane active  p-20" id="agenda" role="tabpanel">
                                                        <h4>Agenda</h4>
                                                    </div>
                                                    <div class="tab-pane p-20" id="espacios" role="tabpanel">
                                                        <h4>Espacios de trabajo</h4>
                                                    </div>
                                                    <div class="tab-pane p-20" id="gruposalimen" role="tabpanel">
                                                        <h4>Grupos Alimenticios</h4>
                                                    </div>
                                                    <div class="tab-pane p-20" id="encuesta_calidad" role="tabpanel">
                                                        <h4>Encuesta de calidad</h4>
                                                    </div>
                                                    <div class="tab-pane p-20" id="panel_usuarios" role="tabpanel">
                                                        <h4>Usuarios</h4>
                                                    </div>
                                                    <div class="tab-pane p-20" id="panel_perfiles" role="tabpanel">
                                                        <h4>Perfiles</h4>
                                                    </div>
                                                </div> 
                                            <!-- -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>    
</div>  
      
