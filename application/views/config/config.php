<style type="text/css">
.card_borde{padding: 0.3rem 1.25rem; background-color: rgba(0,0,0,.03);}
.margen_centro{margin: auto;}
</style>
<link href="<?php echo base_url(); ?>dist/css/pages/stylish-tooltip.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/config.css">
<div class="page-wrapper">
<div class="container-fluid">
   <div class="row">
       <div class="col-md-12"><br>
           <div class="card">
               <div class="row">
                   <div class="col-lg-12">
                       <div class="card-body">
                           <!-------------->
                               <ul class="nav nav-tabs" role="tablist">
                                   <!--<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab"><span class="hidden-xs-down">Perfil</span></a> </li>-->
                                   <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#agenda" role="tab"><span class="hidden-xs-down">Agenda</span></a> </li>
                                   <!--
                                   <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#ticket" role="tab"><span class="hidden-xs-down">Ticket</span></a> </li>
                                   -->
                                   <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#espacios" role="tab"><span class="hidden-xs-down">Espacios de trabajo</span></a> </li>
                                   <!--
                                   <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#equipos" role="tab"><span class="hidden-xs-down">Equipos</span></a> </li>
                                   -->
                                   <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#gruposalimen" id="showdttable" role="tab"><span class="hidden-xs-down">Grupos Alimenticios</span></a> </li>

                                   <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#encuesta_calidad" role="tab"><span class="hidden-xs-down">Encuesta de calidad</span></a> </li>
                                   <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#panel_usuarios" role="tab" onclick="usuario_clean()"><span class="hidden-xs-down">Usuarios</span></a> </li> 
                                   <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#panel_perfiles" role="tab"><span class="hidden-xs-down">Perfiles</span></a> </li> 
                               </ul>
                               <!-- Tab panes -->
                               <div class="tab-content tabcontent-border">
                                   <!--<div class="tab-pane active" id="home" role="tabpanel">
                                       <div class="p-20">
                                           
                                       </div>
                                   </div>-->
                                   <div class="tab-pane active  p-20" id="agenda" role="tabpanel">
                                       <!----------------------------------------->
                                           <div class="row">
                                               <div class="col-md-12" style="text-align: center;">
                                                   <h4 class="title">Configuración de la agenda</h4>
                                               </div>
                                           </div>
                                           <div class="row">
                                               <div class="col-md-3"></div>
                                               <div class="col-md-6">
                                                   <div class="panel panel-default">
                                                       <div class="panel-heading">Configuración de días y horarios laborables</div>
                                                       <div class="panel-body">
                                                           <div class="col-md-12">
                                                               <i class="fas fa-clock text-blue" style="font-size:16px;"></i><span class="text-black1 text-md"> Horario</span>
                                                               <p class="text-xs text-gray">Estos horarios se verán reflejados en tu agenda</p>
                                                           </div>
                                                           <div class="col-md-12">
                                                               <div class="row">
                                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                       <div class=" custom-checkbox">
                                                                           <input type="checkbox" class="" id="horario_1" onclick="horario(1)">
                                                                           <label class="" for="horario_1">Lunes</label>
                                                                       </div>
                                                                   </div>
                                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                       <input type="time" value="" class="form-control" id="horario_i_1" onchange="horario(1)">
                                                                   </div>
                                                                   <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                                                       --
                                                                   </div>
                                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                       <input type="time" value="" class="form-control" id="horario_f_1" onchange="horario(1)">
                                                                   </div>
                                                               </div>
                                                               <div class="row">
                                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                       <div class=" custom-checkbox">
                                                                           <input type="checkbox" class="" id="horario_2" onclick="horario(2)">
                                                                           <label class="" for="horario_2" >Martes</label>
                                                                       </div>
                                                                   </div>
                                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                       <input type="time" value="" class="form-control" id="horario_i_2" onchange="horario(2)">
                                                                   </div>
                                                                   <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                                                       --
                                                                   </div>
                                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                       <input type="time" value="" class="form-control" id="horario_f_2" onchange="horario(2)">
                                                                   </div>
                                                               </div>
                                                               <div class="row">
                                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                       <div class=" custom-checkbox">
                                                                           <input type="checkbox" class="" id="horario_3" onclick="horario(3)">
                                                                           <label class="" for="horario_3" >Miercoles</label>
                                                                       </div>
                                                                   </div>
                                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                       <input type="time" value="" class="form-control" id="horario_i_3" onchange="horario(3)">
                                                                   </div>
                                                                   <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                                                       --
                                                                   </div>
                                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                       <input type="time" value="" class="form-control" id="horario_f_3" onchange="horario(3)">
                                                                   </div>
                                                               </div>
                                                               <div class="row">
                                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                       <div class=" custom-checkbox">
                                                                           <input type="checkbox" class="" id="horario_4" onclick="horario(4)">
                                                                           <label class="" for="horario_4" >Jueves</label>
                                                                       </div>
                                                                   </div>
                                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                       <input type="time" value="" class="form-control" id="horario_i_4" onchange="horario(4)">
                                                                   </div>
                                                                   <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                                                       --
                                                                   </div>
                                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                       <input type="time" value="" class="form-control" id="horario_f_4" onchange="horario(4)">
                                                                   </div>
                                                               </div>
                                                               <div class="row">
                                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                       <div class=" custom-checkbox">
                                                                           <input type="checkbox" class="" id="horario_5" onclick="horario(5)">
                                                                           <label class="" for="horario_5" >Viernes</label>
                                                                       </div>
                                                                   </div>
                                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                       <input type="time" value="" class="form-control" id="horario_i_5" onchange="horario(5)">
                                                                   </div>
                                                                   <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                                                       --
                                                                   </div>
                                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                       <input type="time" value="" class="form-control" id="horario_f_5" onchange="horario(5)">
                                                                   </div>
                                                               </div>
                                                               <div class="row">
                                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                       <div class=" custom-checkbox">
                                                                           <input type="checkbox" class="" id="horario_6" onclick="horario(6)">
                                                                           <label class="" for="horario_6"> Sabado</label>
                                                                       </div>
                                                                   </div>
                                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                       <input type="time" value="" class="form-control" id="horario_i_6" onchange="horario(6)">
                                                                   </div>
                                                                   <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                                                       --
                                                                   </div>
                                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                       <input type="time" value="" class="form-control" id="horario_f_6" onchange="horario(6)">
                                                                   </div>
                                                               </div>
                                                               <div class="row">
                                                                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                                       <div class=" custom-checkbox">
                                                                           <input type="checkbox" class="" id="horario_7" onclick="horario(7)">
                                                                           <label class="" for="horario_7" >Domingo</label>
                                                                       </div>
                                                                   </div>
                                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                       <input type="time" value="" class="form-control" id="horario_i_7" onchange="horario(7)">
                                                                   </div>
                                                                   <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                                                       --
                                                                   </div>
                                                                   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                       <input type="time" value="" class="form-control" id="horario_f_7" onchange="horario(7)">
                                                                   </div>
                                                               </div>
                                                               <div class="row">
                                                                   <hr>
                                                               </div>
                                                               <div class="row" style="border-top: 1px solid rgb(226 231 236);">
                                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                       <i class="fas fa-calendar-times text-blue" style="font-size:16px;"></i> <span class="marginBottom-s text-black1 text-md">Horario no disponible</span>
                                                                       <p class="text-xs text-gray">Agregue horarios no disponibles a tu agenda, para impedir que se programen citas durante ese periodo. </p>
                                                                   </div>
                                                                   <div class="col-md-12">
                                                                       <table class="table table-hover" id="table-h-nodisponible">
                                                                           <thead>
                                                                               <tr>
                                                                                   <th>Días</th>
                                                                                   <th>Inicio</th>
                                                                                   <th>Fin</th>
                                                                                   <th></th>
                                                                               </tr>
                                                                           </thead>
                                                                           <tbody class="table-h-nodisponibletbody">
                                                                               
                                                                           </tbody>
                                                                       </table>
                                                                   </div>
                                                               </div>
                                                               <div class="row">
                                                                   <div class="col-md-6">
                                                                       
                                                                   </div>
                                                                   <div class="col-md-6">
                                                                       <button type="button" class="btn waves-effect waves-light btn-block btn-success addhorarionodisponible">Agregar horario no disponible</button>

                                                                   </div>
                                                               </div>

                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                               <?php /*
                                               <div class="col-md-6">
                                                   <div class="panel panel-default">
                                                       <div class="panel-heading">Otras configuraciones de la agenda</div>
                                                       <div class="panel-body">
                                                           <div class="row">
                                                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                   <i class="fas fa-stopwatch text-blue" style="font-size:16px;"></i> <span class="panel-title text-black1 text-md">Duración de visita médica</span>
                                                                   <p class=" text-xs text-gray">Agregue la duración de tus citas (hh:mm)</p>
                                                               </div>
                                                           </div>
                                                           <?php foreach ($duracion_m as $item) { ?>
                                                               <div class="row form-group">
                                                                   <div class="col-md-6">
                                                                       <input type="checkbox"  
                                                                               id="otra_config_<?php echo $item->id;?>" 
                                                                               data-otra="<?php echo $item->id;?>" 
                                                                               onchange="update_dvm(<?php echo $item->id;?>)"
                                                                               <?php if($item->status==1){ echo 'checked';} ?>

                                                                       >
                                                                       <label for="otra_config_<?php echo $item->id;?>" ><?php echo $item->titulo;?>
                                                                           <?php if ($item->descripcion!=null) { ?>
                                                                                   <span class="mytooltip tooltip-effect-4">
                                                                                       <span class="tooltip-item"><i class="fas fa-question-circle"></i></span> <span class="tooltip-content clearfix">
                                                                                         <img src="<?php echo base_url();?>images/FAV.png" style="background: white;">
                                                                                         <span class="tooltip-text"><?php echo $item->descripcion;?></span> </span>
                                                                                   </span>
                                                                           <?php } ?>
                                                                       </label>
                                                                   </div>
                                                                   <div class="col-md-6 clockpicker ">
                                                                       <input type="text" id="tiempo_<?php echo $item->id;?>" min="00:00" max="11:00" class="form-control" value="<?php echo $item->tiempo;?>" onchange="update_dvm(<?php echo $item->id;?>)">
                                                                   </div>
                                                               </div>
                                                           
                                                           <?php } ?>
                                                           <div class="row" style="border-top: 1px solid rgb(226 231 236);">
                                                               <div class="col-md-8 ">
                                                                   <i class="fas fa-sms text-blue" style="font-size:16px;"></i> <span class="text-black1 text-md">Activar SMS <span class="text-sm">recordatorio el día antes de la cita</span></span>
                                                                   <p class="text-xs text-gray">Esta función envía un recordatorio vía SMS a los pacientes agendados un día antes de su cita.</p>
                                                               </div>
                                                               <div class="col-md-4">
                                                                   <div class="custom-control custom-switch">
                                                                     <input type="checkbox" class="custom-control-input" id="sms" onchange="configsms()">
                                                                     <label class="custom-control-label" for="sms"></label>
                                                                   </div>
                                                               </div>
                                                           </div>
                                                           <div class="row" style="border-top: 1px solid rgb(226 231 236);">
                                                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                   <i class="fas fa-laptop text-blue"></i> <span class="text-black1 text-md">Mi página de inicio es...</span>
                                                                   <p class=" text-xs text-gray">Seleccione la página que mostrará Unne cuando inicie sesión.</p>
                                                               </div>
                                                               <div class="col-md-12">
                                                                   <select class="form-control" id="pag_inicio" onchange="configinicio()">
                                                                       <option value="Inicio">Listado de Pacientes</option>
                                                                       <option value="Agenda">Agenda</option>
                                                                   </select>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                               */
                                               ?>
                                           </div>
                                       <!----------------------------------------->
                                   </div>
                                   <div class="tab-pane p-20" id="ticket" role="tabpanel">
                                       <p></p>
                                   </div> 
                                   <div class="tab-pane p-20" id="espacios" role="tabpanel">
                                       <!--
                                       <div class="row">
                                           <div class="col-md-12" style="text-align: center;">
                                               <h4 class="title">Datos Generales</h4>
                                           </div>
                                       </div>
                                       -->
                                       <div class="row">
                                           <div class="col-md-12">
                                               <div class="panel panel-default">
                                                   <div class="panel-heading">Datos Generales</div>
                                                   <div class="panel-body">
                                                       <div class="row">
                                                           <div class="col-md-5">
                                                               <div class="row">
                                                                   <div class="col-md-10">
                                                                       <form class="form" method="post" role="form" id="form_registro_espacio">
                                                                           <div class="row">
                                                                               <div class="col-md-12">
                                                                                   <div class="form-group">
                                                                                       <label>Espacio de trabajo</label>
                                                                                       <input type="hidden" name="idespacio" id="idespacio_trabajo" value="0" class="form-control">
                                                                                       <input type="text" name="nombre" id="nombre_trabajo" class="form-control">
                                                                                   </div>
                                                                               </div>
                                                                           </div>  
                                                                           <div class="row">
                                                                               <div class="col-md-12">
                                                                                       <label>Selecciona color</label>
                                                                               </div>
                                                                               <div class="div_color">
                                                                                   <div class="col-md-4">
                                                                                       <div class="form-group">
                                                                                           <input type="color" name="color" id="color_trabajo" class="form-control">
                                                                                       </div>
                                                                                   </div>
                                                                               </div>    
                                                                           </div>  
                                                                       </form>    
                                                                   </div>
                                                               </div>
                                                               <hr>
                                                               <div class="row">
                                                                   <div class="col-md-12">
                                                                       <button type="button" class="btn waves-effect waves-light btn_sistema" onclick="guarda_registro_espacio()"><i class="fas fa-save"></i> Guardar</button>
                                                                   </div>    
                                                               </div>
                                                           </div>        
                                                              
                                                           <div class="col-md-7">
                                                               <table class="table" id="table_datos_espacio">
                                                                   <thead class="bg-blue">
                                                                       <tr>
                                                                           <th>Nombre</th>
                                                                           <th>Color</th>
                                                                           <th></th>
                                                                       </tr>
                                                                   </thead>
                                                                   <tbody>
                                                                   </tbody>
                                                               </table>
                                                           </div>
                                                       </div>    
                                                   </div>
                                               </div>
                                           </div>
                                       </div>                    

                                   </div>

                                   <div class="tab-pane p-20" id="gruposalimen" role="tabpanel">
                                    <div class="row">
                                      <div class="col-md-12" style="text-align: center;">
                                         <h4 class="title">Grupos alimenticios</h4>
                                      </div>
                                      <div class="col-md-12">
                                        <table class="table" id="GruposAlimenticios">
                                          <thead>
                                          <th>Grupo De Alimento</th>
                                          <th>Subgrupo</th>
                                          <th>Energia</th>
                                          <th>Proteina</th>
                                          <th>Lipidos</th>
                                          <th>Carboidratos</th>
                                          <th>Sodio</th>
                                          <th>Fibra</th>
                                          <th>Conteo</th>
                                          </thead>
                                        </table>
                                      </div>
                                    </div>
                                   </div>
                                   <div class="tab-pane p-20" id="equipos" role="tabpanel">
                                       <p></p>
                                   </div>    
                                   <div class="tab-pane p-20" id="encuesta_calidad" role="tabpanel">
                                       <p>Encuesta</p>
                                   </div> 
                                   <div class="tab-pane p-20" id="panel_usuarios" role="tabpanel">
                                      <div class="row">
                                        <div class="col-lg-8">
                                          <h4 align="center">Listado de usuarios</h4> 
                                          <table class="table" id="table_datos_usurios">
                                              <thead class="bg-blue">
                                                  <tr>
                                                      <th>Foto</th>
                                                      <th>Personal</th> 
                                                      <th>Usuario</th>
                                                      <th>Perfil</th>
                                                      <th></th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                              </tbody>
                                          </table>
                                        </div>
                                        <div class="col-lg-4" style="border-style: solid;
                                            border-width: 1px;
                                            border-radius: 12px;
                                            border-color: #e5e6e5;">
                                          <h4 align="center"><span class="txt_edit"></span> Usuario</h4> 
                                          <form class="form" method="post" role="form" id="form_usuario">
                                            <input type="hidden" name="UsuarioID" id="UsuarioID" value="0"> 
                                            <div class="txt_usu">
                                              <div class="row">
                                                  <div class="col-md-12">
                                                      <div class="form-group">
                                                          <label>Nombre de usuario <div style="color: red" class="txt_usuario"></div></label>
                                                          <input type="text" name="usuario" id="usuario" class="form-control">
                                                      </div>
                                                  </div>
                                              </div>
                                            </div>  
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Contraseña</label>
                                                        <input type="password" name="contrasena" id="contrasena" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Verificar contraseña</label>
                                                        <input type="password" name="contrasena2" id="contrasena2" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Personal</label>
                                                        <?php
                                                          $result=$this->General_model->getselectwhere('personal','estatus',1);
                                                        ?>
                                                        <select name="personalId" id="personalId" class="form-control">
                                                          <option disabled="" value="0" selected="">Seleccione</option>
                                                        <?php foreach ($result as $item) { ?>
                                                          <option value="<?php echo $item->personalId ?>"><?php echo $item->nombre ?></option>
                                                        <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="text_perfil"></div>
                                                </div>
                                            </div>   
                                          </form>   
                                          <div class="row">
                                            <div class="col-lg-12">
                                              <div class="button-group" align="center"><span class="btn_cancelar_usuario"></span>
                                                  <button type="button" class="btn waves-effect waves-light btn-rounded" style="color: #779155;border-color: #779155;" onclick="guardar_usuario()"><i class="fas fa-save"></i> Guardar</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                   </div>      
                                   <div class="tab-pane p-20" id="panel_perfiles" role="tabpanel">
                                      <h4><span class="txt_edit_perfil"></span> Perfil</h4> 
                                      <div class="row">
                                          <div class="col-md-4">
                                            <form class="form" method="post" role="form" id="form_registro_perfil">
                                              <div class="form-group">
                                                  <label>Nombre de perfil <div style="color: red" class="txt_perfil_aux"></div></label>
                                                  <input type="hidden" name="perfilId" id="perfilId_aux" value="0" class="form-control">
                                                  <input type="text" name="nombre" id="perfil_aux" oninput="verificar_perfil()" class="form-control">
                                              </div>
                                            </form>  
                                          </div>
                                          <div class="col-md-4">
                                              <label><spa style="color:#ff000000;">.</spa></label><br>
                                              <span class="btn_cancelar_perfil"></span>
                                              <button type="button" class="btn waves-effect waves-light btn-rounded btn_perfil_alta" style="color: #779155;border-color: #779155;" onclick="guardar_perfil()"><i class="fas fa-save"></i> Guardar</button>
                                          </div>  
                                      </div> 
                                      <hr> 
                                      <div class="row">
                                        <div class="col-lg-4">
                                          <h4 align="center">Listado de perfiles</h4> 
                                          <table class="table" id="table_datos_perfiles">
                                              <thead class="bg-blue">
                                                  <tr>
                                                      <th>Perfil</th>
                                                      <th></th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                              </tbody>
                                          </table>
                                        </div>
                                        <div class="col-lg-8">
                                          <div class="row">
                                            <div class="col-lg-12">
                                              <h4 align="center">Permisos de acceso</h4>
                                              <div class="txt_perfil_tabla"></div>
                                            </div>
                                          </div>
                                          <div class="permisos_acceso"></div>  
                                        </div>  
                                      </div> 
                                   </div>    
                               </div>
                           <!-------------->
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>    
</div>      

<div id="elimina_registro_modal_espacio" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
   <div class="modal-content">
       <div class="modal-header">
           <div align="center">
               <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
           </div>
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
       </div>
       <div class="modal-body">
           <h5>¿Está seguro que desea eliminar éste espacio de trabajo? Esta operación no se puede deshacer.</h5>
       </div>
       <input type="hidden" id="id_espacio">
       <div class="modal-footer">
           <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
           <button type="button" class="btn btn-danger waves-effect" onclick="delete_registro_espacio()">Aceptar</button>
       </div>
   </div>   
</div>
</div> 
  
 
<div id="elimina_usuario_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste usuario?<br>Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_usuario">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_usuario()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>

<div id="elimina_perfil_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste perfil?<br>Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="idperfile">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_perfil()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>

<div id="elimina_perfil_detalle_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste menú?<br>Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="idperfile_detalle">
            <input type="hidden" id="idperfile_e">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_detalle_perfil()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>