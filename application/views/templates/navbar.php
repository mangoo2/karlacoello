<?php
$color='#779155';


if (!$this->session->userdata('logeado')) {
    redirect('Login');
    $perfilid =0;
}else{
    $perfilid=$this->session->userdata('perfilid');
    $personalId=$this->session->userdata('idpersonal');
    $administrador=$this->session->userdata('administrador');
    $foto=$this->session->userdata('foto');
    $this->fechainicio = date('Y-m-d');
    //$info=$this->Login_model->get_record('personal','personalId',$personalId);
}
if($perfilid!=1){
    $color='#779155';
}

?>
<style type="text/css">

  #sidebarnav>li>a {
    /*width: 200px !important; */
    padding: 0px 12px 0px 15px !important;
}

.sidebar-nav>ul>li>a i {
    width: 35px !important;
    font-size: 30px !important;
    display: inline-block;
    vertical-align: middle;
    color: <?php echo $color ?> !important;
}
.form-group {
    margin-bottom: 5px !important;
}
.vd_red{
    color: red;
}
.vd_green{
    color: green;
}
/* EStilos para adartar el color del logo */
.skin-blue .topbar, .skin-blue-dark .topbar {
    background: <?php echo $color ?> !important;
}
.sidebar-nav ul li a {
    color: <?php echo $color ?> !important;
}
.btn-info{
    background: #779155 !important;
}
.bg-info {
    background-color: <?php echo $color ?> !important;
}
.btn-outline-info {
    color: #779155 !important;
    border-color: #779155 !important;
}
.customtab2 li a.nav-link:hover {
    color: #fff;
    background: <?php echo $color ?> !important;
}
.customtab2 li a.nav-link.active {
    background: <?php echo $color ?> !important;
    color: #fff;
}
.icono-rey{
    color: #779155 !important;
}
.btn-outline-info:hover {
    color: #fff;
    background-color: #AD99A2 !important;
    border-color: #779155 !important;
}
.modal_sistema{
    background: #779155 !important;
    color: #fff !important;
}
.btn_sistema{
    background: #779155 !important;
    color: #fff !important;
}

/* //// */
</style>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">Cargando</p>
    </div>
</div>

<div id="main-wrapper">
    <header class="topbar">
        <nav class="navbar top-navbar navbar-expand-md navbar-dark">
            <div class="navbar-header" style="background-color: white !important;">
                <a class="navbar-brand" href="<?php echo base_url();?>Inicio">
                    <b>
                        <img src="<?php echo base_url();?>images/FAV.png" alt="homepage" class="dark-logo" />
                        <img style="width:50px" src="<?php echo base_url();?>images/FAV.png" alt="homepage" class="light-logo" />
                    </b>
                    <span class="img_unne" style="display: none;">
                     <img src="<?php echo base_url();?>images/Logo.png" alt="homepage" class="dark-logo" />   
                     <img style="width:120px" src="<?php echo base_url();?>images/Logo.png" class="light-logo" alt="homepage" />
                    </span>
                </a>
            </div>
            <div class="navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"> 
                        <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> 
                    </li>
                    <li class="nav-item"> 
                        <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)"><i class="icon-menu"></i></a> 
                    </li>
                    <li class="nav-item dropdown u-pro">
                        <a class="nav-link profile-pic">
                            <?php if($foto!=''){ ?> 
                                <img src="<?php echo base_url();?>assets/images/users/1.jpg" alt="user" class=""> 
                            <?php }?>
                            <span style="font-size: 20px; font-weight: bold;"><?php echo $administrador ?> </span> 
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav my-lg-0">
                    <li class="nav-item"> <a class="nav-link" style="font-size: 20px; font-weight: bold;" href="<?php echo base_url();?>Manual_Operativo">Manual Operativo</a>
                    </li>
                    <li class="nav-item dropdown">
                            <?php 
                                $validar_productos=$this->Login_model->validar_productos_almacen(date("Y-m-d",strtotime($this->fechainicio."+ 7 days")));
                                $aux_existe_producto=0;
                                foreach ($validar_productos as $items){
                                            $aux_existe_producto=1;
                                } 
                                $etiqueta='';
                                if($aux_existe_producto==1){
                                    $etiqueta='<div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>';
                                }else{
                                    $etiqueta='';
                                }
                            ?>
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="ti-email"></i>
                                <?php echo $etiqueta ?>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown">
                                <ul>
                                    <li>
                                        <div class="drop-title">Notificaciones</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            <?php 
                                            $validar_productos=$this->Login_model->validar_productos_almacen(date("Y-m-d",strtotime($this->fechainicio."+ 7 days")));

                                            foreach ($validar_productos as $items) { ?>
                                                <a href="<?php echo base_url() ?>Productos/inventario/<?php echo $items->idalmacen ?>/<?php echo $items->idproducto ?>">
                                                    <div class="btn btn-warning btn-circle"><i class="fas fa-capsules"></i></div>
                                                    <div class="mail-contnet">
                                                        <h5>Producto: <?php echo $items->producto ?></h5> 
                                                        <span class="mail-desc"> Lote: <?php echo $items->lote ?></span> 
                                                        <span class="time">
                                                            <?php
                                                            $etiqueta_caduca='';
                                                            if($this->fechainicio <= $items->fecha_caducidad && date("Y-m-d",strtotime($this->fechainicio."+ 7 days")) >= $items->fecha_caducidad){ 
                                                                $etiqueta_caduca='<span class="label label-warning m-r-10">Próximo a caducar</span>';
                                                            }else if($this->fechainicio > $items->fecha_caducidad){
                                                                $etiqueta_caduca='<span class="label label-danger m-r-10">Lote caducado</span>';
                                                            }else{
                                                                $etiqueta_caduca='<span class="label label-success m-r-10">Vigente</span>';
                                                            }  
                                                            echo $etiqueta_caduca.' '.date("d/m/Y",strtotime($items->fecha_caducidad)); ?>        
                                                        </span> 
                                                    </div>
                                                </a>
                                            <?php } ?>
                                            <!-- Message -->
                                            
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center link" href="javascript:void(0);"> <strong>Cerrar notificaciones</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    <li class="nav-item"> <a class="nav-link" style="font-size: 20px; font-weight: bold;" href="<?php echo base_url();?>Login/logout"><i class="fas fa-sign-out-alt"></i> Salir</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="left-sidebar">
        <div class="scroll-sidebar">
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <?php 
                    $menusub=$this->Login_model->submenus($perfilid,0);
                    foreach ($menusub as $items) { ?>
                        <li> 
                            <a class="waves-effect waves-dark" href="<?php echo base_url();?><?php echo $items->Pagina ?>" aria-expanded="false"><i class="<?php echo $items->Icon ?>"></i> <span class="hide-menu"><?php echo $items->Nombre ?></span></a>
                        </li>
                    <?php } ?>
                </ul>
            </nav>
        </div>
    </aside>
