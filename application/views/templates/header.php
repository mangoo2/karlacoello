<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>images/FAV.png">
    <title>Karla Coello</title>
    <!-- Custom CSS -->
    <style type="text/css">
    .fdf{color: red !important;}
    @font-face {
        font-family: "Helvetica";
        src: url("<?php echo base_url(); ?>public/Helvetica.ttf");
    }
    body{
        font-family: "Helvetica" !important;
        color: #59636C !important;
        font-size: 14px !important;
    }
    </style>
    <link href="<?php echo base_url();?>assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/node_modules/datatables.net-bs4/css/responsive.dataTables.min.css">
    <link href="<?php echo base_url();?>assets/node_modules/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/dist/css/style.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/dist/css/pages/tab-page.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/estilo_css.css" rel="stylesheet">

</head>

<input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
<body class="skin-blue fixed-layout mini-sidebar">