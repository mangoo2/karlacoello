<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h2>Paciente:<?php echo $get_pt->nombre.' '.$get_pt->apll_paterno.' '.$get_pt->apll_materno ?></h2>
                                        <h3>Ventas de producto</h3>

                                    </div> 
                                    <div class="col-lg-12">
                                        <table class="table" id="table_datos">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th>Producto</th> 
                                                    <th>Lote</th>
                                                    <th>Cantidad</th>
                                                    <th>Precio Unitario</th>
                                                    <th>Total</th>
                                                    <th>Fecha Venta/Consulta</th>
                                                    <th>Personal</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                $total_suma=0;
                                                foreach ($get_paciente as $item){ 
                                                    $total_suma+=$item->total;
                                                ?>
                                                <tr>    
                                                    <td><?php echo $item->producto ?></td> 
                                                    <td><?php echo $item->lote ?></td>
                                                    <td><?php echo $item->cantidad ?></td>
                                                    <td><?php echo $item->preciou ?></td>
                                                    <td><?php echo $item->total ?></td>
                                                    <td><?php echo $item->reg ?></td>
                                                    <td><?php echo $item->nombre ?></td>
                                                </tr>
                                                <?php } ?>
                                                <?php foreach ($get_paciente_nu as $item){ 
                                                    $total_suma+=$item->total;
                                                ?>
                                                <tr>                                                        
                                                    <td><?php echo $item->producto ?></td> 
                                                    <td><?php echo $item->lote ?></td>
                                                    <td><?php echo $item->cantidad ?></td>
                                                    <td><?php echo $item->preciou ?></td>
                                                    <td><?php echo $item->total ?></td>
                                                    <td><?php echo $item->reg ?></td>
                                                    <td><?php echo $item->nombre ?></td>
                                                </tr>
                                                <?php } ?>
                                                <?php foreach ($get_paciente_spa as $item){ 
                                                    $total_suma+=$item->total;
                                                ?>
                                                <tr>                                                        
                                                    <td><?php echo $item->producto ?></td> 
                                                    <td><?php echo $item->lote ?></td>
                                                    <td><?php echo $item->cantidad ?></td>
                                                    <td><?php echo $item->preciou ?></td>
                                                    <td><?php echo $item->total ?></td>
                                                    <td><?php echo $item->reg ?></td>
                                                    <td><?php echo $item->nombre ?></td>
                                                </tr>
                                                <?php } ?>
                                                <?php foreach ($get_paciente_nutricion as $item){ 
                                                    $total_suma+=$item->total;
                                                ?>
                                                <tr>                                                        
                                                    <td><?php echo $item->producto ?></td> 
                                                    <td><?php echo $item->lote ?></td>
                                                    <td><?php echo $item->cantidad ?></td>
                                                    <td><?php echo $item->preciou ?></td>
                                                    <td><?php echo $item->total ?></td>
                                                    <td><?php echo $item->reg ?></td>
                                                    <td><?php echo $item->nombre ?></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-lg-11" align="right"><br>
                                        <h3>Total: <span class="total_pro"><?php echo $total_suma ?></span></h3>
                                    </div>    
                                </div>   
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div> 