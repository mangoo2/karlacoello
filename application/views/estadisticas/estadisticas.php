<style type="text/css">
    .customtab2 li a.nav-link.active {
        background: #03a9f3;
        color: #fff;
    }
    .customtab2 li a.nav-link:hover {
        color: #fff;
        background: #03a9f3;
    }
</style>
<div class="page-wrapper">
    <div class="container-fluid">   
        <div class="row">
            <div class="col-md-12"><br>
                <h4 align="center">Estadísticas de la clínica</h4>
                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><i class="fas fa-user" style="color: #03a9f3"></i> <span style="font-size:13px">TOTAL PACIENTES</span></h3>
                                            <p class="text-muted">No. de pacientes:</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter" style="color: #03a9f3"><?php echo $total_pacientes->total ?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><img src="<?php echo base_url() ?>images/iconhoy.png"> <span style="font-size:13px">HOY</span></h3>
                                            <p class="text-muted">Consultas:</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-primary"><?php echo $total_consultashoy->total ?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><img src="<?php echo base_url() ?>images/icons2.png"> <span style="font-size:13px">ÉSTA SEMANA</span></h3>
                                            <p class="text-muted">Consultas:</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-cyan"><?php echo $total_semena->total ?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><img src="<?php echo base_url() ?>images/icons4.png"> <span style="font-size:13px">SEMANA ANTERIOR</span></h3>
                                            <p class="text-muted">Consultas:</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-purple"><?php echo $total_semena_aterior->total ?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h3><img src="<?php echo base_url() ?>images/iconmes.png"> <span style="font-size:15px">MES ACTUAL</span></h3>
                                            <p class="text-muted">Consultas:</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h2 class="counter text-success"><?php echo $totalconsultasmesactual->total ?></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h4 align="center">Registro financiero</h4> 
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Tipo de reporte:</label>
                                    <select class="form-control custom-select" id="tipo_reporte">
                                        <option value="0"></option>
                                        <option value="1">Productos</option>
                                        <option value="2">Nutrición</option>
                                        <option value="3">SPA</option>
                                        <option value="4">Medicina Estética</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">De:</label>
                                    <input type="date" name="fechai" id="fechai" class="form-control" value="<?php echo date("Y-m-d"); ?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">A:</label>
                                    <input type="date" name="fechaf" id="fechaf" class="form-control" value="<?php echo date("Y-m-d"); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12" id="cont_p" style="display: none">
                            <div class="card-body">
                                <div class="row">
                                    <h4>Productos</h4>
                                    <div class="table-responsive m-t-40">
                                        <table class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%" id="table_prods">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Tipo</th>
                                                    <th>Marca</th>
                                                    <th>Lote</th>
                                                    <th>Fecha de venta</th>
                                                    <th>Usuario</th>
                                                    <th>Precio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="6">Total en dinero:</th>
                                                    <th></th>
                                                </tr> 
                                                <tr>
                                                    <th colspan="6">Total de consultas:</th>
                                                    <th id="tot_cons"></th>
                                                </tr>  
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>    
                        </div>
                        <div class="col-lg-12" id="cont_n" style="display: none">
                            <div class="card-body">
                                <div class="row">
                                    <h4>Nutrición</h4>
                                    <div class="table-responsive m-t-40">
                                        <table class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%" id="table_nutri">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th>Fecha de consulta</th>
                                                    <th>Paciente</th>
                                                    <th>Servicio</th>
                                                    <th>Empleado</th>
                                                    <th>Precio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="4">Total en dinero:</th>
                                                    <th></th>
                                                </tr> 
                                                <tr>
                                                    <th colspan="4">Total de consultas:</th>
                                                    <th id="tot_consn"></th>
                                                </tr>  
                                                 
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>    
                        </div>
                        <div class="col-lg-12" id="cont_s" style="display: none">
                            <div class="card-body">
                                <div class="row">
                                    <h4>SPA</h4>
                                    <div class="table-responsive m-t-40">
                                        <table class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%" id="table_spa">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th>Fecha de consulta</th>
                                                    <th>Paciente</th>
                                                    <th>Servicio</th>
                                                    <th>Empleado</th>
                                                    <th>Precio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                               <tr>
                                                    <th colspan="4">Total en dinero:</th>
                                                    <th></th>
                                                </tr> 
                                                <tr>
                                                    <th colspan="4">Total de consultas:</th>
                                                    <th id="tot_conspa"></th>
                                                </tr>  
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>    
                        </div>  
                        <div class="col-lg-12" id="cont_me" style="display: none">
                            <div class="card-body">
                                <div class="row">
                                    <h4>Medicina Estética</h4>
                                    <div class="table-responsive m-t-40">
                                        <table class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%" id="table_med">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th>Fecha de consulta</th>
                                                    <th>Paciente</th>
                                                    <th>Servicio</th>
                                                    <th>Empleado</th>
                                                    <th>Precio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                               <tr>
                                                    <th colspan="4">Total en dinero:</th>
                                                    <th></th>
                                                </tr> 
                                                <tr>
                                                    <th colspan="4">Total de consultas:</th>
                                                    <th id="tot_consnme"></th>
                                                </tr>    
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                            </div>    
                        </div>
                    </div>
                </div>





                <?php /* 
                <div class="card">
                    <div class="card-body p-b-0">
                        <h4 class="card-title" align="center">Estadísticas del consultorio</h4>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs customtab2" role="tablist">
                            <li class="nav-item"> 
                                <a class="nav-link active" data-toggle="tab" href="#Consultas_grafi" role="tab" aria-selected="true">Consultas</a> 
                            </li>
                            <li class="nav-item"> 
                                <a class="nav-link" data-toggle="tab" href="#profile7" role="tab" aria-selected="false">Pacientes</a> 
                            </li>
                            <!--
                            <li class="nav-item"> 
                                <a class="nav-link" data-toggle="tab" href="#cobros" role="tab">Cobros Online</a> 
                            </li>
                            <li class="nav-item"> 
                                <a class="nav-link" data-toggle="tab" href="#messages7" role="tab">SUIVE</a> 
                            </li>
                            <li class="nav-item"> 
                                <a class="nav-link" data-toggle="tab" href="#messages7" role="tab">Hoja diaria</a> 
                            </li>
                            <li class="nav-item"> 
                                <a class="nav-link" data-toggle="tab" href="#messages7" role="tab">Mis Estadísticas</a> 
                            </li>
                            -->
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="Consultas_grafi" role="tabpanel">
                                <div class="row">
                                    <!-- column -->
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title" align="center">Consultas en la semana</h4>
                                                <div id="bar-chart" style="width:100%; height:400px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="card-title" align="center">Consultas en el mes</h4>
                                        <div id="bar-chartm" style="width:100%; height:400px;"></div>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="card-title" align="center">Consultas en el año</h4>
                                        <div id="bar-chartmn" style="width:100%; height:400px;"></div>
                                    </div> 
                                </div>    
                            </div>
                            <div class="tab-pane p-20" id="cobros" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Fecha de búsqueda</label>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="text" name="nombre" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-1" align="center">
                                        <div class="form-group">
                                            <label>a</label>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="text" name="apll_materno" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Filtro por status</label>
                                        <select class="form-control">
                                            <option value="0">Todos</option>
                                            <option value="1">En espera</option>
                                            <option value="2">Cancelado</option>
                                            <option value="3">Pagado</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Nombre del paciente:</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Paciente</th>
                                                        <th>Estatus</th>
                                                        <th>Servicio</th>
                                                        <th>Fecha de servicio</th>
                                                        <th>Fecha de Envío</th>
                                                        <th>Monto</th>
                                                        <th>Evidencia</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane p-20" id="messages7" role="tabpanel">3</div>
                        </div>
                    </div>
                </div>
                */ ?>
            </div>
        </div>
    </div>
</div>            