<style type="text/css">
    .card_borde{padding: 0.3rem 1.25rem; background-color: rgba(0,0,0,.03);}
    .margen_centro{margin: auto;}
    .textleft{text-align: left !important;}

    .portlet.calendar .fc-event .fc-time {
        color: black;
    }
    .fc-time {
        color: black !important;

    }
    .fc-title {
        color: black !important;
        font-size: 9px;
    }
    .txt_hora{
        display : none !important;
    }
    .fc-title{
        font-family: "Helvetica" !important;
        font-size: 11px;
        font-weight: bold;
    }
    .fc-sun { 
        background-color:white; 
    }

    .fc-event{
        padding: 1px;
    }
    .fc .fc-axis{
        width: 33px !important;
    }
    .fc-content{
        white-space: break-spaces !important;
    }
    .fc-time-grid .fc-slats td{
        height: 42px !important;
    }
    .card-body{
        padding: 0.5rem;
    }
    .container-fluid, .container-lg, .container-md, .container-sm, .container-xl{
        padding: 0 20px;
    }
</style>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                
                                <div class="row">
                                    <div class="col-lg-3">
                                        <?php if(isset($_GET['id'])){ ?>
                                        <input type="hidden" id="id_paciente" value="<?php echo $_GET['id'] ?>"> 
                                        <?php }else{ ?>
                                        <input type="hidden" id="id_paciente" value="0"> 
                                        <?php } ?>    
                                        <select class="selectpicker m-b-30 m-r-10" data-style="btn-info" id="tipo_cita_agenda" onchange="tipo_cita()">
                                            <option selected="" disabled="" value="0">Agendar Cita</option>
                                            <option value="1">Paciente de primera vez</option>
                                            <option value="2">Paciente subsecuente</option>
                                            <option value="3">Personal</option>
                                            <option value="4">Congreso</option>
                                            <option value="5">Vacaciones</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Buscar Paciente" aria-label="Username" aria-describedby="basic-addon1" id="pacientes_buscartxt" oninput="buscar_paciente_txt()">
                                        </div>  
                                        <div style="position: relative;">
                                            <div class="pacientes_buscar_t" style="position: absolute; top: 25px; z-index: 3;">  
                                            </div>
                                        </div>    
                                    </div>
                                    <div class="col-lg-4">
                                        <div align="right">
                                            <!--
                                            <button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-heart"></i> Video Consulta</button>
                                           
                                            <button type="button" class="btn waves-effect waves-light btn-secondary"><i class="fas fa-video"></i> Tutorial</button>
                                             -->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-9" style="padding-left: 5px;">
                                        <div class="card-body b-l calender-sidebar" style="box-shadow: 0px 0px 30px #cccccc;">
                                            <div class="text_consultas_pendientes"></div>
                                            <div id="calendar"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3" >
                                        <div class="t_calendario">
                                            <h6 class="card-header bg-info" align="center" style="color: white">Calendario</h6> 
                                            <div style="box-shadow: 0px 0px 30px #cccccc;">  
                                                <div >
                                                    <div class="datepickerc"></div>
                                                </div>
                                                <hr>
                                                <div align="center">
                                                  <button type="button" class="btn waves-effect waves-light btn-info" onclick="restablecer_calendario()">Ver esta semana</button>  
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                        <div class="tipo_cita_texto">
                                        </div>
                                        <br>
                                        <div class="forma_pago_texto" style="display: none">
                                            <div class="card">
                                                <div class="card_borde  bg-info">
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <h4 class="m-b-0 text-white">Pagos/Cobros Online</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body" style="box-shadow: 0px 0px 30px #cccccc;">
                                                    <!-- -->
                                                    <input type="hidden" id="idcita_p">
                                                    <input type="hidden" id="idpaciente_p">
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <div class="form-group">
                                                                <label><i class="fas fa-calendar-check"></i> Fecha del Pago</label>
                                                                <input type="date" id="fecha_pago_p" class="form-control" value="<?php echo $fecha_reciente ?>">
                                                            </div>
                                                        </div>
                                                    </div> 
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <div class="form-group">
                                                                <label><i class="fas fa-dollar-sign"></i> Monto</label>
                                                                <input type="number" id="monto_p" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div> 
                                                    <br> 
                                                    <h5><i class="far fa-money-bill-alt"></i> Método de pago</h5>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="radio" class="check" id="metodo_pago1" name="metodo_pago_p" value="1" data-radio="iradio_square-red" checked="">
                                                            <label for="metodo_pago1">Efectivo</label>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="radio" class="check" id="metodo_pago2" name="metodo_pago_p" value="2" data-radio="iradio_square-red">
                                                            <label for="metodo_pago2">Tarjeta</label>
                                                        </div>  
                                                        <div class="col-md-4">
                                                            <input type="radio" class="check" id="metodo_pago3" name="metodo_pago_p" value="3" data-radio="iradio_square-red">
                                                            <label for="metodo_pago3">Transferencia</label>
                                                        </div> 
                                                    </div>
                                                    <div class="row">   
                                                        <div class="col-md-4">
                                                            <input type="radio" class="check" id="metodo_pago4" name="metodo_pago_p" value="4" data-radio="iradio_square-red">
                                                            <label for="metodo_pago4">Cortesía</label>
                                                        </div> 
                                                        <div class="col-md-4">
                                                            <input type="radio" class="check" id="metodo_pago5" name="metodo_pago_p" value="5" data-radio="iradio_square-red">
                                                            <label for="metodo_pago5">Cheque</label>
                                                        </div> 
                                                        <div class="col-md-3">
                                                            <input type="radio" class="check" id="metodo_pago6" name="metodo_pago_p" value="6" data-radio="iradio_square-red">
                                                            <label for="metodo_pago6">Otros</label>
                                                        </div> 
                                                    </div> 
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="custom-control custom-switch">
                                                                <input type="checkbox" name="factura_p" id="factura_p" class="custom-control-input">
                                                                <label class="custom-control-label" for="factura_p">Facturado</label>
                                                            </div>
                                                        </div>
                                                    </div>  
                                                    <hr>   
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label><i class="fas fa-copy"></i> Nota</label>
                                                                <input type="text" id="notas_p" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>  
                                                    <hr>   
                                                    <!-- -->
                                                    <div class="paciente_texto_tipo1_e" align="center" style="display:block"><button type="button" class="btn waves-effect waves-light btn-info" onclick="guardar_pago()">Guardar</button></div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="t_calendario">
                                            <h6 class="card-header bg-info" style="color: white">Resumen de citas del día de hoy</h6> 
                                            <div style="box-shadow: 0px 0px 30px #cccccc;">  
                                                <div class="resumen_hoy">
                                                </div>
                                                <br>
                                            </div>
                                            <br>
                                        </div>
                                        <div class="lista_espera_txt">
                                        </div>
                                        <div class="corte_hoy_txt">
                                            <!--<h6 class="card-header bg-info" style="color: white">Corte de Hoy <i class="fas fa-question-circle"></i></h6> 
                                            <div style="box-shadow: 0px 0px 30px #cccccc;">  
                                                <table class="table" id="table_pacientes" RULES="cols">
                                                    <thead class="bg-blue">
                                                        <tr>
                                                            <th colspan="2"><i class="far fa-money-bill-alt"></i> Efectivo: $<?php echo number_format($efectivo,2,'.',',') ?></th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="2"><i class="fas fa-credit-card"></i> Tarjeta: $<?php echo number_format($tarjeta,2,'.',',') ?></th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="2"><i class="fas fa-credit-card"></i> Otros: $<?php echo number_format($monto_otros,2,'.',',') ?></th>
                                                        </tr>
                                                        <tr>
                                                            <th>Total: $<?php echo number_format($total_total,2,'.',',') ?></th>
                                                            <th>Consultas: $<?php echo number_format($monto_consulta,2,'.',',') ?></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>-->
                                        </div> 
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>       
<div id="cancelar_cita_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Desea cancelar la cita? Esta acción no se puede deshacer?</h5>
            </div>
            <input type="hidden" id="idcancelar_cita">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="cancelar_cita()">Cancelar cita</button>
            </div>
        </div>   
    </div>
</div>   
<div id="modal_reenviar_f" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                <h4 class="modal-title">Reenviar cita</h4>
                </div>
                <div class="col-lg-1">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div align="center">
                    <h6>Ingrese el correo del paciente y verifique que sea el correcto</h6>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="far fa-envelope"></i> Correo electrónico:</label>
                            <input type="email" id="correo_pac" class="form-control" id="exampleInputEmail1">
                        </div>
                    </div>    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label><i class="far fa-envelope"></i> Correo electrónico 2:</label>
                            <input type="email" id="correo_pac2" class="form-control" id="exampleInputEmail1">
                        </div>
                    </div>    
                </div>
            </div>
            <input type="hidden" id="idcita_rv">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-info waves-effect" onclick="enviar_formulario()">Enviar</button>
            </div>
        </div>   
    </div>
</div>        
<div id="modal_total_citas_pendientes" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #03a9f3; color: white">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                <h4 class="modal-title">Pacientes por llamar</h4>
                </div>
                <div class="col-lg-1">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="tabla_c_p"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
            </div>
        </div>   
    </div>
</div>  