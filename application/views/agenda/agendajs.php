<link href="<?php echo base_url();?>assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/node_modules/calendar/dist/fullcalendar.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/node_modules/icheck/skins/all.css" rel="stylesheet">
<link href="<?php echo base_url();?>dist/css/pages/form-icheck.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/node_modules/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo base_url();?>assets/node_modules/calendar/dist/fullcalendar.css" rel="stylesheet" />
-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>col/dist/spectrum.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>col/docs/docs.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>col/docs/highlight/styles/default.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>col/docs/highlight/styles/monokai-sublime.css">

<script type="text/javascript" src="<?php echo base_url();?>col/dist/spectrum.js"></script>
<script type='text/javascript' src='<?php echo base_url();?>col/docs/toc.js'></script>
<!-- <script type='text/javascript' src='<?php echo base_url();?>col/docs/docs.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>col/docs/highlight/highlight.pack.js'></script>
-->
<!--
<script src="<?php echo base_url();?>assets/node_modules/calendar/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/node_modules/moment/moment.js"></script>
<script src='<?php echo base_url();?>assets/node_modules/calendar/dist/fullcalendar.js'></script>
<script src='<?php echo base_url();?>assets/node_modules/calendar/dist/locale/es.js'></script>
-->
<!--<script src="<?php echo base_url();?>assets/node_modules/calendar/dist/cal-init.js"></script>-->
<script src="<?php echo base_url();?>assets/node_modules/moment/moment.js"></script>
<script src="<?php echo base_url();?>assets/node_modules/calendar/jquery-ui.min.js"></script>
<script src='<?php echo base_url();?>assets/node_modules/calendar/dist/fullcalendar.js'></script>
<script src='<?php echo base_url();?>assets/node_modules/calendar/dist/locale/es.js'></script>
<script src="<?php echo base_url();?>assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/node_modules/icheck/icheck.min.js"></script>
<script src="<?php echo base_url();?>assets/node_modules/icheck/icheck.init.js"></script>
<script src="<?php echo base_url();?>assets/node_modules/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/js/agenda.js?v=20210105"></script>
<style type="text/css">
	.fc-time-grid .fc-business-container {
    	position: relative;
    	z-index: 1;
	}
	.fc-today{
		background: transparent !important;
	}
</style>
<script type="text/javascript">
	var base_url = $('#base_url').val();
	function calendario(){
    var calendar = $('#calendar').fullCalendar({
        defaultView: 'agendaWeek',
        allDaySlot: false,
        //weekends:true,
        //minTime: "08:00:00",
        //maxTime: "20:00:00",
        slotLabelFormat:"HH:mm",
        defaultView: 'agendaWeek',
        //businessHours:true,
        businessHours:[
        	<?php foreach ($hlaboral as $item) { 
                $diaselect=$item->id;
                if ($item->id==7) {
                    $diaselect=0;
                }
        		if($item->activo==1){ 
        				$resulthoras=$this->General_model->horacioslaborales($item->id);
        				$an=1;
						$anr=0;
						$arrayh=array();
        				foreach ($resulthoras->result() as $hrs) {
        					$arrayh[]=array('hora'=>$hrs->horas,'grupo'=>$anr);
							if($an % 2 == 0){
								$anr++;
							}
							$an++;
        				}
						$group = array();
						foreach($arrayh as $hrs){
							$group[$hrs["grupo"]][] = $hrs['hora'];
						}        			
						foreach($group as $hrs){
        		?>
        			{
					    start: '<?php echo $hrs[0];?>', 
					    end: '<?php echo $hrs[1];?>',
					    dow: [<?php echo $diaselect;?>], 
					 },
	        	<?php
	        		}
	        	}else{
        		?>
        			{
        				start: '24:00', 
					    end: '00:00',
					    dow: [<?php echo $diaselect;?>], 
					},
        	<?php }
                } ?>
        		
        	],

        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'agendaWeek,agendaDay'
            //right: 'month,agendaWeek,agendaDay'
        
        },
        events: function(start, end, timezone, callback) {
            $.ajax({
                url: base_url+'Agenda/listcitas',
                dataType: 'json',
                data: {
                    start: start.unix(),
                    end: end.unix()
                },
                success: function(doc) {
                    var datos=doc;
                    var events = [];
                    datos.forEach(function(r) {
                        if(r.tipo_cita==1 || r.tipo_cita==2){
                            events.push({
                                id_cita: r.idcita,
                                title: r.nombre+' '+r.apll_paterno,
                                start: r.fecha_consulta+'T'+r.hora_de,
                                end: r.fecha_consulta+'T'+r.hora_hasta,
                                color: r.color,
                                tipo: r.tipo_cita,
                                cita_confirmada: r.cita_confirmada,
                                paciente_llego: r.paciente_llego,
                                paciente_no_llego: r.paciente_no_llego,
                                idpaciente: r.idpaciente, 
                                pago:r.pago
                            });
                        }else if(r.tipo_cita==3){
                            events.push({
                                id_cita: r.idcita,
                                title: r.motivo_consulta,
                                start: r.fecha_consulta+'T'+r.hora_de,
                                end: r.fecha_consulta+'T'+r.hora_hasta,
                                color: r.color,
                                tipo: r.tipo_cita,
                                cita_confirmada: r.cita_confirmada,
                                paciente_llego: r.paciente_llego,
                                paciente_no_llego: r.paciente_no_llego,
                                idpaciente:0,
                                pago:r.pago
                            });
                        }else if(r.tipo_cita==4){
                            events.push({
                                id_cita: r.idcita,
                                title: r.motivo_consulta,
                                start: r.fecha_consulta+'T'+'00:00:00',
                                end: r.fechafin+'T'+'23:59:59',
                                color: r.color,
                                tipo: r.tipo_cita,
                                cita_confirmada: r.cita_confirmada,
                                paciente_llego: r.paciente_llego,
                                paciente_no_llego: r.paciente_no_llego,
                                idpaciente:0,
                                pago:r.pago
                            });
                        }else if(r.tipo_cita==5){
                            events.push({
                                id_cita: r.idcita,
                                title: r.motivo_consulta,
                                start: r.fecha_consulta+'T'+'00:00:00',
                                end: r.fechafin+'T'+'23:59:59',
                                color: r.color,
                                tipo: r.tipo_cita,
                                cita_confirmada: r.cita_confirmada,
                                paciente_llego: r.paciente_llego,
                                paciente_no_llego: r.paciente_no_llego,
                                idpaciente:0,
                                pago:r.pago
                            });
                        }
                        
                        

                    });
                    callback(events);
                }
            });
        },
        eventRender: function(event, element){ 
            //console.log(event);
            element.click(function() {
                mosrar_cita(event.id_cita,event.tipo,event.idpaciente,event.paciente_llego,0);
            });
            if(event.pago==1){
                element.find(".fc-title").prepend('<i class="fas fa-dollar-sign"></i> ');
            }else{
                if(event.paciente_no_llego==1){
                    element.find(".fc-title").prepend('<i class="fas fa-stop" style="color:#8C77FE"></i> ');
                }else{
                    if(event.paciente_llego==1){
                        element.find(".fc-title").prepend('<i class="fas fa-stop" style="color:#28e652"></i> ');
                    }else{
                        if(event.cita_confirmada==1){
                            element.find(".fc-title").prepend('<i class="fas fa-stop" style="color:yellow"></i> ');
                        }
                    }
                }
            }        
        },
        dayClick: function(date, jsEvent, view) {
            var horas=date.format();
            var aux_horas=horas.split("T");
            var fecha = new Date(date);
            var dd = fecha.getDate();
            var mm = fecha.getMonth()+1;
            var yyyy = fecha.getFullYear();
            if(dd<10) {
                dd='0'+dd;
            } 
            if(mm<10) {
                mm='0'+mm;
            }  
            var aux_horas2 = aux_horas[1];
            $('#fecha_consulta_e').val(yyyy+'-'+mm+'-'+dd);
            $('#hora_de').val(aux_horas2);
            $('#hora_hasta').val(aux_horas2);
        }
        

    });
}
</script>