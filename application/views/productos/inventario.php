<input type="hidden" id="id_producto" value="<?php echo $idpro ?>">
<input type="text" id="fecha_actual" value="<?php echo $fecha_actual ?>">
<input type="text" id="fecha_mayor" value="<?php echo $fecha_mayor ?>">
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12"><h3>Producto en inventario: <?php echo $productos_get->producto ?></h3><hr></div>
                                    <div class="col-md-8">
                                        <div class="card-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div>
                                                        <h3><i class="fas fa-clipboard-list" style="color: #779155"></i> <span style="font-size:13px"><b class="p_stock"></b><br> TOTAL STOCK</span></h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Selecciones tipo de estatus</label>
                                        <div class="input-group mb-3">
                                             <select name="marca" id="lote" onchange="reload_registro()" class="form-control">
                                                <option value="0">Todos</option>
                                                <option value="1">Próximo a caducar</option>
                                                <option value="2">Lote caducado</option>
                                            </select>
                                        </div>
                                    </div>    
                                    <div class="col-lg-12"></div>
                                        <table class="table" id="table_datos">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th>Fecha de ingreso</th> 
                                                    <th>Lote</th>
                                                    <th>Fecha de caducidad</th>
                                                    <th>Stock ingresado</th>
                                                    <th>Stock Real</th>
                                                    <th>Estatus</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div> 
                                <div class="row">
                                    
                                </div>   
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div> 

<div id="eliminar_modal_almacen" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste lote? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_producto_almacen">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_registro_almacen()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>