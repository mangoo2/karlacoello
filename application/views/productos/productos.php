<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12"><h3>Productos</h3><hr></div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-outline-info"  onclick="modal_registro()"><i class="fas fa-capsules"></i> Nuevo producto</button> 
                                        <!--
                                        <button type="button" class="btn btn-outline-info" disabled><i class="fas fa-paper-plane"></i> Enviar cita y formulario</button> 
                                        <button type="button" class="btn btn-outline-info" disabled><i class="far fa-heart"></i> Recomentar</button>
                                    -->
                                    </div>
                                    <div class="col-md-6">    
                                        <div class="input-group bootstrap-touchspin">
                                            <span class="input-group-text">Buscar Producto &nbsp;&nbsp; <i class="fas fa-search"></i></span></span><input id="producto_busqueda" type="text" class="form-control" placeholder="Escriba nombre del producto" oninput="reload_registro()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12"></div>
                                        <table class="table" id="table_datos">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th>Foto</th> 
                                                    <th>Descripción</th>
                                                    <th>Nombre</th>
                                                    <th>Código</th>
                                                    <th>Tipo</th>
                                                    <th>Categoría</th>
                                                    <th>Inventario</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>    
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>            

<div id="registro_datos" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                <h4 class="modal-title">Registro de producto</h4>
                </div>
                <div class="col-lg-1">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3" align="center">
                                <img id="img_avatar" style="width: 145px; height: 145px; border-radius: 25px;" src="<?php echo base_url(); ?>images/final.png">
                                <span class="foto_avatar" >
                                    <input type="file" name="foto_avatar" id="foto_avatar" style="display: none">
                                </span>
                                <label for="foto_avatar">
                                    <span class="btn waves-effect waves-light btn-secondary"><i class="fas fa-camera"></i> Cambiar foto</span>
                                </label>
                            </div>
                            <div class="col-md-9">
                                <input type="hidden" id="foto_validar" value="0">
                                <form class="form" method="post" role="form" id="form_registro">
                                    <input type="hidden" name="idproducto" id="idproducto" value="0"> 
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label><span style="color: red;">*</span> Descripción</label>
                                                <textarea name="descriccion" id="descriccion" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Nombre de producto</label>
                                                <input type="text" name="producto" id="producto" class="form-control">
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Código o Alias</label>
                                                <input type="text" name="codigo" id="codigo" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Tipo</label>
                                                <div class="input-group mb-3">
                                                    <select name="tipo" id="tipo" class="form-control">
                                                            <option disabled="" selected="" value="0">Seleccione</option>
                                                            <?php foreach ($get_tipo as $x){ ?>
                                                                <option value="<?php echo $x->idtipo ?>"><?php echo $x->nombre ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    <div class="input-group-append">
                                                        <button type="button" class="btn waves-effect waves-light btn_sistema" onclick="modal_tipos()"><i class="fas fa-plus-circle"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Categoría</label>
                                                <div class="input-group mb-3">
                                                    <select name="categoria" id="categoria" class="form-control">
                                                        <option disabled="" selected="" value="0">Seleccione</option>
                                                        <?php foreach ($get_categoria as $x){ ?>
                                                            <option value="<?php echo $x->idcategoria ?>"><?php echo $x->nombre ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <div class="input-group-append">
                                                        <button type="button" class="btn waves-effect waves-light btn_sistema" onclick="modal_categorias()"><i class="fas fa-plus-circle"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Marca</label>
                                                <div class="input-group mb-3">
                                                     <select name="marca" id="marca" class="form-control">
                                                        <option disabled="" selected="" value="0">Seleccione</option>
                                                        <?php foreach ($get_marca as $x){ ?>
                                                            <option value="<?php echo $x->idmarca ?>"><?php echo $x->nombre ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <div class="input-group-append">
                                                        <button type="button" class="btn waves-effect waves-light btn_sistema" onclick="modal_marcas()"><i class="fas fa-plus-circle"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                              
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Observaciones</label>
                                                <textarea name="observaciones" id="observaciones" class="form-control js-auto-size"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Precio compra</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">$</span>
                                                    </div>
                                                    <input type="number" name="compra" id="compra" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Precio clínica</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">$</span>
                                                    </div>
                                                    <input type="number" name="clinica" id="clinica" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Precio mayoreo</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">$</span>
                                                    </div>
                                                    <input type="number" name="mayoreo" id="mayoreo" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>   
                                </form>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect btn_registro" onclick="guarda_registro()">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="elimina_registro_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste producto? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_producto">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_registro()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>
<div id="modal_tipo" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div align="center">
                    <h3 class="modal-title" id="myLargeModalLabel">Tipo</h3>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <form class="form" method="post" role="form" id="form_registro_tipo">
                                <label>Tipo</label>
                                <input type="hidden" name="idtipo" id="idtipo_tipo" value="0" class="form-control">
                                <input type="text" name="nombre" id="nombre_tipo" class="form-control">
                            </form> 
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label style="color: #21252900">_</label>
                            <button type="button" class="btn waves-effect waves-light btn_sistema" onclick="guarda_registro_tipo()"><i class="fas fa-save"></i></button>
                        </div>
                    </div>    
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12"></div>
                        <table class="table" id="table_datos_tipo">
                            <thead class="bg-blue">
                                <tr>
                                    <th>Nombre</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>    
            </div>
        </div>   
    </div>
</div>
<div id="modal_categoria" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div align="center">
                    <h3 class="modal-title" id="myLargeModalLabel">Categorías</h3>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <form class="form" method="post" role="form" id="form_registro_categoria">
                                <label>Cateroía</label>
                                <input type="hidden" name="idcategoria" id="idcategoria_categoria" value="0" class="form-control">
                                <input type="text" name="nombre" id="nombre_categoria" class="form-control">
                            </form> 
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label style="color: #21252900">_</label>
                            <button type="button" class="btn waves-effect waves-light btn_sistema" onclick="guarda_registro_categoria()"><i class="fas fa-save"></i></button>
                        </div>
                    </div>    
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12"></div>
                        <table class="table" id="table_datos_categoria">
                            <thead class="bg-blue">
                                <tr>
                                    <th>Nombre</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>    
            </div>
        </div>   
    </div>
</div>

<div id="modal_marca" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div align="center">
                    <h3 class="modal-title" id="myLargeModalLabel">Marcas</h3>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <form class="form" method="post" role="form" id="form_registro_marca">
                                <label>Cateroía</label>
                                <input type="hidden" name="idmarca" id="idmarca_marca" value="0" class="form-control">
                                <input type="text" name="nombre" id="nombre_marca" class="form-control">
                            </form> 
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label style="color: #21252900">_</label>
                            <button type="button" class="btn waves-effect waves-light btn_sistema" onclick="guarda_registro_marca()"><i class="fas fa-save"></i></button>
                        </div>
                    </div>    
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12"></div>
                        <table class="table" id="table_datos_marcas">
                            <thead class="bg-blue">
                                <tr>
                                    <th>Nombre</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>    
            </div>
        </div>   
    </div>
</div>

<div id="elimina_registro_modal_tipo" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste tipo? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_tipo">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_registro_tipo()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>

<div id="elimina_registro_modal_cate" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar ésta categoría? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_cate">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_registro_cate()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>

<div id="elimina_registro_modal_marca" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar ésta categoría? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_marc">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_registro_marca()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>

<div id="modal_almacen_datos" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 1100px;">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Registro de inventario</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="idproducto_almacen">
                <div class="row">    
                    <!--/span-->
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Cantidad</label>
                            <input type="number" id="stock" class="form-control">
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Lote</label>
                            <input type="text" id="lote" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Fecha de registro</label>
                            <input type="date" id="fecha_registro" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Fecha de caducidad</label>
                            <input type="date" id="fecha_caducidad" class="form-control">
                        </div>
                    </div>   
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Proveedor</label><br>
                            <select id="idproveedor" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1"><br>
                        <button type="button" class="btn btn-info btn-circle" onclick="add_almacen()"><i class="fas fa-plus-circle"></i> </button>
                    </div>
                </div> 
                <hr> 
                <div class="row">
                    <div class="col-lg-12 table_datos_almacen">
                        <table class="table" id="table_datos_almacen">
                            <thead class="bg-blue">
                                <tr>
                                    <th>Cantidad</th> 
                                    <th>Lote</th>
                                    <th>Fecha de registro</th>
                                    <th>Fecha de caducidad</th>
                                    <th>Proveedor</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>   
                <div class="row">
                    <div class="col-lg-12">
                        <h4>Total cantidad: <span class="total_stock"></span></h4>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id_marc">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-info waves-effect btn_almacen" onclick="registro_almacen()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>
