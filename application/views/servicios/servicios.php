<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-outline-info"  onclick="modal_registro()"><i class="fas fa-user-plus"></i> Nuevo servicio</button> 
                                        <!--
                                        <button type="button" class="btn btn-outline-info" disabled><i class="fas fa-paper-plane"></i> Enviar cita y formulario</button> 
                                        <button type="button" class="btn btn-outline-info" disabled><i class="far fa-heart"></i> Recomentar</button>
                                    -->
                                    </div>
                                    <div class="col-md-6">    
                                        <div class="input-group bootstrap-touchspin">
                                            <span class="input-group-text">Buscar servicio &nbsp;&nbsp; <i class="fas fa-search"></i></span></span><input id="servicio_busqueda" type="text" class="form-control" placeholder="Escriba nombre del servicio" oninput="reload_registro_servicios()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12"></div>
                                        <table class="table" id="table_datos">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Cantidad</th>
                                                    <th>Costo</th>
                                                    <th>Duración</th>
                                                    <th>Categoría</th>
                                                    <th>Encargado</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>    
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>            

<div id="registro_datos" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                <h4 class="modal-title">Registro de Servicio</h4>
                </div>
                <div class="col-lg-1">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form" method="post" role="form" id="form_registro">
                                    <input type="hidden" name="idservicio" id="idservicio" value="0"> 
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label><span style="color: red;">*</span> Nombre del servicio</label>
                                                <input type="text" name="nombre" id="nombre" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Descripción</label>
                                                <textarea class="form-control" name="descripcion" id="descripcion"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Número de sesiones</label>
                                                <input type="number" name="cantidad" id="cantidad" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Costo</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">$</span>
                                                    </div>
                                                    <input type="number" name="costo" id="costo" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Duración</label>
                                                <input type="text" name="duracion" id="duracion" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Categoría</label>
                                                <div class="input-group mb-3">
                                                    <select name="categoria" id="categoria" class="form-control">
                                                        <option disabled="" selected="" value="0">Seleccione</option>
                                                        <?php foreach ($get_categoria as $x){ ?>
                                                            <option value="<?php echo $x->idcategoria ?>"><?php echo $x->nombre ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <div class="input-group-append">
                                                        <button type="button" class="btn waves-effect waves-light btn_sistema" onclick="modal_categorias()"><i class="fas fa-plus-circle"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Encargado</label>
                                                <input type="text" name="encargado" id="encargado" class="form-control">
                                            </div>
                                        </div>
                                    </div>   
                                </form>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect btn_registro" onclick="guarda_registro()">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="elimina_registro_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste servicio? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_servicio">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_registro()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>

<div id="modal_categoria" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div align="center">
                    <h3 class="modal-title" id="myLargeModalLabel">Categorías</h3>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <form class="form" method="post" role="form" id="form_registro_categoria">
                                <label>Cateroía</label>
                                <input type="hidden" name="idcategoria" id="idcategoria_categoria" value="0" class="form-control">
                                <input type="text" name="nombre" id="nombre_categoria" class="form-control">
                            </form> 
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label style="color: #21252900">_</label>
                            <button type="button" class="btn waves-effect waves-light btn_sistema" onclick="guarda_registro_categoria()"><i class="fas fa-save"></i></button>
                        </div>
                    </div>    
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12"></div>
                        <table class="table" id="table_datos_categoria">
                            <thead class="bg-blue">
                                <tr>
                                    <th>Nombre</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>    
            </div>
        </div>   
    </div>
</div>

<div id="elimina_registro_modal_cate" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar ésta categoría? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_cate">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_registro_cate()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>
