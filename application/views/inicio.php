<style type="text/css">
  .iframeprintc1{
    width: 100%;
    height: 60vh;
    border: 0;
  }
  .iframeticketc1{
    padding: 0px;
  }
</style>

<input type="hidden" id="perfilid" value="<?php echo $perfilid ?>">
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-outline-info"  onclick="modalPaciente()"><i class="fas fa-user-plus"></i> Nuevo Paciente</button> 
                                        <!--
                                        <button type="button" class="btn btn-outline-info" disabled><i class="fas fa-paper-plane"></i> Enviar cita y formulario</button> 
                                        <button type="button" class="btn btn-outline-info" disabled><i class="far fa-heart"></i> Recomentar</button>
                                    -->
                                    </div>
                                    <div class="col-md-6">    
                                        <div class="input-group bootstrap-touchspin">
                                            <span class="input-group-text">Buscar Paciente &nbsp;&nbsp; <i class="fas fa-search"></i></span></span><input id="paciente_busqueda" type="text" class="form-control" placeholder="Escriba nombre del paciente" oninput="reload_paciente()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                            <?php 
                            $aux=0;
                            foreach ($pacientes as $item){
                                $aux=1; 
                            }?> 
                            <?php if($aux==1){ ?> 
                            <div class="row">
                                <div class="col-lg-12"></div>
                                    <table class="table" id="table_pacientes">
                                        <thead class="bg-blue">
                                            <tr>
                                                <th>
                                                   
                                                </th>
                                                <th width="250px">Nombre Paciente</th>
                                                <th width="200px">Fecha última consulta</th>
                                                <th width="550px"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php }else{ ?>    
                                <div class="row">
                                    <div class="col-md-12" align="center">
                                        <h5>No tienes pacientes</h5>
                                        <h6>Te sugerimos no colocar información de pacientes de prueba ya que puede afectar a tus estadísticas</h6>
                                        <h6>Crea un nuevo paciente haciendo click en el botón.</h6>  
                                        <h6><i class="fas fa-arrow-down"></i></h6>
                                        <button type="button" class="btn waves-effect waves-light btn-info" onclick="modalPaciente()"><i class="fas fa-user-plus"></i> Crear Nuevo Paciente</button>
                                    </div>
                                </div>
                            <?php } ?>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>            

<div id="nuevoPaciente" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                <h4 class="modal-title">Selecciona el tipo de paciente</h4>
                </div>
                <div class="col-lg-1">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">    
                        <div align="center">
                            <button type="button" class="btn btn-outline-info" onclick="paciente_tipo(1)"><i class="fas fa-user-plus"></i> Paciente de primera vez</button> 
                            <span> or </span>
                            <button type="button" class="btn btn-outline-info" onclick="paciente_tipo(2)"><i class="fas fa-user"></i> Paciente Subsecuente</button>
                        </div>
                    </div>                        
                </div>
                
                <div class="row contenido_paciente">
                    <div class="col-lg-12">
                        <div align="center"> <br>   
                            <h4 class="titulo_paciente_registro"></h4>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3" align="center">
                              <img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="<?php echo base_url(); ?>images/annon.png">
                              <span class="foto_avatar" >
                                <input type="file" name="foto_avatar" id="foto_avatar" style="display: none">
                                </span>
                                <label for="foto_avatar">
                                    <span class="btn waves-effect waves-light btn-secondary"><i class="fas fa-camera"></i> Cambiar foto</span>
                                </label>
                            </div>
                            <div class="col-md-9">
                            <form class="form" method="post" role="form" id="form_paciente">
                                <input type="hidden" name="tipo" id="tipo" value="0"> 
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label><span style="color: red;">*</span> Nombre(s)</label>
                                            <input type="text" name="nombre" id="nombre" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label><span style="color: red;">*</span> Apellido Paterno</label>
                                            <input type="text" name="apll_paterno" id="apll_paterno" onchange="validar_paciente()" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label><span style="color: red;">*</span> Apellido Materno</label>
                                            <input type="text" name="apll_materno" id="apll_materno" onchange="validar_paciente()" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Fecha de nacimiento</label>
                                            <input type="date" name="fecha_nacimiento" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label><span style="color: red;">*</span> Sexo</label>
                                            <select name="sexo" class="form-control">
                                                <option disabled="" selected="" value="0">Seleccione</option>
                                                <option value="1">Masculino</option>
                                                <option value="2">Femenino</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Correo Electronico</label>
                                            <input type="email" name="correo" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-8"></div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Correo Electronico 2</label>
                                            <input type="email" name="correo2" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Teléfono Celular</label>
                                            <input type="text" name="celular" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Teléfono de Casa</label>
                                            <input type="text" name="casa" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Teléfono de Oficina</label>
                                            <input type="text" name="oficina" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Dirección</label>
                                            <input type="text" name="direccion" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Estado civil</label>
                                            <select name="estado_civil" class="form-control">
                                                <option disabled="" selected="" value="0">Seleccione</option>
                                                <option value="1">Soltero (a)</option>
                                                <option value="2">Casado (a)</option>
                                                <option value="3">Divorciado (a)</option>
                                                <option value="4">Viudo (a)</option>
                                                <option value="5">Unión Libre</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Notas</label>
                                            <input type="text" name="nota" class="form-control">
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Ocupación</label>
                                            <input type="text" name="ocupacion" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox m-b-0">
                                                <input type="checkbox" class="custom-control-input" name="check_acepta" id="verificar_check">
                                                <span class="custom-control-label"> El paciente acepta el <u><b>aviso de privacidad </b></u></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </form>    
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer btn_paciente_registro" align="center" style="display: none">
                <button type="button" class="btn btn-info waves-effect" onclick="guarda_paciente()">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- -->
<div id="modalencuesta" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                <h4 class="modal-title">Encuesta de calidad</h4>
                </div>
                <div class="col-lg-1">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <input type="hidden" id="idclienteencuesta">
                <div class="row">
                    <div class="col-md-3">
                        <input type="tel" id="encuentatel" class="form-control" >
                    </div>
                    <div class="col-md-6 center">
                            <button type="button" class="btn btn-outline-info" onclick="envioencuesta(1)">
                                <i class="fab fa-whatsapp"></i> Enviar por whatsapp</button> 
                            <span> o </span>
                            <button type="button" class="btn btn-outline-info" onclick="envioencuesta(2)">
                                <i class="far fa-envelope"></i> Enviar por correo</button>
                    </div>
                    <div class="col-md-3">
                        <input type="email" id="encuentaemail" class="form-control">
                    </div>
                                           
                </div>
                <div class=" viewencuenta">
                    
                </div>
            </div>
            <div class="modal-footer btn_paciente_registro" align="center" style="display: none">
                <button type="button" class="btn btn-info waves-effect" onclick="guarda_paciente()">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div id="modal_almacen_datos" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 1100px;">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Venta de productos</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-4">
                        <label>Producto</label>
                        <select class="form-control" id="idproducto">
                        </select>
                        <input type="hidden" id="precio_clinica" value="0">
                    </div>
                    <div class="col-lg-8">
                        <div class="lotes_productos">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table" id="tabla_producto_venta_consultas">
                            <thead class="bg-blue">
                                <tr>
                                    <th>Producto</th> 
                                    <th>Lote</th>
                                    <th>Caducidad</th>
                                    <th>Cantidad</th>
                                    <th>Precio U</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <br>
                        <div align="right">
                            <h4>Total: $<span class="total_costop">0</span></h4>
                        </div>  
                    </div>
                </div> 
            </div>
            <input type="hidden" id="id_marc">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-info waves-effect btn_almacen" onclick="add_productos_venta()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>

<div id="modal_ticket_producto" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Ticket</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body iframereporte">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="imprimiriframac1('iframeprintc1')">Imprimir</button>
            </div>
        </div>   
    </div>
</div> 