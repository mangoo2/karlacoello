<?php

    require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF/tcpdf.php');
    $this->load->helper('url');
    //var_dump($GLOBALS['folio']);die;
//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        $img_file = base_url().'public/img/centroneuro/portada_header.jpg'; 
        //$pdf->Image($img_file, 0, 0, 0, 500, '', '', '', false, 500, '', false, false, 0); 
        $this->Image($img_file, 0, 0, 210, 40, '', '', '', false, 330, '', false, false, 0); 

        $html = '<table width="100%" border="0"> 
                    <tr> 
                        <td width="100%" height="80px"></td> 
                    </tr> 
                </table> 
                <table width="100%" border="0"> 
                    <tr> 
                        <td width="20%"></td> 
                        <td width="20%" style="text-align: left;"></td> 
                        <td width="60%" style="text-align: right;"> 
                            <br> 
                            <br> 
                            <span style="font-weight: bold; font-size: 17px;">DR. DATENTE OROPEZA CANTO</span><br> 
                            <span style="font-weight: bold; font-size: 15px;">Neurólogo Clínico</span><br> 
                            <span style="font-size: 13px;">CED. PROF. 2018947 UPAEP CED. ESP. 3413958</span> 
                            <br> 
                             
                        </td> 
                    </tr> 
                </table>'; 
        $this->writeHTML($html, true, false, true, false, ''); 
    }
    // Page footer
    public function Footer() {
        $img_file = base_url().'public/img/centroneuro/portada_footer.jpg'; 
        //$pdf->Image($img_file, 0, 0, 0, 500, '', '', '', false, 500, '', false, false, 0); 
        $this->Image($img_file, 0, 280, 200, 18, '', '', '', false, 330, '', false, false, 0); 

        
          $html = '  
          <table width="100%" border="0"> 
            <tr> 
                <td width="100%" style="text-align: right; font-size: 15px;"> 
                    <span>DR. DANTE OROPEZA CANTO</span><br> 
                    <span>NEUROLOGO CLINICO.</span><br> 
                    <span>CED. PROFESIONAL. 3413958</span> 
                    <br><br> 
                </td> 
            </tr>     
            <tr> 
                <td width="24%"></td> 
                <td width="3%"> 
                    <img width="15" src="'.base_url().'public/img/centroneuro/ubicacion3.jpg"> 
                </td> 
                <td width="35%"> 
                    <span style="font-size: 11px;">Hospital Ángeles Av. Kepler 2143,</span><br> 
                    <span style="font-size: 11px;">Reserva Territorial Atlixcáyotl</span><br> 
                    <span style="font-size: 11px;">Torre de Especialidades Consultorio</span><br> 
                    <span style="font-size: 11px;">3720</span><br> 
                </td> 
                <td width="3%"> 
                    <img width="15" src="'.base_url().'public/img/telefono.jpg"> 
                </td> 
                <td width="35%"> 
                    <span style="font-size: 12px; font-weight: bold;">Cons. 2222 90 76 22</span><br> 
                    <span style="font-size: 12px; font-weight: bold;">Cel. 2222 65 01 33</span> 
                </td> 
            </tr> 
             
          </table>'; 
          //<td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td> 
        $this->writeHTML($html, true, false, true, false, ''); 
        $this->SetXY(200, 250);
        $this->StartTransform();
        $this->Rotate(90);
        $this->Cell(50,0,'w w w . n e u r o a n g e l e s . c o m . m x',0,1,'C',0,'');
        $this->StopTransform();
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cuestionario');
$pdf->SetTitle('Cuestionario');
$pdf->SetSubject('Cuestionario');
$pdf->SetKeywords('Cuestionario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('12', '50', '12'); 
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('60'); 
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 13);
// add a page
$pdf->AddPage('P', 'A4');
  $html='<table width="100%" border="0"> 
            <tr> 
                <td width="100%"> 
                     <br><br> 
                    <span style="font-size: 15px;">Nombre: </span> <span style="font-weight: bold; font-size: 15px;"><u>'.$paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno.'</u></span> 
                </td> 
            </tr> 
            <tr> 
                <td width="40%"> 
                    <span style="font-size: 15px;">Fecha: </span> <span style="font-weight: bold; font-size: 15px;"><u>'.date('d/m/Y',strtotime($consultafecha)).'</u></span> 
                </td> 
                <td width="60%" style="text-align: right; font-size: 10px;"> 
                    <span>CMN 20 de Nov-ISSSTE-UNAM</span><br> 
                    <span>Certificado en Medicina del Sueño (Folio 8)</span><br> 
                    <span>Máster en Sueño , Universidad de Pablo de Olavide, Sevilla España</span><br> 
                    <span>Máster en Transtornos del Movimiento, Universidad de Murcia, España</span><br> 
                    <span>Miembro de la Academia Mexicana de Neurología</span><br> 
                    <span>Miembro de la Academia Americana de Neurología</span><br> 
                    <span>Sociedad Mexicana de Neurofisiologia Clínica </span><br> 
                    <span>Movement Disorder Society, American Academy of Neurology</span><br> 
                    <span>Máster en Neuroinmunología Universidad Autónoma de Barcelona (UBA)</span> 
                </td> 
            </tr>';
            /*
            <tr style="font-size:10%;">
                <td width="20%">
                </td>
                <td width="55%" style="text-align: center; font-size: 11px;">
                    <br><br><br><br>
                    <span >ORDEN DE HOSPITALIZACIÓN</span>
                </td>
                <td width="25%" style="text-align: center; font-size: 11px;" >
                    <br><br><br><br>
                    <span style="text-align: center; font-size: 8px;"> FECHA DE IMPRESIÓN: '.date('d/m/y',strtotime($consultafecha)).'</span>
                </td>
            </tr>
            <tr style="font-size:1%;">
                <td width="100%" style="border-bottom: 2px solid #346c94;">
                </td>
            </tr>
            <tr style="font-size:80%; border-bottom: 2px solid #346c94;">
                <td width="30%" >
                    <span  style="font-size: 10px;">Médico tratante</span><br>
                    <span  style="font-size: 10px;">Dr. DANTE OROPEZA CANTO</span>
                </td>
                <td width="40%"><br><br>
                    <span style="font-size: 10px;">Especialidad: Neurología</span>
                </td>
                <td width="30%">
                    <br><br>
                    <span style="font-size: 10px;"> Cédula: 2018958 / 3413958</span>
                </td>
            </tr>
            <tr style="font-size:10%;">
                <td width="100%" style="border-bottom: 2px solid #346c94;">
                </td>
            </tr>
            */

    $html.='<tr style="font-size:10%;">
                <td width="100%" style="border-bottom: 2px solid #346c94;">
                </td>
            </tr>
            <tr style="font-size:25%;">
                <td width="50%"><br><br>
                    <span  style="font-size: 10px;">Nombre del paciente</span><br>
                </td>
                <td width="50%"><br><br>
                    <span style="font-size: 10px;">Edad</span>
                </td>
            </tr>';

    $html.='<tr style="background-color: #346c94; color:white;">
                <td width="50%">
                    <span  style="font-size: 10px;">'.$paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno.'</span>
                </td>
                <td width="50%">
                    <span style="font-size: 10px;">'.$edad.' años</span>
                </td>
            </tr>
            <tr style="font-size:70%;">
                <td width="100%">
                    <span style="font-size: 10px;">Favor de hospitalizar a: '.$paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno.'</span>
                </td>
            </tr>';
    $html.='<tr style="font-size:1%;">
                <td width="100%" style="border-bottom: 2px solid #346c94;">
                </td>
            </tr>
            <tr style="font-size:70%;">
                <td width="100%">
                    <span style="font-size: 10px;">Diagnóstico:</span><br>
                    <span style="font-size: 10px;">'.$detalles.'</span>
                </td>
            </tr>';
  $html.='</table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('laboratorio.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
?>