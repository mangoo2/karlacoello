<?php 
    require_once('TCPDF/examples/tcpdf_include.php'); 
    require_once('TCPDF/tcpdf.php'); 
    $this->load->helper('url'); 
//======================================================================================= 
class MYPDF extends TCPDF { 
    //Page header 
    //<span style="font-size: 13px; color:red">N° 0400</span> 
    public function Header() { 
        $img_file = base_url().'public/img/centroneuro/portada_header.jpg'; 
        //$pdf->Image($img_file, 0, 0, 0, 500, '', '', '', false, 500, '', false, false, 0); 
        $this->Image($img_file, 0, 0, 210, 40, '', '', '', false, 330, '', false, false, 0); 

        $html = '<table width="100%" border="0"> 
                    <tr> 
                        <td width="100%" height="80px"></td> 
                    </tr> 
                </table> 
                <table width="100%" border="0"> 
                    <tr> 
                        <td width="20%"></td> 
                        <td width="20%" style="text-align: left;"></td> 
                        <td width="60%" style="text-align: right;"> 
                            <br> 
                            <br> 
                            <span style="font-weight: bold; font-size: 17px;">DR. DATENTE OROPEZA CANTO</span><br> 
                            <span style="font-weight: bold; font-size: 15px;">Neurólogo Clínico</span><br> 
                            <span style="font-size: 13px;">CED. PROF. 2018947 UPAEP CED. ESP. 3413958</span> 
                            <br> 
                             
                        </td> 
                    </tr> 
                </table>'; 
        $this->writeHTML($html, true, false, true, false, ''); 
    } 
    // Page footer 
    public function Footer() { 
        $img_file = base_url().'public/img/centroneuro/portada_footer.jpg'; 
        //$pdf->Image($img_file, 0, 0, 0, 500, '', '', '', false, 500, '', false, false, 0); 
        $this->Image($img_file, 0, 280, 200, 18, '', '', '', false, 330, '', false, false, 0); 

        
          $html = '  
          <table width="100%" border="0"> 
            <tr> 
                <td width="100%" style="text-align: right; font-size: 15px;"> 
                    <span>DR. DANTE OROPEZA CANTO</span><br> 
                    <span>NEUROLOGO CLINICO.</span><br> 
                    <span>CED. PROFESIONAL. 3413958</span> 
                    <br><br> 
                </td> 
            </tr>     
            <tr> 
                <td width="24%"></td> 
                <td width="3%"> 
                    <img width="15" src="'.base_url().'public/img/centroneuro/ubicacion3.jpg"> 
                </td> 
                <td width="35%"> 
                    <span style="font-size: 11px;">Hospital Ángeles Av. Kepler 2143,</span><br> 
                    <span style="font-size: 11px;">Reserva Territorial Atlixcáyotl</span><br> 
                    <span style="font-size: 11px;">Torre de Especialidades Consultorio</span><br> 
                    <span style="font-size: 11px;">3720</span><br> 
                </td> 
                <td width="3%"> 
                    <img width="15" src="'.base_url().'public/img/telefono.jpg"> 
                </td> 
                <td width="35%"> 
                    <span style="font-size: 12px; font-weight: bold;">Cons. 2222 90 76 22</span><br> 
                    <span style="font-size: 12px; font-weight: bold;">Cel. 2222 65 01 33</span> 
                </td> 
            </tr> 
             
          </table>'; 
          //<td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td> 
        $this->writeHTML($html, true, false, true, false, ''); 
        $this->SetXY(200, 250);
        $this->StartTransform();
        $this->Rotate(90);
        $this->Cell(50,0,'w w w . n e u r o a n g e l e s . c o m . m x',0,1,'C',0,'');
        $this->StopTransform();
    } 
}  
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); 
 
// set document information 
$pdf->SetCreator(PDF_CREATOR); 
$pdf->SetAuthor('Cuestionario'); 
$pdf->SetTitle('Cuestionario'); 
$pdf->SetSubject('Cuestionario'); 
$pdf->SetKeywords('Cuestionario'); 
 
// set default header data 
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING); 
 
// set header and footer fonts 
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN)); 
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN)); 
 
// set default monospaced font 
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
 
// set margins 
$pdf->SetMargins('12', '50', '12'); 
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('60'); 
// set auto page breaks 
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM); 
 
// set image scale factor 
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
 
$pdf->SetFont('dejavusans', '', 13); 
// add a page 
$pdf->AddPage('P', 'A4'); 
  $html='<table width="100%" border="0"> 
            <tr> 
                <td width="100%"> 
                     <br><br> 
                    <span style="font-size: 15px;">Nombre: </span> <span style="font-weight: bold; font-size: 15px;"><u>'.$paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno.'</u></span> 
                </td> 
            </tr> 
            <tr> 
                <td width="40%"> 
                    <span style="font-size: 15px;">Fecha: </span> <span style="font-weight: bold; font-size: 15px;"><u>'.date('d/m/Y',strtotime($consulta->consultafecha)).'</u></span> 
                </td> 
                <td width="60%" style="text-align: right; font-size: 10px;"> 
                    <span>CMN 20 de Nov-ISSSTE-UNAM</span><br> 
                    <span>Certificado en Medicina del Sueño (Folio 8)</span><br> 
                    <span>Máster en Sueño , Universidad de Pablo de Olavide, Sevilla España</span><br> 
                    <span>Máster en Transtornos del Movimiento, Universidad de Murcia, España</span><br> 
                    <span>Miembro de la Academia Mexicana de Neurología</span><br> 
                    <span>Miembro de la Academia Americana de Neurología</span><br> 
                    <span>Sociedad Mexicana de Neurofisiologia Clínica </span><br> 
                    <span>Movement Disorder Society, American Academy of Neurology</span><br> 
                    <span>Máster en Neuroinmunología Universidad Autónoma de Barcelona (UBA)</span> 
                </td> 
            </tr>'; 
            if($orden->recomendar=='1'){
        $html.='<tr>';
            $html.='<td width="100%"> ';
                $html.='<br><br> ';
                    if($orden->nombre_laboratorio!=''){
                $html.='<span style="font-weight: bold; font-size: 15px;">'.$orden->nombre_laboratorio.'</span><br> ';
                    }
                    if($orden->direccion!=''){
                $html.='<span style="font-weight: bold; font-size: 15px;">'.$orden->direccion.'</span><br> ';
                    }
                    if($orden->telefono!=''){
                $html.='<span style="font-weight: bold; font-size: 15px;">'.$orden->telefono.'</span> ';
                    }
            $html.='</td>';
                /*
                <td width="100%"> 
                    <br><br> 
                    <span style="font-weight: bold; font-size: 15px;">HOSPITAL ANGELES DE PUEBLA</span><br> 
                    <span style="font-weight: bold; font-size: 15px;">DEPARTAMENTO DE IMAGENOLOGÍA</span><br> 
                    <span style="font-weight: bold; font-size: 15px;">Tel: 222 303 66 00 Ext: 2023</span> 
                </td>
                */ 
            $html.='</tr>';
            }
        $html.='<tr style="padding: 5px 10px;" style="margin: auto;"> 
                <td width="100%"> 
                    <br><br>';

                $html.='<span style="font-size: 15px;">Favor de realizar:</span> 
                    <ul>'; 
                    foreach ($list_estudio as $item) {  
                     $html.='<li><span style="font-size: 15px;">'.$item->detalles.'</span></li>'; 
                    }     
             $html.='</ul><br>';
                    if($orden->observaciones!=''){
                    $html.='<span style="font-size: 15px;">'.$orden->observaciones.'</span><br>';
                    }
                    if($orden->incluir==1){
                    $html.='<span style="font-size: 15px;">Diagnóstico: '.$orden->diagnostico.'</span>';
                    }

            $html.='</td>
            </tr>     
        </table>'; 
 
$pdf->writeHTML($html, true, false, true, false, ''); 
$pdf->Output('laboratorio.pdf', 'I'); 
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F'); 
?>