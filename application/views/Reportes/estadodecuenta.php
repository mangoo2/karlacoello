<?php 
    require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF/tcpdf.php');
    $this->load->helper('url');
    //=======================================================================================
    $GLOBALS['folio']='';
    $GLOBALS['empresa']='';
    $GLOBALS['Direccion']='';
    foreach ($datoscontrato->result() as $item) {
        $GLOBALS['folio']=$item->folio;
        $GLOBALS['empresa']=$item->empresa;
        $GLOBALS['Direccion']=$item->Direccion;
    }
    $GLOBALS['facturasvencidas']=0;
    $GLOBALS['montovencido']=0;
    $GLOBALS['ultimafactura']=0;
    foreach ($facturas->result() as $item) {
        if ($item->pagot>0) {
            # code...
        }else{
            $GLOBALS['montovencido']=$GLOBALS['montovencido']+$item->total;
            $GLOBALS['facturasvencidas']=$GLOBALS['facturasvencidas']+1; 
        }
        $GLOBALS['ultimafactura']=$item->fechatimbre;
    }
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      //$img_header = 'header.jpg';
      //$this->Image($img_header, 0, 0, 0, 197, '', '', '', false, 100, '', false, false, 0);
      //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
      $imglogo = base_url().'public/img/ops.png';
      $html = '
          
          <style type="text/css">
            .titulotex{
                font-size:11px;
            }
            .titulotex_grey{
                color:grey;
            }
            .titulotex_red{
                color:red;
            }
          </style>
        <table width="100%" border="0" >
            <tr>
                <td width="19%" class="titulotex titulotex_grey">FACTURAS VENCIDAS</td>
                <td width="17%" class="titulotex titulotex_red">'.$GLOBALS['facturasvencidas'].'</td>
                <td width="28%" align="center" rowspan="3"><img src="'.$imglogo.'" width="100px" ></td>
                <td width="19%"  class="titulotex titulotex_grey">FECHA DE ULTIMA FACTURA</td>
                <td width="17%" class="titulotex titulotex_red">'.$GLOBALS['ultimafactura'].'</td>
            </tr>
            <tr>
                <td class="titulotex titulotex_grey">MONTO VENCIDO</td>
                <td class="titulotex titulotex_red">'.$GLOBALS['montovencido'].'</td>
                <td class="titulotex titulotex_grey">FECHA DE VENCIMIENTO</td>
                <td class="titulotex titulotex_red"></td>
            </tr>
            <tr>
                <td class="titulotex titulotex_grey">NUMERO DE EQUIPOS</td>
                <td class="titulotex titulotex_red"></td>
                <td class="titulotex"></td>
                <td class="titulotex titulotex_red"></td>
            </tr>
        </table>
          <table width="100%" border="0" >
            <tr>
                <td align="center" style="color:#0078d4; font-weight:bold;">
                    '.$GLOBALS['empresa'].'
                </td>
            </tr>
            <tr>
                <td align="center" style="color:#0078d4; font-weight:bold; font-size:14px;">
                    '.$GLOBALS['Direccion'].'
                </td>
            </tr>
            <tr>
                <td align="center" >
                    CONTRATO '.$GLOBALS['folio'].'
                </td>
            </tr>
          </table>
          ';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
      $html = ' 
      <table width="100%" border="0">
        <tr>
          <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>';
      $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cuestionario');
$pdf->SetTitle('Cuestionario');
$pdf->SetSubject('Cuestionario');
$pdf->SetKeywords('Cuestionario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '45', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage('P', 'A4');

        $html = '
                <style type="text/css">
                    .stileback{
                        background-color:#0078d4;
                        color: white;
                        font-weight: bold;
                    }
                    .bodertable{
                        border: #0078d4 5px solid;
                    }
                </style>
                <table cellpadding="5" border="1" class="bodertable" width="100%">';
            $html .= '<tr>
                        <td width="14%" class="stileback bodertable">FECHA DE FACTURA</td>
                        <td width="11%" class="stileback">FACTURA</td>
                        <td width="28%" class="stileback">DESCRIPCIÓN</td>
                        <td width="11%" class="stileback">CARGOS</td>
                        <td width="11%" class="stileback">PAGO</td>
                        <td width="14%" class="stileback">FECHA DE PAGO</td>
                        <td width="11%" class="stileback">TOTAL</td>
                      </tr>';
            foreach ($facturas->result() as $item) {
                $descripcion=$this->ModeloCatalogos->descripcionfactura($item->FacturasId);
                if ($item->fechamax==null) {
                    $fechapago='Pendiente';
                }else{
                    $fechapago=$item->fechamax;
                }
                $totalf=0;
                $totalf=$totalf+($item->total-0);
                $html .= '<tr>
                        <td class="bodertable">'.$item->fechatimbre.'</td>
                        <td>'.$item->Folio.'</td>
                        <td>'.$descripcion.'</td>
                        <td>'.$item->total.'</td>
                        <td>'.$item->pagot.'</td>
                        <td>'.$fechapago.'</td>
                        <td>'.$totalf.'</td>
                      </tr>';
            }


            

        $html .= '</table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Documentos.pdf', 'I');