<?php

    require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF/tcpdf.php');
    $this->load->helper('url');
    //var_dump($GLOBALS['folio']);die;
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
    $img_file = base_url().'public/img/centroneuro/portada_header.jpg'; 
    //$pdf->Image($img_file, 0, 0, 0, 500, '', '', '', false, 500, '', false, false, 0); 
    $this->Image($img_file, 0, 0, 210, 40, '', '', '', false, 330, '', false, false, 0); 
    $html = '';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $img_file = base_url().'public/img/centroneuro/portada_footer.jpg'; 
    //$pdf->Image($img_file, 0, 0, 0, 500, '', '', '', false, 500, '', false, false, 0); 
    $this->Image($img_file, 0, 280, 200, 18, '', '', '', false, 330, '', false, false, 0); 
    $html = '';
    /*
      $html = ' 
      <table>
        <tr>
            <td width="12%" align="left" class="footerpage"><br><br>
                <span style="font-size: 8px;"> pág '.$this->getAliasNumPage().' - '.$this->getAliasNbPages().'</span>
            </td>
            <td width="55%"><br><br>
                <span style="font-size: 8px;"> Avenida Kepler 2143, Reserva Territorial Atlixcáyotl, 72190 Puebla, Pue. | Estado: Puebla </span>
            </td>
            <td width="24%"><br><br>
                <span style="font-size: 8px;"> Dr. DANTE OROPEZA CANTO | Firma: </span>
            </td>
            <td width="9%">
                <img width="40" src="'.base_url().'public/img/firma_doc.PNG">
            </td>
        </tr> 
      </table>';
    */
      //<td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
      $this->writeHTML($html, true, false, true, false, '');
      $this->SetXY(200, 250);
        $this->StartTransform();
        $this->Rotate(90);
        $this->Cell(50,0,'w w w . n e u r o a n g e l e s . c o m . m x',0,1,'C',0,'');
        $this->StopTransform();
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cuestionario');
$pdf->SetTitle('Cuestionario');
$pdf->SetSubject('Cuestionario');
$pdf->SetKeywords('Cuestionario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '40', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetFooterMargin('15');
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 13);
// add a page
$pdf->AddPage('P', 'A4');
  $html='<table style="margin-top: 0px">
            <tr>
                <td width="100%" style="text-align: right;">
                    <br><br>
                    <u><span style="font-size: 15px;"> Fecha de consulta: '.date('d/m/y',strtotime($fecha_consulta_ultima)).'</span></u>
                </td>
            </tr>
            <tr style="font-size:80%; border-bottom: 2px solid #346c94;">
                <td width="100%" >
                    <span  style="font-size: 13px; font-weight: bold;">Paciente: '.$paciente.'</span>
                </td>
            </tr>
            <tr style="font-size:10%;">
                <td width="100%" style="border-bottom: 2px solid #346c94;">
                </td>
            </tr>
            <tr style="font-size:100%;">
                <td width="100%">
                    <span  style="font-size: 12px; font-weight: bold;">Resumen de consulta</span><br>
                    <span  style="font-size: 12px;">Edad del paciente: '.$edad.' años</span><br>
                </td>
            </tr>
            <tr style="font-size:100%;">
                <td width="100%">';
                if($fecha_consulta_ultima==$get_infoc->consultafecha || $aux_existe==0){
                $html.='<span  style="font-size: 12px; font-weight: bold;">Padecimiento Actual</span><br>
                        <span  style="font-size: 12px;">'.$get_infoc->nota_evaluacion.'</span><br>
                        <span  style="font-size: 12px;">'.date('d/m/Y',strtotime($get_infoc->fechainicio)).'</span><br>';
                }else{  
                $html.='<span  style="font-size: 12px; font-weight: bold;">Nota de Evolución</span><br>
                        <span  style="font-size: 12px;">'.$get_infoc->nota_evaluacion.'</span><br>';
                } 
        $html.='</td>
            </tr>';
            if($get_infoc->altrura!=0  || $get_infoc->peso!=0 || $get_infoc->ta!='' || $get_infoc->tempc!=0 || $get_infoc->fc!='' || $get_infoc->fr!=0 || $get_infoc->o2!=0){
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Signos Vitales/Básicos</span><br>';
                
        $html.='</td>
            </tr>';
     $html.='<tr style="font-size:100%;">';
                if($get_infoc->altrura!=0){
         $html.='<td width="19%">
                    <span style="font-size: 12px;">Altura (m) <span style="font-size: 12px; font-weight: bold;">'.$get_infoc->altrura.'</span></span><br>
                </td>';
                }
                if($get_infoc->peso!=0){
         $html.='<td width="12%">
                    <span style="font-size: 12px;">Peso <span style="font-size: 12px; font-weight: bold;">'.$get_infoc->peso.'</span></span><br>
                </td>';
                }
                if($get_infoc->ta!=''){
         $html.='<td width="13%">
                    <span style="font-size: 12px;">T.A. <span style="font-size: 12px; font-weight: bold;">'.$get_infoc->ta.'</span></span><br>
                </td>';
                }
                if($get_infoc->tempc!=0){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">Temp <span style="font-size: 12px; font-weight: bold;">'.$get_infoc->tempc.'</span></span><br>
                </td>';
                }
                if($get_infoc->fc!=''){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">F.C. <span style="font-size: 12px; font-weight: bold;">'.$get_infoc->fc.'</span></span><br>
                </td>';
                }
                if($get_infoc->fr!=0){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">F.R. <span style="font-size: 12px; font-weight: bold;">'.$get_infoc->fr.'</span></span><br>
                </td>';
                }
                if($get_infoc->o2!=0){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">O2 <span style="font-size: 12px; font-weight: bold;">'.$get_infoc->o2.'</span></span><br>
                </td>';
                }
    $html.='</tr>';
            }
            if($get_infoc->exploracionfisica!=''){
            $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Exploración Física</span><br>';
                $html.='<span style="font-size: 12px;">'.$get_infoc->exploracionfisica.'</span>';
        $html.='</td>
            </tr>';
            }
            if($get_infoc->resultadoestudiolaboratorio!=''){
            $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Estudios de Laboratorio</span><br>';
                $html.='<span style="font-size: 12px;">'.$get_infoc->resultadoestudiolaboratorio.'</span>';
        $html.='</td>
            </tr>';
            }
            $aux_dg=0;
            $arraydiag = array('idconsulta' => $idc,'activo'=>1);
            $get_diagn=$this->General_model->getselectwhereall('diagnosticos',$arraydiag);
            foreach($get_diagn as $key){
                $aux_dg=1;
            } 
            if($aux_dg==1){
            $html.='<tr style="font-size:100%;">
                        <td width="100%">';    
                $html.='<span style="font-size: 12px; font-weight: bold;">Diagnósticos</span>
                    <ol style="font-size: 12px;">';
                    foreach ($get_diagn as $item){ 
                    $html.='<li>'.$item->diagnostico.'</li>';
                    }
            $html.='</ol>';
            $html.='</td>
                </tr>'; 
            }  
            $arrayrece = array('idconsulta' => $idc,'activo'=>1);
            $get_receta_t=$this->General_model->getselectwhereall('receta',$arrayrece);
            $aux_v_r=0;
            foreach ($get_receta_t as $item){
                $aux_v_r=1;
            } 
            if($aux_v_r==1){
                $html.='<tr style="font-size:100%;">
                            <td width="100%">';    
                    
                        foreach ($get_receta_t as $item){ 
                            $arrayrecem = array('idreceta'=>$item->idreceta,'activo'=>1);
                            $get_receta_m=$this->General_model->getselectwhereall('receta_medicamento',$arrayrecem);
                        $html.='<span style="font-size: 12px; font-weight: bold;">Medicamentos</span>
                                <ol style="font-size: 12px;">';
                            foreach ($get_receta_m as $item){ 
                            $html.='<li>'.$item->medicamento.' - '.$item->tomar.'</li>';
                            }
                            $html.='</ol>';
                        }
                
                $html.='</td>
                    </tr>'; 
            }
            $arrayorde = array('idconsulta' => $idc,'activo'=>1);
            $get_orden_t=$this->General_model->getselectwhereall('orden_estudio',$arrayorde);
            $aux_o_r=0;
            foreach ($get_orden_t as $item){
                $aux_o_r=1;
            } 
            if($aux_o_r==1){ 

                $html.='<tr style="font-size:100%;">
                            <td width="100%">'; 
                    $html.='<span style="font-size: 12px; font-weight: bold;">Estudios Requeridos</span><br>';
                        foreach ($get_orden_t as $item){
                            $html.='<span style="font-size: 12px;">Orden de laboratorio</span>';
                                $obsev=$item->observaciones;
                        $html.='<ol style="font-size: 12px;">';
                                $arrayordenm = array('idorden'=>$item->idorden,'activo'=>1);
                                $get_orden_m=$this->General_model->getselectwhereall('orden_estudio_laboratorio',$arrayordenm);
                                foreach ($get_orden_m as $item){
                                    $html.='<li>'.$item->detalles.'</li>';
                                }  
                            $html.='</ol>';  
                            if($obsev!=''){
                                $html.='<span style="font-size: 12px;">Observaciones</span><br>';
                            $html.='<span style="font-size: 12px;">'.$obsev.'</span><br>';
                            }
                        }
                
                $html.='</td>
                    </tr>'; 
            }  
            $arrayorde = array('idconsulta' => intval($idc),'activo'=>1);          
            $get_orden_int=$this->General_model->getselectwhereall('interconsulta',$arrayorde);
            $aux_o_int=0;
            foreach ($get_orden_int as $item){
                $aux_o_int=1;
            }
            if($aux_o_r==1){
                $html.='<tr style="font-size:100%;">
                            <td width="100%">'; 
                    $html.='<span style="font-size: 12px; font-weight: bold;">Orden de interconsulta</span><br>';
                    foreach ($get_orden_int as $a){
                        $html.='<ul style="font-size: 12px;">';
                            $arrayordenint = array('idinterconsulta'=>$a->idinterconsulta,'activo'=>1);
                            $get_orden_int=$this->General_model->getselectwhereall('interconsulta_medico',$arrayordenint);
                            $medico_txt='';
                            foreach ($get_orden_int as $item){
                                $medico_txt='<strong>'.$item->nombre.'</strong> |'.$item->especialidad;
                            }
                            $html.='<li>'.$medico_txt.'</li>';
                        $html.='</ul>';
                        $html.='<span style="font-size: 12px;">'.$a->motivo_interconsulta.'</span><br>';
                    }   
                    $html.='</td>
                    </tr>'; 
            }
            $arrayorh = array('idconsulta' => intval($idc),'activo'=>1);           
            $get_orden_orh=$this->General_model->getselectwhereall('orden_hospital',$arrayorh);
            $aux_o_orh=0;
            foreach ($get_orden_orh as $item){
                $aux_o_orh=1;
            }
            if($aux_o_orh==1){
                $html.='<tr style="font-size:100%;">
                            <td width="100%">'; 
                foreach ($get_orden_orh as $a){
                    $html.='<span style="font-size: 12px; font-weight: bold;">Orden de hospitalizacion</span><br>';
                    $html.='<span style="font-size: 12px;">'.$a->detalles.'</span><br>';
                } 
                $html.='</td>
                    </tr>'; 
            }
            $arrayorh = array('idconsulta' => intval($idc),'activo'=>1);       
            $get_orden_orh=$this->General_model->getselectwhereall('certificado_medico',$arrayorh);
            $aux_o_orh=0;
            foreach ($get_orden_orh as $item){
                $aux_o_orh=1;
            }
            if($aux_o_orh==1){
                $html.='<tr style="font-size:100%;">
                            <td width="100%">'; 
                foreach ($get_orden_orh as $a){
                    $html.='<span style="font-size: 12px; font-weight: bold;">Certificado Médico</span><br>';
                    $html.='<span style="font-size: 12px;">'.$a->detalles.'</span><br>';
                }
                $html.='</td>
                    </tr>';      

            }
            $arrayorj = array('idconsulta' => intval($idc),'activo'=>1);
                                
            $get_orden_j=$this->General_model->getselectwhereall('orden_justificante_medico',$arrayorj);
            $aux_o_j=0;
            foreach ($get_orden_j as $item){
                $aux_o_j=1;
            }
            if($aux_o_j==1){
                $html.='<tr style="font-size:100%;">
                            <td width="100%">'; 
                foreach ($get_orden_j as $a){
                    $html.='<span style="font-size: 12px; font-weight: bold;">Justificante Médico</span><br>';
                    $html.='<span style="font-size: 12px;">'.$a->detalles.'</span><br>';
                }   
                $html.='</td>
                    </tr>';                                     
            }
            $arrayoro = array('idconsulta' => intval($idc),'activo'=>1);          
            $get_orden_o=$this->General_model->getselectwhereall('orden_libre',$arrayoro);
            $aux_o_o=0;
            foreach ($get_orden_o as $item){
                $aux_o_o=1;
            }
            if($aux_o_o==1){
                $html.='<tr style="font-size:100%;">
                            <td width="100%">'; 
                foreach ($get_orden_o as $a){
                    $html.='<span style="font-size: 12px; font-weight: bold;">Orden libre</span><br>';
                    $html.='<span style="font-size: 12px;">'.$a->detalles.'</span><br>';
                }     
                $html.='</td>
                    </tr>';     
            }
            $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Nombre del médico: Dr. DANTE OROPEZA CANTO - Última consulta: '.date('d/m/Y',strtotime($get_infoc->consultafecha)).' '.date('G:i:s',strtotime($get_infoc->horainicio)).'</span><br>';
        $html.='</td>
            </tr>';
  $html.='</table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('laboratorio.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
?>
