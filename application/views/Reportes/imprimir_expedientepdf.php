<?php

    require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF/tcpdf.php');
    $this->load->helper('url');
    //var_dump($GLOBALS['folio']);die;
//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        $img_file = base_url().'images/formato/portada_header.jpg'; 
        //$pdf->Image($img_file, 0, 0, 0, 500, '', '', '', false, 500, '', false, false, 0); 
        $this->Image($img_file, 0, 0, 210, 40, '', '', '', false, 330, '', false, false, 0); 

       $html = '<table width="100%" border="0"> 
                    <tr> 
                        <td width="100%" height="80px"></td> 
                    </tr> 
                </table> 
                <table width="100%" border="0"> 
                    <tr> 
                        <td width="20%"></td> 
                        <td width="20%" style="text-align: left;"></td> 
                        <td width="60%" style="text-align: right;"> 
                            <div style="font-weight: bold; font-size: 20px;">Expediente</div>    
                        </td> 
                    </tr> 
                </table>'; 
        $this->writeHTML($html, true, false, true, false, ''); 
    }
    // Page footer
    public function Footer() {
        $img_file = base_url().'images/formato/portada_footer.jpg'; 
        //$pdf->Image($img_file, 0, 0, 0, 500, '', '', '', false, 500, '', false, false, 0); 
        $this->Image($img_file, 0, 280, 210, 18, '', '', '', false, 330, '', false, false, 0); 
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo');
$pdf->SetTitle('Expediente');
$pdf->SetSubject('Expediente');
$pdf->SetKeywords('Expediente');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('12', '50', '12'); 
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('60'); 
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans',13);
// add a page
$pdf->AddPage('P', 'A4');
  $html='<table>';
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
            $html.='<span style="font-size: 12px; font-weight: bold;">Datos del Médico</span>';
        $html.='</td>
            </tr>';

    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
            $html.='<span style="font-size: 12px;">Nombre: Karla Coello</span>';
        $html.='</td>
            </tr>';

    $html.='<tr style="font-size:10%;">
                <td width="100%" style="border-bottom: 2px solid #779155;">
                </td>
            </tr>
            <tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
            $html.='<span style="font-size: 12px; font-weight: bold;">Datos del Paciente</span>';
        $html.='</td>
            </tr>
            <tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
            $html.='<span  style="font-size: 12px;">Nombre del paciente: '.$get_infop->nombre.' '.$get_infop->apll_paterno.' '.$get_infop->apll_materno.'</span>';
        $html.='</td>
            </tr>
            <tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="40%">'; 
            $html.='<span  style="font-size: 12px;">Fecha de nacimiento: '.date('d/m/Y',strtotime($get_infop->fecha_nacimiento)).'</span>';
        $html.='</td>
                <td width="30%">'; 
                    $tipo_sexo='';
                    if($get_infop->sexo==1){
                        $tipo_sexo='Hombre';
                    }else if($get_infop->sexo==2){
                        $tipo_sexo='Mujer';
                    }
                    if($get_infop->sexo!=0){    
                        $html.='<span  style="font-size: 12px;">Género: '.$tipo_sexo.'</span>';
                    }
        $html.='</td>
                <td width="30%">';
                    if($get_infop->correo!=''){    
                    $html.='<span style="font-size: 12px;">Mail: '.$get_infop->correo.'</span>';
                    }
                $html.='</td>
            </tr>';

            if($get_infop->celular!=0){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
            $html.='<span  style="font-size: 12px;">Celular: '.$get_infop->celular.'</span>';
        $html.='</td>
            </tr>';
            }

            if($get_infop->estado_civil!=0 || $get_infop->ocupacion!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">';
                $ocupacion_txt='';
                if($get_infop->estado_civil==1){
                    $ocupacion_txt='Soltero';
                }else if($get_infop->estado_civil==2){ 
                    $ocupacion_txt='Casado';
                }else if($get_infop->estado_civil==3){ 
                    $ocupacion_txt='Divorciado';
                }else if($get_infop->estado_civil==4){ 
                    $ocupacion_txt='Viudo';
                }else if($get_infop->estado_civil==5){ 
                    $ocupacion_txt='Unión Libre';
                }
                if($get_infop->estado_civil!=0){
                    $html.='<td width="20%">'; 
                        $html.='<span  style="font-size: 12px;">Estado Civil:</span><br><span style="font-size: 12px;">'.$ocupacion_txt.'</span>';
                    $html.='</td>';
                }
                if($get_infop->ocupacion!=''){
                $html.='<td width="20%">
                    <span style="font-size: 12px;">Ocupación:</span><br>
                    <span style="font-size: 12px;">'.$get_infop->ocupacion.'</span>
                    </td>';
                }
    $html.='</tr>';
            }

            if($get_infop->celular!=0){
    $html.='<tr style="font-size:10%;">
                <td width="100%" style="border-bottom: 2px solid #779155;">
                </td>
            </tr>';
            }
            if($antecedentes!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
            $html.='<span style="font-size: 12px; font-weight: bold;">Antecedentes Importantes</span>';
        $html.='</td>
            </tr>
            <tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
            $html.='<span style="font-size: 12px;">'.$antecedentes.'</span>';
        $html.='</td>
            </tr>';
            }

            if($alergias!=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
            $html.='<span style="font-size: 12px; font-weight: bold;">Alergias</span>';
        $html.='</td>
            </tr>
            <tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
            $html.='<span style="font-size: 12px;">'.$alergias.'</span>';
        $html.='</td>
            </tr>';
            }
            if($quien_refiere!=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
            $html.='<span style="font-size: 12px; font-weight: bold;">Quién refiere al paciente</span>';
        $html.='</td>
            </tr>
            <tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
            $html.='<span style="font-size: 12px;">'.$quien_refiere.'</span>';
        $html.='</td>
            </tr>';
            }
            if($motivo_consulta!=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
            $html.='<span style="font-size: 12px; font-weight: bold;">Motivo de la Consulta</span>';
        $html.='</td>
            </tr>
            <tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
            $html.='<span style="font-size: 12px;">'.$motivo_consulta.'</span>';
        $html.='</td>
            </tr>';
            }
            if($observaciones!=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
            $html.='<span style="font-size: 12px; font-weight: bold;">Observaciones sobre el paciente</span>';
        $html.='</td>
            </tr>
            <tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
            $html.='<span style="font-size: 12px;">'.$observaciones.'</span>';
        $html.='</td>
            </tr>';
            }

        if($antecedentes!='' || $alergias!='' || $quien_refiere!='' || $motivo_consulta!='' || $observaciones!=''){
    $html.='<tr style="font-size:10%;">
                <td width="100%" style="border-bottom: 2px solid #779155;">
                </td>
            </tr>';
        }

    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px; font-weight: bold;">Historia clínica</span>';
        $html.='</td>
            </tr>';

            if($diabetes!='' || $cancer!='' || $hipertencion!='' || $otro_antecedente!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px; font-weight: bold;">Heredofamiliares (Abuelos, padres, hijos)</span>';
        $html.='</td>
            </tr>';
            }
            if($diabetes!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Diabetes mellitus: '.$diabetes.'</span>';
        $html.='</td>
            </tr>';
            } 
            if($cancer!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Cáncer: '.$cancer.'</span>';
        $html.='</td>
            </tr>';
            }
            if($hipertencion!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Hipertensión arterial: '.$hipertencion.'</span>';
        $html.='</td>
            </tr>';
            }
            if($otro_antecedente!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Otro antecedente importante: '.$otro_antecedente.'</span>';
        $html.='</td>
            </tr>';
            }
    
            if($fruta!='' || $carnes_rojas!='' || $cerdos!='' || $pollo!='' || $lacteos!='' || $lacteos!='' || $harina!='' || $aguas!='' || $chatarras!=''){ 
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 13px; font-weight: bold;">A. Personales NO Patológicos</span>';
        $html.='</td>
            </tr>';
            }
            $cpser='';
            if($chetpersonal_servicios==1){
                $cpser='Si';
            }else{
                $cpser='No';
            }
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">¿Cuenta con todos los servicios? '.$cpser.'</span>';
        $html.='</td>
            </tr>';
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 13px;">Alimentación</span>';
        $html.='</td>
            </tr>';
            if($fruta!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Frutas y vegetales: '.$fruta.'</span>';
        $html.='</td>
            </tr>';
            }
            if($carnes_rojas!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Carnes rojas: '.$carnes_rojas.'</span>';
        $html.='</td>
            </tr>';
            }
            if($cerdos!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Cerdos: '.$cerdos.'</span>';
        $html.='</td>
            </tr>';
            }
            if($pollo!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Pollo / Pescado: '.$pollo.'</span>';
        $html.='</td>                   
            </tr>';
            }
            if($lacteos!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Lacteos: '.$lacteos.'</span>';
        $html.='</td>                    
        </tr>';
            }
            if($harina!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px;">Harinas refinadas: '.$harina.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($aguas!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px;">Agua: '.$aguas.'</span>';
            $html.='</td>                    
            </tr>';
            }    
            if($chatarras!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px;">Chatarra / Gaseosa: '.$chatarras.'</span>';
            $html.='</td>                    
            </tr>';
            }              
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 13px;">Alcohol</span>';
        $html.='</td>
            </tr>';
            if($cuando_inicio_alcohol!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">¿Cuándo inició? '.$cuando_inicio_alcohol.'</span>';
        $html.='</td>                    
            </tr>';
            }
            if($cada_cuando_alcohol!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px;">¿Cada cuánto? '.$cada_cuando_alcohol.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($cuantos_dia_alcohol!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px;">¿Cuántos al día? '.$cuantos_dia_alcohol.'</span>';
            $html.='</td>                    
            </tr>';
            }
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 13px;">Tabaco</span>';
            $html.='</td>
                </tr>';
            if($cuando_inicio_tabaco!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px;">¿Cuándo inició? '.$cuando_inicio_tabaco.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($cada_cuando_tabaco!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px;">¿Cada cuánto? '.$cada_cuando_tabaco.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($cuantos_dia_tabaco!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px;">¿Cuántos al día? '.$cuantos_dia_tabaco.'</span>';
            $html.='</td>                    
            </tr>';
            }
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 13px;">Drogas</span>';
            $html.='</td>
            </tr>';
            if($cuando_inicio_drogas!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px;">¿Cuándo inició? '.$cuando_inicio_drogas.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($cada_cuando_drogas!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px;">¿Cada cuánto? '.$cada_cuando_drogas.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($cuantos_dia_drogas!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px;">¿Cuántos al día? '.$cuantos_dia_drogas.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($inmunizaciones!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px;">Inmunizaciones actual y previa '.$inmunizaciones.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($desparasitacion!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Última desparasitación '.$desparasitacion.'</span>';
            $html.='</td>                    
            </tr>';
            }

    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 13px; font-weight: bold;">Ginecobstetricos</span>';
        $html.='</td>
            </tr>';
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span style="font-size: 12px;">Menarca <u>'.$marcar.'</u> Ciclo menstrual <u>'.$mestrual.'</u> 
                            Dismenorrea <u>'.$dismenorrea.'</u> 
                            Última menstruación <u>'.$menstruacion.'</u>
                            Inicio de vida sexual activa <u>'.$sexual.'</u> 
                            No. parejas <u>'.$parejas.'</u> 
                            Embarazos:<u>'.$embarazos.'</u> 
                            Abortos <u>'.$abortos.'</u> Cesáreas <u>'.$cesareas.'</u> Lactancia ( ) <u>'.$lactancia.'</u> 
                            Métodos anticonceptivos <u>'.$anticonceptivos.'</u> 
                            ETS <u>ets</u> Menopausia <u>'.$menopausia.'</u> 
                            Climaterio <u>'.$climaterio.'</u> 
                            Papanicolau <u>'.$papanicolau.'</u>
                            Mastografía <u>'.$mastografia.'</u>
                        </span>';
            $html.='</td>                    
            </tr>';
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 13px; font-weight: bold;">A. Personales Patológicos</span>';
        $html.='</td>
            </tr>';
            if($medicamentos_deprecion!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">¿Tomas medicamentos contra ansiedad depresión o dormir? / ¿Cúal? Dosis '.$medicamentos_deprecion.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($cirugia!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Cirugías: '.$cirugia.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($otras_enfemedades!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Otras enfermedades importantes: '.$otras_enfemedades.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($transfusiones!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Transfusiones: '.$transfusiones.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($hospitalizacion!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Hospitalizaciones: '.$hospitalizacion.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($alergias!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Alergias(gravedad): '.$alergias.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($fracturas!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Fracturas o lesiones '.$fracturas.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($embarazo_lactancia!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">¿Podrías estar embarazada o en lactancia? / Fecha de última regla '.$embarazo_lactancia.'</span>';
            $html.='</td>                    
            </tr>';
            }
            if($chehepatitisa!='0' || $chehepatitisb!='0' || $chehepatitisc!='0'){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Hepatitis</span>';
        $html.='</td>
            </tr>';
            }
            if($chehepatitisa==1){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Hepatitis A</span>';
        $html.='</td>
            </tr>';
            }
            if($chehepatitisb==1){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Hepatitis B</span>';
        $html.='</td>
            </tr>';
            }
            if($chehepatitisc==1){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Hepatitis C</span>';
        $html.='</td>
            </tr>'; 
            }
            if($hepatitis!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">'.$hepatitis.'</span>';
        $html.='</td>
            </tr>'; 
            }
             
            if($diabetesmillitus!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Diabetes mellitus: '.$diabetesmillitus.'</span>';
        $html.='</td>
            </tr>';
            }
            if($hipertensionarterial!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Hipertensión arterial: '.$hipertensionarterial.'</span>';
        $html.='</td>
            </tr>';
            }
            if($hipercolesterolemia==1){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Hipercolesterolemia</span>';
        $html.='</td>
            </tr>'; 
            }
            if($epilepsia==1){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Epilepsia</span>';
        $html.='</td>
            </tr>'; 
            }
            if($enfermedades_musculares==1){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Enfermedades musculares</span>';
        $html.='</td>
            </tr>'; 
            }
            if($interrogatorio_aparatos!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px; font-weight: bold;">Interrogatorio por Aparatos y Sistemas</span>';
        $html.='</td>
            </tr>';
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">'.$interrogatorio_aparatos.'</span>';
        $html.='</td>
            </tr>';
            } 
            if($medicamento!=''){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px; font-weight: bold;">Medicamentos</span>';
        $html.='</td>
            </tr>';
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">'.$medicamento.'</span>';
        $html.='</td>
            </tr>';
            } 
    $html.='<tr style="font-size:10%;">
                <td width="100%" style="border-bottom: 2px solid #779155;">
                </td>
            </tr>';
            ///  Interrogatorio por sistemas
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px; font-weight: bold;">Interrogatorio por sistemas</span>';
        $html.='</td>
            </tr>';
            if($fiebre!='' || $astenia='' || $aumento!='' || $modificacion!=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px; font-weight: bold;">Síntomas generales</span>';
            $html.='</td>
                </tr>';
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span style="font-size: 12px;">';
                            if($fiebre!=''){
                                $html.='Fiebre <u>'.$fiebre.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($astenia!=''){
                                $html.='Astenia(debilidad o fatiga general) <u>'.$astenia.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($aumento!=''){
                                $html.='Aumento o pérdida de peso <u>'.$aumento.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($modificacion!=''){
                                $html.='Modificaciones del hambre <u>'.$modificacion.'</u> ';
                            }
                    $html.='</span>';
                $html.='</td>                    
                </tr>';
            }
            if($rinorrea!='' || $epistaxis='' || $tos!='' || $expectoracion!='' || $disfonia='' || $hemoptitis!='' || $cianosis!='' || $dolor='' || $disnea!='' || $sibilancias!=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px; font-weight: bold;">Aparato Respiratorio</span>';
            $html.='</td>
                </tr>';
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span style="font-size: 12px;">';
                            if($rinorrea!=''){
                                $html.='Rinorrea (nariz congestionada) <u>'.$rinorrea.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($epistaxis!=''){
                                $html.='Epistaxis (sangrado) <u>'.$epistaxis.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($tos!=''){
                                $html.='Tos <u>'.$tos.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($expectoracion!=''){
                                $html.='Expectoración (expulsar fluidos por la boca) <u>'.$expectoracion.'</u> ';
                            }
                            if($disfonia!=''){
                                $html.='Disfonía (pérdida del timbre normal de voz) <u>'.$disfonia.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($hemoptitis!=''){
                                $html.='Hemoptisis (expulsión de pequeñas cantidades de sangre) <u>'.$hemoptitis.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($cianosis!=''){
                                $html.='Cianosis (coloración azul de la piel y las mucosas) <u>'.$cianosis.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($dolor!=''){
                                $html.='Dolor torácico <u>'.$dolor.'</u> ';
                            }
                            if($disnea!=''){
                                $html.='Disnea (Dificultad para respirar) <u>'.$disnea.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($sibilancias!=''){
                                $html.='Sibilancias audibles a distancia <u>'.$sibilancias.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                    $html.='</span>';
                $html.='</td>                    
                </tr>';
            }
            if($hambre3!='' || $apetito3='' || $masticacion3!='' || $disfagia3!='' || $halitosis3='' || $nausea3!='' || $rumiacion3!='' || $pirosis3='' || $aerofagia3!='' || $eructos3!='' || $meteorismo3!='' || $distension3!='' || $gases3!='' || $hematemesis3!='' || $ictericia3!='' || $fecales3!='' || $constipacion3!='' || $haces_blancas3!='' || $verdes3!='' || $haces_negras3!='' || $amarillas3!='' || $rojas3!='' || $esteatorrea_aceitosas3!='' || $parsitos3!='' || $pujo3!='' || $lienteria3!='' || $tenesmo3!='' || $prurito3!=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px; font-weight: bold;">Digestivo</span>';
            $html.='</td>
                </tr>';
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span style="font-size: 12px;">';
                            if($hambre3!=''){
                                $html.='Hambre <u>'.$hambre3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($apetito3!=''){
                                $html.='Apetito (sangrado) <u>'.$apetito3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($masticacion3!=''){
                                $html.='Alteraciones de la masticación y salivación <u>'.$masticacion3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($disfagia3!=''){
                                $html.='Disfagia (Dificultad para tragar) <u>'.$disfagia3.'</u> ';
                            }
                            if($halitosis3!=''){
                                $html.='Halitosis (Mal aliento) <u>'.$halitosis3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($nausea3!=''){
                                $html.='Náusea <u>'.$nausea3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($rumiacion3!=''){
                                $html.='Rumiación (Devolver los alimentos) <u>'.$rumiacion3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($pirosis3!=''){
                                $html.='Pirosis (Sensación de quemadura desde la faringe hasta el stómago <u>'.$pirosis3.'</u> ';
                            }
                            if($aerofagia3!=''){
                                $html.='Aerofagia (Ingestión de aire que provoca gases o dolor estomacal) <u>'.$aerofagia3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($eructos3!=''){
                                $html.='Eructos <u>'.$eructos3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($meteorismo3!=''){
                                $html.='Meteorismo (Acumulación de gases) <u>'.$meteorismo3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($distension3!=''){
                                $html.='Distensión abdominal (Sentirse lleno o apretado provocando molestia) <u>'.$distension3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($gases3!=''){
                                $html.='Gases <u>'.$gases3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($hematemesis3!=''){
                                $html.='Hematemesis (Vómito de sangre origen estómago) <u>'.$hematemesis3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($ictericia3!=''){
                                $html.='Ictericia (Coloración amarillenta de la piel y mucosas <u>'.$ictericia3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($fecales3!=''){
                                $html.='Características de heces fecales (diarrea <u>'.$fecales3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($constipacion3!=''){
                                $html.='/ constipación (estreñimiento) <u>'.$constipacion3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($haces_blancas3!=''){
                                $html.='/ heces blancas <u>'.$haces_blancas3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($verdes3!=''){
                                $html.='/ verdes <u>'.$verdes3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($haces_negras3!=''){
                                $html.='/ heces negras <u>'.$haces_negras3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($amarillas3!=''){
                                $html.='Amarillas <u>'.$amarillas3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($rojas3!=''){
                                $html.='Rojas <u>'.$rojas3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($esteatorrea_aceitosas3!=''){
                                $html.='Esteatorrea (heces mal olientes o aceitosas) Rectorragia (Pérdida de sangre a través del ano) <u>'.$esteatorrea_aceitosas3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($parsitos3!=''){
                                $html.='Parásitos <u>'.$parsitos3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($pujo3!=''){
                                $html.='Pujo (dolor en abdomen que provoca ganas de evacuar u orinar) <u>'.$pujo3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($lienteria3!=''){
                                $html.='Lienteria (debilitamiento total o parcial de los intestinos) <u>'.$lienteria3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($tenesmo3!=''){
                                $html.='Tenesmo(Sensación de defecar continuamente) <u>'.$tenesmo3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($prurito3!=''){
                                $html.='Prurito anal (Picazón anal) <u>'.$prurito3.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                    $html.='</span>';
                $html.='</td>                    
                </tr>';
            }
            if($palpitaciones4!='' || $dolor_precordial4='' || $disnea_esfuerzo4!='' || $disnea_paroxistica4!='' || $apnea4='' || $cianosis4!='' || $acufenos4!='' || $fosfenos4='' || $sincope4!='' || $lipotimias4!='' || $edema4!=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px; font-weight: bold;">Cardiovascular</span>';
            $html.='</td>
                </tr>';
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span style="font-size: 12px;">';
                            if($palpitaciones4!=''){
                                $html.='Palpitaciones <u>'.$palpitaciones4.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($dolor_precordial4!=''){
                                $html.='Dolor precordial(dolor en el pecho) <u>'.$dolor_precordial4.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($disnea_esfuerzo4!=''){
                                $html.='Disnea de esfuerzo(dificultad para respirar al realizar alguna acción) <u>'.$disnea_esfuerzo4.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($disnea_paroxistica4!=''){
                                $html.='Disnea paroxística(dificultad para respirar al estar acostado) <u>'.$disnea_paroxistica4.'</u> ';
                            }
                            if($apnea4!=''){
                                $html.='Apnea(dificultad para respirar al dormir) <u>'.$apnea4.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($cianosis4!=''){
                                $html.='Cianosis(coloración azulada o violácea general) <u>'.$cianosis4.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($acufenos4!=''){
                                $html.='Acúfenos(zumbidos de oído o ruidos en la cabeza) <u>'.$acufenos4.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($fosfenos4!=''){
                                $html.='Fosfenos(percepción de un destellos luminosos) <u>'.$fosfenos4.'</u> ';
                            }
                            if($sincope4!=''){
                                $html.='Síncope(desmayo) <u>'.$sincope4.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($lipotimias4!=''){
                                $html.='Lipotimias(desvanecimiento) <u>'.$lipotimias4.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($edema4!=''){
                                $html.='Edema <u>'.$edema4.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                    $html.='</span>';
                $html.='</td>                    
                </tr>';
            }
            if($renoureteral5!='' || $disuria5='' || $anuria5!='' || $oliguria5!='' || $poliuria5='' || $polaquiuria5!='' || $hematuria5!='' || $piuria5='' || $coluria5!='' || $incontinencia5!='' || $edema5!=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px; font-weight: bold;">Renal y Urinario</span>';
            $html.='</td>
                </tr>';
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span style="font-size: 12px;">';
                            if($renoureteral5!=''){
                                $html.='Dolor renoureteral(dolor o cólico en genitales por obstrucción) <u>'.$renoureteral5.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($disuria5!=''){
                                $html.='Disuria(dolor al momento de evacuar) <u>'.$disuria5.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($anuria5!=''){
                                $html.='Anuria(ausencia total de orina) <u>'.$anuria5.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($oliguria5!=''){
                                $html.=' Oliguria(disminución anormal de orina) <u>'.$oliguria5.'</u> ';
                            }
                            if($poliuria5!=''){
                                $html.='Poliuria(excreción abundante de orina) <u>'.$poliuria5.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($polaquiuria5!=''){
                                $html.='Polaquiuria (necesidad de orinar muchas veces) <u>'.$polaquiuria5.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($hematuria5!=''){
                                $html.='Hematuria(presencia de sangre en la orina) <u>'.$hematuria5.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($piuria5!=''){
                                $html.='Piuria(presencia de pus en la orina) <u>'.$piuria5.'</u> ';
                            }
                            if($coluria5!=''){
                                $html.='Coluria(presencia de bilis en la orina) <u>'.$coluria5.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($incontinencia5!=''){
                                $html.='Incontinencia <u>'.$incontinencia5.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($edema5!=''){
                                $html.='Edema(exceso de líquido en alguna zona) <u>'.$edema5.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                    $html.='</span>';
                $html.='</td>                    
                </tr>';
            }
            if($leucorrea6!='' || $hemorragias6='' || $alteraciones_menstruales6!='' || $alteracioneslibido6!='' || $practica_sexual6='' || $alteraciones_sangrado6!='' || $alteraciones_sangrado6!='' || $dispareunia6='' || $perturbaciones6!=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px; font-weight: bold;">Genital femenino</span>';
            $html.='</td>
                </tr>';
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span style="font-size: 12px;">';
                            if($leucorrea6!=''){
                                $html.='Leucorrea(flujo abundante) <u>'.$leucorrea6.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($hemorragias6!=''){
                                $html.='Hemorragias transvaginales <u>'.$hemorragias6.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($alteraciones_menstruales6!=''){
                                $html.='Alteraciones menstruales <u>'.$alteraciones_menstruales6.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($alteracioneslibido6!=''){
                                $html.='Alteraciones de la libido <u>'.$alteracioneslibido6.'</u> ';
                            }
                            if($practica_sexual6!=''){
                                $html.='Práctica sexual <u>'.$practica_sexual6.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($alteraciones_sangrado6!=''){
                                $html.='Alteraciones del sangrado menstrual <u>'.$alteraciones_sangrado6.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($dispareunia6!=''){
                                $html.='Dispareunia(dolor o molestia, después o durante la actividad sexual) <u>'.$dispareunia6.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($perturbaciones6!=''){
                                $html.='Perturbaciones y alteraciones sexuales <u>'.$perturbaciones6.'</u> ';
                            }
                    $html.='</span>';
                $html.='</td>                    
                </tr>';
            }
            if($intolerancia_frio7!='' || $hiperactividad7='' || $aumento_volumen7!='' || $polidipsia7!='' || $polifagia7='' || $poliuria7!='' || $cambios_caracteres7!='' || $aumento_perdida7=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px; font-weight: bold;">Endocrino</span>';
            $html.='</td>
                </tr>';
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span style="font-size: 12px;">';
                            if($intolerancia_frio7!=''){
                                $html.='Intolerancia al frío y al calor <u>'.$intolerancia_frio7.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($hiperactividad7!=''){
                                $html.='Hiperactividad <u>'.$hiperactividad7.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($aumento_volumen7!=''){
                                $html.='Aumento de volumen del cuello <u>'.$aumento_volumen7.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($polidipsia7!=''){
                                $html.='Polidipsia(aumento anormal de sed) <u>'.$polidipsia7.'</u> ';
                            }
                            if($polifagia7!=''){
                                $html.='Polifagia(aumento anormal de comer) <u>'.$polifagia7.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($poliuria7!=''){
                                $html.='Poliuria(excresión abundante de orina) <u>'.$poliuria7.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($cambios_caracteres7!=''){
                                $html.='Cambios en los caracteres sexuales secundarios <u>'.$cambios_caracteres7.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($aumento_perdida7!=''){
                                $html.='Aumento o pérdida de peso <u>'.$aumento_perdida7.'</u> ';
                            }
                    $html.='</span>';
                $html.='</td>                    
                </tr>';
            }
            if($palidez8!='' || $disnea8='' || $fatigabilidad8!='' || $palpitaciones8!='' || $sangrado8='' || $equimosis8!='' || $petequia8!='' || $astenia8=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px; font-weight: bold;">Hematopoyético y linfático</span>';
            $html.='</td>
                </tr>';
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span style="font-size: 12px;">';
                            if($palidez8!=''){
                                $html.='Palidez <u>'.$palidez8.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($disnea8!=''){
                                $html.='Disnea(dificultad para respirar) <u>'.$disnea8.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($fatigabilidad8!=''){
                                $html.='Fatigabilidad <u>'.$fatigabilidad8.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($palpitaciones8!=''){
                                $html.='Palpitaciones <u>'.$palpitaciones8.'</u> ';
                            }
                            if($sangrado8!=''){
                                $html.='Sangrado <u>'.$sangrado8.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($equimosis8!=''){
                                $html.='Equimosis(moretones) <u>'.$equimosis8.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($petequia8!=''){
                                $html.='Petequias(lesiones pequeñas rojas) <u>'.$petequia8.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($astenia8!=''){
                                $html.='Astenia(debilidad general) <u>'.$astenia8.'</u> ';
                            }
                    $html.='</span>';
                $html.='</td>                    
                </tr>';
            }
            if($mialgias9!='' || $dolor_oseo9='' || $artralgias9!='' || $alteraciones9!='' || $disminucion_volumen9='' || $limitacion_movimiento9!='' || $deformacion9=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px; font-weight: bold;">Músculo esquelético</span>';
            $html.='</td>
                </tr>';
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span style="font-size: 12px;">';
                            if($mialgias9!=''){
                                $html.='Mialgias (dolor muscular) <u>'.$mialgias9.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($dolor_oseo9!=''){
                                $html.='Dolor óseo <u>'.$dolor_oseo9.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($artralgias9!=''){
                                $html.='Artralgias (Dolor articulaciones) <u>'.$artralgias9.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($alteraciones9!=''){
                                $html.='Alteraciones en la marcha <u>'.$alteraciones9.'</u> ';
                            }
                            if($disminucion_volumen9!=''){
                                $html.='Disminución del volumen muscular <u>'.$disminucion_volumen9.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($limitacion_movimiento9!=''){
                                $html.='Limitación del movimiento <u>'.$limitacion_movimiento9.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($deformacion9!=''){
                                $html.='Deformaciones <u>'.$deformacion9.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                    $html.='</span>';
                $html.='</td>                    
                </tr>';
            }
            if($cefaleas10!='' || $paresias10='' || $parestesias10!='' || $movimientos_anormales10!='' || $alteraciones_marcha10='' || $vertigo10!='' || $mareos10!=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px; font-weight: bold;">Nervioso</span>';
            $html.='</td>
                </tr>';
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span style="font-size: 12px;">';
                            if($cefaleas10!=''){
                                $html.='Cefaleas (Dolor de cabeza) <u>'.$cefaleas10.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($paresias10!=''){
                                $html.='Paresias (Parálisis) <u>'.$paresias10.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($parestesias10!=''){
                                $html.='Parestesias (Adormecimiento manos, miembros, etc) <u>'.$parestesias10.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($movimientos_anormales10!=''){
                                $html.='Movimientos anormales <u>'.$movimientos_anormales10.'</u> ';
                            }
                            if($alteraciones_marcha10!=''){
                                $html.='Alteraciones de la marcha <u>'.$alteraciones_marcha10.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($vertigo10!=''){
                                $html.='Vértigo <u>'.$vertigo10.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($mareos10!=''){
                                $html.='Mareos <u>'.$mareos10.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                    $html.='</span>';
                $html.='</td>                    
                </tr>';
            }
            if($alteraciones_vision11!='' || $audicion11='' || $olfato11!='' || $gusto11!='' || $tacto11='' || $mareos11!='' || $sensaciones11!=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px; font-weight: bold;">Órganos de los sentidos</span>';
            $html.='</td>
                </tr>';
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span style="font-size: 12px;">';
                            if($alteraciones_vision11!=''){
                                $html.='Alteraciones de las visión <u>'.$alteraciones_vision11.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($audicion11!=''){
                                $html.='Audición <u>'.$audicion11.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($olfato11!=''){
                                $html.='Olfato <u>'.$olfato11.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($gusto11!=''){
                                $html.='Gusto <u>'.$gusto11.'</u> ';
                            }
                            if($tacto11!=''){
                                $html.='Tacto <u>'.$tacto11.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($mareos11!=''){
                                $html.='Mareos <u>'.$mareos11.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($sensaciones11!=''){
                                $html.='Sensaciones de líquido en el oído <u>'.$sensaciones11.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                    $html.='</span>';
                $html.='</td>                    
                </tr>';
            }
            if($tristeza12!='' || $euforia12='' || $alteraciones_sueno12!='' || $terrores_nocturnos12!='' || $ideaciones12!='' || $miedo_exagerado12='' || $irritabilidad12!='' || $apatia12!='' || $relaciones_personales12=''){
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px; font-weight: bold;">Esfera psíquica</span>';
            $html.='</td>
                </tr>';
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span style="font-size: 12px;">';
                            if($tristeza12!=''){
                                $html.='Tristeza <u>'.$tristeza12.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($euforia12!=''){
                                $html.='Euforia <u>'.$euforia12.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($alteraciones_sueno12!=''){
                                $html.='Alteraciones del sueño <u>'.$alteraciones_sueno12.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($terrores_nocturnos12!=''){
                                $html.='Terrores nocturnos <u>'.$terrores_nocturnos12.'</u> ';
                            }
                            if($ideaciones12!=''){
                                $html.='Ideaciones <u>'.$ideaciones12.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($miedo_exagerado12!=''){
                                $html.='Miedo exagerado <u>'.$miedo_exagerado12.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($irritabilidad12!=''){
                                $html.='Irritabilidad <u>'.$irritabilidad12.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($apatia12!=''){
                                $html.='Apatía <u>'.$apatia12.'</u> ';
                            }
                            if($relaciones_personales12!=''){
                                $html.='Relaciones personales <u>'.$relaciones_personales12.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                    $html.='</span>';
                $html.='</td>                    
                </tr>';
            }
            if($coloracion_anormal13!='' || $pigmentacion13='' || $prurito13!='' || $caracteristicas13!='' || $unas13='' || $hiperhidrosis13!='' || $xerodermia13!=''){
            
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px; font-weight: bold;">Tegumentario</span>';
            $html.='</td>
                </tr>';
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span style="font-size: 12px;">';
                            if($coloracion_anormal13!=''){
                                $html.='Coloración anormal <u>'.$coloracion_anormal13.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($pigmentacion13!=''){
                                $html.='Pigmentación <u>'.$pigmentacion13.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($prurito13!=''){
                                $html.='Prurito(Hormigueo o irritación) <u>'.$prurito13.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($caracteristicas13!=''){
                                $html.='Características del pelo (seco, graso, normal. lacio, ondulado, chino) <u>'.$caracteristicas13.'</u> ';
                            }
                            if($unas13!=''){
                                $html.='Uñas (firmes, quebradizas) <u>'.$unas13.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($hiperhidrosis13!=''){
                                $html.='Hiperhidrosis (exceso de sudoración) <u>'.$hiperhidrosis13.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($xerodermia13!=''){
                                $html.='Xerodermia (resequedad) <u>'.$xerodermia13.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                    $html.='</span>';
                $html.='</td>                    
                </tr>';
            }
            /*
            if($xxx!='' || $xxx='' || $xxx!='' || $xxx!='' || $xxx='' || $xxx!='' || $xxx!='' || $xxx='' || $xxx!='' || $xxx!='' || $xxx!=''){
            
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px; font-weight: bold;">xxxx</span>';
            $html.='</td>
                </tr>';
        $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span style="font-size: 12px;">';
                            if($xxx!=''){
                                $html.='xxx <u>'.$xxx.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($xxx!=''){
                                $html.='xxx <u>'.$xxx.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($xxxxxx!=''){
                                $html.='xxx <u>'.$xxx.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($xxx!=''){
                                $html.='xxx <u>'.$xxx.'</u> ';
                            }
                            if($xxx!=''){
                                $html.='xxx <u>'.$xxx.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($xxx!=''){
                                $html.='xxx <u>'.$xxx.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($xxx!=''){
                                $html.='xxx <u>'.$xxx.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                            if($xxx!=''){
                                $html.='xxx <u>'.$xxx.'</u> ';
                            }
                            if($xxx!=''){
                                $html.='xxx <u>'.$xxx.'</u>&nbsp;&nbsp;&nbsp;&nbsp; '; 
                            }
                            if($xxx!=''){
                                $html.='xxx <u>'.$xxx.'</u>&nbsp;&nbsp;&nbsp;&nbsp; ';
                            }
                    $html.='</span>';
                $html.='</td>                    
                </tr>';
            }
            */
    $html.='<tr style="font-size:10%;">
                <td width="100%" style="border-bottom: 2px solid #779155;">
                </td>
            </tr>';
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px; font-weight: bold;">Consultas</span>';
        $html.='</td>
            </tr>';
            $tiempo = strtotime($get_infop->fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
                $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                    <td width="100%">'; 
                    $html.='<span  style="font-size: 12px;">Edad del paciente: '.floor($edad).' años</span>';
            $html.='</td>';
    $html.='</tr>';
    if($get_m!=null){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px; font-weight: bold;">Resumen de última consulta de medicina estética</span>';
        $html.='</td>
            </tr>';
    $html.='<tr style="font-size:10%;">
                <td width="100%" style="border-bottom: 2px solid #779155;">
                </td>
            </tr>';
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Fecha de consulta: '.date('d/m/Y',strtotime($get_m->consultafecha)).'</span>';
        $html.='</td>
            </tr>';  
    $html.='<tr>
                <td width="100%">';
                $html.='<span  style="font-size: 12px; font-weight: bold;">Motivo de la consulta</span><br>
                        <span  style="font-size: 12px;">'.$get_m->motivo_consulta.'</span><br>';
        $html.='</td>
            </tr>';
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span  style="font-size: 12px; font-weight: bold;">Nota de Evolución</span><br>
                        <span  style="font-size: 12px;">'.$get_m->nota_evaluacion.'</span><br>';
        $html.='</td>
            </tr>';
        if($get_m->altrura!=0  || $get_m->peso!=0 || $get_m->ta!='' || $get_m->tempc!=0 || $get_m->fc!='' || $get_m->fr!=0 || $get_m->o2!=0){
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Signos Vitales/Básicos</span><br>';
                
        $html.='</td>
            </tr>';
    $html.='<tr style="font-size:100%;">';
                if($get_m->altrura!=0){
        $html.='<td width="19%">
                    <span style="font-size: 12px;">Altura (m)</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$get_m->altrura.'</span>
                </td>';
                }
                if($get_m->peso!=0){
         $html.='<td width="12%">
                    <span style="font-size: 12px;">Peso</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$get_m->peso.'</span>
                </td>';
                }
                if($get_m->ta!=''){
         $html.='<td width="13%">
                    <span style="font-size: 12px;">T.A.</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$get_m->ta.'</span>
                </td>';
                }
                if($get_m->tempc!=0){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">Temp</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$get_m->tempc.'</span>
                </td>';
                }
                if($get_m->fc!=''){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">F.C.</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$get_m->fc.'</span>
                </td>';
                }
                if($get_m->fr!=0){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">F.R. </span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$get_m->fr.'</span>
                </td>';
                }
                if($get_m->o2!=0){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">O2</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$get_m->o2.'</span>
                </td>';
                }
    $html.='</tr>';  
        }
        if($get_m->exploracionfisica!=''){
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Exploración Física</span><br>';
                $html.='<span style="font-size: 12px;">'.$get_m->exploracionfisica.'</span>';
        $html.='</td>
            </tr>';
        }
        if($get_m->tratamiento_estetico!=''){
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Tratamientos estéticos previos (botox, rellenos, hilos, laser etc) ¿Hace cuánto tiempo?</span><br>';
                $html.='<span style="font-size: 12px;">'.$get_m->tratamiento_estetico.'</span>';
        $html.='</td>
            </tr>';
        }
        if($get_m->cirugia_estetica!=''){
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Cirugías estéticas o reconstructivas ¿Hace cuánto tiempo?
                </span><br>';
                $html.='<span style="font-size: 12px;">'.$get_m->cirugia_estetica.'</span>';
        $html.='</td>
            </tr>';
        }
        if($get_m->area_tratar!=''){
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">¿Qué área le gustaría tratar?</span><br>';
                $html.='<span style="font-size: 12px;">'.$get_m->area_tratar.'</span>';
        $html.='</td>
            </tr>';
        }
        if($get_m->rutina_cuidado!=''){
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">¿Tienes una rutina de cuidado para su piel?
                </span><br>';
                $html.='<span style="font-size: 12px;">'.$get_m->rutina_cuidado.'</span>';
        $html.='</td>
            </tr>';
        }
        if($get_m->edad_aparente!=''){
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Edad aparente:</span><br>';
                $html.='<span style="font-size: 12px;">'.$get_m->edad_aparente.'</span>';
        $html.='</td>
            </tr>';
        }
    $html.='<tr style="font-size:100%;">';
            $ti='';
            if($get_m->tipo!=0){
                if($get_m->tipo==1){
                    $ti='Normal';
                }else if($get_m->tipo==2){
                    $ti='Grasa';
                }else if($get_m->tipo==3){
                    $ti='Seca';
                }else if($get_m->tipo==4){    
                    $ti='Asfíctica';
                }
        $html.='<td width="13%">
                    <span style="font-size: 12px;">Tipo</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$ti.'</span>
                </td>';
            }
            $es='';
            if($get_m->estado!=0){
                if($get_m->estado==1){
                    $es='Acneico';
                }else if($get_m->estado==2){
                    $es='Seborreico';
                }else if($get_m->estado==3){
                    $es='Hipersudoral';
                }else if($get_m->estado==4){    
                    $es='Dismetabólico';
                }else if($get_m->estado==5){    
                    $es='Alípico';
                }else if($get_m->estado==6){    
                    $es='Querótico';
                }else if($get_m->estado==7){    
                    $es='Atrópico';
                }
        $html.='<td width="19%">
                    <span style="font-size: 12px;">Estado</span><br>
                     <span style="font-size: 12px; font-weight: bold;">'.$es.'</span>
                </td>';
            }
            $fo='';
            if($get_m->fototipo!=''){
                if($get_m->fototipo==1){
                    $fo='l';
                }else if($get_m->fototipo==2){
                    $fo='ll';
                }else if($get_m->fototipo==3){
                    $fo='lll';
                }else if($get_m->fototipo==4){    
                    $fo='lV';
                }else if($get_m->fototipo==5){    
                    $fo='V';
                } 
        $html.='<td width="11%">
                    <span style="font-size: 12px;">Fototipo</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$fo.'</span>
                </td>';
            }
            $gr='';
            if($get_m->grosor!=0){
                if($get_m->grosor==1){
                    $gr='Media';
                }else if($get_m->grosor==2){
                    $gr='Fina';
                }else if($get_m->grosor==3){
                    $gr='Gruesa';
                } 
        $html.='<td width="13%">
                    <span style="font-size: 12px;">Grosor</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$gr.'</span>
                </td>';
            }
            $fl='';
            if($get_m->flacidez!=''){
                if($get_m->flacidez==1){
                    $fl='Muscular';
                }else if($get_m->flacidez==2){
                    $fl='Cutánea';
                }
        $html.='<td width="13%">
                    <span style="font-size: 12px;">Flacidez</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$fl.'</span>
                </td>';
            }
            $hi='';
            if($get_m->higratacion!=0){
                if($get_m->higratacion==1){
                    $hi='Normal';
                }else if($get_m->higratacion==2){
                    $hi='Deshidratada';
                }else if($get_m->higratacion==3){
                    $hi='Hiperhidratada';
                }
        $html.='<td width="18%">
                    <span style="font-size: 12px;">Hidratación</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$hi.'</span>
                </td>';
            }
            $ci='';
            if($get_m->cicatrizacion!=0){
                if($get_m->cicatrizacion==1){
                    $ci='Normal';
                }else if($get_m->cicatrizacion==2){
                    $ci='Deshidratada';
                }else if($get_m->cicatrizacion==3){
                    $ci='Hidratación';
                }
        $html.='<td width="20%">
                    <span style="font-size: 12px;">Cicatrización</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$ci.'</span>
                </td>';
            }
$html.='</tr>';
    if($get_m->foto_facial!=''){
$html.='<tr style="background-color: #779155; color:white; font-size:53%;">
            <td width="100%" align="center">
                <span  style="font-size: 10px;" align="center">Análisis facial</span>
            </td>
        </tr>';
    $fh = fopen(base_url()."uploads/analisisfacial/".$get_m->foto_facial, 'r') or die("Se produjo un error al abrir el archivo");
    $linea = fgets($fh);
    fclose($fh);  
$html.='<tr width="100%">
            <td width="100%" align="center">
                <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                    <img style="width: 975px; height: 627px; border: 2px dashed rgb(29, 175, 147); background: url('.base_url().'images/medicina/demo2.png);background-repeat:no-repeat; background-position:center;" src="'.$linea.'" width="975" height="627" style="border:dotted 1px black;">
                </div>
            </td>
        </tr>'; 
    }
    //
    $aux_diag=0;
    foreach ($diagn as $item){
        $aux_diag=1;
    }
    if($aux_diag==1){
        $html.='<tr width="100%">
            <td width="100%">
                <b style="font-size: 12px;">Diagnósticos:</b>
                <ol style="font-size: 12px;">';
                foreach ($diagn as $item){
                 $html.='<li>'.$item->diagnostico.'</li>';
                }
                $html.='</ol>';
            $html.='</td>
        </tr>';
    }
    $aux_trata=0;
    foreach ($trata as $item){
        $aux_trata=1;
    }
    if($aux_trata==1){
        $html.='<tr width="100%">
            <td width="100%">
                <b style="font-size: 12px;">Tratamiento:</b>
                <ol style="font-size: 12px;">';
                foreach ($trata as $item){
                 $html.='<li>'.$item->tratamiento.'</li>';
                }
                $html.='</ol>';
            $html.='</td>
        </tr>';
    }
    $aux_servi=0;
    foreach ($servi as $item){
        $aux_servi=1;
    }
    if($aux_servi==1){
        
    $html.='<tr style="background-color: #779155; color:white; font-size:53%;">
                <td width="100%" align="center">
                    <span  style="font-size: 10px;" align="center">Servicios específicos</span>
                </td>
            </tr>';

    $html.='<tr style="font-size:100%;">
                <td width="30%">
                    <span style="font-size: 12px;">Servicio</span>
                </td>
                <td width="70%">
                    <span style="font-size: 12px;">Descripción</span>
                </td>';
    $html.='</tr>';    
        foreach ($servi as $item){
    $html.='<tr width="100%">
                <td width="30%">
                    <span style="font-size: 12px;">'.$item->servicio.'</span>
                </td>
                <td width="70%">
                    <span style="font-size: 12px;">'.$item->descripcion.'</span>
                </td>
            </tr>';
        }

    }
    $aux_produc=0;
    foreach ($produc as $item){
        $aux_produc=1;
    }
    if($aux_produc==1){
        
    $html.='<tr style="background-color: #779155; color:white; font-size:53%;">
                <td width="100%" align="center">
                    <span  style="font-size: 10px;" align="center">Productos específicos</span>
                </td>
            </tr>';

    $html.='<tr style="font-size:100%;">
                <td width="30%">
                    <span style="font-size: 12px;">Producto</span>
                </td>
                <td width="25%">
                    <span style="font-size: 12px;">Lote</span>
                </td>
                <td width="25%">
                    <span style="font-size: 12px;">Caducidad</span>
                </td>
                <td width="20%">
                    <span style="font-size: 12px;">Cantidad</span>
                </td>';
    $html.='</tr>';    
        foreach ($produc as $item){
    $html.='<tr width="100%">
                <td width="30%">
                    <span style="font-size: 12px;">'.$item->producto.'</span>
                </td>
                <td width="25%">
                    <span style="font-size: 12px;">'.$item->lote.'</span>
                </td>
                <td width="25%">
                    <span style="font-size: 12px;">'.$item->fecha_caducidad.'</span>
                </td>
                <td width="20%">
                    <span style="font-size: 12px;">'.$item->cantidad.'</span>
                </td>
            </tr>';
        }
        
    }
    $html.='<tr style="font-size:10%;">
                <td width="100%" style="border-bottom: 2px solid #779155;">
                </td>
            </tr>
            <tr style="font-size:100%;">
                <td width="30%">
                    <b style="font-size: 12px;">Próxima consulta:</b>
                </td>
                <td width="50%">
                    <b style="font-size: 12px;">'.date('d/m/Y',strtotime($get_m->proximafecha)).'</b>
                </td>';
    $html.='</tr>';    
    $html.='<tr width="100%">
                <td width="50%" align="center">
                    <span style="font-size: 12px;">Nombre y firma del paciente</span>
                </td>
                <td width="50%" align="center">
                    <span style="font-size: 12px;">Nombre y firma de la cosmetologa</span>
                </td>
            </tr>';
            $html.='<tr width="100%">
                <td width="50%" align="center">
                    <span style="font-size: 12px;">'.$get_m->firma_paciente_txt.'</span>
                </td>
                <td width="50%" align="center">
                    <span style="font-size: 12px;">'.$get_m->firma_cosmetologa_txt.'</span>
                </td>
            </tr>';
            $fhfp1 = fopen(base_url()."uploads/firmapaciente_c1/".$get_m->firma_paciente, 'r') or die("Se produjo un error al abrir el archivo");
            $lineafp1 = fgets($fhfp1);
            $fhfc1 = fopen(base_url()."uploads/firmacosmetologa_c1/".$get_m->firma_cosmetologa, 'r') or die("Se produjo un error al abrir el archivo");
            $lineafc1 = fgets($fhfc1);
    $html.='<tr width="100%">
                <td width="50%" align="center">
                    <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                        <img style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); background-position:center;" src="'.$lineafp1.'" width="300" height="180" style="border:dotted 1px black;">
                    </div>
                </td>
                <td width="50%" align="center">
                    <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                        <img style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); background-position:center;" src="'.$lineafc1.'" width="300" height="180" style="border:dotted 1px black;">
                    </div>
                </td>
            </tr>';

}
    /// SPA
    if($get_spa!=null){
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px; font-weight: bold;">Resumen de última consulta de SPA</span>';
        $html.='</td>
            </tr>';
    $html.='<tr style="font-size:10%;">
                <td width="100%" style="border-bottom: 2px solid #779155;">
                </td>
            </tr>';
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Cosmetóloga: '.$get_spa->cosmetologa.'</span>';
        $html.='</td>
            </tr>';  
    $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px;">Fecha de consulta: '.date('d/m/Y',strtotime($get_spa->consultafecha)).'</span>';
        $html.='</td>
            </tr>';  
    $html.='<tr>
                <td width="100%">';
                $html.='<span  style="font-size: 12px; font-weight: bold;">Motivo de la consulta</span><br>
                        <span  style="font-size: 12px;">'.$get_spa->motivo_consulta.'</span><br>';
        $html.='</td>
            </tr>';
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span  style="font-size: 12px; font-weight: bold;">Nota de Evolución</span><br>
                        <span  style="font-size: 12px;">'.$get_spa->nota_evaluacion.'</span><br>';
        $html.='</td>
            </tr>';
        if($get_spa->altrura!=0  || $get_spa->peso!=0 || $get_spa->ta!='' || $get_spa->tempc!=0 || $get_spa->fc!='' || $get_spa->fr!=0 || $get_spa->o2!=0){
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Signos Vitales/Básicos</span><br>';
                
        $html.='</td>
            </tr>';
    $html.='<tr style="font-size:100%;">';
                if($get_spa->altrura!=0){
        $html.='<td width="19%">
                    <span style="font-size: 12px;">Altura (m)</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$get_spa->altrura.'</span>
                </td>';
                }
                if($get_spa->peso!=0){
         $html.='<td width="12%">
                    <span style="font-size: 12px;">Peso</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$get_spa->peso.'</span>
                </td>';
                }
                if($get_spa->ta!=''){
         $html.='<td width="13%">
                    <span style="font-size: 12px;">T.A.</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$get_spa->ta.'</span>
                </td>';
                }
                if($get_spa->tempc!=0){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">Temp</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$get_spa->tempc.'</span>
                </td>';
                }
                if($get_spa->fc!=''){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">F.C.</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$get_spa->fc.'</span>
                </td>';
                }
                if($get_spa->fr!=0){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">F.R. </span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$get_spa->fr.'</span>
                </td>';
                }
                if($get_spa->o2!=0){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">O2</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$get_spa->o2.'</span>
                </td>';
                }
    $html.='</tr>';  
        }
        if($get_spa->exploracionfisica!=''){
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Exploración Física</span><br>';
                $html.='<span style="font-size: 12px;">'.$get_spa->exploracionfisica.'</span>';
        $html.='</td>
            </tr>';
        }
        if($get_spa->tipo_servicio==1){
            $tipo_serviciot='<span style="font-size: 12px;">Facial</span>';
        }else if($get_spa->tipo_servicio==2){
            $tipo_serviciot='<span style="font-size: 12px;">Corporal</span>'; 
        }
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Tipo de servicio</span><br>';
                $html.=$tipo_serviciot;
        $html.='</td>
            </tr>';
        if($get_spa->frutas_vegetales!='' || $get_spa->carnes_rojas!=''){
    $html.='<tr style="font-size:100%;">';
            if($get_spa->frutas_vegetales!=''){
            $html.='<td width="50%">';
                    $html.='<span style="font-size: 12px; font-weight: bold;">Frutas y vegetales
                    </span><br>';
                    $html.='<span style="font-size: 12px;">'.$get_spa->frutas_vegetales.'</span>';
            $html.='</td>';
            }
            if($get_spa->carnes_rojas!=''){
            $html.='<td width="50%">';
                    $html.='<span style="font-size: 12px; font-weight: bold;">Carnes rojas
                    </span><br>';
                    $html.='<span style="font-size: 12px;">'.$get_spa->carnes_rojas.'</span>';
            $html.='</td>';
            }
            $html.='</tr>';
        }
        if($get_spa->carne_cerdo!='' || $get_spa->pollo!=''){
    $html.='<tr style="font-size:100%;">';
            if($get_spa->carne_cerdo!=''){
            $html.='<td width="50%">';
                    $html.='<span style="font-size: 12px; font-weight: bold;">Carne de cerdo
                    </span><br>';
                    $html.='<span style="font-size: 12px;">'.$get_spa->carne_cerdo.'</span>';
            $html.='</td>';
            }
            if($get_spa->pollo!=''){
            $html.='<td width="50%">';
                    $html.='<span style="font-size: 12px; font-weight: bold;">Pollo/Pescado
                    </span><br>';
                    $html.='<span style="font-size: 12px;">'.$get_spa->pollo.'</span>';
            $html.='</td>';
            }
            $html.='</tr>';
        }
        if($get_spa->lacteos!='' || $get_spa->harinas_refinadas!=''){
    $html.='<tr style="font-size:100%;">';
            if($get_spa->lacteos!=''){
            $html.='<td width="50%">';
                    $html.='<span style="font-size: 12px; font-weight: bold;">Lácteos
                    </span><br>';
                    $html.='<span style="font-size: 12px;">'.$get_spa->lacteos.'</span>';
            $html.='</td>';
            }
            if($get_spa->harinas_refinadas!=''){
            $html.='<td width="50%">';
                    $html.='<span style="font-size: 12px; font-weight: bold;">Harinas refinadas
                    </span><br>';
                    $html.='<span style="font-size: 12px;">'.$get_spa->harinas_refinadas.'</span>';
            $html.='</td>';
            }
            $html.='</tr>';
        }
        if($get_spa->endulza!='' || $get_spa->aguasl!=''){
    $html.='<tr style="font-size:100%;">';
            if($get_spa->endulza!=''){
            $html.='<td width="50%">';
                    $html.='<span style="font-size: 12px; font-weight: bold;">Endulza
                    </span><br>';
                    $html.='<span style="font-size: 12px;">'.$get_spa->endulza.'</span>';
            $html.='</td>';
            }
            if($get_spa->aguasl!=''){
            $html.='<td width="50%">';
                    $html.='<span style="font-size: 12px; font-weight: bold;">Agua L
                    </span><br>';
                    $html.='<span style="font-size: 12px;">'.$get_spa->aguasl.'</span>';
            $html.='</td>';
            }
            $html.='</tr>';
        }
        if($get_spa->chatarra!='' || $get_spa->gaseosa!=''){
    $html.='<tr style="font-size:100%;">';
            if($get_spa->chatarra!=''){
            $html.='<td width="50%">';
                    $html.='<span style="font-size: 12px; font-weight: bold;">Chatarra
                    </span><br>';
                    $html.='<span style="font-size: 12px;">'.$get_spa->chatarra.'</span>';
            $html.='</td>';
            }
            if($get_spa->gaseosa!=''){
            $html.='<td width="50%">';
                    $html.='<span style="font-size: 12px; font-weight: bold;">Gaseosa
                    </span><br>';
                    $html.='<span style="font-size: 12px;">'.$get_spa->gaseosa.'</span>';
            $html.='</td>';
            }
            $html.='</tr>';
        }
        if($get_spa->tratamiento_anteriores!=''){
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Tratamientos anteriores</span><br>';
                $html.='<span style="font-size: 12px;">'.$get_spa->tratamiento_anteriores.'</span>';
        $html.='</td>
            </tr>';
        }
        if($get_spa->tratamiento_esteticos!=''){
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Tratamientos estéticos recientes(botox/rellenos/hilos)</span><br>';
                $html.='<span style="font-size: 12px;">'.$get_spa->tratamiento_esteticos.'</span>';
        $html.='</td>
            </tr>';
        }
        if($get_spa->cuidado_casa_am!='' || $get_spa->cuidado_casa_pm!=''){
             $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Cuidado en casa actual</span><br>';
                $html.='<span style="font-size: 12px;">Am '.$get_spa->cuidado_casa_am.'</span>&nbsp;&nbsp;&nbsp;&nbsp; ';
                $html.='<span style="font-size: 12px;">Pm '.$get_spa->cuidado_casa_pm.'</span>';
        $html.='</td>
            </tr>';
        }
        if($get_spa->edad_aparente!=''){
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Edad aparente</span><br>';
                $html.='<span style="font-size: 12px;">'.$get_spa->edad_aparente.'</span>';
        $html.='</td>
            </tr>';
        }
        $ti='';
        $html.='<tr style="font-size:100%;">';
            if($get_spa->tipo!=0){
                if($get_spa->tipo==1){
                    $ti='Normal';
                }else if($get_spa->tipo==2){
                    $ti='Grasa';
                }else if($get_spa->tipo==3){
                    $ti='Seca';
                }else if($get_spa->tipo==4){    
                    $ti='Asfíctica';
                }      
                $html.='<td width="13%">';
                        $html.='<span style="font-size: 12px; font-weight: bold;">Tipo</span><br>';
                        $html.='<span style="font-size: 12px;">'.$ti.'</span>';
                $html.='</td>';
            }
            if($get_spa->estado!=0){
                if($get_spa->estado==1){
                    $es='Acneico';
                }else if($get_spa->estado==2){
                    $es='Seborreico';
                }else if($get_spa->estado==3){
                    $es='Hipersudoral';
                }else if($get_spa->estado==4){    
                    $es='Dismetabólico';
                }else if($get_spa->estado==5){    
                    $es='Alípico';
                }else if($get_spa->estado==6){    
                    $es='Querótico';
                }else if($get_spa->estado==7){    
                    $es='Atrópico';
                }
                $html.='<td width="19%">';
                        $html.='<span style="font-size: 12px; font-weight: bold;">Estado</span><br>';
                        $html.='<span style="font-size: 12px;">'.$es.'</span>';
                $html.='</td>';
            }
            $fo='';
            if($get_spa->fototipo!=''){
                if($get_spa->fototipo==1){
                    $fo='l';
                }else if($get_spa->fototipo==2){
                    $fo='ll';
                }else if($get_spa->fototipo==3){
                    $fo='lll';
                }else if($get_spa->fototipo==4){    
                    $fo='lV';
                }else if($get_spa->fototipo==5){    
                    $fo='V';
                } 
                $html.='<td width="11%">';
                        $html.='<span style="font-size: 12px; font-weight: bold;">Fototipo</span><br>';
                        $html.='<span style="font-size: 12px;">'.$fo.'</span>';
                $html.='</td>';
            }
            $gr='';
            if($get_spa->grosor!=0){
                if($get_spa->grosor==1){
                    $gr='Media';
                }else if($get_spa->grosor==2){
                    $gr='Fina';
                }else if($get_spa->grosor==3){
                    $gr='Gruesa';
                } 
                $html.='<td width="11%">';
                        $html.='<span style="font-size: 12px; font-weight: bold;">Grosor</span><br>';
                        $html.='<span style="font-size: 12px;">'.$gr.'</span>';
                $html.='</td>';
            }
            $fl='';
            if($get_spa->flacidez!=''){
                if($get_spa->flacidez==1){
                    $fl='Muscular';
                }else if($get_spa->flacidez==2){
                    $fl='Cutánea';
                }
                $html.='<td width="13%">';
                        $html.='<span style="font-size: 12px; font-weight: bold;">Flacidez</span><br>';
                        $html.='<span style="font-size: 12px;">'.$fl.'</span>';
                $html.='</td>';
            }
            $hi='';
            if($get_spa->higratacion!=0){
                if($get_spa->higratacion==1){
                    $hi='Normal';
                }else if($get_spa->higratacion==2){
                    $hi='Deshidratada';
                }else if($get_spa->higratacion==3){
                    $hi='Hiperhidratada';
                }
                $html.='<td width="18%">';
                        $html.='<span style="font-size: 12px; font-weight: bold;">Hidratación</span><br>';
                        $html.='<span style="font-size: 12px;">'.$hi.'</span>';
                $html.='</td>';
            }
            $ci='';
            if($get_spa->cicatrizacion!=0){
                if($get_spa->cicatrizacion==1){
                    $ci='Normal';
                }else if($get_spa->cicatrizacion==2){
                    $ci='Deshidratada';
                }else if($get_spa->cicatrizacion==3){
                    $ci='Hidratación';
                }
                $html.='<td width="20%">';
                        $html.='<span style="font-size: 12px; font-weight: bold;">Cicatrización</span><br>';
                        $html.='<span style="font-size: 12px;">'.$ci.'</span>';
                $html.='</td>';
            }
        $html.='</tr>';
                  ////
        if($get_spa->foto_facial!=''){
        $html.='<tr style="background-color: #779155; color:white; font-size:53%;">
                    <td width="100%" align="center">
                        <span  style="font-size: 10px;" align="center">Análisis facial</span>
                    </td>
                </tr>';
            $fh = fopen(base_url()."uploads/analisisfacial_spa/".$get_spa->foto_facial, 'r') or die("Se produjo un error al abrir el archivo");
            $linea = fgets($fh);
            fclose($fh);  
        $html.='<tr width="100%">
                    <td width="100%" align="center">
                        <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                            <img style="width: 975px; height: 627px; border: 2px dashed rgb(29, 175, 147); background: url('.base_url().'images/medicina/demo2.png);background-repeat:no-repeat; background-position:center;" src="'.$linea.'" width="975" height="627" style="border:dotted 1px black;">
                        </div>
                    </td>
                </tr>'; 
        }
        $aux_serv_spa=0;
    foreach ($serv_spa as $item){
        $aux_serv_spa=1;
    }
    if($aux_serv_spa==1){
        
    $html.='<tr style="background-color: #779155; color:white; font-size:53%;">
                <td width="100%" align="center">
                    <span  style="font-size: 10px;" align="center">Servicios específicos</span>
                </td>
            </tr>';
    $html.='<tr style="font-size:100%;">
                <td width="30%">';
                $html.='<span style="font-size: 12px;">Servicio</span>';
        $html.='</td>
                <td width="70%">';
                $html.='<span style="font-size: 12px;">Descripción</span>';
        $html.='</td>';
    $html.='</tr>';    
        foreach ($serv_spa as $item){
            $html.='<tr style="font-size:100%;">
                <td width="30%">';
                $html.='<span style="font-size: 12px;">'.$item->servicio.'</span>';
        $html.='</td>
                <td width="70%">';
                $html.='<span style="font-size: 12px;">'.$item->descripcion.'</span>';
        $html.='</td>';
    $html.='</tr>';  
        }

    }
    $aux_produc_spa=0;
    foreach ($produc_spa as $item){
        $aux_produc_spa=1;
    }
    if($aux_produc_spa==1){
        
    $html.='<tr style="background-color: #779155; color:white; font-size:53%;">
                <td width="100%" align="center">
                    <span  style="font-size: 10px;" align="center">Productos específicos</span>
                </td>
            </tr>';
    $html.='<tr style="font-size:100%;">
                <td width="30%">';
                $html.='<span style="font-size: 12px;">Producto</span>';
        $html.='</td>
                <td width="25%">';
                $html.='<span style="font-size: 12px;">Lote</span>';
        $html.='</td>
                <td width="25%">';
                $html.='<span style="font-size: 12px;">Caducidad</span>';
        $html.='</td>
                <td width="20%">';
                $html.='<span style="font-size: 12px;">Cantidad</span>';
        $html.='</td>';
    $html.='</tr>';     
        foreach ($produc_spa as $item){
    $html.='<tr style="font-size:100%;">
                <td width="30%">';
                $html.='<span style="font-size: 12px;">'.$item->producto.'</span>';
        $html.='</td>
                <td width="25%">';
                $html.='<span style="font-size: 12px;">'.$item->lote.'</span>';
        $html.='</td>
                <td width="25%">';
                $html.='<span style="font-size: 12px;">'.$item->fecha_caducidad.'</span>';
        $html.='</td>
                <td width="20%">';
                $html.='<span style="font-size: 12px;">'.$item->cantidad.'</span>';
        $html.='</td>';
    $html.='</tr>';  
        }
        
    }
    $html.='<tr style="font-size:100%;">
                <td width="100%" style="border-bottom: 2px solid #779155;">
                </td>
            </tr>';
    $html.='<tr style="font-size:100%;">
                <td width="100%">';
                $html.='<span style="font-size: 12px; font-weight: bold;">Próxima consulta </span>';
                $html.='<span style="font-size: 12px;">'.date('d/m/Y',strtotime($get_spa->proximafecha)).'</span>';
        $html.='</td>
            </tr>';   
    $html.='<tr style="font-size:100%;">
                <td width="50%" align="center">
                    <span style="font-size: 12px;">Nombre y firma del paciente</span>
                </td>
                <td width="50%" align="center">
                    <span style="font-size: 12px;">Nombre y firma de la cosmetologa</span>
                </td>
            </tr>';
            $html.='<tr width="100%">
                <td width="50%" align="center">
                    <span style="font-size: 12px;">'.$get_spa->firma_paciente_txt.'</span>
                </td>
                <td width="50%" align="center">
                    <span style="font-size: 12px;">'.$get_spa->firma_cosmetologa_txt.'</span>
                </td>
            </tr>';
            $fhfp1 = fopen(base_url()."uploads/firmapaciente_spa/".$get_spa->firma_paciente, 'r') or die("Se produjo un error al abrir el archivo");
            $lineafp1 = fgets($fhfp1);
            $fhfc1 = fopen(base_url()."uploads/firmacosmetologa_spa/".$get_spa->firma_cosmetologa, 'r') or die("Se produjo un error al abrir el archivo");
            $lineafc1 = fgets($fhfc1);
    $html.='<tr width="100%">
                <td width="50%" align="center">
                    <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                        <img style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); background-position:center;" src="'.$lineafp1.'" width="300" height="180" style="border:dotted 1px black;">
                    </div>
                </td>
                <td width="50%" align="center">
                    <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                        <img style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); background-position:center;" src="'.$lineafc1.'" width="300" height="180" style="border:dotted 1px black;">
                    </div>
                </td>
            </tr>';
    }
    /// Nutrición
  $html.='</table>';
  // Nutrición
  if($resultconsulta!=null){
    ///
    $vimg=0;
    $imgpeso = '';
    if($resultconsulta->imc>0 and $resultconsulta->imc<18.5){
        $imgpeso = base_url().'public/img/peso_bajo.png';
        $vimg=1;
    }
    if($resultconsulta->imc>18.5 and $resultconsulta->imc<24.9){
        $imgpeso = base_url().'public/img/peso_normal.png';
        $vimg=2;
    }
    if($resultconsulta->imc>24.9 and $resultconsulta->imc<29.9){
        $imgpeso = base_url().'public/img/peso_sobre.png';
        $vimg=3;
    }
    if($resultconsulta->imc>29.9 and $resultconsulta->imc<34.5){
        $imgpeso = base_url().'public/img/peso_obesidad_g1.png';
        $vimg=4;
    }
    if($resultconsulta->imc>34.5 and $resultconsulta->imc<39.9){
        $imgpeso = base_url().'public/img/peso_obesidad_g2.png';
        $vimg=5;
    }
    if($resultconsulta->imc>=40){
        $imgpeso = base_url().'public/img/peso_obesidad_g3.png';
        $vimg=6;
    }
    if($resultconsulta->comida1==''){
        $comida1='Desayuno';
    }else{
        $comida1=$resultconsulta->comida1;
    }
    if($resultconsulta->comida2==''){
        $comida2='Colacion 1';
    }else{
        $comida2=$resultconsulta->comida2;
    }
    if($resultconsulta->comida3==''){
        $comida3='Comida';
    }else{
        $comida3=$resultconsulta->comida3;
    }
    if($resultconsulta->comida4==''){
        $comida4='colacion 2';
    }else{
        $comida4=$resultconsulta->comida4;
    }
    if($resultconsulta->comida5==''){
        $comida5='Cena';
    }else{
        $comida5=$resultconsulta->comida5;
    }
    if($resultconsulta->comida6==''){
        $comida6='colacion 3';
    }else{
        $comida6=$resultconsulta->comida6;
    }
    if($resultconsulta->comida7==''){
        $comida7='';
    }else{
        $comida7=$resultconsulta->comida7;
    }
    ///
  $html.='
      <table >';
      $html.='<tr style="font-size:80%; border-bottom: 2px solid #779155;">
                <td width="100%">'; 
                $html.='<span  style="font-size: 12px; font-weight: bold;">Resumen de última consulta de nutrición</span>';
        $html.='</td>
            </tr>';
    $html.='<tr style="font-size:10%;">
                <td width="100%" style="border-bottom: 2px solid #779155;">
                </td>
            </tr>';
        $html.='<tr>
          <td style="text-align:center;font-size: 15px;">
            Información General
          </td>
        </tr>
        <tr>
          <td style="text-align:center;font-size: 15px;">
            
          </td>
        </tr>
      </table>
      <table border="0">
        <tr>
          <td style="width:45%" rowspan="5">
            <table border="0" >
              <tr>
                <td style="width:12%; font-size: 13px; text-align:center; background-color: #788f55; color:white;">IMC</td>
                <td style="width:33%; font-size: 13px; text-align:center; border: 1px solid #cccccc;">'.$resultconsulta->imc.'</td>
                <td style="width:10%; font-size: 13px;"></td>
                <td style="width:12%; font-size: 13px; text-align:center; background-color: #788f55; color:white;">%</td>
                <td style="width:33%; font-size: 13px; text-align:center;     border: 1px solid #cccccc;">'.$resultconsulta->agua.'<br>Agua</td>
              </tr>
              <tr>
                <td colspan="5"></td>
              </tr>
              <tr>
                <td style="font-size: 13px; text-align:center; background-color: #788f55; color:white;">%</td>
                <td style="font-size: 13px; text-align:center;     border: 1px solid #cccccc;">'.$resultconsulta->grasa.'<br>Grasa</td>
                <td></td>
                <td style="font-size: 13px; text-align:center; background-color: #788f55; color:white;">%</td>
                <td style="font-size: 13px; text-align:center;     border: 1px solid #cccccc;">'.$resultconsulta->grasa_magra.'<br>Grasa magna</td>
              </tr>
              <tr>
                <td colspan="5"></td>
              </tr>
            </table>
            <table border="0">
              <tr>
                <td style="width:30%; font-size: 13px;"></td>
                <td style="width:40%; font-size: 13px; text-align:center; background-color: #01c0c8; color:white;">'.$resultconsulta->peso_ideal.'<br>Peso ideal
                </td>
                <td style="width:30%; font-size: 13px;"></td>
                
              </tr>
            </table>
          </td>
          <td style="width:15%" rowspan="5">
            <img src="'.$imgpeso.'" alt="Girl in a jacket" width="90" height="140">
          </td>
        </tr>
        <tr>
          <td style="font-size: 13px;" >
          Edad: '.$resultconsulta->edad.'
          </td>
        </tr>
        <tr>
          <td style="font-size: 13px;">
          Fecha de consulta: '.$resultconsulta->consultafecha.'
          </td>
        </tr>
        <tr>
          <td style="font-size: 13px;">
          Indice de cintura: '.$resultconsulta->cintura.'
          </td>
        </tr>
        <tr>
          <td style="font-size: 13px;">
          Indice de cadera: '.$resultconsulta->cadera.'
          </td>
        </tr>
        
      </table>
      <table border="0">
        <tr>
          <td style="text-align:center;font-size: 15px;">
            Tabla de equivalentes
          </td>
        </tr>
        <tr>
          <td style="text-align:center;font-size: 15px;">
            
          </td>
        </tr>
      </table>
      <style>
        .tdcolorboder{
            border: 1px solid black
        }
        
      </style>
      <table border="0" style="padding:1;">
        <tr>
          <td class="tdcolorboder"># equivalentes</td>
          <td class="tdcolorboder">Grupo de alimentos</td>
          <td class="tdcolorboder">Subgrupos</td>
          <td class="tdcolorboder">'.$comida1.'</td>
          <td class="tdcolorboder">'.$comida2.'</td>
          <td class="tdcolorboder">'.$comida3.'</td>
          <td class="tdcolorboder">'.$comida4.'</td>
          <td class="tdcolorboder">'.$comida5.'</td>
          <td class="tdcolorboder">'.$comida6.'</td>
          <td class="tdcolorboder">'.$comida7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte1.'</td>
          <td class="tdcolorboder">Verduras</td>
          <td class="tdcolorboder"></td>
          <td class="tdcolorboder">'.$resultconsulta->ver1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ver2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ver3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ver4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ver5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ver6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ver7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte2.'</td>
          <td class="tdcolorboder">3 Frutas</td>
          <td class="tdcolorboder"></td>
          <td class="tdcolorboder">'.$resultconsulta->fru1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->fru2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->fru3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->fru4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->fru5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->fru6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->fru7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte3.'</td>
          <td class="tdcolorboder" rowspan="2">*1 Cereales y tuberculos</td>
          <td class="tdcolorboder">a. sin grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->ces1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ces2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ces3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ces4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ces5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ces6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ces7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte4.'</td>
          <td class="tdcolorboder">b. Con grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->cec1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->cec2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->cec3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->cec4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->cec5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->cec6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->cec7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte5.'</td>
          <td class="tdcolorboder">Leguminosas</td>
          <td class="tdcolorboder"></td>
          <td class="tdcolorboder">'.$resultconsulta->leg1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->leg2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->leg3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->leg4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->leg5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->leg6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->leg7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte6.'</td>
          <td class="tdcolorboder" rowspan="4">1 Alimentos de origen animal</td>
          <td class="tdcolorboder">a. Muy bajo aporte de grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->amu1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amu2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amu3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amu4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amu5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amu6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amu7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte7.'</td>
          
          <td class="tdcolorboder">b. Bajo aporte de grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->aba1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aba2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aba3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aba4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aba5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aba6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aba7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte8.'</td>
          
          <td class="tdcolorboder">c. Moderado aporte de grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->amo1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amo2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amo3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amo4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amo5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amo6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amo7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte9.'</td>
          
          <td class="tdcolorboder">d. Alto aporte de grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->aal1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aal2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aal3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aal4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aal5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aal6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aal7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte10.'</td>
          <td class="tdcolorboder" rowspan="4">Leche y yogurt</td>
          <td class="tdcolorboder">a. Descremada</td>
          <td class="tdcolorboder">'.$resultconsulta->led1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->led2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->led3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->led4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->led5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->led6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->led7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte11.'</td>
          
          <td class="tdcolorboder">b. semidescremada</td>
          <td class="tdcolorboder">'.$resultconsulta->les1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->les2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->les3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->les4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->les5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->les6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->les7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte12.'</td>
          
          <td class="tdcolorboder">c. Entera</td>
          <td class="tdcolorboder">'.$resultconsulta->lee1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lee2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lee3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lee4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lee5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lee6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lee7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte13.'</td>
          
          <td class="tdcolorboder">d. con azúcar</td>
          <td class="tdcolorboder">'.$resultconsulta->lec1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lec2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lec3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lec4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lec5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lec6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lec7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte14.'</td>
          <td class="tdcolorboder" rowspan="2">2 Aceites y grasas</td>
          <td class="tdcolorboder">a. Sin proteina</td>
          <td class="tdcolorboder">'.$resultconsulta->acs1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acs2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acs3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acs4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acs5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acs6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acs7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte15.'</td>
          <td class="tdcolorboder">b. Con Proteina</td>
          <td class="tdcolorboder">'.$resultconsulta->acc1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acc2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acc3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acc4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acc5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acc6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acc7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte16.'</td>
          <td class="tdcolorboder" rowspan="2">Azucares</td>
          <td class="tdcolorboder">a. Sin grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->azs1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azs2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azs3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azs4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azs5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azs6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azs7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte17.'</td>
          <td class="tdcolorboder">b. Con grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->azc1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azc2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azc3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azc4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azc5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azc6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azc7.'</td>
        </tr>
      </table>
    ';
    if($resultlu->num_rows()>0 || $resultma->num_rows()>0 || $resultmi->num_rows()>0 || $resultju->num_rows()>0 || $resultvi->num_rows()>0){
    $html.='
      <table border="0">
        <tr>
          <td style="text-align:center;font-size: 15px;">
            Ejemplo de menú
          </td>
        </tr>
        <tr>
          <td style="text-align:center;font-size: 15px; height:370px;">
            
          </td>
        </tr>
      </table>
      <style>
        .tdcolorbodermenu{
            border-bottom: 1px solid #7a9557;
            font-size: 15px;
            color:#7a9557
        }
        
      </style>
      <table border="0">
        <tr>
          <td class="tdcolorbodermenu">LUNES</td>
          <td class="tdcolorbodermenu">MARTES</td>
          <td class="tdcolorbodermenu">MIERCOLES</td>
          <td class="tdcolorbodermenu">JUEVES</td>
          <td class="tdcolorbodermenu">VIERNES</td>
        </tr>
        <tr>
            <td>';
            if ($resultlu->num_rows()>0) {
              $html.='<table>';
                foreach ($resultlu->result() as $item) {
                 $html.='<tr><td>'.$item->contenido.'</td></tr>';
                }
              $html.='</table>';
            }
  $html.=' </td>
            <td>';
            if ($resultma->num_rows()>0) {
              $html.='<table>';
                foreach ($resultma->result() as $item) {
                 $html.='<tr><td>'.$item->contenido.'</td></tr>';
                }
              $html.='</table>';
            }
$html.='   </td>
            <td>';
            if ($resultmi->num_rows()>0) {
              $html.='<table>';
                foreach ($resultmi->result() as $item) {
                 $html.='<tr><td>'.$item->contenido.'</td></tr>';
                }
              $html.='</table>';
            }
$html.='   </td>
            <td>';
            if ($resultju->num_rows()>0) {
              $html.='<table>';
                foreach ($resultju->result() as $item) {
                 $html.='<tr><td>'.$item->contenido.'</td></tr>';
                }
              $html.='</table>';
            }
$html.='   </td>
            <td>';
            if ($resultvi->num_rows()>0) {
              $html.='<table>';
                foreach ($resultvi->result() as $item) {
                 $html.='<tr><td>'.$item->contenido.'</td></tr>';
                }
              $html.='</table>';
            }
  $html.=' </td>
        </tr>
      </table>';
    }
    $html.='<table>';
    $aux_serv_nutr=0;
    foreach ($serv_nutr as $item){
        $aux_serv_nutr=1;
    }
    if($aux_serv_nutr==1){
        
    $html.='<tr style="background-color: #779155; color:white; font-size:53%;">
                <td width="100%" align="center">
                    <span  style="font-size: 10px;" align="center">Servicios específicos</span>
                </td>
            </tr>';
    $html.='<tr style="font-size:100%;">
                <td width="30%">';
                $html.='<span style="font-size: 12px;">Servicio</span>';
        $html.='</td>
                <td width="70%">';
                $html.='<span style="font-size: 12px;">Descripción</span>';
        $html.='</td>';
    $html.='</tr>';    
        foreach ($serv_nutr as $item){
            $html.='<tr style="font-size:100%;">
                <td width="30%">';
                $html.='<span style="font-size: 12px;">'.$item->servicio.'</span>';
        $html.='</td>
                <td width="70%">';
                $html.='<span style="font-size: 12px;">'.$item->descripcion.'</span>';
        $html.='</td>';
    $html.='</tr>';  
        }

    }
    $aux_produc_nutr=0;
    foreach ($produc_nutr as $item){
        $aux_produc_nutr=1;
    }
    if($aux_produc_nutr==1){
        
    $html.='<tr style="background-color: #779155; color:white; font-size:53%;">
                <td width="100%" align="center">
                    <span  style="font-size: 10px;" align="center">Productos específicos</span>
                </td>
            </tr>';
    $html.='<tr style="font-size:100%;">
                <td width="30%">';
                $html.='<span style="font-size: 12px;">Producto</span>';
        $html.='</td>
                <td width="25%">';
                $html.='<span style="font-size: 12px;">Lote</span>';
        $html.='</td>
                <td width="25%">';
                $html.='<span style="font-size: 12px;">Caducidad</span>';
        $html.='</td>
                <td width="20%">';
                $html.='<span style="font-size: 12px;">Cantidad</span>';
        $html.='</td>';
    $html.='</tr>';     
        foreach ($produc_nutr as $item){
    $html.='<tr style="font-size:100%;">
                <td width="30%">';
                $html.='<span style="font-size: 12px;">'.$item->producto.'</span>';
        $html.='</td>
                <td width="25%">';
                $html.='<span style="font-size: 12px;">'.$item->lote.'</span>';
        $html.='</td>
                <td width="25%">';
                $html.='<span style="font-size: 12px;">'.$item->fecha_caducidad.'</span>';
        $html.='</td>
                <td width="20%">';
                $html.='<span style="font-size: 12px;">'.$item->cantidad.'</span>';
        $html.='</td>';
    $html.='</tr>';  
        }
        
    }
    $html.='</table>';
}
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('laboratorio.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
?>
