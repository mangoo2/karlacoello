<?php
require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF/tcpdf.php';

$GLOBALS['etiqueta']=$etiqueta;
//
    $style = array(
      'position' => 'C',
      'align' => 'C',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'hpadding' => 'auto',
      'vpadding' => 'auto',
      'fgcolor' => array(0,0,0),
      'bgcolor' => false, //array(255,255,255),
      'text' => true,
      'font' => 'helvetica',
      'fontsize' => 10,
      'stretchtext' => 4
  );

class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      /// datos completos
      $html = '';
  
      $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html = '';
    /*
    $html = '<p></p>';
    $html .= '<table width="100%" border="0">
                <tr>
                  <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                </tr>
              </table>';
    */
    $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT,array(58, 155), true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cuestionario');
$pdf->SetTitle('Cuestionario');
$pdf->SetSubject('Cuestionario');
$pdf->SetKeywords('Cuestionario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('6', '1', '6');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true,6);
$pdf->SetFont('dejavusans', '', 11);
// set image scale factor
//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage('L');
  $codigo = '';
  
  foreach ($GLOBALS['etiqueta'] as $item) {
    $codigo = $item->foliotext;
    $pdf->write1DBarcode($item->foliotext, 'C39', '', '', '', 18, 0.4, $style, 'N');
    $html='<div align="center">
            <img style="width:95px;" src="'.base_url().'app-assets/images/alta.png" alt="materialize logo">
            <div>
              <span style="font-size: 7px;">Cliente: '.$item->empresa.'</span><br>
              <span style="font-size: 7px;">Modelo: '.$item->modelo.'</span><br>
              <span style="font-size: 7px;">Fecha: '.$item->fechagenera.'</span>
            </div>
          </div>';

  }

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Captura.pdf', 'I');
?>