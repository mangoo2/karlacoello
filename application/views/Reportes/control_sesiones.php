<?php

    require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF/tcpdf.php');
    $this->load->helper('url');
    //var_dump($GLOBALS['folio']);die;
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
    $img_file = base_url().'images/formato/portada_header.jpg'; 
    //$pdf->Image($img_file, 0, 0, 0, 500, '', '', '', false, 500, '', false, false, 0); 
    $this->Image($img_file, 0, 0, 210, 40, '', '', '', false, 330, '', false, false, 0); 
    $html = '<table width="100%" border="0"> 
                    <tr> 
                        <td width="100%" height="80px"></td> 
                    </tr> 
                </table> 
                <table width="100%" border="0"> 
                    <tr> 
                        <td width="100%" style="text-align: center;"> 
                            <span style="font-weight: bold; font-size: 25px;">Control de sesiones</span>    
                        </td> 
                    </tr> 
                </table>'; 
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $img_file = base_url().'images/formato/portada_footer2.jpg'; 
    //$pdf->Image($img_file, 0, 0, 0, 500, '', '', '', false, 500, '', false, false, 0); 
    $this->Image($img_file, 0, 280, 210, 18, '', '', '', false, 330, '', false, false, 0); 
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Medicina estética');
$pdf->SetTitle('Medicina estética');
$pdf->SetSubject('Medicina estética');
$pdf->SetKeywords('Medicina estética');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '40', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetFooterMargin('15');
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 13);
// add a page
$pdf->AddPage('P', 'A4');
  $html='<table width="100%" border="0"> 
            <tr style="font-size:20%;"> 
                <td width="100%"> 
                    <div style="font-size: 15px;"><strong>Paciente: '.$paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno.'</strong></div> 
                </td> 
            </tr>';
            
            $resul_sesion=$this->ModelCatalogos->get_servicios_estetica($idconsulta);
            foreach ($resul_sesion as $item){
                $html.='<tr style="font-size:10%;">
                    <td width="100%" style="border-bottom: 2px solid #779155;">
                    </td>
                </tr>';
                $result_ac=$this->ModelCatalogos->get_servicios_estetica_total($idconsulta,$item->idservicio);
                $aux_total_sesiones_actuales=0;
                foreach ($result_ac as $itemac){
                    $aux_total_sesiones_actuales=$itemac->total;
                }
                $html.='<tr style="font-size:20%;"> 
                    <td width="100%"> 
                        <div style="font-size: 12px;"><strong>Servicio: '.$item->nombre.'</strong></div> 
                    </td>
                </tr>';
                $html.='<tr style="font-size:20%;"> 
                    <td width="100%"> 
                        <div style="font-size: 12px;"><strong>Total de sesiones: '.$item->cantidad.'</strong></div> 
                    </td>
                </tr>';
                $html.='<tr style="font-size:20%;"> 
                    <td width="100%"> 
                        <div style="font-size: 12px;"><strong>Sesiones actuales: '.$aux_total_sesiones_actuales.'</strong></div> 
                    </td> 
                </tr>';
                $html.='<tr style="font-size:20%;">
                    <td width="100%">
                      <br><br><br>
                    </td>
                </tr>';
                $html.='<tr style="font-size:20%;"> 
                    <td width="20%" style="border: solid 1px #000000;"> 
                        <div style="font-size: 12px;"><strong> Fecha </strong></div> 
                    </td> 
                    <td width="20%" style="border: solid 1px #000000"> 
                        <div style="font-size: 12px;"><strong> Número de sesión </strong></div> 
                    </td> 
                    <td width="40%" style="border: solid 1px #000000"> 
                        <div style="font-size: 12px;"><strong> Observaciones cosmetóloga </strong></div> 
                    </td> 
                    <td width="20%" style="border: solid 1px #000000"> 
                        <div style="font-size: 12px;text-align: center;"><strong> Firma</strong></div> 
                    </td> 
                </tr>';
                $result_sesiones=$this->General_model->get_records_condition('idconsulta='.$idconsulta.' AND idservicio='.$item->idservicio,'consulta_medicina_estetica_session');
                    $aux_numero=1;
                    foreach ($result_sesiones as $items){
                        $html.='<tr style="font-size:20%;"> 
                            <td width="20%"  style="border: solid 1px #000000"> 
                                <div style="font-size: 12px;"> '.date('d/m/y',strtotime($items->fecha)).' </div> 
                            </td> 
                            <td width="20%"  style="border: solid 1px #000000"> 
                                <div style="font-size: 12px;"> '.$aux_numero.' </div> 
                            </td> 
                            <td width="40%"  style="border: solid 1px #000000"> 
                                <div style="font-size: 12px;">'.$items->observacion.' </div> 
                            </td> 
                            <td width="20%" style="text-align: center; border: solid 1px #000000"> ';
                            if($consulta->firma_paciente!=''){
                                $fhfp1 = fopen(base_url()."uploads/firmapaciente_c1/".$consulta->firma_paciente, 'r') or die("Se produjo un error al abrir el archivo");
                                $lineafp1 = fgets($fhfp1);
                                $html.=' <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                                        <img style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); background-position:center;" src="'.$lineafp1.'" width="300" height="180" style="border:dotted 1px black;">
                                    </div>';
                            }           
                            $html.='</td> 
                        </tr>';
                       $aux_numero++;    
                    }
                    $html.='<tr style="font-size:20%;">
                        <td width="100%">
                          <br><br><br>
                        </td>
                    </tr>';

            }
           
        $html.='</table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('sesiones.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
?>
