<?php
require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF/tcpdf.php';

$GLOBALS['configuracionCotizacion']=$configuracionCotizacion;
$GLOBALS['rfc_datos']=$rfc_datos;
$GLOBALS['dia_reg']=$dia_reg;
$GLOBALS['mes_reg']=$mes_reg;
$GLOBALS['ano_reg']=$ano_reg;
$GLOBALS['ini_personal']=$ini_personal;
$GLOBALS['proinven']=$proinven;
$GLOBALS['cfd']=$cfd;
//
$GLOBALS['formacobro']=$formacobro;
// Datos de del rfc 
$GLOBALS['razon_social']=$razon_social;
$GLOBALS['rfc']=$rfc;
$GLOBALS['num_ext']=$num_ext;
$GLOBALS['colonia']=$colonia;
$GLOBALS['calle']=$calle;
$GLOBALS['cp']=$cp;
//resultprefactura
$GLOBALS['vence']= $vence;
$GLOBALS['cargo']=$cargo;
//metodopagorow
$GLOBALS['metodopago_text']=$metodopago_text;
//formapagorow
$GLOBALS['formapago_text']=$formapago_text;
//cfdirow
$GLOBALS['uso_cfdi_text']=$uso_cfdi_text;
//resultadocli
$GLOBALS['municipio']=$municipio;
$GLOBALS['email']=$email;
//estadorow
$GLOBALS['estado']=$estado;
//resultadoclitel
$GLOBALS['tel_local']=$tel_local;
//resultadoclipcontacto
$GLOBALS['persona_contacto']=$persona_contacto;
//rentaventas
$GLOBALS['rentaventas']=$rentaventas;
//resultadoc
$GLOBALS['videncia']=$videncia;
$GLOBALS['clicks_mono']=$clicks_mono;
$GLOBALS['precio_c_e_mono']=$precio_c_e_mono;
$GLOBALS['clicks_color']=$clicks_color;
$GLOBALS['precio_c_e_color']=$precio_c_e_color;
$GLOBALS['rentaadelantada']=$rentaadelantada;
$GLOBALS['rentadeposito']=$rentadeposito;
$GLOBALS['rentacolor']=$rentacolor;
$GLOBALS['tipocontrato']=$tipocontrato;
$GLOBALS['tiporenta']=$tiporenta;
$GLOBALS['idcontrato']=$idcontrato;
//contrato
$GLOBALS['fechainicio']=$fechainicio;
$GLOBALS['ordencompra']=$ordencompra;
$GLOBALS['horaentregainicio']=$horaentregainicio;
$GLOBALS['horaentregafin']=$horaentregafin;
$GLOBALS['direccion_c']=$direccion_c;
$GLOBALS['equipo_acceso']=$equipo_acceso;
$GLOBALS['doc_acceso']=$doc_acceso;

$GLOBALS['d_calle']=$d_calle;
$GLOBALS['d_numero']=$d_numero;
$GLOBALS['d_colonia']=$d_colonia;
$GLOBALS['d_ciudad']=$d_ciudad;
$GLOBALS['d_estado']=$d_estado;
$GLOBALS['domicilio_entrega']=$domicilio_entrega;
//
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      /// datos completos
      $html = '';
      /*
      $html = '<table border="1" cellpadding="2">
                <tr>
                  <td width="25%"><br><br>
                    <img src="'.base_url().'app-assets/images/'.$GLOBALS['configuracionCotizacion']->logo1.'" style="padding: 1px;">
                  </td>
                  <td width="50%">
                    '.$GLOBALS['configuracionCotizacion']->textoHeader.'
                  </td>
                  <td width="25%"><br><br>
                    <img class="" src="'.base_url().'app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo">
                  </td>
                </tr>
              </table>';
      // datos completos        
      $html.='<table border="1" width="100%" cellpadding="2">
                <tr>
                  <td class="tdstitle" style="width: 10%;">FECHA</td>
                  <td style="width: 5%;">'.$GLOBALS['dia_reg'].'</td>
                  <td style="width: 5%;">'.$GLOBALS['mes_reg'].'</td>
                  <td style="width: 5%;">'.$GLOBALS['ano_reg'].'</td>
                  <td class="tdstitle" style="width: 5%">AC</td>
                  <td style="width: 10%;">'.$GLOBALS['ini_personal'].'</td>
                  <td class="tdstitle" style="width: 20%">PROINVEN No.</td>
                  <td style="width: 10%;">'.$GLOBALS['proinven'].'</td>
                  <td class="tdstitle" style="width: 10%">CDFI</td>
                  <td style="width: 20%">'.$GLOBALS['cfd'].'</td>
                </tr>
              </table>';
      // datos en procesos          
      $html.='<table border="1" width="100%" cellpadding="2">
                <tr>
                  <td style="width: 6%;">Cia.</td>
                  <td style="width: 68%;">'.$GLOBALS['razon_social'].'</td>
                  <td style="width: 6%;">RFC</td>
                  <td style="width: 20%;">'.$GLOBALS['rfc'].'</td>
                </tr>
                <tr>
                  <td style="width: 6%; font-size: 9px">VENCE</td>
                  <td style="width: 10%;">
                   '.$GLOBALS['vence'].'
                  </td>
                  <td style="width: 12%; font-size: 11px; text-align: center;">METODO DE PAGO</td>
                  <td style="width: 17%; font-size: 11px; text-align: center;">
                    '.$GLOBALS['metodopago_text'].'
                  </td>
                  <td style="width: 10%; font-size: 11px; text-align: center;">FORMA DE PAGO</td>
                  <td style="width: 12%; font-size: 11px; text-align: center;">
                    '.$GLOBALS['formapago_text'].'
                  </td>
                  <td style="width: 13%; font-size: 11px;">USO DE CFDI</td>
                  <td style="width: 20%; font-size: 11px;">
                    '.$GLOBALS['uso_cfdi_text'].'
                  </td> 
                </tr>
              </table>'; 
      // datos en proceso         
      $html.='<table border="1" width="100%" cellpadding="2">
                <tr>
                  <td style="font-size: 11px; width: 5%;">Calle</td>
                  <td style="font-size: 11px; width: 23%;">'.$GLOBALS['calle'].'</td>
                  <td style="font-size: 11px; width: 5%;">No.</td>
                  <td style="font-size: 11px; width: 5%;">'.$GLOBALS['num_ext'].'</td>
                  <td style="font-size: 11px; width: 5%;">Col.</td>
                  <td style="font-size: 11px; width: 24%; "colspan="2">'.$GLOBALS['colonia'].'</td>
                  <td style="font-size: 11px; width: 13%; text-align: center;">FORMA DE COBRO</td>
                  <td style="font-size: 11px; width: 20%;">'.$GLOBALS['formacobro'].'</td>
                </tr>
                <tr>
                  <td style="font-size: 11px; width: 5%;">Cd</td>
                  <td style="font-size: 11px; width: 21%;">'.$GLOBALS['municipio'].'</td>
                  <td style="font-size: 11px; width: 5%;">Edo.</td>
                  <td style="font-size: 11px; width: 35%;">'.$GLOBALS['estado'].' C.P '.$GLOBALS['cp'].'</td>
                  <td style="font-size: 11px; width: 5%;">Tel.</td>
                  <td style="font-size: 11px; width: 29%;">'.$GLOBALS['tel_local'].'</td>
                </tr>
                <tr>
                  <td style="font-size: 11px; width: 10%;">Contacto</td>
                  <td style="font-size: 11px; width: 21%;">'.$GLOBALS['persona_contacto'].'</td>
                  <td style="font-size: 11px; width: 6%;">Cargo</td>
                  <td style="font-size: 11px; width: 34%;">'.$GLOBALS['cargo'].'</td>
                  <td style="font-size: 11px; width: 5%;">email:</td>
                  <td style="font-size: 11px; width: 24%;">'.$GLOBALS['email'].'</td>
                </tr>
                <tr>
                  <td style="font-size: 11px; width: 15%;">Fecha de entrega</td>
                  <td style="font-size: 11px; width: 10%;">Orden de compra</td>
                  <td style="font-size: 11px; width: 18%;">Horario de entrega</td>
                  <td style="font-size: 11px; width: 12%;" rowspan="2">REFERENCIA DOMICILIO</td>
                  <th style="font-size: 11px; width: 45%; text-align: center; font-weight: bold;" rowspan="2">LUGAR DE INSTALACIÓN: '.$GLOBALS['direccion_c'].'. '.$GLOBALS['equipo_acceso'].'. '.$GLOBALS['doc_acceso'].'</th>
                </tr>
                <tr>
                  <td style="font-size: 11px; width: 15%;">'.$GLOBALS['fechainicio'].'</td>
                  <td style="font-size: 11px; width: 10%;">'.$GLOBALS['ordencompra'].'</td>
                  <td style="font-size: 11px; width: 18%;">'.$GLOBALS['horaentregainicio'].' a '.$GLOBALS['horaentregafin'].'</td>
                </tr>
              </table>';

      */        
      $this->writeHTML($html, true, false, true, false, '');
  }

    // Page footer
  public function Footer() {
    $html = '';
    /*
    $html.='    
        <table border="1">
          <tbody>
            <tr>
              <th style="font-size: 11px; text-align: center; font-weight: bold;">Deposito a nombre de "ALTA PRODUCTIVIDAD, S.A DE C.V.", BANAMEX, Cuenta 7347132 sucursal 826, CLABET/Transferencia Electrónica 002668082673471327</th>
            </tr>
          </tbody>        
        </table>';
      
    $html.='
      <table border="1">
        <thead>
        <tr>
          <td style="font-size: 10px; text-align: center;" >ACEPTA QUE SE SURTA ESTA ORDEN DE COMPRA, EN LOS TÉRMINOS ESTABLECIDOS</td>
          <td style="font-size: 11px; text-align: center;" >ALMACEN</td>
          <td style="font-size: 11px; text-align: center;" >ASESOR COMERCIAL</td>
          <td style="font-size: 11px; text-align: center;" >GERENCIA GENERAL</td>
        </tr>
        </thead>
        <tbody >
          <tr >
            <td style="font-size: 11px; text-align: center;"><br><br></td>
            <td style="font-size: 11px; text-align: center;"></td>
            <td style="font-size: 11px; text-align: center;"></td>
            <td style="font-size: 11px; text-align: center;"></td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td style="font-size: 11px; text-align: center;">NOMBRE Y FIRMA DEL CLIENTE</td>
            <td style="font-size: 11px; text-align: center;">FIRMA</td>
            <td style="font-size: 11px; text-align: center;">FIRMA</td>
            <td style="font-size: 11px; text-align: center;">FIRMA</td>
          </tr>
        </tfoot>        
      </table>';
    */  
      $html .= '<table width="100%" border="0">
                  <tr>
                    <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
      $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cuestionario');
$pdf->SetTitle('Cuestionario');
$pdf->SetSubject('Cuestionario');
$pdf->SetKeywords('Cuestionario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '10', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '0');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
// Encabezado
$html.= '<table>
          <tr>
            <td width="25%">
              <img src="'.base_url().'app-assets/images/'.$GLOBALS['configuracionCotizacion']->logo1.'" style="padding: 1px;">
            </td>
            <td width="50%">
              '.$GLOBALS['configuracionCotizacion']->textoHeader.'
            </td>
            <td width="25%"><br><br>
              <img class="" src="'.base_url().'app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo">
            </td>
          </tr>
        </table>';
// datos completos        
$html.='<table width="100%">
          <tr><td style="width: 100%;"></td></tr> 
          <tr>
            <td style="width: 20%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Fecha: </span>'.$GLOBALS['dia_reg'].'/'.$GLOBALS['mes_reg'].'/'.$GLOBALS['ano_reg'].'</td>
            <td style="width: 60%; font-size: 11px; border-bottom: 1px solid #000000; text-align: center;"><span style="font-weight: bold;">Ejecutivo: </span>'.$GLOBALS['ini_personal'].'</td>
            <td style="width: 20%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Prefactura: '.$GLOBALS['proinven'].'</span></td>
          </tr>
        </table>';
// datos en procesos          
$html.='<table width="100%">
          <tr>
            <td style="width: 70%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Cia: </span>'.$GLOBALS['razon_social'].'</td>
            <td style="width: 30%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">RFC: </span>'.$GLOBALS['rfc'].'</td>
          </tr>
          <tr>
            <td style="width: 17%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Vence: </span>'.$GLOBALS['vence'].'</td>
            <td style="width: 33%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Método de pago: </span>'.$GLOBALS['metodopago_text'].'</td>
            <td style="width: 27%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Forma de pago: </span>'.$GLOBALS['formapago_text'].'</td>
            <td style="width: 23%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Uso de CFDI: </span>'.$GLOBALS['uso_cfdi_text'].'</td>
          </tr>
        </table>'; 
// datos en proceso         
$html.='<table width="100%">
          <tr>
            <td style="width: 100%; font-size: 12px; text-align: center;"><span style="font-weight: bold;">Dirección Fiscal: </span></td>
          </tr>
          <tr>
            <td style="width: 28%; font-size: 11px;"><span style="font-weight: bold;">Calle: </span>'.$GLOBALS['d_calle'].'</td>
            <td style="width: 35%; font-size: 11px;"><span style="font-weight: bold;"> </span></td>
            <td style="width: 15%; font-size: 11px;"><span style="font-weight: bold;">CD. </span>'.$GLOBALS['d_ciudad'].'</td>
            <td style="width: 22%; font-size: 11px;"><span style="font-weight: bold;">Estado. </span>'.$GLOBALS['d_estado'].'</td>
          </tr>
          <tr>
            <td style="width: 100%; font-size: 12px; text-align: center;"><span style="font-weight: bold;">Dirección de Entrega/Instalación: </span></td>
          </tr>
          <tr>
            <td style="width: 100%; font-size: 11px;"><span style="font-weight: bold;">Domicilio: </span>'.$GLOBALS['domicilio_entrega'].'</td>
          </tr>
          <tr>
            <td style="width: 100%; font-size: 11px;"></td>
          </tr>
          <tr>
            <td style="width: 30%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Contacto: </span>'.$GLOBALS['persona_contacto'].'</td>
            <td style="width: 40%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Cargo: </span>'.$GLOBALS['cargo'].'</td>
            <td style="width: 30%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">E-mail: </span>'.$GLOBALS['email'].'</td>
          </tr> 
          <tr>
            <td style="width: 100%; font-size: 12px; text-align: center;"><span style="font-weight: bold;">Datos de Entrega: </span></td>
          </tr>
          <tr>
            <td style="width: 25%; font-size: 11px;"><span style="font-weight: bold;">Fecha de entrega: </span>'.$GLOBALS['fechainicio'].'</td>
            <td style="width: 20%; font-size: 11px;"><span style="font-weight: bold;">OC. </span>'.$GLOBALS['ordencompra'].'</td>
            <td style="width: 55%; font-size: 11px; text-align: justify;" rowspan="3"><span style="font-weight: bold;">Referencia de Domicilio: </span>Lugar de instalación '.$GLOBALS['direccion_c'].'. '.$GLOBALS['equipo_acceso'].'. '.$GLOBALS['doc_acceso'].'</td>
          </tr>
          <tr><td></td><td></td></tr> 
          <tr>
            <td style="width: 40%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Horario de entrega: </span>'.$GLOBALS['horaentregainicio'].' a '.$GLOBALS['horaentregafin'].'</td>
            <td style="width: 60%; border-bottom: 1px solid #000000;"></td>
          </tr>
          <tr><td></td></tr> 
          <tr>
            <td style="width: 100%; font-size: 13px; text-align: center; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Información de Equipos: </span></td>
          </tr>
        </table>';
// fin de encabezado
// Curpo de documento
$html.='<table width="100%" >';

              $totalgeneral=0;
                          // Tabla de polizas creadas detalles      
              $aux = 0;
              $totalc = 0;
              $totala = 0;
              foreach ($GLOBALS['rentaventas']->result() as $item) { 
         
                $renta_id = $item->idRenta;
                $equipo_id = $item->idEquipo;
                          $rentaequipos_consumible = $this->ModeloCatalogos->rentaequipos_consumible_total($renta_id,$equipo_id);
                          foreach ($rentaequipos_consumible->result() as $itemc) {
                            $totalc = $itemc->totalc;
                          }
                          $rentaequipos_accesorio = $this->ModeloCatalogos->rentaequipos_accesorios_total($renta_id,$equipo_id);
                          foreach ($rentaequipos_accesorio->result() as $itema) {
                            $totala = $itema->totala;
                          }
                          
                $aux = $totalc+$totala+2;
                $totalgeneral=$totalgeneral+($item->precio*$item->cantidad);
                    $bodega = $item->bodega;
                    $html.='<tr>
                              <th style="font-size: 11px; text-align: center; font-weight: bold; border-bottom: 1px solid #000000;">Cantidad</th>
                              <th style="font-size: 11px; text-align: center; font-weight: bold; border-bottom: 1px solid #000000;">Surtir</th>
                              <th style="font-size: 11px; text-align: center; font-weight: bold; border-bottom: 1px solid #000000;">No. de Parte</th>
                              <th style="font-size: 11px; text-align: center; font-weight: bold; border-bottom: 1px solid #000000;" colspan="2">Equipos</th>
                              <th style="font-size: 11px; text-align: center; font-weight: bold; border-bottom: 1px solid #000000;">No. de Equipo</th>
                            </tr>';
                    $tiposervicio = '';
                    if($GLOBALS['tiporenta']==2){
                       $tiposervicio ='Global';
                    }else if($GLOBALS['tiporenta']==1){
                       $tiposervicio ='Individual'; 
                    }       
                    $html.='<tr>
                                <td style="font-size: 11px; text-align: center; border-bottom: 1px solid #000000;">'.$item->cantidad.'</td>
                                <td style="font-size: 11px; text-align: center; border-bottom: 1px solid #000000;">'.$item->bodega.'</td>
                                <td style="font-size: 11px; border-bottom: 1px solid #000000;">'.$item->noparte.'</td>
                                <td style="font-size: 11px; border-bottom: 1px solid #000000;" colspan="4">
                                  <table width="100%">
                                    <tr>
                                      <td width="10%"></td>   
                                      <td width="75%"><span style="font-weight: bold;">Tipo de servicio: </span>Renta '.$tiposervicio.'</td>
                                      <td width="45%"></td> 
                                      <td width="15%"></td>   
                                    </tr>
                                    <tr>
                                      <td width="10%"></td>  
                                      <td width="75%"><span style="font-weight: bold;">Modelo: </span>'.$item->modelo.'</td>   
                                      <td width="15%"></td>   
                                    </tr>
                                    <tr>
                                      <td width="10%"></td>  
                                      <td width="75%"><span style="font-weight: bold;">Tipo de impresora: </span>'.$item->categoria.'</td>   
                                      <td width="15%"></td>   
                                    </tr>
                                  </table>';
                                $html.='<table width="100%">
                                          <tr>
                                            <td width="10%"><span style="font-weight: bold;">No.</span></td>   
                                            <td width="75%" style="text-align: center;"><span style="font-weight: bold;">Serie</span></td> 
                                            <td width="15%"></td>   
                                          </tr>';
                                $equipo_id_r = $item->id; 
                                $datosseries = $this->ModeloCatalogos->rentaventasdseries($item->id); 
                                $auxy=0;
                                foreach($datosseries->result() as $items) {  
                                  $id_serie = $items->serieId;
                                  $auxy = $auxy + 1;
                                  $html.='<tr>
                                            <td width="10%">'.$auxy.'</td>  
                                            <td width="75%" style="text-align: center;">'.$items->serie.'</td>    
                                            <td width="15%">'.$items->folio_equipo.'</td>   
                                          </tr>';
                                }
                                $auxy++;
                          $html.='</table>
                                </td>
                          </tr>';
                          $rentaequipos_consumible = $this->ModeloCatalogos->rentaequipos_consumible($renta_id,$equipo_id);
                          foreach ($rentaequipos_consumible->result() as $item) {
                          $html.='<tr>
                                   <td style="font-size: 11px; text-align: center; border-bottom: 1px solid #000000;"></td>    
                                   <td style="font-size: 11px; text-align: center; border-bottom: 1px solid #000000;"></td>    
                                   <td style="font-size: 11px; text-align: center; border-bottom: 1px solid #000000;"></td>    
                                   <td style="font-size: 11px; text-align: center; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Toner</span></td>    
                                   <td style="font-size: 11px; text-align: center; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Modelo</span></td>    
                                   <td style="font-size: 11px; text-align: center; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Folio</span></td>    
                                  </tr>
                                  <tr>
                                   <td style="font-size: 11px; text-align: center; border-bottom: 1px solid #000000;">'.$item->cantidad.'</td>    
                                   <td style="font-size: 11px; text-align: center; border-bottom: 1px solid #000000;">'.$bodega.'</td>    
                                   <td style="font-size: 11px; text-align: center; border-bottom: 1px solid #000000;">'.$item->parte.'</td>    
                                   <td style="font-size: 11px; text-align: center; border-bottom: 1px solid #000000;"></td>    
                                   <td style="font-size: 11px; text-align: center; border-bottom: 1px solid #000000;">'.$item->modelo.'</td>    
                                   <td style="font-size: 11px; text-align: center; border-bottom: 1px solid #000000;">
                                   ';
                                    $where = array('idrenta'=>$renta_id,'idconsumible'=>$item->id_consumibles);
                                    $contrato_folio = $this->ModeloCatalogos->getselectwheren('contrato_folio',$where);
                                    foreach ($contrato_folio->result() as $itemx) {
                                       $html.=$itemx->foliotext.',';
                                    }  
                            $html.='</td>    
                                  </tr>
                                 ';
                          }
                          $rentaequipos_accesorios = $this->ModeloCatalogos->rentaequipos_accesorios($renta_id,$equipo_id);
                          foreach ($rentaequipos_accesorios->result() as $item) {
                          $html.='<tr>
                                   <td style="font-size: 11px; text-align: center;">'.$item->cantidad.'</td>    
                                   <td style="font-size: 11px; text-align: center;">'.$bodega.'</td>    
                                   <td style="font-size: 11px; text-align: center;">'.$item->no_parte.'</td>    
                                   <td style="font-size: 11px; text-align: center;" colspan="3">'.$item->nombre.'</td>    
                                   <td style="font-size: 11px; text-align: center;"></td>    
                                   <td style="font-size: 11px; text-align: center;"></td>    
                                  </tr>
                                 ';
                          }

                  if($GLOBALS['tiporenta']==1){   
                      foreach($datosseries->result() as $itemss) { 
                        $where_ce=array('contrato'=>$GLOBALS['idcontrato'],'equiposrow'=>$equipo_id_r,'serieId'=>$itemss->serieId);//$id_serie no se si se tendria que quitar esta variable para que muestra los costos por cada equipo/serie xxx
                        $resultado_contrato_equipos = $this->ModeloCatalogos->getselectwheren('contrato_equipos',$where_ce);
                        foreach ($resultado_contrato_equipos->result() as $item) {
                          $html.='<tr>
                                  <td colspan="8">
                                    <table>
                                      <tbody >';
                                $html.='
                                        <tr>
                                          <td colspan="8"></td>
                                        </tr>
                                        <tr>
                                          <td colspan="8" style="font-size: 13px; text-align: center; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Condiciones de arrendamiento</span></td>
                                          </tr>
                                          <tr>
                                            <td colspan="4" style="font-size: 13px; text-align: center; border-bottom: 1px solid #000000;">
                                              <span style="font-weight: bold;">
                                                Renta: 
                                              </span>
                                              <span style="font-weight: bold; color:red;">$'.$item->rentacosto.' MXN</span>
                                            </td>
                                            <td colspan="4" style="font-size: 13px; text-align: center; border-bottom: 1px solid #000000;">
                                              <span style="font-weight: bold;">
                                                Renta: 
                                              </span>
                                              <span style="font-weight: bold; color:red;">$'.$item->rentacostocolor.' MXN</span>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td colspan="8" style="font-size: 13px; text-align: center;"><span style="font-weight: bold;">Volumen Incluido '.$itemss->serie.'</span></td>
                                        </tr>
                                        <tr>
                                          <td colspan="4" style="font-size: 13px; text-align: center;"><span style="font-weight: bold;">Monocromática</span></td>
                                          <td colspan="4" style="font-size: 13px; text-align: center;"><span style="font-weight: bold;">Color</span></td>
                                        </tr>  
                                        <tr>
                                          <td colspan="2" style="font-size: 13px; text-align: center;"><span style="font-weight: bold;">Clicks</span></td>
                                          <td colspan="2" style="font-size: 13px; text-align: center;"><span style="font-weight: bold;">Excedentes</span></td>   
                                          <td colspan="2" style="font-size: 13px; text-align: center;"><span style="font-weight: bold;">Clicks</span></td>
                                          <td colspan="2" style="font-size: 13px; text-align: center;"><span style="font-weight: bold;">Excedentes</span></td>                                    
                                        </tr>
                                        <tr>
                                          <td colspan="2" style="font-size: 13px; text-align: center; border-bottom: 1px solid #000000;"><span style="font-weight: bold; color:red;">'.$item->pag_monocromo_incluidos.'</span></td>
                                          <td colspan="2" style="font-size: 13px; text-align: center; border-bottom: 1px solid #000000;"><span style="font-weight: bold; color:red;">'.$item->excedente.'</span></td>
                                          <td colspan="2" style="font-size: 13px; text-align: center; border-bottom: 1px solid #000000;"><span style="font-weight: bold; color:red;">'.$item->excedentecolor.'</span></td>
                                          <td colspan="2" style="font-size: 13px; text-align: center; border-bottom: 1px solid #000000;"><span style="font-weight: bold; color:red;">'.$item->excedentecolor.'</span></td>                                      
                                        </tr>
                                        


                                        ';   
           
                                  $html.='</tbody>        
                                    </table>
                                  </td> 
                                </tr>
                                <tr>
                                  <td colspan="8"></td>
                                </tr> 
                          ';
                        }
                      }
                        $html.='<br pagebreak="true"/>';
                  } 
              
              }
              if($GLOBALS['tiporenta']==2){        
                    $html.='<tr >
                              <td colspan="8" >
                                <table >';
                            $html.='
                                    <tr>
                                      <td colspan="8"></td>
                                    </tr>
                                    <tr>
                                      <td colspan="8" style="font-size: 13px; text-align: center; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Condiciones de arrendamiento</span></td>
                                    </tr>
                                    <tr>
                                      <td colspan="4" style="font-size: 13px; text-align: center;">
                                        <span style="font-weight: bold;">Renta mensual: </span>
                                        <span style="font-weight: bold; color:red;">$'.$GLOBALS['rentadeposito'].' MXN</span>
                                      </td>
                                      <td colspan="4" style="font-size: 13px; text-align: center;">
                                        <span style="font-weight: bold;">Renta mensual: </span>
                                        <span style="font-weight: bold; color:red;"> $'.$GLOBALS['rentacolor'].' MXN</span></td>
                                    </tr>
                                    <tr>
                                      <td colspan="8" style="font-size: 13px; text-align: center;"><span style="font-weight: bold;">Volumen Incluido</span></td>
                                    </tr>
                                    <tr>
                                      <td colspan="4" style="font-size: 13px; text-align: center;"><span style="font-weight: bold;">Monocromática</span></td>
                                      <td colspan="4" style="font-size: 13px; text-align: center;"><span style="font-weight: bold;">Color</span></td>
                                    </tr>  
                                    <tr>
                                      <td colspan="2" style="font-size: 13px; text-align: center;"><span style="font-weight: bold;">Clicks</span></td>
                                      <td colspan="2" style="font-size: 13px; text-align: center;"><span style="font-weight: bold;">Excedentes</span></td>   
                                      <td colspan="2" style="font-size: 13px; text-align: center;"><span style="font-weight: bold;">Clicks</span></td>
                                      <td colspan="2" style="font-size: 13px; text-align: center;"><span style="font-weight: bold;">Excedentes</span></td>                                    
                                    </tr> 
                                    <tr>
                                      <td colspan="2" style="font-size: 13px; text-align: center;"><span style="font-weight: bold; color:red;">'.$GLOBALS['clicks_mono'].'</span></td>
                                      <td colspan="2" style="font-size: 13px; text-align: center;"><span style="font-weight: bold; color:red;">'.$GLOBALS['precio_c_e_mono'].'</span></td>
                                      <td colspan="2" style="font-size: 13px; text-align: center;"><span style="font-weight: bold; color:red;">'.$GLOBALS['clicks_color'].'</span></td>
                                      <td colspan="2" style="font-size: 13px; text-align: center;"><span style="font-weight: bold; color:red;">'.$GLOBALS['precio_c_e_color'].'</span></td>                                      
                                    </tr> 
                                    
                                    ';      
                                $html.='      
                                </table>

                              </td> 
                            </tr>
                            <tr>
                              <td colspan="8"></td>
                            </tr> 
                      ';
              }
      $html.='    
        </table>';
// Fin del cuerpo 
// pie de pagina 
    $html.='    
        <table>
        <tr>
            <th style="font-size: 12px; text-align: center; font-weight: bold;">Depósito o Transferencias:</th>
            </tr> 
            <tr>
              <th style="font-size: 12px; text-align: center; font-weight: bold; border-bottom: 1px solid #000000;">Deposito a nombre de "ALTA PRODUCTIVIDAD, S.A DE C.V.", BANAMEX, Cuenta 7347132 sucursal 826, CLABET/Transferencia Electrónica 002668082673471327</th>
            </tr>      
        </table>';
      
    $html.='
      <table border="0">
        <thead>
        <tr>
          <td style="font-size: 12px; font-weight: bold; text-align: center;" >Nombre  Cliente</td>
          <td style="font-size: 12px; font-weight: bold; text-align: center;" >Almacen</td>
          <td style="font-size: 12px; font-weight: bold; text-align: center;" >Asesor Comercial</td>
          <td style="font-size: 12px; font-weight: bold; text-align: center;" >Gerencia</td>
        </tr>
        </thead>
        <tbody >
          <tr >
            <td style="font-size: 11px; text-align: center;"><br><br></td>
            <td style="font-size: 11px; text-align: center;"></td>
            <td style="font-size: 11px; text-align: center;"></td>
            <td style="font-size: 11px; text-align: center;"></td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td style="font-size: 11px; font-weight: bold; text-align: center;">FIRMA</td>
            <td style="font-size: 11px; font-weight: bold; text-align: center;">FIRMA</td>
            <td style="font-size: 11px; font-weight: bold; text-align: center;">FIRMA</td>
            <td style="font-size: 11px; font-weight: bold; text-align: center;">FIRMA</td>
          </tr>
        </tfoot>        
      </table>';
        
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Captura.pdf', 'I');

?>