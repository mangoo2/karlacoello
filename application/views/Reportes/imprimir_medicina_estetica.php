<?php

    require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF/tcpdf.php');
    $this->load->helper('url');
    //var_dump($GLOBALS['folio']);die;
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
    $img_file = base_url().'images/formato/portada_header.jpg'; 
    //$pdf->Image($img_file, 0, 0, 0, 500, '', '', '', false, 500, '', false, false, 0); 
    $this->Image($img_file, 0, 0, 210, 40, '', '', '', false, 330, '', false, false, 0); 
    $html = '<table width="100%" border="0"> 
                    <tr> 
                        <td width="100%" height="80px"></td> 
                    </tr> 
                </table> 
                <table width="100%" border="0"> 
                    <tr> 
                        <td width="20%"></td> 
                        <td width="20%" style="text-align: left;"></td> 
                        <td width="60%" style="text-align: right;"> 
                            <span style="font-weight: bold; font-size: 20px;">Consulta de medicina estética</span>    
                        </td> 
                    </tr> 
                </table>'; 
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $img_file = base_url().'images/formato/portada_footer2.jpg'; 
    //$pdf->Image($img_file, 0, 0, 0, 500, '', '', '', false, 500, '', false, false, 0); 
    $this->Image($img_file, 0, 280, 210, 18, '', '', '', false, 330, '', false, false, 0); 
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Medicina estética');
$pdf->SetTitle('Medicina estética');
$pdf->SetSubject('Medicina estética');
$pdf->SetKeywords('Medicina estética');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '40', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetFooterMargin('15');
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 13);
// add a page
$pdf->AddPage('P', 'A4');
  $html='<table width="100%" border="0"> 
            <tr> 
                <td width="100%"> 
                    <span style="font-size: 15px;">Nombre: </span> <span style="font-weight: bold; font-size: 15px;"><u>'.$paciente->nombre.' '.$paciente->apll_paterno.' '.$paciente->apll_materno.'</u></span> 
                </td> 
            </tr>
            <tr> 
                <td width="100%"> 
                    <span style="font-size: 15px;">Fecha de consulta: </span> <span style="font-weight: bold; font-size: 15px;"><u>'.date('d/m/Y',strtotime($consulta->consultafecha)).'</u></span> 
                </td> 
            </tr> 
            <tr> 
                <td width="100%" style="font-size:60%;"> 
                </td> 
            </tr> 
            <tr style="background-color: #779155; color:white; font-size:53%;">
                <td width="100%" align="center">
                    <span  style="font-size: 10px;" align="center">Datos Generales</span>
                </td>
            </tr>';
            if($consulta->motivo_consulta!=''){
                $html.='<tr> 
                    <td width="100%"> 
                        <b style="font-size: 12px;">Motivo de la consulta: </b><br>
                        <b style="font-size: 12px; text-align: justify">'.$consulta->motivo_consulta.'</b> 
                    </td> 
                </tr>';
            }  
            if($consulta->nota_evaluacion!=''){
                $html.='<tr> 
                    <td width="100%"> 
                        <b style="font-size: 12px;">Nota de evolución: </b><br>
                        <b style="font-size: 12px; text-align: justify">'.$consulta->nota_evaluacion.'</b> 
                    </td> 
                </tr>';
            }    
            if($consulta->altrura!=0 || $consulta->peso!=0 || $consulta->ta!='' || $consulta->tempc!=0 || $consulta->fc!='' || $consulta->fr!=0 || $consulta->o2!=0){
                $html.='<tr style="font-size:100%;">
                            <td width="100%">
                                <b style="font-size: 12px;">Signos Vitales/Básicos</b>
                            </td>
                        </tr>';
            }    
    $html.='<tr style="font-size:100%;">';
                if($consulta->altrura!=0){
         $html.='<td width="19%">
                    <span style="font-size: 12px;">Altura (m) <span style="font-size: 12px; font-weight: bold;">'.$consulta->altrura.'</span></span><br>
                </td>';
                }
                if($consulta->peso!=0){
         $html.='<td width="12%">
                    <span style="font-size: 12px;">Peso <span style="font-size: 12px; font-weight: bold;">'.$consulta->peso.'</span></span><br>
                </td>';
                }
                if($consulta->ta!=''){
         $html.='<td width="13%">
                    <span style="font-size: 12px;">T.A. <span style="font-size: 12px; font-weight: bold;">'.$consulta->ta.'</span></span><br>
                </td>';
                }
                if($consulta->tempc!=0){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">Temp <span style="font-size: 12px; font-weight: bold;">'.$consulta->tempc.'</span></span><br>
                </td>';
                }
                if($consulta->fc!=''){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">F.C. <span style="font-size: 12px; font-weight: bold;">'.$consulta->fc.'</span></span><br>
                </td>';
                }
                if($consulta->fr!=0){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">F.R. <span style="font-size: 12px; font-weight: bold;">'.$consulta->fr.'</span></span><br>
                </td>';
                }
                if($consulta->o2!=0){
         $html.='<td width="14%">
                    <span style="font-size: 12px;">O2 <span style="font-size: 12px; font-weight: bold;">'.$consulta->o2.'</span></span><br>
                </td>';
                }
    $html.='</tr>';
            if($consulta->exploracionfisica!=''){
        $html.='<tr> 
                    <td width="100%"> 
                        <span style="font-size: 12px;">Exploración Física: </span><br> 
                        <b style="font-weight: bold; font-size: 12px;">'.$consulta->exploracionfisica.'</b> 
                    </td> 
                </tr>';
            }  
    $html.='<tr style="background-color: #779155; color:white; font-size:53%;">
                <td width="100%" align="center">
                    <span  style="font-size: 10px;" align="center">Datos Estéticos</span>
                </td>
            </tr>';
            if($consulta->tratamiento_estetico!=''){
        $html.='<tr> 
                    <td width="100%"> 
                        <span style="font-size: 12px;">Tratamientos estéticos previos (botox, rellenos, hilos, laser etc) ¿Hace cuánto tiempo? </span><br> 
                        <b style="font-weight: bold; font-size: 12px;">'.$consulta->tratamiento_estetico.'</b> 
                    </td> 
                </tr>';
            }  
            if($consulta->cirugia_estetica!=''){
        $html.='<tr> 
                    <td width="100%"> 
                        <span style="font-size: 12px;">Cirugías estéticas o reconstructivas ¿Hace cuánto tiempo? </span><br> 
                        <b style="font-weight: bold; font-size: 12px;">'.$consulta->cirugia_estetica.'</b> 
                    </td> 
                </tr>';
            }  
            if($consulta->area_tratar!=''){
        $html.='<tr> 
                    <td width="100%"> 
                        <span style="font-size: 12px;">¿Qué área le gustaría tratar? </span><br> 
                        <span style="font-weight: bold; font-size: 12px;">'.$consulta->area_tratar.'</span> 
                    </td> 
                </tr>';
            }  
            if($consulta->rutina_cuidado!=''){
        $html.='<tr> 
                    <td width="100%"> 
                        <span style="font-size: 12px;">¿Tienes una rutina de cuidado para su piel? </span><br> 
                        <span style="font-weight: bold; font-size: 12px;">'.$consulta->rutina_cuidado.'</span> 
                    </td> 
                </tr>';
            }
        ////
    $html.='<tr style="font-size:100%;">';
                $ti='';
                if($consulta->tipo!=0){
                    if($consulta->tipo==1){
                        $ti='Normal';
                    }else if($consulta->tipo==2){
                        $ti='Grasa';
                    }else if($consulta->tipo==3){
                        $ti='Seca';
                    }else if($consulta->tipo==4){    
                        $ti='Asfíctica';
                    }
         $html.='<td width="13%">
                    <span style="font-size: 12px;">Tipo</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$ti.'</span>
                </td>';
                }
                $es='';
                if($consulta->estado!=0){
                    if($consulta->estado==1){
                        $es='Acneico';
                    }else if($consulta->estado==2){
                        $es='Seborreico';
                    }else if($consulta->estado==3){
                        $es='Hipersudoral';
                    }else if($consulta->estado==4){    
                        $es='Dismetabólico';
                    }else if($consulta->estado==5){    
                        $es='Alípico';
                    }else if($consulta->estado==6){    
                        $es='Querótico';
                    }else if($consulta->estado==7){    
                        $es='Atrópico';
                    }
         $html.='<td width="19%">
                    <span style="font-size: 12px;">Estado</span><br>
                     <span style="font-size: 12px; font-weight: bold;">'.$es.'</span>
                </td>';
                }
                $fo='';
                if($consulta->fototipo!=''){
                    if($consulta->fototipo==1){
                        $fo='l';
                    }else if($consulta->fototipo==2){
                        $fo='ll';
                    }else if($consulta->fototipo==3){
                        $fo='lll';
                    }else if($consulta->fototipo==4){    
                        $fo='lV';
                    }else if($consulta->fototipo==5){    
                        $fo='V';
                    } 
         $html.='<td width="11%">
                    <span style="font-size: 12px;">Fototipo</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$fo.'</span>
                </td>';
                }
                $gr='';
                if($consulta->grosor!=0){
                    if($consulta->grosor==1){
                        $gr='Media';
                    }else if($consulta->grosor==2){
                        $gr='Fina';
                    }else if($consulta->grosor==3){
                        $gr='Gruesa';
                    } 
         $html.='<td width="13%">
                    <span style="font-size: 12px;">Grosor</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$gr.'</span>
                </td>';
                }
                $fl='';
                if($consulta->flacidez!=''){
                    if($consulta->flacidez==1){
                        $fl='Muscular';
                    }else if($consulta->flacidez==2){
                        $fl='Cutánea';
                    }
         $html.='<td width="13%">
                    <span style="font-size: 12px;">Flacidez</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$fl.'</span>
                </td>';
                }
                $hi='';
                if($consulta->higratacion!=0){
                    if($consulta->higratacion==1){
                        $hi='Normal';
                    }else if($consulta->higratacion==2){
                        $hi='Deshidratada';
                    }else if($consulta->higratacion==3){
                        $hi='Hiperhidratada';
                    }
         $html.='<td width="18%">
                    <span style="font-size: 12px;">Hidratación</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$hi.'</span>
                </td>';
                }
                $ci='';
                if($consulta->cicatrizacion!=0){
                    if($consulta->cicatrizacion==1){
                        $ci='Normal';
                    }else if($consulta->cicatrizacion==2){
                        $ci='Deshidratada';
                    }else if($consulta->cicatrizacion==3){
                        $ci='Hidratación';
                    }
         $html.='<td width="20%">
                    <span style="font-size: 12px;">Cicatrización</span><br>
                    <span style="font-size: 12px; font-weight: bold;">'.$ci.'</span>
                </td>';
                }
    $html.='</tr>';
    
            if($consulta->foto_facial!=''){
        $html.='<tr style="background-color: #779155; color:white; font-size:53%;">
                    <td width="100%" align="center">
                        <span  style="font-size: 10px;" align="center">Análisis facial</span>
                    </td>
                </tr>';
            $fh = fopen(base_url()."uploads/analisisfacial/".$consulta->foto_facial, 'r') or die("Se produjo un error al abrir el archivo");
            $linea = fgets($fh);
            fclose($fh);  
        $html.='<tr width="100%">
                    <td width="100%" align="center">
                        <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                            <img style="width: 975px; height: 627px; border: 2px dashed rgb(29, 175, 147); background: url('.base_url().'images/medicina/demo2.png);background-repeat:no-repeat; background-position:center;" src="'.$linea.'" width="975" height="627" style="border:dotted 1px black;">
                        </div>
                    </td>
                </tr>';
            }
       
            $aux_diag=0;
            foreach ($diagn as $item){
                $aux_diag=1;
            }
            if($aux_diag==1){
                $html.='<tr width="100%">
                    <td width="100%">
                        <b style="font-size: 12px;">Diagnósticos:</b>
                        <ol style="font-size: 12px;">';
                        foreach ($diagn as $item){
                         $html.='<li>'.$item->diagnostico.'</li>';
                        }
                        $html.='</ol>';
                    $html.='</td>
                </tr>';
            }
            $aux_trata=0;
            foreach ($trata as $item){
                $aux_trata=1;
            }
            if($aux_trata==1){
                $html.='<tr width="100%">
                    <td width="100%">
                        <b style="font-size: 12px;">Tratamiento:</b>
                        <ol style="font-size: 12px;">';
                        foreach ($trata as $item){
                         $html.='<li>'.$item->tratamiento.'</li>';
                        }
                        $html.='</ol>';
                    $html.='</td>
                </tr>';
            }
            $aux_servi=0;
            foreach ($servi as $item){
                $aux_servi=1;
            }
            if($aux_servi==1){
                
            $html.='<tr style="background-color: #779155; color:white; font-size:53%;">
                        <td width="100%" align="center">
                            <span  style="font-size: 10px;" align="center">Servicios específicos</span>
                        </td>
                    </tr>';

            $html.='<tr style="font-size:100%;">
                        <td width="30%">
                            <span style="font-size: 12px;">Servicio</span>
                        </td>
                        <td width="70%">
                            <span style="font-size: 12px;">Descripción</span>
                        </td>';
            $html.='</tr>';    
                foreach ($servi as $item){
            $html.='<tr width="100%">
                        <td width="30%">
                            <span style="font-size: 12px;">'.$item->servicio.'</span>
                        </td>
                        <td width="70%">
                            <span style="font-size: 12px;">'.$item->descripcion.'</span>
                        </td>
                    </tr>';
                }

            }
            $aux_produc=0;
            foreach ($produc as $item){
                $aux_produc=1;
            }
            if($aux_produc==1){
                
            $html.='<tr style="background-color: #779155; color:white; font-size:53%;">
                        <td width="100%" align="center">
                            <span  style="font-size: 10px;" align="center">Productos específicos</span>
                        </td>
                    </tr>';

            $html.='<tr style="font-size:100%;">
                        <td width="30%">
                            <span style="font-size: 12px;">Producto</span>
                        </td>
                        <td width="25%">
                            <span style="font-size: 12px;">Lote</span>
                        </td>
                        <td width="25%">
                            <span style="font-size: 12px;">Caducidad</span>
                        </td>
                        <td width="20%">
                            <span style="font-size: 12px;">Cantidad</span>
                        </td>';
            $html.='</tr>';    
                foreach ($produc as $item){
            $html.='<tr width="100%">
                        <td width="30%">
                            <span style="font-size: 12px;">'.$item->producto.'</span>
                        </td>
                        <td width="25%">
                            <span style="font-size: 12px;">'.$item->lote.'</span>
                        </td>
                        <td width="25%">
                            <span style="font-size: 12px;">'.$item->fecha_caducidad.'</span>
                        </td>
                        <td width="20%">
                            <span style="font-size: 12px;">'.$item->cantidad.'</span>
                        </td>
                    </tr>';
                }
                
            }
            if($consulta->recomendaciones!=''){
        $html.='<tr style="font-size:10%;">
                    <td width="100%" style="border-bottom: 2px solid #779155;">
                    </td>
                </tr> 
                <tr style="font-size:10%;"> 
                    <td width="100%"> 
                        <div style="font-weight: bold;font-size: 12px;">Recomendaciones</div>
                        <div style="font-size: 12px;">'.$consulta->recomendaciones.'</div> 
                    </td> 
                </tr>';
            }  
            $html.='<tr style="font-size:10%;">
                        <td width="100%" style="border-bottom: 2px solid #779155;">
                        </td>
                    </tr>
                    <tr style="font-size:100%;">
                        <td width="20%">
                            <b style="font-size: 12px;">Próxima consulta:</b>
                        </td>
                        <td width="50%">
                            <b style="font-size: 12px;">'.date('d/m/Y',strtotime($consulta->motivo_consulta)).'</b>
                        </td>';
            $html.='</tr>';    
            $html.='<tr width="100%">
                        <td width="50%" align="center">
                            <span style="font-size: 12px;">Nombre y firma del paciente</span>
                        </td>
                        <td width="50%" align="center">
                            <span style="font-size: 12px;">Nombre y firma de la cosmetologa</span>
                        </td>
                    </tr>';
             $html.='<tr width="100%">
                        <td width="50%" align="center">
                            <span style="font-size: 12px;">'.$consulta->firma_paciente_txt.'</span>
                        </td>
                        <td width="50%" align="center">
                            <span style="font-size: 12px;">'.$consulta->firma_cosmetologa_txt.'</span>
                        </td>
                    </tr>';
                    $fhfp1 = fopen(base_url()."uploads/firmapaciente_c1/".$consulta->firma_paciente, 'r') or die("Se produjo un error al abrir el archivo");
                    $lineafp1 = fgets($fhfp1);
                    $fhfc1 = fopen(base_url()."uploads/firmacosmetologa_c1/".$consulta->firma_cosmetologa, 'r') or die("Se produjo un error al abrir el archivo");
                    $lineafc1 = fgets($fhfc1);
            $html.='<tr width="100%">
                        <td width="50%" align="center">
                            <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                                <img style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); background-position:center;" src="'.$lineafp1.'" width="300" height="180" style="border:dotted 1px black;">
                            </div>
                        </td>
                        <td width="50%" align="center">
                            <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                                <img style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); background-position:center;" src="'.$lineafc1.'" width="300" height="180" style="border:dotted 1px black;">
                            </div>
                        </td>
                    </tr>';

        $html.='</table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('laboratorio.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
?>
