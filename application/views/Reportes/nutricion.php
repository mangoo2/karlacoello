<?php
//require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
//require_once dirname(__FILE__) . '/TCPDF2/tcpdf.php';

//====================================================
class MYPDF extends TCPDF {
  
  //Page header
  public function Header() {
      $logos = base_url().'public/img/header.png';
      $footer = base_url().'public/img/footer.png';
      
      $this->Image($logos, 0, 0, 210, 40, '', '', 10, false, 310, '', false, false, 0);
      $this->Image($footer, 0, 282, 210, 15, '', '', 10, false, 310, '', false, false, 0);

      $html = '';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html2='';


    $html2 .= '
      
    ';
      //<img src="http://facturacion33.adminfactura.com.mx/view/image/viamex_logob100.png" width="100px">
      $this->writeHTML($html2, true, false, true, false, '');
      
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nutricion');
$pdf->SetTitle('Nutricion');
$pdf->SetSubject('Nutricion');
$pdf->SetKeywords('Nutricion');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '40', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin('70');

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 7);
// add a page
$pdf->AddPage('P', 'A4');
//===========================================================================================
    $imghr = base_url().'public/img/hr.png';
    $pdf->Image($imghr, 0, 45, 210, 2, '', '', 10, false, 310, '', false, false, 0);
    $pdf->Image($imghr, 0, 96, 210, 2, '', '', 10, false, 310, '', false, false, 0);
    $imgpeso = '#';
    
    
    
    
    $vimg=0;
    //$imgpeso = base_url().'public/img/peso_obesidad_g3.png';
    if($resultconsulta->imc>0 and $resultconsulta->imc<18.5){
      $imgpeso = base_url().'public/img/peso_bajo.png';
      $vimg=1;
    }
    if($resultconsulta->imc>18.5 and $resultconsulta->imc<24.9){
      $imgpeso = base_url().'public/img/peso_normal.png';
      $vimg=2;
    }
    if($resultconsulta->imc>24.9 and $resultconsulta->imc<29.9){
      $imgpeso = base_url().'public/img/peso_sobre.png';
      $vimg=3;
    }
    if($resultconsulta->imc>29.9 and $resultconsulta->imc<34.5){
      $imgpeso = base_url().'public/img/peso_obesidad_g1.png';
      $vimg=4;
    }
    if($resultconsulta->imc>34.5 and $resultconsulta->imc<39.9){
      $imgpeso = base_url().'public/img/peso_obesidad_g2.png';
      $vimg=5;
    }
    if($resultconsulta->imc>=40){
      $imgpeso = base_url().'public/img/peso_obesidad_g3.png';
      $vimg=6;
    }
  if($resultconsulta->comida1==''){
    $comida1='Desayuno';
  }else{
    $comida1=$resultconsulta->comida1;
  }
  if($resultconsulta->comida2==''){
    $comida2='Colacion 1';
  }else{
    $comida2=$resultconsulta->comida2;
  }
  if($resultconsulta->comida3==''){
    $comida3='Comida';
  }else{
    $comida3=$resultconsulta->comida3;
  }
  if($resultconsulta->comida4==''){
    $comida4='colacion 2';
  }else{
    $comida4=$resultconsulta->comida4;
  }
  if($resultconsulta->comida5==''){
    $comida5='Cena';
  }else{
    $comida5=$resultconsulta->comida5;
  }
  if($resultconsulta->comida6==''){
    $comida6='colacion 3';
  }else{
    $comida6=$resultconsulta->comida6;
  }
  if($resultconsulta->comida7==''){
    $comida7='';
  }else{
    $comida7=$resultconsulta->comida7;
  }
    $htmlp='
      <table border="0">
        <tr>
          <td style="text-align:center;font-size: 15px;">
            Información General
          </td>
        </tr>
        <tr>
          <td style="text-align:center;font-size: 15px;">
            
          </td>
        </tr>
      </table>
      <table border="0">';
        $htmlp.='<tr>
          <td style="width:40%; font-size: 13px;">
            Paciente: '.$paciente.'
          </td>
          <td style="width:45%" rowspan="5">
            <table border="0" >
              <tr>
                <td style="width:12%; font-size: 13px; text-align:center; background-color: #788f55; color:white;">IMC</td>
                <td style="width:33%; font-size: 13px; text-align:center; border: 1px solid #cccccc;">'.$resultconsulta->imc.'</td>
                <td style="width:10%; font-size: 13px;"></td>
                <td style="width:12%; font-size: 13px; text-align:center; background-color: #788f55; color:white;">%</td>
                <td style="width:33%; font-size: 13px; text-align:center;     border: 1px solid #cccccc;">'.$resultconsulta->agua.'<br>Agua</td>
              </tr>
              <tr>
                <td colspan="5"></td>
              </tr>
              <tr>
                <td style="font-size: 13px; text-align:center; background-color: #788f55; color:white;">%</td>
                <td style="font-size: 13px; text-align:center;     border: 1px solid #cccccc;">'.$resultconsulta->grasa.'<br>Grasa</td>
                <td></td>
                <td style="font-size: 13px; text-align:center; background-color: #788f55; color:white;">%</td>
                <td style="font-size: 13px; text-align:center;     border: 1px solid #cccccc;">'.$resultconsulta->grasa_magra.'<br>Grasa magna</td>
              </tr>
              <tr>
                <td colspan="5"></td>
              </tr>
            </table>
            <table border="0">
              <tr>
                <td style="width:30%; font-size: 13px;"></td>
                <td style="width:40%; font-size: 13px; text-align:center; background-color: #01c0c8; color:white;">'.$resultconsulta->peso_ideal.'<br>Peso ideal
                </td>
                <td style="width:30%; font-size: 13px;"></td>
                
              </tr>
            </table>
          </td>
          <td style="width:15%" rowspan="5">
            <img src="'.$imgpeso.'" alt="Girl in a jacket" width="90" height="140">
          </td>
        </tr>
        <tr>
          <td style="font-size: 13px;" >
          Edad: '.$resultconsulta->edad.'
          </td>
        </tr>
        <tr>
          <td style="font-size: 13px;">
          Fecha de consulta: '.$resultconsulta->consultafecha.'
          </td>
        </tr>
        <tr>
          <td style="font-size: 13px;">
          Indice de cintura: '.$resultconsulta->cintura.'
          </td>
        </tr>
        <tr>
          <td style="font-size: 13px;">
          Indice de cadera: '.$resultconsulta->cadera.'
          </td>
        </tr>
        
      </table>
      <table border="0">
        <tr>
          <td style="text-align:center;font-size: 15px;">
            Tabla de equivalentes
          </td>
        </tr>
        <tr>
          <td style="text-align:center;font-size: 15px;">
            
          </td>
        </tr>
      </table>
      <style>
        .tdcolorboder{
            border: 1px solid black
        }
        
      </style>
      <table border="0" style="padding:1;">
        <tr>
          <td class="tdcolorboder"># equivalentes</td>
          <td class="tdcolorboder">Grupo de alimentos</td>
          <td class="tdcolorboder">Subgrupos</td>
          <td class="tdcolorboder">'.$comida1.'</td>
          <td class="tdcolorboder">'.$comida2.'</td>
          <td class="tdcolorboder">'.$comida3.'</td>
          <td class="tdcolorboder">'.$comida4.'</td>
          <td class="tdcolorboder">'.$comida5.'</td>
          <td class="tdcolorboder">'.$comida6.'</td>
          <td class="tdcolorboder">'.$comida7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte1.'</td>
          <td class="tdcolorboder">Verduras</td>
          <td class="tdcolorboder"></td>
          <td class="tdcolorboder">'.$resultconsulta->ver1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ver2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ver3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ver4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ver5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ver6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ver7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte2.'</td>
          <td class="tdcolorboder">3 Frutas</td>
          <td class="tdcolorboder"></td>
          <td class="tdcolorboder">'.$resultconsulta->fru1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->fru2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->fru3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->fru4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->fru5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->fru6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->fru7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte3.'</td>
          <td class="tdcolorboder" rowspan="2">*1 Cereales y tuberculos</td>
          <td class="tdcolorboder">a. sin grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->ces1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ces2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ces3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ces4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ces5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ces6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->ces7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte4.'</td>
          <td class="tdcolorboder">b. Con grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->cec1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->cec2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->cec3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->cec4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->cec5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->cec6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->cec7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte5.'</td>
          <td class="tdcolorboder">Leguminosas</td>
          <td class="tdcolorboder"></td>
          <td class="tdcolorboder">'.$resultconsulta->leg1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->leg2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->leg3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->leg4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->leg5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->leg6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->leg7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte6.'</td>
          <td class="tdcolorboder" rowspan="4">1 Alimentos de origen animal</td>
          <td class="tdcolorboder">a. Muy bajo aporte de grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->amu1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amu2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amu3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amu4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amu5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amu6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amu7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte7.'</td>
          
          <td class="tdcolorboder">b. Bajo aporte de grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->aba1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aba2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aba3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aba4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aba5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aba6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aba7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte8.'</td>
          
          <td class="tdcolorboder">c. Moderado aporte de grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->amo1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amo2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amo3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amo4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amo5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amo6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->amo7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte9.'</td>
          
          <td class="tdcolorboder">d. Alto aporte de grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->aal1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aal2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aal3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aal4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aal5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aal6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->aal7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte10.'</td>
          <td class="tdcolorboder" rowspan="4">Leche y yogurt</td>
          <td class="tdcolorboder">a. Descremada</td>
          <td class="tdcolorboder">'.$resultconsulta->led1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->led2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->led3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->led4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->led5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->led6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->led7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte11.'</td>
          
          <td class="tdcolorboder">b. semidescremada</td>
          <td class="tdcolorboder">'.$resultconsulta->les1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->les2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->les3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->les4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->les5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->les6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->les7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte12.'</td>
          
          <td class="tdcolorboder">c. Entera</td>
          <td class="tdcolorboder">'.$resultconsulta->lee1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lee2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lee3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lee4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lee5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lee6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lee7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte13.'</td>
          
          <td class="tdcolorboder">d. con azúcar</td>
          <td class="tdcolorboder">'.$resultconsulta->lec1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lec2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lec3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lec4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lec5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lec6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->lec7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte14.'</td>
          <td class="tdcolorboder" rowspan="2">2 Aceites y grasas</td>
          <td class="tdcolorboder">a. Sin proteina</td>
          <td class="tdcolorboder">'.$resultconsulta->acs1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acs2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acs3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acs4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acs5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acs6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acs7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte15.'</td>
          <td class="tdcolorboder">b. Con Proteina</td>
          <td class="tdcolorboder">'.$resultconsulta->acc1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acc2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acc3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acc4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acc5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acc6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->acc7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte16.'</td>
          <td class="tdcolorboder" rowspan="2">Azucares</td>
          <td class="tdcolorboder">a. Sin grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->azs1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azs2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azs3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azs4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azs5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azs6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azs7.'</td>
        </tr>
        <tr>
          <td class="tdcolorboder">'.$resultconsulta->aporte17.'</td>
          <td class="tdcolorboder">b. Con grasa</td>
          <td class="tdcolorboder">'.$resultconsulta->azc1.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azc2.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azc3.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azc4.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azc5.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azc6.'</td>
          <td class="tdcolorboder">'.$resultconsulta->azc7.'</td>
        </tr>
      </table>
    ';

    $pdf->writeHTML($htmlp, true, false, true, false, '');
//==============================================================
$pdf->AddPage('P', 'A4');
//=============================================================
$pdf->Image($imghr, 0, 45, 210, 2, '', '', 10, false, 310, '', false, false, 0);
$imgmenu = base_url().'public/img/menu.jpg';
$pdf->Image($imgmenu, 0, 47, 210, 100, '', '', 10, false, 310, '', false, false, 0);
$htmlp='
      <table border="0">
        <tr>
          <td style="text-align:center;font-size: 15px;">
            Ejemplo de menú
          </td>
        </tr>
        <tr>
          <td style="text-align:center;font-size: 15px; height:370px;">
            
          </td>
        </tr>
      </table>
      <style>
        .tdcolorbodermenu{
            border-bottom: 1px solid #7a9557;
            font-size: 15px;
            color:#7a9557
        }
        
      </style>
      <table border="0">
        <tr>
          <td class="tdcolorbodermenu">LUNES</td>
          <td class="tdcolorbodermenu">MARTES</td>
          <td class="tdcolorbodermenu">MIERCOLES</td>
          <td class="tdcolorbodermenu">JUEVES</td>
          <td class="tdcolorbodermenu">VIERNES</td>
        </tr>
        <tr>
            <td>';
            if ($resultlu->num_rows()>0) {
              $htmlp.='<table>';
                foreach ($resultlu->result() as $item) {
                  if($item->contenido!='<hr><strong></strong><hr>'){ 
                 $htmlp.='<tr><td>'.$item->contenido.'</td></tr>';
                  }
                }
              $htmlp.='</table>';
            }
  $htmlp.=' </td>
            <td>';
            if ($resultma->num_rows()>0) {
              $htmlp.='<table>';
                foreach ($resultma->result() as $item) {
                  if($item->contenido!='<hr><strong></strong><hr>'){
                    $htmlp.='<tr><td>'.$item->contenido.'</td></tr>';
                  }
                 
                }
              $htmlp.='</table>';
            }
$htmlp.='   </td>
            <td>';
            if ($resultmi->num_rows()>0) {
              $htmlp.='<table>';
                foreach ($resultmi->result() as $item) {
                  if($item->contenido!='<hr><strong></strong><hr>'){ 
                 $htmlp.='<tr><td>'.$item->contenido.'</td></tr>';
                  }
                }
              $htmlp.='</table>';
            }
$htmlp.='   </td>
            <td>';
            if ($resultju->num_rows()>0) {
              $htmlp.='<table>';
                foreach ($resultju->result() as $item) {
                  if($item->contenido!='<hr><strong></strong><hr>'){  
                 $htmlp.='<tr><td>'.$item->contenido.'</td></tr>';
                  }
                }
              $htmlp.='</table>';
            }
$htmlp.='   </td>
            <td>';
            if ($resultvi->num_rows()>0) {
              $htmlp.='<table>';
                foreach ($resultvi->result() as $item) {
                  if($item->contenido!='<hr><strong></strong><hr>'){ 
                 $htmlp.='<tr><td>'.$item->contenido.'</td></tr>';
                  }
                }
              $htmlp.='</table>';
            }
  $htmlp.='</td>
        </tr>';
$htmlp.='</table>';
$pdf->writeHTML($htmlp, true, false, true, false, '');
//=============================================================
$pdf->AddPage('P', 'A4');
      $htmlx='<table>';
        if($grafica!=''){
        $htmlx.='<tr style="background-color: #779155; color:white;">
                    <td width="100%" align="center">
                        <span  style="font-size: 10px;" align="center">Grafica de evolución</span>
                    </td>
                </tr>'; 
        $htmlx.='<tr>
                    <td>
                        <div >
                            <img style="" src="'.base_url().'uploads/graficas_nutricion/'.$grafica.'">
                        </div>
                    </td>
                </tr>';
            }

        if($resultconsulta->recomendaciones!=''){
        $htmlx.='<tr style="font-size:10%;">
                    <td width="100%" style="border-bottom: 2px solid #779155;">
                    </td>
                </tr> 
                <tr style="font-size:10%;"> 
                    <td width="100%"> 
                        <div style="font-weight: bold;font-size: 12px;">Recomendaciones</div>
                        <div style="font-size: 12px;">'.$resultconsulta->recomendaciones.'</div> 
                    </td> 
                </tr>';
            }  
          
          
      $htmlx.='</table>';
$pdf->writeHTML($htmlx, true, false, true, false, '');
//=============================================================
$pdf->AddPage('P', 'A4');
//=============================================================
//$pdf->Image($imghr, 0, 45, 210, 2, '', '', 10, false, 310, '', false, false, 0);
$imgcom1 = base_url().'public/img/comida1.jpg';
$imgcom2 = base_url().'public/img/comida2.jpg';
$htmld='
      <table border="0">
        <tr>
          <td style="text-align:center;font-size: 15px;">
            Tips
          </td>
        </tr>
        <tr>
          <td style="text-align:center;font-size: 15px;">
            
          </td>
        </tr>
      </table>
      <table border="0">
        
        <tr>
          <td style="font-size: 12px;">OJO. Tambien la cantidad es muy importante. Recuerda revisar tu plan de alimentacion y elegir las porciones adecuadas para ti.
          </td>
        </tr>
        <tr>
          <td style="text-align:center">
            <img src="'.$imgcom1.'" alt="Girl in a jacket" >
          </td>
        </tr>
      </table>
      <table border="0">
        
        <tr>
          <td style="width:25%;"></td>
          <td style="width:50%;font-size: 11px;"><B>TIP</B></td>
          <td style="width:25%;"></td>
        </tr>
        <tr>
          <td></td>
          <td style="font-size: 11px;">Todas las comidas peincipales tiene que tener, por lo menos, un alimento de cada uno de los 3 grupos del plato del buen comer.</td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td style="font-size: 11px;">
            <ol>
              <li>Frutas y verduras (50%)</li>
              <li>Cereales (25%)</li>
              <li>Leguminosas y alimentos de origen animal (25%)</li>
            </ol>
          </td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td style="font-size: 12px; text-align:center">
            <img src="'.$imgcom2.'" alt="Girl in a jacket" >
          </td>
          <td></td>
        </tr>
      </table>
      <table>
        <tr>
          <td style="width:20%;"></td>
          <td style="width:60%; font-size: 12px; text-align:center;">Recomendaciones</td>
          <td style="width:20%;"></td>
        </tr>
        <tr>
          <td></td>
          <td style="font-size: 11px;">
            <ul>
              <li>COCINA CON ACEITE DE COCO O AGUACATE</li>
              <li>UTILIZA SARTEN DE CERAMICA (o teflón)</li>
              <li>PREPARA VERDURAS PARA TODO EL DIA PUEDES USAR UNA VAPORERA</li>
              <li>COCINA TUS LEGUMINOSAS (frijoles, lentejas, alubias) 1 a 2 VECES POR SEMANA Y GUARDALAS EN EL REFRI</li>
              <li>SI COMES PROTEÍNA ANIMAL COMPRA 1 VEZ A LA SEMANA, LAVALA SEPARA Y CONGELA, Y DESCONGELA SOLO TUS PROCIONES DIARIAS</li>
              <li>SUPLEMENTOS: Vitamina C 1gr diario y Omega 3 1.2gr diarios</li>

            </ul>
          </td>
          <td></td>
        </tr>
        
      </table>
      
      
      
      
    ';
$pdf->writeHTML($htmld, true, false, true, false, '');
//=============================================================
$pdf->Output('Nutricion.pdf', 'I');
//$pdf->Output('../../facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
//$pdf->Output('/facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
?>