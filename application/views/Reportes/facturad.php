<?php
require_once dirname(__FILE__) . '/TCPDF/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF/tcpdf.php';
$GLOBALS["Folio"]=$Folio;
$GLOBALS["Nombrerasonsocial"]=$Nombrerasonsocial;
$GLOBALS["folio_fiscal"]=$folio_fiscal;
$GLOBALS["rrfc"]=$rrfc;
$GLOBALS["rdireccion"]=$rdireccion;
$GLOBALS["nocertificadosat"]=$nocertificadosat;
$GLOBALS['regimenf']=$regimenf;
$GLOBALS["certificado"]=$certificado;
$GLOBALS["cfdi"]=$cfdi;
$GLOBALS["fechatimbre"]=$fechatimbre;
$GLOBALS["Estado"]=1;//en dado caso de que se encuentre cancelado

$GLOBALS["cliente"]=$cliente;
$GLOBALS["clirfc"]=$clirfc;
$GLOBALS["clidireccion"]=$clidireccion;
$GLOBALS["isr"]=$isr;
$GLOBALS['numordencompra']=$numordencompra;
$GLOBALS["ivaretenido"]=$ivaretenido;
$GLOBALS["cedular"]=$cedular;

if ($numproveedor!='') {
    $GLOBALS['numproveedor']=$numproveedor;
    $GLOBALS['numproveedorv']='block';
}else{
    $GLOBALS['numproveedor']='';
    $GLOBALS['numproveedorv']='none';
}

if ($numordencompra!='') {
    $GLOBALS['numordencompra']=$numordencompra;
    $GLOBALS['numordencomprav']='block';
}else{
    $GLOBALS['numordencompra']='';
    $GLOBALS['numordencomprav']='none';
}
$GLOBALS['observaciones']=$observaciones;
$GLOBALS["total"]=$total;
$GLOBALS["moneda"]=$moneda;
$GLOBALS["subtotal"]=$subtotal;
$GLOBALS["iva"]=$iva;
$GLOBALS["tarjeta"]=$tarjeta;
$GLOBALS["selloemisor"]=$selloemisor;
$GLOBALS["sellosat"]=$sellosat;
$GLOBALS["cadenaoriginal"]=$cadenaoriginal;
$GLOBALS["FormaPago"]=$FormaPago;
$GLOBALS["FormaPagol"]=$FormaPagol;
$GLOBALS["MetodoPago"]=$MetodoPago;
$GLOBALS["MetodoPagol"] =$MetodoPagol; 
//====================================================
class MYPDF extends TCPDF {
  //===========================================================
      var $Void = ""; 
      var $SP = " "; 
      var $Dot = "."; 
      var $Zero = "0"; 
      var $Neg = "Menos";
      function ValorEnLetras($x, $Moneda ){ 
        $s=""; 
        $Ent=""; 
        $Frc=""; 
        $Signo=""; 
             
        if(floatVal($x) < 0) 
         $Signo = $this->Neg . " "; 
        else 
         $Signo = ""; 
         
        if(intval(number_format($x,2,'.','') )!=$x) //<- averiguar si tiene decimales 
          $s = number_format($x,2,'.',''); 
        else 
          $s = number_format($x,2,'.',''); 
            
        $Pto = strpos($s, $this->Dot); 
             
        if ($Pto === false) 
        { 
          $Ent = $s; 
          $Frc = $this->Void; 
        } 
        else 
        { 
          $Ent = substr($s, 0, $Pto ); 
          $Frc =  substr($s, $Pto+1); 
        } 

        if($Ent == $this->Zero || $Ent == $this->Void) 
           $s = "Cero "; 
        elseif( strlen($Ent) > 7) 
        { 
           $s = $this->SubValLetra(intval( substr($Ent, 0,  strlen($Ent) - 6))) .  
                 "Millones " . $this->SubValLetra(intval(substr($Ent,-6, 6))); 
        } 
        else 
        { 
          $s = $this->SubValLetra(intval($Ent)); 
        } 

        if (substr($s,-9, 9) == "Millones " || substr($s,-7, 7) == "Millón ") 
           $s = $s . "de "; 

        $s = $s . $Moneda; 
        if ($Moneda=='pesos') {
          $abreviaturamoneda='M.N.';
        }else{
          $abreviaturamoneda='U.S.D';
        }

        if($Frc != $this->Void) 
        { 
           $s = $s . " " . $Frc. "/100"; 
           //$s = $s . " " . $Frc . "/100"; 
        } 
        $letrass=$Signo . $s . " ".$abreviaturamoneda; 
        return ($Signo . $s . " ".$abreviaturamoneda);    
      } 
      function SubValLetra($numero) { 
          $Ptr=""; 
          $n=0; 
          $i=0; 
          $x =""; 
          $Rtn =""; 
          $Tem =""; 

          $x = trim("$numero"); 
          $n = strlen($x); 

          $Tem = $this->Void; 
          $i = $n; 
           
          while( $i > 0) 
          { 
             $Tem = $this->Parte(intval(substr($x, $n - $i, 1).  
                                 str_repeat($this->Zero, $i - 1 ))); 
             If( $Tem != "Cero" ) 
                $Rtn .= $Tem . $this->SP; 
             $i = $i - 1; 
          } 

           
          //--------------------- GoSub FiltroMil ------------------------------ 
          $Rtn=str_replace(" Mil Mil", " Un Mil", $Rtn ); 
          while(1) 
          { 
             $Ptr = strpos($Rtn, "Mil ");        
             If(!($Ptr===false)) 
             { 
                If(! (strpos($Rtn, "Mil ",$Ptr + 1) === false )) 
                  $this->ReplaceStringFrom($Rtn, "Mil ", "", $Ptr); 
                Else 
                 break; 
             } 
             else break; 
          } 

          //--------------------- GoSub FiltroCiento ------------------------------ 
          $Ptr = -1; 
          do{ 
             $Ptr = strpos($Rtn, "Cien ", $Ptr+1); 
             if(!($Ptr===false)) 
             { 
                $Tem = substr($Rtn, $Ptr + 5 ,1); 
                if( $Tem == "M" || $Tem == $this->Void) 
                   ; 
                else           
                   $this->ReplaceStringFrom($Rtn, "Cien", "Ciento", $Ptr); 
             } 
          }while(!($Ptr === false)); 

          //--------------------- FiltroEspeciales ------------------------------ 
          $Rtn=str_replace("Diez Un", "Once", $Rtn ); 
          $Rtn=str_replace("Diez Dos", "Doce", $Rtn ); 
          $Rtn=str_replace("Diez Tres", "Trece", $Rtn ); 
          $Rtn=str_replace("Diez Cuatro", "Catorce", $Rtn ); 
          $Rtn=str_replace("Diez Cinco", "Quince", $Rtn ); 
          $Rtn=str_replace("Diez Seis", "Dieciseis", $Rtn ); 
          $Rtn=str_replace("Diez Siete", "Diecisiete", $Rtn ); 
          $Rtn=str_replace("Diez Ocho", "Dieciocho", $Rtn ); 
          $Rtn=str_replace("Diez Nueve", "Diecinueve", $Rtn ); 
          $Rtn=str_replace("Veinte Un", "Veintiun", $Rtn ); 
          $Rtn=str_replace("Veinte Dos", "Veintidos", $Rtn ); 
          $Rtn=str_replace("Veinte Tres", "Veintitres", $Rtn ); 
          $Rtn=str_replace("Veinte Cuatro", "Veinticuatro", $Rtn ); 
          $Rtn=str_replace("Veinte Cinco", "Veinticinco", $Rtn ); 
          $Rtn=str_replace("Veinte Seis", "Veintiseís", $Rtn ); 
          $Rtn=str_replace("Veinte Siete", "Veintisiete", $Rtn ); 
          $Rtn=str_replace("Veinte Ocho", "Veintiocho", $Rtn ); 
          $Rtn=str_replace("Veinte Nueve", "Veintinueve", $Rtn ); 

          //--------------------- FiltroUn ------------------------------ 
          If(substr($Rtn,0,1) == "M") $Rtn = "Un " . $Rtn; 
          //--------------------- Adicionar Y ------------------------------ 
          for($i=65; $i<=88; $i++) 
          { 
            If($i != 77) 
               $Rtn=str_replace("a " . Chr($i), "* y " . Chr($i), $Rtn); 
          } 
          $Rtn=str_replace("*", "a" , $Rtn); 
          return($Rtn); 
      } 
      function ReplaceStringFrom(&$x, $OldWrd, $NewWrd, $Ptr) { 
        $x = substr($x, 0, $Ptr)  . $NewWrd . substr($x, strlen($OldWrd) + $Ptr); 
      } 
      function Parte($x) { 
          $Rtn=''; 
          $t=''; 
          $i=''; 
          Do 
          { 
            switch($x) 
            { 
               Case 0:  $t = "Cero";break; 
               Case 1:  $t = "Un";break; 
               Case 2:  $t = "Dos";break; 
               Case 3:  $t = "Tres";break; 
               Case 4:  $t = "Cuatro";break; 
               Case 5:  $t = "Cinco";break; 
               Case 6:  $t = "Seis";break; 
               Case 7:  $t = "Siete";break; 
               Case 8:  $t = "Ocho";break; 
               Case 9:  $t = "Nueve";break; 
               Case 10: $t = "Diez";break; 
               Case 20: $t = "Veinte";break; 
               Case 30: $t = "Treinta";break; 
               Case 40: $t = "Cuarenta";break; 
               Case 50: $t = "Cincuenta";break; 
               Case 60: $t = "Sesenta";break; 
               Case 70: $t = "Setenta";break; 
               Case 80: $t = "Ochenta";break; 
               Case 90: $t = "Noventa";break; 
               Case 100: $t = "Cien";break; 
               Case 200: $t = "Doscientos";break; 
               Case 300: $t = "Trescientos";break; 
               Case 400: $t = "Cuatrocientos";break; 
               Case 500: $t = "Quinientos";break; 
               Case 600: $t = "Seiscientos";break; 
               Case 700: $t = "Setecientos";break; 
               Case 800: $t = "Ochocientos";break; 
               Case 900: $t = "Novecientos";break; 
               Case 1000: $t = "Mil";break; 
               Case 1000000: $t = "Millón";break; 
            } 

            If($t == $this->Void) 
            { 
              $i = $i + 1; 
              $x = $x / 1000; 
              If($x== 0) $i = 0; 
            } 
            else 
               break; 
                  
          }while($i != 0); 
          
          $Rtn = $t; 
          Switch($i) 
          { 
             Case 0: $t = $this->Void;break; 
             Case 1: $t = " Mil";break; 
             Case 2: $t = " Millones";break; 
             Case 3: $t = " Billones";break; 
          } 
          return($Rtn . $t); 
      }  
  //===========================================================
  //Page header
  public function Header() {
      $logos = base_url().'public/img/alta.png';
      if ($GLOBALS["Estado"]==0) {
        $cancelado='<span  style="color:red; font-size:20px"><b>Cancelado</b></span>';
      }else{
        $cancelado='';
      }
        $tamano1='30';//30
        $tamano2='40';//40
        $tamano3='30';//30
      $html = '
          <style type="text/css">
            .info_fac{
              font-size: 9px;
            }
            .info_facd{
              font-size: 8px;
            }
            .httablelinea{
              vertical-align: center;
              border-bottom: 1px solid #9e9e9e;
            }
            
            .httableleft{
              border-left: 1px solid #9e9e9e;
            }
            .httableright{
              border-right: 1px solid #9e9e9e;
            }
            .httabletop{
              border-top: 1px solid #9e9e9e;
            }
          </style>
          <table width="100%" border="0" cellpadding="4px" class="info_fac">
            <tr>
              <td rowspan="10" width="'.$tamano1.'%"><img src="'.$logos.'"></td>
              <td width="50%" valign="top"><h1> Factura Detalles </h1></td>
            </tr>
          </table>

          ';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html2='';
    $styleQR = array('border' => 0, 
         'vpadding' => '0', 
         'hpadding' => '0', 
         'fgcolor' => array(0, 0, 0), 
         'bgcolor' => false, 
         'module_width' => 1, 
         'module_height' => 1);
          $params = $this->serializeTCPDFtagParameters(array($GLOBALS["folio_fiscal"], 'QRCODE,L', '', '', 45, 45, $styleQR, 'N'));
    $html='';

    $html2 .= ' 
          <style type="text/css">
              .fontFooter10{
                font-size: 10px;
              }
              .fontFooterp{
                font-size: 9px;
                margin-top:0px;
              }
              .fontFooter{
                font-size: 9px;
                margin-top:0px;
              }

              .fontFooterp{
                font-size: 8px;
              }
              .fontFooterpt{
                font-size: 7px;
              }
              .fontFooterpt6{
                font-size: 6px;
              }
              p{
                margin:0px;
              }
              .valign{
                vertical-align:middle;
              }
              .httablelinea{
                vertical-align: center;
                border-bottom: 1px solid #9e9e9e;
              }
              .footerpage{
                font-size: 9px;
                color: #9e9e9e;
              }
          </style>';

    $html2 .= '
      <table width="100%" border="0" cellpadding="2" class="fontFooterp">
        <tr>
          <td align="right" class="footerpage">
          Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>
    ';
      $this->writeHTML($html2, true, false, true, false, '');
      
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Kyocera');
$pdf->SetTitle('Folio_'.$GLOBALS["Folio"]);
$pdf->SetSubject('factura');
$pdf->SetKeywords('factura');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '40', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin('20');

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 7);

//====================================================
//$pdf->setPrintHeader(false);
//$pdf->setPrintFooter(false);
//$pdf->SetMargins('10', '10', '10');
//$pdf->SetFooterMargin('10');
$pdf->AddPage('P', 'A4');
  $html='<table border="1" align="center" cellpadding="2" style="background-color: #d8d4d4; color: black">
            <tr valign="middle">
              <th class="httable" style="font-size:12px"><b>CONTADORES</b></th>
            </tr>
          </table>';
  $html.='<table border="1" align="center" cellpadding="2">
          <thead> 
            <tr valign="middle">
              <th class="httable" rowspan="2"><b>Equipo</b></th>
              <th class="httable" rowspan="2"><b>Serie</b></th>
              <th class="httable" colspan="3" ><b>Copias</b></th>
              <th class="httable" colspan="3" ><b>Escaner</b></th>
              <th class="httable" rowspan="2"><b>Produccion</b></th>
              <th class="httable" rowspan="2"><b>Toner consumido</b></th>
            </tr>
            <tr valign="middle">
              <th class="httable"><b>Contador Inicial</b></th>
              <th class="httable"><b>Contador Final</b></th>
              <th class="httable"><b>Producción</b></th>
              <th class="httable"><b>Contador Inicial</b></th>
              <th class="httable"><b>Contador Final</b></th>
              <th class="httable"><b>Producción</b></th>
            </tr>
          </thead>
        ';
$prefacturaId=0;
foreach ($detalleperidofactura->result() as $item) {
  if ($prefacturaId!=$item->prefacturaId) {
      $prefacturaId=$item->prefacturaId;
      $detalleresult=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura',array('prefId'=>$prefacturaId));
      $perdiodo='';
      foreach ($detalleresult->result() as $itemsr) {
        $perdiodo='Periodo de '.$itemsr->periodo_inicial.' al '.$itemsr->periodo_final.' / Fecha de captura '.$itemsr->reg;
      }
      $html.=' 
            <tr valign="middle">
              <td class="httable" colspan="10">'.$perdiodo.'</td>
            </tr>
          
        ';
    }
    $html.=' 
            <tr valign="middle">
              <td class="httable">'.$item->modelo.'</td>
              <td class="httable">'.$item->serie.'</td>
              <td class="httable">'.$item->c_c_i.'</td>
              <td class="httable">'.$item->c_c_f.'</td>
              <td class="httable">'.$tc=$item->c_c_f-$item->c_c_i.'</td>
              <td class="httable">'.$item->e_c_i.'</td>
              <td class="httable">'.$item->e_c_f.'</td>
              <td class="httable">'.$tc=$item->e_c_f-$item->e_c_i.'</td>
              <td class="httable">'.$item->produccion.'</td>
              <td class="httable">'.$item->toner_consumido.'</td>
            </tr>
          
        ';
}
  $html.='</table>';

$pdf->writeHTML($html, true, false, true, false, '');
//=======================================================================

  $html='<br><br><br><br>
          <table border="1" align="center" cellpadding="2" style="background-color: #d8d4d4; color: black">
            <tr valign="middle">
              <th class="httable" style="font-size:12px"><b>TONER</b></th>
            </tr>
          </table>';
  $html.='<table border="1" align="center" cellpadding="2">
          <thead> 
            <tr valign="middle" style="background-color: #d88888; color: black">
              <th class="httable" ><b>TONER</b></th>
              <th class="httable" ><b>FOLIO</b></th>
              <th class="httable"  ><b>ESTATUS</b></th>
              
            </tr>
          </thead>
        ';
foreach ($detalleconsumiblesfolios->result() as $item) {
  if ($item->status==0) {
    $status='';
  }elseif ($item->status==1) {
    $status='stock Cliente';
  }else{
    $status='Retorno';
  }
    $html.=' 
            <tr valign="middle">
              <td class="httable">'.$item->modelo.'</td>
              <td class="httable">'.$item->foliotext.'</td>
              <td class="httable">'.$status.'</td>
            </tr>
          
        ';  
}
 $html.='</table>';
$pdf->writeHTML($html, true, false, true, false, '');
  foreach ($detallecimages->result() as $item) {
    $pdf->AddPage('P', 'A4');
    if($item->tipoimg==0){
      $file = fopen(base_url()."uploads/rentas_hojasestado/".$item->nombre, "r");
      $line = fgets($file);
      $html= $line;
      fclose($file);
    }else{
      $html=base_url()."uploads/rentas_hojasestado/".$item->nombre;
    }
    $pdf->Image($html, 'C', 0, 250, 297, '', '', '', true, 200, 'C', false, false, 0, false, false, true);
    //else  
    //log_message('error', base64_decode(base_url()."uploads/rentas_hojaestado/".$line));
    //log_message('error', '<th class="httable"><img src="'.base64_decode(base_url()."uploads/rentas_hojasestado/".$line).'"></th>');
  }//for

  
//=======================================================================
$pdf->Output('Folios_.pdf', 'I');
//$pdf->Output('../../facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
//$pdf->Output('/facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
?>