<?php

    require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF/tcpdf.php');
    $this->load->helper('url');
    //var_dump($GLOBALS['folio']);die;
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {

    $html = '';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
      $html = ' 
      <table>
        <tr>
            <td width="12%" align="left" class="footerpage"><br><br>
                <span style="font-size: 8px;"> pág '.$this->getAliasNumPage().' - '.$this->getAliasNbPages().'</span>
            </td>
            <td width="55%"><br><br>
                <span style="font-size: 8px;"> Avenida Kepler 2143, Reserva Territorial Atlixcáyotl, 72190 Puebla, Pue. | Estado: Puebla </span>
            </td>
            <td width="24%"><br><br>
                <span style="font-size: 8px;"> Dr. DANTE OROPEZA CANTO | Firma: </span>
            </td>
            <td width="9%">
                <img width="40" src="'.base_url().'public/img/firma_doc.PNG">
            </td>
        </tr> 
      </table>';
      //<td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
      $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cuestionario');
$pdf->SetTitle('Cuestionario');
$pdf->SetSubject('Cuestionario');
$pdf->SetKeywords('Cuestionario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '10', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetFooterMargin('15');
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 13);
// add a page
$pdf->AddPage('P', 'A4');
  $html='<table style="margin-top: 0px">
            <tr style="font-size:10%;">
                <td width="30%">
                    <img width="70" src="'.base_url().'public/img/neurociencias.jpg">
                </td>
                <td width="40%" style="text-align: center; font-size: 11px;">
                    <br><br><br><br>
                    <span >SOLICITUD DE ESTUDIOS DE LABORATORIO</span>
                </td>
                <td width="30%" style="text-align: center; font-size: 11px;" >
                    <br><br><br><br>
                    <span style="text-align: center; font-size: 8px;"> FECHA DE IMPRESIÓN: '.date('d/m/y',strtotime($consultafecha)).'</span>
                </td>
            </tr>
            <tr style="font-size:1%;">
                <td width="100%" style="border-bottom: 2px solid #346c94;">
                </td>
            </tr>
            <tr style="font-size:80%; border-bottom: 2px solid #346c94;">
                <td width="30%" >
                    <span  style="font-size: 10px;">Médico tratante</span><br>
                    <span  style="font-size: 10px;">Dr. DANTE OROPEZA CANTO</span>
                </td>
                <td width="40%"><br><br>
                    <span style="font-size: 10px;">Especialidad: Neurología</span>
                </td>
                <td width="30%">
                    <br><br>
                    <span style="font-size: 10px;"> Cédula: 2018958 / 3413958</span>
                </td>
            </tr>
            <tr style="font-size:10%;">
                <td width="100%" style="border-bottom: 2px solid #346c94;">
                </td>
            </tr>
            <tr style="font-size:25%;">
                <td width="50%"><br><br>
                    <span  style="font-size: 10px;">Nombre del paciente</span><br>
                </td>
                <td width="50%"><br><br>
                    <span style="font-size: 10px;">Edad</span>
                </td>
            </tr>
            <tr style="background-color: #346c94; color:white;">
                <td width="50%">
                    <span  style="font-size: 10px;">'.$paciente.'</span>
                </td>
                <td width="50%">
                    <span style="font-size: 10px;">'.$edad.' años</span>
                </td>
            </tr>
            <tr style="font-size:70%;">
                <td width="100%">
                    <span  style="font-size: 10px;">Hospital Angeles de Puebla</span><br>
                    <span  style="font-size: 10px;">Departamento de Imagenologia. </span><br>
                </td>
            </tr>
            <tr style="background-color: #346c94; color:white;">
                <td width="100%">
                    <span  style="font-size: 10px;">Favor de realizar</span>
                </td>
            </tr>
            <tr >
                <td width="100%">
                    <ul>';
                    foreach ($get_orden_estudio as $item){
                        $arrayinfode = array('idorden'=>$item->idorden,'activo'=>1);
                        $get_orden=$this->General_model->getselectwhereall('orden_estudio_laboratorio',$arrayinfode); 
                        foreach ($get_orden as $iteom){
                      $html.='<li><span  style="font-size: 10px;">'.$iteom->detalles.'</span></li>';
                        }
                    }
             $html.='</ul> 
                </td>
            </tr>
            <tr style="font-size:1%;">
                <td width="100%" style="border-bottom: 2px solid #346c94;">
                </td>
            </tr>
            <tr style="font-size:70%;">
                <td width="100%"><br><br>
                    <span  style="font-size: 10px;">FAVOR DE OTORGAR DISCO AL PACIENTE</span><br>
                    <span  style="font-size: 10px;">Diagnóstico:</span><br>
                    <span style="font-size: 10px;">';
                    foreach ($get_diagnosticos as $item){ 
                    $html.=$item->diagnostico.' , ';
                    }
         $html.='</span> </td>
            </tr>
            <tr style="font-size:10%;">
                <td width="100%" style="border-bottom: 2px solid #346c94;">
                </td>
            </tr>
        </table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('laboratorio.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
?>