
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Karla Coello</title>
<link href="<?php echo base_url();?>assets/dist/css/style.min.css" rel="stylesheet">
</head>
<style type="text/css">
  .btnenvio{
      display: inline-block; padding: 11px 30px;  font-size: 15px; color: #fff; background: #fb9678; border-radius: 60px; text-decoration:none;
      margin-left: auto;
      margin-right: auto;
      margin-top: 20px;
      
  }
</style>
<input type="hidden" id="base_url" value="<?php echo base_url();?>">
<body style="margin:0px; background: #f8f8f8; ">
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
  <div style="max-width: 700px; padding:0 0;  margin: 0px auto; font-size: 14px">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
      <tbody>
        <tr>
          <td style="vertical-align: top;" align="center">
            <a href="#" target="_blank">
              <img src="<?php echo base_url();?>images/Logo.png" alt="Eliteadmin Responsive web app kit" style="border:none; width: 300px;" >
            </a>
          </td>
        </tr>
      </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
      <tbody>
        <tr>
          <td style="background:#00c0c8; padding:20px; color:#fff; text-align:center;">Encuesta</td>
        </tr>
      </tbody>
    </table>
    <div style="padding: 40px; background: #fff;" class="formencuentaenvio">
      <form class="form-horizontal" method="post" id="formencuenta">
        <input type="hidden" name="idpaciente" id="idpaciente" value="<?php echo $idpaciente;?>">
        <div class="row">
          <div class="col-md-12">
            <h4 class="card-title">¿Como calificaría su Experiencia con nuestro servicio.?</h4>
          </div>
        </div>
        <div class="row">
          <div class="custom-control custom-radio col-md-4">
              <input type="radio" id="experiencia1" name="experiencia" class="custom-control-input" value="1">
              <label class="custom-control-label" for="experiencia1">Bastante complacido</label>
          </div>
          <div class="custom-control custom-radio col-md-4">
              <input type="radio" id="experiencia2" name="experiencia" class="custom-control-input" value="2">
              <label class="custom-control-label" for="experiencia2">Complacido</label>
          </div>
          <div class="custom-control custom-radio col-md-4">
              <input type="radio" id="experiencia3" name="experiencia" class="custom-control-input" value="3">
              <label class="custom-control-label" for="experiencia3">Bueno</label>
          </div>
          <div class="custom-control custom-radio col-md-4">
              <input type="radio" id="experiencia4" name="experiencia" class="custom-control-input" value="4">
              <label class="custom-control-label" for="experiencia4">Decepcionante</label>
          </div>
          <div class="custom-control custom-radio col-md-4">
              <input type="radio" id="experiencia5" name="experiencia" class="custom-control-input" value="5">
              <label class="custom-control-label" for="experiencia5">Muy decepcionante</label>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12" style="margin-top: 10px;">
            <h4 class="card-title">¿Que tan probable es que recomiende nuestros servicios a un amigo o colega?</h4>
          </div>
        </div>
        <div class="row">
          <div class="custom-control custom-radio col-md-1">
              <input type="radio" id="recomendacion1" name="recomendacion" class="custom-control-input" value="1">
              <label class="custom-control-label" for="recomendacion1">1</label>
          </div>
          <div class="custom-control custom-radio col-md-1">
              <input type="radio" id="recomendacion2" name="recomendacion" class="custom-control-input" value="2">
              <label class="custom-control-label" for="recomendacion2">2</label>
          </div>
          <div class="custom-control custom-radio col-md-1">
              <input type="radio" id="recomendacion3" name="recomendacion" class="custom-control-input" value="3">
              <label class="custom-control-label" for="recomendacion3">3</label>
          </div>
          <div class="custom-control custom-radio col-md-1">
              <input type="radio" id="recomendacion4" name="recomendacion" class="custom-control-input" value="4">
              <label class="custom-control-label" for="recomendacion4">4</label>
          </div>
          <div class="custom-control custom-radio col-md-1">
              <input type="radio" id="recomendacion5" name="recomendacion" class="custom-control-input" value="5">
              <label class="custom-control-label" for="recomendacion5">5</label>
          </div>
          <div class="custom-control custom-radio col-md-1">
              <input type="radio" id="recomendacion6" name="recomendacion" class="custom-control-input" value="6">
              <label class="custom-control-label" for="recomendacion6">6</label>
          </div>
          <div class="custom-control custom-radio col-md-1">
              <input type="radio" id="recomendacion7" name="recomendacion" class="custom-control-input" value="7">
              <label class="custom-control-label" for="recomendacion7">7</label>
          </div>
          <div class="custom-control custom-radio col-md-1">
              <input type="radio" id="recomendacion8" name="recomendacion" class="custom-control-input" value="8">
              <label class="custom-control-label" for="recomendacion8">8</label>
          </div>
          <div class="custom-control custom-radio col-md-1">
              <input type="radio" id="recomendacion9" name="recomendacion" class="custom-control-input" value="9">
              <label class="custom-control-label" for="recomendacion9">9</label>
          </div>
          <div class="custom-control custom-radio col-md-1">
              <input type="radio" id="recomendacion10" name="recomendacion" class="custom-control-input" value="10">
              <label class="custom-control-label" for="recomendacion10">10</label>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12" style="margin-top: 10px;">
            <h4 class="card-title">¿Alguna sugerencia?</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <textarea class="form-control" id="sugerencia" name="sugerencia"></textarea>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12" style="margin-top: 10px;">
            <h4 class="card-title">¿Alguien de nuestro equipo ha brindado un servicio excelente?</h4>
            <h4 class="card-title">Queremos escucharte</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <input type="text" name="persona" id="persona" class="form-control">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12" style="margin-top: 10px;">
            <h4 class="card-title">Comentarios:</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <textarea class="form-control" id="comentario" name="comentario"></textarea>
          </div>
        </div>
    
      </form>
      <div class="row">
        <div class="col-md-12" style="text-align: center;">
          <a href="#" class="btnenvio" >Enviar</a>
        </div>
      </div>
    </div>
    <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
      <p> Desarrollado por Mangoo Software</p>
    </div>
  </div>
</div>
</body>
</html>
