<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-outline-info"  onclick="modal_empleado()"><i class="fas fa-user-plus"></i> Nuevo Empleado</button> 
                                        <!--
                                        <button type="button" class="btn btn-outline-info" disabled><i class="fas fa-paper-plane"></i> Enviar cita y formulario</button> 
                                        <button type="button" class="btn btn-outline-info" disabled><i class="far fa-heart"></i> Recomentar</button>
                                    -->
                                    </div>
                                    <div class="col-md-6">    
                                        <div class="input-group bootstrap-touchspin">
                                            <span class="input-group-text">Buscar Empleado &nbsp;&nbsp; <i class="fas fa-search"></i></span></span><input id="empleado_busqueda" type="text" class="form-control" placeholder="Escriba nombre del empleado" oninput="reload_registro()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12"></div>
                                        <table class="table" id="table_datos">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th>Foto</th> 
                                                    <th>Nombre</th>
                                                    <th>Correo</th>
                                                    <th>Tel. Celular</th>
                                                    <th>Tel. Casa</th>
                                                    <th>Tel. Oficina</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>    
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>            

<div id="registro_empleado" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                <h4 class="modal-title">Registro de empleado</h4>
                </div>
                <div class="col-lg-1">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3" align="center">
                                <img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="<?php echo base_url(); ?>images/annon.png">
                                <span class="foto_avatar" >
                                    <input type="file" name="foto_avatar" id="foto_avatar" style="display: none">
                                </span>
                                <label for="foto_avatar">
                                    <span class="btn waves-effect waves-light btn-secondary"><i class="fas fa-camera"></i> Cambiar foto</span>
                                </label>
                            </div>
                            <div class="col-md-9">
                                <input type="hidden" id="foto_validar" value="0">
                                <form class="form" method="post" role="form" id="form_empleado">
                                    <input type="hidden" name="personalId" id="personalId" value="0"> 
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label><span style="color: red;">*</span> Nombre completo</label>
                                                <input type="text" name="nombre" id="nombre" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Fecha de nacimiento</label>
                                                <input type="date" name="fechanacimiento" id="fechanacimiento" class="form-control">
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Sexo</label>
                                                <select name="sexo" id="sexo" class="form-control">
                                                    <option disabled="" selected="" value="0">Seleccione</option>
                                                    <option value="1">Masculino</option>
                                                    <option value="2">Femenino</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Correo Electronico</label>
                                                <input type="email" name="correo" id="correo" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Teléfono Celular</label>
                                                <input type="text" name="celular" id="celular" class="form-control">
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Teléfono de Casa</label>
                                                <input type="text" name="telefono" id="telefono" class="form-control">
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Teléfono de Oficina</label>
                                                <input type="text" name="tel_oficina" id="tel_oficina" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Dirección</label>
                                                <input type="text" name="domicilio" id="domicilio" class="form-control">
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Estado civil</label>
                                                <select name="estado_civil" id="estado_civil" class="form-control">
                                                    <option disabled="" selected="" value="0">Seleccione</option>
                                                    <option value="1">Soltero (a)</option>
                                                    <option value="2">Casado (a)</option>
                                                    <option value="3">Divorciado (a)</option>
                                                    <option value="4">Viudo (a)</option>
                                                    <option value="5">Unión Libre</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Notas</label>
                                                <input type="text" name="observaciones" id="observaciones" class="form-control">
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Posición</label>
                                                <input type="text" name="ocupacion" id="ocupacion" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Sueldo</label>
                                                <input type="number" name="sueldo" id="sueldo" class="form-control">
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Fecha de ingreso</label>
                                                <input type="date" name="fechaingreso" id="fechaingreso" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label class="custom-control custom-checkbox m-b-0">
                                                    <input type="checkbox" class="custom-control-input" name="check_baja" id="verificar_check" onclick="check_baja_btn()">
                                                    <span class="custom-control-label" style="color: red">Dar de baja al empleado</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="baja_texto" style="display: none">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Fecha de baja</label>
                                                    <input type="date" name="fechabaja" id="fechabaja" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Motivo</label>
                                                    <textarea name="motivo" id="motivo" class="form-control js-auto-size"></textarea>
                                                </div>
                                            </div>
                                        </div>    
                                    </div>    
                                </form>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect btn_registro" onclick="guarda_empleado()">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="elimina_empleado_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar éste empleado? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_empleado">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_empleado()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>
<div id="entrenamientos_empleado_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Entrenamientos</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <form class="form" method="post" role="form" id="form_registro_entrenamiento">
                                <label>Entrenamiento</label>
                                <input type="hidden" name="identrenamiento" id="identrenamiento" value="0" class="form-control">
                                <input type="text" name="nombre" id="nombre_entrenamiento" class="form-control">
                            </form> 
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label style="color: #21252900">_</label>
                            <button type="button" class="btn waves-effect waves-light btn_sistema" onclick="guarda_registro_entrenamiento()"><i class="fas fa-save"></i></button>
                        </div>
                    </div>    
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12"></div>
                        <table class="table" id="table_entrenamientos">
                            <thead class="bg-blue">
                                <tr>
                                    <th>Nombre</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>   
            </div>
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
            </div>
        </div>   
    </div>
</div>

<div id="elimina_registro_modal_entrenamiento" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar éste entrenamiento? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_entrenamiento">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_registro_entrenamiento()">Aceptar</button>
            </div>
        </div>   
    </div>
</div> 
