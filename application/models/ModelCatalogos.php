<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModelCatalogos extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

////========Paciente=========/////
    function get_paciente($params){
        $columns = array( 
            0=>'idpaciente',
            1=>'personalId',
            2=>'nombre',
            3=>'apll_paterno',
            4=>'apll_materno',
            5=>'foto',
            6=>'reg',
            7=>'ultima_consulta',
            8=>'celular',
            9=>'correo'
        );
        $columnsp = array( 
            1=>'nombre',
            2=>'apll_paterno',
            3=>'apll_materno',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('pacientes');
        $where = array('activo'=>1);
        $this->db->where($where);
        if($params['paciente']!=''){
            $this->db->like('nombre',$params['paciente']);
            $this->db->or_like('apll_paterno',$params['paciente']);
            $this->db->or_like('apll_materno',$params['paciente']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function Get_grupos(){
        $columns = array( 
            0=>'ga.id',
            1=>'ga.grupo_alimento',
            2=>'subgrupo',
            3=>'energia',
            4=>'proteina',
            5=>'lipidos',
            6=>'carbohidratos',
            7=>'sodio',
            8=>'fibra',
            9=>'conteoHc',
            10=>'sg.id AS ids',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('sub_grupos sg');
        $this->db->join('grupos_alimenticios ga', 'ga.id = sg.id_grupo');
        $where = array('sg.status'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query;
    }

    public function total_paciente($params){
        $columns = array( 
            0=>'idpaciente',
            1=>'personalId',
            2=>'nombre',
            3=>'apll_paterno',
            4=>'apll_materno',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('pacientes');
        $where = array('activo'=>1);
        $this->db->where($where);
        if($params['paciente']!=''){
            $this->db->like('nombre',$params['paciente']);
            $this->db->or_like('apll_paterno',$params['paciente']);
            $this->db->or_like('apll_materno',$params['paciente']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    public function buscar_paciente($nom,$idemple){
        $sql='SELECT * FROM pacientes WHERE activo=1 AND personalId='.$idemple.' AND nombre LIKE "%'.$nom.'%"  or apll_paterno LIKE "%'.$nom.'%" or apll_materno LIKE "%'.$nom.'%"';
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function buscar_diagnostico($nom){
        $sql='SELECT DISTINCT * FROM diagnosticos WHERE diagnostico LIKE "%'.$nom.'%" GROUP BY diagnostico';
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function buscar_laboratorio($nom){
        $sql='SELECT DISTINCT * FROM orden_estudio WHERE nombre_laboratorio LIKE "%'.$nom.'%"';
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function buscar_medicamento($nom){
        $sql='SELECT DISTINCT id,nombre,sustancia FROM medicamentos WHERE nombre LIKE "%'.$nom.'%"
              UNION 
              SELECT DISTINCT idmedicamento AS id,medicamento AS nombre,"" AS sustancia FROM receta_medicamento WHERE medicamento LIKE "%'.$nom.'%"
              GROUP BY medicamento';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function buscar_tomar($nom){
        $sql='SELECT DISTINCT * FROM receta_medicamento WHERE tomar LIKE "%'.$nom.'%" GROUP BY tomar';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function buscar_analisis($nom){
        $sql='SELECT DISTINCT id,nombre FROM analisis_medicos WHERE nombre LIKE "%'.$nom.'%"
              UNION
              SELECT DISTINCT idlaboratorio AS id,detalles AS nombre FROM orden_estudio_laboratorio WHERE detalles LIKE "%'.$nom.'%" GROUP BY detalles';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function buscar_interconsulta_medico($nom){
        $sql='SELECT DISTINCT * FROM interconsulta_medico WHERE nombre LIKE "%'.$nom.'%"';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getListcitas($inicio,$fin){
        $strq = 'SELECT c.idcita,p.idpaciente,p.nombre,p.apll_paterno,p.apll_materno,c.color,c.tipo_cita,c.motivo_consulta,c.fecha_consulta,c.hora_de,c.hora_hasta,c.fechafin,c.cita_confirmada,c.paciente_llego,c.paciente_no_llego,c.pago
                 FROM citas AS c 
                 LEFT JOIN pacientes AS p ON p.idpaciente=c.idpaciente 
                 WHERE c.activo=1 AND c.listaespera=0 AND c.fecha_consulta BETWEEN "'.$inicio.'" AND "'.$fin.'"';
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function getcita($id){
        $strq = 'SELECT c.idcita,p.idpaciente,p.nombre,p.apll_paterno,p.apll_materno,c.color,c.tipo_cita,c.motivo_consulta,c.fecha_consulta,c.hora_de,c.hora_hasta,p.celular,co.nombre AS consul,c.motivo_consulta,c.color,c.quiencoresponda,c.motivo_consulta,c.listaespera,c.idconsultorio,c.prioridad,c.cita_confirmada,c.paciente_llego,c.paciente_no_llego,p.correo,p.correo2,c.tipo_consulta
                FROM citas AS c 
                LEFT JOIN pacientes AS p ON p.idpaciente=c.idpaciente 
                LEFT JOIN consultotrio AS co ON co.idconsultorio=c.idconsultorio
                WHERE c.idcita='.$id;
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function getcita345($id){
        $strq = 'SELECT *
                FROM citas 
                WHERE idcita='.$id;
        $query = $this->db->query($strq);
        return $query->result();
    }
    ////// Agenda pagos
    public function getmonto_efectivo($fecha){///Efectivo
        $strq = 'SELECT SUM(ppd.monto) AS total FROM pagos_paciente AS pp 
                INNER JOIN pagos_paciente_detalle AS ppd ON ppd.idpagos_paciente=pp.idpagos_paciente
                WHERE pp.fecha_pago="'.$fecha.'" AND pp.metodo_pago=1';
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() AS $item){
            $total=$item->total;
        }
        return $total;
    }
    public function getmonto_tarjeta($fecha){///Tarjeta
        $strq = 'SELECT SUM(ppd.monto) AS total FROM pagos_paciente AS pp 
                INNER JOIN pagos_paciente_detalle AS ppd ON ppd.idpagos_paciente=pp.idpagos_paciente
                WHERE pp.fecha_pago="'.$fecha.'" AND pp.metodo_pago=2';
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() AS $item){
            $total=$item->total;
        }
        return $total;
    }
    public function getmonto_otros($fecha){///Tarjeta
        $strq = 'SELECT SUM(ppd.monto) AS total FROM pagos_paciente AS pp 
                INNER JOIN pagos_paciente_detalle AS ppd ON ppd.idpagos_paciente=pp.idpagos_paciente
                WHERE pp.fecha_pago="'.$fecha.'" AND pp.metodo_pago!=1 AND pp.metodo_pago!=2';
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() AS $item){
            $total=$item->total;
        }
        return $total;
    }
    public function getmonto_consulta_primera($fecha){///Tarjeta
        $strq = 'SELECT SUM(ppd.monto) AS total FROM pagos_paciente AS pp 
                INNER JOIN pagos_paciente_detalle AS ppd ON ppd.idpagos_paciente=pp.idpagos_paciente
                WHERE pp.fecha_pago="'.$fecha.'" AND ppd.tipo_servicio=1';
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() AS $item){
            $total=$item->total;
        }
        return $total;
    }
    public function getmonto_consulta_sub($fecha){///Tarjeta
        $strq = 'SELECT SUM(ppd.monto) AS total FROM pagos_paciente AS pp 
                INNER JOIN pagos_paciente_detalle AS ppd ON ppd.idpagos_paciente=pp.idpagos_paciente
                WHERE pp.fecha_pago="'.$fecha.'" AND ppd.tipo_servicio=2';
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() AS $item){
            $total=$item->total;
        }
        return $total;
    }
    public function getresumen_consultas($id,$fecha){
        $strq = 'SELECT c.fecha_consulta,c.hora_de,c.hora_hasta,c.motivo_consulta,c.tipo_cita,p.nombre,p.apll_paterno,p.apll_materno,c.cita_confirmada,c.paciente_llego,c.paciente_no_llego,c.idcita,c.idpaciente
                FROM citas AS c
                LEFT JOIN pacientes AS p ON p.idpaciente=c.idpaciente
                WHERE c.personalId='.$id.' AND  c.fecha_consulta="'.$fecha.'"';
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function total_pacientes($id){
        $strq = "SELECT count(*) AS total FROM pacientes
                WHERE personalId=$id AND activo=1";
        $query = $this->db->query($strq);
        return $query->row();
    }
    public function total_consultas_hoy($id,$fecha){
        $strq = "SELECT COUNT(*) AS total FROM pacientes AS p
                 INNER JOIN consultas AS c ON c.idpaciente=p.idpaciente
                 WHERE p.personalId=$id AND c.activo=1 AND consultafecha='".$fecha."'";
        $query = $this->db->query($strq);
        return $query->row();
    }
    public function total_consultas_semana($id,$fecha_inicio,$fecha_fin){
        $strq = "SELECT COUNT(*) AS total FROM pacientes AS p
                 INNER JOIN consultas AS c ON c.idpaciente=p.idpaciente
                 WHERE p.personalId=$id AND c.activo=1 AND consultafecha BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."'";
        $query = $this->db->query($strq);
        return $query->row();
    }
    public function total_consultas_mes_actual($id,$fecha_inicio,$fecha_fin){
        $strq = "SELECT COUNT(*) AS total FROM pacientes AS p
                 INNER JOIN consultas AS c ON c.idpaciente=p.idpaciente
                 WHERE p.personalId=$id AND c.activo=1 AND consultafecha BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."'";
        $query = $this->db->query($strq);
        return $query->row();
    }
    public function total_consultas_semana_anterior($id,$fecha_inicio,$fecha_fin){
        $strq = "SELECT COUNT(*) AS total FROM pacientes AS p
                 INNER JOIN consultas AS c ON c.idpaciente=p.idpaciente
                 WHERE p.personalId=$id AND c.activo=1 AND consultafecha BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."'";
        $query = $this->db->query($strq);
        return $query->row();
    }
    public function total_citas_pendientes($id,$fecha){///Tarjeta
        $strq = 'SELECT count(*) AS total FROM citas AS c
                WHERE c.status=1 AND c.activo=1 AND cita_confirmada=0 AND paciente_llego=0 AND paciente_no_llego=0 AND c.idpaciente>0 AND c.personalId='.$id.' AND c.fecha_consulta<"'.$fecha.'"';
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() AS $item){
            $total=$item->total;
        }
        return $total;
    }
    function get_total_citas_pendientes($params,$id,$fecha){
        $columns = array( 
            0=>'c.idcita',
            1=>'p.idpaciente',
            2=>'p.nombre',
            3=>'p.apll_paterno',
            4=>'p.apll_materno',
            5=>'c.fecha_consulta',
            6=>'p.celular'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('citas c');
        $this->db->join('pacientes p','p.idpaciente=c.idpaciente');
        $where = array('c.status'=>1,'c.activo'=>1 ,'c.cita_confirmada'=>0,'c.paciente_llego'=>0,'c.paciente_no_llego'=>0,'c.idpaciente>'=>0,'c.personalId'=>$id,'c.fecha_consulta<'=>$fecha);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_total_citas_pendientes($params,$id,$fecha){
        $columns = array( 
            0=>'c.idcita',
            1=>'p.idpaciente',
            2=>'p.nombre',
            3=>'p.apll_paterno',
            4=>'p.apll_materno',
            5=>'c.fecha_consulta'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('citas c');
        $this->db->join('pacientes p','p.idpaciente=c.idpaciente');
        $where = array('c.status'=>1,'c.activo'=>1 ,'c.cita_confirmada'=>0,'c.paciente_llego'=>0,'c.paciente_no_llego'=>0,'c.idpaciente>'=>0,'c.personalId'=>$id,'c.fecha_consulta<'=>$fecha);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    public function getcita_lista_espera($idp,$tipo){
        $strq = "SELECT c.idcita,c.idpaciente,p.nombre,p.apll_paterno,c.paciente_llego
                FROM citas AS c 
                LEFT JOIN pacientes AS p ON p.idpaciente=c.idpaciente 
                WHERE c.activo=1 AND c.listaespera=1 AND c.personalId=$idp AND c.prioridad=$tipo";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_ultima_receta($id){
        $sql = "SELECT * FROM receta WHERE idconsulta = $id AND activo=1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_ultima_consulta_medica_estetica($id){
        $sql = "SELECT * FROM consulta_medicina_estetica WHERE activo=1 AND idpaciente=$id 
                ORDER BY consultafecha ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_ultima_consulta_spa($id){
        $sql = "SELECT * FROM consulta_spa WHERE activo=1 AND idpaciente=$id 
                ORDER BY consultafecha ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_ultima_consulta_nutricion($id){
        $sql = "SELECT * FROM consulta_nutricion WHERE activo=1 AND idpaciente=$id 
                ORDER BY consultafecha ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_ultima_cita($id){
        $sql = "SELECT * FROM citas WHERE activo=1 AND cita_confirmada=0 AND idpaciente=$id 
                ORDER BY fecha_consulta DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_select_like_paciente($nom,$app_p,$app_m){
        $apellido_p='';
        if($app_m!=''){
            $apellido_p=" AND apll_materno like '%$app_m%'";
        }else{
            $apellido_p="";
        }
        $strq = "SELECT * FROM pacientes where nombre like '%$nom%' AND apll_paterno like '%$app_p%'".$apellido_p;
        $query = $this->db->query($strq);
        return $query->result();
    }

    //=================================================
     function get_empleados($params){
        $columns = array( 
            0=>'personalId',
            1=>'nombre',
            2=>'fechanacimiento',
            3=>'sexo',
            4=>'correo',
            5=>'telefono',
            6=>'celular',
            7=>'tel_oficina',
            8=>'foto',
            9=>'ocupacion',
            10=>'domicilio',
            11=>'estado_civil',
            12=>'observaciones',
            13=>'sueldo',
            14=>'fechaingreso',
            15=>'fechabaja',
            16=>'motivo',
            17=>'check_baja'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('personal');
        $where = array('estatus'=>1);
        $this->db->where($where);
        if($params['empleado']!=''){
            $this->db->like('nombre',$params['empleado']);
            $this->db->or_like('correo',$params['empleado']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_empleados($params){
        $columns = array( 
            0=>'personalId',
            1=>'nombre',
            2=>'fechanacimiento',
            3=>'sexo',
            4=>'correo',
            5=>'telefono',
            6=>'celular',
            7=>'tel_oficina',
            8=>'foto',
            9=>'ocupacion'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('personal');
        $where = array('estatus'=>1);
        $this->db->where($where);
        if($params['empleado']!=''){
            $this->db->like('nombre',$params['empleado']);
            $this->db->or_like('correo',$params['empleado']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    ///===============!!
    function get_proveedor($params){
        $columns = array( 
            0=>'*',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $columns2 = array( 
            0=>'idproveedor',
            1=>'nombre',
            2=>'correo',
            3=>'celular',
        );
        $this->db->select($select);
        $this->db->from('proveedor');
        $where = array('activo'=>1);
        $this->db->where($where);
        if($params['proveedor']!=''){
            $this->db->like('nombre',$params['proveedor']);
            $this->db->or_like('correo',$params['proveedor']);
            $this->db->or_like('contacto',$params['proveedor']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_proveedor($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('proveedor');
        $where = array('activo'=>1);
        $this->db->where($where);
        if($params['proveedor']!=''){
            $this->db->like('nombre',$params['proveedor']);
            $this->db->or_like('correo',$params['proveedor']);
            $this->db->or_like('contacto',$params['proveedor']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    ///===============!!
    function get_producto($params){
        $columns = array( 
            0=>'p.*',
            1=>'t.nombre AS tipo_producto',
            2=>'c.nombre AS categoria_producto'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $columns2 = array( 
            0=>'p.idproducto',
            1=>'p.descriccion',
            2=>'p.producto',
            3=>'p.codigo',
        );
        $this->db->select($select);
        $this->db->from('productos p');
        $this->db->join('tipo t','t.idtipo=p.tipo','left');
        $this->db->join('categoria c','c.idcategoria=p.categoria','left');
        $where = array('p.activo'=>1);
        $this->db->where($where);
        if($params['producto']!=''){
            $this->db->like('p.descriccion',$params['producto']);
            $this->db->or_like('p.producto',$params['producto']);
            $this->db->or_like('p.codigo',$params['producto']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_producto($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('productos');
        $where = array('activo'=>1);
        $this->db->where($where);
        if($params['producto']!=''){
            $this->db->like('descriccion',$params['producto']);
            $this->db->or_like('producto',$params['producto']);
            $this->db->or_like('codigo',$params['producto']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    ///====== Tipo =========!!
    function get_tipo($params){
        $columns = array( 
            0=>'*',
        );
        $columns2 = array( 
            0=>'idtipo',
            1=>'nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('tipo');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_tipo($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('tipo');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    ///====== Categoria =========!!
    function get_categoria($params){
        $columns = array( 
            0=>'*',
        );
        $columns2 = array( 
            0=>'idcategoria',
            1=>'nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('categoria');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_categoria($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('categoria');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    ///====== Marcas =========!!
    function get_marcas($params){
        $columns = array( 
            0=>'*',
        );
        $columns2 = array( 
            0=>'idmarca',
            1=>'nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('marca');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_marcas($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('marca');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    
    public function getselectwherelikeproveedor($buscar){
        $strq = "SELECT p.idproveedor, p.nombre
                    FROM proveedor as p 
                    where p.activo = 1 AND nombre like '%$buscar%'";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function getselectwherelikeservicio($buscar){
        $strq = "SELECT s.idservicio,s.nombre,s.descripcion,s.costo,s.cantidad
                    FROM servicios as s 
                    where s.activo = 1 AND nombre like '%$buscar%'";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function getstock_producto($id){
        $strq = "SELECT sum(p.stock) AS stock
                    FROM productos_almacen as p 
                    where p.activo=1 AND p.idproducto = $id ";
        $query = $this->db->query($strq);
        return $query->result();
    }

    ///========== Almacen=====!!
    function get_almacen($params){
        
        // c.fecha BETWEEN '$inicio' AND '$fin'
        $columns = array( 
            0=>'p.*'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $columns2 = array( 
            0=>'p.cantidad',
            1=>'p.stock',
            2=>'p.lote',
            3=>'p.fecha_registro',
        );
        $this->db->select($select);
        $this->db->from('productos_almacen p');
        $where = array('p.activo'=>1,'idproducto'=>$params['id_producto']);
        $this->db->where($where);
        if($params['lote']==1){
            //if(fecha_altual<=fecha_caduca && fecha_mayor>=fecha_caduca)     
            $fecha_mayor=date("Y-m-d",strtotime($params['fecha_actual']."+ 7 days"));
            $this->db->where('p.fecha_caducidad >=', $params['fecha_actual']);
            $this->db->where('p.fecha_caducidad <=', $fecha_mayor);
        }else if($params['lote']==2){
            //if(fecha_actual>fecha_caduca)
            $this->db->where('p.fecha_caducidad <=', $params['fecha_actual']);
        }   
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_almacen($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('productos_almacen p');
        $where = array('p.activo'=>1,'idproducto'=>$params['id_producto']);
        $this->db->where($where);
        if($params['lote']==1){
            //if(fecha_altual<=fecha_caduca && fecha_mayor>=fecha_caduca)     
            $fecha_mayor=date("Y-m-d",strtotime($params['fecha_actual']."+ 7 days"));
            $this->db->where('p.fecha_caducidad >=', $params['fecha_actual']);
            $this->db->where('p.fecha_caducidad <=', $fecha_mayor);
        }else if($params['lote']==2){
            //if(fecha_actual>fecha_caduca)
            $this->db->where('p.fecha_caducidad <=', $params['fecha_actual']);
        }   
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    //===============================
    ///====== Espacio de trabajo =========!!
    function get_espacio($params){
        $columns = array( 
            0=>'*',
        );
        $columns2 = array( 
            0=>'idespacio',
            1=>'nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('espacio_trabajo');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_espacio($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('espacio_trabajo');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    ///====== Categoria Servicios =========!!
    function get_categoria_servicios($params){
        $columns = array( 
            0=>'*',
        );
        $columns2 = array( 
            0=>'idcategoria',
            1=>'nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('servicios_categoria');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_categoria_servicios($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('servicios_categoria');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    //==========================
    ///===============!!
    function get_servicios($params){
        $columns = array( 
            0=>'p.*',
            1=>'t.nombre AS servicio',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $columns2 = array( 
            0=>'p.idservicio',
            1=>'p.nombre',
            2=>'p.descripcion',
            3=>'p.cantidad',
        );
        $this->db->select($select);
        $this->db->from('servicios p');
        $this->db->join('servicios_categoria t','t.idcategoria=p.categoria','left');
        $where = array('p.activo'=>1);
        $this->db->where($where);
        if($params['servicio']!=''){
            $this->db->like('p.nombre',$params['servicio']);
            $this->db->or_like('p.encargado',$params['servicio']);
            $this->db->where($where);
            $this->db->or_like('t.nombre',$params['servicio']);
            $this->db->where($where);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_servicios($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('servicios p');
        $this->db->join('servicios_categoria t','t.idcategoria=p.categoria','left');
        $where = array('p.activo'=>1);
        $this->db->where($where);
        if($params['servicio']!=''){
            $this->db->like('p.nombre',$params['servicio']);
            $this->db->or_like('p.encargado',$params['servicio']);
            $this->db->or_like('t.nombre',$params['servicio']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    ///====== Entrenamientos =========!!
    function get_entrenamientos($params){
        $columns = array( 
            0=>'*',
        );
        $columns2 = array( 
            0=>'identrenamiento',
            1=>'nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('entrenamientos');
        $where = array('activo'=>1,'personalId'=>$params['personalId']);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_entrenamientos($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('entrenamientos');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    //((((Medcina estetica))))
    public function buscar_diagnostico_estetico($nom){
        $sql='SELECT DISTINCT * FROM consulta_medicina_estetica_diagnostico WHERE diagnostico LIKE "%'.$nom.'%" GROUP BY diagnostico';
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function buscar_tratamiento_estetico($nom){
        $sql='SELECT DISTINCT * FROM consulta_medicina_estetica_tratamiento WHERE tratamiento LIKE "%'.$nom.'%" GROUP BY tratamiento';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_venta_servicio_c1($id){
        $strq = "SELECT sd.*,se.nombre AS servicio,se.descripcion,se.cuidados_especiales,se.idservicio
                FROM servicio_venta_detalles AS sd
                INNER JOIN servicios AS se ON se.idservicio=sd.idservicio
                where sd.activo = 1 AND sd.idconsulta=".$id;
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function getselectwherelikesproducto($buscar){
        $strq = "SELECT p.*
                FROM productos as p
                where p.activo = 1 AND p.producto like '%$buscar%'";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_venta_productoc1($id){
        $strq = "SELECT sd.*,p.producto,se.lote,se.fecha_caducidad
                FROM productos_venta_detalles AS sd
                INNER JOIN productos_almacen AS se ON se.idalmacen=sd.idalmacen
                INNER JOIN productos AS p ON p.idproducto=se.idproducto
                where sd.activo = 1 AND sd.idconsulta=".$id;
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function get_venta_producto_restac1($id,$cant){
        $strq = "UPDATE productos_almacen AS sd set sd.stock= sd.stock - $cant
                where sd.idalmacen=".$id;
        $query = $this->db->query($strq);
    }
    public function get_aporte_nutrimental_dieta($id){
        $strq = "SELECT su.*
                FROM grupos_alimenticios AS g
                INNER JOIN sub_grupos AS su ON su.id_grupo=g.id
                where su.id=".$id;
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function get_servicios_estetica($id){
        $strq = "SELECT se.nombre,sd.iddetalles,se.cantidad,se.idservicio FROM servicio_venta_detalles AS sd
                INNER JOIN servicios AS se ON se.idservicio=sd.idservicio
                where sd.activo=1 AND sd.idconsulta=".$id;
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function get_servicios_estetica_total($idc,$ids){
        $strq = "SELECT count(*) as total FROM consulta_medicina_estetica_session
                where idconsulta=".$idc." AND idservicio=".$ids;
        $query = $this->db->query($strq);
        return $query->result();
    }
    
    public function get_venta_servicio_c2($id){
        $strq = "SELECT sd.*,se.nombre AS servicio,se.descripcion,se.cantidad,se.idservicio,se.cuidados_especiales,se.idservicio
                FROM consulta_spa_servicio_venta_detalles AS sd
                INNER JOIN servicios AS se ON se.idservicio=sd.idservicio
                where sd.activo = 1 AND sd.idconsulta=".$id;
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_venta_productoc2($id){
        $strq = "SELECT sd.*,p.producto,se.lote,se.fecha_caducidad
                FROM consulta_spa_productos_venta_detalles AS sd
                INNER JOIN productos_almacen AS se ON se.idalmacen=sd.idalmacen
                INNER JOIN productos AS p ON p.idproducto=se.idproducto
                where sd.activo = 1 AND sd.idconsulta=".$id;
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_venta_servicio_c3($id){
        $strq = "SELECT sd.*,se.nombre AS servicio,se.descripcion,se.cuidados_especiales,se.idservicio
                FROM consulta_nutricion_servicio_venta_detalles AS sd
                INNER JOIN servicios AS se ON se.idservicio=sd.idservicio
                where sd.activo = 1 AND sd.idconsulta=".$id;
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_venta_productoc3($id){
        $strq = "SELECT sd.*,p.producto,se.lote,se.fecha_caducidad
                FROM consulta_nutricion_productos_venta_detalles AS sd
                INNER JOIN productos_almacen AS se ON se.idalmacen=sd.idalmacen
                INNER JOIN productos AS p ON p.idproducto=se.idproducto
                where sd.activo = 1 AND sd.idconsulta=".$id;
        $query = $this->db->query($strq);
        return $query->result();
    }
    /// sesiones  spa
    public function get_servicios_spa_total($idc,$ids){
        $strq = "SELECT count(*) as total FROM consulta_medicina_spa_session
                where idconsulta=".$idc." AND idservicio=".$ids;
        $query = $this->db->query($strq);
        return $query->result();
    }
    //// venta de productos pacientes
    public function get_venta_producto_paciente($id){
        $strq = "SELECT sd.*,p.producto,se.lote,se.fecha_caducidad
                FROM pacientes_productos_venta_detalles AS sd
                INNER JOIN productos_almacen AS se ON se.idalmacen=sd.idalmacen
                INNER JOIN productos AS p ON p.idproducto=se.idproducto
                where sd.activo = 1 AND sd.idventa=".$id;
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function get_grafica_nutricion($id){
         $strq = "SELECT *
                FROM consulta_nutricion
                where idpaciente=".$id;
        $query = $this->db->query($strq);
        return $query->result();
    }
    /////////////////////
    public function get_venta_servicio_c1_ce($id){
        $strq = "SELECT sd.*,se.nombre AS servicio,se.descripcion,sd.cuidados_especiales,se.idservicio
                FROM servicio_venta_detalles AS sd
                INNER JOIN servicios AS se ON se.idservicio=sd.idservicio
                where sd.activo = 1 AND sd.idconsulta=".$id;
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function get_venta_servicio_c2_ce($id){
        $strq = "SELECT sd.*,se.nombre AS servicio,se.descripcion,se.cantidad,se.idservicio,sd.cuidados_especiales,se.idservicio
                FROM consulta_spa_servicio_venta_detalles AS sd
                INNER JOIN servicios AS se ON se.idservicio=sd.idservicio
                where sd.activo = 1 AND sd.idconsulta=".$id;
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function get_venta_servicio_c3_ce($id){
        $strq = "SELECT sd.*,se.nombre AS servicio,se.descripcion,sd.cuidados_especiales,se.idservicio
                FROM consulta_nutricion_servicio_venta_detalles AS sd
                INNER JOIN servicios AS se ON se.idservicio=sd.idservicio
                where sd.activo = 1 AND sd.idconsulta=".$id;
        $query = $this->db->query($strq);
        return $query->result();
    }
    //// usuarios
    function get_usurios($params){
        $columns = array( 
            0=>'us.UsuarioID',
            1=>'us.Usuario',
            2=>'pa.nombre as personal',
            3=>'pe.nombre as perfil',
            4=>'pa.foto',
            5=>'pa.personalId',
            6=>'pe.perfilId',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $columns2 = array( 
            0=>'us.UsuarioID',
            1=>'us.Usuario',
            2=>'pa.nombre',
            3=>'pe.nombre',
        );
        $this->db->select($select);
        $this->db->from('usuarios us');
        $this->db->join('personal pa', 'pa.personalId = us.personalId');
        $this->db->join('perfiles pe', 'pe.perfilId = us.perfilId');
        $where = array('us.estatus'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_usuarios($params){
        $columns = array( 
            0=>'us.UsuarioID',
            1=>'us.Usuario',
            2=>'pa.nombre',
            3=>'pe.nombre',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('usuarios us');
        $this->db->join('personal pa', 'pa.personalId = us.personalId');
        $this->db->join('perfiles pe', 'pe.perfilId = us.perfilId');
        $where = array('us.estatus'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    /// Perfil
    //// usuarios
    function get_perfil($params){
        $columns = array( 
            0=>'perfilId',
            1=>'nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('perfiles');
        $where = array('estatus'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_perfil($params){
        $columns = array( 
            0=>'perfilId',
            1=>'nombre'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('usuarios');
        $where = array('estatus'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function get_menu(){
        $sql='SELECT * FROM menu_sub';
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_menu_perfilis($id){
        $sql='SELECT * FROM perfiles_detalles AS pe
            INNER JOIN menu_sub AS me ON me.MenusubId=pe.MenusubId
            WHERE pe.perfilId='.$id;
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_productos_paciente($id){
        $sql='SELECT ppv.cantidad,ppv.preciou,ppv.total,p.producto,pe.nombre,DATE_FORMAT(pv.reg,  "%d / %m / %Y" ) AS reg,pa.lote
            FROM pacientes_productos_venta AS pv
            INNER JOIN pacientes_productos_venta_detalles AS ppv ON ppv.idventa=pv.idventa
            INNER JOIN productos_almacen AS pa ON pa.idalmacen=ppv.idalmacen
            INNER JOIN productos AS p ON p.idproducto=pa.idproducto
            INNER JOIN personal AS pe ON pe.personalId=pv.personalId
            WHERE ppv.idpaciente='.$id;
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_productos_paciente_medicina($id){
        $sql='SELECT ppv.cantidad,ppv.preciou,ppv.total,p.producto,pe.nombre,DATE_FORMAT(pv.reg,  "%d / %m / %Y" ) AS reg,pa.lote
            FROM consulta_medicina_estetica AS pv
            INNER JOIN productos_venta_detalles AS ppv ON ppv.idconsulta=pv.idconsulta
            INNER JOIN productos_almacen AS pa ON pa.idalmacen=ppv.idalmacen
            INNER JOIN productos AS p ON p.idproducto=pa.idproducto
            INNER JOIN personal AS pe ON pe.personalId=pv.personalId
            WHERE pv.idpaciente='.$id;
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_productos_paciente_spa($id){
        $sql='SELECT ppv.cantidad,ppv.preciou,ppv.total,p.producto,pe.nombre,DATE_FORMAT(pv.reg,  "%d / %m / %Y" ) AS reg,pa.lote
            FROM consulta_spa AS pv
            INNER JOIN consulta_spa_productos_venta_detalles AS ppv ON ppv.idconsulta=pv.idconsulta
            INNER JOIN productos_almacen AS pa ON pa.idalmacen=ppv.idalmacen
            INNER JOIN productos AS p ON p.idproducto=pa.idproducto
            INNER JOIN personal AS pe ON pe.personalId=pv.personalId
            WHERE pv.idpaciente='.$id;
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_productos_paciente_nutricion($id){
        $sql='SELECT ppv.cantidad,ppv.preciou,ppv.total,p.producto,pe.nombre,DATE_FORMAT(pv.reg,  "%d / %m / %Y" ) AS reg,pa.lote
            FROM consulta_nutricion AS pv
            INNER JOIN consulta_nutricion_productos_venta_detalles AS ppv ON ppv.idconsulta=pv.idconsulta
            INNER JOIN productos_almacen AS pa ON pa.idalmacen=ppv.idalmacen
            INNER JOIN productos AS p ON p.idproducto=pa.idproducto
            INNER JOIN personal AS pe ON pe.personalId=pv.personalId
            WHERE pv.idpaciente='.$id;
        $query = $this->db->query($sql);
        return $query->result();
    }
}