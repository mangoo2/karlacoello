<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracion extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,10);// 10 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }
	public function index(){
        $data['duracion_m']=$this->General_model->getselectwhereall('config_duracion_medica',array('activo'=>1));
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
		$this->load->view('config/config',$data);
		$this->load->view('templates/footer');
        $this->load->view('config/configjs');
	}

    public function getlistado_gupos(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->Get_grupos($params);
        $totaldata=count($getdata); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function SetDataGrupo(){
        $id=$this->input->post('id');
        $campo=$this->input->post('campo');
        $valor=$this->input->post('valor');
        $data= array($campo => $valor);
        $this->General_model->edit_recordw("id=".$id,$data,"sub_grupos");
    }

    function loadconfiguraciones(){
        $hl = $this->General_model->getselectwhereall('config_horario_laboral',array('status'=>1));
        $hnd = $this->General_model->getselectwhereall('config_horario_nodisponible',array('activo'=>1));
        $config = $this->General_model->getselectwhereall('config_general',array('activo'=>1));
        $sms=0;
        $pagina='Inicio';
        foreach ($config as $item) {
            if ($item->id==1) {
                $sms=$item->status;
            }
            if ($item->id==2) {
                $pagina=$item->url;
            }
        }
        $filasrow= array(
                        'horariol'=>$hl,
                        'horariond'=>$hnd,
                        'sms'=>$sms,
                        'pagina'=>$pagina
                        );
        echo json_encode($filasrow);
    }

    function horariolaboral(){
        $data = $this->input->post();
        $id =$data['id'];
        unset($data['id']);
        $this->General_model->edit_recordw(array('id'=>$id),$data,'config_horario_laboral');
    }
    function save_hora_nd(){
        $data = $this->input->post();
        $datahorarionds = $data['datahorarionds'];
        $hnd = json_decode($datahorarionds);
        //echo count($hnd);
        for ($i=0;$i<count($hnd);$i++) { 
            $datasarray = array(
                            'l'=>$hnd[$i]->l,
                            'm'=>$hnd[$i]->m,
                            'mi'=>$hnd[$i]->mi,
                            'j'=>$hnd[$i]->j,
                            'v'=>$hnd[$i]->v,
                            's'=>$hnd[$i]->s,
                            'd'=>$hnd[$i]->d,
                            'h_inicio'=>$hnd[$i]->h_inicio,
                            'h_fin'=>$hnd[$i]->h_fin,
                                );
            $this->General_model->edit_recordw(array('id'=>$hnd[$i]->id),$datasarray,'config_horario_nodisponible');
        }
    }
    function hora_nd(){
        $datos=array(
            'l'=>0,
                            'm'=>0,
                            'mi'=>0,
                            'j'=>0,
                            'v'=>0,
                            's'=>0,
                            'd'=>0,
                            'h_inicio'=>'00:00',
                            'h_fin'=>'00:00',
        );
        $this->General_model->add_record('config_horario_nodisponible',$datos);
    }
    function visitamedica(){
        $data = $this->input->post();
        $id =$data['id'];
        unset($data['id']);
        $this->General_model->edit_recordw(array('id'=>$id),$data,'config_duracion_medica');
    }
    function configsms(){
        $status = $this->input->post('status');
        $this->General_model->edit_recordw(array('id'=>1),array('status'=>$status),'config_general');
    }
    function configinicio(){
        $inicio = $this->input->post('inicio');
        $this->General_model->edit_recordw(array('id'=>2),array('url'=>$inicio),'config_general');
    }
    function eliminar_horario_disponible(){
        $id = $this->input->post('id');
        $this->General_model->edit_recordw(array('id'=>$id),array('activo'=>0),'config_horario_nodisponible');
    }
    
    public function registro_espacio(){
        $data=$this->input->post();
        $idespacio=$data['idespacio'];
        if($idespacio==0){
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('espacio_trabajo',$data);
        }else{
            $this->General_model->edit_record('idespacio',$idespacio,$data,'espacio_trabajo');
            $id=$idespacio;
        }
        echo $id;
    }

    public function getlistado_espacio(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_espacio($params);
        $totaldata= $this->ModelCatalogos->total_espacio($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function deleteregistro_espacio(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idespacio',$id,$data,'espacio_trabajo');
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_usurios($params);
        $totaldata= $this->ModelCatalogos->total_usuarios($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function validar(){
        $Usuario = $this->input->post('Usuario');
        $result=$this->General_model->getselectwhere('usuarios','Usuario',$Usuario);
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }
    public function add_usuarios(){
        $datos = $this->input->post();
        $pss_verifica = $datos['contrasena'];
        $pass = password_hash($datos['contrasena'], PASSWORD_BCRYPT);
        $datos['contrasena'] = $pass;
        if($pss_verifica == 'xxxxxxxxxx.'){
           unset($datos['contrasena']);
        }
        $id=$datos['UsuarioID'];
        unset($datos['UsuarioID']);
        unset($datos['contrasena2']);
        if ($id>0) {
            $where = array('UsuarioID'=>$id);
            $this->General_model->edit_recordw($where,$datos,'usuarios');
            $result=2;
        }else{
            $this->General_model->add_record('usuarios',$datos);
            $result=1;
        }   
        echo $result;
    }
    public function deleteusuario(){
        $id=$this->input->post('id');
        $data = array('estatus'=>0);
        $this->General_model->edit_record('UsuarioID',$id,$data,'usuarios');
    }
    function validar_perfil(){
        $perfil = $this->input->post('perfil');
        $result=$this->General_model->getselectwhere('perfiles','nombre',$perfil);
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }
    ////////////
    public function getlistado_perfil(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_perfil($params);
        $totaldata= $this->ModelCatalogos->total_perfil($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function deleteperfil(){
        $id=$this->input->post('id');
        $data = array('estatus'=>0);
        $this->General_model->edit_record('perfilId',$id,$data,'perfiles');
    }
    public function get_permisos_perfil(){
        $id=$this->input->post('id');
        $resultm=$this->ModelCatalogos->get_menu();
        $html='<div class="row">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-6">';
                        $html.='<select id="menuid" class="form-control">';
                            $html.='<option disabled="" value="0" selected="">Selecciona</option>">';
                            foreach ($resultm as $item){
                                $html.='<option value="'.$item->MenusubId.'">'.$item->Nombre.'</option>">';
                            }
                        $html.='</select>
                            </div>
                            <div class="col-md-6">
                                <label><spa style="color:#ff000000;">.</spa></label>
                                <button type="button" class="btn waves-effect waves-light btn-rounded btn_perfil_detalle" style="color: #779155;border-color: #779155;" onclick="guardar_permiso_menu('.$id.')"><i class="fas fa-save"></i> Guardar</button>
                            </div>  
                        </div>
                        <table class="table" id="table_datos_usurios">
                          <thead class="bg-blue">
                              <tr>
                                  <th></th>
                                  <th>Menu</th>
                                  <th></th>
                              </tr>
                          </thead>
                          <tbody>';
                            $result=$this->ModelCatalogos->get_menu_perfilis($id);
                            foreach ($result as $item){
                            $html.='<tr>
                                  <td><i class="'.$item->Icon.'"></i></td>
                                  <td>'.$item->Nombre.'</td>
                                  <td><button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-danger" onclick="elimnar_permiso_menu('.$item->Perfil_detalleId.','.$item->perfilId.')"><i class="fas fa-trash-alt"></i></button>
                              </tr>';
                            }
                          $html.='</tbody>
                        </table>
                    </div>';
                    $idpermiso=0;
                    $medicina_estetica=0;
                    $spa=0;
                    $nutricion=0;
                    $result_consultas=$this->General_model->get_records_condition('perfilId='.$id,'permiso_consultas');
                    foreach ($result_consultas as $item){
                        $idpermiso=$item->idpermiso;
                        $medicina_estetica=$item->medicina_estetica;
                        $spa=$item->spa;
                        $nutricion=$item->nutricion;
                    }
                    $medicina_estetica_ch='';
                    $spa_ch='';
                    $nutricion_ch='';
                    if($medicina_estetica==1){
                        $medicina_estetica_ch='checked';
                    }else{
                        $medicina_estetica_ch='';
                    }
                    if($spa==1){
                        $spa_ch='checked';
                    }else{
                        $spa_ch='';
                    }
                    if($nutricion==1){
                        $nutricion_ch='checked';
                    }else{
                        $nutricion_ch='';
                    }
                    $html.='<div class="col-lg-6">
                        <h4 style="text-align: center;">Permiso de consultas</h4>
                        <form class="form" method="post" role="form" id="form_permiso_consultas">
                            <input type="hidden" name="idpermiso" id="idpermiso" value="'.$idpermiso.'"> 
                            <input type="hidden" name="perfilId" id="perfilId" value="'.$id.'">
                            <div class="row">
                              <div class="col-lg-3"></div>
                              <div class="col-lg-9">
                                <div class="custom-control custom-switch">
                                  <input type="checkbox" class="custom-control-input" onclick="guardar_permiso_consulta('.$id.')" name="medicina_estetica" id="medicina_estetica" '.$medicina_estetica_ch.'>
                                  <label class="custom-control-label" for="medicina_estetica">Medicina estética</label>
                                </div>
                              </div>
                              <div class="col-lg-3"></div>
                              <div class="col-lg-9">
                                <div class="custom-control custom-switch">
                                  <input type="checkbox" class="custom-control-input" onclick="guardar_permiso_consulta('.$id.')" name="spa" id="spa" '.$spa_ch.'>
                                  <label class="custom-control-label" for="spa">SPA</label>
                                </div>
                              </div>
                              <div class="col-lg-3"></div>
                              <div class="col-lg-9">
                                <div class="custom-control custom-switch">
                                  <input type="checkbox" class="custom-control-input" onclick="guardar_permiso_consulta('.$id.')" name="nutricion" id="nutricion" '.$nutricion_ch.'>
                                  <label class="custom-control-label" for="nutricion">Nutrición</label>
                                </div>
                              </div>  
                            </div>
                        </form>    
                        <br>
                        <br>
                        <h4 style="text-align: center;">Página principal inicio de sesión</h4>';
                        $id_login=0;
                        $id_menu_sub=0;
                        $result_config_general=$this->General_model->get_records_condition('perfilId='.$id,'config_general');
                        foreach ($result_config_general as $item){
                            $id_login=$item->id;
                            $id_menu_sub=$item->MenusubId;
                        }
                        $html.='
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-lg-8" align="center"> 
                                <form class="form" method="post" role="form" id="form_config_general">
                                    <input type="hidden" name="id" value="'.$id_login.'">
                                    <input type="hidden" name="perfilId" value="'.$id.'">
                                    <select name="MenusubId" class="form-control" onchange="guardar_config_general('.$id.')">';
                                $html.='<option disabled="" value="0" selected="">Selecciona</option>">';
                                foreach ($result as $item){
                                    if($item->MenusubId==$id_menu_sub){
                                    $html.='<option value="'.$item->MenusubId.'" selected>'.$item->Nombre.'</option>">';
                                    }else{
                                    $html.='<option value="'.$item->MenusubId.'">'.$item->Nombre.'</option>">';
                                    }
                                }
                            $html.='</select>
                                </form>    
                            </div>  
                            <div class="col-md-2"></div> 
                        </div>  
                    </div>
                <div>';
        echo $html;
    }
    public function add_perfil_detalle(){
        $datos['perfilId']=$this->input->post('perfilId');
        $datos['MenusubId']=$this->input->post('MenusubId');
        $this->General_model->add_record('perfiles_detalles',$datos);
    }

    public function add_perfil(){
        $datos = $this->input->post();
        $id=$datos['perfilId'];
        if ($id>0) {
            $where = array('perfilId'=>$id);
            $this->General_model->edit_recordw($where,$datos,'perfiles');
        }else{
            $this->General_model->add_record('perfiles',$datos);
        }   
        echo $result;
    }
    public function deleteperfil_detalles(){
        $id=$this->input->post('id');
        $this->General_model->delete_detalle_perfil($id);
    }
    public function add_permisos_consultas(){
        $data = $this->input->post();
        $id=$data['idpermiso'];
        if(isset($data['medicina_estetica'])){
            $data['medicina_estetica']=1;
        }else{
            $data['medicina_estetica']=0; 
        }
        if(isset($data['spa'])){
            $data['spa']=1;
        }else{
            $data['spa']=0; 
        }
        if(isset($data['nutricion'])){
            $data['nutricion']=1;
        }else{
            $data['nutricion']=0; 
        }
        unset($data['idpermiso']);
        if ($id>0) {
            $where = array('idpermiso'=>$id);
            $this->General_model->edit_recordw($where,$data,'permiso_consultas');
        }else{
            $this->General_model->add_record('permiso_consultas',$data);
        }   
        echo $result;
    }

    function modal_texto_perfil(){
        $html='<div class="form-group">
                <label>Perfil</label>';
                $resultp=$this->General_model->getselectwhere('perfiles','estatus',1);
                $html.='<select name="perfilId" id="perfilId" class="form-control">';
                foreach ($resultp as $item) {
                    $html.='<option value="'.$item->perfilId.'">'.$item->nombre.'</option>';
                }
                $html.='</select>
            </div>';
        echo $html;
    }
    public function add_config_general(){
        $data = $this->input->post();
        $id=$data['id'];
        unset($data['id']);
        if ($id>0) {
            $where = array('id'=>$id);
            $this->General_model->edit_recordw($where,$data,'config_general');
        }else{
            $this->General_model->add_record('config_general',$data);
        }   
        echo $result;
    }
}    