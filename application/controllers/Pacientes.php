<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pacientes extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        /*
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }
        */
    }
    public function paciente($id){
        $data['administrador']=$this->administrador;
        $data['idpaciente']=$id; 
        setcookie("idpaciente", $id, time() + 3600, "/ckfinder/");
        $arrayinfo = array('idpaciente'=>$id);
        $list_paciente=$this->General_model->getselectwhereall('pacientes',$arrayinfo);

        $arrayper = array('perfilId'=>$this->perfilid);
        $list_permiso=$this->General_model->getselectwhereall('permiso_consultas',$arrayper);
        $permiso_perfil1=0;
        $permiso_perfil2=0;
        $permiso_perfil3=0;
        foreach ($list_permiso as $item){
            $permiso_perfil1=$item->medicina_estetica;
            $permiso_perfil2=$item->spa;
            $permiso_perfil3=$item->nutricion;
        }
        $data['permiso_perfil1']=$permiso_perfil1;
        $data['permiso_perfil2']=$permiso_perfil2;
        $data['permiso_perfil3']=$permiso_perfil3;
        $foto_p='';
        $nombre_p='';
        $apll_paterno_p='';
        $apll_materno_p='';
        $fechan='';
        $tiempo='';
        $ahora='';
        $edad='';
        $edad='';
        $correo_p='';
        $ocupacion_p='';
        $celular_p='';
        $nota_p='';
        $sexo_p='';
        foreach ($list_paciente as $item) {
            $foto_p=$item->foto;
            $nombre_p=$item->nombre;
            $apll_paterno_p=$item->apll_paterno;
            $apll_materno_p=$item->apll_materno;
            $fechan=$item->fecha_nacimiento;
            $tiempo = strtotime($item->fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
            $edad = floor($edad); 
            $correo_p=$item->correo;
            $ocupacion_p=$item->ocupacion;
            $celular_p=$item->celular;
            $nota_p=$item->nota;
            $sexo_p=$item->sexo;
        }
        $data['foto']=$foto_p;
        $data['nombre']=$nombre_p;
        $data['apll_paterno']=$apll_paterno_p;
        $data['apll_materno']=$apll_materno_p;
        $data['fechan']=$fechan;
        $data['edad']=$edad;
        $data['correo']=$correo_p;
        $data['ocupacion']=$ocupacion_p;
        $data['celular']=$celular_p;
        $data['nota']=$nota_p;
        $data['sexo_p']=$sexo_p;
        ////////////////////////////////////
        ////////////////Relevante / Historia Clínica///////////////////
        $data['idrelevante']='';
        $data['antecedentes']='';
        $data['negadas']='';
        $data['alergias']=''; 
        $data['quien_refiere']=''; 
        $data['motivo_consulta']=''; 
        $data['observaciones']='';

        $arrayhistoria = array('idpaciente'=>$id);
        $list_relevante_historia=$this->General_model->getselectwhereall('relevante_historia',$arrayhistoria);
        foreach ($list_relevante_historia as $item){
            $data['idrelevante']=$item->idrelevante;
            $data['idrelevante']=$item->idrelevante;
            $data['antecedentes']=$item->antecedentes;
            $data['alergias']=$item->alergias;
            if($item->negadas==1){
                $data['negadas']='checked'; 
            }else{
                $data['negadas']='';
            }
            $data['quien_refiere']=$item->quien_refiere; 
            $data['motivo_consulta']=$item->motivo_consulta; 
            $data['observaciones']=$item->observaciones; 
        }
        /////////////Consultas//////////////
        $arrayinfop = array('idpaciente'=>$id,'activo'=>1);
        $get_consultasp_desc=$this->General_model->getselectwhere_orden_asc('consultas',$arrayinfop,'consultafecha');
        $idconsulta_existe=0;
        foreach ($get_consultasp_desc as $item){
            $idconsulta_existe=$item->idconsulta;
        } 
        $data['idconsulta_existe']=$idconsulta_existe;
        $data['fechainicio_v']=$this->fechainicio;
        $data['get_lada']=$this->General_model->get_table('lada');
        ////////////////////////////////////

        $arrayinfo = array('idpaciente'=>$id,'activo'=>1);
        $get_consultas=$this->General_model->getselectwhereall('consultas',$arrayinfo);
        $idpaciente=0;
        $data['idconsulta_ultima']=0;
        foreach ($get_consultas as $item){
            $data['idconsulta_ultima']=$item->idconsulta;    
        } 
        //((( Interrogatorio por sistemas )))
        // Sintomas generales
        $arrayinfoc = array('idpaciente'=>$id);
        $gets1sintomas_generales=$this->General_model->getselectwhereall('s1sintomas_generales',$arrayinfoc);
        $data['idsintomas']=0;
        $data['fiebre']='';
        $data['astenia']='';
        $data['aumento']='';
        $data['modificacion']='';
        foreach ($gets1sintomas_generales as $item){
            $data['idsintomas']=$item->idsintomas;
            $data['fiebre']=$item->fiebre;
            $data['astenia']=$item->astenia;
            $data['aumento']=$item->aumento;
            $data['modificacion']=$item->modificacion; 
        } 
        // Aparato respiratorio
        $gets2aparato_respiratorio=$this->General_model->getselectwhereall('s2aparato_respiratorio',$arrayinfoc);
        $data['idaparato']=0;
        $data['rinorrea']='';
        $data['epistaxis']='';
        $data['tos']='';
        $data['expectoracion']='';
        $data['disfonia']='';
        $data['hemoptitis']='';
        $data['cianosis']='';
        $data['dolor']='';
        $data['disnea']='';
        $data['sibilancias']='';
        foreach ($gets2aparato_respiratorio as $item){
            $data['idaparato']=$item->idaparato;
            $data['rinorrea']=$item->rinorrea;
            $data['epistaxis']=$item->epistaxis;
            $data['tos']=$item->tos;
            $data['expectoracion']=$item->expectoracion;
            $data['disfonia']=$item->disfonia;
            $data['hemoptitis']=$item->hemoptitis;
            $data['cianosis']=$item->cianosis;
            $data['dolor']=$item->dolor;
            $data['disnea']=$item->disnea;
            $data['sibilancias']=$item->sibilancias;
        } 
        // Digestivo
        $gets3digestivo=$this->General_model->getselectwhereall('s3digestivo',$arrayinfoc);
        $data['iddigestivo3']=0;
        $data['hambre3']='';
        $data['apetito3']='';
        $data['masticacion3']='';
        $data['disfagia3']='';
        $data['halitosis3']='';
        $data['nausea3']='';
        $data['rumiacion3']='';
        $data['pirosis3']='';
        $data['aerofagia3']='';
        $data['eructos3']='';
        $data['meteorismo3']='';
        $data['distension3']='';
        $data['gases3']='';
        $data['hematemesis3']='';
        $data['ictericia3']='';
        $data['fecales3']='';
        $data['constipacion3']='';
        $data['haces_blancas3']='';
        $data['verdes3']='';
        $data['haces_negras3']='';
        $data['amarillas3']='';
        $data['rojas3']='';
        $data['esteatorrea_aceitosas3']='';
        $data['parsitos3']='';
        $data['pujo3']='';
        $data['lienteria3']='';
        $data['tenesmo3']='';    
        $data['prurito3']='';
        foreach ($gets3digestivo as $item){
            $data['iddigestivo3']=0;
            $data['hambre3']=$item->hambre;
            $data['apetito3']=$item->apetito;
            $data['masticacion3']=$item->masticacion;
            $data['disfagia3']=$item->disfagia;
            $data['halitosis3']=$item->halitosis;
            $data['nausea3']=$item->nausea;
            $data['rumiacion3']=$item->rumiacion;
            $data['pirosis3']=$item->pirosis;
            $data['aerofagia3']=$item->aerofagia;
            $data['eructos3']=$item->eructos;
            $data['meteorismo3']=$item->meteorismo;
            $data['distension3']=$item->distension;
            $data['gases3']=$item->gases;
            $data['hematemesis3']=$item->hematemesis;
            $data['ictericia3']=$item->ictericia;
            $data['fecales3']=$item->fecales;
            $data['constipacion3']=$item->constipacion;
            $data['haces_blancas3']=$item->haces_blancas;
            $data['verdes3']=$item->verdes;
            $data['haces_negras3']=$item->haces_negras;
            $data['amarillas3']=$item->amarillas;
            $data['rojas3']=$item->rojas;
            $data['esteatorrea_aceitosas3']=$item->esteatorrea_aceitosas;
            $data['parsitos3']=$item->parsitos;
            $data['pujo3']=$item->pujo;
            $data['lienteria3']=$item->lienteria;
            $data['tenesmo3']=$item->tenesmo;
            $data['prurito3']=$item->prurito;
        } 
        // Cardio vascular
        $gets4cardiovascular=$this->General_model->getselectwhereall('s4cardiovascular',$arrayinfoc);
        $data['idcardio4']=0;
        $data['palpitaciones4']='';
        $data['dolor_precordial4']='';
        $data['disnea_esfuerzo4']='';
        $data['disnea_paroxistica4']='';
        $data['apnea4']='';
        $data['cianosis4']='';
        $data['acufenos4']='';
        $data['fosfenos4']='';
        $data['sincope4']='';
        $data['lipotimias4']='';
        $data['edema4']='';
        foreach ($gets4cardiovascular as $item){
            $data['idcardio4']=$item->idcardio;
            $data['palpitaciones4']=$item->palpitaciones;
            $data['dolor_precordial4']=$item->dolor_precordial;
            $data['disnea_esfuerzo4']=$item->disnea_esfuerzo;
            $data['disnea_paroxistica4']=$item->disnea_paroxistica;
            $data['apnea4']=$item->apnea;
            $data['cianosis4']=$item->cianosis;
            $data['acufenos4']=$item->acufenos;
            $data['fosfenos4']=$item->fosfenos;
            $data['sincope4']=$item->sincope;
            $data['lipotimias4']=$item->lipotimias;
            $data['edema4']=$item->edema;
        } 
        // Renal y urinario
        $gets5renalurinario=$this->General_model->getselectwhereall('s5renalurinario',$arrayinfoc);
        $data['idrenal5']=0;
        $data['renoureteral5']='';
        $data['disuria5']='';
        $data['anuria5']='';
        $data['oliguria5']='';
        $data['poliuria5']='';
        $data['polaquiuria5']='';
        $data['hematuria5']='';
        $data['piuria5']='';
        $data['coluria5']='';
        $data['incontinencia5']='';
        $data['edema5']='';
        foreach ($gets5renalurinario as $item){
            $data['idrenal5']=$item->idrenal;
            $data['renoureteral5']=$item->renoureteral;
            $data['disuria5']=$item->disuria;
            $data['anuria5']=$item->anuria;
            $data['oliguria5']=$item->oliguria;
            $data['poliuria5']=$item->poliuria;
            $data['polaquiuria5']=$item->polaquiuria;
            $data['hematuria5']=$item->hematuria;
            $data['piuria5']=$item->piuria;
            $data['coluria5']=$item->coluria;
            $data['incontinencia5']=$item->incontinencia;
            $data['edema5']=$item->edema;
        } 
        // Genital femenino
        $gets6genital_femenino=$this->General_model->getselectwhereall('s6genital_femenino',$arrayinfoc);
        $data['idgenital6']=0;
        $data['leucorrea6']='';
        $data['hemorragias6']='';
        $data['alteraciones_menstruales6']='';
        $data['alteracioneslibido6']='';
        $data['practica_sexual6']='';
        $data['alteraciones_sangrado6']='';
        $data['dispareunia6']='';
        $data['perturbaciones6']='';
        foreach ($gets6genital_femenino as $item){
            $data['idgenital6']=$item->idgenital;
            $data['leucorrea6']=$item->leucorrea;
            $data['hemorragias6']=$item->hemorragias;
            $data['alteraciones_menstruales6']=$item->alteraciones_menstruales;
            $data['alteracioneslibido6']=$item->alteracioneslibido;
            $data['practica_sexual6']=$item->practica_sexual;
            $data['alteraciones_sangrado6']=$item->alteraciones_sangrado;
            $data['dispareunia6']=$item->dispareunia;
            $data['perturbaciones6']=$item->perturbaciones;
        } 
        // Endocrino
        $gets7endocrino=$this->General_model->getselectwhereall('s7endocrino',$arrayinfoc);
        $data['idendocrino7']=0;
        $data['intolerancia_frio7']='';
        $data['hiperactividad7']='';
        $data['aumento_volumen7']='';
        $data['polidipsia7']='';
        $data['polifagia7']='';
        $data['poliuria7']='';
        $data['cambios_caracteres7']='';
        $data['aumento_perdida7']='';
        foreach ($gets7endocrino as $item){
            $data['idendocrino7']=$item->idendocrino;
            $data['intolerancia_frio7']=$item->intolerancia_frio;
            $data['hiperactividad7']=$item->hiperactividad;
            $data['aumento_volumen7']=$item->aumento_volumen;
            $data['polidipsia7']=$item->polidipsia;
            $data['polifagia7']=$item->polifagia;
            $data['poliuria7']=$item->poliuria;
            $data['cambios_caracteres7']=$item->cambios_caracteres;
            $data['aumento_perdida7']=$item->aumento_perdida;
        } 
        // Hematopoyetico
        $gets8hematopoyetico=$this->General_model->getselectwhereall('s8hematopoyetico',$arrayinfoc);
        $data['idhematopoyetico8']=0;
        $data['palidez8']='';
        $data['disnea8']='';
        $data['fatigabilidad8']='';
        $data['palpitaciones8']='';
        $data['sangrado8']='';
        $data['equimosis8']='';
        $data['petequia8']='';
        $data['astenia8']='';
        foreach ($gets8hematopoyetico as $item){
            $data['idhematopoyetico8']=$item->idhematopoyetico;
            $data['palidez8']=$item->palidez;
            $data['disnea8']=$item->disnea;
            $data['fatigabilidad8']=$item->fatigabilidad;
            $data['palpitaciones8']=$item->palpitaciones;
            $data['sangrado8']=$item->sangrado;
            $data['equimosis8']=$item->equimosis;
            $data['petequia8']=$item->petequia;
            $data['astenia8']=$item->astenia;
        } 
        // Musculo esqueletico
        $gets9musculo_esqueletico=$this->General_model->getselectwhereall('s9musculo_esqueletico',$arrayinfoc);
        $data['idmusculo9']=0;
        $data['mialgias9']='';
        $data['dolor_oseo9']='';
        $data['artralgias9']='';
        $data['alteraciones9']='';
        $data['disminucion_volumen9']='';
        $data['limitacion_movimiento9']='';
        $data['deformacion9']='';
        foreach ($gets9musculo_esqueletico as $item){
            $data['idmusculo9']=$item->idmusculo;
            $data['mialgias9']=$item->mialgias;
            $data['dolor_oseo9']=$item->dolor_oseo;
            $data['artralgias9']=$item->artralgias;
            $data['alteraciones9']=$item->alteraciones;
            $data['disminucion_volumen9']=$item->disminucion_volumen;
            $data['limitacion_movimiento9']=$item->limitacion_movimiento;
            $data['deformacion9']=$item->deformacion;
        } 
        // Nervioso
        $gets10nervioso=$this->General_model->getselectwhereall('s10nervioso',$arrayinfoc);
        $data['idnervioso10']=0;
        $data['cefaleas10']='';
        $data['paresias10']='';
        $data['parestesias10']='';
        $data['movimientos_anormales10']='';
        $data['alteraciones_marcha10']='';
        $data['vertigo10']='';
        $data['mareos10']='';
        foreach ($gets10nervioso as $item){
            $data['idnervioso10']=$item->idnervioso;
            $data['cefaleas10']=$item->cefaleas;
            $data['paresias10']=$item->paresias;
            $data['parestesias10']=$item->parestesias;
            $data['movimientos_anormales10']=$item->movimientos_anormales;
            $data['alteraciones_marcha10']=$item->alteraciones_marcha;
            $data['vertigo10']=$item->vertigo;
            $data['mareos10']=$item->mareos;
        } 
        // Organos de los sentidos
        $gets11organos_sentidos=$this->General_model->getselectwhereall('s11organos_sentidos',$arrayinfoc);
        $data['idorganos11']=0;
        $data['alteraciones_vision11']='';
        $data['audicion11']='';
        $data['olfato11']='';
        $data['gusto11']='';
        $data['tacto11']='';
        $data['mareos11']='';
        $data['sensaciones11']='';
        foreach ($gets11organos_sentidos as $item){
            $data['idorganos11']=$item->idorganos;
            $data['alteraciones_vision11']=$item->alteraciones_vision;
            $data['audicion11']=$item->audicion;
            $data['olfato11']=$item->olfato;
            $data['gusto11']=$item->gusto;
            $data['tacto11']=$item->tacto;
            $data['mareos11']=$item->mareos;
            $data['sensaciones11']=$item->sensaciones;
        } 

        // Esfera psiquica
        $gets12esfera_psiquica=$this->General_model->getselectwhereall('s12esfera_psiquica',$arrayinfoc);
        $data['idesfera12']=0;
        $data['tristeza12']='';
        $data['euforia12']='';
        $data['alteraciones_sueno12']='';
        $data['terrores_nocturnos12']='';
        $data['ideaciones12']='';
        $data['miedo_exagerado12']='';
        $data['irritabilidad12']='';
        $data['apatia12']='';
        $data['relaciones_personales12']='';
        foreach ($gets12esfera_psiquica as $item){
            $data['idesfera12']=$item->idesfera;
            $data['tristeza12']=$item->tristeza;
            $data['euforia12']=$item->euforia;
            $data['alteraciones_sueno12']=$item->alteraciones_sueno;
            $data['terrores_nocturnos12']=$item->terrores_nocturnos;
            $data['ideaciones12']=$item->ideaciones;
            $data['miedo_exagerado12']=$item->miedo_exagerado;
            $data['irritabilidad12']=$item->irritabilidad;
            $data['apatia12']=$item->apatia;
            $data['relaciones_personales12']=$item->relaciones_personales;
        } 
        // Tegumentario
        $gets13tegumentario=$this->General_model->getselectwhereall('s13tegumentario',$arrayinfoc);
        $data['idtegumentario13']=0;
        $data['coloracion_anormal13']='';
        $data['pigmentacion13']='';
        $data['prurito13']='';
        $data['caracteristicas13']='';
        $data['unas13']='';
        $data['hiperhidrosis13']='';
        $data['xerodermia13']='';
        foreach ($gets13tegumentario as $item){
            $data['idtegumentario13']=$item->idtegumentario;
            $data['coloracion_anormal13']=$item->coloracion_anormal;
            $data['pigmentacion13']=$item->pigmentacion;
            $data['prurito13']=$item->prurito;
            $data['caracteristicas13']=$item->caracteristicas;
            $data['unas13']=$item->unas;
            $data['hiperhidrosis13']=$item->hiperhidrosis;
            $data['xerodermia13']=$item->xerodermia;
        } 
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
		$this->load->view('paciente/paciente',$data);
        $this->load->view('templates/footer');
        $this->load->view('paciente/pacientejs');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }
    }


    public function detallepaciente(){
        $id=$this->input->post('id');
        $arrayinfo = array('idpaciente'=>$id);
        $list_paciente=$this->General_model->getselectwhereall('pacientes',$arrayinfo);
        $foto_p='';
        $nombre_p='';
        $apll_paterno_p='';
        $apll_materno_p='';
        $fechan='';
        $tiempo='';
        $ahora='';
        $edad='';
        $edad='';
        $correo_p='';
        $ocupacion_p='';
        $celular_p='';
        $nota_p='';
        $doc='';
        $doc_firma='';
        $fecha_doc='';
        $check_acepta='';
        foreach ($list_paciente as $item) {
            $foto_p=$item->foto;
            $nombre_p=$item->nombre;
            $apll_paterno_p=$item->apll_paterno;
            $apll_materno_p=$item->apll_materno;
            $fechan=$item->fecha_nacimiento;
            $tiempo = strtotime($item->fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
            $edad = floor($edad); 
            $correo_p=$item->correo;
            $ocupacion_p=$item->ocupacion;
            $celular_p=$item->celular;
            $nota_p=$item->nota;
            $doc=$item->doc;
            $doc_firma=$item->doc_firma;
            $fecha_doc=$item->fecha_doc;
            $check_acepta=$item->check_acepta;
        }
        $html='<ul class="nav nav-tabs customtab2" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#dato1" role="tab"><span class="hidden-xs-down">Datos generales del paciente</span></a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#dato2" role="tab"> <span class="hidden-xs-down">Datos de facturación</span></a> </li>';
                    /*
                    $html.='<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#dato3" onclick="list_acompanante('.$id.')" role="tab"> <span class="hidden-xs-down">Acompañantes / Familiares</span></a> </li>';
                    */
                    $html.='<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#dato4" role="tab"> <span class="hidden-xs-down">  Documentos Legales</span></a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="dato1" role="tabpanel">
                        <div class="row">
                            <div class="col-md-2"><br>
                                <div align="center">';
                                if($foto_p!=''){
                            $html.='<img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="'.base_url().'uploads/pacientes/'.$foto_p.'">';
                                }else{    
                            $html.='<img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="'.base_url().'images/annon.png">';
                                }
                            $html.='<span class="foto_avatar" >
                                    <input type="file" name="foto_avatar" id="foto_avatar" style="display: none">
                                    </span>
                                    <label for="foto_avatar">
                                        <span class="btn waves-effect waves-light btn-secondary"><i class="fas fa-camera"></i> Cambiar foto</span>
                                    </label>
                                </div>';
                    $html.='</div>    
                            <div class="col-md-10"><br>
                                <div class="detalle_paciente">
                                    <h4>'.$nombre_p.' '.$apll_paterno_p.' '.$apll_materno_p.'</h4>';
                                    if($fechan!='0000-00-00'){
                            $html.='<h5>Edad: '.$edad.' años</h5>';
                                    }
                                    if($correo_p!=''){
                            $html.='<h5>Correo: '.$correo_p.'</h5><span class="correo_paciente" data-correop="'.$correo_p.'"></span>';
                                    }
                                    if($ocupacion_p!=''){
                            $html.='<h5>Ucupación: '.$ocupacion_p.'</h5>';
                                    }
                                    if($celular_p!=''){
                            $html.='<h5><i class="fas fa-mobile-alt"></i> '.$celular_p.'</h5>';
                                    }
                                    if($nota_p!=''){
                            $html.='<h5><i class="fas fa-thumbtack"></i> '.$nota_p.'</h5>';
                                    }
                        $html.='</div> 
                                <div class="detalle_paciente_datos">
                                </div>   
                           </div>   
                        </div>
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <div class="btn_datos1" style="text-align:right">
                                    <button type="button" class="btn waves-effect waves-light btn-outline-info" onclick="infopaciente('.$id.')">Editar paciente</button>
                                    <!--<button type="button" class="btn waves-effect waves-light btn-outline-info" onclick="modal_nuevo_pago('.$id.')">Registrar Pago</button>--> ';
                                    /*
                                    <button type="button" class="btn waves-effect waves-light btn-outline-info" onclick="modal_reenviarformulario('.$id.')">Reenviar formulario</button>
                                    */

                                    $html.='<button type="button" class="btn waves-effect waves-light btn-outline-info" onclick="href_agenda('.$id.')">Agendar</button>
                                </div>    
                            </div>
                        </div>    
                    </div>
                    <div class="tab-pane  p-20" id="dato2" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12">
                                <p>Datos de facturación para el paciente</p>
                            </div>
                        </div>
                        <div class="row" style="border-style: solid; border-width: 1px; border-radius: 10px;">';
                        $razon_social = '';
                        $rfc = '';
                        $correo = '';
                        $calle = '';
                        $num_ext = '';
                        $num_int = '';
                        $colonia = '';
                        $ciudad = '';
                        $estado = '';
                        $codigopostal = '';
                        $arrayinfo = array('idpaciente'=>$id);
                        $list_paciente=$this->General_model->getselectwhereall('paciente_facturacion',$arrayinfo);
                        foreach ($list_paciente as $item) {
                            $idfactura=$item->idfactura;
                            $razon_social =$item->razon_social;
                            $rfc =$item->rfc;
                            $correo =$item->correo;
                            $calle =$item->calle;
                            $num_ext =$item->num_ext;
                            $num_int =$item->num_int;
                            $colonia =$item->colonia;
                            $ciudad =$item->ciudad;
                            $estado =$item->estado;
                            $codigopostal =$item->codigopostal;
                        }
                    $html.='<div class="col-md-12 datos_factura">
                                <br>
                                <p>Razón social: '.$razon_social.'</p>
                                <p>RFC: '.$rfc.'</p>
                                <p>Correo Electrónico: '.$correo.'</p>
                                <hr>
                                <p><span class="dfp"><i class="fas fa-caret-right"></i></span> <u><a style="cursor: pointer;" onclick="direccion_fiscal_paciente()"> Ver dirección</a></u></p>
                                <div class="dfp_texto" style="display:none;">
                                    <div style="background-color: #eae1e1;"><br>
                                        <div class="row">
                                            <div class="col-md-3">
                                               <p>&nbsp; Calle: '.$calle.'</p>
                                               <p>&nbsp; Ciudad: '.$ciudad.'</p>
                                            </div>
                                            <div class="col-md-3">
                                               <p>Número Exterior: '.$num_ext.'</p>
                                               <p>Estado: '.$estado.'</p>
                                            </div>
                                            <div class="col-md-3">
                                               <p>Número Interior: '.$num_int.'</p>
                                               <p>Código Postal: '.$codigopostal.'</p>
                                            </div>
                                            <div class="col-md-3">
                                               <p>Colonia:'.$colonia.'</p>
                                            </div>
                                        </div>
                                    </div> 
                                    <br>
                                </div>
                                <p><button type="button" class="btn waves-effect waves-light btn-outline-info" onclick="btn_edit_dfc('.$id.')">Editar datos fiscales del paciente</button></p>
                            </div> 
                        </div>
                    </div>
                    <div class="tab-pane p-20" id="dato3" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 align="center">Acompañantes / Familiares</h5><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="acompanante_list">
                                        </div>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="add_acompaniante">
                                        </div>
                                    </div> 
                                </div>
                                <p class="btn_agregar_familiar" align="center"><button type="button" class="btn waves-effect waves-light btn-info" onclick="agregar_acompaniante('.$id.')">Agregar acompañante / Familiar</button></p>
                            </div> 
                        </div>
                    </div>
                    <div class="tab-pane  p-20" id="dato4" role="tabpanel">
                        <div class="row" align="center">
                            <div class="col-md-12">
                                <h4>Documentos Legales</h4>
                            </div>
                        </div>'; 
                    //if($doc=='' && $doc_firma==''){
                $html.='<div class="row">
                            <div class="col-md-12">
                                <p class="centrado"><i class="fas fa-exclamation-circle" style="color:red"></i> En este apartado te sugerimos anexar el aviso de privacidad firmado por tu paciente, dándote el beneficio de tener un resguardo y así llevar un mejor control.</p> 
                            </div>
                        </div>    
                        <div class="row">
                            <div class="col-md-12">
                                <div class="recuadro" style="border-left: 5px groove red;">
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <select class="form-control btn_sistema" id="tipo_doc_legal" onchange="select_doc_legal('.$id.')">
                                                <option selected="selected" disabled="" value="0">Seleccione tipo de documento</option>
                                                <option value="1">Inyección de ácido hialurónico</option>
                                                <option value="2">Enzimas Plus</option>
                                                <option value="3">Toxina botulinica</option>
                                                <option value="4">Bichectomia</option>
                                                <option value="5">Luz pulsada</option>
                                                <option value="6">Radiofrecuencia</option>
                                                <option value="7">Hidrolipoclasia</option>
                                            </select>  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="text_doc_legal"></div>
                                        </div>
                                    </div>    
                                </div>
                            </div> 
                        </div>';

                    $result_legal=$this->General_model->get_records_condition('activo=1 AND idpaciente='.$id,'documentos_legales');
                    $aux_doc=0;
                    foreach ($result_legal as $item){ 
                        $aux_doc=1;
                    }   
                    if($aux_doc==1){
                        $che_util=''; 
                        if($check_acepta==1){
                            $che_util='checked'; 
                        }else{
                            $che_util=''; 
                        }  
                $html.='<div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive" style="border-radius: 11px;">
                                    <table class="table color-bordered-table muted-bordered-table">
                                        <thead>
                                            <tr>
                                                <th style="width:400px">Fecha de anexo del A.P</th>
                                                <th>Tipo de documento</th>
                                                <th style="text-align: right;"></th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                                        $result_legal=$this->General_model->get_records_condition('activo=1 AND idpaciente='.$id,'documentos_legales');
                                        foreach ($result_legal as $item){
                                            $txt_doc='';
                                            if($item->tipo==1){
                                                $txt_doc='Inyección de ácido hialurónico';
                                            }else if($item->tipo==2){
                                                $txt_doc='Enzimas Plus';
                                            }else if($item->tipo==3){
                                                $txt_doc='Toxina botulinica';
                                            }else if($item->tipo==4){
                                                $txt_doc='Bichectomia';
                                            }else if($item->tipo==5){
                                                $txt_doc='Luz pulsada';
                                            }else if($item->tipo==6){
                                                $txt_doc='Radiofrecuencia';
                                            }else if($item->tipo==7){
                                                $txt_doc='Hidrolipoclasia';
                                            }   
                                    $html.='<tr>
                                                <td>'.date('d/m/Y',strtotime($item->fecha)).'</td>
                                                <td>'.$txt_doc.'</td>
                                                <td style="text-align: right;">
                                                    <button type="button" class="btn waves-effect waves-light btn-outline-secondary" onclick="modal_eliminar_doc_legal('.$item->iddocumento.')">Eliminar</button>';
                                                if($item->firma!=''){
                                            $html.='<a class="btn waves-effect waves-light btn-outline-info" onclick="imprimir_aviso_privacidad('.$id.','.$item->tipo.','.$item->iddocumento.')"><i class="ti-printer"></i></a></td>';
                                                }else{
                                            $html.='<a class="btn waves-effect waves-light btn-outline-info" target="_blank" href="'.base_url().'uploads/documentos_legal/'.$item->nombre.'"><i class="ti-printer"></i></a></td>'; 
                                                } 
                                            $html.='</tr>';
                                        }
                                    $html.='<tr style="text-align:right">
                                                <td colspan="3"> 
                                                    <div class="custom-control custom-checkbox mr-sm-2 mb-3">
                                                        <input type="checkbox" class="custom-control-input" id="check_acepta" '.$che_util.' onclick="acepta_utilicen_datos('.$id.')">
                                                        <label class="custom-control-label" for="check_acepta">El paciente acepta que se utilicen sus datos para los fines secundarios.</label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>';
                    }
                    /*
                $html.='<br>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 align="center">Otros Documentos</h4>';  
                                //////////
                            if($doc!='' || $doc_firma!=''){
                        $html.='<div class="table-responsive">
                                    <table class="table color-bordered-table muted-bordered-table">
                                        <thead>
                                            <tr>
                                                <th style="width:400px">Documento</th>
                                                <th>Fecha de Aceptación</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Aviso de Privacidad</td>
                                                <td>'.date('d/m/Y',strtotime($fecha_doc)).'</td>
                                                <td></td>
                                            </tr> 
                                        </tbody>
                                    </table>
                                </div>';
                                ////////
                            }
                    $html.='</div>
                        </div>
                    </div>
                    */
                $html.='</div>';
        echo $html;
        
    }
    function get_tipo_documento(){
        $id=$this->input->post('id');
        $tipo=$this->input->post('tipo');
        $html='<hr>';
        $html_t='';
        if($tipo==1){
            $html_t='<h4>Inyección de ácido hialurónico</h4>';
        }else if($tipo==2){
            $html_t='<h4>Enzimas Plus</h4>';
        }else if($tipo==3){
            $html_t='<h4>Toxina botulinica</h4>';
        }else if($tipo==4){
            $html_t='<h4>Bichectomia</h4>';
        }else if($tipo==5){
            $html_t='<h4>Luz pulsada</h4>';
        }else if($tipo==6){
            $html_t='<h4>Radiofrecuencia</h4>';
        }else if($tipo==7){
            $html_t='<h4>Hidrolipoclasia</h4>';
        }
        $html.=$html_t;
        $html.='<button type="button" class="btn waves-effect waves-light btn-outline-info" onclick="imprimir_aviso_privacidad('.$id.','.$tipo.',0)"><i class="fas fa-print"></i> Imprimir aviso de privacidad en blanco</button>
            <button type="button" class="btn waves-effect waves-light btn-outline-info" onclick="firmar_aviso_privacidad('.$id.','.$tipo.')">Firmar aviso de privacidad digital</button>
            <button type="button" class="btn waves-effect waves-light btn-outline-info" onclick="qr_firmar_aviso('.$id.','.$tipo.')"> Código de acceso rápido</button>
            <span class="firma_aviso_text" style="display:none;">
                <div class="row">
                    <div class="col-md-3"><br>
                        <input type="hidden" id="firma_id_text" value="0">
                        <div id="qrcode"></div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-8"><br>
                        <p>Utiliza un lector de QR en un dispositivo móvil (como una tablet) para que tu paciente firme el aviso de privacidad de forma digital.</p>
                    </div>
                </div>         
            </span>
            <hr>
            <p align="center">
            <div class="row">
                <div class="col-md-12" align="center">
                    <input type="hidden" id="tipo_doc_legal_imp" value="'.$tipo.'">
                    <input type="file" id="doc_legal" style="display: none">
                    </span>
                    <label for="doc_legal">
                        <span class="btn waves-effect waves-light btn-danger"><i class="fas fa-upload"></i> Anexar aviso de privacidad firmado</span>
                    </label>
                </div>
            </div>    
            </p>';
        echo $html;  
        $this->load->view('paciente/qrjs');  
    }

    function cargafiles(){
        $id=$this->input->post('id');
        $folder="pacientes";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
          $array = array('foto'=>$newfile);
          $this->General_model->edit_record('idpaciente',$id,$array,'pacientes');
          $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    } 
    public function detallepaciente_info(){
        $id=$this->input->post('id');
        $list_paises=$this->General_model->get_table('paises');
        $get_info=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $html='<p>Datos generales</p>';
        $html.='<form class="form" method="post" role="form" id="form_paciente">
                    <input type="hidden" name="idpaciente" value="'.$get_info->idpaciente.'"> 
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" name="nombre" value="'.$get_info->nombre.'" class="form-control">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Apellido Paterno</label>
                                <input type="text" name="apll_paterno" value="'.$get_info->apll_paterno.'" class="form-control">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Apellido Materno</label>
                                <input type="text" name="apll_materno" value="'.$get_info->apll_materno.'" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Fecha de nacimiento</label>
                                <input type="date" name="fecha_nacimiento" value="'.$get_info->fecha_nacimiento.'" class="form-control">
                            </div>
                        </div>';
                        $aux_sexo1='';
                        $aux_sexo2='';
                        if($get_info->sexo==1){
                            $aux_sexo1='selected'; 
                        }else if($get_info->sexo==2){
                            $aux_sexo2='selected';
                        } 
                $html.='<div class="col-md-4">
                            <div class="form-group">
                                <label> Sexo</label>
                                <select name="sexo" class="form-control">
                                    <option disabled="" selected="" value="0">Seleccione</option>
                                    <option value="1" '.$aux_sexo1.'>Masculino</option>
                                    <option value="2" '.$aux_sexo2.'>Femenino</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Correo Electronico</label>
                                <input type="email" name="correo" value="'.$get_info->correo.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-8"></div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Correo Electronico 2</label>
                                <input type="email" name="correo2" value="'.$get_info->correo2.'" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Teléfono Celular</label>
                                <input type="text" name="celular" value="'.$get_info->celular.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Teléfono de Casa</label>
                                <input type="text" name="casa" value="'.$get_info->casa.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Teléfono de Oficina</label>
                                <input type="text" name="oficina" value="'.$get_info->oficina.'" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Dirección</label>
                                <input type="text" name="direccion" value="'.$get_info->direccion.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>País</label>
                                <select name="pais" class="form-control">
                                    <option disabled="" selected="" value="0">Selecciona un país</option>';
                                foreach ($list_paises as $item){
                                    if($item->idpaises==$get_info->pais){
                                    $html.='<option value="'.$item->idpaises.'" selected>'.$item->pais.'</option>';
                                    }else{
                                    $html.='<option value="'.$item->idpaises.'">'.$item->pais.'</option>';
                                    }
                                }
                        $html.='</select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Ocupación</label>
                                <input type="text" name="ocupacion" value="'.$get_info->ocupacion.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Estado civil</label>';
                        $aux_estado_civil1='';
                        $aux_estado_civil2='';
                        $aux_estado_civil3='';
                        $aux_estado_civil4='';
                        $aux_estado_civil5='';
                        if($get_info->estado_civil==1){
                            $aux_estado_civil1='selected'; 
                        }else if($get_info->estado_civil==2){
                            $aux_estado_civil2='selected';
                        }else if($get_info->estado_civil==3){
                            $aux_estado_civil3='selected';
                        }else if($get_info->estado_civil==4){
                            $aux_estado_civil4='selected';
                        }else if($get_info->estado_civil==5){
                            $aux_estado_civil5='selected';
                        } 
                        $html.='<select name="estado_civil" class="form-control">
                                    <option disabled="" selected="" value="0">Seleccione</option>
                                    <option value="1" '.$aux_estado_civil1.'>Soltero (a)</option>
                                    <option value="2" '.$aux_estado_civil2.'>Casado (a)</option>
                                    <option value="3" '.$aux_estado_civil3.'>Divorciado (a)</option>
                                    <option value="4" '.$aux_estado_civil4.'>Viudo (a)</option>
                                    <option value="5" '.$aux_estado_civil5.'>Unión Libre</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Notas</label>
                                <input type="text" name="nota" value="'.$get_info->nota.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>No. Expediente</label>
                                <input type="text" name="num_expediente" value="'.$get_info->num_expediente.'" class="form-control">
                            </div>
                        </div>
                    </div>
                </form>';
        $html.='<div align="center">
                    <button type="button" class="btn waves-effect waves-light btn-info" onclick="guarda_paciente()">Guardar</button>
                </div>';
        echo $html;
        $this->load->view('paciente/qrjs');
    } 
    public function registraPaciente(){
        $data=$this->input->post();
        $id=$data['idpaciente'];
        unset($data['idpaciente']);
        $this->General_model->edit_record('idpaciente',$id,$data,'pacientes');
        echo $id;
    }
    public function registraPago(){
        $data=$this->input->post();
        if(isset($data['factura'])){
            $data['factura']=1;
        }else{
            $data['factura']=0; 
        }
        $data['personalId']=$this->idpersonal;
        $id=$this->General_model->add_record('pagos_paciente',$data);
        echo $id;
    }
    public function registraPagodetalle(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $data['idpagos_paciente']=$DATA[$i]->idpagos_paciente;
            $data['tipo_servicio']=$DATA[$i]->tipo_servicio;
            $data['monto']=$DATA[$i]->monto;
            $data['notas']=$DATA[$i]->notas;  
            $this->General_model->add_record('pagos_paciente_detalle',$data);
        }
    }
    function enviar(){
        $idp=$this->input->post('id');
        $correo=$this->input->post('correo');
        $get_info=$this->General_model->get_record('idpaciente',$idp,'pacientes');
        //================================
            //cargamos la libreria email
            $this->load->library('email');
            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mocha3014.mochahost.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'contacto@estudiosunne.sicoi.net';

            //Nuestra contraseña
            $config["smtp_pass"] = '_@fzi[%~5Kx)';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('contacto@estudiosunne.sicoi.net','Cita Médica Unne');
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
        /*
        $cita = $this->ModeloCatalogos->getCita($id);      
        foreach ($cita as $item) {
              $fecha = date('d-m-Y',strtotime($item->fecha));
              $hora = date('g:i A',strtotime($item->hora));  
              $paciente = $item->paciente; 
              $correo = $item->correo; 
              $tipo_paciente = $item->tipo_paciente;
              $medico_externo = $item->medico; 
              $estudio = $item->estudio;
              $area = $item->area;
              $color = $item->color;
              $lugar = $item->unidades;
              $observaciones = $item->observaciones;
              $requisitos = $item->requsitos;
        }
        */
        $nombre_completo=$get_info->nombre.' '.$get_info->apll_paterno.' '.$get_info->apll_materno;
        $perosnaldatos=$this->General_model->get_record('personalId',$this->idpersonal,'personal');
        $this->email->to($correo,$nombre_completo);
        
        $asunto='Información necesaria para su próxima cita médica '.$perosnaldatos->nombre;

      //Definimos el asunto del mensaje
        $this->email->subject($asunto);
         
      //Definimos el mensaje a enviar
      //$this->email->message($body)

        $message  = "<div style='background: url(http://estudiosunne.sicoi.net/public/img/anahuac-fondo-footer.jpg) #003166;width: 97%;height: 94%;padding: 20px;margin: 0px;'>
                      <div style='background: white; border: 1px solid #F8F8F8; border-radius: 15px; padding: 25px; width: 80%; margin-left: auto; margin-right: auto; position: absolute; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                      -moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                      box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);'>
                        <table>
                           <thead>
                            
                            <tr>
                              <th colspan='2' align='center'>
                                <div style='font-family: sans-serif; font-size:15px; color: #003166; -webkit-print-color-adjust: exact;'><h3>UNNE</h3></div> 
                              </th>
                            </tr>
                            <tr>
                              <th colspan='5' style='background-color: #00a5e1; color: #00a5e1; -webkit-print-color-adjust: exact;'>..</th>
                            </tr>
                            <tr>
                              <th>
                                <br>
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:13px;' align='left'>
                                Buen día, ".$get_info->nombre."
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:13px;' align='left'>
                                Es un gusto saber que pronto estará en consulta con nosotros. Antes de acudir al consultorio, le invitamos a completar la siguiente información que nos será de gran utilidad para otorgarle una mejor atención.
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:13px; color: #003166; -webkit-print-color-adjust: exact;' align='left'>
                                <div align='center'>
                                <a href='#' style='font-family: sans-serif; font-size:15px; color: #003166; -webkit-print-color-adjust: exact; background-color: #00a5e1; color: white; padding: 15px 32px; text-align: center;  text-decoration: none; display: inline-block; font-size: 16px; border-radius: 10px'>completar su cita aquí</a>
                                </div>
                              </th>
                            </tr>
                        </table><br>         
                      </div>
                  </div>";
                    

        $this->email->message($message);

        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
        //==================
    }
    function datos_factura_text(){
        $id=$this->input->post('id');
        $idfactura = 0;
        $razon_social = '';
        $rfc = '';
        $correo = '';
        $calle = '';
        $num_ext = '';
        $num_int = '';
        $colonia = '';
        $ciudad = '';
        $estado = '';
        $codigopostal = '';
        $arrayinfo = array('idpaciente'=>$id);
        $list_paciente=$this->General_model->getselectwhereall('paciente_facturacion',$arrayinfo);
        foreach ($list_paciente as $item) {
            $idfactura=$item->idfactura;
            $razon_social =$item->razon_social;
            $rfc =$item->rfc;
            $correo =$item->correo;
            $calle =$item->calle;
            $num_ext =$item->num_ext;
            $num_int =$item->num_int;
            $colonia =$item->colonia;
            $ciudad =$item->ciudad;
            $estado =$item->estado;
            $codigopostal =$item->codigopostal;
        }
        $html='<form method="post" role="form" id="form_paciente_factura"> 
                    <br>
                    <input type="hidden" name="idfactura" value="'.$idfactura.'">
                    <input type="hidden" name="idpaciente" value="'.$id.'">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Razón social</label>
                                <input type="text" name="razon_social" value="'.$razon_social.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>RFC</label>
                                <input type="text" name="rfc" value="'.$rfc.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Correo Electrónico</label>
                                <input type="text" name="correo" value="'.$correo.'" class="form-control">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Calle</label>
                                <input type="text" name="calle" value="'.$calle.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Número Exterior</label>
                                <input type="text" name="num_ext" value="'.$num_ext.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Número Interior</label>
                                <input type="text" name="num_int" value="'.$num_int.'" class="form-control">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Colonia</label>
                                <input type="text" name="colonia" value="'.$colonia.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Ciudad</label>
                                <input type="text" name="ciudad" value="'.$ciudad.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Estado</label>
                                <input type="text" name="estado" value="'.$estado.'" class="form-control">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Código Postal</label>
                                <input type="text" name="codigopostal" value="'.$codigopostal.'" class="form-control">
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12" align="center">
                        <button type="button" class="btn waves-effect waves-light btn-info" onclick="guarda_factura_paciente()">Guardar</button><br><br>
                    </div>    
                </div>';
        echo $html; 
    }
    public function registra_factura_paciente(){
        $data=$this->input->post();
        $id=$data['idfactura'];
        unset($data['idfactura']);
        if($id==0){
            $this->General_model->add_record('paciente_facturacion',$data);
        }else{
            $this->General_model->edit_record('idfactura',$id,$data,'paciente_facturacion');
        }
        echo $id;
    }
    function agregar_familiares(){
        $id=$this->input->post('id');
        $html='<div class="row">
                    <div class="col-md-2"><br>
                        <div align="center">
                            <img id="img_agre_familiar" style="width: 145px; height: 145px; border-radius: 70px;" src="'.base_url().'images/annon.png"
                            <span class="foto_avatar" >
                            <input type="file" name="foto_avatar_familiar" id="foto_avatar_familiar" style="display: none">
                            </span>
                            <label for="foto_avatar_familiar">
                                <span class="btn waves-effect waves-light btn-secondary"><i class="fas fa-camera"></i> Cambiar foto</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-10"><br>
                        <h6>Datos del acompañante</h6>
                        <form method="post" role="form" id="form_paciente_acompanante"> 
                            <br>
                            <input type="hidden" name="idacompanante" value="0">
                            <input type="hidden" name="idpaciente" id="idpaciente_n" value="'.$id.'">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" name="nombre" class="form-control" placeholder="Nombre(s)">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" name="apll_paterno" class="form-control" placeholder="Apellido Paterno">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" name="apll_materno" class="form-control" placeholder="Apellido Materno">
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select name="relacion" class="form-control">
                                                <option value="1">Madre</option>
                                                <option value="2">Padre</option>
                                                <option value="3">Abuelo</option>
                                                <option value="4">Abuela</option>
                                                <option value="5">Hermano</option>
                                                <option value="6">Hijo</option>
                                                <option value="7">Hija</option>
                                                <option value="8">Esposa</option>
                                                <option value="9">Esposo</option>
                                                <option value="10">Amigo(a)</option>
                                                <option value="11">Compañero(a)</option>
                                                <option value="12">Otro</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" name="correo" class="form-control" placeholder="Correo Electrónico">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="date" name="fecha_nacimiento" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" name="celular" class="form-control" placeholder="Celular (10 dígitos)">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" name="casa" class="form-control" placeholder="Casa (10 dígitos)">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" name="oficina" class="form-control" placeholder="Oficina (10 dígitos)">
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" name="direccion" class="form-control" placeholder="Dirección">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select name="estado_civil" class="form-control">
                                            <option value="1">Soltero (a)</option>
                                            <option value="2">Casado (a)</option>
                                            <option value="3">Divorciado (a)</option>
                                            <option value="4">Viudo (a)</option>
                                            <option value="5">Unión Libre</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" name="nota" class="form-control" placeholder="Notas">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" name="ocupacion" class="form-control" placeholder="Ocupación">
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-md-12">
                                <div align="center">
                                    <button type="button" class="btn waves-effect waves-light btn-info" onclick="guarda_acompanante()">Guardar</button>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>';
        echo $html;
    }
    public function registra_acompanante(){
        $data=$this->input->post();
        $id=$data['idacompanante'];
        unset($data['idacompanante']);
        if($id==0){
            $id=$this->General_model->add_record('paciente_acompanante',$data);
        }else{
            $this->General_model->edit_record('idacompanante',$id,$data,'paciente_acompanante');
        }
        echo $id;
    }
    function cargafiles_acompanante(){
        $id=$this->input->post('id');
        $folder="acompanante";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
          $array = array('foto'=>$newfile);
          $this->General_model->edit_record('idacompanante',$id,$array,'paciente_acompanante');
          $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }
    function listado_acompanante(){
        $id=$this->input->post('id');
        $arrayinfo = array('idpaciente'=>$id,'activo'=>1);
        $html='';
        $list_acompanante=$this->General_model->getselectwhereall('paciente_acompanante',$arrayinfo);
        $relaciontxt='';
        foreach ($list_acompanante as $item){
            if($item->relacion==1){$relaciontxt='Madre';}
            else if($item->relacion==2){$relaciontxt='Padre';}
            else if($item->relacion==3){$relaciontxt='Abuelo';}
            else if($item->relacion==4){$relaciontxt='Abuela';}
            else if($item->relacion==5){$relaciontxt='Hermano';}
            else if($item->relacion==6){$relaciontxt='Hijo';}
            else if($item->relacion==7){$relaciontxt='Hija';}
            else if($item->relacion==8){$relaciontxt='Esposa';}
            else if($item->relacion==9){$relaciontxt='Esposo';}
            else if($item->relacion==10){$relaciontxt='Amigo(a)';}
            else if($item->relacion==11){$relaciontxt='Compañero(a)';}
            else if($item->relacion==12){$relaciontxt='Otro';}
            $tiempo = strtotime($item->fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
            $edad = floor($edad); 
        $html.='<div class="row" style="border-style: solid; border-width: 1px; border-radius: 10px;">
                    <div class="col-md-2"><br>
                        <div align="center">';
                        if($item->foto!=''){
                    $html.='<img id="img_avat_'.$item->idacompanante.'" style="width: 145px; height: 145px; border-radius: 70px;" src="'.base_url().'uploads/acompanante/'.$item->foto.'">';
                        }else{    
                    $html.='<img id="img_avat_'.$item->idacompanante.'" style="width: 145px; height: 145px; border-radius: 70px;" src="'.base_url().'images/annon.png">';
                        }
                    $html.='<span class="foto_avat_'.$item->idacompanante.'" >
                            <input type="file" id="foto_avat_'.$item->idacompanante.'" style="display: none">
                            </span>
                            <label for="foto_avat_'.$item->idacompanante.'"">
                                <span class="btn waves-effect waves-light btn-secondary"><i class="fas fa-camera"></i> Cambiar foto </span>
                            </label>
                        </div>';
            $html.='</div> 
                    <div class="col-md-10">
                        <div class="texto_acompanate_'.$item->idacompanante.'">
                            <div class="row">
                                <div class="col-md-9"><br>
                                    <h5>Acompañante</h5> 
                                    <h6><span class="label label-info m-r-10">'.$relaciontxt.'</span> <strong>'.$item->nombre.' '.$item->apll_paterno.' '.$item->apll_materno.'</strong></h6>';
                                if($item->fecha_nacimiento!='0000-00-00'){
                                    $html.='<h6>Edad: '.$edad.' años</h6>';
                                }
                                if($item->celular!=''){
                                    $html.='<h6><i class="fas fa-mobile-alt "></i> '.$item->celular.'</h6>';
                                }
                                if($item->direccion!=''){
                                    $html.='<h6><i class="fas fa-map-marker-alt"></i> Dirección: '.$item->direccion.'</h6>';
                                }   
                                if($item->correo!=''){
                                    $html.='<h6><i class="fas fa-envelope"></i> '.$item->correo.'</h6>';
                                }
                            
                        $html.='</div>    
                                <div class="col-md-3"><br><br><br><br><br>
                                    <button type="button" class="btn waves-effect waves-light btn-outline-danger" onclick="eliminar_acompanante('.$item->idacompanante.','.$id.')"><i class="fas fa-trash-alt"></i> Borrar</button>
                                    <button type="button" class="btn waves-effect waves-light btn-outline-success" onclick="edit_acompanante('.$item->idacompanante.','.$id.')">Editar</button>
                                </div> 
                            </div>
                        </div>
                    </div>    
                </div>';
        } 
        $arrayinfo = array('info'=>$html,'list_acompa'=>$list_acompanante);
        echo json_encode($arrayinfo);
    }
    public function alimanaracompanante(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idacompanante',$id,$data,'paciente_acompanante');
    }
    public function alimanarpaciente(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idpaciente',$id,$data,'pacientes');
    }
    public function texto_acompanate_e(){
        $ida=$this->input->post('ida');
        $idp=$this->input->post('idp');
        $ac=$this->General_model->get_record('idacompanante',$ida,'paciente_acompanante');
        $stt1='';$stt2='';$stt3='';$stt4='';$stt5='';$stt6='';$stt7='';$stt8='';$stt9='';$stt10='';$stt11='';$stt12='';
        if($ac->relacion==1){$stt1='selected';}
        else if($ac->relacion==2){$stt2='selected';}
        else if($ac->relacion==3){$stt3='selected';}
        else if($ac->relacion==4){$stt4='selected';}
        else if($ac->relacion==5){$stt5='selected';}
        else if($ac->relacion==6){$stt6='selected';}
        else if($ac->relacion==7){$stt7='selected';}
        else if($ac->relacion==8){$stt8='selected';}
        else if($ac->relacion==9){$stt9='selected';}
        else if($ac->relacion==10){$stt1='selected';}
        else if($ac->relacion==11){$stt11='selected';}
        else if($ac->relacion==12){$stt12='selected';}
        $html='<h6>Datos del acompañante</h6>
                <form method="post" role="form" id="form_paciente_acompanante_edit"> 
                    <br>
                    <input type="hidden" name="idacompanante" value="'.$ac->idacompanante.'">
                    <input type="hidden" id="idpaciente_edit" value="'.$idp.'">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="nombre" class="form-control" placeholder="Nombre(s)" value="'.$ac->nombre.'">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="apll_paterno" class="form-control" placeholder="Apellido Paterno" value="'.$ac->apll_paterno.'">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="apll_materno" class="form-control" placeholder="Apellido Materno" value="'.$ac->apll_materno.'">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <select name="relacion" class="form-control">
                                    <option value="1" '.$stt1.'>Madre</option>
                                    <option value="2" '.$stt2.'>Padre</option>
                                    <option value="3" '.$stt3.'>Abuelo</option>
                                    <option value="4" '.$stt4.'>Abuela</option>
                                    <option value="5" '.$stt5.'>Hermano</option>
                                    <option value="6" '.$stt6.'>Hijo</option>
                                    <option value="7" '.$stt7.'>Hija</option>
                                    <option value="8" '.$stt8.'>Esposa</option>
                                    <option value="9" '.$stt9.'>Esposo</option>
                                    <option value="10" '.$stt10.'>Amigo(a)</option>
                                    <option value="11" '.$stt11.'>Compañero(a)</option>
                                    <option value="12" '.$stt12.'>Otro</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="correo" class="form-control" placeholder="Correo Electrónico" value="'.$ac->correo.'">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="date" name="fecha_nacimiento" class="form-control" value="'.$ac->fecha_nacimiento.'">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="celular" class="form-control" placeholder="Celular (10 dígitos)" value="'.$ac->celular.'">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="casa" class="form-control" placeholder="Casa (10 dígitos)" value="'.$ac->casa.'">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="oficina" class="form-control" placeholder="Oficina (10 dígitos)" value="'.$ac->oficina.'">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" name="direccion" class="form-control" placeholder="Dirección" value="'.$ac->direccion.'">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select name="estado_civil" class="form-control">
                                    <option value="1">Soltero (a)</option>
                                    <option value="2">Casado (a)</option>
                                    <option value="3">Divorciado (a)</option>
                                    <option value="4">Viudo (a)</option>
                                    <option value="5">Unión Libre</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" name="nota" class="form-control" placeholder="Notas" value="'.$ac->nota.'">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="ocupacion" class="form-control" placeholder="Ocupación" value="'.$ac->ocupacion.'">
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <div align="center">
                            <button type="button" class="btn waves-effect waves-light btn-info" onclick="guarda_acompanante_edit()">Guardar</button>
                        </div>
                    </div>    
                </div>
          ';
        echo $html;        
    }
    public function aviso($id){
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $this->load->view('paciente/aviso',$data);
        $this->load->view('paciente/print_formato');
    }
    public function aviso_inyeccion($id,$idd){
        $data['tipo']=0;
        $data['tipo_aviso']=1;
        $data['iddocumento']=$idd;
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $this->load->view('paciente/aviso_inyeccion',$data);
        $this->load->view('paciente/print_formato');
    }
    public function aviso_enzimas($id,$idd){
        $data['tipo']=0;
        $data['tipo_aviso']=2;
        $data['iddocumento']=$idd;
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $this->load->view('paciente/aviso_enzimas',$data);
        $this->load->view('paciente/print_formato');
    }
    public function aviso_toxina($id,$idd){
        $data['tipo']=0;
        $data['tipo_aviso']=3;
        $data['iddocumento']=$idd;
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $this->load->view('paciente/aviso_toxina',$data);
        $this->load->view('paciente/print_formato');
    }
    public function aviso_bichectomia($id,$idd){
        $data['tipo']=0;
        $data['tipo_aviso']=4;
        $data['iddocumento']=$idd;
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $this->load->view('paciente/aviso_bichectomia',$data);
        $this->load->view('paciente/print_formato');
    }
    public function aviso_luz_pulsada($id,$idd){
        $data['tipo']=0;
        $data['tipo_aviso']=5;
        $data['iddocumento']=$idd;
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $this->load->view('paciente/aviso_luz_pulsada',$data);
        $this->load->view('paciente/print_formato');
    }
    public function aviso_radio_frecuencia($id,$idd){
        $data['tipo']=0;
        $data['tipo_aviso']=6;
        $data['iddocumento']=$idd;
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $this->load->view('paciente/aviso_radio_frecuencia',$data);
        $this->load->view('paciente/print_formato');
    }
    public function aviso_hidrolipoclasia($id,$idd){
        $data['tipo']=0;
        $data['tipo_aviso']=7;
        $data['iddocumento']=$idd;
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $this->load->view('paciente/aviso_hidrolipoclasia',$data);
        $this->load->view('paciente/print_formato');
    }
    public function firma_inyeccion($id){
        $data['tipo']=1;
        $data['tipo_aviso']=1;
        $data['iddocumento']=0;
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $data['personal']=$this->General_model->get_record('personalId',$this->idpersonal,'personal');
        $data['fecha']=$this->fechainicio;
        $this->load->view('paciente/aviso_inyeccion',$data);
        $this->load->view('paciente/jscanvas_firma');
    }
    public function firma_enzimas($id){
        $data['tipo']=1;
        $data['tipo_aviso']=2;
        $data['iddocumento']=0;
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $data['personal']=$this->General_model->get_record('personalId',$this->idpersonal,'personal');
        $data['fecha']=$this->fechainicio;
        $this->load->view('paciente/aviso_enzimas',$data);
        $this->load->view('paciente/jscanvas_firma');
    }
    public function firma_toxina($id){
        $data['tipo']=1;
        $data['tipo_aviso']=3;
        $data['iddocumento']=0;
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $data['personal']=$this->General_model->get_record('personalId',$this->idpersonal,'personal');
        $data['fecha']=$this->fechainicio;
        $this->load->view('paciente/aviso_toxina',$data);
        $this->load->view('paciente/jscanvas_firma_doc');
    }
    public function firma_bichectomia($id){
        $data['tipo']=1;
        $data['tipo_aviso']=4;
        $data['iddocumento']=0;
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $data['personal']=$this->General_model->get_record('personalId',$this->idpersonal,'personal');
        $data['fecha']=$this->fechainicio;
        $this->load->view('paciente/aviso_bichectomia',$data);
        $this->load->view('paciente/jscanvas_firma');
    }
    public function firma_luz_pulsada($id){
        $data['tipo']=1;
        $data['tipo_aviso']=5;
        $data['iddocumento']=0;
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $data['personal']=$this->General_model->get_record('personalId',$this->idpersonal,'personal');
        $data['fecha']=$this->fechainicio;
        $this->load->view('paciente/aviso_luz_pulsada',$data);
        $this->load->view('paciente/jscanvas_firma');
    }
    public function firma_radio_frecuencia($id){
        $data['tipo']=1;
        $data['tipo_aviso']=6;
        $data['iddocumento']=0;
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $data['personal']=$this->General_model->get_record('personalId',$this->idpersonal,'personal');
        $data['fecha']=$this->fechainicio;
        $this->load->view('paciente/aviso_radio_frecuencia',$data);
        $this->load->view('paciente/jscanvas_firma_doc');
    }
    public function firma_hidrolipoclasia($id){
        $data['tipo']=1;
        $data['tipo_aviso']=7;
        $data['iddocumento']=0;
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $data['personal']=$this->General_model->get_record('personalId',$this->idpersonal,'personal');
        $data['fecha']=$this->fechainicio;
        $this->load->view('paciente/aviso_hidrolipoclasia',$data);
        $this->load->view('paciente/jscanvas_firma_doc');
    }

    public function registra_inf_relevante(){
        $data=$this->input->post();
        $id=$data['idrelevante'];
        unset($data['idrelevante']);
        if(isset($data['negadas'])){
            $data['negadas']=1;
        }else{
            $data['negadas']=0; 
        }
        if($id==0){
            $id=$this->General_model->add_record('relevante_historia',$data);
        }else{
            $this->General_model->edit_record('idrelevante',$id,$data,'relevante_historia');
        }
        echo $id;
    }
    public function texto_historia_clinica(){
        $id=$this->input->post('idp');
        $arrayinfo = array('idpaciente'=>$id);
        
        $idclinica=0;
        $chediabetes=0;
        $chediabetess='display:none';
        $diabetes='';
        $checancer=0;
        $checancers='display:none';
        $cancer='';
        $chehipertencion=0;
        $chehipertencions='display:none';
        $hipertencion='';
        $cheotro_antecedente=0;
        $cheotro_antecedentes='display:none';
        $otro_antecedente='';
        $cheingesta=0;
        $cheingestas='display:none';
        $ingesta='';
        $checirugia=0;
        $checirugias='display:none';
        $cirugia='';
        $otras_enfemedades='';
        $chetransfusiones=0;
        $chetransfusioness='display:none';
        $transfusiones='';
        $chehepatitis=0;
        $chehepatitiss='display:none';
        $chehepatitisa=0;
        $chehepatitisb=0;
        $chehepatitisc=0;
        $hepatitis='';
        $chediabetesmillitus=0;
        $chediabetesmillituss='display:none';
        $diabetesmillitus='';
        $chehipertensionarterial=0;
        $chehipertensionarterials='display:none';
        $hipertensionarterial='';
        $chetabaquismo=0;
        $chetabaquismos='display:none';
        $tabaquismo='';
        $chetomabebidas=0;
        $chetomabebidass='display:none';
        $veces_semana='';
        $copasdia='';
        $bebidas_alcoholicas='';
        $checonsumodrogas=0;
        $checonsumodrogass='display:none';
        $consumodrogas='';
        $cherealizaejercicio=0;
        $cherealizaejercicios='display:none';
        $realizaejercicio='';
        $interrogatorio_aparatos='';
        //===================================
        $chetpersonal='';
        $chetpersonals='display:none';
        $chetpersonal_servicios='';
        $chetalimentacion='';
        $chetalimentacions='display:none';
        $chetfruta='';
        $chetfrutas='display:none';
        $fruta='';

        $chetcarne='';
        $chetcarnes='display:none';
        $carnes_rojas='';

        $chetcerdo='';
        $chetcerdos='display:none';
        $cerdos='';
        
        $chetpollo='';
        $chetpollos='display:none';
        $pollo='';

        $chetlacteo='';
        $chetlacteos='display:none';
        $lacteos='';

        $chetharina='';
        $chetharinas='display:none';
        $harina='';

        $chetagua='';
        $chetaguas='display:none';
        $aguas='';

        $chetchatarra='';
        $chetchatarras='display:none';
        $chatarras='';
        
        $chetalcohol='';
        $chetalcoholes='display:none';
        $cuando_inicio_alcohol='';
        $cada_cuando_alcohol='';
        $cuantos_dia_alcohol='';
        
        $chettabaco='';
        $chettabacos='display:none';
        $cuando_inicio_tabaco='';
        $cada_cuando_tabaco='';
        $cuantos_dia_tabaco='';
        
        $chetdroga='';
        $chetdrogas='display:none';
        
        $marcar='';
        $mestrual='';
        $dismenorrea='';
        $menstruacion='';
        $sexual='';
        $parejas='';
        $embarazos='';
        $abortos='';
        $cesareas='';
        $lactancia='';
        $anticonceptivos='';
        $ets='';
        $menopausia='';
        $climaterio='';
        $papanicolau='';
        $mastografia='';

        $chetmedicamento='';
        $chetmedicamentos='display:none';
        $medicamentos_deprecion='';

        $chethospitalizacion='';
        $chethospitalizaciones='display:none';
        $hospitalizacion='';

        $chetalegia='';
        $chetalegias='display:none';
        $alergias='';

        $chetfractura='';
        $chetfracturas='display:none';
        $fracturas='';

        $chetlactancia='';
        $chetlactancias='display:none';
        $embarazo_lactancia='';

        $hipercolesterolemia='';
        $epilepsia='display:none';
        $enfermedades_musculares='';
        
        $cuando_inicio_drogas='';
        $cuando_inicio_drogas='';
        $cada_cuando_drogas='';
        $cuantos_dia_drogas='';  
        $inmunizaciones='';
        $desparasitacion='';
        $medicamenton='';
        $get_historia_clinica=$this->General_model->getselectwhereall('historia_clinica',$arrayinfo);
        foreach ($get_historia_clinica as $item){
            $idclinica=$item->idclinica;
            $idpaciente=$item->idpaciente;
            if($item->chediabetes==1){
                $chediabetes='checked';
                $chediabetess='display:block';
            }else{
                $chediabetes='';
                $chediabetess='display:none';
            }
            $diabetes=$item->diabetes;
            if($item->checancer==1){
                $checancer='checked';
                $checancers='display:block';
            }else{
                $checancer='';
                $checancers='display:none';
            }
            $cancer=$item->cancer;
            if($item->chehipertencion==1){
                $chehipertencion='checked';
                $chehipertencions='display:block';
            }else{
                $chehipertencion='';
                $chehipertencions='display:none';
            }
            $hipertencion=$item->hipertencion;
            if($item->cheotro_antecedente==1){
                $cheotro_antecedente='checked';
                $cheotro_antecedentes='display:block';
            }else{
                $cheotro_antecedente='';
                $cheotro_antecedentes='display:none';
            }
            $otro_antecedente=$item->otro_antecedente;
            if($item->cheingesta==1){
                $cheingesta='checked';
                $cheingestas='display:block';
            }else{
                $cheingesta='';
                $cheingestas='display:none';
            }
            $ingesta=$item->ingesta;
            if($item->checirugia==1){
                $checirugia='checked';
                $checirugias='display:block';
            }else{
                $checirugia='';
                $checirugias='display:none';
            }
            $cirugia=$item->cirugia;
            $otras_enfemedades=$item->otras_enfemedades;
            if($item->chetransfusiones==1){
                $chetransfusiones='checked';
                $chetransfusioness='display:block';
            }else{
                $chetransfusiones='';
                $chetransfusioness='display:none';
            }
            $transfusiones=$item->transfusiones;
            if($item->chehepatitis==1){
                $chehepatitis='checked';
                $chehepatitiss='display:block';
            }else{
                $chehepatitis='';
                $chehepatitiss='display:none';
            }
            if($item->chehepatitisa==1){
                $chehepatitisa='checked';
            }else{
                $chehepatitisa='';
            }
            if($item->chehepatitisb==1){
                $chehepatitisb='checked';
            }else{
                $chehepatitisb='';
            }
            if($item->chehepatitisc==1){
                $chehepatitisc='checked';
            }else{
                $chehepatitisc='';
            }
            $hepatitis=$item->hepatitis;
            if($item->chediabetesmillitus==1){
                $chediabetesmillitus='checked';
                $chediabetesmillituss='display:block';
            }else{
                $chediabetesmillitus='';
                $chediabetesmillituss='display:none';
            }
            $diabetesmillitus=$item->diabetesmillitus;
            if($item->chehipertensionarterial==1){
                $chehipertensionarterial='checked';
                $chehipertensionarterials='display:block';
            }else{
                $chehipertensionarterial='';
                $chehipertensionarterials='display:none';
            }
            $hipertensionarterial=$item->hipertensionarterial;
            if($item->chetabaquismo==1){
                $chetabaquismo='checked';
                $chetabaquismos='display:block';
            }else{
                $chetabaquismo='';
                $chetabaquismos='display:none';
            }
            $tabaquismo=$item->tabaquismo;
            if($item->chetomabebidas==1){
                $chetomabebidas='checked';
                $chetomabebidass='display:block';
            }else{
                $chetomabebidas='';
                $chetomabebidass='display:none';
            }
            $veces_semana=$item->veces_semana;
            $copasdia=$item->copasdia;
            $bebidas_alcoholicas=$item->bebidas_alcoholicas;
            if($item->checonsumodrogas==1){
                $checonsumodrogas='checked';
                $checonsumodrogass='display:block';
            }else{
                $checonsumodrogas='';
                $checonsumodrogass='display:none';
            }
            $consumodrogas=$item->consumodrogas;
            if($item->cherealizaejercicio==1){
                $cherealizaejercicio='checked';
                $cherealizaejercicios='display:block';
            }else{
                $cherealizaejercicio='';
                $cherealizaejercicios='display:none';
            }
            $realizaejercicio=$item->realizaejercicio;
            $interrogatorio_aparatos=$item->interrogatorio_aparatos;
            //======================================================
            if($item->chetpersonal==1){
                $chetpersonal='checked';
                $chetpersonals='display:block';
            }else{
                $chetpersonal='';
                $chetpersonals='display:none';
            }
            if($item->chetpersonal_servicios==1){
                $chetpersonal_servicios='checked';
            }else{
                $chetpersonal_servicios='';
            }
            if($item->chetalimentacion==1){
                $chetalimentacion='checked';
                $chetalimentacions='display:block';
            }else{
                $chetalimentacion='';
                $chetalimentacions='display:none';
            }
            if($item->chetfruta==1){
                $chetfruta='checked';
                $chetfrutas='display:block';
            }else{
                $chetfruta='';
                $chetfrutas='display:none';
            }
            $fruta=$item->fruta;

            if($item->chetcarne==1){
                $chetcarne='checked';
                $chetcarnes='display:block';
            }else{
                $chetcarne='';
                $chetcarnes='display:none';
            }
            $carnes_rojas=$item->carnes_rojas;
            if($item->chetcerdo==1){
                $chetcerdo='checked';
                $chetcerdos='display:block';
            }else{
                $chetcerdo='';
                $chetcerdos='display:none';
            }
            $cerdos=$item->cerdos;
            
            if($item->chetpollo==1){
                $chetpollo='checked';
                $chetpollos='display:block';
            }else{
                $chetcerdo='';
                $chetpollos='display:none';
            }
            $pollo=$item->pollo;
            
            if($item->chetlacteo==1){
                $chetlacteo='checked';
                $chetlacteos='display:block';
            }else{
                $chetlacteo='';
                $chetlacteos='display:none';
            }
            $lacteos=$item->lacteos;
            
            if($item->chetharina==1){
                $chetharina='checked';
                $chetharinas='display:block';
            }else{
                $chetharina='';
                $chetharinas='display:none';
            }
            $harina=$item->harina;

            if($item->chetagua==1){
                $chetagua='checked';
                $chetaguas='display:block';
            }else{
                $chetagua='';
                $chetaguas='display:none';
            }
            $aguas=$item->aguas;

            if($item->chetchatarra==1){
                $chetchatarra='checked';
                $chetchatarras='display:block';
            }else{
                $chetchatarra='';
                $chetchatarras='display:none';
            }
            $chatarras=$item->chatarras;
            //
            if($item->chetchatarra==1){
                $chetchatarra='checked';
                $chetchatarras='display:block';
            }else{
                $chetchatarra='';
                $chetchatarras='display:none';
            }
            if($item->chetalcohol==1){
                $chetalcohol='checked';
                $chetalcoholes='display:block';
            }else{
                $chetalcohol='';
                $chetalcoholes='display:none';
            }
            $cuando_inicio_alcohol=$item->cuando_inicio_alcohol;
            $cada_cuando_alcohol=$item->cada_cuando_alcohol;
            $cuantos_dia_alcohol=$item->cuantos_dia_alcohol;
            
            if($item->chettabaco==1){
                $chettabaco='checked';
                $chettabacos='display:block';
            }else{
                $chettabaco='';
                $chettabacos='display:none';
            }
            $cuando_inicio_tabaco=$item->cuando_inicio_tabaco;
            $cada_cuando_tabaco=$item->cada_cuando_tabaco;
            $cuantos_dia_tabaco=$item->cuantos_dia_tabaco;  
            
            if($item->chetdroga==1){
                $chetdroga='checked';
                $chetdrogas='display:block';
            }else{
                $chetdroga='';
                $chetdrogas='display:none';
            } 

            $cuando_inicio_drogas=$item->cuando_inicio_drogas;
            $cada_cuando_drogas=$item->cada_cuando_drogas;
            $cuantos_dia_drogas=$item->cuantos_dia_drogas;  
            $inmunizaciones=$item->inmunizaciones;
            $desparasitacion=$item->desparasitacion;

            $marcar=$item->marcar;
            $mestrual=$item->mestrual;
            $dismenorrea=$item->dismenorrea;
            $menstruacion=$item->menstruacion;
            $sexual=$item->sexual;
            $parejas=$item->parejas;
            $embarazos=$item->embarazos;
            $abortos=$item->abortos;
            $cesareas=$item->cesareas;
            $lactancia=$item->lactancia;
            $anticonceptivos=$item->anticonceptivos;
            $ets=$item->ets;
            $menopausia=$item->menopausia;
            $climaterio=$item->climaterio;
            $papanicolau=$item->papanicolau;
            $mastografia=$item->mastografia;
            ///
            if($item->chetmedicamento==1){
                $chetmedicamento='checked';
                $chetmedicamentos='display:block';
            }else{
                $chetmedicamento='';
                $chetmedicamentos='display:none';
            }
            $medicamentos_deprecion=$item->medicamentos_deprecion;

            if($item->chethospitalizacion==1){
                $chethospitalizacion='checked';
                $chethospitalizaciones='display:block';
            }else{
                $chethospitalizacion='';
                $chethospitalizaciones='display:none';
            }
            $hospitalizacion=$item->hospitalizacion;
            
            if($item->chetalegia==1){
                $chetalegia='checked';
                $chetalegias='display:block';
            }else{
                $chetalegia='';
                $chetalegias='display:none';
            }
            $alergias=$item->alergias;

            if($item->chetfractura==1){
                $chetfractura='checked';
                $chetfracturas='display:block';
            }else{
                $chetfractura='';
                $chetfracturas='display:none';
            }
            $fracturas=$item->fracturas;

            if($item->chetlactancia==1){
                $chetlactancia='checked';
                $chetlactancias='display:block';
            }else{
                $chetlactancia='';
                $chetlactancias='display:none';
            }
            $embarazo_lactancia=$item->embarazo_lactancia;
            ///
            if($item->hipercolesterolemia==1){
                $hipercolesterolemia='checked';
            }else{
                $hipercolesterolemia='';
            }
            if($item->epilepsia==1){
                $epilepsia='checked';
            }else{
                $epilepsia='';
            }
            if($item->enfermedades_musculares==1){
                $enfermedades_musculares='checked';
            }else{
                $enfermedades_musculares='';
            }
            $medicamenton=$item->medicamento;
            
        }
        $html1='<div class="margentexto">
                    <input type="hidden" name="idclinica" id="idclinica_e" class="form-control colorlabel" value="'.$idclinica.'">
                    <input type="hidden" name="idpaciente" class="form-control colorlabel" value="'.$id.'">
                    <ul>
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chediabetes" id="chediabetes" '.$chediabetes.'>
                            <label class="custom-control-label" for="chediabetes"><strong>Diabetes mellitus</strong></label>
                        </div>
                        <div class="t_in_1" style="'.$chediabetess.'">
                            <div class="form-group input-group mb-3 recordableHolder">
                                <input type="text" name="diabetes" class="form-control colorlabel recordable rinited" value="'.$diabetes.'">
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div>
                        </div>
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="checancer" id="checancer" '.$checancer.'>
                            <label class="custom-control-label" for="checancer"><strong>Cáncer</strong></label>
                        </div>
                        <div class="t_in_2" style="'.$checancers.'">
                            <div class="form-group input-group mb-3 recordableHolder">
                                <input type="text" name="cancer" class="form-control colorlabel recordable rinited" value="'.$cancer.'">
                            <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div>    
                        </div>
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chehipertencion" id="chehipertencion" '.$chehipertencion.'>
                            <label class="custom-control-label" for="chehipertencion"><strong>Hipertensión arterial</strong></label>
                        </div>
                        <div class="t_in_3" style="'.$chehipertencions.'">
                            <div class="form-group input-group mb-3 recordableHolder">
                                <input type="text" name="hipertencion" class="form-control colorlabel recordable rinited" value="'.$hipertencion.'">
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div> 
                        </div>
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="cheotro_antecedente" id="cheotro_antecedente" '.$cheotro_antecedente.'>
                            <label class="custom-control-label" for="cheotro_antecedente"><strong>Otro antecedente importante</strong></label>
                        </div>
                        <div class="t_in_4" style="'.$cheotro_antecedentes.'">
                            <div class="form-group input-group mb-3 recordableHolder">
                                <input type="text" name="otro_antecedente" class="form-control colorlabel recordable rinited" value="'.$otro_antecedente.'">
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div>   
                        </div>
                    </ul>
                </div>';
        $html2='<div class="margentexto">
                    <ul>
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chetmedicamento" id="chetmedicamento" '.$chetmedicamento.'>
                            <label class="custom-control-label" for="chetmedicamento"><strong>¿Tomas medicamentos contra ansiedad depresión o dormir? / ¿Cúal? Dosis</strong></label>
                        </div>
                        <div class="t_ap_18s" style="'.$chetmedicamentos.'">
                            <div class="form-group input-group mb-3 recordableHolder">
                                <input type="text" name="medicamentos_deprecion" class="form-control colorlabel recordable rinited" value="'.$medicamentos_deprecion.'">
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div>
                        </div>
                        
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="checirugia" id="checirugia" '.$checirugia.'>
                            <label class="custom-control-label" for="checirugia"><strong>Cirugías:</strong></label>
                        </div>
                        <div class="t_pe_2" style="'.$checirugias.'">
                            <div class="form-group input-group mb-3 recordableHolder">
                                <input type="text" name="cirugia" class="form-control colorlabel recordable rinited" value="'.$cirugia.'">
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div>
                        </div>
                        <label><strong>Otras enfermedades importantes</strong></label>
                        <div class="form-group input-group mb-3 recordableHolder">
                            <input type="text" name="otras_enfemedades" class="form-control colorlabel recordable rinited" value="'.$otras_enfemedades.'">
                            <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                        </div>
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chetransfusiones" id="chetransfusiones" '.$chetransfusiones.'>
                            <label class="custom-control-label" for="chetransfusiones"><strong>Transfusiones</strong></label>
                        </div>  
                        <div class="t_pe_3" style="'.$chetransfusioness.'">
                            <div class="form-group input-group mb-3 recordableHolder">
                                <input type="text" name="transfusiones" class="form-control colorlabel recordable rinited" value="'.$transfusiones.'"> 
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div> 
                        </div>
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chethospitalizacion" id="chethospitalizacion" '.$chethospitalizacion.'>
                            <label class="custom-control-label" for="chethospitalizacion"><strong>Hospitalizaciones</strong></label>
                        </div>  
                        <div class="t_pe_7" style="'.$chethospitalizaciones.'">
                            <div class="form-group input-group mb-3 recordableHolder">
                                <input type="text" name="hospitalizacion" class="form-control colorlabel recordable rinited" value="'.$hospitalizacion.'"> 
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div> 
                        </div>
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chetalegia" id="chetalegia" '.$chetalegia.'>
                            <label class="custom-control-label" for="chetalegia"><strong>Alergias(gravedad)</strong></label>
                        </div>  
                        <div class="t_pe_19s" style="'.$chetalegias.'">
                            <div class="form-group input-group mb-3 recordableHolder">
                                <input type="text" name="alergias" class="form-control colorlabel recordable rinited" value="'.$alergias.'"> 
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div> 
                        </div>
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chetfractura" id="chetfractura" '.$chetfractura.'>
                            <label class="custom-control-label" for="chetfractura"><strong>Fracturas o lesiones</strong></label>
                        </div>  
                        <div class="t_pe_20s" style="'.$chetfracturas.'">
                            <div class="form-group input-group mb-3 recordableHolder">
                                <input type="text" name="fracturas" class="form-control colorlabel recordable rinited" value="'.$fracturas.'"> 
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div> 
                        </div>
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chetlactancia" id="chetlactancia" '.$chetlactancia.'>
                            <label class="custom-control-label" for="chetlactancia"><strong>¿Podrías estar embarazada o en lactancia? / Fecha de última regla </strong></label>
                        </div>  
                        <div class="t_pe_21s" style="'.$chetlactancias.'">
                            <div class="form-group input-group mb-3 recordableHolder">
                                <input type="text" name="embarazo_lactancia" class="form-control colorlabel recordable rinited" value="'.$embarazo_lactancia.'"> 
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div> 
                        </div>
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chehepatitis" id="chehepatitis" '.$chehepatitis.'>
                            <label class="custom-control-label" for="chehepatitis"><strong>Hepatitis</strong></label>
                        </div>
                        <div class="t_pe_4" style="'.$chehepatitiss.'">
                            <ul>  
                                <div class="custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="chehepatitisa" id="chehepatitisa" '.$chehepatitisa.'>
                                    <label class="custom-control-label" for="chehepatitisa"><strong>Hepatitis A</strong></label>
                                </div>
                                <div class="custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="chehepatitisb" id="chehepatitisb" '.$chehepatitisb.'>
                                    <label class="custom-control-label" for="chehepatitisb"><strong>Hepatitis B</strong></label>
                                </div>
                                <div class="custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="chehepatitisc" id="chehepatitisc" '.$chehepatitisc.'>
                                    <label class="custom-control-label" for="chehepatitisc"><strong>Hepatitis C</strong></label>
                                </div>
                                <div class="form-group input-group mb-3 recordableHolder">
                                    <input type="text" name="hepatitis" class="form-control colorlabel recordable rinited" value="'.$hepatitis.'">
                                    <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                </div>
                            </ul>
                        </div>
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chediabetesmillitus" id="chediabetesmillitus" '.$chediabetesmillitus.'>
                            <label class="custom-control-label" for="chediabetesmillitus"><strong>Diabetes mellitus:</strong></label>
                        </div>
                        <div class="t_pe_5" style="'.$chediabetesmillituss.'">
                            <div class="form-group input-group mb-3 recordableHolder">    
                                <input type="text" name="diabetesmillitus" class="form-control colorlabel recordable rinited" value="'.$diabetesmillitus.'"> 
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div> 
                        </div>
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chehipertensionarterial" id="chehipertensionarterial" '.$chehipertensionarterial.'>
                            <label class="custom-control-label" for="chehipertensionarterial"><strong>Hipertensión arterial</strong></label>
                        </div>
                        <div class="t_pe_6" style="'.$chehipertensionarterials.'">
                            <div class="form-group input-group mb-3 recordableHolder">
                                <input type="text" name="hipertensionarterial" class="form-control colorlabel recordable rinited" value="'.$hipertensionarterial.'">
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div>
                        </div>  
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="hipercolesterolemia" id="hipercolesterolemia" '.$hipercolesterolemia.'>
                            <label class="custom-control-label" for="hipercolesterolemia"><strong>Hipercolesterolemia</strong></label>
                        </div> 
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="epilepsia" id="epilepsia" '.$epilepsia.'>
                            <label class="custom-control-label" for="epilepsia"><strong>Epilepsia</strong></label>
                        </div> 
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="enfermedades_musculares" id="enfermedades_musculares" '.$enfermedades_musculares.'>
                            <label class="custom-control-label" for="enfermedades_musculares"><strong>Enfermedades musculares</strong></label>
                        </div> 
                    </ul>
                </div>'; 
        $html3='<div class="margentexto">
                    <ul>';
                    //=============================================================================================
                        $html3.='<div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chetpersonal" id="chetpersonal" '.$chetpersonal.'>
                            <label class="custom-control-label" for="chetpersonal"><strong>Personal</strong></label>
                        </div>
                        <div class="t_ap_5s" style="'.$chetpersonals.'">
                            <div class="form-group input-group mb-3 recordableHolder">   
                                <strong>¿Cuenta con todos los servicios?</strong>
                                <div class="custom-control custom-switch">&nbsp;&nbsp;
                                  <input type="checkbox" class="custom-control-input" name="chetpersonal_servicios" id="chetpersonal_servicios" '.$chetpersonal_servicios.'>
                                  <label class="custom-control-label" for="chetpersonal_servicios"></label>
                                </div>
                            </div>
                        </div>'; 

                        $html3.='<div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chetalimentacion" id="chetalimentacion" '.$chetalimentacion.'>
                            <label class="custom-control-label" for="chetalimentacion"><strong>Alimentación</strong></label>
                        </div>';
                        $html3.='<div class="t_ap_6s" style="'.$chetalimentacions.'">
                            <ul>';
                            $html3.='<div class="custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="chetfruta" id="chetfruta" '.$chetfruta.'>
                                    <label class="custom-control-label" for="chetfruta"><strong>Frutas y vegetales</strong></label>
                                </div>
                                <div class="t_ap_7s" style="'.$chetfrutas.'">
                                    <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="fruta" class="form-control colorlabel recordable rinited" value="'.$fruta.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                                </div>';
                                $html3.='<div class="custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="chetcarne" id="chetcarne" '.$chetcarne.'>
                                    <label class="custom-control-label" for="chetcarne"><strong>Carnes rojas</strong></label>
                                </div>
                                <div class="t_ap_8s" style="'.$chetcarnes.'">
                                    <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="carnes_rojas" class="form-control colorlabel recordable rinited" value="'.$carnes_rojas.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                                </div>'; 
                                $html3.='<div class="custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="chetcerdo" id="chetcerdo" '.$chetcerdo.'>
                                    <label class="custom-control-label" for="chetcerdo"><strong>Cerdo</strong></label>
                                </div>
                                <div class="t_ap_9s" style="'.$chetcerdos.'">
                                    <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="cerdos" class="form-control colorlabel recordable rinited" value="'.$cerdos.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                                </div>'; 
                                
                                $html3.='<div class="custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="chetpollo" id="chetpollo" '.$chetpollo.'>
                                        <label class="custom-control-label" for="chetpollo"><strong>Pollo / Pescado</strong></label>
                                    </div>
                                <div class="t_ap_10s" style="'.$chetpollos.'">
                                    <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="pollo" class="form-control colorlabel recordable rinited" value="'.$pollo.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                                </div>'; 
                                $html3.='<div class="custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="chetlacteo" id="chetlacteo" '.$chetlacteo.'>
                                    <label class="custom-control-label" for="chetlacteo"><strong>Lacteos</strong></label>
                                </div>
                                <div class="t_ap_11s" style="'.$chetlacteos.'">
                                    <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="lacteos" class="form-control colorlabel recordable rinited" value="'.$lacteos.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                                </div>'; 
                                $html3.='<div class="custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="chetharina" id="chetharina" '.$chetharina.'>
                                    <label class="custom-control-label" for="chetharina"><strong>Harinas refinadas</strong></label>
                                </div>
                                <div class="t_ap_12s" style="'.$chetharinas.'">
                                    <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="harina" class="form-control colorlabel recordable rinited" value="'.$harina.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                                </div>'; 
                                $html3.='<div class="custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="chetagua" id="chetagua" '.$chetagua.'>
                                    <label class="custom-control-label" for="chetagua"><strong>Agua</strong></label>
                                </div>
                                <div class="t_ap_13s" style="'.$chetaguas.'">
                                    <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="aguas" class="form-control colorlabel recordable rinited" value="'.$aguas.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                                </div>'; 
                                $html3.='<div class="custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="chetchatarra" id="chetchatarra" '.$chetchatarra.'>
                                    <label class="custom-control-label" for="chetchatarra"><strong>Chatarra / Gaseosa</strong></label>
                                </div>
                                <div class="t_ap_14s" style="'.$chetchatarras.'">
                                    <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="chatarras" class="form-control colorlabel recordable rinited" value="'.$chatarras.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                                </div>'; 

                            $html3.='</ul>
                        </div>'; 
                        $html3.='<div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chetalcohol" id="chetalcohol" '.$chetalcohol.'>
                            <label class="custom-control-label" for="chetalcohol"><strong>Alcohol</strong></label>
                        </div>
                        <div class="t_ap_15s" style="'.$chetalcoholes.'">
                            <div class="form-group input-group mb-3 recordableHolder">   
                                <strong>¿Cuándo inició?</strong>
                                <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="cuando_inicio_alcohol" class="form-control colorlabel recordable rinited" value="'.$cuando_inicio_alcohol.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                            </div>
                            <div class="form-group input-group mb-3 recordableHolder">   
                                <strong>¿Cada cuánto?</strong>
                                <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="cada_cuando_alcohol" class="form-control colorlabel recordable rinited" value="'.$cada_cuando_alcohol.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                            </div>
                            <div class="form-group input-group mb-3 recordableHolder">   
                                <strong> ¿Cuántos al día?</strong>
                                <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="cuantos_dia_alcohol" class="form-control colorlabel recordable rinited" value="'.$cuantos_dia_alcohol.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                            </div>
                        </div>';
                        $html3.='<div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chettabaco" id="chettabaco" '.$chettabaco.'>
                            <label class="custom-control-label" for="chettabaco"><strong>Tabaco</strong></label>
                        </div>
                        <div class="t_ap_16s" style="'.$chettabacos.'">
                            <div class="form-group input-group mb-3 recordableHolder">   
                                <strong>¿Cuándo inició?</strong>
                                <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="cuando_inicio_tabaco" class="form-control colorlabel recordable rinited" value="'.$cuando_inicio_tabaco.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                            </div>
                            <div class="form-group input-group mb-3 recordableHolder">   
                                <strong>¿Cada cuánto?</strong>
                                <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="cada_cuando_tabaco" class="form-control colorlabel recordable rinited" value="'.$cada_cuando_tabaco.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                            </div>
                            <div class="form-group input-group mb-3 recordableHolder">   
                                <strong> ¿Cuántos al día?</strong>
                                <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="cuantos_dia_tabaco" class="form-control colorlabel recordable rinited" value="'.$cuantos_dia_tabaco.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                            </div>
                        </div>';
                        $html3.='<div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chetdroga" id="chetdroga" '.$chetdroga.'>
                            <label class="custom-control-label" for="chetdroga"><strong>Drogas</strong></label>
                        </div>
                        <div class="t_ap_17s" style="'.$chetdrogas.'">
                            <div class="form-group input-group mb-3 recordableHolder">   
                                <strong>¿Cuándo inició?</strong>
                                <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="cuando_inicio_drogas" class="form-control colorlabel recordable rinited" value="'.$cuando_inicio_drogas.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" datachetdroga-label="undefined"></i></a>
                                    </div>
                            </div>
                            <div class="form-group input-group mb-3 recordableHolder">   
                                <strong>¿Cada cuánto?</strong>
                                <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="cada_cuando_drogas" class="form-control colorlabel recordable rinited" value="'.$cada_cuando_drogas.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                            </div>
                            <div class="form-group input-group mb-3 recordableHolder">   
                                <strong> ¿Cuántos al día?</strong>
                                <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="cuantos_dia_drogas" class="form-control colorlabel recordable rinited" value="'.$cuantos_dia_drogas.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                            </div>
                        </div>';
                        $html3.='<div class="form-group input-group mb-3 recordableHolder">   
                                <strong>Inmunizaciones actual y previa</strong>
                                <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="inmunizaciones" class="form-control colorlabel recordable rinited" value="'.$inmunizaciones.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                            </div>';
                        $html3.='<div class="form-group input-group mb-3 recordableHolder">   
                                <strong> Última desparasitación</strong>
                                <div class="form-group input-group mb-3 recordableHolder">
                                        <input type="text" name="desparasitacion" class="form-control colorlabel recordable rinited" value="'.$desparasitacion.'">
                                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                    </div>
                            </div>';
                        /*
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chetabaquismo" id="chetabaquismo" '.$chetabaquismo.'>
                            <label class="custom-control-label" for="chetabaquismo"><strong>Tabaquismo</strong></label>
                        </div>
                        <div class="t_ap_1" style="'.$chetabaquismos.'">
                            <div class="form-group input-group mb-3 recordableHolder">    
                                <input type="text" name="tabaquismo" class="form-control colorlabel recordable rinited" value="'.$tabaquismo.'">
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div>
                        </div>   
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="chetomabebidas" id="chetomabebidas" '.$chetomabebidas.'>
                            <label class="custom-control-label" for="chetomabebidas"><strong>Toma bebidas alcohólicas</strong></label>
                        </div>
                        <div class="t_ap_2" style="'.$chetomabebidass.'">
                            <div class="row">
                                <div class="col-md-2">
                                    <input type="text" name="veces_semana" class="form-control colorlabel" value="'.$veces_semana.'">
                                </div>
                                <div class="col-md-4">
                                    <label><strong> Veces a la semana </strong></label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" name="copasdia" class="form-control colorlabel" value="'.$copasdia.'">
                                </div>
                                <div class="col-md-3">
                                    <label><strong> Copas al día </strong></label>
                                </div>
                            </div>
                            <div class="form-group input-group mb-3 recordableHolder">
                                <input type="text" name="bebidas_alcoholicas" class="form-control colorlabel recordable rinited" value="'.$bebidas_alcoholicas.'">
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div>
                        </div> 
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="checonsumodrogas" id="checonsumodrogas" '.$checonsumodrogas.'>
                            <label class="custom-control-label" for="checonsumodrogas"><strong>Consume Drogas:</strong></label>
                        </div> 
                        <div class="t_ap_3" style="'.$checonsumodrogass.'">
                            <div class="form-group input-group mb-3 recordableHolder">
                                <input type="text" name="consumodrogas" class="form-control colorlabel recordable rinited" value="'.$consumodrogas.'">  
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div>
                        </div>
                        <div class="custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="cherealizaejercicio" id="cherealizaejercicio" '.$cherealizaejercicio.'>
                            <label class="custom-control-label" for="cherealizaejercicio"><strong>Realiza Ejercicio</strong></label>
                        </div> 
                        <div class="t_ap_4" style="'.$cherealizaejercicios.'">
                            <div class="form-group input-group mb-3 recordableHolder">
                                <input type="text" name="realizaejercicio" class="form-control colorlabel recordable rinited" value="'.$realizaejercicio.'">
                                <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            </div>
                        </div> 
                        */

                    $html3.='</ul>
                </div>';   
        $html4='<ul>
                    <div class="form-group input-group mb-3 recordableHolder">
                        <input type="text" name="interrogatorio_aparatos" class="form-control colorlabel recordable rinited" value="'.$interrogatorio_aparatos.'">
                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                    </div>
                </ul>'; 
        $html5='<div class="card-header bg-info">
                    <div class="row">
                        <div class="col-md-12" align="right">
                            <div class="editarhistoriacli_btn">
                            <button  type="button" class="btn waves-effect waves-light btn-sm btn-secondary" onclick="registra_historialclinica()">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>';

        $html6='<div class="margentexto">
                    <ul>';
                    //=============================================================================================
                        $html6.='<div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label><strong>Menarca</strong></label>
                                            <input type="text" name="marcar" id="marcar" value="'.$marcar.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><strong>Ciclo menstrual</strong></label>
                                            <input type="text" name="mestrual" id="mestrual" value="'.$mestrual.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><strong>Dismenorrea</strong></label>
                                            <input type="text" name="dismenorrea" id="dismenorrea" value="'.$dismenorrea.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label><strong>Última menstruación</strong></label>
                                            <input type="text" name="menstruacion" id="menstruacion" value="'.$menstruacion.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                 </div';
                        $html6.='<div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label><strong>Inicio de vida sexual activa</strong></label>
                                            <input type="text" name="sexual" id="sexual" value="'.$sexual.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><strong>No. parejas</strong></label>
                                            <input type="text" name="parejas" id="parejas" value="'.$parejas.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label><strong>Embarazos</strong></label>
                                            <input type="text" name="embarazos" id="embarazos" value="'.$embarazos.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label><strong>Abortos</strong></label>
                                            <input type="text" name="abortos" id="abortos" value="'.$abortos.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                </div>';
                        $html6.='<div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label><strong>Cesáreas</strong></label>
                                            <input type="text" name="cesareas" id="cesareas" value="'.$cesareas.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><strong>Lactancia</strong></label>
                                            <input type="text" name="lactancia" id="lactancia" value="'.$lactancia.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label><strong>Métodos anticonceptivos</strong></label>
                                            <input type="text" name="anticonceptivos" id="anticonceptivos" value="'.$anticonceptivos.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><strong>ETS</strong></label>
                                            <input type="text" name="ets" id="ets" value="'.$ets.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                </div>';
                        $html6.='<div class="row">            
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><strong>Menopausia</strong></label>
                                            <input type="text" name="menopausia" id="menopausia" value="'.$menopausia.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><strong>Climaterio</strong></label>
                                            <input type="text" name="climaterio" id="climaterio" value="'.$climaterio.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><strong>Papanicolau</strong></label>
                                            <input type="text" name="papanicolau" id="papanicolau" value="'.$papanicolau.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><strong>Mastografía</strong></label>
                                            <input type="text" name="mastografia" id="mastografia" value="'.$mastografia.'" class="form-control colorlabel">
                                        </div>
                                    </div>
                                </div>';
                    $html6.='</ul>
                </div>'; 
        $html6_6='<ul>
                    <div class="form-group input-group mb-3 recordableHolder">
                        <input type="text" name="medicamento" class="form-control colorlabel recordable rinited" value="'.$medicamenton.'">
                        <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                    </div>
                </ul>';                           
        $arrayhistoria = array('his1'=>$html1,'his2'=>$html2,'his3'=>$html3,'his4'=>$html4,'his5'=>$html5,'his6_5'=>$html6,'his6_6'=>$html6_6);        
        echo json_encode($arrayhistoria);
    }

    public function registra_historia_clinica(){
        $data=$this->input->post();
        $id=$data['idclinica'];
        unset($data['idclinica']);
        if(isset($data['chediabetes'])){
            $data['chediabetes']=1;
        }else{
            $data['chediabetes']=0; 
        }
        if(isset($data['checancer'])){
            $data['checancer']=1;
        }else{
            $data['checancer']=0; 
        }
        if(isset($data['chehipertencion'])){
            $data['chehipertencion']=1;
        }else{
            $data['chehipertencion']=0; 
        }
        if(isset($data['cheotro_antecedente'])){
            $data['cheotro_antecedente']=1;
        }else{
            $data['cheotro_antecedente']=0; 
        }
        if(isset($data['cheingesta'])){
            $data['cheingesta']=1;
        }else{
            $data['cheingesta']=0; 
        }
        if(isset($data['checirugia'])){
            $data['checirugia']=1;
        }else{
            $data['checirugia']=0; 
        }
        if(isset($data['chetransfusiones'])){
            $data['chetransfusiones']=1;
        }else{
            $data['chetransfusiones']=0; 
        }
        if(isset($data['chehepatitis'])){
            $data['chehepatitis']=1;
        }else{
            $data['chehepatitis']=0; 
        }
        if(isset($data['chehepatitisa'])){
            $data['chehepatitisa']=1;
        }else{
            $data['chehepatitisa']=0; 
        }
        if(isset($data['chehepatitisb'])){
            $data['chehepatitisb']=1;
        }else{
            $data['chehepatitisb']=0; 
        }
        if(isset($data['chehepatitisc'])){
            $data['chehepatitisc']=1;
        }else{
            $data['chehepatitisc']=0; 
        }
        if(isset($data['chediabetesmillitus'])){
            $data['chediabetesmillitus']=1;
        }else{
            $data['chediabetesmillitus']=0; 
        }
        if(isset($data['chehipertensionarterial'])){
            $data['chehipertensionarterial']=1;
        }else{
            $data['chehipertensionarterial']=0; 
        }
        if(isset($data['chetabaquismo'])){
            $data['chetabaquismo']=1;
        }else{
            $data['chetabaquismo']=0; 
        }
        if(isset($data['chetomabebidas'])){
            $data['chetomabebidas']=1;
        }else{
            $data['chetomabebidas']=0; 
        }
        if(isset($data['checonsumodrogas'])){
            $data['checonsumodrogas']=1;
        }else{
            $data['checonsumodrogas']=0; 
        }
        if(isset($data['cherealizaejercicio'])){
            $data['cherealizaejercicio']=1;
        }else{
            $data['cherealizaejercicio']=0; 
        }
        //=============== Agregados ================
        if(isset($data['chetpersonal'])){
            $data['chetpersonal']=1;
        }else{
            $data['chetpersonal']=0; 
        }
        if(isset($data['chetpersonal_servicios'])){
            $data['chetpersonal_servicios']=1;
        }else{
            $data['chetpersonal_servicios']=0; 
        }
        if(isset($data['chetalimentacion'])){
            $data['chetalimentacion']=1;
        }else{
            $data['chetalimentacion']=0; 
        }
        if(isset($data['chetfruta'])){
            $data['chetfruta']=1;
        }else{
            $data['chetfruta']=0; 
        }
        if(isset($data['chetcarne'])){
            $data['chetcarne']=1;
        }else{
            $data['chetcarne']=0; 
        }
        if(isset($data['chetcerdo'])){
            $data['chetcerdo']=1;
        }else{
            $data['chetcerdo']=0; 
        }
        if(isset($data['chetpollo'])){
            $data['chetpollo']=1;
        }else{
            $data['chetpollo']=0; 
        }
        if(isset($data['chetlacteo'])){
            $data['chetlacteo']=1;
        }else{
            $data['chetlacteo']=0; 
        }
        if(isset($data['chetharina'])){
            $data['chetharina']=1;
        }else{
            $data['chetharina']=0; 
        }
        if(isset($data['chetagua'])){
            $data['chetagua']=1;
        }else{
            $data['chetagua']=0; 
        }
        if(isset($data['chetchatarra'])){
            $data['chetchatarra']=1;
        }else{
            $data['chetchatarra']=0; 
        }
        if(isset($data['chetalcohol'])){
            $data['chetalcohol']=1;
        }else{
            $data['chetalcohol']=0; 
        }
        if(isset($data['chettabaco'])){
            $data['chettabaco']=1;
        }else{
            $data['chettabaco']=0; 
        }
        if(isset($data['chetdroga'])){
            $data['chetdroga']=1;
        }else{
            $data['chetdroga']=0; 
        }

        if(isset($data['chetmedicamento'])){
            $data['chetmedicamento']=1;
        }else{
            $data['chetmedicamento']=0; 
        }

        if(isset($data['chethospitalizacion'])){
            $data['chethospitalizacion']=1;
        }else{
            $data['chethospitalizacion']=0; 
        }

        if(isset($data['chetalegia'])){
            $data['chetalegia']=1;
        }else{
            $data['chetalegia']=0; 
        }

        if(isset($data['chetfractura'])){
            $data['chetfractura']=1;
        }else{
            $data['chetfractura']=0; 
        }

        if(isset($data['chetlactancia'])){
            $data['chetlactancia']=1;
        }else{
            $data['chetlactancia']=0; 
        }
        //
        if(isset($data['hipercolesterolemia'])){
            $data['hipercolesterolemia']=1;
        }else{
            $data['hipercolesterolemia']=0; 
        }
        if(isset($data['epilepsia'])){
            $data['epilepsia']=1;
        }else{
            $data['epilepsia']=0; 
        }
        if(isset($data['enfermedades_musculares'])){
            $data['enfermedades_musculares']=1;
        }else{
            $data['enfermedades_musculares']=0; 
        }

        //===============================
        if($id==0){
            $id=$this->General_model->add_record('historia_clinica',$data);
        }else{
            $this->General_model->edit_record('idclinica',$id,$data,'historia_clinica');
        }
        echo $id;
    }
    //----------------------------------------------------------------------------------------------------
    public function text_consulta(){
        $idc=$this->input->post('idc');
        $idp=$this->input->post('idp');

        $idconsulta=0;
        $fechainicio='';
        $consultafecha=$this->fechainicio;
        $nota_evaluacion='';
        $altrura='';
        $peso='';
        $ta='';
        $tempc='';
        $fc='';
        $fr='';
        $o2='';
        $exploracionfisica='';
        $resultadoestudiolaboratorio='';
        $proximaconsulta='';
        $proximafecha='';
        $proximaconsulta1='';
        $proximaconsulta2='';
        $proximaconsulta3='';
        $proximaconsulta4='';
        $proximaconsulta5='';
        $proximaconsulta6='';
        $proximaconsulta7='';
        $proximaconsulta8='';

        $arrayinfo = array('idconsulta'=>$idc);
        $get_consultas=$this->General_model->getselectwhereall('consultas',$arrayinfo);
        $aux_existe=0;
        $fecha_consulta_ultima=$this->fechainicio;
        foreach ($get_consultas as $item){
            $idconsulta=$item->idconsulta;
            $idpaciente=$item->idpaciente;
            $fechainicio=$item->fechainicio;
            $consultafecha=$item->consultafecha;
            $nota_evaluacion=$item->nota_evaluacion;
            $altrura=$item->altrura;
            $peso=$item->peso;
            $ta=$item->ta;
            $tempc=$item->tempc;
            $fc=$item->fc;
            $fr=$item->fr;
            $o2=$item->o2;
            $exploracionfisica=$item->exploracionfisica;
            $resultadoestudiolaboratorio=$item->resultadoestudiolaboratorio;
            $proximaconsulta=$item->proximaconsulta;
            $proximafecha=$item->proximafecha;
        }
        $arrayinfop = array('idpaciente'=>$idp);
        $get_consultasp_desc=$this->General_model->getselectwhere_orden_desc('consultas',$arrayinfop,'consultafecha');
        foreach ($get_consultasp_desc as $item){
            $aux_existe=1;
            $fecha_consulta_ultima=$item->consultafecha;
        }
        if($proximaconsulta==1){$proximaconsulta1='selected';}
        else if($proximaconsulta==2){$proximaconsulta2='selected';}
        else if($proximaconsulta==3){$proximaconsulta3='selected';}
        else if($proximaconsulta==4){$proximaconsulta4='selected';}
        else if($proximaconsulta==5){$proximaconsulta5='selected';}
        else if($proximaconsulta==6){$proximaconsulta6='selected';}
        else if($proximaconsulta==7){$proximaconsulta7='selected';}
        else if($proximaconsulta==8){$proximaconsulta8='selected';}
        $evaluacion_padecimiento='';
        $html='<div class="margen_todo">
                <div class="card-body" style="padding: 0.25rem !important;">
                    <div align="center">
                        <span class="badge badge-info badge-pill">Consulta</span>
                    </div>
                    <form class="form" method="post" role="form" id="form_consulta">
                        <input type="hidden" name="idconsulta" value="'.$idconsulta.'">
                        <input type="hidden" name="idpaciente" value="'.$idp.'">
                        <div class="row">
                            <div class="col-md-6">';
                            if($fecha_consulta_ultima==$consultafecha || $aux_existe==0){ 
                                $evaluacion_padecimiento='Padecimiento Actual:';
                        $html.='<div class="form-group row">
                                    <label for="example-text-input" class="col-7 col-form-label"><i class="far fa-calendar-alt"></i> Fecha de inicio del padecimiento:</label>
                                    <div class="col-5">
                                        <input type="date" name="fechainicio" value="'.$fechainicio.'" class="form-control colorlabel_white">
                                    </div>
                                </div>';
                            }else{
                                $evaluacion_padecimiento='Nota de Evolución:';
                            }
                    $html.='</div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-5 col-form-label"><i class="far fa-calendar-alt"></i> Fecha de la consulta:</label>
                                    <div class="col-6">
                                        <input type="date" name="consultafecha" id="consultafecha" value="'.$consultafecha.'" class="form-control colorlabel_white">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>'.$evaluacion_padecimiento.'</label>
                                <div class="form-group input-group mb-3 recordableHolder">
                                    <textarea type="text" name="nota_evaluacion" class="form-control colorlabel_white recordable rinited js-auto-size">'.$nota_evaluacion.'</textarea>
                                    <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <button type="button" class="btn waves-effect waves-light btn-rounded btn-info btn_style_c" onclick="ocultar_signosvitales()">Signos Vitales/Básicos &nbsp; <i class="fas fa-check-circle"></i></button>
                            </div>
                        </div>   
                        <div class="margen_div"></div>
                        <div class="txt_signosvitales">     
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label
                                            data-toggle="tooltip" data-placement="top" data-original-title="Altura"
                                        >Altura (m)</label>
                                        <input type="number" name="altrura" value="'.$altrura.'" class="form-control colorlabel_white">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label
                                            data-toggle="tooltip" data-placement="top" data-original-title="Peso"
                                        >Peso (kg)</label>
                                        <input type="number" name="peso" value="'.$peso.'" class="form-control colorlabel_white">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label
                                            data-toggle="tooltip" data-placement="top" data-original-title="Tensión Arterial"
                                        >T.A.</label>
                                        <input type="text" name="ta" value="'.$ta.'" class="form-control colorlabel_white">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label
                                            data-toggle="tooltip" data-placement="top" data-original-title="Temperatura Corporal"
                                        >Temp (°C)</label>
                                        <input type="number" name="tempc" value="'.$tempc.'" class="form-control colorlabel_white">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label original-title="Steave"
                                            data-toggle="tooltip" data-placement="top" data-original-title="Frecuencia cardiaca"
                                        >F.C.</label>
                                        <input type="text" name="fc" value="'.$fc.'" class="form-control colorlabel_white">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label
                                            data-toggle="tooltip" data-placement="top" data-original-title="Frecuencia Respiratoria"
                                        >F.R.</label>
                                        <input type="number" name="fr" value="'.$fr.'" class="form-control colorlabel_white">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label
                                            data-toggle="tooltip" data-placement="top" data-original-title="Oxigenación"
                                        >O2 (%)</label>
                                        <input type="number" name="o2" value="'.$o2.'" class="form-control colorlabel_white">
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="row">
                            <div class="col-md-12">
                                <label>Exploración Física:</label>
                                <div class="form-group input-group mb-3 recordableHolder">
                                    <textarea type="text" name="exploracionfisica" class="form-control colorlabel_white recordable rinited js-auto-size">'.$exploracionfisica.'</textarea>
                                    <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Resultados de Estudios de Laboratorio y Gabinete:</label>
                                <div class="form-group input-group mb-3 recordableHolder">
                                    <textarea type="text" name="resultadoestudiolaboratorio" class="form-control colorlabel_white recordable rinited js-auto-size">'.$resultadoestudiolaboratorio.'</textarea>
                                    <a class="rec"><i class="fas rastreable fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="margen_todo">
                                    <div class="card-body" style="padding: 0.25rem !important;">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label><i class="fas fa-user-md"></i> Diagnósticos:</label>
                                                <div class="texto_diagnostico">
                                                    <ol class="ol_diagnost_text">
                                                    </ol>
                                                </div>    
                                            </div>
                                        </div>
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="texto_recet">   
                            <div class="texto_receta">
                            </div>    
                        </div>
                        <div class="margen_div"></div>
                        <div class="row" align="right">
                            <div class="col-lg-11">
                                <button type="button" class="btn waves-effect waves-light btn-info" onclick="agregar_recet()">Nueva receta</button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="margen_todo">
                                    <div class="card-body" style="padding: 0.25rem !important;">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h6 class="m-b-0"><i class="fas fa-file-medical"></i> Órden de Estudio de Laboratorio o Estudio de Gabinete o Interconsulta</h6>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="orden_estudio_txt">
                                            <div class="orden_estudio_texto">
                                            </div>
                                        </div>    
                                        <div class="row">
                                            <div class="col-lg-8"></div>
                                            <div class="col-lg-4">
                                                <div class="select_ordenestudio">
                                                    <select id="selectorder" class="form-control" style="background-color: #03a9f3;color: #FFF;" onchange="selector_orden()">
                                                        <option selected="selected" disabled="" value="0">Agregar nueva orden</option>
                                                        <option value="1">Orden de estudios de laboratorio </option>
                                                        <option value="2">Orden de interconsulta</option>
                                                        <option value="3">Orden de hospitalización</option>
                                                        <option value="4">Certificado médico</option>
                                                        <option value="5">Justificante médico</option>
                                                        <option value="6" data-search="false">Orden libre</option>
                                                    </select>
                                                </div>    
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>     
                        <div class="margen_div"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="margen_todo">
                                    <div class="card-body margen_left" style="padding: 0.25rem !important;">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h6 class="m-b-0"><i class="fas fa-calendar-plus"></i> Próxima consulta:</h6>
                                            </div>
                                        </div>
                                        <div class="margen_div"></div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-5 col-form-label">Regresar ...</label>
                                                    <div class="col-7">
                                                        <select class="form-control" name="proximaconsulta" id="proximaconsulta">
                                                            <option value="0" disabled="" selected="">Seleccionar tiempo</option>
                                                            <option value="1" '.$proximaconsulta1.'>La próxima semana</option>
                                                            <option value="2" '.$proximaconsulta2.'>En dos semanas</option>
                                                            <option value="3" '.$proximaconsulta3.'>En tres semanas</option>
                                                            <option value="4" '.$proximaconsulta4.'>En un mes</option>
                                                            <option value="5" '.$proximaconsulta5.'>En dos meses</option>
                                                            <option value="6" '.$proximaconsulta6.'>En tres meses</option>
                                                            <option value="7" '.$proximaconsulta7.'>En seis meses</option>
                                                            <option value="8" '.$proximaconsulta8.'>En un año</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1" align="center"><p>ó</p></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="date" name="proximafecha" id="proximafecha" value="'.$proximafecha.'" class="form-control colorlabel_white">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="margen_div"></div>
                    <div class="row">
                        <div class="col-md-12" align="center">
                            <button type="button" class="btn waves-effect waves-light btn-info btn_consulta" onclick="guardar_consulta(1)">Guardar</button>
                        </div>
                    </div>
                </div>        
            </div>';
        echo $html;
    }
    public function registra_consulta(){
        $data=$this->input->post();
        $id=$data['idconsulta'];
        $idpaciente=$data['idpaciente'];
        $consultafecha=$data['consultafecha'];
        unset($data['idconsulta']);

        if($id==0){
            $data['horainicio']=$this->inicioactual;
            if($this->perfilid!=1){
                $data['tipo']=0;
            }else{
                $data['tipo']=1;
            }
            $id=$this->General_model->add_record('consultas',$data);
            $arrayconsulta_ultima = array('ultima_consulta'=>$consultafecha);
            $this->General_model->edit_record('idpaciente',$idpaciente,$arrayconsulta_ultima,'pacientes');
        }else{
            if($this->perfilid==1){
                $data['tipo']=1;
            }
            $this->General_model->edit_record('idconsulta',$id,$data,'consultas');
        }
        echo $id;
    }
    public function listado_consulta(){
        $tipo=$this->input->post('tipo');
        $idp=$this->input->post('idp');
        $html=''; 
        $arrayinfo = array('idpaciente'=>$idp,'activo'=>1);
        $list_consultas=$this->General_model->getselectwhere_orden_desc('consulta_medicina_estetica',$arrayinfo,'consultafecha');
        $list_consultas_spa=$this->General_model->getselectwhere_orden_desc('consulta_spa',$arrayinfo,'consultafecha');
        $list_consultas_nutricion=$this->General_model->getselectwhere_orden_desc('consulta_nutricion',$arrayinfo,'consultafecha');
        $arrayper = array('perfilId'=>$this->perfilid);
        $list_permiso=$this->General_model->getselectwhereall('permiso_consultas',$arrayper);
        $permiso_perfil1=0;
        $permiso_perfil2=0;
        $permiso_perfil3=0;
        foreach ($list_permiso as $item){
            $permiso_perfil1=$item->medicina_estetica;
            $permiso_perfil2=$item->spa;
            $permiso_perfil3=$item->nutricion;
        }

        if($tipo==0){
            
            $html.='<div class="row">
                        <div class="col-md-12">
                            <div class="margen_todo">
                                <div class="card-body" style="padding: 0.25rem !important;">';
                                /// Medicina estética
                                
                                if($permiso_perfil1==1){  
                                    foreach ($list_consultas as $item){
                                        $import=''; 
                                        if($item->estatus==1){
                                            $import='style="color: #346c94 !important; border-color: #346c94 !important; background-color:#fff7b4;"'; 
                                        }else{
                                            $import='style="color: #346c94 !important; border-color: #346c94 !important;"'; 
                                        }
                                        //$colorbtn=''; 
                                        $icon='<span> <i class="fas fa-syringe"></i> </span>'; 
                                        $strike1='';
                                        $strike2='';
                                        if($item->activo==0){
                                            $strike1='<strike>';
                                            $strike2='</strike>';
                                        }else{

                                        }
                                    $html.='<button type="button" class="btn btn-rounded btn-block" '.$import.' onclick="visualizar_consulta_medicina_esttetica('.$item->idconsulta.')">'.$strike1.' E '.$icon.'  '.date('d/m/Y',strtotime($item->consultafecha)).' '.$strike2.'</button>';
                                    }//<i class="mdi mdi-copyright icon_to"></i>
                                    // Consulta SPA
                                }
                                if($permiso_perfil2==1){  
                                    foreach ($list_consultas_spa as $item){
                                        $import=''; 
                                        if($item->estatus==1){
                                            $import='style="color: #346c94 !important; border-color: #346c94 !important; background-color:#fff7b4;"'; 
                                        }else{
                                            $import='style="color: #346c94 !important; border-color: #346c94 !important;"'; 
                                        }
                                        //$colorbtn=''; 
                                        $icon='<i class="fab fa-envira"></i>'; 
                                        $strike1='';
                                        $strike2='';
                                        if($item->activo==0){
                                            $strike1='<strike>';
                                            $strike2='</strike>';
                                        }else{

                                        }
                                    $html.='<button type="button" class="btn btn-rounded btn-block" '.$import.' onclick="visualizar_spa('.$item->idconsulta.')">'.$strike1.' S '.$icon.'  '.date('d/m/Y',strtotime($item->consultafecha)).' '.$strike2.'</button>';
                                    }//<i class="mdi mdi-copyright icon_to"></i>
                                }
                                //Consulta nutricinal 
                                if($permiso_perfil3==1){  
                                    foreach ($list_consultas_nutricion as $item){
                                        $import=''; 
                                        if($item->estatus==1){
                                            $import='style="color: #346c94 !important; border-color: #346c94 !important; background-color:#fff7b4;"'; 
                                        }else{
                                            $import='style="color: #346c94 !important; border-color: #346c94 !important;"'; 
                                        }
                                        //$colorbtn=''; 
                                        $icon='<i class="fab fa-apple"></i>'; 
                                        $strike1='';
                                        $strike2='';
                                        if($item->activo==0){
                                            $strike1='<strike>';
                                            $strike2='</strike>';
                                        }else{

                                        }
                                    $html.='<button type="button" class="btn btn-rounded btn-block" btn_style_c" '.$import.' onclick="visualizar_consulta_nutricion('.$item->idconsulta.')">'.$strike1.' N '.$icon.'  '.date('d/m/Y',strtotime($item->consultafecha)).' '.$strike2.'</button>';
                                    }//<i class="mdi mdi-copyright icon_to"></i>
                                }
                        $html.='</div> 
                         </div>    
                    </div>';
        }
        if($tipo==1){
            if($permiso_perfil1==1){  
                foreach ($list_consultas as $item){
                    $import=''; 
                    if($item->estatus==1){
                        $import='style="color: #346c94 !important; border-color: #346c94 !important; background-color:#fff7b4;"'; 
                    }else{
                        $import='style="color: #346c94 !important; border-color: #346c94 !important;"'; 
                    }
                    //$colorbtn=''; 
                    $icon='<span> <i class="fas fa-syringe"></i> </span>'; 
                    $strike1='';
                    $strike2='';
                    if($item->activo==0){
                        $strike1='<strike>';
                        $strike2='</strike>';
                    }else{

                    }
                $html.='<button type="button" class="btn btn-rounded btn-block" '.$import.' onclick="visualizar_consulta_medicina_esttetica('.$item->idconsulta.')">'.$strike1.' E '.$icon.'  '.date('d/m/Y',strtotime($item->consultafecha)).' '.$strike2.'</button>';
                }//<i class="mdi mdi-copyright icon_to"></i>
            }
        }    
        if($tipo==2){
            if($permiso_perfil2==1){  
                foreach ($list_consultas_spa as $item){
                    $import=''; 
                    if($item->estatus==1){
                        $import='style="color: #346c94 !important; border-color: #346c94 !important; background-color:#fff7b4;"'; 
                    }else{
                        $import='style="color: #346c94 !important; border-color: #346c94 !important;"'; 
                    }
                    //$colorbtn=''; 
                    $icon='<i class="fab fa-envira"></i>'; 
                    $strike1='';
                    $strike2='';
                    if($item->activo==0){
                        $strike1='<strike>';
                        $strike2='</strike>';
                    }else{

                    }
                $html.='<button type="button" class="btn btn-rounded btn-block" '.$import.' onclick="visualizar_spa('.$item->idconsulta.')">'.$strike1.' '.$icon.'  '.date('d/m/Y',strtotime($item->consultafecha)).' S '.$strike2.'</button>';
                }//<i class="mdi mdi-copyright icon_to"></i>
            }
        }
        if($tipo==3){
            if($permiso_perfil3==1){  
                foreach ($list_consultas_nutricion as $item){
                    $import=''; 
                    if($item->estatus==1){
                        $import='style="color: #346c94 !important; border-color: #346c94 !important; background-color:#fff7b4;"'; 
                    }else{
                        $import='style="color: #346c94 !important; border-color: #346c94 !important;"'; 
                    }
                    //$colorbtn=''; 
                    $icon='<i class="fab fa-apple"></i>'; 
                    $strike1='';
                    $strike2='';
                    if($item->activo==0){
                        $strike1='<strike>';
                        $strike2='</strike>';
                    }else{

                    }
                $html.='<button type="button" class="btn btn-rounded btn-block" btn_style_c" '.$import.' onclick="visualizar_consulta_nutricion('.$item->idconsulta.')">'.$strike1.' N '.$icon.'  '.date('d/m/Y',strtotime($item->consultafecha)).' '.$strike2.'</button>';
                }//<i class="mdi mdi-copyright icon_to"></i>
            }
        }
        if($tipo==4){
            $html.='<div class="row">
                    <div class="col-md-12">
                        <div class="margen_todo">
                            <div class="card-body" style="padding: 0.25rem !important;">';
                                if($permiso_perfil1==1){  
                                    $arrayinfoc1 = array('idpaciente'=>$idp,'activo'=>0);
                                    $list_consultasc1=$this->General_model->getselectwhere_orden_desc('consulta_medicina_estetica',$arrayinfoc1,'consultafecha');
                                    foreach ($list_consultasc1 as $item){
                                        $import=''; 
                                        if($item->estatus==1){
                                            $import='style="background-color:#FFC107; border-radius: 5px;"'; 
                                        }else{
                                            $import=''; 
                                        }
                                        $colorbtn='style="color: #346c94 !important; border-color: #346c94 !important;"'; 
                                        $icon='<span '.$import.'> <i class="fas fa-syringe"></i> </span>'; 
                                    $html.='<button type="button" class="btn btn-rounded btn-block btn-xs btn_style_c" '.$colorbtn.' onclick="visualizar_consulta_medicina_esttetica('.$item->idconsulta.')"><strike> '.$icon.'  '.date('d/m/Y',strtotime($item->consultafecha)).' </strike></button>';
                                    }//<i class="mdi mdi-copyright icon_to"></i>
                                }
                                if($permiso_perfil2==1){
                                    $arrayinfoc2 = array('idpaciente'=>$idp,'activo'=>0);
                                    $list_consultasc2=$this->General_model->getselectwhere_orden_desc('consulta_spa',$arrayinfoc2,'consultafecha');
                                    foreach ($list_consultasc2 as $item){
                                        $import=''; 
                                        if($item->estatus==1){
                                            $import='style="background-color:#FFC107; border-radius: 5px;"'; 
                                        }else{
                                            $import=''; 
                                        }
                                        $colorbtn='style="color: #346c94 !important; border-color: #346c94 !important;"'; 
                                        $icon='<span '.$import.'> <i class="fab fa-envira"></i> </span>'; 
                                    $html.='<button type="button" class="btn btn-rounded btn-block btn-xs btn_style_c" '.$colorbtn.' onclick="visualizar_spa('.$item->idconsulta.')"><strike> '.$icon.'  '.date('d/m/Y',strtotime($item->consultafecha)).' </strike></button>';
                                    }//<i class="mdi mdi-copyright icon_to"></i>
                                }
                                if($permiso_perfil3==1){  
                                    $arrayinfoc3 = array('idpaciente'=>$idp,'activo'=>0);
                                    $list_consultasc3=$this->General_model->getselectwhere_orden_desc('consulta_nutricion',$arrayinfoc3,'consultafecha');
                                    foreach ($list_consultasc3 as $item){
                                        $import=''; 
                                        if($item->estatus==1){
                                            $import='style="background-color:#FFC107; border-radius: 5px;"'; 
                                        }else{
                                            $import=''; 
                                        }
                                        $colorbtn='style="color: #346c94 !important; border-color: #346c94 !important;"'; 
                                        $icon='<span '.$import.'> <i class="fab fa-apple"></i> </span>'; 
                                    $html.='<button type="button" class="btn btn-rounded btn-block btn-xs btn_style_c" '.$colorbtn.' onclick="visualizar_consulta_nutricion('.$item->idconsulta.')"><strike> '.$icon.'  '.date('d/m/Y',strtotime($item->consultafecha)).' </strike></button>';
                                    }//<i class="mdi mdi-copyright icon_to"></i>
                                }
                    $html.='</div> 
                     </div>    
                </div>'; 
        }
        echo $html;
    }    
    /*
    public function listado_consulta(){
        $tipo=$this->input->post('tipo');
        $idp=$this->input->post('idp');
        if($tipo==1){
            $arrayinfo = array('idpaciente'=>$idp,'activo'=>1);
        }else if($tipo==2){
            $arrayinfo = array('idpaciente'=>$idp,'activo'=>1);
        }else if($tipo==3){
            $arrayinfo = array('idpaciente'=>$idp,'activo'=>1);
        }else if($tipo==4){
            $arrayinfo = array('idpaciente'=>$idp,'activo'=>1);
        }else if($tipo==5){
            $arrayinfo = array('idpaciente'=>$idp,'activo'=>0);
        }else{
            $arrayinfo = array('idpaciente'=>$idp);
        }
        $list_consultas=$this->General_model->getselectwhere_orden_desc('consultas',$arrayinfo,'consultafecha');
        $html='';
        if($tipo==0){
        $html.='<div class="row">
                    <div class="col-md-12">
                        <div class="margen_todo">
                         <div class="card-body" style="padding: 0.25rem !important;">';
                        foreach ($list_consultas as $item){
                            $colorbtn='';
                            $icon='';
                            $import=''; 
                            if($item->status==1){
                                $import='style="background-color:#FFC107; border-radius: 5px;"'; 
                            }else{
                                $import=''; 
                            }
                            if($item->tipo!=1){
                                $colorbtn='style="color: #9C27B0 !important; border-color: #9C27B0 !important;"'; 
                                $icon='<span '.$import.'>(AM)</span>'; 
                            }else{
                                $colorbtn='style="color: #346c94 !important; border-color: #346c94 !important;"'; 
                                $icon='<span '.$import.'>(AMD)</span>'; 
                            } 
                            $strike1='';
                            $strike2='';
                            if($item->activo==0){
                                $strike1='<strike>';
                                $strike2='</strike>';
                            }else{

                            }

                        $html.='<button type="button" class="btn btn-rounded btn-block btn-xs" btn_style_c" '.$colorbtn.' onclick="visualizar_consulta('.$item->idconsulta.')">'.$strike1.' '.$icon.'  '.date('d/m/Y',strtotime($item->consultafecha)).' '.$strike2.'</button>';
                        }//<i class="mdi mdi-copyright icon_to"></i>
                    $html.='</div> 
                     </div>    
                </div>';
        }else if($tipo==1){
        $html.='<div class="row">
                    <div class="col-md-12">
                        <div class="margen_todo">
                         <div class="card-body" style="padding: 0.25rem !important;">';
                        foreach ($list_consultas as $item){
                            $colorbtn='';
                            $icon='';
                            $import=''; 
                            if($item->status==1){
                                $import='style="background-color:#FFC107; border-radius: 5px;"'; 
                            }else{
                                $import=''; 
                            }
                            if($item->tipo!=1){
                                $colorbtn='style="color: #9C27B0 !important; border-color: #9C27B0 !important;"'; 
                                $icon='<span '.$import.'>(AM)</span>'; 
                            }else{
                                $colorbtn='style="color: #346c94 !important; border-color: #346c94 !important;"'; 
                                $icon='<span '.$import.'>(AMD)</span>'; 
                            } 

                        $html.='<button type="button" class="btn btn-rounded btn-block btn-xs" btn_style_c" '.$colorbtn.' onclick="visualizar_consulta('.$item->idconsulta.')">'.$icon.'  '.date('d/m/Y',strtotime($item->consultafecha)).'</button>';
                        }//<i class="mdi mdi-copyright icon_to"></i>
                    $html.='</div> 
                     </div>    
                </div>';
        }else if($tipo==2){

        }else if($tipo==3){

        }else if($tipo==4){

        }else if($tipo==5){
        $html.='<div class="row">
                    <div class="col-md-12">
                        <div class="margen_todo">
                         <div class="card-body" style="padding: 0.25rem !important;">';
                        foreach ($list_consultas as $item){
                            $colorbtn='';
                            $icon='';
                            $import=''; 
                            if($item->status==1){
                                $import='style="background-color:#FFC107; border-radius: 5px;"'; 
                            }else{
                                $import=''; 
                            }
                            if($item->tipo!=1){
                                $colorbtn='style="color: #9C27B0 !important; border-color: #9C27B0 !important;"'; 
                                $icon='<span '.$import.'>(AM)</span>'; 
                            }else{
                                $colorbtn='style="color: #346c94 !important; border-color: #346c94 !important;"'; 
                                $icon='<span '.$import.'>(AMD)</span>'; 
                            } 

                        $html.='<button type="button" class="btn btn-rounded btn-block btn-xs" btn_style_c" '.$colorbtn.' onclick="visualizar_consulta('.$item->idconsulta.')"><strike> '.$icon.'  '.date('d/m/Y',strtotime($item->consultafecha)).' </strike></button>';
                        }//<i class="mdi mdi-copyright icon_to"></i>
                    $html.='</div> 
                     </div>    
                </div>';
        }
        echo $html;
    }
    */
    public function visualizar_consulta_text(){
        $idc=$this->input->post('id');
        $get_infoc=$this->General_model->get_record('idconsulta',$idc,'consultas');
        if($get_infoc->status==0){
            $style_color='';
            $importante="Importante";
        }else{
            $importante="No Importante";
            $style_color='background-color: #fff7b4;';
        }
        $get_infop=$this->General_model->get_record('idpaciente',$get_infoc->idpaciente,'pacientes');
        $tiempo = strtotime($get_infop->fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
            $edad = floor($edad); 
        
        $aux_existe=0;
        $fecha_consulta_ultima=$this->fechainicio;
        $arrayinfop = array('idpaciente'=>$get_infoc->idpaciente);
        $get_consultasp_desc=$this->General_model->getselectwhere_orden_desc('consultas',$arrayinfop,'consultafecha');
        foreach($get_consultasp_desc as $item){
            $aux_existe=1;
            $fecha_consulta_ultima=$item->consultafecha;
        }
        $html='';
        $html.='<div class="margen_todo">
                    <div class="card-body" style="padding: 0.25rem !important; '.$style_color.'">
                        <div class="row">
                            <div class="col-md-12" align="right">';
                            if($get_infoc->activo==1){
                        $html.='<button type="button" class="btn waves-effect waves-light btn-info" onclick="txt_consulta('.$idc.')">Editar consulta</button>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-outline-secondary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="fas fa-ellipsis-v"></i>
                                    </button>
                                    <div class="dropdown-menu" x-placement="bottom-start" style="will-change: transform; position: absolute; transform: translate3d(0px, 35px, 0px); top: 0px; left: 0px;">
                                      <a class="dropdown-item" onclick="eliminar_consulta('.$idc.')"><i class="fas fa-trash-alt"></i>  Eliminar consulta</a>
                                      <a class="dropdown-item" onclick="marcar_importante('.$idc.','.$get_infoc->status.')"><i class="fas fa-check-double"></i> Marcar como '.$importante.'</a>
                                    </div>
                                </div>';
                            }else{
                        $html.='<button type="button" class="btn waves-effect waves-light btn-info" onclick="txt_consulta('.$idc.')">Editar consulta</button>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-outline-secondary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="fas fa-ellipsis-v"></i>
                                    </button>
                                    <div class="dropdown-menu" x-placement="bottom-start" style="will-change: transform; position: absolute; transform: translate3d(0px, 35px, 0px); top: 0px; left: 0px;">
                                      <a class="dropdown-item" onclick="restaurar_consulta('.$idc.')">Restaurar consulta</a>
                                    </div>
                                </div>';
                            }
                    $html.='</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" align="center">
                                <h6>Resumen de consulta<h6> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h6><i class="fas fa-user"></i> Edad del paciente: <span class="letranegrita">'.$edad.' años</span></h6>
                                <h6><i class="fas fa-calendar-check"></i> Fecha de consulta: <span class="letranegrita">'.date('d/m/Y',strtotime($get_infoc->consultafecha)).'</span></h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">';
                            if($fecha_consulta_ultima==$get_infoc->consultafecha || $aux_existe==0){ 
                        $html.='<h4 class="div_abajo_solid">Padecimiento Actual</h4>
                                <p>'.$get_infoc->nota_evaluacion.'</p>
                                <h4 class="div_abajo_solid">Fecha de inicio del padecimiento</h4>
                                <p>'.date('d/m/Y',strtotime($get_infoc->fechainicio)).'</p>';
                            }else{
                        $html.='<h4 class="div_abajo_solid">Nota de Evolución</h4>
                                <p>'.$get_infoc->nota_evaluacion.'</p>';
                            }
                    $html.='</div>
                        </div>
                        <div class="row">';
                            if($get_infoc->altrura!=0  || $get_infoc->peso!=0 || $get_infoc->ta!='' || $get_infoc->tempc!=0 || $get_infoc->fc!='' || $get_infoc->fr!=0 || $get_infoc->o2!=0){  
                            $html.='<div class="col-md-12">';
                                $html.='<h4 class="div_abajo_solid">Signos Vitales/Básicos</h4>
                                    </div>';
                                if($get_infoc->altrura!=0){
                            $html.='<div class="col-md-3">
                                        <p>Altura <span class="div_etiqueta">&nbsp'.$get_infoc->altrura.'&nbsp</span></p>
                                    </div>';
                                }
                                if($get_infoc->peso!=0){
                            $html.='<div class="col-md-3">
                                        <p>Peso <span class="div_etiqueta">&nbsp'.$get_infoc->peso.'&nbsp</span></p>
                                    </div>';
                                } 
                                if($get_infoc->ta!=''){
                            $html.='<div class="col-md-3">
                                        <p>T.A. <span class="div_etiqueta">&nbsp'.$get_infoc->ta.'&nbsp</span></p>
                                    </div>';
                                }
                                if($get_infoc->tempc!=0){
                            $html.='<div class="col-md-3">
                                        <p>Temp <span class="div_etiqueta">&nbsp'.$get_infoc->tempc.'&nbsp</span></p>
                                    </div>';
                                }
                                if($get_infoc->fc!=''){
                            $html.='<div class="col-md-3">
                                        <p>F.C. <span class="div_etiqueta">&nbsp'.$get_infoc->fc.'&nbsp</span></p>
                                    </div>';
                                }    
                                if($get_infoc->fr!=0){
                            $html.='<div class="col-md-3">
                                        <p>F.R. <span class="div_etiqueta">&nbsp'.$get_infoc->fr.'&nbsp</span></p>
                                    </div>';
                                }
                                if($get_infoc->o2!=0){
                            $html.='<div class="col-md-3">
                                        <p>O2 <span class="div_etiqueta">&nbsp'.$get_infoc->o2.'&nbsp</span></p>
                                    </div>';
                                }
                            }
                $html.='</div>
                        <div class="row">';
                    $html.='<div class="col-md-12">';
                                if($get_infoc->exploracionfisica!=''){
                                $html.='<h4 class="div_abajo_solid">Exploración Física</h4>
                                    <p>'.$get_infoc->exploracionfisica.'</p>
                                ';
                                }
                    $html.='</div>
                            <div class="col-md-12">';
                                if($get_infoc->resultadoestudiolaboratorio!=''){
                                $html.='<h4 class="div_abajo_solid">Estudios de Laboratorio</h4>
                                    <p>'.$get_infoc->resultadoestudiolaboratorio.'</p>
                                ';
                                }
                    $html.='</div>
                             <div class="col-md-12">';
                                $aux_dg=0;
                                $arraydiag = array('idconsulta' => $idc,'activo'=>1);
                                $get_diagn=$this->General_model->getselectwhereall('diagnosticos',$arraydiag);
                                foreach($get_diagn as $key){
                                    $aux_dg=1;
                                }
                                if($aux_dg==1){
                                    $html.='<h4 class="div_abajo_solid">Diagnósticos</h4>
                                    <ol>';
                                    foreach ($get_diagn as $item){
                                    $html.='<li>'.$item->diagnostico.'</li>';   
                                    }
                                $html.='</ol>';
                                }
                    $html.='</div>
                            <div class="col-md-12">';
                                $arrayrece = array('idconsulta' => $idc,'activo'=>1);
                                $get_receta_t=$this->General_model->getselectwhereall('receta',$arrayrece);
                                $aux_v_r=0;
                                foreach ($get_receta_t as $item){
                                    $aux_v_r=1;
                                }
                                if($aux_v_r==1){
                                    $html.='<h4 class="div_abajo_solid">Medicamentos</h4>';
                                    $html.='<ol>';
                                    foreach ($get_receta_t as $item){
                                        $arrayrecem = array('idreceta'=>$item->idreceta,'activo'=>1);
                                        $get_receta_m=$this->General_model->getselectwhereall('receta_medicamento',$arrayrecem);
                                        foreach ($get_receta_m as $item){
                                            $html.='<li>'.$item->medicamento.' - '.$item->tomar.'</li>';
                                        }
                                    }
                                    $html.='</ol>';
                                }
                                
                    $html.='</div>
                            <div class="col-md-12">';
                                $arrayorde = array('idconsulta' => $idc,'activo'=>1);
                                $get_orden_t=$this->General_model->getselectwhereall('orden_estudio',$arrayorde);
                                $aux_o_r=0;
                                foreach ($get_orden_t as $item){
                                    $aux_o_r=1;
                                }
                                if($aux_o_r==1){
                                    $html.='<h4 class="div_abajo_solid">Estudios Requeridos</h4>';    
                                    foreach ($get_orden_t as $item){
                                        $html.='<h4 style="border-bottom: 1px dashed #8dada6 !important;">Orden de laboratorio</h4>';
                                        $obsev=$item->observaciones;
                                        $html.='<ol>';
                                        $arrayordenm = array('idorden'=>$item->idorden,'activo'=>1);
                                        $get_orden_m=$this->General_model->getselectwhereall('orden_estudio_laboratorio',$arrayordenm);
                                        foreach ($get_orden_m as $item){
                                            $html.='<li>'.$item->detalles.'</li>';
                                        }
                                    $html.='</ol>';
                                        if($obsev!=''){
                                        $html.='<strong>Observaciones</strong>';
                                        $html.='<h6 style="border-bottom: 1px dashed #8dada6 !important;">'.$obsev.'</h6>';
                                        }
                                    }
                                    
                                }
                                
                    $html.='</div>
                            <div class="col-md-12">';

                                $arrayorde = array('idconsulta' => intval($idc),'activo'=>1);
                                
                                $get_orden_int=$this->General_model->getselectwhereall('interconsulta',$arrayorde);
                                $aux_o_int=0;
                                foreach ($get_orden_int as $item){
                                    $aux_o_int=1;
                                }
                                
                                if($aux_o_r==1){
                                    $html.='<h4 class="div_abajo_solid">Orden de interconsulta</h4>';
                                    foreach ($get_orden_int as $a){
                                        $html.='<ul>';
                                        $arrayordenint = array('idinterconsulta'=>$a->idinterconsulta,'activo'=>1);
                                        $get_orden_int=$this->General_model->getselectwhereall('interconsulta_medico',$arrayordenint);
                                        $medico_txt='';
                                        foreach ($get_orden_int as $item){
                                            $medico_txt='<strong>'.$item->nombre.'</strong> |'.$item->especialidad;
                                        }
                                        $html.='<li>'.$medico_txt.'</li>';
                                        $html.='</ul>';

                                        $html.='<h6 style="border-bottom: 1px dashed #8dada6 !important;">'.$a->motivo_interconsulta.'</h6>';
                                    }   

                                    /* 
                                    foreach ($get_orden_t as $item){
                                        $obsev=$item->observaciones;
                                        $html.='<ol>';
                                        $arrayordenm = array('idorden'=>$item->idorden,'activo'=>1);
                                        $get_orden_m=$this->General_model->getselectwhereall('orden_estudio_laboratorio',$arrayordenm);
                                        foreach ($get_orden_m as $item){
                                            $html.='<li>'.$item->detalles.'</li>';
                                        }
                                    
                                    $html.='</ol>';
                                    */
                                    
                                    
                                }
                                
                    $html.='</div>
                            <div class="col-md-12">';
                                $arrayorh = array('idconsulta' => intval($idc),'activo'=>1);
                                
                                $get_orden_orh=$this->General_model->getselectwhereall('orden_hospital',$arrayorh);
                                $aux_o_orh=0;
                                foreach ($get_orden_orh as $item){
                                    $aux_o_orh=1;
                                }
                                if($aux_o_orh==1){
                                    foreach ($get_orden_orh as $a){
                                        $html.='<h4 class="div_abajo_solid">Orden de hospitalizacion</h4>';
                                        $html.='<h6 style="border-bottom: 1px dashed #8dada6 !important;">'.$a->detalles.'</h6>';
                                    }                                       
                                }
                                
                    $html.='</div>
                            <div class="col-md-12">';
                                $arrayorh = array('idconsulta' => intval($idc),'activo'=>1);
                                
                                $get_orden_orh=$this->General_model->getselectwhereall('certificado_medico',$arrayorh);
                                $aux_o_orh=0;
                                foreach ($get_orden_orh as $item){
                                    $aux_o_orh=1;
                                }
                                if($aux_o_orh==1){
                                    foreach ($get_orden_orh as $a){
                                        $html.='<h4 class="div_abajo_solid">Certificado Médico</h4>';
                                        $html.='<h6 style="border-bottom: 1px dashed #8dada6 !important;">'.$a->detalles.'</h6>';
                                    }                                       
                                }
                                
                    $html.='</div>
                            <div class="col-md-12">';
                                $arrayorj = array('idconsulta' => intval($idc),'activo'=>1);
                                
                                $get_orden_j=$this->General_model->getselectwhereall('orden_justificante_medico',$arrayorj);
                                $aux_o_j=0;
                                foreach ($get_orden_j as $item){
                                    $aux_o_j=1;
                                }
                                if($aux_o_j==1){
                                    foreach ($get_orden_j as $a){
                                        $html.='<h4 class="div_abajo_solid">Justificante Médico</h4>';
                                        $html.='<h6 style="border-bottom: 1px dashed #8dada6 !important;">'.$a->detalles.'</h6>';
                                    }                                       
                                }
                    $html.='</div>
                            <div class="col-md-12">';
                                $arrayoro = array('idconsulta' => intval($idc),'activo'=>1);
                                
                                $get_orden_o=$this->General_model->getselectwhereall('orden_libre',$arrayoro);
                                $aux_o_o=0;
                                foreach ($get_orden_o as $item){
                                    $aux_o_o=1;
                                }
                                if($aux_o_o==1){
                                    foreach ($get_orden_o as $a){
                                        $html.='<h4 class="div_abajo_solid">Orden libre</h4>';
                                        $html.='<h6 style="border-bottom: 1px dashed #8dada6 !important;">'.$a->detalles.'</h6>';
                                    }                                       
                                }
                    $html.='</div>


                            <div class="col-md-12">';
                                if($get_infoc->proximafecha!='0000-00-00'){
                                $html.='<h4 class="div_abajo_solid">Próxima cita</h4>
                                    <p>Semana del '.date('d/m/Y',strtotime($get_infoc->proximafecha)).'</p>
                                ';
                                }
                    $html.='</div>
                            <div class="col-md-12">';
                    $html.='</div>
                        </div>      
                        <div class="row">
                            <div class="col-md-12" align="right">
                                <p style="font-style: italic"><span style="font-size:20px">'.$this->administrador.' </span><span class="letranegrita"> '.date('d/m/Y',strtotime($get_infoc->consultafecha)).' '.date('G:i:s',strtotime($get_infoc->horainicio)).'</span>&nbsp&nbsp</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" align="center">
                                <button type="button" class="btn waves-effect waves-light btn-info" onclick="imprimir_resumen('.$idc.')"><i class="fas fa-print"></i>  Imprimir Resumen</button>
                                <button type="button" class="btn waves-effect waves-light btn-secondary" onclick="imprimir_ultima_receta('.$idc.')">Imprimir última receta</button>
                            </div>
                        </div>    
                    </div>
                </div>';
        echo $html;
    }
    public function registro_diagnostico(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $iddiagnostico = $DATA[$i]->iddiagnostico;

            $data['idconsulta']=$DATA[$i]->idconsulta;  
            $data['diagnostico']=$DATA[$i]->diagnostico; 
            if($iddiagnostico==0){
                $this->General_model->add_record('diagnosticos',$data);
            }else{
                $this->General_model->edit_record('iddiagnostico',$iddiagnostico,$data,'diagnosticos');
            }
        }
    }
    public function registro_receta(){
        $datos = $this->input->post('data');
        $datosm = $this->input->post('datam');
        //var_dump($datos);
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idreceta = $DATA[$i]->idreceta;
            $idreceta_aux = $DATA[$i]->idreceta_aux;
            $data['idconsulta']=$DATA[$i]->idconsulta;  
            $data['indicaciones']=$DATA[$i]->indicaciones; 
            $id_receta=0;
            if($idreceta==0){
                $id_receta=$this->General_model->add_record('receta',$data);
            }else{
                $this->General_model->edit_record('idreceta',$idreceta,$data,'receta');
                $id_receta=$idreceta;
            }
            $medi_receta_aux=01;
            $DATAM = json_decode($datosm);
            for ($im=0;$im<count($DATAM);$im++) { 
                $idmedicamento = $DATAM[$im]->idmedicamento;
                $medi_receta_aux = $DATAM[$im]->medi_receta_aux;
                $datam['idreceta']=$id_receta;  
                $datam['medicamento']=$DATAM[$im]->medicamento;
                $datam['tomar']=$DATAM[$im]->tomar; 
                if($medi_receta_aux==$idreceta_aux){
                    if($idmedicamento==0){
                        $this->General_model->add_record('receta_medicamento',$datam);
                    }else{
                        $this->General_model->edit_record('idmedicamento',$idmedicamento,$datam,'receta_medicamento');
                    }
                }    
            }
        
        }
    }

    public function registro_interconsulta(){
        $datos = $this->input->post('data');
        $datosm = $this->input->post('datam');
        //var_dump($datos);
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idinterconsulta = $DATA[$i]->idinterconsulta;
            $idinterconsulta_aux = $DATA[$i]->idinterconsulta_aux;

            $data['idconsulta']=$DATA[$i]->idconsulta;
            if($DATA[$i]->incluir_diagnostico==true){
               $data['incluir_diagnostico']=1;    
            }else{
               $data['incluir_diagnostico']=0;     
            }
            $data['nombre_diagnostico']=$DATA[$i]->nombre_diagnostico;
            $data['motivo_interconsulta']=$DATA[$i]->motivo_interconsulta;

            $id_interconsulta=0;
            if($idinterconsulta==0){
                $id_interconsulta=$this->General_model->add_record('interconsulta',$data);
            }else{
                $this->General_model->edit_record('idinterconsulta',$idinterconsulta,$data,'interconsulta');
                $id_interconsulta=$idinterconsulta;
            }
            $meid_medico_aux=01;
            $DATAM = json_decode($datosm);
            for ($im=0;$im<count($DATAM);$im++) { 
                $idmedico = $DATAM[$im]->idmedico;
                $meid_medico_aux = $DATAM[$im]->meid_medico_aux;
                $datam['idinterconsulta']=$id_interconsulta;  
                $datam['nombre']=$DATAM[$im]->nombre;
                $datam['especialidad']=$DATAM[$im]->especialidad; 
                $datam['hospital']=$DATAM[$im]->hospital; 
                $datam['no_consultorio']=$DATAM[$im]->no_consultorio; 
                $datam['domicilio']=$DATAM[$im]->domicilio; 
                $datam['telefono']=$DATAM[$im]->telefono; 
                $datam['ciudad']=$DATAM[$im]->ciudad; 

                if($meid_medico_aux==$idinterconsulta_aux){
                    if($idmedico==0){
                        $this->General_model->add_record('interconsulta_medico',$datam);
                    }else{
                        $this->General_model->edit_record('id',$idmedico,$datam,'interconsulta_medico');
                    }
                }    
            }
        
        }
    }

    public function registro_ordenestudio(){
        $datos = $this->input->post('data');
        $datosl = $this->input->post('datal');
        //var_dump($datos);die;
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idorden = intval($DATA[$i]->idorden);
            //var_dump($idorden);die;
            $idorden_aux = $DATA[$i]->idorden_aux;
            $data['idconsulta']=$DATA[$i]->idconsulta;  
            if($DATA[$i]->recomendar==true){
               $data['recomendar']=1;    
            }else{
               $data['recomendar']=0;     
            }
            $data['nombre_laboratorio']=$DATA[$i]->nombre_laboratorio; 
            $data['direccion']=$DATA[$i]->direccion; 
            $data['telefono']=$DATA[$i]->telefono; 
            $data['observaciones']=$DATA[$i]->observaciones; 
            if($DATA[$i]->incluir==true){
               $data['incluir']=1;    
            }else{
               $data['incluir']=0;     
            }
            $data['diagnostico']=$DATA[$i]->diagnostico; 
            $id_orden=0;
            if($idorden==0){
                $id_orden=$this->General_model->add_record('orden_estudio',$data);
            }else{
                $this->General_model->edit_record('idorden',$idorden,$data,'orden_estudio');
                $id_orden=$idorden;
            }
            $labora_aux=01;
            $DATAL = json_decode($datosl);

            for ($il=0;$il<count($DATAL);$il++) { 
                $idlaboratorio = $DATAL[$il]->idlaboratorio;
                $labora_aux = $DATAL[$il]->laboratorio_aux;
                $datal['idorden']=$id_orden;  
                $datal['detalles']=$DATAL[$il]->detalles; 
                if($labora_aux==$idorden_aux){
                    if($idlaboratorio==0){
                        $this->General_model->add_record('orden_estudio_laboratorio',$datal);
                    }else{
                        $this->General_model->edit_record('idlaboratorio',$idlaboratorio,$datal,'orden_estudio_laboratorio');
                    }
                }    
            }
        
        }
    }
    public function registro_orden_hospital(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idorden_hospital = $DATA[$i]->idorden_hospital;
            $data['idconsulta']=$DATA[$i]->idconsulta;  
            $data['detalles']=$DATA[$i]->detalles; 
            if($idorden_hospital==0){
              $this->General_model->add_record('orden_hospital',$data);
            }else{
              $this->General_model->edit_record('idorden_hospital',$idorden_hospital,$data,'orden_hospital');
            }
        }
    }
    public function registro_certificado_medico(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idcertificado_medico = $DATA[$i]->idcertificado_medico;
            $data['idconsulta']=$DATA[$i]->idconsulta;  
            $data['detalles']=$DATA[$i]->detalles; 
            if($idcertificado_medico==0){
              $this->General_model->add_record('certificado_medico',$data);
            }else{
              $this->General_model->edit_record('idcertificado_medico',$idcertificado_medico,$data,'certificado_medico');
            }
        }
    }

    public function registro_justificante_medico(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idjustificante = $DATA[$i]->idjustificante;
            $data['idconsulta']=$DATA[$i]->idconsulta;  
            $data['detalles']=$DATA[$i]->detalles; 
            if($idjustificante==0){
              $this->General_model->add_record('orden_justificante_medico',$data);
            }else{
              $this->General_model->edit_record('idjustificante',$idjustificante,$data,'orden_justificante_medico');
            }
        }
    }

    public function registro_orden_libre(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idorden = $DATA[$i]->idorden;
            $data['idconsulta']=$DATA[$i]->idconsulta;  
            $data['detalles']=$DATA[$i]->detalles; 
            if($idorden==0){
              $this->General_model->add_record('orden_libre',$data);
            }else{
              $this->General_model->edit_record('idorden',$idorden,$data,'orden_libre');
            }
        }
    }

    function get_diagnostico(){
        $id = $this->input->post('id');
        $arrayinfo = array('idconsulta' => $id,'activo'=>1);
        $results=$this->General_model->getselectwhereall('diagnosticos',$arrayinfo);
        echo json_encode($results);
    }
    public function archivar_diagnostico(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('iddiagnostico',$id,$data,'diagnosticos');
    }
    function get_receta(){
        $id = $this->input->post('id');
        $arrayinfo = array('idconsulta' => $id,'activo'=>1);
        $results=$this->General_model->getselectwhereall('receta',$arrayinfo);
        echo json_encode($results);
    }
    function get_ordenes_estudio(){
        $id = $this->input->post('id');
        $arrayinfo = array('idconsulta' => $id,'activo'=>1);
        $results=$this->General_model->getselectwhereall('orden_estudio',$arrayinfo);
        echo json_encode($results);
    }
    function get_li_receta(){
        $id = $this->input->post('id');
        $arrayinfo = array('idreceta' => $id,'activo'=>1);
        $results=$this->General_model->getselectwhereall('receta_medicamento',$arrayinfo);
        echo json_encode($results);
    }
    function get_li_ordenes_estudio(){
        $id = $this->input->post('id');
        $arrayinfo = array('idorden' => $id,'activo'=>1);
        $results=$this->General_model->getselectwhereall('orden_estudio_laboratorio',$arrayinfo);
        echo json_encode($results);
    }
    function get_ordenes_interconsulta(){
        $id = $this->input->post('id');
        $arrayinfo = array('idconsulta' => $id,'activo'=>1);
        $results=$this->General_model->getselectwhereall('interconsulta',$arrayinfo);
        echo json_encode($results);
    }
    function get_li_ordenes_interconsulta_medico(){
        $id = $this->input->post('id');
        $arrayinfo = array('idinterconsulta' => $id,'activo'=>1);
        $results=$this->General_model->getselectwhereall('interconsulta_medico',$arrayinfo);
        echo json_encode($results);
    }
    function get_orden_hospital(){
        $id = $this->input->post('id');
        $arrayinfo = array('idconsulta' => $id,'activo'=>1);
        $results=$this->General_model->getselectwhereall('orden_hospital',$arrayinfo);
        echo json_encode($results);
    }
    function get_certificado(){
        $id = $this->input->post('id');
        $arrayinfo = array('idconsulta' => $id,'activo'=>1);
        $results=$this->General_model->getselectwhereall('certificado_medico',$arrayinfo);
        echo json_encode($results);
    }
    function get_justicante_medico(){
        $id = $this->input->post('id');
        $arrayinfo = array('idconsulta' => $id,'activo'=>1);
        $results=$this->General_model->getselectwhereall('orden_justificante_medico',$arrayinfo);
        echo json_encode($results);
    }

    function getorden_libre(){
        $id = $this->input->post('id');
        $arrayinfo = array('idconsulta' => $id,'activo'=>1);
        $results=$this->General_model->getselectwhereall('orden_libre',$arrayinfo);
        echo json_encode($results);
    }

    public function eliminar_receta(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idreceta',$id,$data,'receta');
    }
    public function eliminar_orden(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idorden',$id,$data,'orden_estudio');
    }
    public function eliminar_li_receta(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idmedicamento',$id,$data,'receta_medicamento');
    }
    public function eliminar_li_orden(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idlaboratorio',$id,$data,'orden_estudio_laboratorio');
    }
    public function eliminar_orden_hospi(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idorden_hospital',$id,$data,'orden_hospital');
    }
    public function eliminar_certificado(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idcertificado_medico',$id,$data,'certificado_medico');
    }
    public function eliminar_interconsulta(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idinterconsulta',$id,$data,'interconsulta');
    }
    public function eliminar_li_interconsulta(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
       $this->General_model->edit_record('id',$id,$data,'interconsulta_medico');
    }
    public function eliminar_jutificante_medico(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idjustificante',$id,$data,'orden_justificante_medico');
    }
     public function eliminar_orden_libre(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idorden',$id,$data,'orden_libre');
    }
    /*
    public function imprimireceta($id){
        $ac=$this->General_model->get_record('idreceta',$id,'receta');
        $ci=$this->General_model->get_record('idconsulta',$ac->idconsulta,'consultas');
        $pa=$this->General_model->get_record('idpaciente',$ci->idpaciente,'pacientes');
        $tiempo = strtotime($pa->fecha_nacimiento); 
        $ahora = time(); 
        $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
        $edad = floor($edad);
        $arrayinfo = array('idreceta'=>$ac->idreceta,'activo'=>1);
        $data['list_consultas']=$this->General_model->getselectwhereall('receta_medicamento',$arrayinfo);
        $data['receta']=$ac;
        $data['consulta']=$ci;
        $data['paciente']=$pa;
        $data['edad']=$edad;
        $this->load->view('paciente/imprimir_recetav2',$data);
        $this->load->view('paciente/print_formato');
    }
    */
    public function imprimireceta($id){
        $ac=$this->General_model->get_record('idreceta',$id,'receta');
        $ci=$this->General_model->get_record('idconsulta',$ac->idconsulta,'consultas');
        $pa=$this->General_model->get_record('idpaciente',$ci->idpaciente,'pacientes');
        $tiempo = strtotime($pa->fecha_nacimiento); 
        $ahora = time(); 
        $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
        $edad = floor($edad);
        $arrayinfo = array('idreceta'=>$ac->idreceta,'activo'=>1);
        $data['list_consultas']=$this->General_model->getselectwhereall('receta_medicamento',$arrayinfo);
        $data['receta']=$ac;
        $data['consulta']=$ci;
        $data['paciente']=$pa;
        $data['edad']=$edad;
        $data['fechahoy']=$this->fechainicio;
        $this->load->view('Reportes/recetapdf',$data);
        //$this->load->view('paciente/imprimir_recetav2',$data);
        //$this->load->view('paciente/print_formato');
    }
    public function imprimireceta2($id){
        $ac=$this->General_model->get_record('idreceta',$id,'receta');
        $ci=$this->General_model->get_record('idconsulta',$ac->idconsulta,'consultas');
        $pa=$this->General_model->get_record('idpaciente',$ci->idpaciente,'pacientes');
        $tiempo = strtotime($pa->fecha_nacimiento); 
        $ahora = time(); 
        $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
        $edad = floor($edad);
        $arrayinfo = array('idreceta'=>$ac->idreceta,'activo'=>1);
        $data['list_consultas']=$this->General_model->getselectwhereall('receta_medicamento',$arrayinfo);
        $data['receta']=$ac;
        $data['consulta']=$ci;
        $data['paciente']=$pa;
        $data['edad']=$edad;
        $data['fechahoy']=$this->fechainicio;
        $this->load->view('Reportes/recetapdf',$data);
        //$this->load->view('paciente/imprimir_recetav2',$data);
        //$this->load->view('paciente/print_formato');
    }

    public function consultar_ultima_receta(){
        $id=$this->input->post('id');
        $result=$this->ModelCatalogos->get_ultima_receta($id);
        $id=0;
        foreach ($result as $item){
            $id=$item->idreceta;
        }
        echo $id;

    }
    public function consultar_ultima_receta_paciente(){
        $id=$this->input->post('id');
        $result=$this->ModelCatalogos->get_ultima_receta_paciente($id);
        $id_aux=0;
        foreach ($result as $item){
            $id_aux=$item->idconsulta;
        }
        $result=$this->ModelCatalogos->get_ultima_receta(intval($id_aux));
        foreach ($result as $item){
            $id_aux=$item->idreceta;
        }
        $id_i=intval($id_aux);
        echo $id_i;

    }
    public function imprimirorden($id){
        $ac=$this->General_model->get_record('idorden',$id,'orden_estudio');
        $ci=$this->General_model->get_record('idconsulta',$ac->idconsulta,'consultas');
        $pa=$this->General_model->get_record('idpaciente',$ci->idpaciente,'pacientes');
        $tiempo = strtotime($pa->fecha_nacimiento); 
        $ahora = time(); 
        $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
        $edad = floor($edad);
        $arrayinfo = array('idorden'=>$ac->idorden,'activo'=>1);
        $data['list_estudio']=$this->General_model->getselectwhereall('orden_estudio_laboratorio',$arrayinfo);
        $data['orden']=$ac;
        $data['consulta']=$ci;
        $data['paciente']=$pa;
        $data['edad']=$edad;
        $this->load->view('paciente/imprimir_orden',$data);
        $this->load->view('paciente/print_formato');
    }
    public function imprimirorden_v2($id){
        $ac=$this->General_model->get_record('idorden',$id,'orden_estudio');
        $ci=$this->General_model->get_record('idconsulta',$ac->idconsulta,'consultas');
        $pa=$this->General_model->get_record('idpaciente',$ci->idpaciente,'pacientes');
        $tiempo = strtotime($pa->fecha_nacimiento); 
        $ahora = time(); 
        $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
        $edad = floor($edad);
        $arrayinfo = array('idorden'=>$ac->idorden,'activo'=>1);
        $data['list_estudio']=$this->General_model->getselectwhereall('orden_estudio_laboratorio',$arrayinfo);
        $data['orden']=$ac;
        $data['consulta']=$ci;
        $data['paciente']=$pa;
        $data['edad']=$edad;
        //var_dump($pa);
        $this->load->view('paciente/header');
        $this->load->view('paciente/imprimir_ordenv2',$data);
        $this->load->view('paciente/footer');
        $this->load->view('paciente/print_formato');

    }
    function imprimirordenv2($id){
        $ac=$this->General_model->get_record('idorden',$id,'orden_estudio');
        $ci=$this->General_model->get_record('idconsulta',$ac->idconsulta,'consultas');
        $pa=$this->General_model->get_record('idpaciente',$ci->idpaciente,'pacientes');
        $tiempo = strtotime($pa->fecha_nacimiento); 
        $ahora = time(); 
        $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
        $edad = floor($edad);
        $arrayinfo = array('idorden'=>$ac->idorden,'activo'=>1);
        $data['list_estudio']=$this->General_model->getselectwhereall('orden_estudio_laboratorio',$arrayinfo);
        $data['orden']=$ac;
        $data['consulta']=$ci;
        $data['paciente']=$pa;
        $data['edad']=$edad;
        $this->load->view('Reportes/ordenlaboratorio',$data);
    }
    public function video_llamada(){
        $this->load->view('paciente/videollamada');
        $this->load->view('paciente/videollamadajs');
    }
    public function firma_aviso($id){
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $data['personal']=$this->General_model->get_record('personalId',$this->idpersonal,'personal');
        $data['fecha']=$this->fechainicio;
        $this->load->view('paciente/firmar_aviso',$data);
        $this->load->view('paciente/jscanvas');
    }
    public function firma_aviso_movil($id){
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $data['personal']=$this->General_model->get_record('personalId',$this->idpersonal,'personal');
        $data['fecha']=$this->fechainicio;
        $this->load->view('paciente/firma_aviso_movil',$data);
    }
    public function firma_doc($id){
        $data['paciente']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $data['personal']=$this->General_model->get_record('personalId',$this->idpersonal,'personal');
        $data['fecha']=$this->fechainicio;
        $this->load->view('paciente/firma_aviso_m',$data);
        $this->load->view('paciente/jscanvas');
    }
    function guardarimg(){
      $id = $this->input->post('id');
      $acepto_firma = $this->input->post('acepto_firma');
      $utilicen_datos = $this->input->post('utilicen_datos');

      $datax = $this->input->post();
      //$equipo=$datax['equipo'];
      //$idserie=$datax['idserie']; 
      $base64Image=$datax['firma'];
      //$comentario=$datax['comentario']; 
      //$tipo=$datax['tipo'];
      $namefile=date('Y_m_d-h_i_s');
      file_put_contents('uploads/paciente_firma/'.$namefile.'.txt',$base64Image);
      
      $datos = array('doc'=>'','doc_firma'=>$namefile.'.txt','acepto_firma'=>$acepto_firma,'utilicen_datos'=>$utilicen_datos,'fecha_doc'=>$this->fechainicio);
      $this->General_model->edit_record('idpaciente',$id,$datos,'pacientes');
      //echo $base64Image;
    }
    public function eliminar_doc(){
        $id=$this->input->post('id');
        $data = array('doc'=>'','doc_firma'=>'');
        $this->General_model->edit_record('idpaciente',$id,$data,'pacientes');
    }
    function enviar_video_llamada(){
        $idp=$this->input->post('id');
        $correo=$this->input->post('correo');
        $direccion=$this->input->post('direcc');
        $get_info=$this->General_model->get_record('idpaciente',$idp,'pacientes');
        //================================
            //cargamos la libreria email
            $this->load->library('email');
            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mocha3014.mochahost.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'contacto@estudiosunne.sicoi.net';

            //Nuestra contraseña
            $config["smtp_pass"] = '_@fzi[%~5Kx)';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
            $perosnaldatos=$this->General_model->get_record('personalId',$this->idpersonal,'personal');
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('contacto@estudiosunne.sicoi.net',$perosnaldatos->nombre);
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
        /*
        $cita = $this->ModeloCatalogos->getCita($id);      
        foreach ($cita as $item) {
              $fecha = date('d-m-Y',strtotime($item->fecha));
              $hora = date('g:i A',strtotime($item->hora));  
              $paciente = $item->paciente; 
              $correo = $item->correo; 
              $tipo_paciente = $item->tipo_paciente;
              $medico_externo = $item->medico; 
              $estudio = $item->estudio;
              $area = $item->area;
              $color = $item->color;
              $lugar = $item->unidades;
              $observaciones = $item->observaciones;
              $requisitos = $item->requsitos;
        }
        */
        $nombre_completo=$get_info->nombre.' '.$get_info->apll_paterno.' '.$get_info->apll_materno;
        
        $this->email->to($correo,$nombre_completo);
        
        $asunto='Inicie su Video Consulta';

      //Definimos el asunto del mensaje
        $this->email->subject($asunto);
         
      //Definimos el mensaje a enviar
      //$this->email->message($body)

        $message  = "<div style='background: url(http://estudiosunne.sicoi.net/public/img/anahuac-fondo-footer.jpg) #003166;width: 97%;height: 94%;padding: 20px;margin: 0px;'>
                      <div style='background: white; border: 1px solid #F8F8F8; border-radius: 15px; padding: 25px; width: 80%; margin-left: auto; margin-right: auto; position: absolute; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                      -moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                      box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);'>
                        <table>
                           <thead>
                            
                            <tr>
                              <th colspan='2' align='center'>
                                <div style='font-family: sans-serif; font-size:15px; color: #003166; -webkit-print-color-adjust: exact;'><h3>UNNE</h3></div> 
                              </th>
                            </tr>
                            <tr>
                              <th colspan='5' style='background-color: #00a5e1; color: #00a5e1; -webkit-print-color-adjust: exact;'>..</th>
                            </tr>
                            <tr>
                              <th>
                                <br>
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:13px;' align='left'>
                                Buen día, ".$get_info->nombre."
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:13px;' align='center'>
                                Sú médico ".$perosnaldatos->nombre." le ha generado una Video Consulta, Para iniciar dé click el siguiente botón:
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:13px; color: #003166; -webkit-print-color-adjust: exact;'>
                                <div align='center'>
                                <a href='".$direccion."' style='font-family: sans-serif; font-size:15px; color: #003166; -webkit-print-color-adjust: exact; background-color: #00a5e1; color: white; padding: 15px 32px; text-align: center;  text-decoration: none; display: inline-block; font-size: 16px; border-radius: 10px'>Comenzar Videollamada</a>
                                </div>
                              </th>
                            </tr>
                        </table><br>         
                      </div>
                  </div>";
                    

        $this->email->message($message);

        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
        //==================
    }
    public function datos_utilizar(){
        $id=$this->input->post('id');
        $che_acet=$this->input->post('che_acet');
        $data = array('check_acepta'=>$che_acet);
        $this->General_model->edit_record('idpaciente',$id,$data,'pacientes');
    }
    function cargafiles_doc(){
        $id=$this->input->post('id');
        $folder="doc_firma";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
          $array = array('doc'=>$newfile,'doc_firma'=>'','fecha_doc'=>$this->fechainicio);
          $this->General_model->edit_record('idpaciente',$id,$array,'pacientes');
          $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    } 
    public function buscardignostico(){
        $nom=$this->input->post('nombre');
        $html='';
        $result=$this->ModelCatalogos->buscar_diagnostico($nom);
        $aux=0;
        $aux_cont=0;
        foreach ($result as $item){
            $aux_cont=$aux_cont+1;
        }
        $etiqueta_s='';
        if($aux_cont>=9){
            $etiqueta_s='height: 300px; overflow-y: scroll;';
        }else{
            $etiqueta_s='';
        }
        if($nom!=''){
            $html.='<div class="alert-info  alert-rounded" style="'.$etiqueta_s.'">
                        <table style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
                            <tbody>';
                        foreach ($result as $item){
                        $html.='<tr>
                                    <td style="border: 1px solid #ddd;" class="max-texts"> <a class="btn diag_text'.$item->iddiagnostico.'" onclick="diagnostico_select('.$item->iddiagnostico.')">'.$item->diagnostico.'</a></td>
                                </tr>';
                        }
                    $html.='</tbody> 
                        </table>
                    </div>';
        }
        if($aux==0){
            $html.='';
        }   
        echo $html;
    }
    public function buscarlaboratorio(){
        $nom=$this->input->post('nombre');
        $html='';
        $result=$this->ModelCatalogos->buscar_laboratorio($nom);
        $aux=0;
        $aux_cont=0;
        foreach ($result as $item){
            $aux_cont=$aux_cont+1;
        }
        $etiqueta_s='';
        if($aux_cont>=9){
            $etiqueta_s='height: 300px; overflow-y: scroll;';
        }else{
            $etiqueta_s='';
        }
        if($nom!=''){
            $html.='<div class="alert-info  alert-rounded" style="'.$etiqueta_s.'">
                        <table style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
                            <tbody>';
                        foreach ($result as $item){
                        $html.='<tr>
                                    <td style="border: 1px solid #ddd;" class="max-texts"> <a  class="btn orden_laboratorio_'.$item->idorden.'" data-nombre_laboratorio="'.$item->nombre_laboratorio.'" data-direccion="'.$item->direccion.'" data-telefono="'.$item->telefono.'" onclick="laboratorio_select('.$item->idorden.')">'.$item->nombre_laboratorio.' / '.$item->direccion.'</a></td>
                                </tr>';
                        }
                    $html.='</tbody> 
                        </table>
                    </div>';
        }
        if($aux==0){
            $html.='';
        }   
        echo $html;
    }
    ///
    public function buscarmedicamento(){
        $nom=$this->input->post('nombre');
        $html='';
        $result=$this->ModelCatalogos->buscar_medicamento($nom);
        $aux=0;
        $aux_cont=0;
        foreach ($result as $item){
            $aux_cont=$aux_cont+1;
        }
        $etiqueta_s='';
        if($aux_cont>=9){
            $etiqueta_s='height: 300px; overflow-y: scroll;';
        }else{
            $etiqueta_s='';
        }
        if($nom!=''){
            $html.='<div class="alert-info  alert-rounded" style="'.$etiqueta_s.'">
                        <table style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
                            <tbody>';
                        foreach ($result as $item){
                        $html.='<tr>
                                    <td style="border: 1px solid #ddd;" class="max-texts"> <a class="btn" style="text-align: left;" onclick="medicamento_select('.$item->id.')"><span style="font-weight: bold;" class="medica_text'.$item->id.'">'.$item->nombre.'</span><br><span style="color:#027db400">______</span> '.$item->sustancia.' </a></td>
                                </tr>';
                        }
                    $html.='</tbody> 
                        </table>
                    </div>';
        }
        if($aux==0){
            $html.='';
        }   
        echo $html;
    }

    public function buscartomar(){
        $nom=$this->input->post('nombre');
        $html='';
        $result=$this->ModelCatalogos->buscar_tomar($nom);
        $aux=0;
        $aux_cont=0;
        foreach ($result as $item){
            $aux_cont=$aux_cont+1;
        }
        $etiqueta_s='';
        if($aux_cont>=9){
            $etiqueta_s='height: 300px; overflow-y: scroll;';
        }else{
            $etiqueta_s='';
        }
        if($nom!=''){
            $html.='<div class="alert-info  alert-rounded" style="'.$etiqueta_s.'">
                        <table style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
                            <tbody>';
                        foreach ($result as $item){
                        $html.='<tr>
                                    <td style="border: 1px solid #ddd;" class="max-texts"> <a class="btn" onclick="tomar_select('.$item->idmedicamento.')"><span style="font-weight: bold;" class="tomar_text'.$item->idmedicamento.'">'.$item->tomar.'</span></a></td>
                                </tr>';
                        }
                    $html.='</tbody> 
                        </table>
                    </div>';
        }
        if($aux==0){
            $html.='';
        }   
        echo $html;
    }

    public function buscaranalisis(){
        $nom=$this->input->post('nombre');
        $html='';
        $result=$this->ModelCatalogos->buscar_analisis($nom);
        $aux=0;
        $aux_cont=0;
        foreach ($result as $item){
            $aux_cont=$aux_cont+1;
        }
        $etiqueta_s='';
        if($aux_cont>=9){
            $etiqueta_s='height: 300px; overflow-y: scroll;';
        }else{
            $etiqueta_s='';
        }
        if($nom!=''){
            $html.='<div class="alert-info  alert-rounded" style="'.$etiqueta_s.'">
                        <table style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
                            <tbody>';
                        foreach ($result as $item){
                        $html.='<tr>
                                    <td style="border: 1px solid #ddd;" class="max-texts"> <a class="btn" onclick="analisis_select('.$item->id.')"><span style="font-weight: bold;" class="analisis_text'.$item->id.'">'.$item->nombre.'</span></a></td>
                                </tr>';
                        }
                    $html.='</tbody> 
                        </table>
                    </div>';
        }
        if($aux==0){
            $html.='';
        }   
        echo $html;
    }
        public function buscarmedico(){
        $nom=$this->input->post('nombre');
        $html='';
        $result=$this->ModelCatalogos->buscar_interconsulta_medico($nom);
        $aux=0;
        $aux_cont=0;
        foreach ($result as $item){
            $aux_cont=$aux_cont+1;
        }
        $etiqueta_s='';
        if($aux_cont>=9){
            $etiqueta_s='height: 300px; overflow-y: scroll;';
        }else{
            $etiqueta_s='';
        }
        if($nom!=''){
            $html.='<div class="alert-info  alert-rounded" style="'.$etiqueta_s.'">
                        <table style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
                            <tbody>';
                        foreach ($result as $item){
                        $html.='<tr>
                                    <td style="border: 1px solid #ddd;" class="max-texts"> <a class="btn medico_int_'.$item->id.'" data-medico="'.$item->nombre.'" data-especialidad="'.$item->especialidad.'" data-hospital="'.$item->hospital.'" data-no_consultorio="'.$item->no_consultorio.'" data-domicilio="'.$item->domicilio.'" data-telefono="'.$item->telefono.'" data-ciudad="'.$item->ciudad.'" onclick="medico_select('.$item->id.')"><span style="font-weight: bold;" class="medico_text'.$item->id.'">'.$item->nombre.'</span></a></td>
                                </tr>';
                        }
                    $html.='</tbody> 
                        </table>
                    </div>';
        }
        if($aux==0){
            $html.='';
        }   
        echo $html;
    }
    //
    public function get_consultas_paciente(){
        $idc=$this->input->post('id');
        $arrayinfo = array('idconsulta'=>$idc);
        $get_consultas=$this->General_model->getselectwhereall('consultas',$arrayinfo);
        $idpaciente=0;
        foreach ($get_consultas as $item){
            $idpaciente=$item->idpaciente;
        } 
        $arrayinfop = array('idpaciente'=>$idpaciente);
        $get_consultasp=$this->General_model->getselectwhereall('pacientes',$arrayinfop);
        $celular=0;
        foreach ($get_consultasp as $itemp){
            $celular=$itemp->celular;
        } 
        echo $celular;
    }

    public function imprimiresumen($id){
        $idc=intval($id);
        $get_infoc=$this->General_model->get_record('idconsulta',$idc,'consultas');
        $data['get_infoc']=$get_infoc;
        if($get_infoc->status==0){
            $style_color='';
            $importante="Importante";
        }else{
            $importante="No Importante";
            $style_color='background-color: #fff7b4;';
        }
        $data['idc']=$idc;
        $get_infop=$this->General_model->get_record('idpaciente',$get_infoc->idpaciente,'pacientes');
        $tiempo = strtotime($get_infop->fecha_nacimiento); 
        $ahora = time(); 
        $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
        $data['edad'] = floor($edad); 
        $data['paciente']=$get_infop->nombre.' '.$get_infop->apll_paterno.' '.$get_infop->apll_materno;
        $data['aux_existe']=0;
        $data['fecha_consulta_ultima']=$this->fechainicio;
        $arrayinfop = array('idpaciente'=>$get_infoc->idpaciente);
        $get_consultasp_desc=$this->General_model->getselectwhere_orden_desc('consultas',$arrayinfop,'consultafecha');
        foreach($get_consultasp_desc as $item){
            $data['aux_existe']=1;
            $data['fecha_consulta_ultima']=$item->consultafecha;
        }

        //$data['idconsultap']=$id;
        //$this->load->view('paciente/header');
        $this->load->view('Reportes/imprimir_resumenpdf',$data);
        //$this->load->view('paciente/imprimir_resumen',$data);
        //$this->load->view('paciente/print_formato');
        //$this->load->view('paciente/footer');

    }
    public function pdfinterconsulta($id){
        $idn=intval($id);
        //
        $arrayinfor = array('idinterconsulta'=>$idn);
        $get_intercon=$this->General_model->getselectwhereall('interconsulta',$arrayinfor);
        $idinterconsulta=0;
        $in_diagnostico='';
        $nombre_diagnostico='';
        $motivo_interconsulta='';
        $incluir_diagnostico=0;
        $idconsulta=0;
        foreach ($get_intercon as $item){
            $idinterconsulta=$item->idinterconsulta; 
            $idconsulta=$item->idconsulta; 
            $incluir_diagnostico=$item->incluir_diagnostico; 
            $nombre_diagnostico=$item->nombre_diagnostico; 
            $motivo_interconsulta=$item->motivo_interconsulta; 
        }
        $data['incluir_diagnostico']=$incluir_diagnostico;
        $data['nombre_diagnostico']=$nombre_diagnostico;
        $data['motivo_interconsulta']=$motivo_interconsulta;
        
        $arrayinfo = array('idconsulta'=>intval($idconsulta));
        $get_consultas=$this->General_model->getselectwhereall('consultas',$arrayinfo);
        $idpaciente=0;
        foreach ($get_consultas as $item){
            $idpaciente=$item->idpaciente;
            $data['consultafecha']=$item->consultafecha;
        } 

        $idpaciente=intval($idpaciente);
        //$data['consultafecha']=$this->fechainicio;
        $arrayinfop = array('idpaciente'=>$idpaciente);
        $pa=$this->General_model->get_record('idpaciente',$idpaciente,'pacientes');
        $tiempo = strtotime($pa->fecha_nacimiento); 
        $ahora = time(); 
        $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
        $edad = floor($edad);
        $data['paciente']=$pa;
        $data['edad']=$edad;

        $arrayinform = array('idinterconsulta'=>$idinterconsulta);
        $get_interconm=$this->General_model->getselectwhereall('interconsulta_medico',$arrayinform);
        //var_dump($idn);die;
        $idmedico=0;
        $medico=''; 
        $especialidad=''; 
        $hospital=''; 
        $no_consultorio=''; 
        $domicilio=''; 
        $telefono=''; 
        $ciudad=''; 
        foreach ($get_interconm as $item){
            $medico=$item->nombre; 
            $especialidad=$item->especialidad; 
            $hospital=$item->hospital; 
            $no_consultorio=$item->no_consultorio; 
            $domicilio=$item->domicilio; 
            $telefono=$item->telefono; 
            $ciudad=$item->ciudad;
        }
        $data['medico']=$medico; 
        $data['especialidad']=$especialidad; 
        $data['hospital']=$hospital; 
        $data['no_consultorio']=$no_consultorio; 
        $data['domicilio']=$domicilio; 
        $data['telefono']=$telefono; 
        $data['ciudad']=$ciudad; 
        $this->load->view('Reportes/interconsultapdf',$data);
    }
    //
    public function pdfhospitalizacion($id){
        $idn=intval($id);
        //
        $arrayinfor = array('idorden_hospital'=>$idn);
        $get_intercon=$this->General_model->getselectwhereall('orden_hospital',$arrayinfor);
        $detalles='';
        $idconsulta=0;
        foreach ($get_intercon as $item){
            $idconsulta=$item->idconsulta; 
            $detalles=$item->detalles; 
        }
        $data['detalles']=$detalles;
        $arrayinfo = array('idconsulta'=>intval($idconsulta));
        $get_consultas=$this->General_model->getselectwhereall('consultas',$arrayinfo);
        $idpaciente=0;
        foreach ($get_consultas as $item){
            $idpaciente=$item->idpaciente;
            $data['consultafecha']=$item->consultafecha;
        } 
        $idpaciente=intval($idpaciente);
        //$data['consultafecha']=$this->fechainicio;
        $pa=$this->General_model->get_record('idpaciente',$idpaciente,'pacientes');
        $tiempo = strtotime($pa->fecha_nacimiento); 
        $ahora = time(); 
        $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
        $data['edad'] = floor($edad); 
        $data['paciente']=$pa;
        $this->load->view('Reportes/hospitalizacionpdf',$data);
    }
    public function pdfcerticadomedico($id){
        $idn=intval($id);
        //
        $arrayinfor = array('idcertificado_medico'=>$idn);
        $get_intercon=$this->General_model->getselectwhereall('certificado_medico',$arrayinfor);
        $detalles='';
        foreach ($get_intercon as $item){
            $idconsulta=$item->idconsulta; 
            $detalles=$item->detalles; 
        }
        $data['detalles']=$detalles;
        $arrayinfo = array('idconsulta'=>intval($idconsulta));
        $get_consultas=$this->General_model->getselectwhereall('consultas',$arrayinfo);
        $idpaciente=0;
        foreach ($get_consultas as $item){
            $idpaciente=$item->idpaciente;
            $data['consultafecha']=$item->consultafecha;
        } 
        $idpaciente=intval($idpaciente);
        $pa=$this->General_model->get_record('idpaciente',$idpaciente,'pacientes');
        $tiempo = strtotime($pa->fecha_nacimiento); 
        $ahora = time(); 
        $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
        $data['edad'] = floor($edad);
        $data['paciente']=$pa;
        /*
        $data['consultafecha']=$this->fechainicio;

        $arrayinfop = array('idpaciente'=>$idpaciente);
        $get_consultasp=$this->General_model->getselectwhereall('pacientes',$arrayinfop);
        foreach ($get_consultasp as $itemp){
            $data['paciente']=$itemp->nombre.' '.$itemp->apll_paterno.' '.$itemp->apll_materno;
            $tiempo = strtotime($itemp->fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
            $data['edad'] = floor($edad); 
        } 
        */
        $this->load->view('Reportes/certificadomedicopdf',$data);
    }
    public function pdfjustificantemedico($id){
        $idn=intval($id);
        //
        $arrayinfor = array('idjustificante'=>$idn);
        $get_array=$this->General_model->getselectwhereall('orden_justificante_medico',$arrayinfor);
        $detalles='';
        foreach ($get_array as $item){
            $idconsulta=$item->idconsulta; 
            $detalles=$item->detalles; 
        }
        $data['detalles']=$detalles;
        $arrayinfo = array('idconsulta'=>intval($idconsulta));
        $get_consultas=$this->General_model->getselectwhereall('consultas',$arrayinfo);
        $idpaciente=0;
        foreach ($get_consultas as $item){
            $idpaciente=$item->idpaciente;
            $data['consultafecha']=$item->consultafecha;
        } 
        $idpaciente=intval($idpaciente);
        $pa=$this->General_model->get_record('idpaciente',$idpaciente,'pacientes');
        $tiempo = strtotime($pa->fecha_nacimiento); 
        $ahora = time(); 
        $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
        $data['edad'] = floor($edad);
        $data['paciente']=$pa;
        //$data['consultafecha']=$this->fechainicio;
        /*
        $arrayinfop = array('idpaciente'=>$idpaciente);
        $get_consultasp=$this->General_model->getselectwhereall('pacientes',$arrayinfop);
        foreach ($get_consultasp as $itemp){
            $data['paciente']=$itemp->nombre.' '.$itemp->apll_paterno.' '.$itemp->apll_materno;
            $tiempo = strtotime($itemp->fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
            $data['edad'] = floor($edad); 
        } 
        */

        $this->load->view('Reportes/justificadopdf',$data);
    }  

    public function pdforden_libre($id){
        $idn=intval($id);
        //
        $arrayinfor = array('idorden'=>$idn);
        $get_array=$this->General_model->getselectwhereall('orden_libre',$arrayinfor);
        $detalles='';
        foreach ($get_array as $item){
            $idconsulta=$item->idconsulta; 
            $detalles=$item->detalles; 
        }
        $data['detalles']=$detalles;
        $arrayinfo = array('idconsulta'=>intval($idconsulta));
        $get_consultas=$this->General_model->getselectwhereall('consultas',$arrayinfo);
        $idpaciente=0;
        foreach ($get_consultas as $item){
            $idpaciente=$item->idpaciente;
            $data['consultafecha']=$item->consultafecha;
        } 
        $idpaciente=intval($idpaciente);
        $idpaciente=intval($idpaciente);
        $pa=$this->General_model->get_record('idpaciente',$idpaciente,'pacientes');
        $tiempo = strtotime($pa->fecha_nacimiento); 
        $ahora = time(); 
        $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
        $data['edad'] = floor($edad);
        $data['paciente']=$pa;
        /*
        $data['consultafecha']=$this->fechainicio;
        $arrayinfop = array('idpaciente'=>$idpaciente);
        $get_consultasp=$this->General_model->getselectwhereall('pacientes',$arrayinfop);
        foreach ($get_consultasp as $itemp){
            $data['paciente']=$itemp->nombre.' '.$itemp->apll_paterno.' '.$itemp->apll_materno;
            $tiempo = strtotime($itemp->fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
            $data['edad'] = floor($edad); 
        } 
        */
        $this->load->view('Reportes/ordenlibrepdf',$data);
    }  

    public function imprimirexpediente($idpaciente){
        $get_infoconsultam=$this->ModelCatalogos->get_ultima_consulta_medica_estetica(intval($idpaciente));
        $status_btn=0;
        $idcm=0;
        foreach ($get_infoconsultam as $item){
            $idcm=$item->idconsulta;
        }
        $data['idc']=$idcm;
        ///=============
        $get_infoconsulspa=$this->ModelCatalogos->get_ultima_consulta_spa(intval($idpaciente));
        $idcspa=0;
        foreach ($get_infoconsulspa as $item){
            $idcspa=$item->idconsulta;
        }
        ///=============
        $get_infoconsulnutricion=$this->ModelCatalogos->get_ultima_consulta_nutricion(intval($idpaciente));
        $idcnutr=0;
        foreach ($get_infoconsulnutricion as $item){
            $idcnutr=$item->idconsulta;
        }
        ///=============
        //var_dump($data['get_infoc']);die;
        $data['get_infop']=$this->General_model->get_record('idpaciente',intval($idpaciente),'pacientes');
        $get_relevante_historia=$this->General_model->getselectwhere('relevante_historia','idpaciente',intval($idpaciente));
        $antecedentes='';
        $negadas='';
        $alergias='';
        $quien_refiere='';
        $motivo_consulta='';
        $observaciones='';
        foreach($get_relevante_historia as $item){
            $antecedentes=$item->antecedentes;
            $negadas=$item->negadas;
            $alergias=$item->alergias;
            $quien_refiere=$item->quien_refiere;
            $motivo_consulta=$item->motivo_consulta;
            $observaciones=$item->observaciones;
        }
        $data['antecedentes']=$antecedentes;
        $data['negadas']=$negadas;
        $data['alergias']=$alergias;
        $data['quien_refiere']=$quien_refiere;
        $data['motivo_consulta']=$motivo_consulta;
        $data['observaciones']=$observaciones;
        ////////////// Historial Clinico
        // Heredofamiliares (Abuelos, padres, hijos)                                                                        
            $diabetes='';
            $cancer='';
            $hipertencion='';
            $otro_antecedente='';
            //  A. Personales NO Patológicos                                   
            $chetpersonal_servicios='';
            //Alimentación
            $fruta='';
            $carnes_rojas='';
            $cerdos='';
            $pollo='';
            $lacteos='';
            $harina='';
            $aguas='';
            $chatarras='';
            // Alcohol
            $cuando_inicio_alcohol='';
            $cada_cuando_alcohol='';
            $cuantos_dia_alcohol='';
            // Tabaco
            $cuando_inicio_tabaco='';
            $cada_cuando_tabaco='';
            $cuantos_dia_tabaco='';
            // Drogas
            $cuando_inicio_drogas='';
            $cada_cuando_drogas='';
            $cuantos_dia_drogas='';
            $inmunizaciones='';
            $desparasitacion='';
            //  A. Ginecobstetricos                                             
            $marcar='';
            $mestrual='';
            $dismenorrea='';
            $menstruacion='';

            $sexual='';
            $parejas='';
            $embarazos='';
            $abortos='';

            $cesareas='';
            $lactancia='';
            $anticonceptivos='';
            $ets='';

            $menopausia='';
            $climaterio='';
            $papanicolau='';
            $mastografia='';
            /// A. Personales Patológicos                                                        
            $medicamentos_deprecion='';
            $cirugia='';
            $otras_enfemedades='';
            $transfusiones='';
            $hospitalizacion='';

            $alergias='';
            $fracturas='';
            $embarazo_lactancia='';

            $chehepatitisa='';
            $chehepatitisb='';
            $chehepatitisc='';
            $hepatitis='';

            $diabetesmillitus='';
            $hipertensionarterial='';
            $hipercolesterolemia='';
            $epilepsia='';
            $enfermedades_musculares='';
            // Interrogatorio por Aparatos y Sistemas
            $interrogatorio_aparatos='';
            // Medicamentos                                                          
            $medicamento='';

        $arrayinfohc = array('idpaciente'=>$idpaciente);
        $get_historia_clinica=$this->General_model->getselectwhereall('historia_clinica',$arrayinfohc);
        foreach ($get_historia_clinica as $item){
            // Heredofamiliares (Abuelos, padres, hijos)                                                                        
            $diabetes=$item->diabetes;
            $cancer=$item->cancer;
            $hipertencion=$item->hipertencion;
            $otro_antecedente=$item->otro_antecedente;
            //  A. Personales NO Patológicos                                   
            $chetpersonal_servicios=$item->chetpersonal_servicios;
            //Alimentación
            $fruta=$item->fruta;
            $carnes_rojas=$item->carnes_rojas;
            $cerdos=$item->cerdos;
            $pollo=$item->pollo;
            $lacteos=$item->lacteos;
            $harina=$item->harina;
            $aguas=$item->aguas;
            $chatarras=$item->chatarras;
            // Alcohol
            $cuando_inicio_alcohol=$item->cuando_inicio_alcohol;
            $cada_cuando_alcohol=$item->cada_cuando_alcohol;
            $cuantos_dia_alcohol=$item->cuantos_dia_alcohol;
            // Tabaco
            $cuando_inicio_tabaco=$item->cuando_inicio_tabaco;
            $cada_cuando_tabaco=$item->cada_cuando_tabaco;
            $cuantos_dia_tabaco=$item->cuantos_dia_tabaco;
            // Drogas
            $cuando_inicio_drogas=$item->cuando_inicio_drogas;
            $cada_cuando_drogas=$item->cada_cuando_drogas;
            $cuantos_dia_drogas=$item->cuantos_dia_drogas;
            $inmunizaciones=$item->inmunizaciones;
            $desparasitacion=$item->desparasitacion;
            //  A. Ginecobstetricos                                             
            $marcar=$item->marcar;
            $mestrual=$item->mestrual;
            $dismenorrea=$item->dismenorrea;
            $menstruacion=$item->menstruacion;

            $sexual=$item->sexual;
            $parejas=$item->parejas;
            $embarazos=$item->embarazos;
            $abortos=$item->abortos;

            $cesareas=$item->cesareas;
            $lactancia=$item->lactancia;
            $anticonceptivos=$item->anticonceptivos;
            $ets=$item->ets;

            $menopausia=$item->menopausia;
            $climaterio=$item->climaterio;
            $papanicolau=$item->papanicolau;
            $mastografia=$item->mastografia;
            /// A. Personales Patológicos                                                        
            $medicamentos_deprecion=$item->medicamentos_deprecion;
            $cirugia=$item->cirugia;
            $otras_enfemedades=$item->otras_enfemedades;
            $transfusiones=$item->transfusiones;
            $hospitalizacion=$item->hospitalizacion;

            $alergias=$item->alergias;
            $fracturas=$item->fracturas;
            $embarazo_lactancia=$item->embarazo_lactancia;

            $chehepatitisa=$item->chehepatitisa;
            $chehepatitisb=$item->chehepatitisb;
            $chehepatitisc=$item->chehepatitisc;
            $hepatitis=$item->hepatitis;

            $diabetesmillitus=$item->diabetesmillitus;
            $hipertensionarterial=$item->hipertensionarterial;
            $hipercolesterolemia=$item->hipercolesterolemia;
            $epilepsia=$item->epilepsia;
            $enfermedades_musculares=$item->enfermedades_musculares;
            // Interrogatorio por Aparatos y Sistemas
            $interrogatorio_aparatos=$item->interrogatorio_aparatos;
            // Medicamentos                                                          
            $medicamento=$item->medicamento;

        }
        /////////////////////================
        //((( Interrogatorio por sistemas )))
            // Sintomas generales
            $arrayinfoc = array('idpaciente'=>intval($idpaciente));
            $gets1sintomas_generales=$this->General_model->getselectwhereall('s1sintomas_generales',$arrayinfoc);
            $data['idsintomas']=0;
            $data['fiebre']='';
            $data['astenia']='';
            $data['aumento']='';
            $data['modificacion']='';
            foreach ($gets1sintomas_generales as $item){
                $data['idsintomas']=$item->idsintomas;
                $data['fiebre']=$item->fiebre;
                $data['astenia']=$item->astenia;
                $data['aumento']=$item->aumento;
                $data['modificacion']=$item->modificacion; 
            } 
            // Aparato respiratorio
            $gets2aparato_respiratorio=$this->General_model->getselectwhereall('s2aparato_respiratorio',$arrayinfoc);
            $data['idaparato']=0;
            $data['rinorrea']='';
            $data['epistaxis']='';
            $data['tos']='';
            $data['expectoracion']='';
            $data['disfonia']='';
            $data['hemoptitis']='';
            $data['cianosis']='';
            $data['dolor']='';
            $data['disnea']='';
            $data['sibilancias']='';
            foreach ($gets2aparato_respiratorio as $item){
                $data['idaparato']=$item->idaparato;
                $data['rinorrea']=$item->rinorrea;
                $data['epistaxis']=$item->epistaxis;
                $data['tos']=$item->tos;
                $data['expectoracion']=$item->expectoracion;
                $data['disfonia']=$item->disfonia;
                $data['hemoptitis']=$item->hemoptitis;
                $data['cianosis']=$item->cianosis;
                $data['dolor']=$item->dolor;
                $data['disnea']=$item->disnea;
                $data['sibilancias']=$item->sibilancias;
            } 
            // Digestivo
            $gets3digestivo=$this->General_model->getselectwhereall('s3digestivo',$arrayinfoc);
            $data['iddigestivo3']=0;
            $data['hambre3']='';
            $data['apetito3']='';
            $data['masticacion3']='';
            $data['disfagia3']='';
            $data['halitosis3']='';
            $data['nausea3']='';
            $data['rumiacion3']='';
            $data['pirosis3']='';
            $data['aerofagia3']='';
            $data['eructos3']='';
            $data['meteorismo3']='';
            $data['distension3']='';
            $data['gases3']='';
            $data['hematemesis3']='';
            $data['ictericia3']='';
            $data['fecales3']='';
            $data['constipacion3']='';
            $data['haces_blancas3']='';
            $data['verdes3']='';
            $data['haces_negras3']='';
            $data['amarillas3']='';
            $data['rojas3']='';
            $data['esteatorrea_aceitosas3']='';
            $data['parsitos3']='';
            $data['pujo3']='';
            $data['lienteria3']='';
            $data['tenesmo3']='';    
            $data['prurito3']='';
            foreach ($gets3digestivo as $item){
                $data['iddigestivo3']=0;
                $data['hambre3']=$item->hambre;
                $data['apetito3']=$item->apetito;
                $data['masticacion3']=$item->masticacion;
                $data['disfagia3']=$item->disfagia;
                $data['halitosis3']=$item->halitosis;
                $data['nausea3']=$item->nausea;
                $data['rumiacion3']=$item->rumiacion;
                $data['pirosis3']=$item->pirosis;
                $data['aerofagia3']=$item->aerofagia;
                $data['eructos3']=$item->eructos;
                $data['meteorismo3']=$item->meteorismo;
                $data['distension3']=$item->distension;
                $data['gases3']=$item->gases;
                $data['hematemesis3']=$item->hematemesis;
                $data['ictericia3']=$item->ictericia;
                $data['fecales3']=$item->fecales;
                $data['constipacion3']=$item->constipacion;
                $data['haces_blancas3']=$item->haces_blancas;
                $data['verdes3']=$item->verdes;
                $data['haces_negras3']=$item->haces_negras;
                $data['amarillas3']=$item->amarillas;
                $data['rojas3']=$item->rojas;
                $data['esteatorrea_aceitosas3']=$item->esteatorrea_aceitosas;
                $data['parsitos3']=$item->parsitos;
                $data['pujo3']=$item->pujo;
                $data['lienteria3']=$item->lienteria;
                $data['tenesmo3']=$item->tenesmo;
                $data['prurito3']=$item->prurito;
            } 
            // Cardio vascular
            $gets4cardiovascular=$this->General_model->getselectwhereall('s4cardiovascular',$arrayinfoc);
            $data['idcardio4']=0;
            $data['palpitaciones4']='';
            $data['dolor_precordial4']='';
            $data['disnea_esfuerzo4']='';
            $data['disnea_paroxistica4']='';
            $data['apnea4']='';
            $data['cianosis4']='';
            $data['acufenos4']='';
            $data['fosfenos4']='';
            $data['sincope4']='';
            $data['lipotimias4']='';
            $data['edema4']='';
            foreach ($gets4cardiovascular as $item){
                $data['idcardio4']=$item->idcardio;
                $data['palpitaciones4']=$item->palpitaciones;
                $data['dolor_precordial4']=$item->dolor_precordial;
                $data['disnea_esfuerzo4']=$item->disnea_esfuerzo;
                $data['disnea_paroxistica4']=$item->disnea_paroxistica;
                $data['apnea4']=$item->apnea;
                $data['cianosis4']=$item->cianosis;
                $data['acufenos4']=$item->acufenos;
                $data['fosfenos4']=$item->fosfenos;
                $data['sincope4']=$item->sincope;
                $data['lipotimias4']=$item->lipotimias;
                $data['edema4']=$item->edema;
            } 
            // Renal y urinario
            $gets5renalurinario=$this->General_model->getselectwhereall('s5renalurinario',$arrayinfoc);
            $data['idrenal5']=0;
            $data['renoureteral5']='';
            $data['disuria5']='';
            $data['anuria5']='';
            $data['oliguria5']='';
            $data['poliuria5']='';
            $data['polaquiuria5']='';
            $data['hematuria5']='';
            $data['piuria5']='';
            $data['coluria5']='';
            $data['incontinencia5']='';
            $data['edema5']='';
            foreach ($gets5renalurinario as $item){
                $data['idrenal5']=$item->idrenal;
                $data['renoureteral5']=$item->renoureteral;
                $data['disuria5']=$item->disuria;
                $data['anuria5']=$item->anuria;
                $data['oliguria5']=$item->oliguria;
                $data['poliuria5']=$item->poliuria;
                $data['polaquiuria5']=$item->polaquiuria;
                $data['hematuria5']=$item->hematuria;
                $data['piuria5']=$item->piuria;
                $data['coluria5']=$item->coluria;
                $data['incontinencia5']=$item->incontinencia;
                $data['edema5']=$item->edema;
            } 
            // Genital femenino
            $gets6genital_femenino=$this->General_model->getselectwhereall('s6genital_femenino',$arrayinfoc);
            $data['idgenital6']=0;
            $data['leucorrea6']='';
            $data['hemorragias6']='';
            $data['alteraciones_menstruales6']='';
            $data['alteracioneslibido6']='';
            $data['practica_sexual6']='';
            $data['alteraciones_sangrado6']='';
            $data['dispareunia6']='';
            $data['perturbaciones6']='';
            foreach ($gets6genital_femenino as $item){
                $data['idgenital6']=$item->idgenital;
                $data['leucorrea6']=$item->leucorrea;
                $data['hemorragias6']=$item->hemorragias;
                $data['alteraciones_menstruales6']=$item->alteraciones_menstruales;
                $data['alteracioneslibido6']=$item->alteracioneslibido;
                $data['practica_sexual6']=$item->practica_sexual;
                $data['alteraciones_sangrado6']=$item->alteraciones_sangrado;
                $data['dispareunia6']=$item->dispareunia;
                $data['perturbaciones6']=$item->perturbaciones;
            } 
            // Endocrino
            $gets7endocrino=$this->General_model->getselectwhereall('s7endocrino',$arrayinfoc);
            $data['idendocrino7']=0;
            $data['intolerancia_frio7']='';
            $data['hiperactividad7']='';
            $data['aumento_volumen7']='';
            $data['polidipsia7']='';
            $data['polifagia7']='';
            $data['poliuria7']='';
            $data['cambios_caracteres7']='';
            $data['aumento_perdida7']='';
            foreach ($gets7endocrino as $item){
                $data['idendocrino7']=$item->idendocrino;
                $data['intolerancia_frio7']=$item->intolerancia_frio;
                $data['hiperactividad7']=$item->hiperactividad;
                $data['aumento_volumen7']=$item->aumento_volumen;
                $data['polidipsia7']=$item->polidipsia;
                $data['polifagia7']=$item->polifagia;
                $data['poliuria7']=$item->poliuria;
                $data['cambios_caracteres7']=$item->cambios_caracteres;
                $data['aumento_perdida7']=$item->aumento_perdida;
            } 
            // Hematopoyetico
            $gets8hematopoyetico=$this->General_model->getselectwhereall('s8hematopoyetico',$arrayinfoc);
            $data['idhematopoyetico8']=0;
            $data['palidez8']='';
            $data['disnea8']='';
            $data['fatigabilidad8']='';
            $data['palpitaciones8']='';
            $data['sangrado8']='';
            $data['equimosis8']='';
            $data['petequia8']='';
            $data['astenia8']='';
            foreach ($gets8hematopoyetico as $item){
                $data['idhematopoyetico8']=$item->idhematopoyetico;
                $data['palidez8']=$item->palidez;
                $data['disnea8']=$item->disnea;
                $data['fatigabilidad8']=$item->fatigabilidad;
                $data['palpitaciones8']=$item->palpitaciones;
                $data['sangrado8']=$item->sangrado;
                $data['equimosis8']=$item->equimosis;
                $data['petequia8']=$item->petequia;
                $data['astenia8']=$item->astenia;
            } 
            // Musculo esqueletico
            $gets9musculo_esqueletico=$this->General_model->getselectwhereall('s9musculo_esqueletico',$arrayinfoc);
            $data['idmusculo9']=0;
            $data['mialgias9']='';
            $data['dolor_oseo9']='';
            $data['artralgias9']='';
            $data['alteraciones9']='';
            $data['disminucion_volumen9']='';
            $data['limitacion_movimiento9']='';
            $data['deformacion9']='';
            foreach ($gets9musculo_esqueletico as $item){
                $data['idmusculo9']=$item->idmusculo;
                $data['mialgias9']=$item->mialgias;
                $data['dolor_oseo9']=$item->dolor_oseo;
                $data['artralgias9']=$item->artralgias;
                $data['alteraciones9']=$item->alteraciones;
                $data['disminucion_volumen9']=$item->disminucion_volumen;
                $data['limitacion_movimiento9']=$item->limitacion_movimiento;
                $data['deformacion9']=$item->deformacion;
            } 
            // Nervioso
            $gets10nervioso=$this->General_model->getselectwhereall('s10nervioso',$arrayinfoc);
            $data['idnervioso10']=0;
            $data['cefaleas10']='';
            $data['paresias10']='';
            $data['parestesias10']='';
            $data['movimientos_anormales10']='';
            $data['alteraciones_marcha10']='';
            $data['vertigo10']='';
            $data['mareos10']='';
            foreach ($gets10nervioso as $item){
                $data['idnervioso10']=$item->idnervioso;
                $data['cefaleas10']=$item->cefaleas;
                $data['paresias10']=$item->paresias;
                $data['parestesias10']=$item->parestesias;
                $data['movimientos_anormales10']=$item->movimientos_anormales;
                $data['alteraciones_marcha10']=$item->alteraciones_marcha;
                $data['vertigo10']=$item->vertigo;
                $data['mareos10']=$item->mareos;
            } 
            // Organos de los sentidos
            $gets11organos_sentidos=$this->General_model->getselectwhereall('s11organos_sentidos',$arrayinfoc);
            $data['idorganos11']=0;
            $data['alteraciones_vision11']='';
            $data['audicion11']='';
            $data['olfato11']='';
            $data['gusto11']='';
            $data['tacto11']='';
            $data['mareos11']='';
            $data['sensaciones11']='';
            foreach ($gets11organos_sentidos as $item){
                $data['idorganos11']=$item->idorganos;
                $data['alteraciones_vision11']=$item->alteraciones_vision;
                $data['audicion11']=$item->audicion;
                $data['olfato11']=$item->olfato;
                $data['gusto11']=$item->gusto;
                $data['tacto11']=$item->tacto;
                $data['mareos11']=$item->mareos;
                $data['sensaciones11']=$item->sensaciones;
            } 

            // Esfera psiquica
            $gets12esfera_psiquica=$this->General_model->getselectwhereall('s12esfera_psiquica',$arrayinfoc);
            $data['idesfera12']=0;
            $data['tristeza12']='';
            $data['euforia12']='';
            $data['alteraciones_sueno12']='';
            $data['terrores_nocturnos12']='';
            $data['ideaciones12']='';
            $data['miedo_exagerado12']='';
            $data['irritabilidad12']='';
            $data['apatia12']='';
            $data['relaciones_personales12']='';
            foreach ($gets12esfera_psiquica as $item){
                $data['idesfera12']=$item->idesfera;
                $data['tristeza12']=$item->tristeza;
                $data['euforia12']=$item->euforia;
                $data['alteraciones_sueno12']=$item->alteraciones_sueno;
                $data['terrores_nocturnos12']=$item->terrores_nocturnos;
                $data['ideaciones12']=$item->ideaciones;
                $data['miedo_exagerado12']=$item->miedo_exagerado;
                $data['irritabilidad12']=$item->irritabilidad;
                $data['apatia12']=$item->apatia;
                $data['relaciones_personales12']=$item->relaciones_personales;
            } 
            // Tegumentario
            $gets13tegumentario=$this->General_model->getselectwhereall('s13tegumentario',$arrayinfoc);
            $data['idtegumentario13']=0;
            $data['coloracion_anormal13']='';
            $data['pigmentacion13']='';
            $data['prurito13']='';
            $data['caracteristicas13']='';
            $data['unas13']='';
            $data['hiperhidrosis13']='';
            $data['xerodermia13']='';
            foreach ($gets13tegumentario as $item){
                $data['idtegumentario13']=$item->idtegumentario;
                $data['coloracion_anormal13']=$item->coloracion_anormal;
                $data['pigmentacion13']=$item->pigmentacion;
                $data['prurito13']=$item->prurito;
                $data['caracteristicas13']=$item->caracteristicas;
                $data['unas13']=$item->unas;
                $data['hiperhidrosis13']=$item->hiperhidrosis;
                $data['xerodermia13']=$item->xerodermia;
            }
            /////////////////////================

            // Heredofamiliares (Abuelos, padres, hijos)                                                                        
            $data['diabetes']=$diabetes;
            $data['cancer']=$cancer;
            $data['hipertencion']=$hipertencion;
            $data['otro_antecedente']=$otro_antecedente;
            //  A. Personales NO Patológicos                                  
            $data['chetpersonal_servicios']=$chetpersonal_servicios;
            //Alimentación
            $data['fruta']=$fruta;
            $data['carnes_rojas']=$carnes_rojas;
            $data['cerdos']=$cerdos;
            $data['pollo']=$pollo;
            $data['lacteos']=$lacteos;
            $data['harina']=$harina;
            $data['aguas']=$aguas;
            $data['chatarras']=$chatarras;
            // Alcohol
            $data['cuando_inicio_alcohol']=$cuando_inicio_alcohol;
            $data['cada_cuando_alcohol']=$cada_cuando_alcohol;
            $data['cuantos_dia_alcohol']=$cuantos_dia_alcohol;
            // Tabaco
            $data['cuando_inicio_tabaco']=$cuando_inicio_tabaco;
            $data['cada_cuando_tabaco']=$cada_cuando_tabaco;
            $data['cuantos_dia_tabaco']=$cuantos_dia_tabaco;
            // Drogas
            $data['cuando_inicio_drogas']=$cuando_inicio_drogas;
            $data['cada_cuando_drogas']=$cada_cuando_drogas;
            $data['cuantos_dia_drogas']=$cuantos_dia_drogas;
            $data['inmunizaciones']=$inmunizaciones;
            $data['desparasitacion']=$desparasitacion;
            //  A. Ginecobstetricos                                            
            $data['marcar']=$marcar;
            $data['mestrual']=$mestrual;
            $data['dismenorrea']=$dismenorrea;
            $data['menstruacion']=$menstruacion;
            $data['sexual']=$sexual;
            $data['parejas']=$parejas;
            $data['embarazos']=$embarazos;
            $data['abortos']=$abortos;
            $data['cesareas']=$cesareas;
            $data['lactancia']=$lactancia;
            $data['anticonceptivos']=$anticonceptivos;
            $data['ets']=$ets;
            $data['menopausia']=$menopausia;
            $data['climaterio']=$climaterio;
            $data['papanicolau']=$papanicolau;
            $data['mastografia']=$mastografia;
            /// A. Personales Patológicos                                                    
            $data['medicamentos_deprecion']=$medicamentos_deprecion;
            $data['cirugia']=$cirugia;
            $data['otras_enfemedades']=$otras_enfemedades;
            $data['transfusiones']=$transfusiones;
            $data['hospitalizacion']=$hospitalizacion;
            $data['alergias']=$alergias;
            $data['fracturas']=$fracturas;
            $data['embarazo_lactancia']=$embarazo_lactancia;
            $data['chehepatitisa']=$chehepatitisa;
            $data['chehepatitisb']=$chehepatitisb;
            $data['chehepatitisc']=$chehepatitisc;
            $data['hepatitis']=$hepatitis;
            $data['diabetesmillitus']=$diabetesmillitus;
            $data['hipertensionarterial']=$hipertensionarterial;
            $data['hipercolesterolemia']=$hipercolesterolemia;
            $data['epilepsia']=$epilepsia;
            $data['enfermedades_musculares']=$enfermedades_musculares;
            // Interrogatorio por Aparatos y Sist']emas
            $data['interrogatorio_aparatos']=$interrogatorio_aparatos;
            // Medicamentos                                                        
            $data['medicamento']=$medicamento;
        /////////////////////////////////////////////////////////
        $data['get_m']=$this->General_model->get_record('idconsulta',intval($idcm),'consulta_medicina_estetica');
        $diagn=$this->General_model->get_records_condition('idconsulta='.$idcm.' AND activo=1','consulta_medicina_estetica_diagnostico');
        $trata=$this->General_model->get_records_condition('idconsulta='.$idcm.' AND activo=1','consulta_medicina_estetica_tratamiento');
        $servi=$this->ModelCatalogos->get_venta_servicio_c1($idcm);
        $produc=$this->ModelCatalogos->get_venta_productoc1($idcm);
        $data['diagn']=$diagn;
        $data['trata']=$trata;
        $data['servi']=$servi;
        $data['produc']=$produc;
        /// 
        $data['get_spa']=$this->General_model->get_record('idconsulta',intval($idcspa),'consulta_spa');
        $serv_spa=$this->ModelCatalogos->get_venta_servicio_c2($idcspa);
        $produc_spa=$this->ModelCatalogos->get_venta_productoc2($idcspa);
        $data['serv_spa']=$serv_spa;
        $data['produc_spa']=$produc_spa;
        /// nutrición
        $data['resultlu']=$this->General_model->getselectwhereall2('plan_alimenticion_lunes',array('consultaId'=>$idcnutr,'activo'=>1));
        $data['resultma']=$this->General_model->getselectwhereall2('plan_alimenticion_martes',array('consultaId'=>$idcnutr,'activo'=>1));
        $data['resultmi']=$this->General_model->getselectwhereall2('plan_alimenticion_miercoles',array('consultaId'=>$idcnutr,'activo'=>1));
        $data['resultju']=$this->General_model->getselectwhereall2('plan_alimenticion_jueves',array('consultaId'=>$idcnutr,'activo'=>1));
        $data['resultvi']=$this->General_model->getselectwhereall2('plan_alimenticion_viernes',array('consultaId'=>$idcnutr,'activo'=>1));
        $resultconsulta=$this->General_model->getselectwhereall2('consulta_nutricion',array('idconsulta'=>$idcnutr,'activo'=>1));
        $resultconsulta=$resultconsulta->result();
        $resultconsulta=$resultconsulta[0];
        $data['resultconsulta']=$resultconsulta;
        $serv_nutr=$this->ModelCatalogos->get_venta_servicio_c3($idcnutr);
        $produc_nutr=$this->ModelCatalogos->get_venta_productoc3($idcnutr);
        $data['serv_nutr']=$serv_nutr;
        $data['produc_nutr']=$produc_nutr;
        ///
        $this->load->view('Reportes/imprimir_expedientepdf',$data);
    }

    public function registrosistemas1(){
        $data=$this->input->post();
        $idsintomas=$data['idsintomas'];
        if($idsintomas==0){
            unset($data['idsintomas']);
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('s1sintomas_generales',$data);
        }else{
            $id=$this->General_model->edit_record('idsintomas',$idsintomas,$data,'s1sintomas_generales');
            $id=$idsintomas;
        }
        echo $id;
    }
    public function registrosistemas2(){
        $data=$this->input->post();
        $idaparato=$data['idaparato'];
        if($idaparato==0){
            unset($data['idaparato']);
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('s2aparato_respiratorio',$data);
        }else{
            $id=$this->General_model->edit_record('idaparato',$idaparato,$data,'s2aparato_respiratorio');
            $id=$idaparato;
        }
        echo $id;
    }
    public function registrosistemas3(){
        $data=$this->input->post();
        $iddigestivo=$data['iddigestivo'];
        if($iddigestivo==0){
            unset($data['iddigestivo']);
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('s3digestivo',$data);
        }else{
            $id=$this->General_model->edit_record('iddigestivo',$iddigestivo,$data,'s3digestivo');
            $id=$iddigestivo;
        }
        echo $id;
    }
    public function registrosistemas4(){
        $data=$this->input->post();
        $idcardio=$data['idcardio'];
        if($idcardio==0){
            unset($data['idcardio']);
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('s4cardiovascular',$data);
        }else{
            $id=$this->General_model->edit_record('idcardio',$idcardio,$data,'s4cardiovascular');
            $id=$idcardio;
        }
        echo $id;
    }
    public function registrosistemas5(){
        $data=$this->input->post();
        $idrenal=$data['idrenal'];
        if($idrenal==0){
            unset($data['idrenal']);
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('s5renalurinario',$data);
        }else{
            $id=$this->General_model->edit_record('idrenal',$idrenal,$data,'s5renalurinario');
            $id=$idrenal;
        }
        echo $id;
    }
    public function registrosistemas6(){
        $data=$this->input->post();
        $idgenital=$data['idgenital'];
        if($idgenital==0){
            unset($data['idgenital']);
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('s6genital_femenino',$data);
        }else{
            $id=$this->General_model->edit_record('idgenital',$idgenital,$data,'s6genital_femenino');
            $id=$idgenital;
        }
        echo $id;
    }
    public function registrosistemas7(){
        $data=$this->input->post();
        $idendocrino=$data['idendocrino'];
        if($idendocrino==0){
            unset($data['idendocrino']);
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('s7endocrino',$data);
        }else{
            $id=$this->General_model->edit_record('idendocrino',$idendocrino,$data,'s7endocrino');
            $id=$idendocrino;
        }
        echo $id;
    }
    public function registrosistemas8(){
        $data=$this->input->post();
        $idhematopoyetico=$data['idhematopoyetico'];
        if($idhematopoyetico==0){
            unset($data['idhematopoyetico']);
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('s8hematopoyetico',$data);
        }else{
            $id=$this->General_model->edit_record('idhematopoyetico',$idhematopoyetico,$data,'s8hematopoyetico');
            $id=$idhematopoyetico;
        }
        echo $id;
    }
    public function registrosistemas9(){
        $data=$this->input->post();
        $idmusculo=$data['idmusculo'];
        if($idmusculo==0){
            unset($data['idmusculo']);
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('s9musculo_esqueletico',$data);
        }else{
            $id=$this->General_model->edit_record('idmusculo',$idmusculo,$data,'s9musculo_esqueletico');
            $id=$idmusculo;
        }
        echo $id;
    }
    public function registrosistemas10(){
        $data=$this->input->post();
        $idnervioso=$data['idnervioso'];
        if($idnervioso==0){
            unset($data['idnervioso']);
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('s10nervioso',$data);
        }else{
            $id=$this->General_model->edit_record('idnervioso',$idnervioso,$data,'s10nervioso');
            $id=$idnervioso;
        }
        echo $id;
    }
    public function registrosistemas11(){
        $data=$this->input->post();
        $idorganos=$data['idorganos'];
        if($idorganos==0){
            unset($data['idorganos']);
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('s11organos_sentidos',$data);
        }else{
            $id=$this->General_model->edit_record('idorganos',$idorganos,$data,'s11organos_sentidos');
            $id=$idorganos;
        }
        echo $id;
    }
    public function registrosistemas12(){
        $data=$this->input->post();
        $idesfera=$data['idesfera'];
        if($idesfera==0){
            unset($data['idesfera']);
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('s12esfera_psiquica',$data);
        }else{
            $id=$this->General_model->edit_record('idesfera',$idesfera,$data,'s12esfera_psiquica');
            $id=$idesfera;
        }
        echo $id;
    }
    public function registrosistemas13(){
        $data=$this->input->post();
        $idtegumentario=$data['idtegumentario'];
        if($idtegumentario==0){
            unset($data['idtegumentario']);
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('s13tegumentario',$data);
        }else{
            $id=$this->General_model->edit_record('idtegumentario',$idtegumentario,$data,'s13tegumentario');
            $id=$idtegumentario;
        }
        echo $id;
    }
    ///===== ((( General Consulta ))) ===
    public function delete_consulta(){
        $data=$this->input->post();
        $id=$data['id'];
        $tabla=$data['tabla'];
        $data = array('activo'=>0);
        $this->General_model->edit_record('idconsulta',intval($id),$data,$tabla);
    }
    public function restaura_consulta(){
        $data=$this->input->post();
        $id=$data['id'];
        $tabla=$data['tabla'];
        $data = array('activo'=>1);
        $this->General_model->edit_record('idconsulta',$id,$data,$tabla);
    }
    public function consulta_importante(){
        $data=$this->input->post();
        $id=$data['id'];
        $estatus=$data['estatus'];
        $tabla=$data['tabla'];
        if($estatus==0){
           $data = array('estatus'=>1);
        }else{
           $data = array('estatus'=>0);
        }
        $this->General_model->edit_record('idconsulta',$id,$data,$tabla);
    }
    ///======================((Medicina estetica))====================
    function get_diagnostico_estetica(){
        $id = $this->input->post('id');
        $arrayinfo = array('idconsulta' => $id,'activo'=>1);
        $results=$this->General_model->getselectwhereall('consulta_medicina_estetica_diagnostico',$arrayinfo);
        echo json_encode($results);
    }
    function get_tratamiento_estetica(){
        $id = $this->input->post('id');
        $arrayinfo = array('idconsulta' => $id,'activo'=>1);
        $results=$this->General_model->getselectwhereall('consulta_medicina_estetica_tratamiento',$arrayinfo);
        echo json_encode($results);
    }
    public function delete_diagnostico_estetica(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('iddiagnostico',$id,$data,'consulta_medicina_estetica_diagnostico');
    }
    public function delete_tratamiento_estetica(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idtratamiento',$id,$data,'consulta_medicina_estetica_tratamiento');
    }
    
    function tipo_consulta(){
        $id=$this->input->post('id');
        $idp=$this->input->post('idp');
        ////
        $titulo_consul='';
        if($id==1){
            $titulo_consul='Medicina estética';
        }else if($id==2){
            $titulo_consul='SPA';
        }else if($id==3){
            $titulo_consul='Nutrición';
        }
        $html='<div class="row">
                    <div class="col-md-6">
                        <h3>'.$titulo_consul.'</h3>
                        <br>
                    </div>
                    <div class="col-md-6" align="right">
                        <button type="button" class="btn waves-effect waves-light btn-info" onclick="ver_consultas_tipo()">Regresar a consultas</button>
                    </div>
                </div>';
        ////
        $arrayinfo = array('idpaciente'=>$idp,'activo'=>1);
        $import=''; 
        if($id==1){
            $list_consultas=$this->General_model->getselectwhere_orden_desc('consulta_medicina_estetica',$arrayinfo,'consultafecha');
            $icon='<span> <i class="fas fa-syringe"></i> </span>';         
        }else if($id==2){
            $list_consultas=$this->General_model->getselectwhere_orden_desc('consulta_spa',$arrayinfo,'consultafecha');
            $icon='<span> <i class="fab fa-envira"></i> </span>';         
        }else if($id==3){
            $list_consultas=$this->General_model->getselectwhere_orden_desc('consulta_nutricion',$arrayinfo,'consultafecha');
            $icon='<span> <i class="fab fa-apple"></i> </span>';         
        }
        $html.='<div class="row">';
                foreach ($list_consultas as $item){
                    if($item->estatus==1){
                        $import='style="color: #346c94 !important; border-color: #346c94 !important; background-color:#fff7b4;"'; 
                    }else{
                        $import='style="color: #346c94 !important; border-color: #346c94 !important;"'; 
                    }
                $html.='<div class="col-md-3">
                            <button type="button" class="btn btn-rounded btn-block btn-xs" '.$import.' onclick="servicios_consulta('.$item->idconsulta.','.$id.')">'.$icon.'  '.date('d/m/Y',strtotime($item->consultafecha)).'</button><br>
                        </div>';
                }
        $html.='</div>';
        echo $html;
    }
    public function tipo_servicios(){
        $id=$this->input->post('id');
        $tipo=$this->input->post('tipo');
        if($tipo==1){
            //$titulo_consul='Medicina estética';
            $result_consul=$this->ModelCatalogos->get_venta_servicio_c1($id);
        }else if($tipo==2){
            $result_consul=$this->ModelCatalogos->get_venta_servicio_c2($id);
            //$titulo_consul='SPA';
        }else if($tipo==3){
            $result_consul=$this->ModelCatalogos->get_venta_servicio_c3($id);
            //$titulo_consul='Nutrición';
        }
        $html='<div class="cuidado_especial"> 
                    <div>';
        foreach ($result_consul as $item){
        $html.='<div class="row">
                    <div class="col-md-4">
                        <h5>Servicio</h5> 
                        <h5>'.$item->servicio.'</h5>
                    </div>
                    <div class="col-md-8">
                        <h5>Descripción</h5> 
                        <h5>'.$item->descripcion.'</h5>
                    </div>
                </div>
                <div class="row row_col">
                    <input type="hidden" id="idservicio_c" value="'.$item->idservicio.'">
                    <input type="hidden" id="idconsulta_c" value="'.$id.'">
                    <input type="hidden" id="tipo_c" value="'.$tipo.'">
                    <input type="hidden" id="iddetalles_c" value="'.$item->iddetalles.'"> 
                    <div class="col-md-12">
                        <div class="form-group">
                          <label>Cuidados especiales: </label>
                          <textarea type="text" rows="6" class="form-control border-primary cuidados_especiales" id="cuidados_especiales">'.$item->cuidados_especiales.'</textarea>
                        </div>
                    </div>
                </div>
                <hr>';
        }
        $html.='</div>
            </div>';
        $html.='<div class="row">
                    <div class="col-md-12" align="center">
                        <button type="button" class="btn waves-effect waves-light btn-info" onclick="guardar_servicios('.$id.','.$tipo.')">Guardar</button>
                    </div>
                </div>';
        echo $html;
    }
    public function registro_cuidados_especiales(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idservicio = $DATA[$i]->idservicio;
            $idconsulta = $DATA[$i]->idconsulta;
            $tipo = $DATA[$i]->tipo;
            $iddetalles = $DATA[$i]->iddetalles;
            $data['cuidados_especiales']=$DATA[$i]->cuidados_especiales;  
            $this->General_model->edit_record('idservicio',$idservicio,$data,'servicios');
            if($tipo==1){
                $this->General_model->edit_record('iddetalles',$iddetalles,$data,'servicio_venta_detalles');
            }else if($tipo==2){
                $this->General_model->edit_record('iddetalles',$iddetalles,$data,'consulta_spa_servicio_venta_detalles');
            }else if($tipo==3){
                $this->General_model->edit_record('iddetalles',$iddetalles,$data,'consulta_nutricion_servicio_venta_detalles');
            }
        }
    }


    public function prueba_correo(){
        $this->load->view('paciente/prueba.php');
    }
    function add_img_grafica(){
      $id = $this->input->post('id');
      $datax = $this->input->post();
      $base64Image=$datax['grafica'];
      $namefile=date('Y_m_d-h_i_s').'png';

      $imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $base64Image));

      file_put_contents('uploads/graficas_nutricion/'.$namefile, $imagenBinaria);


      //file_put_contents('uploads/graficas_nutricion/'.$namefile.'.txt',$base64Image);
      $datos = array('grafica'=>$namefile);
      $this->General_model->edit_record('idpaciente',$id,$datos,'pacientes');
      //echo $base64Image;
    }

    //// funcion para guardar archivos
    function cargafiles_doc_legal(){
        $id=$this->input->post('id');
        $tipo=$this->input->post('tipo');
        $folder="documentos_legal";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $array = array('idpaciente'=>$id,'nombre'=>$newfile,'fecha'=>$this->fechainicio,'tipo'=>$tipo);
            $this->General_model->add_record('documentos_legales',$array);
            $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    } 
    public function eliminar_doc_legal(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('iddocumento',$id,$data,'documentos_legales');
    }
    function guardar_tipo_aviso(){
      $id = $this->input->post('id');
      $tipo_aviso = $this->input->post('tipo_aviso');
      $datax = $this->input->post();
      //$equipo=$datax['equipo'];
      //$idserie=$datax['idserie']; 
      $base64Image=$datax['firma'];
      //$comentario=$datax['comentario']; 
      //$tipo=$datax['tipo'];
      $namefile=date('Y_m_d-h_i_s');
      file_put_contents('uploads/paciente_firma/'.$namefile.'.txt',$base64Image);
      
      $datos = array('idpaciente'=>$id,'firma'=>$namefile.'.txt','tipo'=>$tipo_aviso,'fecha'=>$this->fechainicio);
      $id=$this->General_model->add_record('documentos_legales',$datos);
    }

    function guardar_tipo_aviso_doc(){
      $id = $this->input->post('id');
      $tipo_aviso = $this->input->post('tipo_aviso');
      $datax = $this->input->post();

      $base64Image=$datax['firma'];
      $namefile=date('Y_m_d-h_i_s');
      file_put_contents('uploads/paciente_firma/'.$namefile.'.txt',$base64Image);

      $base64Image2=$datax['firma_doc'];
      $namefile2=date('Y_m_d-h_i_s');
      file_put_contents('uploads/paciente_firma_doc/'.$namefile2.'.txt',$base64Image2);
      
      $datos = array('idpaciente'=>$id,'firma'=>$namefile.'.txt','firma_medico'=>$namefile2.'.txt','tipo'=>$tipo_aviso,'fecha'=>$this->fechainicio);
      $id=$this->General_model->add_record('documentos_legales',$datos);
    }

}

