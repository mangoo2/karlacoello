<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->perfilid=$this->session->userdata('perfilid');
  }
	public function index(){
    $id_pefil=$this->perfilid;
    $id_pefil_aux=0;
    if($id_pefil!=0){
      $id_pefil_aux=$this->perfilid;
    }else{
      $id_pefil_aux=0; 
    }
    $resconfig=$this->General_model->get_records_menu($id_pefil_aux);
    $pagina='Inicio';
    foreach ($resconfig as $item) {
      $pagina=$item->Pagina;
    }
 
		if (!isset($_SESSION['perfilid'])) {
                  $perfilview=0;
                  redirect('/Login');
        }else{
            $perfilview=$_SESSION['perfilid'];
        }
       	if ($perfilview>=1) {
        	redirect('/'.$pagina);
        }else{
        	redirect('/Login');
        }
	}
}
