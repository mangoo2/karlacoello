<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }
    }
	public function index()
	{
        $arraypaciente = array('activo'=>1);
        $list_paciente=$this->General_model->getselectwhereall('pacientes',$arraypaciente);
        $data['pacientes']=$list_paciente;
        $data['fecha_reciente']=$this->fecha_reciente;
        //////////////////////////////////
        //Agenda
        $data['efectivo']=$this->ModelCatalogos->getmonto_efectivo($this->fecha_reciente);
        $data['tarjeta']=$this->ModelCatalogos->getmonto_tarjeta($this->fecha_reciente);
        $data['monto_otros']=$this->ModelCatalogos->getmonto_otros($this->fecha_reciente);
        $data['total_total']=$data['efectivo']+$data['tarjeta']+$data['monto_otros'];
        $consulta_primera=$this->ModelCatalogos->getmonto_consulta_primera($this->fecha_reciente);
        $consulta_sub=$this->ModelCatalogos->getmonto_consulta_sub($this->fecha_reciente);
        $data['monto_consulta']=$consulta_primera+$consulta_sub;
        $data['hlaboral']=$this->General_model->getselectwhereall('config_horario_laboral',array('status'=>1));
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
		$this->load->view('agenda/agenda',$data);
		$this->load->view('templates/footer');
        $this->load->view('agenda/agendajs');
	}
    public function tipo_cita_text(){
        $nu=$this->input->post('tipo');
        $idcita=$this->input->post('idc');
        $idp=$this->input->post('idp');
        $perosnaldatos=$this->General_model->get_record('personalId',$this->idpersonal,'personal');  
        $html='';
        $titilo_p1='';
        $etiquetaocultar='';
        if($idcita==0){
            $etiquetaocultar='display:block';
        }else{
            
            $etiquetaocultar='display:none';
        }
        if($nu==1 || $nu==2){
            if($nu==1){
                $titilo_p1='Paciente de primera vez ';
            }else{
                $titilo_p1='Paciente subsecuente ';
            }
            $html.='<div class="card ">
                        <div class="card_borde  bg-info">
                            <div class="row">
                                <div class="col-md-10 margen_centro">
                                    <h4 class="m-b-0 text-white">'.$titilo_p1.'</h4>
                                </div>
                                <div class="col-md-2" align="right">
                                    <button type="button" class="btn waves-effect waves-light btn-sm btn-outline-danger" onclick="mostra_calendario()"><i class="fas fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" style="box-shadow: 0px 0px 30px #cccccc;">';
                        if($nu==2){   
                    $html.='<div align=center><b>Paciente subsecuente</b></div>';   
                            if($idcita==0){
                                if($idp==0){
                        $html.='<div class="form-group row">
                                    <div class="input-group mb-12">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="ti-search"></i></span>
                                        </div>
                                        <input type="text" id="buscar_nombre_txt" class="form-control" placeholder="Buscar paciente" aria-label="Username" aria-describedby="basic-addon1" oninput="buscar_paciente()">
                                    </div>
                                </div>
                                <div style="position: relative;">
                                    <div class="lista_empleado" style="position: absolute; top: 25px; z-index: 3; width:270px;">
                                    </div> 
                                </div>';
                                }
                            } 
                        }else{
                            $html.='<div align=center><b>Paciente de primera vez</b></div>';       
                        }
                        
                            $idcitar=0;
                            $nombrer='';
                            $apll_paternor='';
                            $apll_maternor='';
                            $celularr='';
                            $idpacienter=0;
                            $fecha_consultar='';
                            $hora_der='';
                            $hora_hastar='';
                            $consulr='';
                            $motivo_consultar='';
                            $colorr='';
                            $quiencorespondar='';
                            $motivo_consultar='';
                            $listaesperar='';
                            $listaesperar_check='';
                            $idconsultorior='';
                            $prioridadr=0;
                            $cita_confirmadar=0;
                            $paciente_llegor=0;
                            $paciente_no_llegor=0;
                            $cita_confirmadar_check='';
                            $paciente_llegor_check='';
                            $paciente_no_llegor_check='';
                            $correor='';
                            $tipo_consulta='';
                            $resultcita=$this->ModelCatalogos->getcita($idcita);
                            foreach ($resultcita as $item){
                                $idcitar=$item->idcita;
                                $nombrer=$item->nombre;
                                $apll_paternor=$item->apll_paterno;
                                $apll_maternor=$item->apll_materno;
                                $celularr=$item->celular;
                                $idpacienter=$item->idpaciente;
                                $fecha_consultar=$item->fecha_consulta;
                                $hora_der=$item->hora_de;
                                $hora_hastar=$item->hora_hasta;
                                $consulr=$item->consul;
                                $motivo_consultar=$item->motivo_consulta; 
                                $colorr=$item->color;
                                $quiencorespondar=$item->quiencoresponda; 
                                $motivo_consultar=$item->motivo_consulta;
                                $listaesperar=$item->listaespera;
                                $idconsultorior=$item->idconsultorio;
                                $prioridadr=$item->prioridad;
                                $cita_confirmadar=$item->cita_confirmada;
                                $paciente_llegor=$item->paciente_llego;
                                $paciente_no_llegor=$item->paciente_no_llego;
                                $correor=$item->correo;
                                $correor2=$item->correo2;
                                $tipo_consulta=$item->tipo_consulta;
                            } 
                            if($listaesperar==1){
                                $listaesperar_check='checked';
                                $etiqueta_listaespera='display:block';
                            }else{
                                $listaesperar_check='';
                                $etiqueta_listaespera='display:none';
                            }
                            // estatus de cita
                            if($cita_confirmadar==1){
                                $cita_confirmadar_check='checked';
                            }else{
                                $cita_confirmadar_check='';
                            }
                            if($paciente_llegor==1){
                                $paciente_llegor_check='checked';
                            }else{
                                $paciente_llegor_check='';
                            }
                            if($paciente_no_llegor==1){
                                $paciente_no_llegor_check='checked';
                            }else{
                                $paciente_no_llegor_check='';
                            }
                            if($idcita!=0){
                            $html.='<div>
                                    <h5><i class="fas fa-user"></i> '.$nombrer.' '.$apll_paternor.' '.$apll_maternor.'</h5>';
                                    if($celularr!=0){
                                        $html.='<h6><i class="fas fa-mobile-alt"></i> '.$celularr.'</h6>';    
                                    }
                            $html.='</div>
                                    <div align="center">
                                        <button type="button" class="btn waves-effect waves-light btn-outline-info" onclick="verexpediente('.$idpacienter.')">Ver Expediente</button>
                                        <button type="button" class="btn waves-effect waves-light btn-outline-info correo_paciente" data-correop="'.$correor.'" data-correop2="'.$correor2.'" onclick="modal_reenviarformulario('.$idcitar.')">Reenviar cita</button>
                                    </div>
                                <div class="paciente_texto_tipo1">    
                                    <hr>
                                    <h6><i class="fas fa-calendar"></i> Fecha de la cita: '.date('d/m/Y',strtotime($fecha_consultar)).'</h6>
                                    <h6><i class="fas fa-clock"></i> Hora de la cita: '.$hora_der.'</h6>';
                                    if($consulr!=''){
                                        $html.='<h6><i class="fas fa-map-marker-alt"></i> '.$consulr.'</h6>';    
                                    }
                            $html.='<h6><i class="fas fa-stethoscope"></i> Motivo de la consulta: '.$motivo_consultar.'</h6>';
                                    if($tipo_consulta==1){
                                        $txt_c1='Medicina estética';
                                    }else if($tipo_consulta==2){
                                        $txt_c1='SPA';
                                    }else if($tipo_consulta==3){
                                        $txt_c1='Nutrición';
                                    }else{
                                        $txt_c1='';
                                    }
                                        

                            $html.='<h6>Consulta: '.$txt_c1.'</h6>
                                    <hr>';

                                    $result_lada=$this->General_model->get_table('lada'); 
                            $selectmex=521;       
                            $html.='<select class="form-control" id="lada">';
                                    foreach ($result_lada as $item_l){
                                        if($item_l->id==$selectmex){
                                    $html.='<option value="'.$item_l->id.'" selected>'.$item_l->nombre.'</option>';            
                                        }else{
                                    $html.='<option value="'.$item_l->id.'">'.$item_l->nombre.'</option>';
                                        }
                                    }
                            $html.='</select>'; 
                            $html.='<button target="_blank" class="btn waves-effect waves-light btn-success info_pas" data-category="WhatsApp" data-event="click" data-label="Enviar recordatorio" data-celular_w="'.$celularr.'" data-pernombre_w="'.$perosnaldatos->nombre.'" data-fecha_w="'.$fecha_consultar.'" data-hora_w="'.$hora_der.'" onclick="enviar_whatsapp()"><i class="fab fa-whatsapp"></i> Confirmar vía WhatsApp</button>
                                <br>
                                <br>'; 
                            $html.='<form class="form" method="post" role="form" id="form_cita_check">';
                            $html.='    <input type="hidden" name="idcita" value="'.$idcitar.'">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" name="cita_confirmada" '.$cita_confirmadar_check.' class="custom-control-input" id="cita_confirmada" onclick="cita_status1()">
                                            <label class="custom-control-label" for="cita_confirmada"><span style="background-color: yellow; color:yellow">__</span> Cita confirmada</label>
                                        </div>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" name="paciente_llego" '.$paciente_llegor_check.' class="custom-control-input" id="paciente_llego" onclick="cita_status2()">
                                            <label class="custom-control-label" for="paciente_llego"><span style="background-color:#28e652; color:#28e652">__</span> El paciente llegó</label>
                                        </div>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" name="paciente_no_llego" '.$paciente_no_llegor_check.' class="custom-control-input" id="paciente_no_llego" onclick="cita_status3()">
                                            <label class="custom-control-label" for="paciente_no_llego"><span style="background-color:#8C77FE; color:#8C77FE">__</span> El paciente NO llegó</label>
                                        </div>
                                    ';
                            $html.='</form>
                                    <hr>
                                    <div align="center">
                                        <button type="button" class="btn waves-effect waves-light btn-outline-danger" onclick="modal_cancelar('.$idcita.')">X Cancelar Cita</button>
                                        <button type="button" class="btn waves-effect waves-light btn-outline-info" onclick="mostrarpacientetipo1()">Editar Cita</button>
                                    </div>
                                </div>';
                            }
                        if($idp==0){      
                    $html.='<form class="form" method="post" role="form" id="form_paciente">
                                <input type="hidden" name="idpaciente" value="0">
                                ';
                        $html.='<div style="'.$etiquetaocultar.'">  
                                    <div class="form-group row">
                                        <label class="col-sm-5 control-label"><span style="color: red;">*</span> Nombre(s)</label>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <input type="text" name="nombre" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-5 control-label"><span style="color: red;">*</span> Apellido Paterno</label>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <input type="text" name="apll_paterno" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-5 control-label"><span style="color: red;">*</span> Apellido Materno</label>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <input type="text" name="apll_materno" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-5 control-label">Fecha de nacimiento</label>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <input type="date" name="fecha_nacimiento" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-5 control-label"><span style="color: red;">*</span> Sexo</label>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <select name="sexo" class="form-control">
                                                    <option disabled="" selected="" value="0">Seleccione</option>
                                                    <option value="1">Masculino</option>
                                                    <option value="2">Femenino</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 control-label"><span style="color: red;">*</span> Celular</label>
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <input type="number" name="celular" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                           <div class="btn_tel">
                                           <button type="button" class="btn btn-info btn-circle" onclick="mostrar_tel_c_o()">+</button>
                                           </div>  
                                        </div>    
                                    </div>
                                    <div class="tele_c_o" style="display: none">
                                        <div class="form-group row">
                                            <label class="col-sm-5 control-label">Casa</label>
                                            <div class="col-sm-7">
                                                <div class="input-group">
                                                    <input type="number" name="casa" class="form-control">
                                                </div>
                                            </div>  
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-5 control-label">Oficina</label>
                                            <div class="col-sm-7">
                                                <div class="input-group">
                                                    <input type="number" name="oficina" class="form-control">
                                                </div>
                                            </div>    
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-5 control-label">Correo Electrónico</label>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <input type="text" name="correo" id="correo_p" class="form-control">
                                            </div>
                                        </div>
                                    </div>';
                                    if($nu==1){ 
                                        $html.='<input type="hidden" name="tipo" value="1">';    
                                    }else{
                                        $html.='<input type="hidden" name="tipo" value="2">';       
                                    }
                         $html.='</div>
                            </form>';
                        }else{
                            $r_emple=$this->General_model->get_record('idpaciente',$idp,'pacientes');
                    $html.='<div>
                            <h5><i class="fas fa-user"></i> '.$r_emple->nombre.' '.$r_emple->apll_paterno.' '.$r_emple->apll_materno.'</h5>';
                            if($r_emple->celular!=0){
                                $html.='<h6><i class="fas fa-mobile-alt"></i> '.$r_emple->celular.'</h6>';    
                            }
                    $html.='<form class="form" method="post" role="form" id="form_paciente">
                                <input type="hidden" name="idpaciente" value="'.$r_emple->idpaciente.'">
                                <input type="hidden" name="nombre" value="'.$r_emple->nombre.'">
                                <input type="hidden" name="apll_paterno" value="'.$r_emple->apll_paterno.'">
                                <input type="hidden" name="apll_materno" value="'.$r_emple->apll_materno.'">
                                <input type="hidden" name="celular" value="'.$r_emple->celular.'">
                                <input type="hidden" id="correo_p" value="'.$r_emple->correo.'">
                            </form>';
                    $html.='<hr></div>';        
                        }
                    $html.='<form class="form" method="post" role="form" id="form_cita">
                                <input type="hidden" name="idcita" value="'.$idcitar.'">
                                <div class="paciente_texto_tipo1_e" style="'.$etiquetaocultar.'">  ';
                                    if($idcita!=0){
                                         $html.='<hr>';
                                    }
                                    if($nu==1){ 
                                        $html.='<input type="hidden" name="tipo_cita" value="1">';    
                                    }else{
                                        $html.='<input type="hidden" name="tipo_cita" value="2">';       
                                    }
                            $html.='<div class="form-group row">
                                        <label class="col-sm-12 control-label"><span style="color: red;">*</span> Fecha de consulta</label>
                                        <div class="col-sm-12">
                                            <div class="input-group">
                                                <input type="date" name="fecha_consulta" id="fecha_consulta_e" value="'.$fecha_consultar.'" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-12 control-label"><span style="color: red;">*</span> Horario de consulta</label>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 control-label text-right">De</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="time" name="hora_de" id="hora_de" value="'.$hora_der.'" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 control-label text-right">Hasta</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="time" name="hora_hasta" id="hora_hasta" value="'.$hora_hastar.'" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <label class="col-sm-6 control-label"><i class="fas fa-paint-brush"></i> Color</label>
                                        <div class="col-sm-3">
                                            <div class="input-group">
                                                <input type="color" name="color" value="'.$colorr.'" id="color_spec" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>';
                                    $tipo_consultas1='';
                                    $tipo_consultas2='';
                                    $tipo_consultas3='';
                                    if($tipo_consulta==1){
                                        $tipo_consultas1='selected';
                                    }else{
                                        $tipo_consultas1='';
                                    }
                                    if($tipo_consulta==2){
                                        $tipo_consultas2='selected';
                                    }else{
                                        $tipo_consultas2='';
                                    }
                                    if($tipo_consulta==3){
                                        $tipo_consultas3='selected';
                                    }else{
                                        $tipo_consultas3='';
                                    }
                                    $html.='<div class="form-group">
                                        <label>Tipo de consulta</label>
                                        <select name="tipo_consulta" class="form-control">
                                            <option value="1" '.$tipo_consultas1.'>Medicina estética</option>
                                            <option value="2" '.$tipo_consultas2.'>SPA</option>
                                            <option value="3" '.$tipo_consultas3.'>Nutrición</option>        
                                        </select>
                                    </div>';
                                    $arrayconsultorio = array('activo'=>1,'personalId'=>$this->idpersonal);
                                    $list_consultorio=$this->General_model->getselectwhereall('consultotrio',$arrayconsultorio);
                            $html.='<div class="form-group">
                                        <label>Consultorio</label>
                                        <select name="idconsultorio" class="form-control">';        
                                    foreach ($list_consultorio as $item){
                                        
                                        if($idconsultorior==$item->idconsultorio){
                                        $html.='<option value="'.$item->idconsultorio.'" selected>'.$item->nombre.'</option>';    
                                        }else{   
                                        $html.='<option value="'.$item->idconsultorio.'">'.$item->nombre.'</option>';    
                                        }
                                    }
                                $html.='</select>
                                    </div>
                                    <div class="form-group">
                                        <label><i class="fas fa-user-md"></i> Quien recomienda</label>
                                        <input type="text" name="quiencoresponda" value="'.$quiencorespondar.'" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label><i class="fas fa-stethoscope"></i> Motivo de consulta</label>
                                        <input type="text" name="motivo_consulta" value="'.$motivo_consultar.'" class="form-control">
                                    </div>
                                    <hr>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" name="listaespera" id="listaespera" onclick="listaespera_chek()" '.$listaesperar_check.' class="custom-control-input" id="listaespera">
                                        <label class="custom-control-label" for="listaespera">Lista de espera</label>
                                    </div>
                                    <div class="ocultar_lista_prioridad" style="'.$etiqueta_listaespera.'">
                                        <div class="form-group row">
                                            <label class="col-sm-6 control-label text-right">Prioridad</label>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <select name="prioridad" class="form-control">';
                                                       if($prioridadr==1){
                                                $html.='<option value="1" selected>Alta</option>
                                                        <option value="2">Mediana</option>
                                                        <option value="3">Baja</option>
                                                       ';
                                                       }else if($prioridadr==2){
                                                $html.='<option value="1">Alta</option>
                                                        <option value="2" selected>Mediana</option>
                                                        <option value="3">Baja</option>
                                                       ';
                                                       }else if($prioridadr==3){
                                                $html.='<option value="1">Alta</option>
                                                        <option value="2">Mediana</option>
                                                        <option value="3" selected>Baja</option>
                                                       ';  
                                                       }else{
                                                $html.='<option value="1">Alta</option>
                                                        <option value="2">Mediana</option>
                                                        <option value="3">Baja</option>';        
                                                       }
                                            $html.='</select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>    
                            </form>
                            <div class="paciente_texto_tipo1_e" align="center" style="'.$etiquetaocultar.'">';
                            if($idcita!=0){    
                                $html.='<button type="button" class="btn waves-effect waves-light btn-outline-danger" onclick="modal_cancelar('.$idcita.')">Cancelar Cita</button>
                                         <button type="button" class="btn waves-effect waves-light btn-info" onclick="guarda_citae()">Guardar</button>';
                            }else{  
                              $html.='<button type="button" class="btn waves-effect waves-light btn-outline-danger" onclick="modal_cancelar('.$idcita.')">Cancelar Cita</button> <button type="button" class="btn waves-effect waves-light btn-info" onclick="guarda_cita()">Agendar</button>';
                            }
                    $html.='</div>
                        </div>
                    </div>';
        }else if($nu==3){// Personal 
            $idcitar=0;
            $motivo_consultar='';
            $fecha_consultar='';
            $listaesperar=0;
            $hora_der='';
            $hora_hastar='';
            $colorr='';
            $listaesperar_check='';
            $resultcita=$this->ModelCatalogos->getcita345($idcita);// Personal
            foreach ($resultcita as $item){
                $idcitar=$item->idcita;
                $motivo_consultar=$item->motivo_consulta;
                $fecha_consultar=$item->fecha_consulta;
                $listaesperar=$item->listaespera;
                $hora_der=$item->hora_de;
                $hora_hastar=$item->hora_hasta;
                $colorr=$item->color;
            }
            if($listaesperar==1){
                $listaesperar_check='checked';
                $horario_consulta_ckeck='display:none';
            }else{
                $listaesperar_check='';
                $horario_consulta_ckeck='display:block';
            }
            $html.='<div class="card ">
                        <div class="card_borde  bg-info">
                            <div class="row">
                                <div class="col-md-10 margen_centro">
                                    <h4 class="m-b-0 text-white">Personal</h4>
                                </div>
                                <div class="col-md-2" align="right">
                                    <button type="button" class="btn waves-effect waves-light btn-sm btn-outline-danger" onclick="mostra_calendario()"><i class="fas fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" style="box-shadow: 0px 0px 30px #cccccc;">
                            <div align=center><b>Personal</b></div>
                            ';
                    $html.='<form class="form" method="post" role="form" id="form_personal">
                                <input type="hidden" name="idcita" value="'.$idcitar.'">
                                <div class="form-group row">
                                    <label class="col-sm-5 control-label">Motivo (opcional)</label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <input type="text" name="motivo_consulta" value="'.$motivo_consultar.'" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-5 control-label"><span style="color: red;">*</span> Fecha de inicio</label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <input type="date" name="fecha_consulta" id="fecha_consulta_e" value="'.$fecha_consultar.'" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="listaespera" id="listaespera3" '.$listaesperar_check.' onclick="listaesperar_check3()">
                                    <label class="custom-control-label" for="listaespera3">Todo el día</label>
                                </div>
                                <div class="horario_consulta_ckeck" style="'.$horario_consulta_ckeck.'">
                                    <div class="form-group row">
                                        <label class="col-sm-6 control-label"><span style="color: red;">*</span> Horario de consulta</label>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-6 control-label text-right">De</label>
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <input type="time" name="hora_de" id="hora_de" value="'.$hora_der.'" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-6 control-label text-right">Hasta</label>
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <input type="time" name="hora_hasta" id="hora_hasta" value="'.$hora_hastar.'" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-sm-9 control-label"><i class="fas fa-paint-brush"></i> Color</label>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="color" name="color" value="'.$colorr.'" id="color_spec" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="tipo_cita" value="3">
                                <hr>
                            </form>
                            <div align="center">
                                <button type="button" class="btn waves-effect waves-light btn-secondary" onclick="modal_cancelar('.$idcita.')">Cancelar Cita</button>
                                <button type="button" class="btn waves-effect waves-light btn-info" onclick="guardar_cita_tipo3()">Agendar</button>
                            </div>
                        </div>
                    </div>';
        }else if($nu==4){
            $idcitar=0;
            $motivo_consultar='';
            $fecha_consultar='';
            $fechafin='';
            $resultcita=$this->ModelCatalogos->getcita345($idcita);// Personal
            foreach ($resultcita as $item){
                $idcitar=$item->idcita;
                $motivo_consultar=$item->motivo_consulta;
                $fecha_consultar=$item->fecha_consulta;
                $fechafin=$item->fechafin;
            }
            $html.='<div class="card ">
                        <div class="card_borde  bg-info">
                            <div class="row">
                                <div class="col-md-10 margen_centro">
                                    <h4 class="m-b-0 text-white">Congreso</h4>
                                </div>
                                <div class="col-md-2" align="right">
                                    <button type="button" class="btn waves-effect waves-light btn-sm btn-outline-danger" onclick="mostra_calendario()"><i class="fas fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" style="box-shadow: 0px 0px 30px #cccccc;">
                            <div align=center><b>Congreso</b></div>';
                    $html.='<form class="form" method="post" role="form" id="form_congreso">
                                <input type="hidden" name="idcita" value="'.$idcitar.'">
                                <div class="form-group row">
                                    <label class="col-sm-5 control-label">Nombre del Congreso</label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <input type="text" name="motivo_consulta" value="'.$motivo_consultar.'" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-5 control-label"><span style="color: red;">*</span> Empieza el</label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <input type="date" name="fecha_consulta" id="fecha_consulta_e" value="'.$fecha_consultar.'" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-5 control-label"><span style="color: red;">*</span> Fecha fin</label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <input type="date" name="fechafin" value="'.$fechafin.'" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="tipo_cita" value="4">
                                <hr>
                            </form>
                            <div align="center">
                                <button type="button" class="btn waves-effect waves-light btn-secondary" onclick="modal_cancelar('.$idcita.')">Cancelar Cita</button>
                                <button type="button" class="btn waves-effect waves-light btn-info" onclick="guardar_cita_tipo4()">Agendar</button>
                            </div>
                        </div>
                    </div>';
        }else if($nu==5){
            $idcitar=0;
            $motivo_consultar='';
            $fecha_consultar='';
            $fechafin='';
            $resultcita=$this->ModelCatalogos->getcita345($idcita);// Personal
            foreach ($resultcita as $item){
                $idcitar=$item->idcita;
                $motivo_consultar=$item->motivo_consulta;
                $fecha_consultar=$item->fecha_consulta;
                $fechafin=$item->fechafin;
            }
            $html.='<div class="card ">
                        <div class="card_borde  bg-info">
                            <div class="row">
                                <div class="col-md-10 margen_centro">
                                    <h4 class="m-b-0 text-white">Vacaciones</h4>
                                </div>
                                <div class="col-md-2" align="right">
                                    <button type="button" class="btn waves-effect waves-light btn-sm btn-outline-danger" onclick="mostra_calendario()"><i class="fas fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" style="box-shadow: 0px 0px 30px #cccccc;">
                            <div align=center><b>Vacaciones</b></div>';
                    $html.='<form class="form" method="post" role="form" id="form_vacacional">
                                <input type="hidden" name="idcita" value="'.$idcitar.'">
                                <div class="form-group row">
                                    <label class="col-sm-5 control-label">Vacaciones (opcional)</label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <input type="text" name="motivo_consulta" value="'.$motivo_consultar.'" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-5 control-label"><span style="color: red;">*</span> Empieza el</label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <input type="date" name="fecha_consulta" id="fecha_consulta_e" value="'.$fecha_consultar.'" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-5 control-label"><span style="color: red;">*</span> Fecha fin</label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <input type="date" name="fechafin" value="'.$fechafin.'" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="tipo_cita" value="5">
                                <hr>
                            </form>
                            <div align="center">
                                <button type="button" class="btn waves-effect waves-light btn-secondary" onclick="modal_cancelar('.$idcita.')">Cancelar Cita</button>
                                <button type="button" class="btn waves-effect waves-light btn-info" onclick="guardar_cita_tipo5()">Agendar</button>
                            </div>
                        </div>
                    </div>';
        }

        echo $html;
    }
    public function registraPaciente(){
        $data=$this->input->post();
        $id=$data['idpaciente'];
        if($id==0){
            $data['reg']=$this->fechahoy;
            $data['personalId']=$this->idpersonal;
            $id=$this->General_model->add_record('pacientes',$data);
        }else{
            $this->General_model->edit_record('idpaciente',$id,$data,'pacientes');
        }
        echo $id;
    }
    public function registraCita(){
        $data=$this->input->post();
        $id=$data['idcita'];
        unset($data['idcita']);

        if(isset($data['listaespera'])){
            $data['listaespera']=1;
        }else{
            $data['listaespera']=0; 
        }
        if($id==0){
            $data['reg']=$this->fechahoy;
            $data['personalId']=$this->idpersonal;
            $id=$this->General_model->add_record('citas',$data);
        }else{
            $this->General_model->edit_record('idcita',$id,$data,'citas');
        }
        echo $id;
    }
    public function buscarPaciente(){
        $nom=$this->input->post('nombre');
        $html='';
        $result=$this->ModelCatalogos->buscar_paciente($nom,$this->idpersonal);
        $aux=0;
        if($nom!=''){
            foreach ($result as $item){
                $aux=1;
                $img='';
                if($item->foto!=''){
                    $img='<img src="'.base_url().'uploads/pacientes/'.$item->foto.'" width="30" class="img-circle" alt="img">'; 
                }else{
                    $img='<img src="'.base_url().'assets/images/icon/staff.png" width="30" class="img-circle" alt="img">';
                }
                $html.='<div class="alert alert-info  alert-rounded">'.$img.' 
                        <a href="'.base_url().'Agenda/?id='.$item->idpaciente.'" class="btn waves-effect waves-light btn-rounded btn-xs btn-secondary">Agendar cita</a>
                        <a href="'.base_url().'Pacientes/paciente/'.$item->idpaciente.'" class="btn waves-effect waves-light btn-rounded btn-xs btn-secondary">Ver expediente</a>
                        <br>'.$item->nombre.' '.$item->apll_paterno.' '.$item->apll_materno.'</div>';
            }
        }
        if($aux==0){
            $html.='';
        }
        if($nom==''){
            $html.='';
        }
        echo $html;
    }
    public function listcitas(){
       $data = $this->input->get();
       $inicio = date('Y-m-d',$data['start']);
       $fin = date('Y-m-d',$data['end']);
       $result = $this->ModelCatalogos->getListcitas($inicio,$fin);
       echo json_encode($result);
    }
    public function check_cita(){
        $data=$this->input->post();
        $id=$data['idcita'];
        unset($data['idcita']);
        if(isset($data['cita_confirmada'])){
            $data['cita_confirmada']=1;
        }else{
            $data['cita_confirmada']=0; 
        }
        if(isset($data['paciente_llego'])){
            $data['paciente_llego']=1;
        }else{
            $data['paciente_llego']=0; 
        }
        if(isset($data['paciente_no_llego'])){
            $data['paciente_no_llego']=1;
        }else{
            $data['paciente_no_llego']=0; 
        }
        $this->General_model->edit_record('idcita',$id,$data,'citas');
    }
    public function cancelar_cita(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idcita',$id,$data,'citas');
    }
    function enviar(){
        $idc=$this->input->post('id');
        $correo=$this->input->post('correo');
        $resultcita=$this->ModelCatalogos->getcita($idc);
        foreach ($resultcita as $item){
            $idcitar=$item->idcita;
            $nombrer=$item->nombre;
            $apll_paternor=$item->apll_paterno;
            $apll_maternor=$item->apll_materno;
            $celularr=$item->celular;
            $idpacienter=$item->idpaciente;
            $fecha_consultar=$item->fecha_consulta;
            $hora_der=$item->hora_de;
            $hora_hastar=$item->hora_hasta;
            $consulr=$item->consul;
            $motivo_consultar=$item->motivo_consulta; 
            $colorr=$item->color;
            $quiencorespondar=$item->quiencoresponda; 
            $motivo_consultar=$item->motivo_consulta;
            $listaesperar=$item->listaespera;
            $idconsultorior=$item->idconsultorio;
            $prioridadr=$item->prioridad;
            $cita_confirmadar=$item->cita_confirmada;
            $paciente_llegor=$item->paciente_llego;
            $paciente_no_llegor=$item->paciente_no_llego;
            $tipo_consulta=$item->tipo_consulta;
        } 
        $perosnaldatos=$this->General_model->get_record('personalId',$this->idpersonal,'personal');
        //================================
            //cargamos la libreria email
            $this->load->library('email');
            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='estudiosunne.sicoi.net'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'contacto@estudiosunne.sicoi.net';

            //Nuestra contraseña
            $config["smtp_pass"] = '_@fzi[%~5Kx)';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('contacto@estudiosunne.sicoi.net','Clínica Karla Coello');
            //$this->email->addBCC('contacto@estudiosunne.sicoi.net');
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
        /*
        $cita = $this->ModeloCatalogos->getCita($id);      
        foreach ($cita as $item) {
              $fecha = date('d-m-Y',strtotime($item->fecha));
              $hora = date('g:i A',strtotime($item->hora));  
              $paciente = $item->paciente; 
              $correo = $item->correo; 
              $tipo_paciente = $item->tipo_paciente;
              $medico_externo = $item->medico; 
              $estudio = $item->estudio;
              $area = $item->area;
              $color = $item->color;
              $lugar = $item->unidades;
              $observaciones = $item->observaciones;
              $requisitos = $item->requsitos;
        }
        */
        $nombre_completo=$nombrer.' '.$apll_paternor.' '.$apll_maternor;

        $this->email->to($correo,$nombre_completo);
        
        $asunto='Información necesaria para su próxima cita médica '.$perosnaldatos->nombre;

      //Definimos el asunto del mensaje
        $this->email->subject($asunto);
         
      //Definimos el mensaje a enviar
      //$this->email->message($body)
        /*
        <tr>
                                <td width='100%' align='center'>
                                  <a href='#' style='font-family: sans-serif; font-size:15px; color: #003166; -webkit-print-color-adjust: exact; background-color: #00a5e1; color: white; padding: 15px 32px; text-align: center;  text-decoration: none; display: inline-block; font-size: 16px; border-radius: 10px'>completar su cita aquí</a>
                                </td>
                              </tr>   
        */

        $message  = "<div style='background: url(http://estudiosunne.sicoi.net/public/img/anahuac-fondo-footer.jpg) #003166;width: 97%;height: 94%;padding: 20px;margin: 0px;'>
                        <div style='background: white; border: 1px solid #F8F8F8; border-radius: 15px; padding: 25px; width: 80%; margin-left: auto; margin-right: auto; position: absolute; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                                          -moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                                          box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);'>
                          <table width='100%'>
                            <tbody>
                              <tr>
                                <td width='100%' align='center'>
                                  <img width='200px' src='".base_url()."images/Logo.png'>
                                  <br>
                                   <h1 style='font: 250% sans-serif'> <img width='40px' src='http://unneeleonor.sicoi.net/public/img/centroneuro/mano2.png'> ".$nombre_completo."</h1>
                                </td>
                              </tr>
                              <tr>
                                <td width='100%'>
                                  <h3 style='font: 140% sans-serif'>Es un placer atenderte en tu próxima consulta, antes de acudir al consultorio, por favor completar la sigueinte información</h3>
                                </td>
                              </tr>  
                              <tr>
                                <td width='100%'>
                                  <h3 style='font: 140% sans-serif'>Su próxima consulta es:</h3>
                                </td>
                              </tr>    
                            </tbody>  
                          </table>
                          <table width='100%'>
                            <tbody>
                              <tr>
                                <td rowspan='6' style='background-color: #779155; color: #779155; -webkit-print-color-adjust: exact;'>..</td>
                              </tr>
                              <tr>
                                <td width='100%'>
                                  <h1 style='font: 130% sans-serif'> <img width='30px' src='http://unneeleonor.sicoi.net/public/img/centroneuro/calendario.png'> El día ".date('d/m/Y',strtotime($fecha_consultar))." a las ".$hora_der." hrs</h1>
                                </td>
                              </tr> 
                              <tr>
                                <td width='100%'>
                                  <h1 style='font: 130% sans-serif'> <img width='30px' src='http://unneeleonor.sicoi.net/public/img/centroneuro/casa.png'>Clínica Karla Coello</h1>
                                </td>
                              </tr>  
                              <tr>
                                <td width='100%'>
                                  <h1 style='font: 130% sans-serif'> <img width='30px' src='http://unneeleonor.sicoi.net/public/img/centroneuro/casa.png'> Con dirección 27 A norte 1019 Colonia San Alejandro 72090 Puebla de Zaragoza, México
Clínica de Rejuvenecimiento Integral - Karla Coello</h1>
                                </td>
                              </tr>  
                              <tr>
                                <td width='100%'>";
                                    if($tipo_consulta==1){
                                        $txt_c1='Medicina estética';
                                    }else if($tipo_consulta==2){
                                        $txt_c1='SPA';
                                    }else if($tipo_consulta==3){
                                        $txt_c1='Nutrición';
                                    }else{
                                        $txt_c1='';
                                    }
                                    $message.="<h1 style='font: 130% sans-serif'>Consulta: ".$txt_c1."</h1>";
                                $message.="</td>
                              </tr> 
                              <tr>
                                <td width='100%'>
                                  <h1 style='font: 130% sans-serif'>- Te sugerimos presentarte con 15 minutos de anticipación a tu consulta</h1>
                                </td>
                              </tr>  
                            </tbody>  
                          </table>
                        </div>
                    </div>";
                    
        /*
        <div style='background: url(http://estudiosunne.sicoi.net/public/img/anahuac-fondo-footer.jpg) #003166;width: 97%;height: 94%;padding: 20px;margin: 0px;'>
                      <div style='background: white; border: 1px solid #F8F8F8; border-radius: 15px; padding: 25px; width: 80%; margin-left: auto; margin-right: auto; position: absolute; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                      -moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                      box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);'>
                        <table>
                           <thead>
                            
                            <tr>
                              <th colspan='2' align='center'>
                                <div style='font-family: sans-serif; font-size:15px; color: #003166; -webkit-print-color-adjust: exact;'><h3>UNNE</h3></div> 
                              </th>
                            </tr>
                            <tr>
                              <th colspan='5' style='background-color: #00a5e1; color: #00a5e1; -webkit-print-color-adjust: exact;'>..</th>
                            </tr>
                            <tr>
                              <th>
                                <br>
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:13px;' align='left'>
                                Buen día, ".$nombrer."
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:13px;' align='left'>
                                Es un gusto saber que pronto estará en consulta con nosotros. Antes de acudir al consultorio, le invitamos a completar la siguiente información que nos será de gran utilidad para otorgarle una mejor atención.
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:13px; color: #003166; -webkit-print-color-adjust: exact;' align='left'>
                                <div align='center'>
                                <a href='#' style='font-family: sans-serif; font-size:15px; color: #003166; -webkit-print-color-adjust: exact; background-color: #00a5e1; color: white; padding: 15px 32px; text-align: center;  text-decoration: none; display: inline-block; font-size: 16px; border-radius: 10px'>completar su cita aquí</a>
                                </div>
                              </th>
                            </tr>
                        </table> <br>
                        <div style='background: white; border: 1px solid #F8F8F8; border-radius: 5px; padding: 10px; width: 80%; margin-left: auto; margin-right: auto; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                  -moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                  box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);'>
                                <b>Su próxima consulta es</b><hr>
                                <p>Fecha: <span style='font: 90% sans-serif;'>".date('d/m/Y',strtotime($fecha_consultar))."</span><br>
                                   Hora : <span style='font: 90% sans-serif;'>".$hora_der." hrs. </span><br>
                                   En el consultorio: <span style='font: 90% sans-serif;'> 1, en ".$consulr." </span><br>
                                   Con dirección: <span style='font: 90% sans-serif;'> ".$perosnaldatos->domicilio." </span></p>
                        </div>          
                      </div>
                  </div>
        */
        $this->email->message($message);

        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
        //==================
    }
    public function enviodatos(){
        $message  = "<div style='background: url(http://estudiosunne.sicoi.net/public/img/anahuac-fondo-footer.jpg) #003166;width: 97%;height: 94%;padding: 20px;margin: 0px;'>
                      <div style='background: white; border: 1px solid #F8F8F8; border-radius: 15px; padding: 25px; width: 80%; margin-left: auto; margin-right: auto; position: absolute; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                      -moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                      box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);'>
                        <table>
                           <thead>
                            
                            <tr>
                              <th colspan='2' align='center'>
                                <div style='font-family: sans-serif; font-size:15px; color: #003166; -webkit-print-color-adjust: exact;'> UNNE</div> 
                              </th>
                            </tr>
                            <tr>
                              <th colspan='5' style='background-color: #00a5e1; color: #00a5e1; -webkit-print-color-adjust: exact;'>..</th>
                            </tr>
                            <tr>
                              <th>
                                <br>
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:13px;' align='left'>
                                Buen día, ".''."
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:13px;' align='left'>
                                Es un gusto saber que pronto estará en consulta con nosotros. Antes de acudir al consultorio, le invitamos a completar la siguiente información que nos será de gran utilidad para otorgarle una mejor atención.
                              </th>
                            </tr>
                            <tr>
                              <th colspan='4' style='font-family: sans-serif; font-size:13px; color: #003166; -webkit-print-color-adjust: exact;' align='left'>
                                <div align='center'>
                                <a href='#' style='font-family: sans-serif; font-size:15px; color: #003166; -webkit-print-color-adjust: exact; background-color: #00a5e1; color: white; padding: 15px 32px; text-align: center;  text-decoration: none; display: inline-block; font-size: 16px; border-radius: 10px'>completar su cita aquí</a>
                                </div>
                              </th>
                            </tr>
                        </table> <br>
                        <div style='background: white; border: 1px solid #F8F8F8; border-radius: 5px; padding: 10px; width: 80%; margin-left: auto; margin-right: auto; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                  -moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);
                                  box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);'>
                                <b>Su próxima consulta es</b><hr>
                                <p>Fecha: <span style='font: 90% sans-serif;'> 14/07/2020 </span><br>
                                   Hora : <span style='font: 90% sans-serif;'> 05:00 hrs. </span><br>
                                   En el consultorio: <span style='font: 90% sans-serif;'> 1, en Unne Neurología y Neurofisiología </span><br>
                                   Con dirección: <span style='font: 90% sans-serif;'> Privada 9 B Sur 5101, Colonia Prados Agua Azul, Codigo Postal 72430, Sobre Circuito Juan Pablo II. Puebla, Pue. Méx. </span></p>
                        </div>          
                      </div>
                  </div>";
            $data['envio']= $message;     
        $this->load->view('agenda/envio',$data);
    }
    public function total_citas_pendientes(){
        $html='';
        $result=$this->ModelCatalogos->total_citas_pendientes($this->idpersonal,$this->fecha_reciente);
        if($result>0){
           $html.='<button type="button" class="btn waves-effect waves-light btn-block btn-sm btn-danger" onclick="modal_total_citas_pendientes()"><span class="badge badge-pill badge-primary text-white ml-auto">'.$result.'</span> PACIENTES PENDIENTES POR LLAMAR</button>';
        }else{
           $html.='';
        }
        echo $html;
    }
    public function getlistadocitaspendientes(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_total_citas_pendientes($params,$this->idpersonal,$this->fecha_reciente);
        $totaldata= $this->ModelCatalogos->total_total_citas_pendientes($params,$this->idpersonal,$this->fecha_reciente); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function eliminar_cita_pendiente(){
        $id=$this->input->post('id');
        $data = array('status'=>0);
        $this->General_model->edit_record('idcita',$id,$data,'citas');
    }
    public function editar_cita_pago(){
        $id=$this->input->post('id');
        $data = array('pago'=>1);
        $this->General_model->edit_record('idcita',$id,$data,'citas');
    }
    public function guardar_pago_paciente(){
        $data=$this->input->post();
        $idpaciente=$data['idpaciente'];
        $fecha_pago=$data['fecha_pago'];
        $metodo_pago=0;
        if(isset($data['metodo_pago'])){
            $metodo_pago=$data['metodo_pago'];
        }
        $factura=0;
        if(isset($data['factura'])){
            $factura=1;
        }
        $data = array('idpaciente'=>$idpaciente,
                      'personalId'=>$this->idpersonal,
                      'fecha_pago'=>$fecha_pago,
                      'metodo_pago'=>$metodo_pago,
                      'factura'=>$factura,
               );
        $id=$this->General_model->add_record('pagos_paciente',$data);
        echo $id;
    }
    public function guardar_pago_paciente_detalles(){
        $data=$this->input->post();
        $idpagos_paciente=$data['idpagos_paciente'];
        $monto=$data['monto_p'];
        $notas=$data['notas_p'];
        $data = array('idpagos_paciente'=>$idpagos_paciente,
                      'tipo_servicio'=>2,
                      'monto'=>$monto,
                      'notas'=>$notas
                );
        $id=$this->General_model->add_record('pagos_paciente_detalle',$data);
        echo $id;
    }
    function gat_lista_espera(){
        $html='';
        $wherec = array('activo'=>1,'personalId'=>$this->idpersonal,'listaespera'=>1);
        $lista_c=$this->General_model->getselectwhereall('citas',$wherec);
        $aux_lista=0;
        foreach ($lista_c as $item){
            $aux_lista=1;
        }
        if($aux_lista==0){
            $html.='';
        }else{
            $lista_c1=$this->ModelCatalogos->getcita_lista_espera($this->idpersonal,1);
            $lista_c2=$this->ModelCatalogos->getcita_lista_espera($this->idpersonal,2);
            $lista_c3=$this->ModelCatalogos->getcita_lista_espera($this->idpersonal,3);
            $html.='<h6 class="card-header bg-info" style="color: white">Lista de Espera </h6>
                    <div style="box-shadow: 0px 0px 30px #cccccc;"> 
                        <div class="row">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_text_prioridad_a()">
                                    <input type="hidden" id="int_prioridad_a" value="0">
                                    <div class="row">
                                        <div class="col-md-10">Alta prioridad</div>
                                        <div class="col-md-2" align="right"><strong><i class="fas fa-caret-down"></i></strong></div>
                                    </div>
                                </button>
                                <div class="texto_prioridad_a" style="display:none">
                                    <div class="card_borde">';
                                    foreach ($lista_c1 as $item){
                                    $html.='<div class="row">
                                                <div class="col-md-8">
                                                    <p style="font-size:13px">'.$item->nombre.' '.$item->nombre.'</p>
                                                </div>
                                                <div class="col-md-4" align="right">
                                                    <a style="font-size:13px; color: #03A9F4;" onclick="editar_cita_espera('.$item->idcita.','.$item->idpaciente.','.$item->paciente_llego.')"><u>Editar cita</u></a>
                                                </div>
                                            </div>';
                                    }
                                $html.='</div>    
                                </div>
                                <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_text_prioridad_m()">
                                    <input type="hidden" id="int_prioridad_m" value="0">
                                    <div class="row">
                                        <div class="col-md-10">Media prioridad</div>
                                        <div class="col-md-2" align="right"><strong><i class="fas fa-caret-down"></i></strong></div>
                                    </div>
                                </button>
                                <div class="texto_prioridad_m" style="display:none">
                                    <div class="card_borde">';
                                    foreach ($lista_c2 as $item){
                                    $html.='<div class="row">
                                                <div class="col-md-8">
                                                    <p style="font-size:13px">'.$item->nombre.' '.$item->nombre.'</p>
                                                </div>
                                                <div class="col-md-4" align="right">
                                                    <a style="font-size:13px; color: #03A9F4;" onclick="editar_cita_espera('.$item->idcita.','.$item->idpaciente.','.$item->paciente_llego.')"><u>Editar cita</u></a>
                                                </div>
                                            </div>';
                                    }
                                $html.='</div>    
                                </div>
                                <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_text_prioridad_b()">
                                    <input type="hidden" id="int_prioridad_b" value="0">
                                    <div class="row">
                                        <div class="col-md-10">Baja prioridad</div>
                                        <div class="col-md-2" align="right"><strong><i class="fas fa-caret-down"></i></strong></div>
                                    </div>
                                </button>
                                <div class="texto_prioridad_b" style="display:none">
                                    <div class="card_borde">';
                                    foreach ($lista_c3 as $item){
                                    $html.='<div class="row">
                                                <div class="col-md-8">
                                                    <p style="font-size:13px">'.$item->nombre.' '.$item->nombre.'</p>
                                                </div>
                                                <div class="col-md-4" align="right">
                                                    <a style="font-size:13px; color: #03A9F4;" onclick="editar_cita_espera('.$item->idcita.','.$item->idpaciente.','.$item->paciente_llego.')"><u>Editar cita</u></a>
                                                </div>
                                            </div>';
                                    }
                                $html.='</div>    
                                </div>
                            </div>
                        </div>        
                    </div>
                    <br>';
        }    
        echo $html;
    }
    function get_resumen_hoy(){
        $resumen=$this->ModelCatalogos->getresumen_consultas($this->idpersonal,$this->fecha_reciente);
        $html='<div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered" >
                                <tbody>';
                                foreach ($resumen as $item) {
                             $html.='<tr>
                                        <td width="9px">
                                            <span>'.$item->hora_de.'</span><br>
                                            <span>'.$item->hora_hasta.'</span>
                                        </td>
                                        <td>';
                                        if($item->tipo_cita==1 || $item->tipo_cita==2){
                                        $html.='<span style="font-size: 13px;">'.$item->nombre.' '.$item->apll_paterno.' '.$item->apll_materno.'</span><br>';
                                        $html.='<span style="font-size: 13px;">'.$item->motivo_consulta.'</span><br>';

                                            if($item->paciente_no_llego==1){
                                                $html.='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs" style="background-color: #9C27B0; color:white" onclick="mosrar_cita('.$item->idcita.','.$item->tipo_cita.','.$item->idpaciente.','.$item->paciente_llego.',0)">El paciente NO llegó</button>';
                                            }else{
                                                if($item->paciente_llego==1){
                                                    $html.='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-success" onclick="mosrar_cita('.$item->idcita.','.$item->tipo_cita.','.$item->idpaciente.','.$item->paciente_llego.',0)">El paciente llegó</button>';
                                                }else{
                                                    if($item->cita_confirmada==1){
                                                        $html.='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-warning" onclick="mosrar_cita('.$item->idcita.','.$item->tipo_cita.','.$item->idpaciente.','.$item->paciente_llego.',0)">Cita confirmada</button>';
                                                    }else{
                                                        $html.='<button type="button" class="btn waves-effect waves-light btn-rounded btn-xs btn-danger" onclick="mosrar_cita('.$item->idcita.','.$item->tipo_cita.','.$item->idpaciente.','.$item->paciente_llego.',0)">No confirmado</button>';
                                                    }
                                                }
                                            }
                                        
                                        }else{
                                            $html.=$item->motivo_consulta;
                                        }   
                                        $html.='</td>
                                    </tr>';
                                    } 
                         $html.='</tbody>
                            </table>
                        </div>
                    </div>
                </div>';
        echo $html;
    }


    function correo_p(){
        $this->load->view('agenda/pruebacorreo');
    }
    
    /*

<button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_text_his1()">
                            <div class="row">
                                <div class="col-md-10">Media prioridad</div>
                                <div class="col-md-2" align="right"><strong><i class="fas fa-caret-down"></i></strong></div>
                            </div>
                        </button>
                        <button type="button" class="btn btn-secondary waves-effect waves-light btn-block textleft" onclick="btn_text_his1()">
                            <div class="row">
                                <div class="col-md-10">Baja prioridad</div>
                                <div class="col-md-2" align="right"><strong><i class="fas fa-caret-down"></i></strong></div>
                            </div>
                        </button>
    public function searchpaciente(){
        $search = $this->input->get('search');
        $results=$this->ModelCatalogos->get_select_like_paciente($search,$this->idpersonal);
        echo json_encode($results);    
    }
    */
}    