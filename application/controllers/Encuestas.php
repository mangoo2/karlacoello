<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Encuestas extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
    
    }
	public function index(){
        /*
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
		$this->load->view('config/config',$data);
		$this->load->view('templates/footer');
        $this->load->view('config/configjs');
        */
	}
    function whatsapp($tel,$cliente){
        //$cliente=base64_encode($cliente);
        $result=$this->General_model->getselectwhereall('pacientes',array('idpaciente'=>$cliente));
        $nomcliente='';
        foreach ($result as $item) {
            $nomcliente=$item->nombre.' '.$item->apll_paterno.' '.$item->apll_materno;
        }
        $text='';
        $text.='Estimado: '.$nomcliente.' ';
        $text.='Es un gusto para nosotros tu confianza y preferencia. Nos ayudaría muchísimo que nos apoyaras en responder una pequeña encuesta de calidad para la mejora de nuestros servicios y atención de nuestro personal. No te llevará más de 2 min. ';
        $text.=' ';
        $text.=base_url().'Encuestas/answer/'.$cliente;
        //echo $url; 
        //echo base64_encode(4);
        redirect('https://api.whatsapp.com/send?phone=52'.$tel.'&text='.$text);
        
    }
    public function answer($id=0){
        //$id=1;
        $data['idpaciente']=$id;
        $this->load->view('encuesta/encuesta',$data);
        $this->load->view('encuesta/encuestajs');
    }
    
    function capturar(){
        $params=$this->input->post();
        $this->General_model->add_record('encuesta',$params);
    }
    function mail(){
        $nomcliente='';
        //$paciente=0;
        
        $params=$this->input->post();
        $paciente = $params['paciente'];
        $correoelectronico = $params['mail'];

        $result=$this->General_model->getselectwhereall('pacientes',array('idpaciente'=>$paciente));
        $nomcliente='';
        foreach ($result as $item) {
            $nomcliente=$item->nombre.' '.$item->apll_paterno.' '.$item->apll_materno;
        }
        
        $url=base_url().'Encuestas/answer/'.$paciente;
        $headerback = base_url().'public/img/headermail.jpeg';
        $logom = base_url().'public/img/logo.jpeg';
        $html='<body style="margin:0px; background: #f8f8f8; ">
                <div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
                  <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
                    <div style="padding-top: 0px; background:white;">
                    
                        <table border="0" style="width:100%">
                            <tr>
                                <td colspan="3">
                                    <img src="'.$headerback.'" style="width:100%;height: 59px;">
                                </td>
                            <tr>
                            <tr>
                                <td>
                                    <img src="'.$logom.'" style="height: 59px;">
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Encuesta de calidad</td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding: 40px; background: #fff;">
                      <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tbody>
                          <tr>
                            <td>
                                <p>Estimado : '.$nomcliente.'</p>
                                <p>Es un gusto para nosotros tu confianza y preferencia. Nos ayudaría muchísimo que nos apoyaras en responder una pequeña encuesta de calidad para la mejora de nuestros servicios y atención de nuestro personal. No te llevará más de 2 min.</p>
                                <p>Unicamente dale clic al siguiente enlace:</p>
                                <p>
                                    <a href="'.$url.'" style="display: inline-block; padding: 11px 30px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #00c0c8; border-radius: 60px; text-decoration:none;"> Click aqui </a>
                                </p>
                             </td>
                            </tr>
                            <tr>
                             <td style="text-align:center;">
                                <p>¡Muchas gracias por tu preferencia y confianza!</p>
                                <p>Att. Karla Coello</p>
                             </td>
                            </tr>
                            <tr>
                             <td style="text-align:center;">
                                <p><img src="'.$logom.'" style="height: 59px;"></p>
                             </td>
                            </tr>
                        </tbody>
                      </table>
                    </div>
                    <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
                      <p> Desarrollado por Mangoo Software</p>
                    </div>
                  </div>
                </div>
                </body>';
        //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='karlacoello.sicoi.net'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'contacto@karlacoello.sicoi.net';

            //Nuestra contraseña
            $config["smtp_pass"] = '~fq))c=0P2ZP';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('contacto@karlacoello.sicoi.net','karla coello');
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
        
        
        $this->email->to($correoelectronico, $nomcliente);
        $this->email->bcc('contacto@karlacoello.sicoi.net');
        $asunto='Encuesta';
      
        //Definimos el asunto del mensaje
        $this->email->subject($asunto);
         
        //Definimos el mensaje a enviar
        //$this->email->message($body)

        $message  = $html;

        $this->email->message($message);

        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            echo 'enviado';
        }else{
            echo $this->email->print_debugger();
        }

        //==================
        
    }
    function ultimaescuesta(){
        $params=$this->input->post();
        $paciente=$params['paciente'];
        $result=$this->General_model->getselectwhereall('encuesta',array('idpaciente'=>$paciente));
        $nomcliente='';
        $html='';
        foreach ($result as $item) {
            $html='';
            
            
            
            $html='<div class="row">
                      <div class="col-md-12">
                        <h4 class="card-title">¿Como calificaría su Experiencia con nuestro servicio.?</h4>
                      </div>
                    </div>
                    <div class="row">';
                    if($item->experiencia==1){
                        $html.='<div class="custom-control col-md-4">
                          <label class="" >Bastante complacido</label>
                      </div>';

                    }
                    if($item->experiencia==2){
                        $html.='<div class="custom-control col-md-4">
                          <label class="" >Complacido</label>
                      </div>';
                        
                    }
                    if($item->experiencia==3){
                        $html.='<div class="custom-control col-md-4">
                          <label class="" >Bueno</label>
                      </div>';
                        
                    }
                    if($item->experiencia==4){
                        $html.='<div class="custom-control col-md-4">
                          <label class="" >Decepcionante</label>
                      </div>';
                        
                    }
                    if($item->experiencia==5){
                        $html.='<div class="custom-control col-md-4">
                          <label class="" >Muy decepcionante</label>
                      </div>';
                        
                    }
            $html.='                      
                    </div>
                    <div class="row">
                      <div class="col-md-12" style="margin-top: 10px;">
                        <h4 class="card-title">¿Que tan probable es que recomiende nuestros servicios a un amigo o colega?</h4>
                      </div>
                    </div>
                    <div class="row">
                      <div class="custom-control col-md-1">
                          <label class="" >'.$item->recomendacion.'</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12" style="margin-top: 10px;">
                        <h4 class="card-title">¿Alguna sugerencia?</h4>
                      </div>
                    </div>
                    <div class="row">
                      <div class="custom-control col-md-12">
                        '.$item->sugerencia.'
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12" style="margin-top: 10px;">
                        <h4 class="card-title">¿Alguien de nuestro equipo ha brindado un servicio excelente?</h4>
                        <h4 class="card-title">Queremos escucharte</h4>
                      </div>
                    </div>
                    <div class="row">
                      <div class="custom-control col-md-12">
                        '.$item->persona.'
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12" style="margin-top: 10px;">
                        <h4 class="card-title">Comentarios:</h4>
                      </div>
                    </div>
                    <div class="row">
                      <div class="custom-control col-md-12">
                        '.$item->comentario.'
                      </div>
                    </div>';

        }
        echo $html;
    }
    


}    