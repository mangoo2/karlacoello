<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empleados extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }
    }
    public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('empleados/empleados');
        $this->load->view('templates/footer');
        $this->load->view('empleados/empleadosjs');
    }

    public function registra_empleado(){
        $data=$this->input->post();
        $personalId=$data['personalId'];
        if(isset($data['check_baja'])){
            $data['check_baja']='on';
        }else{
            $data['check_baja']='';
        }
        if($personalId==0){
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('personal',$data);
        }else{
            $id=$this->General_model->edit_record('personalId',$personalId,$data,'personal');
            $id=$personalId;
        }
        echo $id;
    }
    function cargafiles(){
        $id=$this->input->post('id');
        $folder="empleados";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
          $array = array('foto'=>$newfile);
          $this->General_model->edit_record('personalId',$id,$array,'personal');
          $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }  

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_empleados($params);
        $totaldata= $this->ModelCatalogos->total_empleados($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function delete_empleado(){
        $id=$this->input->post('id');
        $data = array('estatus'=>0);
        $this->General_model->edit_record('personalId',$id,$data,'personal');
    }
    
    public function getlistado_entrenamiento(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_entrenamientos($params);
        $totaldata= $this->ModelCatalogos->total_entrenamientos($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function registro_entrenamiento(){
        $data=$this->input->post();
        $identrenamiento=$data['identrenamiento'];
        if($identrenamiento==0){
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('entrenamientos',$data);
        }else{
            $this->General_model->edit_record('identrenamiento',$identrenamiento,$data,'entrenamientos');
            $id=$identrenamiento;
        }
        echo $id;
    }

    public function deleteregistro_entrenamiento(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('identrenamiento',$id,$data,'entrenamientos');
    }
}