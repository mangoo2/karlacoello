<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Videollamada extends CI_Controller {
    public function __construct() {
        parent::__construct();
    
    }
    public function video_llamada(){
        $this->load->view('paciente/videollamada');
        $this->load->view('paciente/videollamadajs');
    }
}