<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedores extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }
    }
    public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('proveedores/proveedor');
        $this->load->view('templates/footer');
        $this->load->view('proveedores/proveedorjs');
    }

    public function registro(){
        $data=$this->input->post();
        $idproveedor=$data['idproveedor'];
        if(isset($data['check_fiscales'])){
            $data['check_fiscales']='on';
        }else{
            $data['check_fiscales']='';
        }
        if($idproveedor==0){
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('proveedor',$data);
        }else{
            $id=$this->General_model->edit_record('idproveedor',$idproveedor,$data,'proveedor');
            $id=$idproveedor;
        }
        echo $id;
    }
    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_proveedor($params);
        $totaldata= $this->ModelCatalogos->total_proveedor($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function deleteregistro(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idproveedor',$id,$data,'proveedor');
    }

}