<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consultas extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->fechainicio = date('Y-m-d');
    }
    public function consulta($idc){
        $get_infoc=$this->General_model->get_record('idconsulta',$idc,'consulta_medicina_estetica');
        $get_infop=$this->General_model->get_record('idpaciente',$get_infoc->idpaciente,'pacientes');
        $data['get_p']=$get_infop;
        $tiempo = strtotime($get_infop->fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
            $edad = floor($edad); 
        
        $aux_existe=0;
        $html='';
        $html.='<div class="margen_todo">
                    <div class="card-body" style="padding: 0.25rem !important;">
                        <div class="row">
                            <div class="col-md-5">
                                 <h3>Medicina Estética</h3>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" align="center">
                                <h6>Resumen de consulta<h6> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h6><i class="fas fa-user"></i> Edad del paciente: <span class="letranegrita">'.$edad.' años</span></h6>
                                <h6><i class="fas fa-calendar-check"></i> Fecha de consulta: <span class="letranegrita">'.date('d/m/Y',strtotime($get_infoc->consultafecha)).'</span></h6>
                            </div>
                        </div>';
                        if($get_infoc->motivo_consulta!=''){
                    $html.='<div class="row">
                            <div class="col-md-12">
                                <h4 class="div_abajo_solid">Motivo de la consulta</h4>
                                <pre>'.$get_infoc->motivo_consulta.'</pre>';
                    $html.='</div>
                        </div>';
                        }
                        if($get_infoc->nota_evaluacion!=''){
                    $html.='<div class="row">
                            <div class="col-md-12">
                                <h4 class="div_abajo_solid">Nota de evolución</h4>
                                <pre>'.$get_infoc->nota_evaluacion.'</pre>';
                    $html.='</div>
                        </div>';
                        }
                    $html.='<div class="row">';
                            if($get_infoc->altrura!=0  || $get_infoc->peso!=0 || $get_infoc->ta!='' || $get_infoc->tempc!=0 || $get_infoc->fc!='' || $get_infoc->fr!=0 || $get_infoc->o2!=0){  
                            $html.='<div class="col-md-12">';
                                $html.='<h4 class="div_abajo_solid">Signos Vitales/Básicos</h4>
                                    </div>';
                                if($get_infoc->altrura!=0){
                            $html.='<div class="col-md-3">
                                        <p>Altura <span class="div_etiqueta">&nbsp'.$get_infoc->altrura.'&nbsp</span></p>
                                    </div>';
                                }
                                if($get_infoc->peso!=0){
                            $html.='<div class="col-md-3">
                                        <p>Peso <span class="div_etiqueta">&nbsp'.$get_infoc->peso.'&nbsp</span></p>
                                    </div>';
                                } 
                                if($get_infoc->ta!=''){
                            $html.='<div class="col-md-3">
                                        <p>T.A. <span class="div_etiqueta">&nbsp'.$get_infoc->ta.'&nbsp</span></p>
                                    </div>';
                                }
                                if($get_infoc->tempc!=0){
                            $html.='<div class="col-md-3">
                                        <p>Temp <span class="div_etiqueta">&nbsp'.$get_infoc->tempc.'&nbsp</span></p>
                                    </div>';
                                }
                                if($get_infoc->fc!=''){
                            $html.='<div class="col-md-3">
                                        <p>F.C. <span class="div_etiqueta">&nbsp'.$get_infoc->fc.'&nbsp</span></p>
                                    </div>';
                                }    
                                if($get_infoc->fr!=0){
                            $html.='<div class="col-md-3">
                                        <p>F.R. <span class="div_etiqueta">&nbsp'.$get_infoc->fr.'&nbsp</span></p>
                                    </div>';
                                }
                                if($get_infoc->o2!=0){
                            $html.='<div class="col-md-3">
                                        <p>O2 <span class="div_etiqueta">&nbsp'.$get_infoc->o2.'&nbsp</span></p>
                                    </div>';
                                }
                            }
                $html.='</div>
                        <div class="row">';
                            if($get_infoc->exploracionfisica!=''){
                    $html.='<div class="col-md-12">
                                <h4 class="div_abajo_solid">Exploración Física</h4>
                                <p>'.$get_infoc->exploracionfisica.'</p>
                            </div>';
                            }
                            if($get_infoc->tratamiento_estetico!=''){
                    $html.='<div class="col-md-12">
                                <h4 class="div_abajo_solid">Tratamientos estéticos previos (botox, rellenos, hilos, laser etc) ¿Hace cuánto tiempo?</h4>
                                    <p>'.$get_infoc->tratamiento_estetico.'</p>
                                </div>';
                            }
                            if($get_infoc->cirugia_estetica!=''){
                    $html.='<div class="col-md-12">
                                <h4 class="div_abajo_solid">Cirugías estéticas o reconstructivas ¿Hace cuánto tiempo?</h4>
                                    <p>'.$get_infoc->cirugia_estetica.'</p>
                            </div>';
                            }
                            if($get_infoc->area_tratar!=''){
                    $html.='<div class="col-md-12">
                                <h4 class="div_abajo_solid">¿Qué área le gustaría tratar?</h4>
                                    <p>'.$get_infoc->area_tratar.'</p>
                            </div>';
                            }
                            if($get_infoc->rutina_cuidado!=''){
                    $html.='<div class="col-md-12">
                                <h4 class="div_abajo_solid">¿Tienes una rutina de cuidado para su piel?</h4>
                                <p>'.$get_infoc->rutina_cuidado.'</p>
                            </div>';
                            }
                            if($get_infoc->edad_aparente!=''){
                    $html.='<div class="col-md-12">
                                <h4 class="div_abajo_solid">Edad aparente</h4>
                                <p>'.$get_infoc->edad_aparente.'</p>
                            </div>';
                            }

                    $html.='</div>
                            <div class="row">';
                            if($get_infoc->tipo!=0){
                            $html.='<div class="col-md-2">
                                <h4>Tipo</h4>';
                                $ti='';
                                if($get_infoc->tipo==1){ $ti='Normal'; }
                                else if($get_infoc->tipo==2){ $ti='Grasa'; }
                                else if($get_infoc->tipo==3){ $ti='Seca'; }
                                else if($get_infoc->tipo==4){ $ti='Asfíctica'; }
                                $html.='<p>'.$ti.'</p>
                            </div>';
                            }
                            if($get_infoc->estado!=0){
                            $html.='<div class="col-md-2">
                                <h4>Estado</h4>';
                                $es='';
                                if($get_infoc->estado==1){ $es='Acneico'; }
                                else if($get_infoc->estado==2){ $es='Seborreico'; }
                                else if($get_infoc->estado==3){ $es='Hipersudoral'; }
                                else if($get_infoc->estado==4){ $es='Dismetabólico'; }
                                else if($get_infoc->estado==5){ $es='Alípico'; }
                                else if($get_infoc->estado==6){ $es='Querótico'; }
                                else if($get_infoc->estado==7){ $es='Atrópico'; }
                                $html.='<p>'.$es.'</p>
                            </div>';
                            }
                            if($get_infoc->fototipo!=0){
                            $html.='<div class="col-md-2">
                                <h4>Fototipo</h4>';
                                $fo='';
                                if($get_infoc->fototipo==1){ $fo='l'; }
                                else if($get_infoc->fototipo==2){ $fo='ll'; }
                                else if($get_infoc->fototipo==3){ $fo='lll'; }
                                else if($get_infoc->fototipo==4){ $fo='lV'; }
                                else if($get_infoc->fototipo==5){ $fo='V'; }
                                $html.='<p>'.$fo.'</p>
                            </div>';
                            }
                            if($get_infoc->grosor!=0){
                            $html.='<div class="col-md-2">
                                <h4>Grosor</h4>';
                                $gr='';
                                if($get_infoc->grosor==1){ $gr='Media'; }
                                else if($get_infoc->grosor==2){ $gr='Fina'; }
                                else if($get_infoc->grosor==3){ $gr='Gruesa'; }
                                $html.='<p>'.$fo.'</p>
                            </div>';
                            }
                            if($get_infoc->flacidez!=0){
                            $html.='<div class="col-md-2">
                                <h4>Flacidez</h4>';
                                $fl='';
                                if($get_infoc->flacidez==1){ $fl='Muscular'; }
                                else if($get_infoc->flacidez==2){ $fl='Cutánea'; }
                                $html.='<p>'.$fl.'</p>
                            </div>';
                            }
                            if($get_infoc->higratacion!=0){
                            $html.='<div class="col-md-2">
                                <h4>Hidratación</h4>';
                                $fl='';
                                if($get_infoc->higratacion==1){ $gr='Normal'; }
                                else if($get_infoc->higratacion==2){ $gr='Deshidratada'; }
                                else if($get_infoc->higratacion==3){ $gr='Hiperhidratada'; }
                                $html.='<p>'.$gr.'</p>
                            </div>';
                            }
                            if($get_infoc->higratacion!=0){
                            $html.='<div class="col-md-2">
                                <h4>Cicatrización</h4>';
                                $fl='';
                                if($get_infoc->higratacion==1){ $gr='Normal'; }
                                else if($get_infoc->higratacion==2){ $gr='Hipertrófica'; }
                                else if($get_infoc->higratacion==3){ $gr='Queloide'; }
                                $html.='<p>'.$gr.'</p>
                            </div>';
                           }
                        $html.='</div>';
                        if($get_infoc->foto_facial!=''){
                            $fh = fopen(base_url()."uploads/analisisfacial/".$get_infoc->foto_facial, 'r') or die("Se produjo un error al abrir el archivo");
                            $linea = fgets($fh);
                            fclose($fh); 
                        $html.='<div class="row">
                                    <div class="col-md-12">
                                        <h4 class="div_abajo_solid">Análisis facial</h4>
                                        <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                                            <img style="width: 975px; height: 627px; border: 2px dashed rgb(29, 175, 147); background: url('.base_url().'images/medicina/demo2.png);background-repeat:no-repeat; background-position:center;" src="'.$linea.'" width="975" height="627" style="border:dotted 1px black;">
                                        </div>
                                    </div>
                                </div>';
                        }
                        $arraydiag = array('idconsulta' => $idc,'activo'=>1);
                        $get_diag=$this->General_model->getselectwhereall('consulta_medicina_estetica_diagnostico',$arraydiag); 
                        $aux_diag=0; 
                        foreach ($get_diag as $item){
                            $aux_diag=1;
                        }
                        if($aux_diag==1){
                        $html.='<div class="row">
                            <div class="col-md-12">
                                <h4 class="div_abajo_solid">Diagnósticos:</h4>';
                                
                                $html.='<ol>';
                                foreach ($get_diag as $item){
                                    $html.='<li>'.$item->diagnostico.'</li>';    
                                }
                                $html.='</ol>';
                    $html.='</div>
                        </div>';
                        }
                        $arraydiag = array('idconsulta' => $idc,'activo'=>1);
                        $get_tra=$this->General_model->getselectwhereall(' consulta_medicina_estetica_tratamiento',$arraydiag);
                        $aux_tra=0; 
                        foreach ($get_tra as $item){
                            $aux_tra=1;
                        }
                        if($aux_tra==1){
                    $html.='<div class="row">
                            <div class="col-md-12">
                                <h4 class="div_abajo_solid">Tratamiento:</h4>';
                                $html.='<ol>';
                                foreach ($get_tra as $item){
                                    $html.='<li>'.$item->tratamiento.'</li>';    
                                }
                                $html.='</ol>';
                    $html.='</div>
                        </div>';
                        }
                        $get_serv=$this->ModelCatalogos->get_venta_servicio_c1($idc);
                        $aux_serv=0;
                        foreach ($get_serv as $item){
                            $aux_serv=1;
                        }
                        if($aux_serv==1){
                    $html.='<div class="row">
                            <div class="col-md-12">
                                <h4 class="div_abajo_solid">Servicios específicos:</h4>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Servicio</td>
                                                <td>Descripción</td>
                                            </tr>
                                        </thead>
                                        <tbody>';
                                foreach ($get_serv as $item){
                                    $html.='<tr><td>'.$item->servicio.'</td><td>'.$item->descripcion.'</td></<tr>';    
                                }
                                    $html.='<tbody>
                                       </table>';
                    $html.='</div>
                        </div>';
                        }
                        $get_prod=$this->ModelCatalogos->get_venta_productoc1($idc);
                        $aux_prod=0;
                        foreach ($get_serv as $item){
                            $aux_prod=1;
                        }
                        if($aux_prod==1){
                    $html.='<div class="row">
                            <div class="col-md-12">
                                <h4 class="div_abajo_solid">Productos específicos:</h4>';
                                
                                $html.='<table class="table">
                                            <thead>
                                                <tr>
                                                    <td>Producto</td>
                                                    <td>Lote</td>
                                                    <td>Caducidad</td>
                                                    <td>Cantidad</td>
                                                </tr>
                                            </thead>
                                            <tbody>';
                                foreach ($get_prod as $item){
                                    $html.='<tr><td>'.$item->producto.'</td><td>'.$item->lote.'</td>
                                                <td>'.date('d/m/Y',strtotime($item->fecha_caducidad)).'</td><td>'.$item->cantidad.'</td>
                                            </<tr>';    
                                }
                                    $html.='<tbody>
                                       </table>';
                    $html.='</div>
                        </div>';
                        }
                        if($get_infoc->proximafecha!='0000-00-00'){
                        $html.='<div class="row">
                            <div class="col-md-12">
                                <h4 class="div_abajo_solid">Próxima consulta:</h4>
                                <pre>'.date('d/m/Y',strtotime($get_infoc->proximafecha)).'</pre>';
                    $html.='</div>';
                        }
                    $html.='</div>
                    </div>
                </div>';
        $data['info_consulta']=$html;        
        ///
    	$this->load->view('paciente/consulta/header');
        $this->load->view('paciente/consulta/consulta',$data);
        $this->load->view('paciente/consulta/footer');
        $this->load->view('paciente/consulta/consultajs');
    }
    public function medicina($idc){
        $get_infoc=$this->General_model->get_record('idconsulta',$idc,'consulta_medicina_estetica');
        $get_infop=$this->General_model->get_record('idpaciente',$get_infoc->idpaciente,'pacientes');
        $data['get_p']=$get_infop;
        $tiempo = strtotime($get_infop->fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
            $edad = floor($edad); 
        
        $aux_existe=0;
        $html='';
        $html.='<div class="margen_todo">
                    <div class="card-body" style="padding: 0.25rem !important;">
                        <div class="row">
                            <div class="col-md-5">
                                 <h3>Medicina Estética</h3>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h6><i class="fas fa-user"></i> Edad del paciente: <span class="letranegrita">'.$edad.' años</span></h6>
                                <h6><i class="fas fa-calendar-check"></i> Fecha de consulta: <span class="letranegrita">'.date('d/m/Y',strtotime($get_infoc->consultafecha)).'</span></h6>
                            </div>
                        </div><hr>';
                        $result_consul=$this->ModelCatalogos->get_venta_servicio_c1_ce($idc);
                        foreach ($result_consul as $item){
                        $html.='<div class="row">
                                    <div class="col-md-4">
                                        <h5>Servicio</h5> 
                                        <h5>'.$item->servicio.'</h5>
                                    </div>
                                    <div class="col-md-8">
                                        <h5>Descripcion</h5> 
                                        <h5>'.$item->descripcion.'</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div>
                                            <h2 class="badge badge-info badge-pill">Cuidados especiales</h2>
                                        </div>
                                        '.$item->cuidados_especiales.'
                                    </div>
                                </div>
                                <hr>';
                        }
                    $html.='
                    </div>
                </div>';
        $data['info_consulta']=$html;        
        ///
        $this->load->view('paciente/consulta/header');
        $this->load->view('paciente/consulta/consulta',$data);
        $this->load->view('paciente/consulta/footer');
        $this->load->view('paciente/consulta/consultajs');
    }
    public function spa($idc){
        $get_infoc=$this->General_model->get_record('idconsulta',$idc,'consulta_spa');
        $get_infop=$this->General_model->get_record('idpaciente',$get_infoc->idpaciente,'pacientes');
        $data['get_p']=$get_infop;
        $tiempo = strtotime($get_infop->fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
            $edad = floor($edad); 
        
        $aux_existe=0;
        $html='';
        $html.='<div class="margen_todo">
                    <div class="card-body" style="padding: 0.25rem !important;">
                        <div class="row">
                            <div class="col-md-5">
                                 <h3>SPA</h3>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h6><i class="fas fa-user"></i> Edad del paciente: <span class="letranegrita">'.$edad.' años</span></h6>
                                <h6><i class="fas fa-calendar-check"></i> Fecha de consulta: <span class="letranegrita">'.date('d/m/Y',strtotime($get_infoc->consultafecha)).'</span></h6>
                            </div>
                        </div><hr>';
                        $result_consul=$this->ModelCatalogos->get_venta_servicio_c2_ce($idc);
                        foreach ($result_consul as $item){
                        $html.='<div class="row">
                                    <div class="col-md-4">
                                        <h5>Servicio</h5> 
                                        <h5>'.$item->servicio.'</h5>
                                    </div>
                                    <div class="col-md-8">
                                        <h5>Descripcion</h5> 
                                        <h5>'.$item->descripcion.'</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div>
                                            <h2 class="badge badge-info badge-pill">Cuidados especiales</h2>
                                        </div>
                                        '.$item->cuidados_especiales.'
                                    </div>
                                </div>
                                <hr>';
                        }
                    $html.='
                    </div>
                </div>';
        $data['info_consulta']=$html;        
        ///
        $this->load->view('paciente/consulta/header');
        $this->load->view('paciente/consulta/consulta',$data);
        $this->load->view('paciente/consulta/footer');
        $this->load->view('paciente/consulta/consultajs');
    }
    public function nutricion($idc){
        $get_infoc=$this->General_model->get_record('idconsulta',$idc,'consulta_nutricion');
        $get_infop=$this->General_model->get_record('idpaciente',$get_infoc->idpaciente,'pacientes');
        ////
        $resultlu=$this->General_model->getselectwhereall2('plan_alimenticion_lunes',array('consultaId'=>$idc,'activo'=>1));
        $resultma=$this->General_model->getselectwhereall2('plan_alimenticion_martes',array('consultaId'=>$idc,'activo'=>1));
        $resultmi=$this->General_model->getselectwhereall2('plan_alimenticion_miercoles',array('consultaId'=>$idc,'activo'=>1));
        $resultju=$this->General_model->getselectwhereall2('plan_alimenticion_jueves',array('consultaId'=>$idc,'activo'=>1));
        $resultvi=$this->General_model->getselectwhereall2('plan_alimenticion_viernes',array('consultaId'=>$idc,'activo'=>1));
        $resultconsulta=$this->General_model->getselectwhereall2('consulta_nutricion',array('idconsulta'=>$idc,'activo'=>1));
        $resultconsulta=$resultconsulta->result();
        $resultconsulta=$resultconsulta[0];
        $resultconsulta=$resultconsulta;

        $datospaciente=$this->General_model->getselectwhereall2('pacientes',array('idpaciente'=>$resultconsulta->idpaciente,'activo'=>1));
        $paciente='';
        $grafica='';
        foreach ($datospaciente->result() as $item) {
            $paciente=$item->nombre.' '.$item->apll_paterno.' '.$item->apll_materno;
            $grafica=$item->grafica;
        }
        $data['paciente'] =$paciente;
        $data['grafica'] =$grafica;
        ////
        $data['get_p']=$get_infop;
        $tiempo = strtotime($get_infop->fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
            $edad = floor($edad); 
        
        $aux_existe=0;
        $html='';
        $html.='<div class="margen_todo">
                    <div class="card-body" style="padding: 0.25rem !important;">
                        <div class="row">
                            <div class="col-md-5">
                                 <h3>Nutrición</h3>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h6><i class="fas fa-user"></i> Edad del paciente: <span class="letranegrita">'.$edad.' años</span></h6>
                                <h6><i class="fas fa-calendar-check"></i> Fecha de consulta: <span class="letranegrita">'.date('d/m/Y',strtotime($get_infoc->consultafecha)).'</span></h6>
                            </div>
                        </div><hr>';
                        $result_consul=$this->ModelCatalogos->get_venta_servicio_c3_ce($idc);
                        foreach ($result_consul as $item){
                        $html.='<div class="row">
                                    <div class="col-md-4">
                                        <h5>Servicio</h5> 
                                        <h5>'.$item->servicio.'</h5>
                                    </div>
                                    <div class="col-md-8">
                                        <h5>Descripcion</h5> 
                                        <h5>'.$item->descripcion.'</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div>
                                            <h2 class="badge badge-info badge-pill">Cuidados especiales</h2>
                                        </div>
                                        '.$item->cuidados_especiales.'
                                    </div>
                                </div>
                                <hr>';
                        }
                        /////////////////
                        $arrayrele = array('idpaciente'=>$get_infoc->idpaciente);
        $get_relevante=$this->General_model->getselectwhereall('relevante_historia',$arrayrele);
        $rele_motivo='';
        foreach ($get_relevante as $item){
            $rele_motivo=$item->motivo_consulta;
        }
        ///
        $get_paciente=$this->General_model->get_record('idpaciente',$get_infoc->idpaciente,'pacientes');
        $idconsulta=0;
        $consultafecha=$this->fechainicio;
        ////////////////////
            $nutriologa='';
            $motivo_consulta=$rele_motivo;
            $edad='';
            $peso='';
            $altura='';
            $cuello='';
            $proximafecha='';
            $imc='';
            $grasa_magra='';
            $agua='';
            $grasa='';
            $peso_ideal='';
            $indice_cintura_cadera='';
            $cintura='';
            $cadera='';
            $indice_cintura_cadera_porcentaje='';
            $mifflin_st_jeor='';
            $harris_benedict='';
            $fao_oms_18_30='';
            $fao_oms_30_60='';
            $fao_oms_60='';
            $fao_oms_20_kcal='';
            $fao_oms_25_kcal='';
            $fao_oms_30__kcal='';
            $macro_carboidratos='';
            $macro_lipidos='';
            $macro_proteinas='';
            $macro_kcal='';
            $macro_carboidratos_gramo='';
            $macro_lipidos_gramos='';
            $macro_proteinas_gramos='';
            $total_calorias='';
            $total_meta='';
            $aporte1='';
            $aporte2='';
            $aporte3='';
            $aporte4='';
            $aporte5='';
            $aporte6='';
            $aporte7='';
            $aporte8='';
            $aporte9='';
            $aporte10='';
            $aporte11='';
            $aporte12='';
            $aporte13='';
            $aporte14='';
            $aporte15='';
            $aporte16='';
            $aporte17='';
            $aporte18='';
            $aporte19='';
            $hora1='09:00';
            $hora2='11:00';
            $hora3='03:00';
            $hora4='06:00';
            $hora5='09:00';
            $hora6='11:00';
            $hora7='';
            $comida1='Desayuno';
            $comida2='Colación 1';
            $comida3='Comida';
            $comida4='Colación 2';
            $comida5='Cena';
            $comida6='Colación 3';
            $comida7='';
            $ver1='';
            $ver2='';
            $ver3='';
            $ver4='';
            $ver5='';
            $ver6='';
            $ver7='';
            $fru1='';
            $fru2='';
            $fru3='';
            $fru4='';
            $fru5='';
            $fru6='';
            $fru7='';
            $ces1='';
            $ces2='';
            $ces3='';
            $ces4='';
            $ces5='';
            $ces6='';
            $ces7='';
            $cec1='';
            $cec2='';
            $cec3='';
            $cec4='';
            $cec5='';
            $cec6='';
            $cec7='';
            $leg1='';
            $leg2='';
            $leg3='';
            $leg4='';
            $leg5='';
            $leg6='';
            $leg7='';
            $amu1='';
            $amu2='';
            $amu3='';
            $amu4='';
            $amu5='';
            $amu6='';
            $amu7='';
            $aba1='';
            $aba2='';
            $aba3='';
            $aba4='';
            $aba5='';
            $aba6='';
            $aba7='';
            $amo1='';
            $amo2='';
            $amo3='';
            $amo4='';
            $amo5='';
            $amo6='';
            $amo7='';
            $aal1='';
            $aal2='';
            $aal3='';
            $aal4='';
            $aal5='';
            $aal6='';
            $aal7='';
            $led1='';
            $led2='';
            $led3='';
            $led4='';
            $led5='';
            $led6='';
            $led7='';
            $les1='';
            $les2='';
            $les3='';
            $les4='';
            $les5='';
            $les6='';
            $les7='';
            $lee1='';
            $lee2='';
            $lee3='';
            $lee4='';
            $lee5='';
            $lee6='';
            $lee7='';
            $lec1='';
            $lec2='';
            $lec3='';
            $lec4='';
            $lec5='';
            $lec6='';
            $lec7='';
            $acs1='';
            $acs2='';
            $acs3='';
            $acs4='';
            $acs5='';
            $acs6='';
            $acs7='';
            $acc1='';
            $acc2='';
            $acc3='';
            $acc4='';
            $acc5='';
            $acc6='';
            $acc7='';
            $azs1='';
            $azs2='';
            $azs3='';
            $azs4='';
            $azs5='';
            $azs6='';
            $azs7='';
            $azc1='';
            $azc2='';
            $azc3='';
            $azc4='';
            $azc5='';
            $azc6='';
            $azc7='';
            $ali1='';
            $ali2='';
            $ali3='';
            $ali4='';
            $ali5='';
            $ali6='';
            $ali7='';
            $bed1='';
            $bed2='';
            $bed3='';
            $bed4='';
            $bed5='';
            $bed6='';
            $bed7='';
            $recomendaciones='';
        ////////////////////
        $arrayinfo = array('idconsulta'=>$idc);
        $get_consultas=$this->General_model->getselectwhereall('consulta_nutricion',$arrayinfo);
        $aux_existe=0;
        $fecha_consulta_ultima=$this->fechainicio;
        foreach ($get_consultas as $item){
            $idconsulta=$item->idconsulta;
            $consultafecha=$item->consultafecha;
            $nutriologa=$item->nutriologa;
            $motivo_consulta=$item->motivo_consulta;
            $edad=$item->edad;
            $peso=$item->peso;
            $altura=$item->altura;
            $cuello=$item->cuello;
            $proximafecha=$item->proximafecha;
            $imc=$item->imc;
            $grasa_magra=$item->grasa_magra;
            $agua=$item->agua;
            $grasa=$item->grasa;
            $peso_ideal=$item->peso_ideal;
            $indice_cintura_cadera=$item->indice_cintura_cadera;
            $cintura=$item->cintura;
            $cadera=$item->cadera;
            $indice_cintura_cadera_porcentaje=$item->indice_cintura_cadera_porcentaje;
            $mifflin_st_jeor=$item->mifflin_st_jeor;
            $harris_benedict=$item->harris_benedict;
            $fao_oms_18_30=$item->fao_oms_18_30;
            $fao_oms_30_60=$item->fao_oms_30_60;
            $fao_oms_60=$item->fao_oms_60;
            $fao_oms_20_kcal=$item->fao_oms_20_kcal;
            $fao_oms_25_kcal=$item->fao_oms_25_kcal;
            $fao_oms_30__kcal=$item->fao_oms_30__kcal;
            $macro_carboidratos=$item->macro_carboidratos;
            $macro_lipidos=$item->macro_lipidos;
            $macro_proteinas=$item->macro_proteinas;
            $macro_kcal=$item->macro_kcal;
            $macro_carboidratos_gramo=$item->macro_carboidratos_gramo;
            $macro_lipidos_gramos=$item->macro_lipidos_gramos;
            $macro_proteinas_gramos=$item->macro_proteinas_gramos;
            $total_calorias=$item->total_calorias;
            $total_meta=$item->total_meta;
            $aporte1=$item->aporte1;
            $aporte2=$item->aporte2;
            $aporte3=$item->aporte3;
            $aporte4=$item->aporte4;
            $aporte5=$item->aporte5;
            $aporte6=$item->aporte6;
            $aporte7=$item->aporte7;
            $aporte8=$item->aporte8;
            $aporte9=$item->aporte9;
            $aporte10=$item->aporte10;
            $aporte11=$item->aporte11;
            $aporte12=$item->aporte12;
            $aporte13=$item->aporte13;
            $aporte14=$item->aporte14;
            $aporte15=$item->aporte15;
            $aporte16=$item->aporte16;
            $aporte17=$item->aporte17;
            $aporte18=$item->aporte18;
            $aporte19=$item->aporte19;
            $hora1=$item->hora1;
            $hora2=$item->hora2;
            $hora3=$item->hora3;
            $hora4=$item->hora4;
            $hora5=$item->hora5;
            $hora6=$item->hora6;
            $hora7=$item->hora7;
            $comida1=$item->comida1;
            $comida2=$item->comida2;
            $comida3=$item->comida3;
            $comida4=$item->comida4;
            $comida5=$item->comida5;
            $comida6=$item->comida6;
            $comida7=$item->comida7;
            $ver1=$item->ver1;
            $ver2=$item->ver2;
            $ver3=$item->ver3;
            $ver4=$item->ver4;
            $ver5=$item->ver5;
            $ver6=$item->ver6;
            $ver7=$item->ver7;
            $fru1=$item->fru1;
            $fru2=$item->fru2;
            $fru3=$item->fru3;
            $fru4=$item->fru4;
            $fru5=$item->fru5;
            $fru6=$item->fru6;
            $fru7=$item->fru7;
            $ces1=$item->ces1;
            $ces2=$item->ces2;
            $ces3=$item->ces3;
            $ces4=$item->ces4;
            $ces5=$item->ces5;
            $ces6=$item->ces6;
            $ces7=$item->ces7;
            $cec1=$item->cec1;
            $cec2=$item->cec2;
            $cec3=$item->cec3;
            $cec4=$item->cec4;
            $cec5=$item->cec5;
            $cec6=$item->cec6;
            $cec7=$item->cec7;
            $leg1=$item->leg1;
            $leg2=$item->leg2;
            $leg3=$item->leg3;
            $leg4=$item->leg4;
            $leg5=$item->leg5;
            $leg6=$item->leg6;
            $leg7=$item->leg7;
            $amu1=$item->amu1;
            $amu2=$item->amu2;
            $amu3=$item->amu3;
            $amu4=$item->amu4;
            $amu5=$item->amu5;
            $amu6=$item->amu6;
            $amu7=$item->amu7;
            $aba1=$item->aba1;
            $aba2=$item->aba2;
            $aba3=$item->aba3;
            $aba4=$item->aba4;
            $aba5=$item->aba5;
            $aba6=$item->aba6;
            $aba7=$item->aba7;
            $amo1=$item->amo1;
            $amo2=$item->amo2;
            $amo3=$item->amo3;
            $amo4=$item->amo4;
            $amo5=$item->amo5;
            $amo6=$item->amo6;
            $amo7=$item->amo7;
            $aal1=$item->aal1;
            $aal2=$item->aal2;
            $aal3=$item->aal3;
            $aal4=$item->aal4;
            $aal5=$item->aal5;
            $aal6=$item->aal6;
            $aal7=$item->aal7;
            $led1=$item->led1;
            $led2=$item->led2;
            $led3=$item->led3;
            $led4=$item->led4;
            $led5=$item->led5;
            $led6=$item->led6;
            $led7=$item->led7;
            $les1=$item->les1;
            $les2=$item->les2;
            $les3=$item->les3;
            $les4=$item->les4;
            $les5=$item->les5;
            $les6=$item->les6;
            $les7=$item->les7;
            $lee1=$item->lee1;
            $lee2=$item->lee2;
            $lee3=$item->lee3;
            $lee4=$item->lee4;
            $lee5=$item->lee5;
            $lee6=$item->lee6;
            $lee7=$item->lee7;
            $lec1=$item->lec1;
            $lec2=$item->lec2;
            $lec3=$item->lec3;
            $lec4=$item->lec4;
            $lec5=$item->lec5;
            $lec6=$item->lec6;
            $lec7=$item->lec7;
            $acs1=$item->acs1;
            $acs2=$item->acs2;
            $acs3=$item->acs3;
            $acs4=$item->acs4;
            $acs5=$item->acs5;
            $acs6=$item->acs6;
            $acs7=$item->acs7;
            $acc1=$item->acc1;
            $acc2=$item->acc2;
            $acc3=$item->acc3;
            $acc4=$item->acc4;
            $acc5=$item->acc5;
            $acc6=$item->acc6;
            $acc7=$item->acc7;
            $azs1=$item->azs1;
            $azs2=$item->azs2;
            $azs3=$item->azs3;
            $azs4=$item->azs4;
            $azs5=$item->azs5;
            $azs6=$item->azs6;
            $azs7=$item->azs7;
            $azc1=$item->azc1;
            $azc2=$item->azc2;
            $azc3=$item->azc3;
            $azc4=$item->azc4;
            $azc5=$item->azc5;
            $azc6=$item->azc6;
            $azc7=$item->azc7;
            $ali1=$item->ali1;
            $ali2=$item->ali2;
            $ali3=$item->ali3;
            $ali4=$item->ali4;
            $ali5=$item->ali5;
            $ali6=$item->ali6;
            $ali7=$item->ali7;
            $bed1=$item->bed1;
            $bed2=$item->bed2;
            $bed3=$item->bed3;
            $bed4=$item->bed4;
            $bed5=$item->bed5;
            $bed6=$item->bed6;
            $bed7=$item->bed7;
            $recomendaciones=$item->recomendaciones;
        }
            $html.='<div align="center">
                        <span class="badge badge-info badge-pill">Datos generales</span>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-3 col-form-label"><i class="fas fa-user"></i>Nutriologa</label>
                                <div class="col-8">
                                    <input type="text" name="nutriologa" id="nutriologa" value="'.$nutriologa.'" class="form-control colorlabel_white">
                                </div>
                            </div>
                        ';
                $html.='</div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-5 col-form-label"><i class="far fa-calendar-alt"></i> Fecha de la consulta:</label>
                                <div class="col-6">
                                    <input type="date" name="consultafecha" id="consultafecha" value="'.$consultafecha.'" class="form-control colorlabel_white">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Motivo de la consulta</label>
                            <div class="form-group input-group mb-3 recordableHolder">
                                <textarea type="text" name="motivo_consulta" class="form-control colorlabel_white recordable rinited js-auto-size">'.$motivo_consulta.'</textarea>
                            </div>
                        </div>
                    </div>
                    <div align="center">
                        <span class="badge badge-info badge-pill">Información clínica</span>
                    </div>
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label
                                    data-toggle="tooltip" data-placement="top" data-original-title="Peso"
                                >Edad</label>
                                <input type="number" name="edad" id="edadn" value="'.$edad.'" class="form-control colorlabel_white">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label
                                    data-toggle="tooltip" data-placement="top" data-original-title="Peso"
                                >Peso (kg)</label>
                                <input type="number" name="peso" id="peson" value="'.$peso.'" oninput="calcularimc()"  class="form-control colorlabel_white">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label
                                    data-toggle="tooltip" data-placement="top" data-original-title="Altura"
                                >Altura (m)</label>
                                <input type="number" name="altura" id="alturan" value="'.$altura.'" oninput="calcularimc()"  class="form-control colorlabel_white">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label
                                    data-toggle="tooltip" data-placement="top" data-original-title="Tensión Arterial"
                                >Cuello</label>
                                <input type="number" name="cuello" id="cuellon" value="'.$cuello.'"  oninput="calcularimc()" class="form-control colorlabel_white">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card" style="border-style: solid;
                                                    border-width: 1px;
                                                    border-radius: 12px;
                                                    border-color: #e5e6e5;">
                                        <div class="d-flex flex-row">
                                            <div class="p-10 bg-info">
                                                <h3 class="text-white box m-b-0">IMC</i></h3></div>
                                            <div class="align-self-center m-l-20">
                                                <h3 class="m-b-0" style="font-weight: bold"><span class="imc_n">0<span></h3>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="card" style="border-style: solid;
                                                    border-width: 1px;
                                                    border-radius: 12px;
                                                    border-color: #e5e6e5;">
                                        <div class="d-flex flex-row">
                                            <div class="p-10 bg-info">
                                                <h3 class="text-white box m-b-0">%</h3></div>
                                            <div class="align-self-center m-l-20">
                                                <h3 class="m-b-0" style="font-weight: bold"><span class="masa_magra_txt">0<span></h3>
                                                <h5 class="text-muted m-b-0"><b>Grasa magra</b></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="card" style="border-style: solid;
                                                    border-width: 1px;
                                                    border-radius: 12px;
                                                    border-color: #e5e6e5;">
                                        <div class="d-flex flex-row">
                                            <div class="p-10 bg-info">
                                                <h3 class="text-white box m-b-0">%</h3>
                                            </div>
                                            <div class="align-self-center m-l-20">
                                                <h3 class="m-b-0" style="font-weight: bold"><span class="agua_txt">0</span></h3>
                                                <h5 class="text-muted m-b-0"><b>Agua</b></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="card" style="border-style: solid;
                                                    border-width: 1px;
                                                    border-radius: 12px;
                                                    border-color: #e5e6e5;">
                                        <div class="d-flex flex-row">
                                            <div class="p-10 bg-info">
                                                <h3 class="text-white box m-b-0">%</h3>
                                            </div>
                                            <div class="align-self-center m-l-20">
                                                <h3 class="m-b-0" style="font-weight: bold"><span class="grasa_txt">0</span></h3>
                                                <h5 class="text-muted m-b-0"><b>Grasa</b></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="col-lg-9">
                            <div class="nutricion_imc">
                            </div> 
                        </div>       
                    </div> 
                    <div style="display:none">
                        <div align="center">
                            <span class="badge badge-info badge-pill">Otras mediciones</span><br><br>
                        </div> 
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-3">
                                <div class="card" style="border-style: solid;
                                    border-width: 1px;
                                    border-radius: 12px;
                                    border-color: #e5e6e5;">
                                    <div class="box text-center">
                                        <h1 class="font-light text-black" style="font-size: 53px;"><span class="indice_cin_can_txt">0</span></h1>
                                        <h6 class="text-black">Índice cintura altura</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="card" style="border-style: solid;
                                    border-width: 1px;
                                    border-radius: 12px;
                                    border-color: #e5e6e5;">
                                    <div class="box">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h6 class="text-black">Índice cintura cadera</h6>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label
                                                        data-toggle="tooltip" data-placement="top" data-original-title="Peso"
                                                    >Cintura</label>
                                                    <input type="number" name="cintura" id="cinturan" value="'.$cintura.'" oninput="calcularimc()" class="form-control colorlabel_white">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label
                                                        data-toggle="tooltip" data-placement="top" data-original-title="Peso"
                                                    >Cadera</label>
                                                    <input type="number" name="cadera" id="caderan" value="'.$cadera.'" oninput="calcularimc()" class="form-control colorlabel_white">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="box text-center">
                                                    <h1 class="font-light text-black"><span class="indice_cintura_cadera_txt">0</span></h1>
                                                </div>    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="display:none">
                        <div align="center">
                            <span class="badge badge-info badge-pill">Metabolismo Basal</span><br><br>
                        </div> 
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card" style="border-style: solid;
                                            border-width: 1px;
                                            border-radius: 12px;
                                            border-color: #e5e6e5;">
                                            <div class="box text-center">
                                                <h1 class="font-light text-black"><span class="mifflin_st_jeor_txt">0</span></h1>
                                                <h6 class="text-black">Mifflin St-Jeor</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card" style="border-style: solid;
                                            border-width: 1px;
                                            border-radius: 12px;
                                            border-color: #e5e6e5;">
                                            <div class="box text-center">
                                                <h1 class="font-light text-black"><span class="harris_benedict_txt">0</span></h1>
                                                <h6 class="text-black">Harris-Benedict</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                            </div>
                            <div class="col-md-4">
                                <h4 align="center" style="font-weight: bold;">FAO/OMS</h4>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Edad</th>
                                            <th>Fórmula</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>18-30</td>
                                            <td><span class="for1_txt">0</span></td>
                                        </tr>
                                        <tr>
                                            <td>30-60</td>
                                            <td><span class="for2_txt">0</span></td>
                                        </tr>
                                        <tr>
                                            <td>>60</td>
                                            <td><span class="for3_txt">0</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-4">
                                <h4 align="center" style="color:white;">.</h4>
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: bold;">20 kcal</td>
                                            <td><span class="kcal1_txt">0</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">25 kcal</td>
                                            <td><span class="kcal2_txt">0</span></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">30 kcal</td>
                                            <td><span class="kcal3_txt">0</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>  
                    </div>
                     <div style="display:none">
                        <div align="center">
                            <span class="badge badge-info badge-pill">Macronutrientes</span><br><br>
                        </div>    
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>%</th>
                                            <th>Kcal</th>
                                            <th>Gramos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Dieta Total</td>
                                            <td>100</td>
                                            <td><input type="number" name="macro_kcal" id="dieta_kcal" value="'.$macro_kcal.'"  oninput="btn_macronutrientes_kcal()" class="form-control colorlabel_white"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Carbohidratos</td>
                                            <td><input type="number" name="macro_carboidratos" id="carbohidratos" value="'.$macro_carboidratos.'" oninput="btn_macronutrientes()" class="form-control colorlabel_white"></td>
                                            <td><span class="car_kcal_txt">0</span></td>
                                            <td><span class="car_gra_txt">0</span></td>
                                        </tr>
                                        <tr>
                                            <td>Lípidos</td>
                                            <td><input type="number" name="macro_lipidos" id="lipidos" value="'.$macro_lipidos.'"  oninput="btn_macronutrientes()" class="form-control colorlabel_white"></td>
                                            <td><span class="lip_kcal_txt">0</span></td>
                                            <td><span class="lip_gra_txt">0</span></td>
                                        </tr>
                                        <tr>
                                            <td>Proteínas</td>
                                            <td><input type="number" name="macro_proteinas" id="proteina" value="'.$macro_proteinas.'"  oninput="btn_macronutrientes()" class="form-control colorlabel_white"></td>
                                            <td><span class="pro_kcal_txt">0</span></td>
                                            <td><span class="pro_gra_txt">0</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div style="display:none">
                        <div class="row">
                            <div class="col-lg-7">
                                <div align="center">
                                    <span class="badge badge-info badge-pill">Actividad Física</span><br>
                                </div>    
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Mifflin St-Jeor</th>
                                            <th>Harris-Benedict</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Actividad</td>';
                                            $tipo_sexo='';
                                            if($get_paciente->sexo==1){
                                                $tipo_sexo='Masculino';
                                            }else{
                                                $tipo_sexo='Femenino';
                                            }
                                    $html.='<td>'.$tipo_sexo.'</td>
                                            <td>'.$tipo_sexo.'</td>
                                        </tr>
                                        <tr>
                                            <td>Encamado</td>
                                            <td><span class="enc_mifflin">0</span></td>
                                            <td><span class="enc_harris">0</span></td>
                                        </tr>
                                        <tr>
                                            <td>Leve/Sedentario</td>
                                            <td><span class="lev_mifflin">0</span></td>
                                            <td><span class="lev_harris">0</span></td>
                                        </tr>
                                        <tr>
                                            <td>Moderada</td>
                                            <td><span class="mod_mifflin">0</span></td>
                                            <td><span class="mod_harris">0</span></td>
                                        </tr>
                                        <tr>
                                            <td>Fuerte</td>
                                            <td><span class="fue_mifflin">0</span></td>
                                            <td><span class="fue_harris">0</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> 
                            <div class="col-lg-5">
                                <div align="center">
                                    <span class="badge badge-info badge-pill">FAO/OMS</span><br>
                                </div> 
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Edad/Actividad</th>
                                            <th>18-30</th>
                                            <th>30-60</th>
                                            <th>>60</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Sedentario</td>
                                            <td><span class="sed_18">0</span></td>
                                            <td><span class="sed_30">0</span></td>
                                            <td><span class="sed_60">0</span></td>
                                        </tr>
                                        <tr>
                                            <td>Ligera</td>
                                            <td><span class="lig_18">0</span></td>
                                            <td><span class="lig_30">0</span></td>
                                            <td><span class="lig_60">0</span></td>
                                        </tr>
                                        <tr>
                                            <td>Moderada</td>
                                            <td><span class="mod_18">0</span></td>
                                            <td><span class="mod_30">0</span></td>
                                            <td><span class="mod_60">0</span></td>
                                        </tr>
                                        <tr>
                                            <td>Alta</td>
                                            <td><span class="alt_18">0</span></td>
                                            <td><span class="alt_30">0</span></td>
                                            <td><span class="alt_60">0</span></td>
                                        </tr>
                                        <tr>
                                            <td>Muy Alta</td>
                                            <td><span class="muy_18">0</span></td>
                                            <td><span class="muy_30">0</span></td>
                                            <td><span class="muy_60">0</span></td>
                                        </tr>
                                    </tbody>
                                </table>   
                            </div>
                        </div> 
                    </div>
                    <div style="display:none">
                        <div align="center">
                            <span class="badge badge-info badge-pill">Aporte nutrimental</span><br><br>
                        </div> 
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-3">
                                <div class="card" style="border-style: solid;
                                    border-width: 1px;
                                    border-radius: 12px;
                                    border-color: #e5e6e5;">
                                    <div class="box">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label
                                                        data-toggle="tooltip" data-placement="top"
                                                    >Total de calorias</label>
                                                    <input type="number" name="total_calorias" id="total_calorias" value="'.$total_calorias.'" oninput="calcular_calorias()" class="form-control colorlabel_white">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <div class="col-md-3">
                                <div class="card" style="border-style: solid;
                                    border-width: 1px;
                                    border-radius: 12px;
                                    border-color: #e5e6e5;">
                                    <div class="box">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label
                                                        data-toggle="tooltip" data-placement="top"
                                                    >Total meta</label>
                                                    <input type="number" name="total_meta" id="total_meta" value="'.$total_meta.'"  oninput="calcular_calorias()" class="form-control colorlabel_white">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div style="display:none">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="calculoplanalimenticio">
                                    <thead>
                                        <tr class="tr_n">
                                            <th class="th_n" rowspan="2" style="width: 70px;text-align:center">5</th>
                                            <th class="th_n" rowspan="2">Grupo de Alimento</th>
                                            <th class="th_n" rowspan="2">Subgrupos</th>
                                            <th class="th_n" colspan="6" style="text-align:center">Aporte nutrimental promedio en dieta ideal</th>
                                        </tr>
                                        <tr class="tr_n">
                                            <th class="th_n">Energía (Kcal)</th>
                                            <th class="th_n">Proteína (g)</th>
                                            <th class="th_n">Lípidos (g)</th>
                                            <th class="th_n">Carbohidratos (g)</th>
                                            <th class="th_n">Fibra(g)</th>
                                            <th class="th_n">Sodio (mg)</th
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte1" id="aporte1" value="'.$aporte1.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n">Verduras</td>
                                            <td class="td_n"></td>
                                            <td class="td_n"><span class="ene1_tx">0</span></td>
                                            <td class="td_n"><span class="pro1_tx">0</span></td>
                                            <td class="td_n"><span class="lip1_tx">0</span></td>
                                            <td class="td_n"><span class="car1_tx">0</span></td>
                                            <td class="td_n"><span class="fib1_tx">0</span></td>
                                            <td class="td_n"><span class="sod1_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte2" id="aporte2" value="'.$aporte2.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n">3 Frutas</td>
                                            <td class="td_n"></td>
                                            <td class="td_n"><span class="ene2_tx">0</span></td>
                                            <td class="td_n"><span class="pro2_tx">0</span></td>
                                            <td class="td_n"><span class="lip2_tx">0</span></td>
                                            <td class="td_n"><span class="car2_tx">0</span></td>
                                            <td class="td_n"><span class="fib2_tx">0</span></td>
                                            <td class="td_n"><span class="sod2_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte3" id="aporte3" value="'.$aporte3.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n" rowspan="2">*1 Cereales y tubérculos</td>
                                            <td class="td_n">a. Sin grasa</td>
                                            <td class="td_n"><span class="ene3_tx">0</span></td>
                                            <td class="td_n"><span class="pro3_tx">0</span></td>
                                            <td class="td_n"><span class="lip3_tx">0</span></td>
                                            <td class="td_n"><span class="car3_tx">0</span></td>
                                            <td class="td_n"><span class="fib3_tx">0</span></td>
                                            <td class="td_n"><span class="sod3_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte4" id="aporte4" value="'.$aporte4.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n">b. Con grasa</td>
                                            <td class="td_n"><span class="ene4_tx">0</span></td>
                                            <td class="td_n"><span class="pro4_tx">0</span></td>
                                            <td class="td_n"><span class="lip4_tx">0</span></td>
                                            <td class="td_n"><span class="car4_tx">0</span></td>
                                            <td class="td_n"><span class="fib4_tx">0</span></td>
                                            <td class="td_n"><span class="sod4_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte5" id="aporte5" value="'.$aporte5.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n">Leguminosas</td>
                                            <td class="td_n"></td>
                                            <td class="td_n"><span class="ene5_tx">0</span></td>
                                            <td class="td_n"><span class="pro5_tx">0</span></td>
                                            <td class="td_n"><span class="lip5_tx">0</span></td>
                                            <td class="td_n"><span class="car5_tx">0</span></td>
                                            <td class="td_n"><span class="fib5_tx">0</span></td>
                                            <td class="td_n"><span class="sod5_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte6" id="aporte6" value="'.$aporte6.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n" rowspan="4">1 Alimentos de origen animal</td>
                                            <td class="td_n">a. Muy bajo aporte de grasa</td>
                                            <td class="td_n"><span class="ene6_tx">0</span></td>
                                            <td class="td_n"><span class="pro6_tx">0</span></td>
                                            <td class="td_n"><span class="lip6_tx">0</span></td>
                                            <td class="td_n"><span class="car6_tx">0</span></td>
                                            <td class="td_n"><span class="fib6_tx">0</span></td>
                                            <td class="td_n"><span class="sod6_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte7" id="aporte7" value="'.$aporte7.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n">b. Bajo aporte de grasa</td>
                                            <td class="td_n"><span class="ene7_tx">0</span></td>
                                            <td class="td_n"><span class="pro7_tx">0</span></td>
                                            <td class="td_n"><span class="lip7_tx">0</span></td>
                                            <td class="td_n"><span class="car7_tx">0</span></td>
                                            <td class="td_n"><span class="fib7_tx">0</span></td>
                                            <td class="td_n"><span class="sod7_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte8" id="aporte8" value="'.$aporte8.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n">c. Moderado aporte de grasa</td>
                                            <td class="td_n"><span class="ene8_tx">0</span></td>
                                            <td class="td_n"><span class="pro8_tx">0</span></td>
                                            <td class="td_n"><span class="lip8_tx">0</span></td>
                                            <td class="td_n"><span class="car8_tx">0</span></td>
                                            <td class="td_n"><span class="fib8_tx">0</span></td>
                                            <td class="td_n"><span class="sod8_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte9" id="aporte9" value="'.$aporte9.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n">d. Alto aporte de grasa</td>
                                            <td class="td_n"><span class="ene9_tx">0</span></td>
                                            <td class="td_n"><span class="pro9_tx">0</span></td>
                                            <td class="td_n"><span class="lip9_tx">0</span></td>
                                            <td class="td_n"><span class="car9_tx">0</span></td>
                                            <td class="td_n"><span class="fib9_tx">0</span></td>
                                            <td class="td_n"><span class="sod9_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte10" id="aporte10" value="'.$aporte10.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n" rowspan="4">Leche y yogurt</td>
                                            <td class="td_n">a. Descremada</td>
                                            <td class="td_n"><span class="ene10_tx">0</span></td>
                                            <td class="td_n"><span class="pro10_tx">0</span></td>
                                            <td class="td_n"><span class="lip10_tx">0</span></td>
                                            <td class="td_n"><span class="car10_tx">0</span></td>
                                            <td class="td_n"><span class="fib10_tx">0</span></td>
                                            <td class="td_n"><span class="sod10_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte11" id="aporte11" value="'.$aporte11.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n">b. Semidescremada</td>
                                            <td class="td_n"><span class="ene11_tx">0</span></td>
                                            <td class="td_n"><span class="pro11_tx">0</span></td>
                                            <td class="td_n"><span class="lip11_tx">0</span></td>
                                            <td class="td_n"><span class="car11_tx">0</span></td>
                                            <td class="td_n"><span class="fib11_tx">0</span></td>
                                            <td class="td_n"><span class="sod11_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte12" id="aporte12" value="'.$aporte12.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n">c. Entera</td>
                                            <td class="td_n"><span class="ene12_tx">0</span></td>
                                            <td class="td_n"><span class="pro12_tx">0</span></td>
                                            <td class="td_n"><span class="lip12_tx">0</span></td>
                                            <td class="td_n"><span class="car12_tx">0</span></td>
                                            <td class="td_n"><span class="fib12_tx">0</span></td>
                                            <td class="td_n"><span class="sod12_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte13" id="aporte13" value="'.$aporte13.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n">d. Con azúcar</td>
                                            <td class="td_n"><span class="ene13_tx">0</span></td>
                                            <td class="td_n"><span class="pro13_tx">0</span></td>
                                            <td class="td_n"><span class="lip13_tx">0</span></td>
                                            <td class="td_n"><span class="car13_tx">0</span></td>
                                            <td class="td_n"><span class="fib13_tx">0</span></td>
                                            <td class="td_n"><span class="sod13_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte14" id="aporte14" value="'.$aporte14.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n" rowspan="2">2 Aceites y grasas</td>
                                            <td class="td_n">a. Sin proteína</td>
                                            <td class="td_n"><span class="ene14_tx">0</span></td>
                                            <td class="td_n"><span class="pro14_tx">0</span></td>
                                            <td class="td_n"><span class="lip14_tx">0</span></td>
                                            <td class="td_n"><span class="car14_tx">0</span></td>
                                            <td class="td_n"><span class="fib14_tx">0</span></td>
                                            <td class="td_n"><span class="sod14_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte15" id="aporte15" value="'.$aporte15.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n">b. Con proteína</td>
                                            <td class="td_n"><span class="ene15_tx">0</span></td>
                                            <td class="td_n"><span class="pro15_tx">0</span></td>
                                            <td class="td_n"><span class="lip15_tx">0</span></td>
                                            <td class="td_n"><span class="car15_tx">0</span></td>
                                            <td class="td_n"><span class="fib15_tx">0</span></td>
                                            <td class="td_n"><span class="sod15_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte16" id="aporte16" value="'.$aporte16.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n" rowspan="2">Azúcares</td>
                                            <td class="td_n">a. Sin grasa</td>
                                            <td class="td_n"><span class="ene16_tx">0</span></td>
                                            <td class="td_n"><span class="pro16_tx">0</span></td>
                                            <td class="td_n"><span class="lip16_tx">0</span></td>
                                            <td class="td_n"><span class="car16_tx">0</span></td>
                                            <td class="td_n"><span class="fib16_tx">0</span></td>
                                            <td class="td_n"><span class="sod16_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte17" id="aporte17" value="'.$aporte17.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n">b. Con grasa</td>
                                            <td class="td_n"><span class="ene17_tx">0</span></td>
                                            <td class="td_n"><span class="pro17_tx">0</span></td>
                                            <td class="td_n"><span class="lip17_tx">0</span></td>
                                            <td class="td_n"><span class="car17_tx">0</span></td>
                                            <td class="td_n"><span class="fib17_tx">0</span></td>
                                            <td class="td_n"><span class="sod17_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte18" id="aporte18" value="'.$aporte18.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n">Alimentos libres en energía</td>
                                            <td class="td_n"></td>
                                            <td class="td_n"><span class="ene18_tx">0</span></td>
                                            <td class="td_n"><span class="pro18_tx">0</span></td>
                                            <td class="td_n"><span class="lip18_tx">0</span></td>
                                            <td class="td_n"><span class="car18_tx">0</span></td>
                                            <td class="td_n"><span class="fib18_tx">0</span></td>
                                            <td class="td_n"><span class="sod18_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n">
                                                <input type="number" name="aporte19" id="aporte19" value="'.$aporte19.'"  oninput="calcular_dieta()" class="form-control color_input_tabla">
                                            </td>
                                            <td class="td_n">Bebidas alcohólicas</td>
                                            <td class="td_n"></td>
                                            <td class="td_n"><span class="ene19_tx">0</span></td>
                                            <td class="td_n"><span class="pro19_tx">0</span></td>
                                            <td class="td_n"><span class="lip19_tx">0</span></td>
                                            <td class="td_n"><span class="car19_tx">0</span></td>
                                            <td class="td_n"><span class="fib19_tx">0</span></td>
                                            <td class="td_n"><span class="sod19_tx">0</span></td>
                                        </tr>
                                        <tr class="tr_n" >
                                            <td class="td_n" colspan="3" style="text-align:center;background-color: #88d64d;">Total</td>
                                            <td class="td_n" style="text-align:center;background-color: #88d64d;"><span class="total_calori_txt">0</span></td>
                                            <td class="td_n" style="text-align:center;background-color: #ffbaa5;"><span class="total_proteina">0</span></td>
                                            <td class="td_n" style="text-align:center;background-color: #88d64d;"><span class="total_lipidos">verdpan></td>
                                            <td class="td_n" style="text-align:center;background-color: red;color:black"><span class="total_carboidratos">0</span></td>
                                            <td class="td_n" rowspan="2" style="text-align:center;background-color: #88d64d;"><span class="total_fibra">0</span></td>
                                            <td class="td_n" rowspan="2" style="text-align:center;background-color: #88d64d;"><span class="total_sodio">0</span></td>
                                        </tr>
                                        <tr class="tr_n" >
                                            <td class="td_n" colspan="3" style="text-align:center;background-color: #f9e53a;">Total Meta</td>
                                            <td class="td_n" style="text-align:center;background-color: #f9e53a;color:red"><span class="total_meta_txt">0</span></td>
                                            <td class="td_n" style="text-align:center;background-color: #f9e53a;"><span class="total_proteina_gramos">0</span></td>
                                            <td class="td_n" style="text-align:center;background-color: #f9e53a;"><span class="total_lipidos_gramos">0</span></td>
                                            <td class="td_n" style="text-align:center;background-color: #f9e53a;"><span class="total_carboidratos_gramos">0</span></td>
                                        </tr>
                                        <tr class="tr_n">
                                            <td class="td_n" colspan="3" style="text-align:center;">% Porcentaje de Adecuacion</td>
                                            <td class="td_n" style="text-align:center;color:red"><span class="total_porcentaje_txt">0</span></td>
                                            <td class="td_n" style="text-align:center;color:red"><span class="total_porcentaje2_txt">0</span></td>
                                            <td class="td_n" style="text-align:center;color:red"><span class="total_porcentaje3_txt">0</span></td>
                                            <td class="td_n" style="text-align:center;color:red"><span class="total_porcentaje4_txt">0</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"></td>
                                            <td>Cuantos g FALTAN</td>
                                            <td style="background-color: #ff9800;text-align:center;"><span class="total_cuantos_faltan1_txt">0</span></td>
                                            <td style="background-color: #ff9800;text-align:center;"><span class="total_cuantos_faltan2_txt">0</span></td>
                                            <td style="background-color: #ff9800;text-align:center;"><span class="total_cuantos_faltan3_txt">0</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td colspan="2" style="text-align:right;font-size: 20px; color:red">No. RACIONES</td>
                                            <td class="td_n" style="background-color: #ffeb3b;text-align:center;font-size: 30px;"><span class="total_no_raciones_txt">0</span></td>
                                            <td class="td_n" style="background-color: #ffeb3b;text-align:center;font-size: 30px;"><span class="total_no_raciones2_txt">0</span></td>
                                            <td class="td_n" style="background-color: #ffeb3b;text-align:center;font-size: 30px;"><span class="total_no_raciones3_txt">0</span></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>   

                            </div>
                        </div>
                    </div>
                    <div align="center">
                            <h2 class="badge badge-info badge-pill">Aporte Nutrimental</h2><br>
                        </div> 
                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            <table>
                                <thead>
                                    <tr>
                                        <th colspan="3"></th>
                                        <th class="th_n3">
                                            <input type="text" placeholder="09:00" name="hora1" id="hora1" value="'.$hora1.'" class="form-control color_input_tabla2">
                                        </th>
                                        <th class="th_n4">
                                            <input type="text" placeholder="11:00" name="hora2" id="hora2" value="'.$hora2.'" class="form-control color_input_tabla2">
                                        </th>
                                        <th class="th_n5">
                                            <input type="text" placeholder="03:00 " name="hora3" id="hora3" value="'.$hora3.'" class="form-control color_input_tabla2">
                                        </th>
                                        <th class="th_n6">
                                            <input type="text" placeholder="06:00" name="hora4" id="hora4" value="'.$hora4.'" class="form-control color_input_tabla2">
                                        </th>
                                        <th class="th_n6">
                                            <input type="text" placeholder="09:00" name="hora5" id="hora5" value="'.$hora5.'" class="form-control color_input_tabla2">
                                        </th>
                                        <th class="th_n6">
                                            <input type="text" placeholder="11:00" name="hora6" id="hora6" value="'.$hora6.'" class="form-control color_input_tabla2">
                                        </th>
                                        <th class="th_n6">
                                            <input type="text" placeholder="" name="hora7" id="hora7" value="'.$hora7.'" class="form-control color_input_tabla2">
                                        </th>
                                    </tr>
                                    <tr class="th_n2">
                                        <th class="th_n2" rowspan="2" style="width: 45px;text-align:center"># Equivalentes</th>
                                        <th class="th_n2" rowspan="2">Grupo de Alimento</th>
                                        <th class="th_n2" rowspan="2">Subgrupos</th>
                                        <th class="th_n3">
                                            <input type="text" placeholder="Desayuno" name="comida1" id="comida1"  value="'.$comida1.'"class="form-control color_input_tabla2 grupo_plan_1">
                                        </th>
                                        <th class="th_n4">
                                            <input type="text" placeholder="Colación 1" name="comida2" id="comida2" value="'.$comida2.'" class="form-control color_input_tabla2 grupo_plan_2">
                                        </th>
                                        <th class="th_n5">
                                            <input type="text" placeholder="Comida" name="comida3" id="comida3" value="'.$comida3.'" class="form-control color_input_tabla2 grupo_plan_3">
                                        </th>
                                        <th class="th_n6">
                                            <input type="text" placeholder="Colación 2" name="comida4" id="comida4" value="'.$comida4.'"class="form-control color_input_tabla2 grupo_plan_4">
                                        </th>
                                        <th class="th_n6">
                                            <input type="text" placeholder="Cena" name="comida5" id="comida5" value="'.$comida5.'" class="form-control color_input_tabla2 grupo_plan_5">
                                        </th>
                                        <th class="th_n6">
                                            <input type="text" placeholder="Colación 3" name="comida6" id="comida6" value="'.$comida6.'"class="form-control color_input_tabla2 grupo_plan_6">
                                        </th>
                                        <th class="th_n6">
                                            <input type="text" placeholder="" name="comida7" id="comida7" value="'.$comida7.'" class="form-control color_input_tabla2 grupo_plan_7">
                                        </th>
                                    </tr>
                                    <tr class="tr_n">
                                        <th class="th_n3"><span class="oculto_text3">_</span></th>
                                        <th class="th_n4"><span class="oculto_text4">_</span></th>
                                        <th class="th_n5"><span class="oculto_text5">_</span></th>
                                        <th class="th_n5_6"><span class="oculto_text6">_</span></th>
                                        <th class="th_n5_6"><span class="oculto_text6">_</span></th>
                                        <th class="th_n5_6"><span class="oculto_text6">_</span></th>
                                        <th class="th_n5_6"><span class="oculto_text6">_</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext1">0</span></td>
                                        <td class="td_n">Verduras</td>
                                        <td class="td_n"></td>
                                        <td class="th_n3">
                                            <input type="number" name="ver1" id="ver1" value="'.$ver1.'" oninput="calcular_verdura()" class="form-control color_input_tabla2">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="ver2" id="ver2" value="'.$ver2.'" oninput="calcular_verdura()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="ver3" id="ver3" value="'.$ver3.'" oninput="calcular_verdura()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="ver4" id="ver4" value="'.$ver4.'" oninput="calcular_verdura()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="ver5" id="ver5" value="'.$ver5.'" oninput="calcular_verdura()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="ver6" id="ver6" value="'.$ver6.'" oninput="calcular_verdura()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="ver7" id="ver7" value="'.$ver7.'" oninput="calcular_verdura()" class="form-control color_input_tabla2"></td>
                                        <td class="td_n tm_td verdura_color" style="text-align:center; color:white"><span class="icono_verdura"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext2">0</span></td>
                                        <td class="td_n">3 Frutas</td>
                                        <td class="td_n"></td>
                                        <td class="th_n3">
                                            <input type="number" name="fru1" id="fru1" value="'.$fru1.'" oninput="calcular_fruta()" class="form-control color_input_tabla2">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="fru2" id="fru2" value="'.$fru2.'" oninput="calcular_fruta()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="fru3" id="fru3" value="'.$fru3.'" oninput="calcular_fruta()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="fru4" id="fru4" value="'.$fru4.'" oninput="calcular_fruta()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="fru5" id="fru5" value="'.$fru5.'" oninput="calcular_fruta()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="fru6" id="fru6" value="'.$fru6.'" oninput="calcular_fruta()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="fru7" id="fru7" value="'.$fru7.'" oninput="calcular_fruta()" class="form-control color_input_tabla2"></td>
                                        <td class="td_n tm_td fruta_color" style="text-align:center; color:white"><span class="icono_fruta"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext3">0</span></td>
                                        <td class="td_n" rowspan="2">*1 Cereales y tubérculos</td>
                                        <td class="td_n">a. Sin grasa</td>
                                        <td class="th_n3">
                                            <input type="number" name="ces1" id="ces1" value="'.$ces1.'" oninput="calcular_cereals()" class="form-control color_input_tabla2 cerales1">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="ces2" id="ces2" value="'.$ces2.'" oninput="calcular_cereals()" class="form-control color_input_tabla2 cerales2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="ces3" id="ces3" value="'.$ces3.'" oninput="calcular_cereals()" class="form-control color_input_tabla2 cerales3"></td>
                                        <td class="th_n6">
                                            <input type="number" name="ces4" id="ces4" value="'.$ces4.'" oninput="calcular_cereals()" class="form-control color_input_tabla2 cerales4"></td>
                                        <td class="th_n6">
                                            <input type="number" name="ces5" id="ces5" value="'.$ces5.'" oninput="calcular_cereals()" class="form-control color_input_tabla2 cerales5"></td>
                                        <td class="th_n6">
                                            <input type="number" name="ces6" id="ces6" value="'.$ces6.'" oninput="calcular_cereals()" class="form-control color_input_tabla2 cerales6"></td>
                                        <td class="th_n6">
                                            <input type="number" name="ces7" id="ces7" value="'.$ces7.'" oninput="calcular_cereals()" class="form-control color_input_tabla2 cerales7"></td>
                                        <td class="td_n tm_td cerials_color" style="text-align:center; color:white"><span class="icono_cerials"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext4">0</span></td>
                                        <td class="td_n">b. Con grasa</td>
                                        <td class="th_n3">
                                            <input type="number" name="cec1" id="cec1" value="'.$cec1.'" oninput="calcular_cerealc()" class="form-control color_input_tabla2 cerales1">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="cec2" id="cec2" value="'.$cec2.'" oninput="calcular_cerealc()" class="form-control color_input_tabla2 cerales2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="cec3" id="cec3" value="'.$cec3.'" oninput="calcular_cerealc()" class="form-control color_input_tabla2 cerales3"></td>
                                        <td class="th_n6">
                                            <input type="number" name="cec4" id="cec4" value="'.$cec4.'" oninput="calcular_cerealc()" class="form-control color_input_tabla2 cerales4"></td>
                                        <td class="th_n6">
                                            <input type="number" name="cec5" id="cec5" value="'.$cec5.'" oninput="calcular_cerealc()" class="form-control color_input_tabla2 cerales5"></td>
                                        <td class="th_n6">
                                            <input type="number" name="cec6" id="cec6" value="'.$cec6.'" oninput="calcular_cerealc()" class="form-control color_input_tabla2 cerales6"></td>
                                        <td class="th_n6">
                                            <input type="number" name="cec7" id="cec7" value="'.$cec7.'" oninput="calcular_cerealc()" class="form-control color_input_tabla2 cerales7"></td>
                                        <td class="td_n tm_td cerialc_color" style="text-align:center; color:white"><span class="icono_cerialc"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext5">0</span></td>
                                        <td class="td_n">Leguminosas</td>
                                        <td class="td_n"></td>
                                        <td class="th_n3">
                                            <input type="number" name="leg1" id="leg1" value="'.$leg1.'" oninput="calcular_leguminosa()" class="form-control color_input_tabla2">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="leg2" id="leg2" value="'.$leg2.'" oninput="calcular_leguminosa()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="leg3" id="leg3" value="'.$leg3.'" oninput="calcular_leguminosa()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="leg4" id="leg4" value="'.$leg4.'" oninput="calcular_leguminosa()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="leg5" id="leg5" value="'.$leg5.'" oninput="calcular_leguminosa()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="leg6" id="leg6" value="'.$leg6.'" oninput="calcular_leguminosa()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="leg7" id="leg7" value="'.$leg7.'" oninput="calcular_leguminosa()" class="form-control color_input_tabla2"></td>
                                        <td class="td_n tm_td leguminosa_color" style="text-align:center; color:white"><span class="icono_leguminosa"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext6">0</span></td>
                                        <td class="td_n" rowspan="4">1 Alimentos de origen animal</td>
                                        <td class="td_n">a. Muy bajo aporte de grasa</td>
                                        <td class="th_n3">
                                            <input type="number" name="amu1" id="amu1" value="'.$amu1.'"  oninput="calcular_alimentos_muy_bajo()" class="form-control color_input_tabla2 aoa1">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="amu2" id="amu2" value="'.$amu2.'"  oninput="calcular_alimentos_muy_bajo()" class="form-control color_input_tabla2 aoa2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="amu3" id="amu3" value="'.$amu3.'"  oninput="calcular_alimentos_muy_bajo()" class="form-control color_input_tabla2 aoa3"></td>
                                        <td class="th_n6">
                                            <input type="number" name="amu4" id="amu4" value="'.$amu4.'"  oninput="calcular_alimentos_muy_bajo()" class="form-control color_input_tabla2 aoa4"></td>
                                        <td class="th_n6">
                                            <input type="number" name="amu5" id="amu5" value="'.$amu5.'"  oninput="calcular_alimentos_muy_bajo()" class="form-control color_input_tabla2 aoa5"></td>
                                        <td class="th_n6">
                                            <input type="number" name="amu6" id="amu6" value="'.$amu6.'"  oninput="calcular_alimentos_muy_bajo()" class="form-control color_input_tabla2 aoa6"></td>
                                        <td class="th_n6">
                                            <input type="number" name="amu7" id="amu7" value="'.$amu7.'"  oninput="calcular_alimentos_muy_bajo()" class="form-control color_input_tabla2 aoa7"></td>
                                        <td class="td_n tm_td alimentos_muy_bajo_color" style="text-align:center; color:white"><span class="icono_alimentos_muy_bajo"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext7">0</span></td>
                                        <td class="td_n">b. Bajo aporte de grasa</td>
                                        <td class="th_n3">
                                            <input type="number" name="aba1" id="aba1" value="'.$aba1.'" oninput="calcular_alimentos_bajo()" class="form-control color_input_tabla2 aoa1">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="aba2" id="aba2" value="'.$aba2.'" oninput="calcular_alimentos_bajo()" class="form-control color_input_tabla2 aoa2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="aba3" id="aba3" value="'.$aba3.'" oninput="calcular_alimentos_bajo()" class="form-control color_input_tabla2 aoa3"></td>
                                        <td class="th_n6">
                                            <input type="number" name="aba4" id="aba4" value="'.$aba4.'" oninput="calcular_alimentos_bajo()" class="form-control color_input_tabla2 aoa4"></td>
                                        <td class="th_n6">
                                            <input type="number" name="aba5" id="aba5" value="'.$aba5.'" oninput="calcular_alimentos_bajo()" class="form-control color_input_tabla2 aoa5"></td>
                                        <td class="th_n6">
                                            <input type="number" name="aba6" id="aba6" value="'.$aba6.'" oninput="calcular_alimentos_bajo()" class="form-control color_input_tabla2 aoa6"></td>
                                        <td class="th_n6">
                                            <input type="number" name="aba7" id="aba7" value="'.$aba7.'" oninput="calcular_alimentos_bajo()" class="form-control color_input_tabla2 aoa7"></td>
                                        <td class="td_n tm_td alimentos_bajo_color" style="text-align:center; color:white"><span class="icono_alimentos_bajo"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext8">0</span></td>
                                        <td class="td_n">c. Moderado aporte de grasa</td>
                                        <td class="th_n3">
                                            <input type="number" name="amo1" id="amo1" value="'.$amo1.'" oninput="calcular_alimentos_moderado()" class="form-control color_input_tabla2 aoa1">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="amo2" id="amo2" value="'.$amo2.'" oninput="calcular_alimentos_moderado()" class="form-control color_input_tabla2 aoa2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="amo3" id="amo3" value="'.$amo3.'" oninput="calcular_alimentos_moderado()" class="form-control color_input_tabla2 aoa3"></td>
                                        <td class="th_n6">
                                            <input type="number" name="amo4" id="amo4" value="'.$amo4.'" oninput="calcular_alimentos_moderado()" class="form-control color_input_tabla2 aoa4"></td>
                                        <td class="th_n6">
                                            <input type="number" name="amo5" id="amo5" value="'.$amo5.'" oninput="calcular_alimentos_moderado()" class="form-control color_input_tabla2 aoa5"></td>
                                        <td class="th_n6">
                                            <input type="number" name="amo6" id="amo6" value="'.$amo6.'" oninput="calcular_alimentos_moderado()" class="form-control color_input_tabla2 aoa6"></td>
                                        <td class="th_n6">
                                            <input type="number" name="amo7" id="amo7" value="'.$amo7.'" oninput="calcular_alimentos_moderado()" class="form-control color_input_tabla2 aoa7"></td>
                                        <td class="td_n tm_td alimentos_moderado_color" style="text-align:center; color:white"><span class="icono_moderado_bajo"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext9">0</span></td>
                                        <td class="td_n">d. Alto aporte de grasa</td>
                                        <td class="th_n3">
                                            <input type="number" name="aal1" id="aal1" value="'.$aal1.'" oninput="calcular_alimentos_alto_aporte()" class="form-control color_input_tabla2 aoa1">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="aal2" id="aal2" value="'.$aal2.'" oninput="calcular_alimentos_alto_aporte()" class="form-control color_input_tabla2 aoa2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="aal3" id="aal3" value="'.$aal3.'" oninput="calcular_alimentos_alto_aporte()" class="form-control color_input_tabla2 aoa3"></td>
                                        <td class="th_n6">
                                            <input type="number" name="aal4" id="aal4" value="'.$aal4.'" oninput="calcular_alimentos_alto_aporte()" class="form-control color_input_tabla2 aoa4"></td>
                                        <td class="th_n6">
                                            <input type="number" name="aal5" id="aal5" value="'.$aal5.'" oninput="calcular_alimentos_alto_aporte()" class="form-control color_input_tabla2 aoa5"></td>
                                        <td class="th_n6">
                                            <input type="number" name="aal6" id="aal6" value="'.$aal6.'" oninput="calcular_alimentos_alto_aporte()" class="form-control color_input_tabla2 aoa6"></td>
                                        <td class="th_n6">
                                            <input type="number" name="aal7" id="aal7" value="'.$aal7.'" oninput="calcular_alimentos_alto_aporte()" class="form-control color_input_tabla2 aoa7"></td>
                                        <td class="td_n tm_td ali_alto_aporte_color" style="text-align:center; color:white"><span class="icono_ali_alto_aporte"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext10">0</span></td>
                                        <td class="td_n" rowspan="4">Leche y yogurt</td>
                                        <td class="td_n">a. Descremada</td>
                                        <td class="th_n3">
                                            <input type="number" name="led1" id="led1" value="'.$led1.'" oninput="calcular_leche_descremada()" class="form-control color_input_tabla2 leyog1">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="led2" id="led2" value="'.$led2.'" oninput="calcular_leche_descremada()" class="form-control color_input_tabla2 leyog2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="led3" id="led3" value="'.$led3.'" oninput="calcular_leche_descremada()" class="form-control color_input_tabla2 leyog3"></td>
                                        <td class="th_n6">
                                            <input type="number" name="led4" id="led4" value="'.$led4.'" oninput="calcular_leche_descremada()" class="form-control color_input_tabla2 leyog4"></td>
                                        <td class="th_n6">
                                            <input type="number" name="led5" id="led5" value="'.$led5.'" oninput="calcular_leche_descremada()" class="form-control color_input_tabla2 leyog5"></td>
                                        <td class="th_n6">
                                            <input type="number" name="led6" id="led6" value="'.$led6.'" oninput="calcular_leche_descremada()" class="form-control color_input_tabla2 leyog6"></td>
                                        <td class="th_n6">
                                            <input type="number" name="led7" id="led7" value="'.$led7.'" oninput="calcular_leche_descremada()" class="form-control color_input_tabla2 leyog7"></td>
                                        <td class="td_n tm_td leche_descremada_color" style="text-align:center; color:white"><span class="icono_leche_descremada"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext11">0</span></td>
                                        <td class="td_n">b. Semidescremada</td>
                                        <td class="th_n3">
                                            <input type="number" name="les1" id="les1" value="'.$les1.'" oninput="calcular_leche_semi()" class="form-control color_input_tabla2 leyog1">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="les2" id="les2" value="'.$les2.'" oninput="calcular_leche_semi()" class="form-control color_input_tabla2 leyog2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="les3" id="les3" value="'.$les3.'" oninput="calcular_leche_semi()" class="form-control color_input_tabla2 leyog3"></td>
                                        <td class="th_n6">
                                            <input type="number" name="les4" id="les4" value="'.$les4.'" oninput="calcular_leche_semi()" class="form-control color_input_tabla2 leyog4"></td>
                                        <td class="th_n6">
                                            <input type="number" name="les5" id="les5" value="'.$les5.'" oninput="calcular_leche_semi()" class="form-control color_input_tabla2 leyog5"></td>
                                        <td class="th_n6">
                                            <input type="number" name="les6" id="les6" value="'.$les6.'" oninput="calcular_leche_semi()" class="form-control color_input_tabla2 leyog6"></td>
                                        <td class="th_n6">
                                            <input type="number" name="les7" id="les7" value="'.$les7.'" oninput="calcular_leche_semi()" class="form-control color_input_tabla2 leyog7"></td>
                                        <td class="td_n tm_td leche_semi_color" style="text-align:center; color:white"><span class="icono_leche_semi"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext12">0</span></td>
                                        <td class="td_n">c. Entera</td>
                                        <td class="th_n3">
                                            <input type="number" name="lee1" id="lee1" value="'.$lee1.'" oninput="calcular_leche_entera()" class="form-control color_input_tabla2 leyog1">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="lee2" id="lee2" value="'.$lee2.'" oninput="calcular_leche_entera()" class="form-control color_input_tabla2 leyog2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="lee3" id="lee3" value="'.$lee3.'" oninput="calcular_leche_entera()" class="form-control color_input_tabla2 leyog3"></td>
                                        <td class="th_n6">
                                            <input type="number" name="lee4" id="lee4" value="'.$lee4.'" oninput="calcular_leche_entera()" class="form-control color_input_tabla2 leyog4"></td>
                                        <td class="th_n6">
                                            <input type="number" name="lee5" id="lee5" value="'.$lee5.'" oninput="calcular_leche_entera()" class="form-control color_input_tabla2 leyog5"></td>
                                        <td class="th_n6">
                                            <input type="number" name="lee6" id="lee6" value="'.$lee6.'" oninput="calcular_leche_entera()" class="form-control color_input_tabla2 leyog6"></td>
                                        <td class="th_n6">
                                            <input type="number" name="lee7" id="lee7" value="'.$lee7.'" oninput="calcular_leche_entera()" class="form-control color_input_tabla2 leyog7"></td>
                                        <td class="td_n tm_td leche_entera_color" style="text-align:center; color:white"><span class="icono_leche_entera"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext13">0</span></td>
                                        <td class="td_n">d. Con azúcar</td>
                                        <td class="th_n3">
                                            <input type="number" name="lec1" id="lec1" value="'.$lec1.'" oninput="calcular_leche_con_azucar()" class="form-control color_input_tabla2 leyog1">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="lec2" id="lec2" value="'.$lec2.'" oninput="calcular_leche_con_azucar()" class="form-control color_input_tabla2 leyog2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="lec3" id="lec3" value="'.$lec3.'" oninput="calcular_leche_con_azucar()" class="form-control color_input_tabla2 leyog3"></td>
                                        <td class="th_n6">
                                            <input type="number" name="lec4" id="lec4" value="'.$lec4.'" oninput="calcular_leche_con_azucar()" class="form-control color_input_tabla2 leyog4"></td>
                                        <td class="th_n6">
                                            <input type="number" name="lec5" id="lec5" value="'.$lec5.'" oninput="calcular_leche_con_azucar()" class="form-control color_input_tabla2 leyog5"></td>
                                        <td class="th_n6">
                                            <input type="number" name="lec6" id="lec6" value="'.$lec6.'" oninput="calcular_leche_con_azucar()" class="form-control color_input_tabla2 leyog6"></td>
                                        <td class="th_n6">
                                            <input type="number" name="lec7" id="lec7" value="'.$lec7.'" oninput="calcular_leche_con_azucar()" class="form-control color_input_tabla2 leyog7"></td>
                                        <td class="td_n tm_td leche_con_azucar_color" style="text-align:center; color:white"><span class="icono_leche_con_azucar"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext14">0</span></td>
                                        <td class="td_n" rowspan="2">2 Aceites y grasas</td>
                                        <td class="td_n">a. Sin proteína</td>
                                        <td class="th_n3">
                                            <input type="number" name="acs1" id="acs1" value="'.$acs1.'" oninput="calcular_aceite_sin_proteina()" class="form-control color_input_tabla2 azucara">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="acs2" id="acs2" value="'.$acs2.'" oninput="calcular_aceite_sin_proteina()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="acs3" id="acs3" value="'.$acs3.'" oninput="calcular_aceite_sin_proteina()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="acs4" id="acs4" value="'.$acs4.'" oninput="calcular_aceite_sin_proteina()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="acs5" id="acs5" value="'.$acs5.'" oninput="calcular_aceite_sin_proteina()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="acs6" id="acs6" value="'.$acs6.'" oninput="calcular_aceite_sin_proteina()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="acs7" id="acs7" value="'.$acs7.'" oninput="calcular_aceite_sin_proteina()" class="form-control color_input_tabla2"></td>
                                        <td class="td_n tm_td aceite_sin_proteina_color" style="text-align:center; color:white"><span class="icono_aceite_sin_proteina"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext15">0</span></td>
                                        <td class="td_n">b. Con proteína</td>
                                        <td class="th_n3">
                                            <input type="number" name="acc1" id="acc1" value="'.$acc1.'" oninput="calcular_aceite_con_proteina()" class="form-control color_input_tabla2">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="acc2" id="acc2" value="'.$acc2.'" oninput="calcular_aceite_con_proteina()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="acc3" id="acc3" value="'.$acc3.'" oninput="calcular_aceite_con_proteina()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="acc4" id="acc4" value="'.$acc4.'" oninput="calcular_aceite_con_proteina()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="acc5" id="acc5" value="'.$acc5.'" oninput="calcular_aceite_con_proteina()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="acc6" id="acc6" value="'.$acc6.'" oninput="calcular_aceite_con_proteina()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="acc7" id="acc7" value="'.$acc7.'" oninput="calcular_aceite_con_proteina()" class="form-control color_input_tabla2"></td>
                                        <td class="td_n tm_td aceite_con_proteina_color" style="text-align:center; color:white"><span class="icono_aceite_con_proteina"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext16">0</span></td>
                                        <td class="td_n" rowspan="2">Azúcares</td>
                                        <td class="td_n">a. Sin grasa</td>
                                        <td class="th_n3">
                                            <input type="number" name="azs1" id="azs1" value="'.$azs1.'" oninput="calcular_azucares_sin_grasa()" class="form-control color_input_tabla2 azucara1">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="azs2" id="azs2" value="'.$azs2.'" oninput="calcular_azucares_sin_grasa()" class="form-control color_input_tabla2 azucara2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="azs3" id="azs3" value="'.$azs3.'" oninput="calcular_azucares_sin_grasa()" class="form-control color_input_tabla2 azucara3"></td>
                                        <td class="th_n6">
                                            <input type="number" name="azs4" id="azs4" value="'.$azs4.'" oninput="calcular_azucares_sin_grasa()" class="form-control color_input_tabla2 azucara4"></td>
                                        <td class="th_n6">
                                            <input type="number" name="azs5" id="azs5" value="'.$azs5.'" oninput="calcular_azucares_sin_grasa()" class="form-control color_input_tabla2 azucara5"></td>
                                        <td class="th_n6">
                                            <input type="number" name="azs6" id="azs6" value="'.$azs6.'" oninput="calcular_azucares_sin_grasa()" class="form-control color_input_tabla2 azucara6"></td>
                                        <td class="th_n6">
                                            <input type="number" name="azs7" id="azs7" value="'.$azs7.'" oninput="calcular_azucares_sin_grasa()" class="form-control color_input_tabla2 azucara7"></td>
                                        <td class="td_n tm_td azucares_sin_grasa_color" style="text-align:center; color:white"><span class="icono_azucares_sin_grasa"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext17">0</span></td>
                                        <td class="td_n">b. Con grasa</td>
                                        <td class="th_n3">
                                            <input type="number" name="azc1" id="azc1" value="'.$azc1.'" oninput="calcular_azucares_con_grasa()" class="form-control color_input_tabla2 azucara1">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="azc2" id="azc2" value="'.$azc2.'" oninput="calcular_azucares_con_grasa()" class="form-control color_input_tabla2 azucara2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="azc3" id="azc3" value="'.$azc3.'" oninput="calcular_azucares_con_grasa()" class="form-control color_input_tabla2 azucara3"></td>
                                        <td class="th_n6">
                                            <input type="number" name="azc4" id="azc4" value="'.$azc4.'" oninput="calcular_azucares_con_grasa()" class="form-control color_input_tabla2 azucara4"></td>
                                        <td class="th_n6">
                                            <input type="number" name="azc5" id="azc5" value="'.$azc5.'" oninput="calcular_azucares_con_grasa()" class="form-control color_input_tabla2 azucara5"></td>
                                        <td class="th_n6">
                                            <input type="number" name="azc6" id="azc6" value="'.$azc6.'" oninput="calcular_azucares_con_grasa()" class="form-control color_input_tabla2 azucara6"></td>
                                        <td class="th_n6">
                                            <input type="number" name="azc7" id="azc7" value="'.$azc7.'" oninput="calcular_azucares_con_grasa()" class="form-control color_input_tabla2 azucara7"></td>
                                        <td class="td_n tm_td azucares_con_grasa_color" style="text-align:center; color:white"><span class="icono_azucares_con_grasa"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext18">0</span></td>
                                        <td class="td_n">Alimentos libres en energía</td>
                                        <td class="td_n"></td>
                                        <td class="th_n3">
                                            <input type="number" name="ali1" id="ali1" value="'.$ali1.'" oninput="calcular_alimentos_libres()" class="form-control color_input_tabla2">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="ali2" id="ali2" value="'.$ali2.'" oninput="calcular_alimentos_libres()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="ali3" id="ali3" value="'.$ali3.'" oninput="calcular_alimentos_libres()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="ali4" id="ali4" value="'.$ali4.'" oninput="calcular_alimentos_libres()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="ali5" id="ali5" value="'.$ali5.'" oninput="calcular_alimentos_libres()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="ali6" id="ali6" value="'.$ali6.'" oninput="calcular_alimentos_libres()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="ali7" id="ali7" value="'.$ali7.'" oninput="calcular_alimentos_libres()" class="form-control color_input_tabla2"></td>
                                        <td class="td_n tm_td alimentos_libres_color" style="text-align:center; color:white"><span class="icono_alimentos_libres"></span></td>
                                    </tr>
                                    <tr class="tr_n">
                                        <td class="td_n1"><span class="aportext19">0</span></td>
                                        <td class="td_n">Bebidas alcohólicas</td>
                                        <td class="td_n"></td>
                                        <td class="th_n3">
                                            <input type="number" name="bed1" id="bed1" value="'.$bed1.'" oninput="calcular_bebidas_alcholicas()" class="form-control color_input_tabla2">
                                            </td>
                                        <td class="th_n4">
                                            <input type="number" name="bed2" id="bed2" value="'.$bed2.'" oninput="calcular_bebidas_alcholicas()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n5">
                                            <input type="number" name="bed3" id="bed3" value="'.$bed3.'" oninput="calcular_bebidas_alcholicas()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="bed4" id="bed4" value="'.$bed4.'" oninput="calcular_bebidas_alcholicas()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="bed5" id="bed5" value="'.$bed5.'" oninput="calcular_bebidas_alcholicas()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="bed6" id="bed6" value="'.$bed6.'" oninput="calcular_bebidas_alcholicas()" class="form-control color_input_tabla2"></td>
                                        <td class="th_n6">
                                            <input type="number" name="bed7" id="bed7" value="'.$bed7.'" oninput="calcular_bebidas_alcholicas()" class="form-control color_input_tabla2"></td>
                                        <td class="td_n tm_td bebidas_acl_color" style="text-align:center; color:white"><span class="icono_bebidas_acl"></span></td>
                                    </tr>
                                    
                                </tbody>
                            </table>   
                        </div>
                    </div> 
                    <div class="margen_div"></div>';

                    $html.='<hr><div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>Recomendaciones: </label>
                              <pre >'.$recomendaciones.'</pre>
                            </div>
                          </div>
                    </div>';
                    $html.='<div class="row">
                        <div class="col-md-12">
                            <div class="margen_todo">
                                <div class="card-body margen_left" style="padding: 0.25rem !important;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h6 class="m-b-0"><i class="fas fa-calendar-plus"></i> Próxima consulta:</h6>
                                        </div>
                                    </div>
                                    <div class="margen_div"></div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="date" name="proximafecha" id="proximafecha" value="'.$proximafecha.'" class="form-control colorlabel_white">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
                    $html.='<style>
        .tdcolorbodermenu{
            border-bottom: 1px solid #7a9557;
            font-size: 15px;
            color:#7a9557
        }
        
      </style>
      <table border="0">
        <tr>
          <td class="tdcolorbodermenu">LUNES</td>
          <td class="tdcolorbodermenu">MARTES</td>
          <td class="tdcolorbodermenu">MIERCOLES</td>
          <td class="tdcolorbodermenu">JUEVES</td>
          <td class="tdcolorbodermenu">VIERNES</td>
        </tr>
        <tr>
            <td>';
            if ($resultlu->num_rows()>0) {
              $html.='<table>';
                foreach ($resultlu->result() as $item) {
                 $html.='<tr><td>'.$item->contenido.'</td></tr>';
                }
              $html.='</table>';
            }
  $html.=' </td>
            <td>';
            if ($resultma->num_rows()>0) {
              $html.='<table>';
                foreach ($resultma->result() as $item) {
                 $html.='<tr><td>'.$item->contenido.'</td></tr>';
                }
              $html.='</table>';
            }
$html.='   </td>
            <td>';
            if ($resultmi->num_rows()>0) {
              $html.='<table>';
                foreach ($resultmi->result() as $item) {
                 $html.='<tr><td>'.$item->contenido.'</td></tr>';
                }
              $html.='</table>';
            }
$html.='   </td>
            <td>';
            if ($resultju->num_rows()>0) {
              $html.='<table>';
                foreach ($resultju->result() as $item) {
                 $html.='<tr><td>'.$item->contenido.'</td></tr>';
                }
              $html.='</table>';
            }
$html.='   </td>
            <td>';
            if ($resultvi->num_rows()>0) {
              $html.='<table>';
                foreach ($resultvi->result() as $item) {
                 $html.='<tr><td>'.$item->contenido.'</td></tr>';
                }
              $html.='</table>';
            }
  $html.='</td>
        </tr>';
$html.='</table>';

        if($grafica!=''){
            $html.='<br><div align="center">
                            <h2 class="badge badge-info badge-pill">Grafica de evolución</h2><br>
                        </div>';
            $html.='<table>'; 
        $html.='<tr>
                    <td>
                        <div >
                            <img style="" src="'.base_url().'uploads/graficas_nutricion/'.$grafica.'">
                        </div>
                    </td>
                </tr>';
            $html.='</table>';
            }
                    $html.='
                    </div>
                </div>';
        $data['info_consulta']=$html;        
        ///
        $this->load->view('paciente/consulta/header');
        $this->load->view('paciente/consulta/consulta',$data);
        $this->load->view('paciente/consulta/footer');
        $this->load->view('paciente/consulta/nutricion');
    }

    function get_grasa(){
        $c13=$this->input->post('c13');
        $c5=$this->input->post('c5');
        $c4=$this->input->post('c4');
        $c14=$this->input->post('c14');
        $tipo=$this->input->post('tipo');
        log_message('error',$c5);
        if($tipo==1){
            $suma=(495/(1.0324-0.19077*(log10($c13-$c5))+0.15456*(log10($c4)))-450);
        }else{
            $suma=(495/(1.29579-0.35004*(log10($c13+$c14-$c5))+0.221*(log10($c4)))-450);
        }
        echo $suma;
    }

}    