<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicios extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }
    }
    public function index(){
        $data['get_categoria']=$this->General_model->get_records_condition('activo=1','servicios_categoria');
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('servicios/servicios',$data);
        $this->load->view('templates/footer');
        $this->load->view('servicios/serviciosjs');
    }

    public function registro(){
        $data=$this->input->post();
        $idservicio=$data['idservicio'];
        if($idservicio==0){
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('servicios',$data);
        }else{
            $id=$this->General_model->edit_record('idservicio',$idservicio,$data,'servicios');
            $id=$idservicio;
        }
        echo $id;
    }
    
    public function registro_categoria(){
        $data=$this->input->post();
        $idcategoria=$data['idcategoria'];
        if($idcategoria==0){
            $id=$this->General_model->add_record('servicios_categoria',$data);
        }else{
            $this->General_model->edit_record('idcategoria',$idcategoria,$data,'servicios_categoria');
            $id=$idcategoria;
        }
        echo $id;
    }
    
    public function deleteregistro_cate(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idcategoria',$id,$data,'servicios_categoria');
    }

    public function getlistado_categoria(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_categoria_servicios($params);
        $totaldata= $this->ModelCatalogos->total_categoria_servicios($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    
    public function getlistado_servicios(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_servicios($params);
        $totaldata= $this->ModelCatalogos->total_servicios($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function deleteregistro(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idservicio',$id,$data,'servicios');
    }

}