<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TEMP extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('General_model');
        redirect('/Login');
	}

	public function index(){
			$elements="1- -25-2-0-4-0-3-0.3/2--60-0-0-15-0-2-1/3-Sin grasa-70-2-0-15-50-2-1/3-Con grasa-115-2-5-15-50-2-1/4--120-8-1-20-20-0-1.3/5-Muy bajo aporte de grasa-40-7-1-0-40-0-0/5-Bajo aporte de grasa-55-7-3-0-40-0-0/5-Moderado aporte de grasa-75-7-5-0-40-0-0/5-Alto aporte de grasa-100-7-8-0-40-0-0/6-Descremada-95-9-2-12-100-0-0.8/6-Semidescremada-110-9-4-12-100-0-0.8/6-Entera-150-9-8-12-100-0-0.8/6-Con azúcar-200-8-5-30-100-0-2/7-Sin proteína-45-0-5-0-25-0-0/7-Con proteína-70-3-5-3-25-0-0.2/8-Sin grasa-40-0-0-10-20-0-0.7/8-Con grasa-85-0-5-10-20-0-0.7/9--0-0-0-0-0-0-0/10--140-0-0-20-0-0-1.3";
			$de=explode("/",$elements);
			$fin=count($de);
			for ($i=0; $i < $fin; $i++){ 
				$se=explode("-",$de[$i]);
				$dtinfo=array('id_grupo' => $se[0], 'subgrupo' => $se[1],'energia' => $se[2],'proteina' => $se[3],'lipidos' => $se[4],'carbohidratos' => $se[5],'sodio' => $se[6],'fibra' => $se[7],'conteoHC' => $se[8]);
				echo $this->General_model->add_record("sub_grupos",$dtinfo);
				echo "<br>";
			}

	}

}

/* End of file TEMP.php */
/* Location: ./application/controllers/TEMP.php */