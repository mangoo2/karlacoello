<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parkinson extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
    }
    public function consulta($idpaciente){
        $idn=intval($idpaciente);
        $arrayinfo = array('idpaciente'=>intval($idn));
        $get_consultas=$this->General_model->getselectwhereall('consultas',$arrayinfo);
        $idpaciente=0;
        $idconsulta=0;
        foreach ($get_consultas as $item){
            $idconsulta=$item->idconsulta;
            $idpaciente=$item->idpaciente;
            $data['consultafecha']=$item->consultafecha;
            
        } 
        $arrayinfop = array('idpaciente'=>$idpaciente);
        $get_consultasp=$this->General_model->getselectwhereall('pacientes',$arrayinfop);

        foreach ($get_consultasp as $itemp){
            $data['paciente']=$itemp->nombre.' '.$itemp->apll_paterno.' '.$itemp->apll_materno;
        } 
        $arrayinfor = array('idconsulta'=>$idconsulta);
        $get_receta=$this->General_model->getselectwhereall('receta',$arrayinfor);
        $idreceta=0;
        foreach ($get_receta as $item){
            $idreceta=$item->idreceta; 
        }
        ///
        $get_cita=$this->ModelCatalogos->get_ultima_cita($idpaciente);
        $idcita=0;
        $fecha_consulta='';
        $hora_de='';
        foreach ($get_cita as $item){
            $idcita=$item->idcita; 
            $fecha_consulta=$item->fecha_consulta; 
            $hora_de=date("g:i A",strtotime($item->hora_de));
        }
        $data['idcita']=$idcita;
        $data['fecha_consulta']=$fecha_consulta;
        $data['hora_de']=$hora_de;
        /// 

        $arrayinfor2 = array('idreceta'=>$idreceta);
        $data['get_receta']=$this->General_model->getselectwhereall('receta',$arrayinfor2);
        /// 
        $arrayinfor = array('idconsulta'=>$idconsulta);
        $data['get_diagnosticos']=$this->General_model->getselectwhereall('diagnosticos',$arrayinfor);
        ///
        $arrayinfoe = array('idconsulta'=>$idconsulta,'activo'=>1);
        $data['get_orden_estudio']=$this->General_model->getselectwhereall('orden_estudio',$arrayinfoe);
        ///
        $arrayinfin = array('idconsulta'=>$idconsulta,'activo'=>1);
        $data['get_interconsulta']=$this->General_model->getselectwhereall('interconsulta',$arrayinfin);
        ///
        $arrayinfho = array('idconsulta'=>$idconsulta,'activo'=>1);
        $data['get_orden_hospital']=$this->General_model->getselectwhereall('orden_hospital',$arrayinfho);
        ///
        $arrayinfce = array('idconsulta'=>$idconsulta,'activo'=>1);
        $data['get_certificado_medico']=$this->General_model->getselectwhereall('certificado_medico',$arrayinfce);
        ///
        $arrayinfju = array('idconsulta'=>$idconsulta,'activo'=>1);
        $data['get_orden_justificante_medico']=$this->General_model->getselectwhereall('orden_justificante_medico',$arrayinfju);
        ///
        $arrayinfli = array('idconsulta'=>$idconsulta,'activo'=>1);
        $data['get_orden_orden_libre']=$this->General_model->getselectwhereall('orden_libre',$arrayinfli);
        ///
        
        $data['idconsulta']=$idconsulta;
        // receta_medicamento
    	$this->load->view('paciente/header');
        $this->load->view('paciente/parkinson',$data);
        $this->load->view('paciente/footer');
        $this->load->view('paciente/parkinsonjs');
    }
    public function pdfestudiolaboratorio($id){
        $idn=intval($id);
        $arrayinfo = array('idconsulta'=>intval($idn));
        $get_consultas=$this->General_model->getselectwhereall('consultas',$arrayinfo);
        $idpaciente=0;
        foreach ($get_consultas as $item){
            $idpaciente=$item->idpaciente;
            $data['consultafecha']=$item->consultafecha;
            
        } 
        $arrayinfop = array('idpaciente'=>$idpaciente);
        $get_consultasp=$this->General_model->getselectwhereall('pacientes',$arrayinfop);

        foreach ($get_consultasp as $itemp){
            $data['paciente']=$itemp->nombre.' '.$itemp->apll_paterno.' '.$itemp->apll_materno;
            $tiempo = strtotime($itemp->fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
            $data['edad'] = floor($edad); 
        } 
        $arrayinfor = array('idconsulta'=>$idn);
        $get_receta=$this->General_model->getselectwhereall('receta',$arrayinfor);
        $idreceta=0;
        foreach ($get_receta as $item){
            $idreceta=$item->idreceta; 
        }
        $arrayinfor2 = array('idreceta'=>$idreceta);
        $data['get_receta']=$this->General_model->getselectwhereall('receta',$arrayinfor2);
        /// 
        $arrayinfor = array('idconsulta'=>$idn);
        $data['get_diagnosticos']=$this->General_model->getselectwhereall('diagnosticos',$arrayinfor);
        ///
        $arrayinfoe = array('idconsulta'=>$idn,'activo'=>1);
        $data['get_orden_estudio']=$this->General_model->getselectwhereall('orden_estudio',$arrayinfoe);
        ///
        $data['idconsulta']=$idn;

        $this->load->view('Reportes/pdfparkinson',$data);
    }
    public function prueba_p(){
        $this->load->view('Reportes/pruebapar');
    }
    public function cita_confirmada(){
        $id=$this->input->post('id');
        $data['cita_confirmada']=1;
        $this->General_model->edit_record('idcita',$id,$data,'citas');
    }
    public function cancelar_cita(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idcita',$id,$data,'citas');
    }

    public function pdfhospitalizacion($id){
        $idn=intval($id);
        //
        $arrayinfor = array('idorden_hospital'=>$idn);
        $get_intercon=$this->General_model->getselectwhereall('orden_hospital',$arrayinfor);
        $detalles='';
        $idconsulta=0;
        foreach ($get_intercon as $item){
            $idconsulta=$item->idconsulta; 
            $detalles=$item->detalles; 
        }
        $data['detalles']=$detalles;
        $arrayinfo = array('idconsulta'=>intval($idconsulta));
        $get_consultas=$this->General_model->getselectwhereall('consultas',$arrayinfo);
        $idpaciente=0;
        foreach ($get_consultas as $item){
            $idpaciente=$item->idpaciente;
        } 
        $idpaciente=intval($idpaciente);
        $data['consultafecha']=$this->fechainicio;
        $arrayinfop = array('idpaciente'=>$idpaciente);
        $get_consultasp=$this->General_model->getselectwhereall('pacientes',$arrayinfop);
        foreach ($get_consultasp as $itemp){
            $data['paciente']=$itemp->nombre.' '.$itemp->apll_paterno.' '.$itemp->apll_materno;
            $tiempo = strtotime($itemp->fecha_nacimiento); 
            $ahora = time(); 
            $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
            $data['edad'] = floor($edad); 
        } 
        $this->load->view('Reportes/hospitalizacionpdf',$data);
    }
    
}    