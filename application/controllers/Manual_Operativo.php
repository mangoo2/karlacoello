<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manual_Operativo extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
		$this->load->view('manual_operativo/manual');
		$this->load->view('templates/footer');
		$this->load->view('manual_operativo/manualjs');
    }
}    		

