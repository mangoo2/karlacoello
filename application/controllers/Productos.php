<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }
    }
    public function index(){
        $data['get_tipo']=$this->General_model->get_records_condition('activo=1','tipo');
        $data['get_categoria']=$this->General_model->get_records_condition('activo=1','categoria');
        $data['get_marca']=$this->General_model->get_records_condition('activo=1','marca');
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('productos/productos',$data);
        $this->load->view('templates/footer');
        $this->load->view('productos/productosjs');
    }

    public function registro(){
        $data=$this->input->post();
        $idproducto=$data['idproducto'];
        if($idproducto==0){
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('productos',$data);
        }else{
            $this->General_model->edit_record('idproducto',$idproducto,$data,'productos');
            $id=$idproducto;
        }
        echo $id;
    }

    function cargafiles(){
        $id=$this->input->post('id');
        $folder="productos";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
          $array = array('foto'=>$newfile);
          $this->General_model->edit_record('idproducto',$id,$array,'productos');
          $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }  

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_producto($params);
        $totaldata= $this->ModelCatalogos->total_producto($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function deleteregistro(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idproducto',$id,$data,'productos');
    }
    
    public function registro_tipo(){
        $data=$this->input->post();
        $idtipo=$data['idtipo'];
        if($idtipo==0){
            $id=$this->General_model->add_record('tipo',$data);
        }else{
            $this->General_model->edit_record('idtipo',$idtipo,$data,'tipo');
            $id=$idtipo;
        }
        echo $id;
    }
    public function registro_categoria(){
        $data=$this->input->post();
        $idcategoria=$data['idcategoria'];
        if($idcategoria==0){
            $id=$this->General_model->add_record('categoria',$data);
        }else{
            $this->General_model->edit_record('idcategoria',$idcategoria,$data,'categoria');
            $id=$idcategoria;
        }
        echo $id;
    }

    public function registro_marca(){
        $data=$this->input->post();
        $idmarca=$data['idmarca'];
        if($idmarca==0){
            $id=$this->General_model->add_record('marca',$data);
        }else{
            $this->General_model->edit_record('idmarca',$idmarca,$data,'marca');
            $id=$idmarca;
        }
        echo $id;
    }

    public function getlistado_tipo(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_tipo($params);
        $totaldata= $this->ModelCatalogos->total_tipo($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function getlistado_categoria(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_categoria($params);
        $totaldata= $this->ModelCatalogos->total_categoria($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function getlistado_marca(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_marcas($params);
        $totaldata= $this->ModelCatalogos->total_marcas($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    
    public function deleteregistro_tipo(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idtipo',$id,$data,'tipo');
    }

    public function deleteregistro_cate(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idcategoria',$id,$data,'categoria');
    }

    public function deleteregistro_marca(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idmarca',$id,$data,'marca');
    }

    public function registra_almacen(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $data['idproducto']=$DATA[$i]->idproducto;
            $data['cantidad']=$DATA[$i]->stock;
            $data['stock']=$DATA[$i]->stock;
            $data['lote']=$DATA[$i]->lote;
            $data['fecha_registro']=$DATA[$i]->fecha_registro;
            $data['fecha_caducidad']=$DATA[$i]->fecha_caducidad; 
            $data['proveedor']=$DATA[$i]->idproveedor; 
            $data['reg']=$this->fecha_hora_actual; 
            $this->General_model->add_record('productos_almacen',$data);
        }
    }
    
    public function searchproveedor(){
        $search = $this->input->get('search');
        $results=$this->ModelCatalogos->getselectwherelikeproveedor($search);
        echo json_encode($results);    
    }
    public function inventario($idal,$id){
        $arraydata = array('alerta' => 0);
        $this->General_model->edit_record('idalmacen',$idal,$arraydata,'productos_almacen');
        $data['idpro']=$id;
        $data['fecha_actual']=$this->fechainicio;
        $data['fecha_mayor']=date("Y-m-d",strtotime($this->fechainicio."+ 7 days"));
        $data['productos_get']=$this->General_model->get_record('idproducto',$id,'productos');
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('productos/inventario',$data);
        $this->load->view('templates/footer');
        $this->load->view('productos/inventariojs');
    }
    public function inventario_producto($id){
        $data['idpro']=$id;
        $data['fecha_actual']=$this->fechainicio;
        $data['fecha_mayor']=date("Y-m-d",strtotime($this->fechainicio."+ 7 days"));
        $data['productos_get']=$this->General_model->get_record('idproducto',$id,'productos');
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('productos/inventario',$data);
        $this->load->view('templates/footer');
        $this->load->view('productos/inventariojs');
    }
    public function total_stock_get(){
        $id = $this->input->post('id');
        $results=$this->ModelCatalogos->getstock_producto($id);
        $aux_stock=0;
        foreach ($results as $i){
            $aux_stock=$i->stock;
        }
        echo $aux_stock;
    }

    public function getlistado_almacen(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_almacen($params);
        $totaldata= $this->ModelCatalogos->total_almacen($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function deleteregistro_almacen(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idalmacen',$id,$data,'productos_almacen');
    }
    public function validar_stock_almacen(){
        $id=$this->input->post('id');
        $results=$this->ModelCatalogos->getstock_producto($id);
        $aux_stock=0;
        foreach ($results as $i){
            $aux_stock=$i->stock;
        }
        echo $aux_stock;
    }
}