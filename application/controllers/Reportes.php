<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('Pdf');
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }
    }
    function ticket_medicina_estetica($id){
        
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(80, 250), true, 'UTF-8', false);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Mangoo');
        $pdf->SetTitle('Recibo');
        $pdf->SetSubject('Ticket por venta');
        $pdf->SetKeywords('Recibo, Pago, Comprobante');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set margins
        $pdf->SetMargins('8','9','8');
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetFont('courier', '', '10');

        $pdf->AddPage();
        
        $imglogo=base_url().'images/Logo.png';
        $ci=$this->General_model->get_record('idconsulta',$id,'consulta_medicina_estetica');
        $pa=$this->General_model->get_record('idpaciente',$ci->idpaciente,'pacientes');

        $html = '';
        $html .= '<table border="0">
                    <tr>
                        <td align="center">
                            <img src="'.$imglogo.'" width="100px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">Clínica Karla Coello</td>
                    </tr>
                    <tr>
                        <td align="center">Rejuvenecimiento Integral</td>
                    </tr>
                    <tr>
                        <td align="center">27 A Norte 1019 Colonia San Alejandro CP.72090
                           <hr></td>
                       
                    </tr>
                    <tr>
                        <td align="center" style="font-style: italic;">"La pasión cuando llega, nos hace más humildes y nos da un sentimiento de éxito diario"<br></td>
                        <hr>
                    </tr>
                    <tr>
                        <td align="center">Información de consulta</td>
                    </tr>
                    <tr>
                        <td align="center">Consulta: Medicina estética</td>
                    </tr>
                    <tr>
                        <td align="center">Fecha de consulta: '.date('d/m/Y',strtotime($ci->consultafecha)).'</td>
                    </tr>
                    <tr> 
                        <td align="center">Paciente: '.$pa->nombre.' '.$pa->apll_paterno.' '.$pa->apll_materno.'</td>
                    </tr>
                    <tr>
                        <hr> 
                        <td align="center">Productos y/o servicios</td>
                        <hr>
                    </tr>
                    <tr width="100%">
                        <td width="25%">Cant</td>
                        <td width="25%">Pro/Ser</td>
                        <td width="25%">P/U</td>
                        <td width="25%">Total</td>
                    </tr>';
                    $servi=$this->ModelCatalogos->get_venta_servicio_c1($id);
                    $sumas_aux=0;
                    foreach ($servi as $item) {
          $html .= '<tr width="100%">
                        <td width="25%"></td>
                        <td width="25%">'.$item->servicio.'</td>
                        <td width="25%">$'.$item->costo.'</td>
                        <td width="25%">$'.$item->costo.'</td>
                    </tr>';
                        $sumas_aux+=$item->costo;;
                    }
                    $produc=$this->ModelCatalogos->get_venta_productoc1($id);
                    $sumap_aux=0;
                    foreach ($produc as $item) {
          $html .= '<tr width="100%">
                        <td width="25%">'.$item->cantidad.'</td>
                        <td width="25%">'.$item->producto.'</td>
                        <td width="25%">$'.$item->preciou.'</td>
                        <td width="25%">$'.$item->total.'</td>
                    </tr>';
                        $sumap_aux+=$item->total;;
                    }
                    $sum_total=$sumas_aux+$sumap_aux;
                    //$total=$sum_total;
                    //$totaliva=16*$sum_total;
                    //$sumatotal=$totaliva/100;
                    //$tatolt=$sumatotal;
                    $totalx=$sum_total;
                    $iva_a=$totalx / 1.16;
                    $iva_au=$iva_a * 0.16;
          $html .= '<tr width="100%">
                        <td width="15%"></td>
                        <td width="20%"></td>
                        <td width="35%">Subtotal</td>
                        <td width="30%">$'.number_format($iva_a, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <td width="15%"></td>
                        <td width="20%"></td>
                        <td width="35%">IVA</td>
                        <td width="30%">$'.number_format($iva_au, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <td width="15%"></td>
                        <td width="20%"></td>
                        <td width="35%">TOTAL</td>
                        <td width="30%">$'.$sum_total.'</td>
                    </tr>
                    <tr width="100%">
                        <hr>
                        <td width="100%" align="center">
                            ¡GRACIAS POR TU PREFERENCIA!
                        </td>
                        <hr>
                    </tr>
                    <tr width="100%">
                        <td align="center">
                            Este ticket no tiene ninguna validéz o representación fiscal
                        </td>
                    </tr>

                    ';
  
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->IncludeJS('print(true);');
        $pdf->Output('ticket.pdf', 'I');

    }
    
    function reportenutricion($id=0){
        $data['resultlu']=$this->General_model->getselectwhereall2('plan_alimenticion_lunes',array('consultaId'=>$id,'activo'=>1));
        $data['resultma']=$this->General_model->getselectwhereall2('plan_alimenticion_martes',array('consultaId'=>$id,'activo'=>1));
        $data['resultmi']=$this->General_model->getselectwhereall2('plan_alimenticion_miercoles',array('consultaId'=>$id,'activo'=>1));
        $data['resultju']=$this->General_model->getselectwhereall2('plan_alimenticion_jueves',array('consultaId'=>$id,'activo'=>1));
        $data['resultvi']=$this->General_model->getselectwhereall2('plan_alimenticion_viernes',array('consultaId'=>$id,'activo'=>1));
        $resultconsulta=$this->General_model->getselectwhereall2('consulta_nutricion',array('idconsulta'=>$id,'activo'=>1));
        $resultconsulta=$resultconsulta->result();
        $resultconsulta=$resultconsulta[0];
        $data['resultconsulta']=$resultconsulta;

        $datospaciente=$this->General_model->getselectwhereall2('pacientes',array('idpaciente'=>$resultconsulta->idpaciente,'activo'=>1));
        $paciente='';
        $grafica='';
        foreach ($datospaciente->result() as $item) {
            $paciente=$item->nombre.' '.$item->apll_paterno.' '.$item->apll_materno;
            $grafica=$item->grafica;
        }
        $data['paciente'] =$paciente;
        $data['grafica'] =$grafica;
        $this->load->view('Reportes/nutricion',$data);
    }
    /// ticket span
    function ticket_pan($id){
        
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(80, 250), true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Mangoo');
        $pdf->SetTitle('Recibo');
        $pdf->SetSubject('Ticket por venta');
        $pdf->SetKeywords('Recibo, Pago, Comprobante');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set margins
        $pdf->SetMargins('8','9','8');
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetFont('courier', '', '10');

        $pdf->AddPage();
        
        $imglogo=base_url().'images/Logo.png';
        $ci=$this->General_model->get_record('idconsulta',$id,'consulta_spa');
        $pa=$this->General_model->get_record('idpaciente',$ci->idpaciente,'pacientes');

        $html = '';
        $html .= '<table border="0">
                    <tr>
                        <td align="center">
                            <img src="'.$imglogo.'" width="100px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">Clínica Karla Coello</td>
                    </tr>
                    <tr>
                        <td align="center">Rejuvenecimiento Integral</td>
                    </tr>
                    <tr>
                        <td align="center">27 A Norte 1019 Colonia San Alejandro CP.72090
                           <hr></td>
                       
                    </tr>
                    <tr>
                        <td align="center" style="font-style: italic;">"La pasión cuando llega, nos hace más humildes y nos da un sentimiento de éxito diario"<br></td>
                        <hr>
                    </tr>
                    <tr>
                        <td align="center">Información de consulta</td>
                    </tr>
                    <tr>
                        <td align="center">Consulta: SPA</td>
                    </tr>
                    <tr>
                        <td align="center">Fecha de consulta: '.date('d/m/Y',strtotime($ci->consultafecha)).'</td>
                    </tr>
                    <tr> 
                        <td align="center">Paciente: '.$pa->nombre.' '.$pa->apll_paterno.' '.$pa->apll_materno.'</td>
                    </tr>
                    <tr>
                        <hr> 
                        <td align="center">Productos y/o servicios</td>
                        <hr>
                    </tr>
                    <tr width="100%">
                        <td width="25%">Cant</td>
                        <td width="25%">Pro/Ser</td>
                        <td width="25%">P/U</td>
                        <td width="25%">Total</td>
                    </tr>';
                    $servi=$this->ModelCatalogos->get_venta_servicio_c2($id);
                    $sumas_aux=0;
                    foreach ($servi as $item) {
          $html .= '<tr width="100%">
                        <td width="25%"></td>
                        <td width="25%">'.$item->servicio.'</td>
                        <td width="25%">$'.$item->costo.'</td>
                        <td width="25%">$'.$item->costo.'</td>
                    </tr>';
                        $sumas_aux+=$item->costo;;
                    }
                    $produc=$this->ModelCatalogos->get_venta_productoc2($id);
                    $sumap_aux=0;
                    foreach ($produc as $item) {
          $html .= '<tr width="100%">
                        <td width="25%">'.$item->cantidad.'</td>
                        <td width="25%">'.$item->producto.'</td>
                        <td width="25%">$'.$item->preciou.'</td>
                        <td width="25%">$'.$item->total.'</td>
                    </tr>';
                        $sumap_aux+=$item->total;;
                    }
                    $sum_total=$sumas_aux+$sumap_aux;
                    //$total=$sum_total;
                    //$totaliva=16*$sum_total;
                    //$sumatotal=$totaliva/100;
                    //$tatolt=$sumatotal;
                    $totalx=$sum_total;
                    $iva_a=$totalx / 1.16;
                    $iva_au=$iva_a * 0.16;
          $html .= '<tr width="100%">
                        <td width="15%"></td>
                        <td width="20%"></td>
                        <td width="35%">Subtotal</td>
                        <td width="30%">$'.number_format($iva_a, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <td width="15%"></td>
                        <td width="20%"></td>
                        <td width="35%">IVA</td>
                        <td width="30%">$'.number_format($iva_au, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <td width="15%"></td>
                        <td width="20%"></td>
                        <td width="35%">TOTAL</td>
                        <td width="30%">$'.$sum_total.'</td>
                    </tr>
                    <tr width="100%">
                        <hr>
                        <td width="100%" align="center">
                            ¡GRACIAS POR TU PREFERENCIA!
                        </td>
                        <hr>
                    </tr>
                    <tr width="100%">
                        <td align="center">
                            Este ticket no tiene ninguna validéz o representación fiscal
                        </td>
                    </tr>

                    ';
  
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->IncludeJS('print(true);');
        $pdf->Output('ticket.pdf', 'I');

    }

    function ticket_nutricion($id){
        
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(80, 250), true, 'UTF-8', false);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Mangoo');
        $pdf->SetTitle('Recibo');
        $pdf->SetSubject('Ticket por venta');
        $pdf->SetKeywords('Recibo, Pago, Comprobante');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set margins
        $pdf->SetMargins('8','9','8');
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetFont('courier', '', '10');

        $pdf->AddPage();
        
        $imglogo=base_url().'images/Logo.png';
        $ci=$this->General_model->get_record('idconsulta',$id,'consulta_nutricion');
        $pa=$this->General_model->get_record('idpaciente',$ci->idpaciente,'pacientes');

        $html = '';
        $html .= '<table border="0">
                    <tr>
                        <td align="center">
                            <img src="'.$imglogo.'" width="100px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">Clínica Karla Coello</td>
                    </tr>
                    <tr>
                        <td align="center">Rejuvenecimiento Integral</td>
                    </tr>
                    <tr>
                        <td align="center">27 A Norte 1019 Colonia San Alejandro CP.72090
                           <hr></td>
                       
                    </tr>
                    <tr>
                        <td align="center" style="font-style: italic;">"La pasión cuando llega, nos hace más humildes y nos da un sentimiento de éxito diario"<br></td>
                        <hr>
                    </tr>
                    <tr>
                        <td align="center">Información de consulta</td>
                    </tr>
                    <tr>
                        <td align="center">Consulta: Nutrición</td>
                    </tr>
                    <tr>
                        <td align="center">Fecha de consulta: '.date('d/m/Y',strtotime($ci->consultafecha)).'</td>
                    </tr>
                    <tr> 
                        <td align="center">Paciente: '.$pa->nombre.' '.$pa->apll_paterno.' '.$pa->apll_materno.'</td>
                    </tr>
                    <tr>
                        <hr> 
                        <td align="center">Productos y/o servicios</td>
                        <hr>
                    </tr>
                    <tr width="100%">
                        <td width="25%">Cant</td>
                        <td width="25%">Pro/Ser</td>
                        <td width="25%">P/U</td>
                        <td width="25%">Total</td>
                    </tr>';
                    $servi=$this->ModelCatalogos->get_venta_servicio_c3($id);
                    $sumas_aux=0;
                    foreach ($servi as $item) {
          $html .= '<tr width="100%">
                        <td width="25%"></td>
                        <td width="25%">'.$item->servicio.'</td>
                        <td width="25%">$'.$item->costo.'</td>
                        <td width="25%">$'.$item->costo.'</td>
                    </tr>';
                        $sumas_aux+=$item->costo;;
                    }
                    $produc=$this->ModelCatalogos->get_venta_productoc3($id);
                    $sumap_aux=0;
                    foreach ($produc as $item) {
          $html .= '<tr width="100%">
                        <td width="25%">'.$item->cantidad.'</td>
                        <td width="25%">'.$item->producto.'</td>
                        <td width="25%">$'.$item->preciou.'</td>
                        <td width="25%">$'.$item->total.'</td>
                    </tr>';
                        $sumap_aux+=$item->total;;
                    }
                    $sum_total=$sumas_aux+$sumap_aux;
                    //$total=$sum_total;
                    //$totaliva=16*$sum_total;
                    //$sumatotal=$totaliva/100;
                    //$tatolt=$sumatotal;
                    $totalx=$sum_total;
                    $iva_a=$totalx / 1.16;
                    $iva_au=$iva_a * 0.16;
          $html .= '<tr width="100%">
                        <td width="15%"></td>
                        <td width="20%"></td>
                        <td width="35%">Subtotal</td>
                        <td width="30%">$'.number_format($iva_a, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <td width="15%"></td>
                        <td width="20%"></td>
                        <td width="35%">IVA</td>
                        <td width="30%">$'.number_format($iva_au, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <td width="15%"></td>
                        <td width="20%"></td>
                        <td width="35%">TOTAL</td>
                        <td width="30%">$'.$sum_total.'</td>
                    </tr>
                    <tr width="100%">
                        <hr>
                        <td width="100%" align="center">
                            ¡GRACIAS POR TU PREFERENCIA!
                        </td>
                        <hr>
                    </tr>
                    <tr width="100%">
                        <td align="center">
                            Este ticket no tiene ninguna validéz o representación fiscal
                        </td>
                    </tr>

                    ';
  
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->IncludeJS('print(true);');
        $pdf->Output('ticket.pdf', 'I');

    }
    //// Paciente producto
    function ticket_paciente_producto($id){
        
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(80, 250), true, 'UTF-8', false);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Mangoo');
        $pdf->SetTitle('Recibo');
        $pdf->SetSubject('Ticket por venta');
        $pdf->SetKeywords('Recibo, Pago, Comprobante');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set margins
        $pdf->SetMargins('8','9','8');
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetFont('courier', '', '10');

        $pdf->AddPage();
        
        $imglogo=base_url().'images/Logo.png';
        $ci=$this->General_model->get_record('idventa',$id,'pacientes_productos_venta');
        $pa=$this->General_model->get_record('idpaciente',$ci->idpaciente,'pacientes');

        $html = '';
        $html .= '<table border="0">
                    <tr>
                        <td align="center">
                            <img src="'.$imglogo.'" width="100px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">Clínica Karla Coello</td>
                    </tr>
                    <tr>
                        <td align="center">Rejuvenecimiento Integral</td>
                    </tr>
                    <tr>
                        <td align="center">27 A Norte 1019 Colonia San Alejandro CP.72090
                           <hr></td>
                       
                    </tr>
                    <tr>
                        <td align="center" style="font-style: italic;">"La pasión cuando llega, nos hace más humildes y nos da un sentimiento de éxito diario"<br></td>
                        <hr>
                    </tr>
                    <tr>
                        <td align="center">Información de consulta</td>
                    </tr>
                    <tr>
                        <td align="center">Farmacia</td>
                    </tr>
                    <tr>
                        <td align="center">Fecha de venta: '.date('d/m/Y').'</td>
                    </tr>
                    <tr> 
                        <td align="center">Paciente: '.$pa->nombre.' '.$pa->apll_paterno.' '.$pa->apll_materno.'</td>
                    </tr>
                    <tr>
                        <hr> 
                        <td align="center">Productos</td>
                        <hr>
                    </tr>
                    <tr width="100%">
                        <td width="25%">Cant</td>
                        <td width="25%">Pro/Ser</td>
                        <td width="25%">P/U</td>
                        <td width="25%">Total</td>
                    </tr>';
                    $produc=$this->ModelCatalogos->get_venta_producto_paciente($id);
                    $sumap_aux=0;
                    foreach ($produc as $item) {
          $html .= '<tr width="100%">
                        <td width="25%">'.$item->cantidad.'</td>
                        <td width="25%">'.$item->producto.'</td>
                        <td width="25%">$'.$item->preciou.'</td>
                        <td width="25%">$'.$item->total.'</td>
                    </tr>';
                        $sumap_aux+=$item->total;;
                    }
                    $sum_total=$sumap_aux;
                    $totalx=$sum_total;
                    $iva_a=$totalx / 1.16;
                    $iva_au=$iva_a * 0.16;
          $html .= '<tr width="100%">
                        <td width="15%"></td>
                        <td width="20%"></td>
                        <td width="35%">Subtotal</td>
                        <td width="30%">$'.number_format($iva_a, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <td width="15%"></td>
                        <td width="20%"></td>
                        <td width="35%">IVA</td>
                        <td width="30%">$'.number_format($iva_au, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <td width="15%"></td>
                        <td width="20%"></td>
                        <td width="35%">TOTAL</td>
                        <td width="30%">$'.$sum_total.'</td>
                    </tr>
                    <tr width="100%">
                        <hr>
                        <td width="100%" align="center">
                            ¡GRACIAS POR TU PREFERENCIA!
                        </td>
                        <hr>
                    </tr>
                    <tr width="100%">
                        <td align="center">
                            Este ticket no tiene ninguna validéz o representación fiscal
                        </td>
                    </tr>

                    ';
  
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->IncludeJS('print(true);');
        $pdf->Output('ticket.pdf', 'I');

    }
}    