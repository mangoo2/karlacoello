<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }
    }
	public function index()
	{
        $arraypaciente = array('activo'=>1);
        $list_paciente=$this->General_model->getselectwhereall('pacientes',$arraypaciente);
        $data['pacientes']=$list_paciente;
        $data['perfilid']=$this->perfilid;
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
		$this->load->view('inicio',$data);
		$this->load->view('templates/footer');
        $this->load->view('iniciojs');
	}
    public function registraPaciente(){
        $data=$this->input->post();
        if(isset($data['check_acepta'])){
            $data['check_acepta']=1;
        }else{
            $data['check_acepta']=0; 
        }
        $data['reg']=$this->fechahoy;
        $data['personalId']=$this->idpersonal;
        $id=$this->General_model->add_record('pacientes',$data);
        $datahc['idpaciente']=$id; 
        $this->General_model->add_record('historia_clinica',$datahc);
        echo $id;
    }
    function cargafiles(){
        $id=$this->input->post('id');
        $folder="pacientes";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
          $array = array('foto'=>$newfile);
          $this->General_model->edit_record('idpaciente',$id,$array,'pacientes');
          $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }  
    public function getlistadoPaciente(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_paciente($params);
        $totaldata= $this->ModelCatalogos->total_paciente($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function get_validar_paciente(){
        $nom = $this->input->post('nom');
        $appp = $this->input->post('appp');
        $appm = $this->input->post('appm'); 
        $get_paciente = $this->ModelCatalogos->get_select_like_paciente($nom,$appp,$appm);
        $html='';
        foreach ($get_paciente as $item) {
            $html=$item->nombre.' '.$item->apll_paterno.' '.$item->apll_materno;
        }
        echo $html;
    }
    public function searchproducto(){
        $search = $this->input->get('search');
        $results=$this->ModelCatalogos->getselectwherelikesproducto($search);
        echo json_encode($results);    
    }
    public function get_detalles_productos(){
        $id=$this->input->post('id');
        $html='';
        $aux=0;
        $results=$this->General_model->get_records_condition('idproducto='.$id.' AND activo=1','productos_almacen');
        $html.='<div class="row">
                    <div class="col-lg-6">
                        <label>Lotes </label>
                        <div class="input-group mb-3">
                            <select class="form-control" id="idalmacen" onchange="select_producto_detalle()">';
                                $html.='<option value="0" selected>Seleccionar una opción</option>';
                            foreach ($results as $item){
                                $aux=1;
                                ////
                                if($item->stock!=0){
                                    $etiqueta_caduca='';
                                    if($this->fechainicio <= $item->fecha_caducidad && date("Y-m-d",strtotime($this->fechainicio."+ 7 days")) >= $item->fecha_caducidad){ 
                                        $etiqueta_color='style="background-color: #fec107;color: white;"';
                                        $etiqueta_caduca='Próximo a caducar';
                                    }else if($this->fechainicio > $item->fecha_caducidad){
                                        $etiqueta_color='style="background-color: #e46a76;color: white;"';
                                        $etiqueta_caduca='Lote caducado';
                                    }else{
                                        $etiqueta_color='style="background-color: #00c292;color: white;"';
                                        $etiqueta_caduca='Vigente';
                                    }  
                                    ////
                                    $html.='<option value="'.$item->idalmacen.'" '.$etiqueta_color.'>'.$item->lote.' / Stock:'.$item->stock.' / Fecha caducidad:'.$etiqueta_caduca.'-'.date("d/m/Y",strtotime($item->fecha_caducidad)).'</option>';
                                }
                            } 
                    $html.='</select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <label>Cantidad </label>
                        <div class="input-group mb-3">
                            <input type="number" id="cantidad_lote" class="form-control colorlabel_white">
                            <div class="input-group-append">
                                <button type="button" class="btn waves-effect waves-light btn_sistema" onclick="agregar_productos_venta()"><i class="fas fa-plus-circle"></i></button>
                            </div>
                        </div>
                    </div>
                </div>';
        
        $aux_html='';
        if($aux==1){
            $aux_html=$html;
        }else{
            $aux_html='<div class="row">
                            <div class="col-lg-12">
                                <br><br>
                                <span class="badge badge-pill badge-danger">Producto no disponible en Stock</span>
                            </div>
                        </div>';
        }
        
        echo $aux_html;                     
    }

    public function get_producto_almacen(){
        $id=$this->input->post('id');
        $idalmacen=0;
        $lote='';
        $cadu='';
        $cantidad=0;
        $results=$this->General_model->get_records_condition('idalmacen='.$id,'productos_almacen');
        foreach ($results as $item){
            $idalmacen=$item->idalmacen;
            $lote=$item->lote;
            $cadu=$item->fecha_caducidad;
            $cantidad=$item->stock;
        }
        $arrayinfo = array('idalmacen'=>$idalmacen,'lote'=>$lote,'cadu'=>date('d/m/Y',strtotime($cadu)),'cantidad'=>$cantidad);
        echo json_encode($arrayinfo);
    }
    public function venta_productos(){
        $datos = $this->input->post();
        $datos['personalId']=$this->idpersonal;
        $datos['reg']=$this->fechahoy;
        $id_aux=$this->General_model->add_record('pacientes_productos_venta',$datos);
        echo $id_aux;
    }
    // Venta de Productos 
    public function registro_venta_producto(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        $id_aux=0;
        for ($i=0;$i<count($DATA);$i++) { 
            $iddetalles = $DATA[$i]->iddetalles;
            $idalmacen=$DATA[$i]->idalmacen; 
            $cantidad=$DATA[$i]->cantidad; 
            $data['idventa']=$DATA[$i]->idventa;   
            $data['idpaciente']=$DATA[$i]->idpaciente;   
            $data['idalmacen']=$DATA[$i]->idalmacen; 
            $data['cantidad']=$DATA[$i]->cantidad; 
            $data['preciou']=$DATA[$i]->preciou; 
            $data['total']=$DATA[$i]->totalp; 
            if(intval($iddetalles)==0){
                $id_aux=$this->General_model->add_record('pacientes_productos_venta_detalles',$data);
                $this->ModelCatalogos->get_venta_producto_restac1($idalmacen,$cantidad);
            }else{
                $this->General_model->edit_record('iddetalles',$iddetalles,$data,'pacientes_productos_venta_detalles');
            }
        }
    }
    function productos($id){
        $data['get_pt']=$this->General_model->get_record('idpaciente',$id,'pacientes');
        $data['get_paciente']=$this->ModelCatalogos->get_productos_paciente($id);
        $data['get_paciente_nu']=$this->ModelCatalogos->get_productos_paciente_medicina($id);
        $data['get_paciente_spa']=$this->ModelCatalogos->get_productos_paciente_spa($id);
        $data['get_paciente_nutricion']=$this->ModelCatalogos->get_productos_paciente_nutricion($id);
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('productos',$data);
        $this->load->view('templates/footer');
        $this->load->view('productosjs');
    }

}
