<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estadisticas extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloReportes');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }
    }
    public function index(){   
        $data['total_pacientes']=$this->ModelCatalogos->total_pacientes($this->idpersonal); 
        $data['total_consultashoy']=$this->ModelCatalogos->total_consultas_hoy($this->idpersonal,$this->fechainicio); 
        $diaSemana = date("w");
        $year=date('Y');
        $month=date('m');
        $day=date('d');
        # Obtenemos el numero de la semana
        $semana=date("W",mktime(0,0,0,$month,$day,$year));
        # Obtenemos el día de la semana de la fecha dada
        $diaSemana=date("w",mktime(0,0,0,$month,$day,$year));
        $primerDia=date("Y-m-d",mktime(0,0,0,$month,$day-$diaSemana,$year));
        $ultimoDia=date("Y-m-d",mktime(0,0,0,$month,$day+(7-$diaSemana-1),$year));
        $data['total_semena']=$this->ModelCatalogos->total_consultas_semana($this->idpersonal,$primerDia,$ultimoDia); 
        //
        $dia_anterior_semana=date("Y-m-d",mktime(0,0,0,$month,$day-(7-$diaSemana+1),$year));
        $data['total_semena_aterior']=$this->ModelCatalogos->total_consultas_semana_anterior($this->idpersonal,$dia_anterior_semana,$primerDia); 
        $fecha = new DateTime();
        $fecha->modify('first day of this month');
        $fecha_inicio=$fecha->format('Y-m-d');
        $fecha = new DateTime();
        $fecha->modify('last day of this month');
        $fecha_fin=$fecha->format('Y-m-d');
        $data['totalconsultasmesactual']=$this->ModelCatalogos->total_consultas_mes_actual($this->idpersonal,$fecha_inicio,$fecha_fin); 

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('estadisticas/estadisticas',$data);
        $this->load->view('templates/footer');
        $this->load->view('estadisticas/estadisticasjs');
    }

    public function datatable_prods(){
        $params = $this->input->post();
        $datas = $this->ModeloReportes->reporte_prods($params);
        $json_data = array("data"=>$datas);
        echo json_encode($json_data);
    } 

    public function datatable_tot_consult(){
        $params = $this->input->post();
        $datas = $this->ModeloReportes->reporte_total_consult($params);
        echo json_encode($datas);
    } 

    public function datatable_spa(){
        $params = $this->input->post();
        $datas = $this->ModeloReportes->reporte_spa($params);
        $json_data = array("data"=>$datas);
        echo json_encode($json_data);
    } 

    public function datatable_med(){
        $params = $this->input->post();
        $datas = $this->ModeloReportes->reporte_med($params);
        $json_data = array("data"=>$datas);
        echo json_encode($json_data);
    }

     public function datatable_nutri(){
        $params = $this->input->post();
        $datas = $this->ModeloReportes->reporte_nutri($params);
        $json_data = array("data"=>$datas);
        echo json_encode($json_data);
    }

    public function datatable_tot_consultSpa(){
        $params = $this->input->post();
        $datas = $this->ModeloReportes->reporte_total_consultSpa($params);
        echo json_encode($datas);
    }

    public function datatable_tot_consultME(){
        $params = $this->input->post();
        $datas = $this->ModeloReportes->reporte_total_consultME($params);
        echo json_encode($datas);
    }

    public function datatable_tot_consultNut(){
        $params = $this->input->post();
        $datas = $this->ModeloReportes->reporte_total_consultNut($params);
        echo json_encode($datas);
    }
}