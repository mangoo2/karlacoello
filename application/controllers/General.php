<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
        }
    }
    public function index(){
        
    }

    public function verduras(){
        $data=$this->input->post();
        $cantidad=$data['cantidad'];
        $result_l=$this->General_model->get_info_random('n_verduras', $cantidad);
        $result_m=$this->General_model->get_info_random('n_verduras', $cantidad);
        $result_mi=$this->General_model->get_info_random('n_verduras', $cantidad);
        $result_j=$this->General_model->get_info_random('n_verduras', $cantidad);
        $result_v=$this->General_model->get_info_random('n_verduras', $cantidad);

            $array = array(
                        'lu'=>$result_l->result(),
                        'ma'=>$result_m->result(),
                        'mi'=>$result_mi->result(),
                        'ju'=>$result_j->result(),
                        'vi'=>$result_v->result()
                        );
        echo json_encode($array);
    }
    public function fruta(){
        $data=$this->input->post();
        $cantidad=$data['cantidad'];
        $result_l=$this->General_model->get_info_random('n_fruta', $cantidad);
        $result_m=$this->General_model->get_info_random('n_fruta', $cantidad);
        $result_mi=$this->General_model->get_info_random('n_fruta', $cantidad);
        $result_j=$this->General_model->get_info_random('n_fruta', $cantidad);
        $result_v=$this->General_model->get_info_random('n_fruta', $cantidad);

            $array = array(
                        'lu'=>$result_l->result(),
                        'ma'=>$result_m->result(),
                        'mi'=>$result_mi->result(),
                        'ju'=>$result_j->result(),
                        'vi'=>$result_v->result()
                        );
        echo json_encode($array);
    }
    public function cereales(){
        $data=$this->input->post();
        $cantidad=$data['cantidad'];
        $result_l=$this->General_model->get_info_random('n_cereal', $cantidad);
        $result_m=$this->General_model->get_info_random('n_cereal', $cantidad);
        $result_mi=$this->General_model->get_info_random('n_cereal', $cantidad);
        $result_j=$this->General_model->get_info_random('n_cereal', $cantidad);
        $result_v=$this->General_model->get_info_random('n_cereal', $cantidad);

            $array = array(
                        'lu'=>$result_l->result(),
                        'ma'=>$result_m->result(),
                        'mi'=>$result_mi->result(),
                        'ju'=>$result_j->result(),
                        'vi'=>$result_v->result()
                        );
        echo json_encode($array);
    }
    public function leguminosas(){
        $data=$this->input->post();
        $cantidad=$data['cantidad'];
        $result_l=$this->General_model->get_info_random('n_leguminosas', $cantidad);
        $result_m=$this->General_model->get_info_random('n_leguminosas', $cantidad);
        $result_mi=$this->General_model->get_info_random('n_leguminosas', $cantidad);
        $result_j=$this->General_model->get_info_random('n_leguminosas', $cantidad);
        $result_v=$this->General_model->get_info_random('n_leguminosas', $cantidad);

            $array = array(
                        'lu'=>$result_l->result(),
                        'ma'=>$result_m->result(),
                        'mi'=>$result_mi->result(),
                        'ju'=>$result_j->result(),
                        'vi'=>$result_v->result()
                        );
        echo json_encode($array);
    }
    public function aoal(){
        $data=$this->input->post();
        $cantidad=$data['cantidad'];
        $result_l=$this->General_model->get_info_random('n_alimentos_origen_animal', $cantidad);
        $result_m=$this->General_model->get_info_random('n_alimentos_origen_animal', $cantidad);
        $result_mi=$this->General_model->get_info_random('n_alimentos_origen_animal', $cantidad);
        $result_j=$this->General_model->get_info_random('n_alimentos_origen_animal', $cantidad);
        $result_v=$this->General_model->get_info_random('n_alimentos_origen_animal', $cantidad);

            $array = array(
                        'lu'=>$result_l->result(),
                        'ma'=>$result_m->result(),
                        'mi'=>$result_mi->result(),
                        'ju'=>$result_j->result(),
                        'vi'=>$result_v->result()
                        );
        echo json_encode($array);
    }
    public function leyog(){
        $data=$this->input->post();
        $cantidad=$data['cantidad'];
        $result_l=$this->General_model->get_info_random('n_leche', $cantidad);
        $result_m=$this->General_model->get_info_random('n_leche', $cantidad);
        $result_mi=$this->General_model->get_info_random('n_leche', $cantidad);
        $result_j=$this->General_model->get_info_random('n_leche', $cantidad);
        $result_v=$this->General_model->get_info_random('n_leche', $cantidad);

            $array = array(
                        'lu'=>$result_l->result(),
                        'ma'=>$result_m->result(),
                        'mi'=>$result_mi->result(),
                        'ju'=>$result_j->result(),
                        'vi'=>$result_v->result()
                        );
        echo json_encode($array);
    }
    public function acs(){
        $data=$this->input->post();
        $cantidad=$data['cantidad'];
        $result_l=$this->General_model->get_info_random('n_aceites_grasas_sin_proteína', $cantidad);
        $result_m=$this->General_model->get_info_random('n_aceites_grasas_sin_proteína', $cantidad);
        $result_mi=$this->General_model->get_info_random('n_aceites_grasas_sin_proteína', $cantidad);
        $result_j=$this->General_model->get_info_random('n_aceites_grasas_sin_proteína', $cantidad);
        $result_v=$this->General_model->get_info_random('n_aceites_grasas_sin_proteína', $cantidad);

            $array = array(
                        'lu'=>$result_l->result(),
                        'ma'=>$result_m->result(),
                        'mi'=>$result_mi->result(),
                        'ju'=>$result_j->result(),
                        'vi'=>$result_v->result()
                        );
        echo json_encode($array);
    }
    public function acc(){
        $data=$this->input->post();
        $cantidad=$data['cantidad'];
        $result_l=$this->General_model->get_info_random('n_aceites_grasas_con_proteina', $cantidad);
        $result_m=$this->General_model->get_info_random('n_aceites_grasas_con_proteina', $cantidad);
        $result_mi=$this->General_model->get_info_random('n_aceites_grasas_con_proteina', $cantidad);
        $result_j=$this->General_model->get_info_random('n_aceites_grasas_con_proteina', $cantidad);
        $result_v=$this->General_model->get_info_random('n_aceites_grasas_con_proteina', $cantidad);

            $array = array(
                        'lu'=>$result_l->result(),
                        'ma'=>$result_m->result(),
                        'mi'=>$result_mi->result(),
                        'ju'=>$result_j->result(),
                        'vi'=>$result_v->result()
                        );
        echo json_encode($array);
    }
    public function azucara(){
        $data=$this->input->post();
        $cantidad=$data['cantidad'];
        $result_l=$this->General_model->get_info_random('n_azucar', $cantidad);
        $result_m=$this->General_model->get_info_random('n_azucar', $cantidad);
        $result_mi=$this->General_model->get_info_random('n_azucar', $cantidad);
        $result_j=$this->General_model->get_info_random('n_azucar', $cantidad);
        $result_v=$this->General_model->get_info_random('n_azucar', $cantidad);

            $array = array(
                        'lu'=>$result_l->result(),
                        'ma'=>$result_m->result(),
                        'mi'=>$result_mi->result(),
                        'ju'=>$result_j->result(),
                        'vi'=>$result_v->result()
                        );
        echo json_encode($array);
    }
    function save_pla_alimenticio(){
        $params=$this->input->post();
        $arrayPlu = $params['arrayPlu'];
        $arrayPma = $params['arrayPma'];
        $arrayPmi = $params['arrayPmi'];
        $arrayPju = $params['arrayPju'];
        $arrayPvi = $params['arrayPvi'];

        $DATAlu = json_decode($arrayPlu);
        $DATAma = json_decode($arrayPma);
        $DATAmi = json_decode($arrayPmi);
        $DATAju = json_decode($arrayPju);
        $DATAvi = json_decode($arrayPvi);

        for ($i=0;$i<count($DATAlu);$i++) {
            $this->General_model->add_record('plan_alimenticion_lunes',array('consultaId'=>$DATAlu[$i]->consultaId,'contenido'=>$DATAlu[$i]->contenido));
        }
        for ($j=0;$j<count($DATAma);$j++) {
            $this->General_model->add_record('plan_alimenticion_martes',array('consultaId'=>$DATAma[$j]->consultaId,'contenido'=>$DATAma[$j]->contenido));
        }
        for ($k=0;$k<count($DATAmi);$k++) {
            $this->General_model->add_record('plan_alimenticion_miercoles',array('consultaId'=>$DATAmi[$k]->consultaId,'contenido'=>$DATAmi[$k]->contenido));
        }
        for ($l=0;$l<count($DATAju);$l++) {
            $this->General_model->add_record('plan_alimenticion_jueves',array('consultaId'=>$DATAju[$l]->consultaId,'contenido'=>$DATAju[$l]->contenido));
        }
        for ($m=0;$m<count($DATAvi);$m++) {
            $this->General_model->add_record('plan_alimenticion_viernes',array('consultaId'=>$DATAvi[$m]->consultaId,'contenido'=>$DATAvi[$m]->contenido));
        }
    }
    
  

}